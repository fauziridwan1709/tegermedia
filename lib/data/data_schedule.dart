part of '_static_data.dart';

///Schedule shared value
const List<String> iconMateri = [
  'Kimia',
  'Fisika',
  'Biologi',
  'Matematika',
  'Menghitung',
  'Agama Islam',
  'Agama Hindu',
  'Agama Khonghucu',
  'Agama Kristen',
  'Antropologi',
  'Bahasa Indonesia',
  'Bahasa Inggris',
  'Ekonomi',
  'Geografi',
  'IPA',
  'IPS',
  'Kepribadian',
  'Membaca',
  'Menghitung',
  'Panduan',
  'PJOK',
  'PPKn',
  'Sejarah',
  'Seni Budaya',
  'Sosiologi',
  'Tematik'
];

const List<String> kategori = ['Pelajaran', 'Ujian', 'Ekskul'];
// 'Acara Sekolah'];
const List<String> tahun = ['2020/2021'];
