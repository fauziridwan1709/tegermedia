part of '_static_data.dart';

const List<String> calendarScopes = <String>[
  'https://www.googleapis.com/auth/calendar',
];

const List<String> driveScopes = <String>[
  'https://www.googleapis.com/auth/drive.file',
];

const List<String> classroomScopes = <String>[
  'https://www.googleapis.com/auth/classroom.courses',
  'https://www.googleapis.com/auth/classroom.rosters',
  // 'https://www.googleapis.com/auth/classroom.profile.emails',
  // 'https://www.googleapis.com/auth/classroom.profile.photos',
  // 'https://www.googleapis.com/auth/classroom.coursework.me',
  // 'https://www.googleapis.com/auth/classroom.coursework.students',
  // 'https://www.googleapis.com/auth/classroom.courseworkmaterials'
];
