part of '_static_data.dart';

class MenuData {
  static String homeI = 'assets/icons/home';
  static String homeImg = 'assets/images/asset_home';
  static Map<String, String> menu = {
    'Tugas': '$homeI/tugas.png',
    'Ujian': '$homeI/ujian.png',
    'Jadwal': '$homeI/jadwal.png',
    'Kelasku': '$homeI/kelasku.png',
    // 'Lainnya': '$homeI/lainnya.png',
    'Konferensi': '$homeI/konferensi.png',
  };

  static Map<String, String> menuWeb = {
    'Tugas': '$homeI/tugas.png',
    'Ujian': '$homeI/ujian.png',
    'Jadwal': '$homeI/jadwal.png',
    'Kelasku': '$homeI/kelasku.png',
    'Absensi': '$homeI/absensi.png',
    'Dokumen': '$homeI/dokumen.png',
    'Forum': '$homeI/forum.png',
    'Konferensi': '$homeI/konferensi.png',
    'Nilai': '$homeI/nilai.png',
  };

  static Map<String, String> kelas = {
    'Tugas': '$homeImg/tugas.png',
    'Ujian': '$homeImg/ujian.png',
    'Forum': '$homeImg/forum.png',
    'Absensi': '$homeImg/absensi.png',
    'Jadwal': '$homeImg/jadwal.png',
    'Kelasku': '$homeImg/kelasku.png',
    'Dokumen': '$homeImg/dokumen.png',
    'Konferensi': '$homeImg/konferensi.png',
  };

  static Map<String, String> personal = {
    'Tugas': '$homeI/tugas.png',
    'Konferensi': '$homeI/konferensi.png',
    'Kelasku': '$homeI/kelasku.png',
  };

  static Map<String, String> akademis = {
    'Nilai': '$homeI/nilai.png',
    // 'Aktifitas': '$homeI/aktifitas.png',
    // 'Pendaftaran': '$homeI/pendaftaran.png',
    // 'Alumni': '$homeI/alumni.png',
  };

  static Map<String, String> pendidikan = {
    // 'Donasi': '$homeI/donasi.png',
    // 'Belanja': '$homeI/belanja.png',
    'Jadwal': '$homeI/jadwal.png',
    // 'Beasiswa': '$homeI/beasiswa.png',
    // 'Top-Up': '$homeI/topup.png',
    // 'Protokol': '$homeI/protokol.png',
    // 'Panduan': '$homeI/bantuan.png',
    // 'Artikel': '$homeI/artikel.png',
    // 'Materi': '$homeI/gudang materi.png',
  };

  static Map<String, String> lainnya = {
    // 'Empty1': '$homeI/donasi.png',
    // 'Empty2': '$homeI/belanja.png',
    'Empty3': '$homeI/jadwal.png',
    // 'Empty4': '$homeI/beasiswa.png',
    // 'Empty5': '$homeI/topup.png',
    // 'Empty6': '$homeI/protokol.png',
    // 'Empty7': '$homeI/bantuan.png',
    // 'Empty8': '$homeI/artikel.png',
    // 'Empty9': '$homeI/gudang materi.png',
    // 'Empty10': '$homeI/gudang materi.png',
  };

  static Map<String, String> all = <String, String>{
    'Tugas': '$homeImg/tugas.png',
    'Ujian': '$homeImg/ujian.png',
    'Forum': '$homeImg/forum.png',
    'Absensi': '$homeImg/absensi.png',
    'Jadwal': '$homeImg/jadwal.png',
    'Kelasku': '$homeImg/kelasku.png',
    'Dokumen': '$homeImg/dokumen.png',
    'Konferensi': '$homeImg/konferensi.png',
    'Nilai': '$homeImg/nilai.png',
    // 'Aktifitas': '$homeI/aktifitas.png',
    // 'Pendaftaran': '$homeI/pendaftaran.png',
    // 'Alumni': '$homeI/alumni.png',
    // 'Donasi': '$homeI/donasi.png',
    // 'Belanja': '$homeI/belanja.png',
    // 'Beasiswa': '$homeI/beasiswa.png',
    // 'Top-Up': '$homeI/topup.png',
    // 'Protokol': '$homeI/protokol.png',
    // 'Panduan': '$homeI/bantuan.png',
    // 'Artikel': '$homeI/artikel.png',
    // 'Materi': '$homeI/gudang materi.png',
    'Lainnya': '$homeI/lainnya.png',
  };
}
