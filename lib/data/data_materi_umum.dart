part of '_static_data.dart';

const List<List<String>> subTopicList = [
  bahasaDanBudaya,
  kewirausahaanDanFinansial,
  persiapanKuliah,
  personalGrowth,
  sains,
  teknologiDanDesain,
  hobi,
  kecerdasan,
];

const List<String> topicList = [
  'Bahasa dan Budaya',
  'Kewirausahaan dan Finansial',
  'Persiapan Kuliah',
  'Personal Growth',
  'Sains',
  'Teknologi & Desain',
  'Hobi',
  'Kecerdasan',
];

const List<String> bahasaDanBudaya = [
  'Bahasa Arab',
  'Bahasa Inggris',
  'Bahasa Italia',
  'Bahasa Jepang',
  'Bahasa Jerman',
  'Bahasa Korea',
  'Bahasa Mandarin',
  'Bahasa Perancis',
  'Bahasa Spanyol',
  'Filsafat',
  'Sejarah Dunia'
];

const List<String> kecerdasan = [
  'Analytical Reasoning',
  'Computational Thinking',
  'Critical Thinking',
  'Emotional Intelligence',
  'EQ Test',
  'IQ Test',
  'Personality Test',
  'Problem Solving',
  'Manajemen Stres',
  'Meditasi',
];

const List<String> kewirausahaanDanFinansial = [
  'Kewirausahaan (Bisnis)',
  'Digital Marketing',
  'Investasi',
  'Kerajinan',
  'Kewirausahaan (Bisnis)',
  'Personal Finance',
];

const List<String> persiapanKuliah = [
  'TOEFL/IELTS/SAT',
  'TPA SAINTEK',
  'TPA SOSHUM',
  'TPS',
];

const List<String> personalGrowth = [
  'Growth Mindset',
  'Karir',
  'Kepemimpinan',
  'Komunikasi Efektif',
  'Manajemen Waktu',
  'Public Speaking',
  'Teamwork',
  'Komunikasi'
];

const List<String> sains = [
  'Astronomi',
  'Ekologi',
  'Elektronika',
  'Geografi',
  'Statistik',
  'Arkeologi',
  'Geologi',
  'Kedokteran',
];

const List<String> teknologiDanDesain = [
  'Data Science',
  'Design',
  'Programming',
  'Robotic',
  'Teknologi Manufaktur',
  'Teknologi Komputer',
];

const List<String> hobi = [
  'Fotografi',
  'Olahraga',
  'Otomotif',
  'Seni Musik',
  'Seni Tampil',
  'Tata Boga',
  'Tata Busana',
  'Beternak dan Berkebun',
];

const List<String> du = [];
