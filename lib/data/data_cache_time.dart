part of '_static_data.dart';

const String TIME_FORUM = 'time_forum';
const String TIME_QUIZ_UPLOAD = 'time_quiz_upload';
const String TIME_LIST_CLASS = 'time_list_class';
const String TIME_LIST_COURSE = 'time_list_course';
const String TIME_LIST_STUDENT = 'time_student_list_class';
const String TIME_LIST_CONFERENCE = 'time_conference_list';
const String TIME_EDIT_ABSENSI = 'time_edit_absensi';
const String TIME_DOCUMENTS = 'time_edit_absen';
const String TIME_TASKS = 'time_list_tugas';
const String TIME_FORUMS = 'time_list_forum';
const String TIME_ABSENSI_SISWA = 'time_absensi_schedule_siswa';
const String TIME_ABSENSI_SCHEDULE_SISWA = 'time_absensi_siswa';
const String TIME_DETAIL_CLASS = 'time_detail_class';

const Duration GLOBAL_TIMEOUT = Duration(seconds: 15);
