part of '_home.dart';

class MenuHomePage extends StatelessWidget {
  final List<String> data = ['Jadwal', 'Tugas', 'Ujian', 'Kelasku'];
  @override
  Widget build(BuildContext context) {
    return Container(
      width: S.w,
      margin: EdgeInsets.symmetric(horizontal: S.w * .03),
      child: GridView.count(
          physics: ScrollPhysics(),
          crossAxisCount: 3,
          shrinkWrap: true,
          childAspectRatio: 1.6,
          scrollDirection: Axis.vertical,
          children: data
              .map((e) => Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    margin: EdgeInsets.symmetric(
                        horizontal: S.w * .02, vertical: S.w * .02),
                    child: Container(
                        height: S.w / 3,
                        padding: EdgeInsets.symmetric(
                            horizontal: S.w * .02, vertical: S.w * .02),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Center(child: Text(e)),
                          ],
                        )),
                  ))
              .toList()),
    );
  }
}
