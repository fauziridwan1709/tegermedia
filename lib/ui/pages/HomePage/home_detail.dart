part of '_home.dart';

class HomeDetailInfo extends StatelessWidget {
  HomeDetailInfo({this.object});
  final Future<HomeModel> object;

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Lihat semua',
            style: TextStyle(color: Colors.black, fontSize: S.w / 22)),
      ),
      body: FutureBuilder<HomeModel>(
          future: object,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data.rows;
              return SingleChildScrollView(
                child: Column(
                    children: snapshot.data.rows.reversed
                        .map((data) => Column(
                              children: [
                                InkWell(
                                  onTap: () => launchInBrowser(data.link),
                                  child: Container(
                                    height: S.w / 2.9,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: S.w / 20,
                                        vertical: S.w / 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: S.w / 3,
                                          height: S.w / 3.3,
                                          decoration: BoxDecoration(
                                              borderRadius: radius(12),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(.2),
                                                    spreadRadius: 2,
                                                    blurRadius: 4,
                                                    offset: Offset(1, 1))
                                              ],
                                              image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: data.cached
                                                      ? FileImage(
                                                          File(data.gambar))
                                                      : NetworkImage(
                                                          data.gambar))),
                                        ),
                                        SizedBox(
                                          width: S.w * .05,
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              RichText(
                                                maxLines: 3,
                                                overflow: TextOverflow.ellipsis,
                                                text: TextSpan(
                                                  text: data.judul,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: S.w / 30,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    data.penerbit,
                                                    style: TextStyle(
                                                        fontSize: S.w / 35),
                                                  ),
                                                  SizedBox(height: S.w * .01),
                                                  Text(
                                                    data.tanggal,
                                                    style: TextStyle(
                                                        fontSize: S.w / 35),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Divider(
                                  color: Colors.grey[300],
                                ),
                              ],
                            ))
                        .toList()),
              );
            }
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.black),
            ));
          }),
    );
  }

  static Future<void> launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}
