import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:url_launcher/url_launcher.dart';

part 'game_web_view.dart';
part 'home_detail.dart';
part 'menu_home_page.dart';
part 'splash_page.dart';
part 'web_view.dart';
