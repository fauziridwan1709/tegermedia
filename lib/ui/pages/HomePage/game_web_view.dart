part of '_home.dart';

class GameWebView extends StatefulWidget {
  GameWebView({this.url, this.title});
  final String title;
  final String url;
  @override
  _GameWebViewState createState() => _GameWebViewState();
}

class _GameWebViewState extends State<GameWebView> {
  InAppWebViewController webView;
  double progress = 0;
  String link;

  @override
  void initState() {
    super.initState();
    link = widget.url;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
    ]);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
        return Future.value(true);
      },
      child: Scaffold(
        body: Container(
            child: Column(children: <Widget>[
          Container(
              child: progress < 1.0
                  ? LinearProgressIndicator(value: progress)
                  : Container()),
          // Expanded(
          //   child: Container(
          //     child: InAppWebView(
          //       initialUrl: link,
          //       initialOptions: InAppWebViewGroupOptions(
          //         crossPlatform: InAppWebViewOptions(
          //             cacheEnabled: true,
          //             debuggingEnabled: true,
          //             userAgent:
          //                 'Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19'),
          //       ),
          //       onWebViewCreated: (InAppWebViewController controller) {
          //         webView = controller;
          //       },
          //       onLoadStart: (InAppWebViewController controller, String url) {},
          //       onLoadStop:
          //           (InAppWebViewController controller, String url) async {},
          //       onProgressChanged:
          //           (InAppWebViewController controller, int progress) {
          //         setState(() {
          //           this.progress = progress / 100;
          //         });
          //       },
          //     ),
          //   ),
          // ),
        ])),
      ),
    );
  }
}
