part of '../_pages.dart';

class MenuList extends StatefulWidget {
  final String action;
  final List<String> favoriteMenu;

  const MenuList({this.action = 'normal', this.favoriteMenu});

  @override
  _MenuListState createState() => _MenuListState();
}

class _MenuListState extends State<MenuList> {
  final authState = Injector.getAsReactive<AuthState>();
  bool isEdit = false;
  var favoriteEditable = <String>[];

  @override
  void initState() {
    super.initState();
    favoriteEditable = widget.favoriteMenu;
    if (widget.action == 'normal') {
      if (favoriteEditable.contains('Lainnya')) favoriteEditable.removeLast();
    }
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //
    // });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          Navigator.pop(context);
          return;
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            shadowColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              'Semua Kategori',
              style: boldBlack.copyWith(
                  fontSize: S.w / 22,
                  color: Colors.white,
                  fontWeight: FontWeight.w600),
            ),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xffB5D548), Colors.green],
                  begin: Alignment.topCenter,
                  end: Alignment(0, 1.5),
                ),
              ),
            ),
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          ),
          body: ListView(
            children: [
              SizedBox(height: 30),
              if (widget.action == 'normal')
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .055, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Favorite Kamu',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: S.w / 25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      InkWell(
                          onTap: () {
                            if (isEdit) {
                              if (favoriteEditable
                                      .where((element) => element != 'Empty')
                                      .length <
                                  4) {
                                return CustomFlushBar.errorFlushBar(
                                    'Pilih minimal 4 menu favorite', context);
                              } else {
                                var newList = <String>[];
                                favoriteEditable.forEach((element) {
                                  if (element != 'Empty') {
                                    newList.add(element);
                                  }
                                });
                                authState.setState(
                                    (s) => s.setListFavorite(newList));
                                // auth.setListFavorite(newList);
                                setState(() {
                                  isEdit = !isEdit;
                                });
                              }
                            } else {
                              setState(() {
                                isEdit = !isEdit;
                              });
                            }
                          },
                          child: Material(
                              color: Colors.transparent,
                              child: Container(
                                  height: S.w * .06,
                                  width: S.w * .2,
                                  decoration: BoxDecoration(
                                    color: isEdit
                                        ? SiswamediaTheme.green
                                        : Colors.transparent,
                                    borderRadius: radius(100),
                                    border: Border.all(
                                        color: SiswamediaTheme.green, width: 1),
                                  ),
                                  child: Center(
                                      child: Text(isEdit ? 'Save' : 'Edit',
                                          style: semiBlack.copyWith(
                                              color: isEdit
                                                  ? Colors.white
                                                  : SiswamediaTheme.green,
                                              fontSize: S.w / 30)))))),
                    ],
                  ),
                ),
              if (widget.action == 'normal')
                GridView.count(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 5,
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .02),
                  childAspectRatio: .8,
                  children: favoriteEditable
                      .map((e) => e.contains('Empty')
                          ? Container(
                              height: S.w * .16,
                              width: S.w * .16,
                              child: Center(
                                  child: Column(
                                children: [
                                  Container(
                                      height: S.w * .14,
                                      width: S.w * .14,
                                      decoration: BoxDecoration(
                                        borderRadius: radius(12),
                                        border: Border.all(
                                            color: Color(0xffCFCFCF)),
                                      ),
                                      child: Center(
                                        child: Icon(Icons.add,
                                            color: SiswamediaTheme.green),
                                      )),
                                  SizedBox(height: 5),
                                  Text(e.contains('Empty') ? '' : e,
                                      style: descBlack.copyWith(
                                          fontSize: S.w / 36))
                                ],
                              )),
                            )
                          : Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () {
                                  if (isEdit) switchMenu(e);
                                },
                                child: Container(
                                  height: S.w * .16,
                                  width: S.w * .16,
                                  child: Stack(
                                    children: [
                                      Center(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Image.asset(MenuData.all[e],
                                                height: S.w * .14,
                                                width: S.w * .14),
                                            SizedBox(height: 5),
                                            Text(e,
                                                style: descBlack.copyWith(
                                                    fontSize: S.w / 36))
                                          ],
                                        ),
                                      ),
                                      if (isEdit)
                                        favoriteEditable.contains(e)
                                            ? AddRemove().remove()
                                            : AddRemove().add()
                                    ],
                                  ),
                                ),
                              ),
                            ))
                      .toList(),
                ),
              // if(widget.action == 'normal')
              Titles(title: 'Media Kelas'),
              GridView.count(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 5,
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .02),
                childAspectRatio: .8,
                children: MenuData.kelas.keys
                    .map((e) => Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              if (isEdit) switchMenu(e);
                            },
                            child: Container(
                              height: S.w * .16,
                              width: S.w * .16,
                              alignment: Alignment.center,
                              child: Stack(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(MenuData.kelas[e] ?? '',
                                          height: S.w * .14, width: S.w * .14),
                                      SizedBox(height: 5),
                                      Text(e,
                                          style: descBlack.copyWith(
                                              fontSize: S.w / 36))
                                    ],
                                  ),
                                  if (isEdit)
                                    favoriteEditable.contains(e)
                                        ? AddRemove().remove()
                                        : AddRemove().add()
                                ],
                              ),
                            ),
                          ),
                        ))
                    .toList(),
              ),
              Titles(title: 'Media Akademis'),
              Container(
                child: GridView.count(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 5,
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .02),
                  childAspectRatio: .8,
                  children: MenuData.akademis.keys
                      .map((e) => Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                switchMenu(e);
                              },
                              child: Container(
                                height: S.w * .16,
                                width: S.w * .16,
                                child: Stack(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(MenuData.akademis[e] ?? '',
                                            height: S.w * .14,
                                            width: S.w * .14),
                                        SizedBox(height: 5),
                                        Text(e,
                                            style: descBlack.copyWith(
                                                fontSize: S.w / 36))
                                      ],
                                    ),
                                    if (isEdit)
                                      favoriteEditable.contains(e)
                                          ? AddRemove().remove()
                                          : AddRemove().add()
                                  ],
                                ),
                              ),
                            ),
                          ))
                      .toList(),
                ),
              ),
              Titles(title: 'Media Pendidikan'),
              GridView.count(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 5,
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .02),
                childAspectRatio: .8,
                children: MenuData.pendidikan.keys
                    .map((e) => Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              switchMenu(e);
                            },
                            child: Container(
                              height: S.w * .16,
                              width: S.w * .16,
                              child: Stack(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(MenuData.pendidikan[e],
                                          height: S.w * .14, width: S.w * .14),
                                      SizedBox(height: 5),
                                      Text(e,
                                          style: descBlack.copyWith(
                                              fontSize: S.w / 36))
                                    ],
                                  ),
                                  if (isEdit)
                                    favoriteEditable.contains(e)
                                        ? AddRemove().remove()
                                        : AddRemove().add()
                                ],
                              ),
                            ),
                          ),
                        ))
                    .toList(),
              ),
            ],
          ),
        ));
  }

  void switchMenu(String e) {
    if (favoriteEditable.contains(e)) {
      setState(() {
        favoriteEditable.remove(e);
        favoriteEditable.add('Empty');
      });
    } else {
      if (!favoriteEditable.contains('Empty')) {
        return CustomFlushBar.errorFlushBar(
            'Maksimal memilih 9 menu favorit', context);
      }
      setState(() {
        favoriteEditable.replaceRange(favoriteEditable.indexOf('Empty'),
            favoriteEditable.indexOf('Empty') + 1, [e]);
        // favoriteEditable.remove('Empty');
      });
    }
  }
}

class AddRemove {
  // final VoidCallback onTapAdd;
  // final VoidCallback onTapRemove;
  // AddRemove({this.onTapAdd, this.onTapRemove});

  Widget remove() {
    return Positioned(
        left: 0,
        top: 0,
        child: Container(
          height: 14,
          width: 14,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.red,
          ),
          child: Icon(Icons.remove, size: 12, color: Colors.white),
        ));
  }

  Widget add() {
    return Positioned(
        left: 0,
        top: 0,
        child: Container(
          height: 14,
          width: 14,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: SiswamediaTheme.green,
          ),
          child: Icon(Icons.add, size: 12, color: Colors.white),
        ));
  }
}
