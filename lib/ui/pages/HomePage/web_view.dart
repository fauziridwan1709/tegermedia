part of '_home.dart';

class WebViewHome extends StatefulWidget {
  WebViewHome({this.url, this.title, this.authorize = false});
  final String title;
  final String url;
  final bool authorize;
  @override
  _WebViewHomeState createState() => _WebViewHomeState();
}

class _WebViewHomeState extends State<WebViewHome> {
  InAppWebViewController webView;
  double progress = 0;
  String title = '';
  String link;

  @override
  void initState() {
    super.initState();
    link = widget.url;
    title = widget.title;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
          color: SiswamediaTheme.green,
        ),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        title: RichText(
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          text: TextSpan(
              text: widget.authorize ? title : widget.title,
              style: descBlack.copyWith(fontSize: S.w / 22)),
        ),
      ),
      body: Container(
          child: Column(children: <Widget>[
        Container(child: progress < 1.0 ? LinearProgressIndicator(value: progress) : Container()),
        Expanded(
          child: Container(
            child: InAppWebView(
              initialUrlRequest: URLRequest(url: Uri.parse(link)),
              initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                    cacheEnabled: false,
                    clearCache: true,
                    userAgent:
                        'Mozilla/5.0 (Linux; Android 7.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/86.0.4240.185 Mobile Safari/535.19'),
              ),
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
              },
              onUpdateVisitedHistory: (InAppWebViewController controller, Uri uri, __) {
                Logger().d(uri.toString());
                if (uri.toString().contains('https://quiz-sekolah.nusatek.dev/students/ujian/')) {
                } else if (uri.toString().contains('https://quiz-sekolah.nusatek.dev/students/')) {
                  Navigator.pop(context);
                  CustomFlushBar.successFlushBar('Anda baru saja selesai mengerjakan kuis', context);
                }
              },
              onLoadStart: (InAppWebViewController controller, Uri url) {
                Logger().d(url);
                print(controller.getUrl());
              },
              onLoadStop: (InAppWebViewController controller, Uri url) {
                Logger().d(url);
                print(controller.getUrl());
              },
              onProgressChanged: (InAppWebViewController controller, int progress) {
                setState(() {
                  this.progress = progress / 100;
                });
              },
            ),
          ),
        ),
      ])),
    );
  }
}
