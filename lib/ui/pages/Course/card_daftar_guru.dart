part of '../_pages.dart';

class CardDaftarGuru extends StatelessWidget {
  final TeacherClassroomModel data;
  final ReactiveModel<TeacherState> teacherState;

  CardDaftarGuru({this.data, this.teacherState});

  @override
  Widget build(BuildContext context) {
    var auth = Injector.getAsReactive<AuthState>().state;
    var profile = Injector.getAsReactive<ProfileState>().state.profile;
    // return ListTile(
    //   title: Container(
    //     padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    //     decoration: SiswamediaTheme.whiteRadiusShadow,
    //     child: Row(
    //       children: [
    //         ClipRRect(
    //             borderRadius: BorderRadius.circular(2000),
    //             child: data.image.isEmpty
    //                 ? Image.asset(
    //                     'assets/profile-default.png',
    //                     height: 50,
    //                     width: 50,
    //                     fit: BoxFit.cover,
    //                   )
    //                 : Image.network(data.image,
    //                     fit: BoxFit.cover, height: 50, width: 50)),
    //         Column(
    //           children: <Widget>[
    //             CustomText(data.fullName, Kind.headingMax2Line,
    //                 align: TextAlign.left),
    //             CustomText('${data.role}', Kind.descBlack,
    //                 align: TextAlign.left),
    //           ],
    //         ),
    //       ],
    //     ),
    //   ),
    // );
    return InkWell(
      onTap: () => showDialog<void>(
          context: context,
          builder: (context) => dialogContact(context) // child
          ),
      child: Container(
          padding: EdgeInsets.all(S.w * .05),
          margin: EdgeInsets.symmetric(horizontal: 0, vertical: 6),
          decoration: SiswamediaTheme.whiteRadiusShadow,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Row(
                  children: <Widget>[
                    ClipRRect(
                        borderRadius: BorderRadius.circular(2000),
                        child: data.image.isEmpty
                            ? Image.asset(
                                'assets/profile-default.png',
                                height: 50,
                                width: 50,
                                fit: BoxFit.cover,
                              )
                            : Image.network(data.image,
                                fit: BoxFit.cover, height: 50, width: 50)),
                    SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomText(data.fullName, Kind.headingMax2Line,
                              align: TextAlign.left),
                          CustomText('${data.role}', Kind.descBlack,
                              align: TextAlign.left),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     RaisedButton(
              //         shape: RoundedRectangleBorder(borderRadius: radius(200)),
              //         onPressed: () {
              //           showDialog<void>(
              //               context: context,
              //               builder: (context) => dialogContact(context) // child
              //               );
              //         },
              //         color: SiswamediaTheme.green,
              //         child: Center(
              //           child: Text(
              //             'Kontak',
              //             style: semiWhite.copyWith(fontSize: S.w / 30),
              //           ),
              //         ))
              //   ],
              // ),
              SizedBox(height: 10),
              if (data.role != 'WALIKELAS' &&
                  (auth.selectedClassRole.isGuruOrWaliKelas ||
                      auth.currentState.isAdmin) &&
                  profile.email != data.email)
                InkWell(
                  onTap: () {
                    delete(context, data);
                  },
                  child: Icon(
                    Icons.remove_circle_outline_outlined,
                  ),
                )
            ],
          )),
    );
  }

  Widget dialogContact(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: radius(12)),
        child: Container(
          width: S.w,
          padding: EdgeInsets.all(S.w * .05),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            CustomText('Kontak', Kind.heading1),
            SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                    width: 60, child: CustomText('Telepon', Kind.descBlack)),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      decoration: BoxDecoration(
                          color: Color(0xFFECECEC),
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(color: Color(0xFFECECEC))),
                      child: Text(
                          (data.noTelp == '' ? 'Tidak ada' : data.noTelp)
                              .toString(),
                          style: TextStyle(color: SiswamediaTheme.green)),
                    )),
                    GestureDetector(
                        onTap: () {
                          if (data.noTelp != '') {
                            Navigator.pop(context);
                            ShareService.share(context, data.noTelp, 'Tindakan');
                          } else {
                            CustomFlushBar.errorFlushBar(
                              'Tidak ada nomor telepon',
                              context,
                            );
                          }
                        },
                        child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 5),
                            decoration: BoxDecoration(
                                color: SiswamediaTheme.green,
                                borderRadius: BorderRadius.circular(3)),
                            child: Icon(
                              Icons.share,
                              color: Colors.white,
                              size: S.w * .05,
                            ))),
                  ],
                ),
              ],
            ),
            SizedBox(height: 5),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(width: 60, child: CustomText('Email', Kind.descBlack)),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      decoration: BoxDecoration(
                          color: Color(0xFFECECEC),
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(color: Color(0xFFECECEC))),
                      child: Text(data.email ?? '',
                          style: TextStyle(color: SiswamediaTheme.green)),
                    )),
                    GestureDetector(
                        onTap: () {
                          if (data.email != '') {
                            Navigator.pop(context);
                            ShareService.share(context, data.email, 'Tindakan');
                          } else {
                            CustomFlushBar.errorFlushBar(
                              'Tidak ada email',
                              context,
                            );
                          }
                        },
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(3)),
                          child: Icon(Icons.share,
                              color: Colors.white, size: S.w * .05),
                        )),
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Card(
                    elevation: 4,
                    shadowColor: Colors.grey.withOpacity(.2),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    child: InkWell(
                      onTap: () async {
                        if (data.email != '') {
                          var email =
                              'mailto:${data.email}?subject=News&body=Hai%20Siswamedia';
                          if (await canLaunch(email)) {
                            await launch(email);
                          } else {
                            throw 'Could not launch $email';
                          }
                        } else {
                          CustomFlushBar.errorFlushBar(
                            'Tidak ada email',
                            context,
                          );
                        }
                      },
                      child: Container(
                        height: 50,
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: Center(
                          child: Row(
                            children: [
                              Icon(Icons.email),
                              SizedBox(width: 5),
                              CustomText('Kirim Email', Kind.descBlack),
                            ],
                          ),
                        ),
                      ),
                    )),
                Card(
                  elevation: 4,
                  shadowColor: Colors.grey.withOpacity(.2),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6)),
                  child: InkWell(
                    onTap: () async {
                      if (data.noTelp != '') {
                        var telp = 'tel:${data.noTelp}';
                        if (await canLaunch(telp)) {
                          await launch(telp);
                        } else {
                          throw 'Could not launch $telp';
                        }
                      } else {
                        CustomFlushBar.errorFlushBar(
                          'Tidak ada no telepon',
                          context,
                        );
                      }
                    },
                    child: Container(
                      height: 50,
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Center(
                        child: Row(
                          children: [
                            Icon(Icons.phone),
                            SizedBox(width: 5),
                            CustomText('Telepon', Kind.descBlack),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            )),
          ]),
        ));
  }

  void delete(BuildContext context, TeacherClassroomModel data) {
    showDialog<void>(
        context: context,
        builder: (context2) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
              width: S.w,
              padding: EdgeInsets.all(S.w * .05),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text('Keluarkan member ini?',
                    style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                SizedBox(height: 15),
                Text('Apakah Anda yakin untuk mengeluarkan member ini ?',
                    textAlign: TextAlign.center,
                    style: descBlack.copyWith(fontSize: S.w / 34)),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .01),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: S.w / 10,
                          width: S.w / 3.5,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: SiswamediaTheme.green,
                          ),
                          child: InkWell(
                            onTap: () async {
                              // ignore: unawaited_futures
                              showDialog<void>(
                                  context: context,
                                  builder: (_) => Center(
                                      child: CircularProgressIndicator()));
                              await ClassServices.kickMemberClass(
                                      classId: data.classId,
                                      userId: data.userId)
                                  .then((value) async {
                                if (value.statusCode == StatusCodeConst.success ||
                                    value.statusCode == 201) {
                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                  await GlobalState.detailClass().setState((s) =>
                                      s.updateCountTeacher(data.classId));
                                  await teacherState.state
                                      .retrieveData(data.classId);
                                  // setState(
                                  //     (s) => s.retrieveData(data.classId));
                                  await teacherState.notify();
                                  CustomFlushBar.successFlushBar(
                                    'Berhasil mengeluarkan member',
                                    context,
                                  );
                                } else {
                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                  CustomFlushBar.errorFlushBar(
                                    'Terjadi kesalahan ketika mengeluarkan member',
                                    context,
                                  );
                                }
                              });

                              //Navigator.push(context,
                              //  MaterialPageRoute<void>(builder: (_) => null));
                            },
                            child: Center(
                              child: Text('Ya',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: SiswamediaTheme.lightBlack,
                          ),
                          height: S.w / 10,
                          width: S.w / 3.5,
                          child: InkWell(
                            onTap: () => Navigator.pop(context),
                            child: Center(
                              child: Text('Tidak',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                      ]),
                )
              ]),
            )));
  }
}
