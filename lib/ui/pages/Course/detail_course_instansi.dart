part of '../_pages.dart';

class DetailCourseInstansi extends StatefulWidget {
  final int id;
  final int classId;
  DetailCourseInstansi({@required this.id, this.classId});
  @override
  _DetailCourseInstansiState createState() => _DetailCourseInstansiState();
}

class _DetailCourseInstansiState extends State<DetailCourseInstansi> {
  final authState = Injector.getAsReactive<AuthState>();
  final teacherState = Injector.getAsReactive<TeacherState>();
  ModelDetailClassId data;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      //todo penting
      // if (classProvider.teachers == null) {
      //   await classProvider.getTeachersInClass(widget.classId);
      // }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Detail Mata Pelajaran'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: FutureBuilder<ModelDetailCourse>(
            future: ClassServices.detailCourse(id: widget.id),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Padding(
                  padding: EdgeInsets.only(top: S.w / 3),
                  child: Center(child: CircularProgressIndicator()),
                );
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data.statusCode == 200) {
                  var data = snapshot.data.data;
                  return DetailMataPelajaran(data: data);
                } else {
                  return Container(
                      child: Center(child: Text('Terjadi Kesalahan...')));
                }
              } else {
                return Container(child: Text('Terjadi Kesalahan...'));
              }
            }));
  }
}

class DetailMataPelajaran extends StatelessWidget {
  final DetailCourse data;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const DetailMataPelajaran({this.data, this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    var teacherState = Injector.getAsReactive<TeacherState>();
    var realData = teacherState.state.teacherModel.data.where((element) => data
        .listGuru
        .where((element2) => element2.userId == element.userId)
        .isNotEmpty);
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 24),
          child: Column(
            children: [
              Text(someCapitalizedString(data.name),
                  textAlign: TextAlign.center,
                  style: boldBlack.copyWith(fontSize: S.w / 22)),
              SizedBox(height: 10),
              CustomText(
                  '${data.description == '' ? 'Tidak ada deskripsi' : data.description}',
                  Kind.descBlack,
                  align: TextAlign.center),
              SizedBox(height: 10),
              CustomText('Kontak Guru', Kind.heading1, align: TextAlign.left),
              SizedBox(height: 10),
              if (realData.length == 1)
                SizedBox(
                  width: S.w * .5,
                  child: AspectRatio(
                      aspectRatio: .7,
                      child: CardDaftarGuru(data: realData.single)),
                ),
              if (realData.length > 1)
                GridView.count(
                  padding: EdgeInsets.all(10),
                  shrinkWrap: true,
                  crossAxisCount: 2,
                  childAspectRatio: .7,
                  children:
                      realData.map((e) => CardDaftarGuru(data: e)).toList(),
                  // children: data.,
                ),
              SizedBox(height: 16),
              Divider(),
            ],
          ),
        ),
      ],
    );
  }
}
