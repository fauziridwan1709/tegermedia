part of '../_pages.dart';

class PersonalSchoolList extends StatelessWidget {
  final List<DataSchoolModel> mySchool;

  PersonalSchoolList({this.mySchool});

  @override
  Widget build(BuildContext context) {
    final authState = Injector.getAsReactive<AuthState>();
    final courseState = Injector.getAsReactive<CourseState>();

    var auth = authState.state;
    var meCourse = courseState.state.courseModel.data;
    return Column(
      children: [
        Container(
            margin: EdgeInsets.symmetric(
              horizontal: S.w * .05,
            ),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(height: 10),
              Text('Personal Media Merdeka Belajar (Gratis)',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
              SizedBox(height: 16),
              Container(
                width: S.w,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(
                        meCourse.length,
                        (index) => InkWell(
                            onTap: () async {
                              await authState.setState((s) =>
                                  s.setNamaSekolah(meCourse[index].name));
                              var currentState = UserCurrentState(
                                  schoolId: meCourse[index].id,
                                  schoolName: meCourse[index].name,
                                  schoolRole: [meCourse[index].role],
                                  type: 'PERSONAL',
                                  className: null,
                                  classId: null,
                                  classRole: null,
                                  isAdmin: meCourse[index].role == 'GURU');
                              await authState.setState((s) =>
                                  s.changeCurrentState(state: currentState));
                              await auth.setCurrentClass(
                                  className: null,
                                  classId: null,
                                  classRole: null);

                              var result = await ClassServices.setCurrentState(
                                  data: json.encode(currentState.toJson()));
                              print(result.statusCode);
                              if (result.statusCode != 200) {
                                CustomFlushBar.errorFlushBar(
                                  result.message.toString() + '!',
                                  context,
                                );
                              }
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 10),
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(
                                  horizontal: S.w * .05, vertical: 13),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: SiswamediaTheme.grey
                                            .withOpacity(0.2),
                                        offset: const Offset(0.2, 0.2),
                                        blurRadius: 3.0),
                                  ],
                                  color: auth.currentState.schoolId ==
                                          meCourse[index].id
                                      ? SiswamediaTheme.green
                                      : Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  border:
                                      Border.all(color: Color(0xFF00000029))),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(meCourse[index].name,
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: auth.currentState.schoolId ==
                                                    meCourse[index].id
                                                ? Colors.white
                                                : Colors.black)),
                                    Text('Sebagai : ${meCourse[index].role}',
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: auth.currentState.schoolId ==
                                                    meCourse[index].id
                                                ? Colors.white
                                                : Colors.black)),
                                  ]),
                            ))).toList()),
              )
            ])),
        SizedBox(height: 5),
        InkWell(
          onTap: () {
            Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => SelectEntitasPersonal(),
              ),
            );
          },
          child: Container(
            width: S.w * .9,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xFFD9D9D9)),
                borderRadius: BorderRadius.circular(10)),
            child: Icon(Icons.add_circle, size: 35, color: Color(0xFF4b4b4b)),
          ),
        ),
        SizedBox(
          height: 25,
        )
      ],
    );
  }
}
