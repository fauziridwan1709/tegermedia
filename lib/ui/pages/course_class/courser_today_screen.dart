part of '_course.dart';

class CourseTodayScreen extends StatefulWidget {
  @override
  _CourseTodayScreenState createState() => _CourseTodayScreenState();
}

class _CourseTodayScreenState extends State<CourseTodayScreen> {
  final List<Widget> myTabs = [
    Tab(
      child: Column(
        children: [
          Icon(Icons.mode_comment_outlined, color: SiswamediaTheme.primary),
          vText('Komentar', color: SiswamediaTheme.primary)
        ],
      ),
    ),
    Tab(
      child: Column(
        children: [
          Icon(Icons.attach_file_outlined, color: SiswamediaTheme.primary),
          vText('Lampiran', color: SiswamediaTheme.primary)
        ],
      ),
    ),
    Tab(
      child: Column(
        children: [
          Icon(Icons.assignment_outlined, color: SiswamediaTheme.primary),
          vText('Quiz', color: SiswamediaTheme.primary)
        ],
      ),
    ),
    Tab(
      child: Column(
        children: [
          Icon(Icons.article_outlined, color: SiswamediaTheme.primary),
          vText('Tugas', color: SiswamediaTheme.primary)
        ],
      ),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          dataCourse(statusDate: 'Today'),
          SizedBox(height: 20),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: SiswamediaTheme.lightGreen),
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                vText('Petunjuk Belajar',
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
                SizedBox(height: 10),
                vText(
                    'Mohon mendengarkan penjelasan dalam Video lampiran yang saya buat , Jika ada yang belum jelas silahkan chat disini dan jika sudah jelas silahkan kerjakan QUIZ pada course ini',
                    color: SiswamediaTheme.textGrey,
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Expanded(
              child: DefaultTabController(
                  length: myTabs.length,
                  initialIndex: 0,
                  child: Column(
                    children: [
                      TabBar(
                        isScrollable: false,
                        tabs: myTabs,
                      ),
                      Expanded(
                          child: TabBarView(
                        children: [
                          comment(),
                          lampiran(),
                          quiz(),
                          quiz(),
                        ],
                      ))
                    ],
                  )))
        ],
      ),
    );
  }
}
