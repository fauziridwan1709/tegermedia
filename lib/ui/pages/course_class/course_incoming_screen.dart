part of '_course.dart';

class CourseIncomingScreen extends StatefulWidget {
  @override
  _CourseIncomingScreenState createState() => _CourseIncomingScreenState();
}

class _CourseIncomingScreenState extends State<CourseIncomingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          dataCourse(statusDate: "Incoming")
        ],
      ),
    );
  }
}
