part of '_course.dart';

class CourseClassScreen extends StatefulWidget {
  @override
  _CourseClassScreenState createState() => _CourseClassScreenState();
}

class _CourseClassScreenState extends State<CourseClassScreen>
    with SingleTickerProviderStateMixin {
  TabController controller;
  @override
  void initState() {
    controller = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: vText(
          'Jurnal Belajar',
        ),
        bottom: TabBar(controller: controller, tabs: <Tab>[
          Tab(
            child: vText('Today', color: SiswamediaTheme.primary),
          ),
          Tab(
            child: vText('Incoming', color: SiswamediaTheme.primary),
          ),
        ]),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_outlined,
            color: SiswamediaTheme.primary,
          ),
        ),
      ),
      body: TabBarView(controller: controller, children: <Widget>[
        Container(child: CourseTodayScreen()),
        Container(child: CourseIncomingScreen()),
      ]),
    );
  }
}
