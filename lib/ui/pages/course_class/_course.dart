import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:tegarmedia/utils/v_widget.dart';

part 'course_class_screen.dart';
part 'course_incoming_screen.dart';
part 'courser_today_screen.dart';
