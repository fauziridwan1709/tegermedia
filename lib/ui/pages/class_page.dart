part of '_pages.dart';

class ClassPage extends StatefulWidget {
  @override
  _ClassPageState createState() => _ClassPageState();
}

class _ClassPageState extends State<ClassPage> with TickerProviderStateMixin {
  final authState = Injector.getAsReactive<AuthState>();
  final ScrollController scrollController = ScrollController();
  bool isScrollingDown = false;

  UserProfile profile = UserProfile(data: null);
  static const snackBarDuration = Duration(seconds: 3);
  final keyPilihKelas = GlobalKey();
  SharedPreferences prefs;

  DateTime backButtonPressTime;

  @override
  void initState() {
    super.initState();
    // authState.setState((s) => s.initializeState());
    // getClass(context);
  }

  @override
  void dispose() {
    scrollController.removeListener(() {});
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
            child: StateBuilder<AuthState>(
          observe: () => authState,
          builder: (context, snapshot) {
            if (authState.state.isLogin != null && authState.state.isLogin) {
              return sectionAlreadyLogin(context: context);
            } else if (authState.state.isLogin != null && !authState.state.isLogin) {
              return sectionNotLogin(
                  context: context, login: () => GlobalState.auth().state.login(context));
            }
            return Center(child: CircularProgressIndicator());
          },
        )));
  }

  Future<bool> onWillPop() async {
    var currentTime = DateTime.now();
    var backButtonHasNotBeenPressedOrSnackBarHasBeenClosed = backButtonPressTime == null ||
        currentTime.difference(backButtonPressTime) > snackBarDuration;

    if (backButtonHasNotBeenPressedOrSnackBarHasBeenClosed) {
      backButtonPressTime = currentTime;
      CustomFlushBar.eventFlushBar('Press back again to leave', context);
      return false;
    }
    return true;
  }

  Widget sectionAlreadyLogin({BuildContext context}) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        body: WillPopScope(
      onWillPop: onWillPop,
      child: sectionClassHome(context),
    ));
  }

  Widget sectionClassHome(BuildContext context) {
    var ortuMenu = <Map<String, String>>[
      {
        'name': 'Kelasku',
        'icon': 'assets/kelas/kelasku.png',
      },
      {
        'name': 'Forum',
        'icon': 'assets/images/forum.png',
      },
      {
        'name': 'Jadwal',
        'icon': 'assets/kelas/Jadwal.png',
      },
      {
        'name': 'Nilai',
        'icon': 'assets/icons/dokumen/empty.png',
      },
      {
        'name': 'Tugas',
        'icon': 'assets/icons/kelas/Tugas.png',
      },
      {
        'name': 'Ujian',
        'icon': 'assets/kelas/kuis.png',
      },
      {
        'name': 'Konferensi',
        'icon': 'assets/kelas/konferensi.png',
      },
      {
        'name': 'Dokumen',
        'icon': 'assets/kelas/dokumen.png',
      },
      {
        'name': 'Absen',
        'icon': 'assets/kelas/absensi.png',
      },
      {
        'name': 'Interaksi',
        'icon': 'assets/kelas/interaksi_belajar.png',
      },
      // {
      //   'name': 'Jurnal Belajar',
      //   'icon': 'assets/icons/course/course.png',
      // }
    ];

    var dataMenu = <Map<String, String>>[
      {
        'name': 'Kelasku',
        'icon': 'assets/images/asset_kelas/kelasku.png',
      },
      {
        'name': 'Forum',
        'icon': 'assets/images/asset_kelas/tugas.png',
      },
      {
        'name': 'Jadwal',
        'icon': 'assets/images/asset_kelas/jadwal.png',
      },
      {
        'name': 'Nilai',
        'icon': 'assets/images/asset_kelas/tugas.png',
      },
      {
        'name': 'Tugas',
        'icon': 'assets/images/asset_kelas/tugas.png',
      },
      {
        'name': 'Ujian',
        'icon': 'assets/images/asset_kelas/ujian.png',
      },
      {
        'name': 'Konferensi',
        'icon': 'assets/images/asset_kelas/konferensi.png',
      },
      {
        'name': 'Dokumen',
        'icon': 'assets/images/asset_kelas/tugas.png',
      },
      {
        'name': 'Absen',
        'icon': 'assets/images/asset_kelas/absensi.png',
      },
      {
        'name': 'Interaksi',
        'icon': 'assets/images/asset_kelas/interaksi.png',
      },
      {
        'name': 'Jurnal Belajar',
        'icon': 'assets/images/asset_kelas/interaksi.png',
      }
    ];

    var menuPersonal = <Map<String, String>>[
      {
        'name': 'Kelasku',
        'icon': 'assets/images/asset_kelas/kelasku.png',
      },
      {
        'name': 'Jadwal',
        'icon': 'assets/kelas/Jadwal.png',
      },
      {
        'name': 'Tugas',
        'icon': 'assets/icons/kelas/Tugas.png',
      },
      {
        'name': 'Konferensi',
        'icon': 'assets/kelas/konferensi.png',
      },
    ];

    var auth = authState.state;
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        SliverAppBar(
          toolbarHeight: AppBar().preferredSize.height * 1.5,
          floating: true,
          titleSpacing: 0,
          shadowColor: Colors.transparent,
          title: AppBarUI(
            onLogin: () async {
              await GlobalState.auth().state.login(context);
            },
            onTapNotif: () => dialogDev(context),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            LayoutBuilder(
              builder: (context, _) {
                if (auth.currentState.schoolId != -1 && auth.currentState.type == 'SEKOLAH') {
                  print('school home');
                  return DetailSchoolHome(
                    keyPilihKelas: keyPilihKelas,
                  );
                } else if (auth.currentState.schoolId != null &&
                    auth.currentState.type == 'PERSONAL') {
                  return DetailSchoolHome(
                    keyPilihKelas: keyPilihKelas,
                  );
                  // return SizedBox();
                  // return detailCoursePersonal(
                  //     context, auth.currentState.schoolId);
                } else {
                  return Column(children: [
                    Container(
                        height: 170,
                        width: 215,
                        margin: EdgeInsets.only(bottom: 8, top: 20),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/icons/kelas/book_lover.png')))),
                    Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Text('Anda belum memilih/mempunyai sekolah')),
                    ButtonDetail(),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
                      child: Divider(),
                    )
                  ]);
                }
              },
            ),
            Container(
              width: S.w,
              margin: EdgeInsets.symmetric(horizontal: S.w * .03),
              child: LayoutBuilder(builder: (context, constrains) {
                var orientation = MediaQuery.of(context).orientation;
                var isPortrait = orientation == Orientation.portrait;

                return GridView.count(
                  physics: ScrollPhysics(),
                  crossAxisCount: isPortrait ? 2 : 4,
                  shrinkWrap: true,
                  childAspectRatio: .95,
                  scrollDirection: Axis.vertical,
                  children: (auth.currentState.type == 'PERSONAL'
                          ? menuPersonal
                          : auth.currentState.classRole == 'ORTU'
                              ? ortuMenu
                              : dataMenu)
                      .map((e) => CardMenu(
                          scrollController: scrollController,
                          context: context,
                          data: e,
                          keyPilihKelas: keyPilihKelas,
                          onTap: (data) {
                            vUtils.setLog(data['name']);
                            if (data['name'] == 'Kelasku') {
                              vUtils.setLog('Kelasku');
                              if (auth.currentState.schoolId != null &&
                                  auth.currentState.type == 'PERSONAL') {
                                context.push<void>(ListClassOnlyme(
                                  id: auth.currentState.schoolId,
                                  stateRouteRegister: true,
                                  schoolRole: auth.currentState.schoolRole,
                                ));
                              } else if (auth.currentState.schoolId != null &&
                                  auth.currentState.type == 'SEKOLAH') {
                                context.push<void>(ListClassOnlyme(
                                  id: auth.currentState.schoolId,
                                  stateRouteRegister: true,
                                  schoolRole: auth.currentState.schoolRole,
                                ));
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (auth.currentState.classId == null) {
                              vUtils.setLog('SEKOLAH');
                              showDialog<void>(
                                context: context,
                                barrierDismissible: true,
                                builder: (context2) => WillPopScope(
                                  onWillPop: () async => true,
                                  child: CustomDialog(
                                    title: 'Oops!',
                                    description: 'Anda Belum Memilih/Mempunyai Kelas',
                                    image: 'assets/dialog/pilih_kelas.png',
                                    onTap: () {
                                      Navigator.pop(context2);
                                      scrollController
                                          .animateTo(0.0,
                                              duration: Duration(milliseconds: 600),
                                              curve: Curves.easeIn)
                                          .then((_) async {
                                        await Future<void>.delayed(Duration(milliseconds: 100))
                                            .then((value) async {
                                          final RenderBox renderBox =
                                              keyPilihKelas.currentContext.findRenderObject();
                                          final offset = renderBox.localToGlobal(Offset.zero);
                                          await Navigator.push<void>(
                                              context, TutorialOverlay(offset: offset));
                                        });
                                      });
                                    },
                                    actions: ['Tunjukkan'],
                                  ),
                                ),
                              );
                            } else if (data['name'] == 'Ujian') {
                              if (auth.currentState.schoolId != null) {
                                if (auth.currentState.type == 'PERSONAL') {
                                  return dialogDev(context,
                                      title: 'Konfirmasi',
                                      desc: 'Untuk kelas personal pada fitur ini belum diaktifkan');
                                } else {
                                  navigate(context, QuizClass(),
                                      settings: RouteSettings(name: '/quiz_home'));
                                }
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Forum') {
                              if (auth.currentState.schoolId != null &&
                                  auth.currentState.type == 'SEKOLAH') {
                                Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) => ChooseForum(),
                                  ),
                                );
                              } else if (auth.currentState.type == 'PERSONAL') {
                                dialogDev(context,
                                    title: 'Kelas Personal',
                                    desc: 'Untuk kelas personal pada fitur ini belum diaktifkan');
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Dokumen') {
                              if (auth.currentState.schoolId != null) {
                                if (auth.currentState.type == 'PERSONAL') {
                                  return dialogDev(context,
                                      title: 'Kelas Personal',
                                      desc: 'Untuk kelas personal pada fitur ini belum diaktifkan');
                                } else {
                                  Navigator.push<dynamic>(
                                    context,
                                    MaterialPageRoute<dynamic>(
                                      builder: (BuildContext context) => MainDocument(),
                                    ),
                                  );
                                }
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Tugas') {
                              if (auth.currentState.type == 'SEKOLAH' &&
                                      auth.currentState.schoolId != null &&
                                      auth.currentState.isAdmin
                                  ? true
                                  : auth.currentState.classRole != null && auth.currentState.isAdmin
                                      ? true
                                      : auth.currentState.classRole.isNotEmpty) {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                        builder: (context) =>
                                            MainAssignment(role: auth.currentState.classRole)));
                              } else if (auth.currentState.type == 'PERSONAL' &&
                                  auth.currentState.schoolId != null) {
                                print(auth.currentState.schoolRole.first);
                                navigate(
                                    context, MainAssignment(role: auth.currentState.classRole));
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Jadwal') {
                              if (auth.currentState.schoolId != null) {
                                navigateWithName(
                                    context,
                                    JadwalKelas(classId: auth.currentState.classId),
                                    '/jadwal-kelas');
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Konferensi') {
                              if (auth.currentState.schoolId != null) {
                                Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) => Conference(),
                                  ),
                                );
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Absen') {
                              if (auth.currentState.schoolId != null &&
                                  auth.currentState.type == 'SEKOLAH') {
                                Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) => Absensi(),
                                  ),
                                );
                              } else if (auth.currentState.type == 'PERSONAL') {
                                dialogDev(context,
                                    title: 'Kelas Personal',
                                    desc:
                                        'Mohon maaf kelas personal tidak dapat mengakses fitur ini');
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Nilai') {
                              if (auth.currentState.schoolId != null) {
                                if (auth.currentState.type == 'PERSONAL') {
                                  return dialogDev(context,
                                      title: 'Kelas Personal',
                                      desc:
                                          'Mohon maaf kelas personal tidak dapat mengakses fitur ini');
                                }
                                Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) => Score(),
                                  ),
                                );
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Interaksi') {
                              if (auth.currentState.schoolId != null) {
                                context.push<void>(InteractionStudyScreen());
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (data['name'] == 'Jurnal Belajar') {
                              if (auth.currentState.schoolId != null) {
                                context.push<void>(StudyJournalPage());
                              } else {
                                dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                              }
                            } else if (auth.currentState.schoolId == null) {
                              vUtils.setLog('schoolId');
                              return dialogDev(context,
                                  title: 'Konfirmasi', desc: 'Harap Daftar atau Join Sekolah');
                            } else if (auth.currentState.schoolRole.isEmpty) {
                              vUtils.setLog('Kelasku');
                              return dialogDev(context,
                                  title: 'Konfirmasi', desc: 'Belum mengassign role');
                            }
                          }))
                      .toList(),
                );
              }),
            ),
            SizedBox(height: 25)
          ]),
        )
      ],
    );
  }

  Widget detailCoursePersonal(BuildContext context, int id) {
    return FutureBuilder<ModelDetailCourse>(
        future: ClassServices.detailCourse(id: id),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Padding(
              padding: EdgeInsets.only(top: S.w / 3),
              child: Center(child: CircularProgressIndicator()),
            );
          } else {
            var data = snapshot.data.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, top: 24, bottom: 8),
                  child: Column(
                    children: [
                      Text(
                        data.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                ButtonDetail(),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
                  child: Divider(),
                )
              ],
            );
          }
        });
  }
}

Widget sectionNotLogin({BuildContext context, VoidCallback login}) {
  var orientation = MediaQuery.of(context).orientation;
  var isPortrait = orientation == Orientation.portrait;
  return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            child: Image.asset(
              'assets/dev.png',
              alignment: Alignment.bottomCenter,
              height: isPortrait ? S.w * .6 : S.w * .5,
              width: isPortrait ? S.w : S.w * .9,
              fit: BoxFit.contain,
            ),
          ),
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                    top: isPortrait ? S.h / 4 - 20 : S.w * .3,
                    left: isPortrait ? 40 : S.w * .5,
                    right: isPortrait ? 40 : S.w * .5),
                child: Text(
                    'Oopss! Kamu tidak bisa mengakses halaman ini, Kamu harus terdaftar sebagai pengguna Siswamedia untuk melanjutkan.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: S.w / 30, fontWeight: FontWeight.w700, color: Colors.black87)),
              ),
              SizedBox(height: 15),
              SizedBox(
                width: S.w * .6,
                child: AspectRatio(
                  aspectRatio: 6,
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(S.w),
                        // color: Color(0xff3971A9),
                        color: SiswamediaTheme.green,
                      ),
                      child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                              onTap: login,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                child: Row(children: [
                                  Container(
                                    width: S.w / 18,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white,
                                        image: DecorationImage(
                                            image: AssetImage('assets/google.png'))),
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    'Masuk/Daftar Dengan Google',
                                    style: descWhite.copyWith(fontSize: S.w / 32),
                                  ),
                                ]),
                              )))),
                ),
              )
            ],
          ),
        ],
      ));
}
