part of '_interaksi.dart';

class ChooseClassScreen extends StatefulWidget {
  @override
  _ChooseClassScreenState createState() => _ChooseClassScreenState();
}

class _ChooseClassScreenState extends State<ChooseClassScreen> {
  final multiSelectClassState = GlobalState.multiSelectClass();
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;
  TextEditingController _searchController;
  bool _visible;

  // var selected = <DataMultiSelectClass>[];

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    _searchController = TextEditingController();
    _visible = true;
    Initializer(
      reactiveModel: multiSelectClassState,
      rIndicator: _refreshIndicatorKey,
      state: multiSelectClassState.state.listData != null,
      cacheKey: 'time_select_class',
    )..initialize();
  }

  Future<void> retrieveData() async {
    if (multiSelectClassState.isWaiting) {
      setState(() => _visible = false);
    }

    await multiSelectClassState.setState((s) => s.retrieveData(),
        onData: (context, data) => setState(
              () => _visible = false,
            ),
        onRebuildState: (_) => setState(() => _visible = true));
  }

  TextEditingController searchText;
  @override
  Widget build(BuildContext context) {
    var selected = multiSelectClassState.state.selected;
    return Scaffold(
      appBar: AppBar(
        title: vText(
          "Pilih Kelas",
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.close,
            color: SiswamediaTheme.primary,
          ),
        ),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                  color: SiswamediaTheme.white,
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .03),
                  child: TextField(
                    controller: _searchController,
                    onChanged: (val) {
                      multiSelectClassState.state.search(val);
                      multiSelectClassState.notify();
                    },
                    decoration: InputDecoration(
                        hintText: 'Masukkan pasangan kelas dan pelajaran',
                        hintStyle: hintText(context),
                        contentPadding: EdgeInsets.symmetric(horizontal: 24),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(width: 1.5)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 1.5, color: SiswamediaTheme.border))),
                  )),
              Expanded(
                child: StateBuilder<MultiSelectClassState>(
                    observe: () => multiSelectClassState,
                    builder: (context, _) {
                      return RefreshIndicator(
                        key: _refreshIndicatorKey,
                        onRefresh: retrieveData,
                        child: WhenRebuilder<MultiSelectClassState>(
                            observe: () => multiSelectClassState,
                            onIdle: () => WaitingView(),
                            onWaiting: () => WaitingView(),
                            onError: (dynamic error) => ErrorView(error: error),
                            onData: (data) {
                              var filteredData = data.listData
                                  .where((element) =>
                                      ('${element.className} - ${element.courseName}')
                                          .toLowerCase()
                                          .contains(multiSelectClassState
                                              .state.searchKey
                                              .toLowerCase()))
                                  .toList();
                              if (data.listData.isEmpty) {
                                return Container();
                              }
                              return AnimatedOpacity(
                                opacity: _visible ? 1.0 : 0.0,
                                duration: Duration(milliseconds: 300),
                                child: ListView.separated(
                                  padding: EdgeInsets.only(bottom: 100),
                                  itemCount: filteredData.length,
                                  itemBuilder: (context, index) {
                                    var item = filteredData[index];
                                    return ListTile(
                                      onTap: () {
                                        print(
                                            '${item.classId}-${item.courseId}');
                                        multiSelectClassState.state
                                            .switchSelected(item);
                                        multiSelectClassState.notify();
                                      },
                                      visualDensity: VisualDensity(
                                          horizontal: 0,
                                          vertical:
                                              VisualDensity.minimumDensity),
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 0, horizontal: S.w * .05),
                                      title: Text(
                                        '${item.className} - ${item.courseName}',
                                        style: mediumBlack.copyWith(
                                            fontSize: S.w / 29,
                                            color: (selected.containsKey(
                                                        item.classId) &&
                                                    selected[item.classId]
                                                            .courseId !=
                                                        item.courseId)
                                                ? Colors.black38
                                                : Colors.black87),
                                      ),
                                      trailing: (selected
                                                  .containsKey(item.classId) &&
                                              selected[item.classId].courseId ==
                                                  item.courseId)
                                          ? Icon(Icons.check,
                                              color: SiswamediaTheme.green)
                                          : SizedBox(),
                                    );
                                  },
                                  separatorBuilder:
                                      (BuildContext context, int index) {
                                    return Divider();
                                  },
                                ),
                              );
                            }),
                      );
                    }),
              )
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: StateBuilder<MultiSelectClassState>(
              observe: () => multiSelectClassState,
              builder: (context, data) {
                return Container(
                  height: S.w * .3,
                  padding: EdgeInsets.symmetric(
                      vertical: S.w * .05, horizontal: S.w * .05),
                  color: SiswamediaTheme.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomText(
                        'Kelas Terpilih: ${selected.length}',
                        Kind.heading3,
                        align: TextAlign.left,
                      ),
                      SizedBox(height: 5),
                      Container(
                        height: 45,
                        decoration: BoxDecoration(
                            borderRadius: radius(8),
                            color: SiswamediaTheme.green,
                            boxShadow: [SiswamediaTheme.shadowContainer]),
                        child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                                onTap: () async => Navigator.pop(context),
                                child: Center(
                                    child:
                                        CustomText('Selesai', Kind.heading4)))),
                      )
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _itemClass(int i) {
    return Container(
        margin: EdgeInsets.only(right: 16, left: 16),
        child: InkWell(
          onTap: () {},
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: vText("Kelas 10 - 1", fontSize: 17),
                  ),
                  Icon(Icons.check, color: SiswamediaTheme.primary)
                ],
              ),
              SizedBox(height: 10),
              Divider(
                color: SiswamediaTheme.border,
                height: 1,
              )
            ],
          ),
        ));
  }
}
