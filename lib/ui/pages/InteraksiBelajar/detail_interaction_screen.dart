part of '_interaksi.dart';

class DetailInteractionScreen extends StatefulWidget {
  final String url;
  final String title;
  final String startDate;
  final String id;
  final String room_id;
  final bool isRiwayat;

  const DetailInteractionScreen(
    this.id, {
    Key key,
    this.url,
    this.title,
    this.startDate,
    this.isRiwayat, this.room_id,
  }) : super(key: key);

  @override
  _DetailInteractionScreenState createState() =>
      _DetailInteractionScreenState();
}

class _DetailInteractionScreenState extends State<DetailInteractionScreen> {
  final interactionState = GlobalState.interaction().state;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: SiswamediaTheme.primary,
          ),
        ),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 1,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            color: Colors.white,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 22, bottom: 5),
                        child: vText(widget.title,
                            color: SiswamediaTheme.textGrey[500],
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.calendar_today_outlined,
                            size: 14,
                            color: SiswamediaTheme.textGrey[200],
                          ),
                          Expanded(
                            child: vText(
                                formattedDate(widget.startDate).toString(),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: SiswamediaTheme.textGrey[200]),
                          ),
                          InkWell(
                            onTap: () async {
                              if (!widget.isRiwayat) {
                                await interactionState.joinParticipant(widget.room_id);
                                Navigator.push<dynamic>(
                                    context,
                                    MaterialPageRoute<dynamic>(
                                        builder: (BuildContext context) =>
                                            WhiteboardScreen(
                                              title: widget.title,
                                              url: widget.url,
                                              id: widget.id,
                                              room_id: widget.room_id
                                            )));
                              }
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: widget.isRiwayat
                                      ? SiswamediaTheme.borderGrey
                                      : SiswamediaTheme.primary),
                              padding: EdgeInsets.symmetric(
                                  vertical: 6, horizontal: 11),
                              child: vText(
                                  widget.isRiwayat
                                      ? 'Selesai'
                                      : 'Mulai Sekarang',
                                  color: Colors.white),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 17,
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
