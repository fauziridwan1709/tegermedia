part of '_interaksi.dart';

class InteractionStudyScreen extends StatefulWidget {
  @override
  _InteractionStudyScreenState createState() => _InteractionStudyScreenState();
}

class _InteractionStudyScreenState extends State<InteractionStudyScreen> {
  final interactionState = GlobalState.interaction();
  final authState = GlobalState.auth().state;

  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    Utils.log('init state', info: 'init state');
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(TIME_LIST_CLASS)) {
        var date = value.getString(TIME_LIST_CLASS);
        if (isDifferenceLessThanFiveMin(date) && interactionState.state.interactionData != null) {
          await refreshIndicatorKey.currentState.show();
        } else {
          await value.setString(TIME_LIST_CLASS, DateTime.now().toIso8601String());
          await refreshIndicatorKey.currentState.show();
        }
      } else {
        await value.setString(TIME_LIST_CLASS, DateTime.now().toIso8601String());
        await refreshIndicatorKey.currentState.show();
      }
    });
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await SharedPreferences.getInstance().then((value) async {
        await interactionState.setState((s) async => await s.retrieveData(0), catchError: true,
            onError: (context, dynamic error) {
          print((error as SiswamediaException).message);
        }, onData: (_, data) {
          interactionState.setState((s) => s.setCount(0));
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Utils.log(authState.currentState, info: 'make sure something');
    // print(authState.currentState);
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: vText(
          'Interaksi Belajar',
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_outlined,
            color: SiswamediaTheme.primary,
          ),
        ),
      ),
      body: StateBuilder<InteractionState>(builder: (context, snapshot) {
        return RefreshIndicator(
          key: refreshIndicatorKey,
          onRefresh: () async {
            await retrieveData();
          },
          child: WhenRebuilder<InteractionState>(
            observe: () => interactionState,
            onWaiting: () => WaitingView(),
            onError: (dynamic error) => ErrorView(error: error),
            onIdle: () => WaitingView(),
            onData: (data) {
              return ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      children: <Widget>[
                        if (!authState.currentState.classRole.isSiswaOrOrtu)
                          InkWell(
                            onTap: () {
                              context.push<void>(CreateStudyRoomScreen()).then((_) async {
                                await retrieveData();
                                print('add');
                                setState(() {});
                              });
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(color: SiswamediaTheme.borderGrey, width: 1),
                                    color: Colors.white),
                                padding: EdgeInsets.symmetric(vertical: 7),
                                margin: EdgeInsets.symmetric(vertical: 12),
                                width: double.infinity,
                                child: Stack(
                                  children: [
                                    Center(
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: SiswamediaTheme.primary,
                                            borderRadius: BorderRadius.circular(72)),
                                        padding: EdgeInsets.all(8),
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.white,
                                        ),
                                      ),
                                    )
                                  ],
                                )),
                          ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            vText(!data.isRiwayat ? 'Sedang Berlangsung' : 'Riwayat',
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                                color: SiswamediaTheme.nearBlack),
                            InkWell(
                              onTap: () {
                                data.selectedRiwayat();
                                setState(() {});
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 6, horizontal: 29),
                                decoration: BoxDecoration(
                                    color: data.isRiwayat ? SiswamediaTheme.primary : Colors.blue,
                                    borderRadius: BorderRadius.circular(5)),
                                child: vText(data.isRiwayat ? 'Berlangsung' : 'Riwayat',
                                    color: Colors.white, fontSize: 14),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        if (!authState.currentState.classRole.isSiswaOrOrtu)
                          ListView.builder(
                              itemCount: data.interactionSelected.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (c, i) => _itemList(
                                  data.interactionSelected[i].studyRoomId.toString(), data,
                                  startDate: data.interactionSelected[i].startDate,
                                  title: data.interactionSelected[i].title,
                                  room_id: data.interactionSelected[i].chatRoomId,
                                  url: data.interactionSelected[i].url,
                                  data: data.interactionSelected[i])),
                        if (authState.currentState.classRole == 'SISWA')
                          ListView.builder(
                              itemCount: data.studentSelected.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (c, i) => _itemList(
                                  data.studentSelected[i].studyRoomRefId.toString(), data,
                                  room_id: data.studentSelected[i].chatRoomId,
                                  startDate: data.studentSelected[i].startDate,
                                  title: data.studentSelected[i].title,
                                  url: data.studentSelected[i].url,
                                  CourseName: data.studentSelected[i].courseName)),
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        );
      }),
    );
  }

  Widget _itemList(String id, InteractionState state,
      {String startDate,
      String title,
      String url,
      String CourseName,
      String room_id,
      InteractionModel data}) {
    return InkWell(
      onTap: () {
        Navigator.push<dynamic>(
            context,
            MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => DetailInteractionScreen(
                      id,
                      startDate: startDate,
                      title: title,
                      url: url,
                      room_id: room_id,
                      isRiwayat: state.isRiwayat,
                    ))).then((dynamic value) async {
          await retrieveData();
        });
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(color: SiswamediaTheme.borderGrey, width: 1),
        ),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        margin: EdgeInsets.only(top: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (!authState.currentState.classRole.isSiswaOrOrtu)
              Container(
                height: 20,
                width: double.infinity,
                child: ListView.builder(
                  itemBuilder: (c, i) => i == 0
                      ? vText(data.listCourseClassId[i].courseName,
                          color: SiswamediaTheme.primary, fontWeight: FontWeight.bold, fontSize: 16)
                      : vText(' & ' + data.listCourseClassId[i].courseName,
                          color: SiswamediaTheme.primary,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                  scrollDirection: Axis.horizontal,
                  itemCount: data.listCourseClassId.length,
                ),
              ),
            if (authState.currentState.classRole == 'SISWA')
              vText(CourseName,
                  color: SiswamediaTheme.black, fontWeight: FontWeight.bold, fontSize: 16),
            SizedBox(
              height: 6,
            ),
            vText(title,
                color: SiswamediaTheme.textGrey, fontWeight: FontWeight.bold, fontSize: 14),
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.calendar_today_outlined,
                            size: 12,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Container(
                            child: vText(formattedDate(startDate).toString(),
                                color: SiswamediaTheme.textGrey[500],
                                fontSize: 14,
                                overflow: TextOverflow.ellipsis),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 12,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 4, horizontal: 24),
                  decoration: BoxDecoration(
                    color: SiswamediaTheme.primary,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: vText('Lihat', color: Colors.white, fontSize: 17),
                )
              ],
            ),
            SizedBox(
              height: 12,
            ),
            Divider(
              color: SiswamediaTheme.border,
            ),
            Row(
              children: [
                if (!authState.currentState.classRole.isSiswaOrOrtu)
                  Expanded(
                    child: Container(
                      height: 20,
                      child: ListView.builder(
                        itemBuilder: (c, i) => i == 0
                            ? vText(data.listCourseClassId[i].className,
                                color: SiswamediaTheme.textGrey, fontWeight: FontWeight.w500)
                            : vText(' & ' + data.listCourseClassId[i].className,
                                color: SiswamediaTheme.textGrey, fontWeight: FontWeight.w500),
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: data.listCourseClassId.length,
                      ),
                    ),
                  ),
                if (!authState.currentState.classRole.isSiswaOrOrtu)
                  InkWell(
                      onTap: () async {
                        var deleted = await state.deletedId(data.studyRoomId);

                        if (deleted.statusCode == 200) {
                          await refreshIndicatorKey.currentState.show();
                        } else {
                          CustomFlushBar.errorFlushBar(
                            deleted.message,
                            context,
                          );
                        }
                      },
                      child: Icon(Icons.delete_outline)),
                SizedBox(
                  width: 20,
                  height: 20,
                ),
                if (!authState.currentState.classRole.isSiswaOrOrtu && !state.isRiwayat)
                  InkWell(
                      onTap: () {
                        Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                                builder: (BuildContext context) => CreateStudyRoomScreen(
                                      data: data,
                                    )));
                      },
                      child: Icon(Icons.edit_outlined)),
              ],
            )
          ],
        ),
      ),
    );
  }
}
