part of '_interaksi.dart';

class CreateStudyRoomScreen extends StatefulWidget {
  final InteractionModel data;

  const CreateStudyRoomScreen({Key key, this.data}) : super(key: key);
  @override
  _CreateStudyRoomScreenState createState() => _CreateStudyRoomScreenState();
}

class _CreateStudyRoomScreenState extends State<CreateStudyRoomScreen> {
  TextEditingController editingContoller = TextEditingController();
  TextEditingController startDateContoller = TextEditingController();
  String startDate;
  final interactionState = GlobalState.interaction();
  final authState = GlobalState.auth();
  final multiClass = GlobalState.multiSelectClass();

  @override
  void initState() {
    super.initState();
    startDate = widget.data?.startDate == null
        ? DateTime.now().toLocal().toIso8601String().substring(0, 19)
        : widget.data.startDate;
    if (widget.data != null) {
      editingContoller.text = widget.data.title;
      startDate = DateTime.parse(startDate).toLocal().toIso8601String();

      print('$startDate');
      widget.data.listCourseClassId.forEach((element) {
        var value = DataMultiSelectClass(
            classId: element.classId,
            courseId: element.courseId,
            className: element.className,
            courseName: element.courseName);
        multiClass.state.selected[value.classId] = value;
      });
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: vText(
            widget.data == null
                ? 'Buat Ruangan Belajar'
                : 'Edit Ruangan Belajar',
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_outlined,
              color: SiswamediaTheme.primary,
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Stack(
            children: [
              _body(),
              Align(alignment: Alignment.bottomCenter, child: _bottomMain())
            ],
          ),
        ));
  }

  Widget _bottomMain() {
    return InkWell(
      onTap: () async {
        if (widget.data == null) {
          SiswamediaTheme.infoLoading(context: context, info: 'Sedang membuat');
          var dateStart = DateTime.parse(startDate).toUtc();
          var listCourseClass = <Map<String, int>>[];
          GlobalState.multiSelectClass().state.selected.forEach((key, value) {
            listCourseClass.add(
                <String, int>{'class_id': key, 'course_id': value.courseId});
          });
          var dataMap = <String, dynamic>{
            'title': editingContoller.text,
            'start_date': dateStart.toIso8601String(),
            'list_course_class_id': listCourseClass
          };
          var result = await InteractionServices.createRoom(data: dataMap);
          if (result.statusCode == 200) {
            Navigator.pop(context);
            Navigator.pop(context);
            //todo
            // interactionState.state.retrieveData();
            // interactionState.notify();
            await interactionState.setState((s) => s.retrieveData(0));
            CustomFlushBar.successFlushBar(
              'Berhasil membuat',
              context,
            );
            //todo change with cleaner
            await GlobalState.multiSelectClass().refresh();
          } else {
            Navigator.pop(context);
            Navigator.pop(context);
            CustomFlushBar.errorFlushBar(
              'Eror' + '!',
              context,
            );
            await GlobalState.multiSelectClass().refresh();
          }
        } else {
          SiswamediaTheme.infoLoading(context: context, info: 'Sedang membuat');
          var dateStart = DateTime.parse(startDate).toUtc();
          var listCourseClass = <Map<String, int>>[];
          var deleteCourseClass = <Map<String, int>>[];
          print('to utc');
          print(dateStart);
          GlobalState.multiSelectClass().state.selected.forEach((key, value) {
            if (widget.data.listCourseClassId
                .where((element) => element.classId == key)
                .isEmpty) {
              listCourseClass.add(
                  <String, int>{'class_id': key, 'course_id': value.courseId});
            }
          });
          widget.data.listCourseClassId.forEach((element) {
            if (!GlobalState.multiSelectClass()
                .state
                .selected
                .containsKey(element.classId)) {
              deleteCourseClass.add(<String, int>{
                'study_room_relation_id': element.studyRoomRelationId
              });
            }
          });
          var dataMap = <String, dynamic>{
            'title': editingContoller.text,
            'start_date': dateStart.toIso8601String(),
            'added_class_course': listCourseClass,
            'deleted_class_course': deleteCourseClass
          };
          print(dataMap.toString());
          var result = await InteractionServices.updateRoom(
              idInteraction: widget.data.studyRoomId, body: dataMap);
          if (result.statusCode == 200) {
            Navigator.pop(context);
            Navigator.pop(context);
            //todo
            CustomFlushBar.successFlushBar(
              'Berhasil membuat',
              context,
            );
            //todo change with cleaner
            await interactionState.setState((s) => s.retrieveData(0));
            // interactionState.state.retrieveData();
            // interactionState.notify();
            await GlobalState.multiSelectClass().refresh();
          } else {
            Navigator.pop(context);
            Navigator.pop(context);
            CustomFlushBar.errorFlushBar(
              'Eror' + '!',
              context,
            );
            await GlobalState.multiSelectClass().refresh();
          }
        }
      },
      child: Container(
        padding: EdgeInsets.all(11),
        margin: EdgeInsets.only(bottom: 21),
        height: 40,
        width: double.infinity,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: SiswamediaTheme.primary,
          borderRadius: BorderRadius.circular(5),
        ),
        child: vText(widget.data == null ? 'Buat Sekarang' : 'Edit Sekarang',
            color: Colors.white, fontSize: 14),
      ),
    );
  }

  Widget _body() {
    print('startingggg');
    print(startDate);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 12,
        ),
        TextField(
          controller: editingContoller,
          decoration: new InputDecoration(hintText: 'Topik Belajar'),
        ),
        SizedBox(
          height: 12,
        ),
        CustomText('Tentukkan Kelas', Kind.heading3),
        authState.state.currentState.type == 'PERSONAL'
            ? SizedBox()
            : MultiSelectClass(
                onTap: () => navigate(context, ChooseClassScreen()),
              ),
        SizedBox(height: 10),
        StateBuilder<MultiSelectClassState>(
            observe: () => multiClass,
            builder: (context, data) {
              return Wrap(
                spacing: 10,
                runSpacing: 10,
                children: data.state.selected.values
                    .map((e) => Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 14, vertical: 8),
                          decoration: BoxDecoration(
                              color: SiswamediaTheme.lightBorder,
                              borderRadius: radius(6)),
                          child: Text('${e.className} - ${e.courseName}',
                              style: mediumBlack.copyWith(
                                  color: Color(0xFF4B4B4B),
                                  fontSize: S.w / 30)),
                        ))
                    .toList(),
              );
            }),
        SizedBox(
          height: 12,
        ),
        title('Tanggal Mulai'),
        SizedBox(
          height: 12,
        ),
        DateTimePicker(
          type: DateTimePickerType.dateTime,
          controller: TextEditingController(text: (startDate + 'Z')),
          dateMask: 'dd MMM yyyy - HH:mm',
          firstDate: DateTime.now(),
          lastDate: DateTime(2100),
          icon: Icon(Icons.calendar_today_outlined),
          dateLabelText: 'Tanggal mulai dan jam mulai',
          onChanged: (val) {
            setState(() {
              startDate = val;
              print(val);
            });
          },
          // decoration: InputDecoration(
          //   border: InputBorder.none,
          // ),
        ),
        SizedBox(
          height: 12,
        ),
      ],
    );
  }

  Widget title(String title) {
    return vText(title,
        fontSize: 19,
        color: SiswamediaTheme.darkText,
        fontWeight: FontWeight.w500);
  }

  Widget filterOption(
    String titleDropdown,
    String selectedValue,
    List<String> dropdownValue,
    String hint,
    ValueChanged onChanged,
  ) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              vText(titleDropdown, fontSize: 14),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(18),
                    border:
                        Border.all(color: SiswamediaTheme.border, width: 1)),
                child: ClassicDropdown<String>(
                  hint: hint,
                  titleValue: (item) => item,
                  valueSelected: selectedValue,
                  onChanged: onChanged,
                  itemsDropdown: dropdownValue,
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
