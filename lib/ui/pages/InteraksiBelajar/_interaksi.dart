import 'dart:async';

import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/core/socket/socket.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/interaction/chat.dart';
import 'package:tegarmedia/models/interaction/message.dart';
import 'package:tegarmedia/models/interaction/participants.dart';
import 'package:tegarmedia/services/interaction/_interaction.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:tegarmedia/ui/widgets/class/_class.dart';
import 'package:tegarmedia/ui/widgets/v_dropdown.dart';
import 'package:tegarmedia/utils/v_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';

part 'choose_class_screen.dart';
part 'create_study_room_screen.dart';
part 'detail_chat_screen.dart';
part 'detail_interaction_screen.dart';
part 'interaction_study_screen.dart';
part 'whiteboard_screen.dart';
