part of '_interaksi.dart';

class WhiteboardScreen extends StatefulWidget {
  final String url;
  final String title;
  final String id;
  final String room_id;

  const WhiteboardScreen({Key key, this.url, this.title, this.id, this.room_id})
      : super(key: key);

  @override
  _WhiteboardScreenState createState() => _WhiteboardScreenState();
}

class _WhiteboardScreenState extends State<WhiteboardScreen> {
  final authState = GlobalState.auth().state;
  final state = GlobalState.interaction().state;

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
    state.getChats('', widget.room_id);
    state.getParticipants('', widget.room_id);
    initData();
  }

  final interactionState = GlobalState.interaction();
  final SocketServices _socket =SocketServices.instance;

  void initData() async {
    // Socket Retrieve Data

    print("init socket");
    await _socket.connectSocket(onMsgReceived: (dynamic data) async {
      DataMessage dataMessage = data;
      await interactionState.setState(
          (s) async {
            if (data.title.contains('addChat')) {
              print('addChat : ${dataMessage.message} ${dataMessage.title}');
              DetailChats message = dataMessage.message;

              s.realTimeAddData(message);
            } else {
              print('participant : ${data.message}');
              DataParticipants participant = data.message;
              s.realTimeAddParticipants(participant);
            }
          },
          catchError: true,
          onError: (context, dynamic error) {
            print((error as SiswamediaException).message);
          },
          onData: (_, data) {
            interactionState.setState((s) => s.setCount(0));
          });
    });

    await interactionState.setState((s) => s.clearChats());
  }

  @override
  void dispose() {
    onDispose();

    super.dispose();
  }

  Future<void> onDispose() async {
    _socket.close();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            centerTitle: false,
            automaticallyImplyLeading: true,
            titleSpacing: 0,
            title: authState.currentState.classRole == 'SISWA'
                ? InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: vText('Keluar Ruangan',
                        fontSize: 12, color: SiswamediaTheme.textGrey[500]),
                  )
                : Container(),
            leading: authState.currentState.classRole != 'SISWA'
                ? IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: SiswamediaTheme.primary,
                    ),
                  )
                : IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.exit_to_app,
                      size: 20,
                      color: SiswamediaTheme.textGrey[500],
                    ),
                  ),
            actions: [
              if (authState.currentState.classRole != 'SISWA')
                Center(
                  child: InkWell(
                    onTap: () async {

                      await interactionState.state.endClass(widget.id);
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: SiswamediaTheme.blueBackground,
                          borderRadius: BorderRadius.circular(5)),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                      margin: EdgeInsets.only(right: 16),
                      child: vText('Akhiri Kelas', color: Colors.white),
                    ),
                  ),
                )
            ],
          ),
          body: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                margin: EdgeInsets.only(top: 1),
                color: Colors.white,
                child: InkWell(
                  onTap: () async {
                    interactionState.state.clearChats();
                    interactionState.notify();
                    await Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => DetailChatScreen(
                                  room_id: widget.room_id,
                                )));
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 22, bottom: 5),
                        child: vText(widget.title,
                            color: SiswamediaTheme.textGrey[500],
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      WhenRebuilder<InteractionState>(
                        observe: () => interactionState,
                        onWaiting: () => WaitingView(),
                        onError: (dynamic error) =>
                            ErrorView(error: (error is SiswamediaException)),
                        onIdle: () => WaitingView(),
                        onData: (data) {
                          return Row(
                            children: [
                              Icon(
                                Icons.group,
                                color: state.chatCount == 0
                                    ? SiswamediaTheme.textGrey[200]
                                    : SiswamediaTheme.primary,
                              ),
                              if (state.chatCount != 0)
                                vText(state.chatCount.toString(),
                                    color: SiswamediaTheme.primary),
                            ],
                          );
                        },
                      ),
                      SizedBox(
                        height: 17,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: WebView(
                  initialUrl: widget.url,
                  javascriptMode: JavascriptMode.unrestricted,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
