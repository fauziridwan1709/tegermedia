part of '_interaksi.dart';

class DetailChatScreen extends StatefulWidget {
  final String room_id;

  const DetailChatScreen({Key key, this.room_id}) : super(key: key);

  @override
  _DetailChatScreenState createState() => _DetailChatScreenState();
}

class _DetailChatScreenState extends State<DetailChatScreen>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: SiswamediaTheme.primary,
          ),
        ),
        title: vText('Detail'),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 1,
          ),
          DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: TabBar(
              controller: tabController,
              indicatorColor: SiswamediaTheme.primary,
              labelColor: SiswamediaTheme.primary,
              unselectedLabelColor:
                  Theme.of(context).dividerColor.withOpacity(0.54),
              tabs: [
                Tab(
                  text: 'Chat',
                  icon: Icon(Icons.message_outlined),
                ),
                Tab(
                  text: 'Personal',
                  icon: Icon(
                    Icons.group,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: tabController,
              children: [
                ChatParticipantScreen(
                  room_id: widget.room_id,
                ),
                PersonalParticipantScreen(
                  room_id: widget.room_id,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ChatParticipantScreen extends StatefulWidget {
  final String room_id;

  const ChatParticipantScreen({Key key, this.room_id}) : super(key: key);

  @override
  _ChatParticipantScreenState createState() => _ChatParticipantScreenState();
}

class _ChatParticipantScreenState extends State<ChatParticipantScreen> {
  double width;
  final interactionState = GlobalState.interaction();
  TextEditingController textEditingController;

  ScrollController scrollController;
  // SocketServices _socket = SocketServices.instance;

  // bool _initialLoad = true;
  // Timer _timer;

  @override
  void dispose() {
    // TODO: implement dispose
    GlobalState.interaction().state.clearChats();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    textEditingController = TextEditingController();
    scrollController = ScrollController();
    WidgetsBinding.instance.addPostFrameCallback(widgetBuilt);
  }

  void widgetBuilt(Duration d) {
    scrollController.animateTo(
      scrollController.position.maxScrollExtent,
      duration: Duration(milliseconds: 150),
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Container(
      height: height,
      width: width,
      child: WhenRebuilder<InteractionState>(
        observe: () => interactionState,
        onWaiting: () => WaitingView(),
        onError: (dynamic error) =>
            ErrorView(error: (error is SiswamediaException)),
        onIdle: () => WaitingView(),
        onData: (data) {
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                    itemCount: data.detailsChats.length,
                    controller: scrollController,
                    padding: EdgeInsets.only(bottom: 10),
                    itemBuilder: (c, i) => Container(
                          margin: EdgeInsets.only(left: 8, top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                  width: 35,
                                  height: 35,
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(8),
                                      image: DecorationImage(
                                          image: data.detailsChats[i].user
                                                      .profileImagePresignedUrl !=
                                                  null
                                              ? NetworkImage(
                                                  data.detailsChats[i].user
                                                      .profileImagePresignedUrl,
                                                )
                                              : Image.asset(
                                                  'assets/user_pic.png'),
                                          fit: BoxFit.cover))),
                              SizedBox(
                                width: 16,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      vText(data.detailsChats[i].user.email,
                                          color: SiswamediaTheme.primary,
                                          fontWeight: FontWeight.bold),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      vText(
                                          formattedDate(data
                                                  .detailsChats[i].createdAt)
                                              .toString()
                                              .toString(),
                                          color: SiswamediaTheme.textGrey)
                                    ],
                                  ),
                                  Container(
                                      width: width - 70,
                                      child: vText(data.detailsChats[i].message,
                                          maxLines: 5,
                                          overflow: TextOverflow.ellipsis)),
                                ],
                              ),
                            ],
                          ),
                        )),
              ),
              Container(
                width: width,
                color: Colors.white,
                child: Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: const InputDecoration(
                            hintText: 'Tulis Pesan !',
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none),
                        maxLines: 5,
                        minLines: 1,
                        controller: textEditingController,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    InkWell(
                        onTap: () {
                          String message = textEditingController.text;
                          interactionState.state
                              .sendMessage(message, widget.room_id);
                          textEditingController.text = '';
                        },
                        child: Icon(
                          Icons.send_outlined,
                          color: SiswamediaTheme.primary,
                        )),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),
              )
            ],
          );
        },
      ),
    ));
  }
}

class PersonalParticipantScreen extends StatefulWidget {
  final String room_id;

  const PersonalParticipantScreen({Key key, this.room_id}) : super(key: key);
  @override
  _PersonalParticipantScreenState createState() =>
      _PersonalParticipantScreenState();
}

class _PersonalParticipantScreenState extends State<PersonalParticipantScreen> {
  final interactionState = GlobalState.interaction();
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 8,
          ),
          Container(
            margin: EdgeInsets.only(left: 8),
            child: vText('IN CALL',
                color: SiswamediaTheme.primary,
                fontSize: 16,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 8,
          ),
          WhenRebuilder<InteractionState>(
            observe: () => interactionState,
            onWaiting: () => WaitingView(),
            onError: (dynamic error) =>
                ErrorView(error: (error is SiswamediaException)),
            onIdle: () => WaitingView(),
            onData: (data) {
              return Expanded(
                child: ListView.builder(
                    itemCount: data.detailsParticipants.participants.length,
                    shrinkWrap: true,
                    itemBuilder: (c, i) => Container(
                          margin: EdgeInsets.only(left: 8, top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(72),
                                      image: DecorationImage(
                                          image: NetworkImage(data
                                              .detailsParticipants
                                              .participants[i]
                                              .user
                                              .profileImagePresignedUrl),
                                          fit: BoxFit.cover))),
                              SizedBox(
                                width: 16,
                              ),
                              Expanded(
                                child: vText(
                                  data.detailsParticipants.participants[i].user
                                      .name,
                                  fontSize: 18,
                                  color: SiswamediaTheme.textGrey,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        )),
              );
            },
          ),
        ],
      ),
    );
  }
}
