part of '_pages.dart';

class HomePage extends StatefulWidget {
  final bool isWeb;
  const HomePage({this.isWeb = false});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  final auth = GlobalState.auth();
  final authState = GlobalState.auth().state;
  final home = GlobalState.home();
  final homeState = GlobalState.home().state;
  final profile = GlobalState.profile();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  final scaffoldKey = GlobalKey<ScaffoldState>();
  List<String> tagNews = <String>['Berita Sekolah', 'Artikel'];
  int currentIndex = 0;
  AnimationController _ColorAnimationController;
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  AnimationController _TextAnimationController;
  Animation _colorTween, _homeTween, _workOutTween, _iconTween, _drawerTween, _bottomBorderTween;

  ScrollController _scrollViewController;
  bool _showAppbar = true;
  bool isScrollingDown = false;

  double height = 0.0;

  @override
  void initState() {
    _ColorAnimationController = AnimationController(vsync: this, duration: Duration(seconds: 0));
    _colorTween =
        ColorTween(begin: Colors.transparent, end: Colors.white).animate(_ColorAnimationController);
    _bottomBorderTween = ColorTween(begin: Colors.transparent, end: Colors.grey[300])
        .animate(_ColorAnimationController);
    _iconTween = ColorTween(begin: Colors.white, end: Colors.lightBlue.withOpacity(0.5))
        .animate(_ColorAnimationController);
    _drawerTween =
        ColorTween(begin: Colors.black, end: Colors.black).animate(_ColorAnimationController);
    _homeTween = ColorTween(begin: SiswamediaTheme.white, end: SiswamediaTheme.white)
        .animate(_ColorAnimationController);
    _workOutTween = ColorTween(begin: SiswamediaTheme.green, end: SiswamediaTheme.green)
        .animate(_ColorAnimationController);
    _TextAnimationController = AnimationController(vsync: this, duration: Duration(seconds: 0));

    super.initState();
    if (authState.isLogin) {
      // authState.setState((s) => s.initializeState());
    }
    _scrollViewController = ScrollController();
    _scrollViewController.addListener(() {
      if (_scrollViewController.position.userScrollDirection == ScrollDirection.reverse) {
        if (!isScrollingDown) {
          isScrollingDown = true;
          _showAppbar = false;
          setState(() {});
        }
      }

      if (_scrollViewController.position.userScrollDirection == ScrollDirection.forward) {
        if (isScrollingDown) {
          isScrollingDown = false;
          _showAppbar = true;
          setState(() {});
        }
      }
    });
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   showOverlay(context);
    // });

    // fetchData();
  }

  @override
  void dispose() {
    _scrollViewController.removeListener(() {});
    _scrollViewController.dispose();
    super.dispose();
  }

  void showOverlay(BuildContext context) {
    prefs.then((pref) {
      if (!pref.getBool('is_no_school')) {
        print('something');
        return ActionOverlay.noSchoolOverlay(context);
      }
    });
  }

  Future<void> retrieveData() async {}

  bool scrollListener(ScrollNotification scrollInfo) {
    var scroll = false;
    if (scrollInfo.metrics.axis == Axis.vertical) {
      _ColorAnimationController.animateTo(scrollInfo.metrics.pixels / 80);
      _TextAnimationController.animateTo(scrollInfo.metrics.pixels);
      if (_scrollViewController.position.userScrollDirection == ScrollDirection.reverse) {
        if (!isScrollingDown) {
          setState(() {
            isScrollingDown = true;
            _showAppbar = false;
          });
        }
      }

      if (_scrollViewController.position.userScrollDirection == ScrollDirection.forward) {
        if (isScrollingDown) {
          setState(() {
            isScrollingDown = false;
            _showAppbar = true;
          });
        }
      }
      return scroll = true;
    }
    return scroll;
  }

  var list = <String>['SCHOOL NEWS', 'SERTIMEDIA NEWS'];
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark,
      statusBarColor: Colors.transparent,
    ));
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    var ratio = MediaQuery.of(context).size.aspectRatio;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: NotificationListener<ScrollNotification>(
        onNotification: scrollListener,
        child: Stack(
          children: [
            CustomScrollView(controller: _scrollViewController, slivers: <Widget>[
              SliverList(
                  delegate: SliverChildListDelegate([
                Container(
                  height: (S.w * .16) * ((((MenuData.menu.keys.length) - 1) ~/ 5) + 1) +
                      (S.w * .55) +
                      MediaQuery.of(context).padding.top,
                  child: Stack(children: [
                    Container(
                        height: (S.w * .55),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                          // colors: [SiswamediaTheme.green, SiswamediaTheme.green],
                          colors: [SiswamediaTheme.white, SiswamediaTheme.white],
                          // colors: [Color(0xffB5D548), Colors.green],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ))),
                    Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                              width: MediaQuery.of(context).size.width * .9,
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(top: S.w * .05, bottom: S.w * .05),
                              margin: EdgeInsets.only(
                                  left: S.w * .05,
                                  right: S.w * .05,
                                  top: S.w * .27 + MediaQuery.of(context).padding.top),
                              decoration: BoxDecoration(
                                  color: SiswamediaTheme.green,
                                  borderRadius: BorderRadius.circular(12),
                                  boxShadow: [
                                    BoxShadow(
                                        offset: Offset(2, 2),
                                        spreadRadius: 4,
                                        blurRadius: 4,
                                        color: Colors.grey.withOpacity(.1))
                                  ]),
                              child: MobileGrid(
                                ratio: ratio,
                                isPortrait: isPortrait,
                                isWeb: widget.isWeb,
                              )
                              // ScreenTypeLayout(
                              //     desktop: DesktopGrid(), mobile: MobileGrid())
                              ),
                        ],
                      ),
                    )
                  ]),
                ),
                ScreenTypeLayout(
                    desktop: Titles(
                      title: 'Informasi Buat Kamu',
                      // size: S.w / 100,
                    ),
                    mobile: Titles(title: 'Informasi Buat Kamu')),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * .05),
                  child: Row(mainAxisAlignment: MainAxisAlignment.start, children: _buildTag()),
                ),
                [
                  BeritaSekolah(
                    future: HomeServices.getData('SCHOOL NEWS'),
                    onTap: (e) => navigate(context, WebViewHome(title: e.judul, url: e.link)),
                    ratio: ratio,
                    orientation: orientation,
                  ),
                  SiswamediaNews(
                    ratio: ratio,
                    orientation: orientation,
                  )
                ].elementAt(currentIndex),
                Center(
                  child: InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                            builder: (_) => HomeDetailInfo(
                                object: HomeServices.getData(list.elementAt(currentIndex))))),
                    child: Text(
                      'Lihat Selengkapnya',
                      style: descBlack.copyWith(fontSize: S.w / 32, color: SiswamediaTheme.green),
                    ),
                  ),
                ),
                // ScreenTypeLayout(
                //     mobile: Titles(
                //   title: 'Dana Bos, SIPLAH',
                //   subtitle: 'Lihat Semua',
                //   callback: () => navigate(
                //       context,
                //       HomeDetailInfo(
                //         object: HomeServices.getData('SPESIAL BUAT KAMU'),
                //       )),
                // )),
                // SizedBox(
                //   height: S.w * .6,
                //   child: FutureBuilder<HomeModel>(
                //     future: HomeServices.getData('SPESIAL BUAT KAMU', typeWeb: 3),
                //     builder: (context, snapshot) {
                //       if (snapshot.hasData) {
                //         return SingleChildScrollView(
                //           scrollDirection: Axis.horizontal,
                //           padding: EdgeInsets.symmetric(
                //               horizontal: MediaQuery.of(context).size.width * .05),
                //           child: Row(
                //             children: snapshot.data.rows.reversed
                //                 .map(
                //                   (e) => SizedBox(
                //                     height: S.w * .6,
                //                     child: InkWell(
                //                         onTap: () => navigate(
                //                             context, WebViewHome(title: e.judul, url: e.link)),
                //                         child: CardAds(e)),
                //                   ),
                //                 )
                //                 .toList()
                //                 .sublist(
                //                     0,
                //                     snapshot.data.rows.reversed.toList().length > 4
                //                         ? 4
                //                         : snapshot.data.rows.reversed.toList().length),
                //           ),
                //         );
                //       } else {
                //         return SingleChildScrollView(
                //           scrollDirection: Axis.horizontal,
                //           padding: EdgeInsets.symmetric(horizontal: S.w * .02),
                //           child: Row(
                //             children: ['2', '2']
                //                 .map(
                //                   (e) => SizedBox(
                //                     height: S.w * .6,
                //                     child: CardAds(null),
                //                   ),
                //                 )
                //                 .toList(),
                //           ),
                //         );
                //       }
                //     },
                //   ),
                // ),
                // Titles(
                //     title: 'Siswamedia Youtube Channel',
                //     subtitle: 'Lihat semua',
                //     callback: () => navigate(
                //         context,
                //         HomeDetailInfo(
                //           object: HomeServices.getData('SISWAMEDIA YOUTUBE'),
                //         ))),
                // SizedBox(
                //   height: S.w * .45,
                //   child: FutureBuilder<HomeModel>(
                //     future: HomeServices.getData('SISWAMEDIA YOUTUBE'),
                //     builder: (context, snapshot) {
                //       if (snapshot.hasData) {
                //         return ListView(
                //             padding: EdgeInsets.only(
                //                 left: MediaQuery.of(context).size.width * .05,
                //                 right: MediaQuery.of(context).size.width * .05),
                //             scrollDirection: Axis.horizontal,
                //             children: snapshot.data.rows.reversed
                //                 .map((e) => Padding(
                //                     padding: EdgeInsets.only(left: 0, right: 12),
                //                     child: InkWell(
                //                         onTap: () => navigate(
                //                             context,
                //                             YoutubePlayer(
                //                                 videoId: e.link.substring(e.link.indexOf('=') + 1),
                //                                 isMoreInfo: true,
                //                                 title: e.judul,
                //                                 description: e.judul)),
                //                         child: CardDonasi(e))))
                //                 .toList()
                //                 .sublist(0,
                //                     snapshot.data.rows.length > 3 ? 4 : snapshot.data.rows.length));
                //       } else {
                //         return ListView.builder(
                //             scrollDirection: Axis.horizontal,
                //             itemCount: 4,
                //             itemBuilder: (_, index) {
                //               return Padding(
                //                   padding: EdgeInsets.only(
                //                       left: (index == 0) ? 24 : 0, right: (index == 1) ? 24 : 8),
                //                   child: CardDonasi(null));
                //             });
                //       }
                //     },
                //   ),
                // ),
                InkWell(
                  onTap: () => context.push<void>(Pantau()),
                  child: Container(
                    height: 150,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          bottom: 0,
                          child: Container(
                            height: 100,
                            width: S.w - 40,
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [SiswamediaTheme.shadowContainer],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          child: Hero(
                              tag: 'pantau',
                              child: Image.asset('assets/icons/pantau/pantau.png', height: 140)),
                        ),
                        Positioned(
                          right: 0,
                          top: 80,
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text('Pantau', style: semiWhite.copyWith(fontSize: 14)),
                                Text('Aktivitas siswa disini!',
                                    style: semiWhite.copyWith(fontSize: 12)),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Titles(
                  title: 'Protokol',
                  subtitle: 'Lihat Semua',
                  size: S.w / 25,
                  callback: () => Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                          builder: (_) => MorePageAcademic(
                                data: dataProtokol,
                                type: 'Protokol',
                              ))),
                ),
                SizedBox(
                    height: S.w * .42,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: dataProtokol.sublist(0, 3).length,
                        itemBuilder: (_, index) {
                          return Padding(
                              padding: EdgeInsets.only(
                                  left: (index == 0) ? defaultMargin : 0,
                                  right: (index == dataProtokol.length - 1) ? defaultMargin : 10),
                              child: InkWell(
                                onTap: () {
                                  // if (dataDummy[index].name == 'Pendaftaran') {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                      builder: (_) => StepRegistration(
                                          type: 'Protokol',
                                          url: dataProtokol[index].url,
                                          title: dataProtokol[index].name),
                                    ),
                                  );
                                  // }
                                },
                                child: Card(
                                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                    elevation: 5,
                                    shadowColor: Colors.grey.withOpacity(.2),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(12)),
                                    child: AspectRatio(
                                      aspectRatio: 1,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            dataProtokol[index].icon,
                                            height: S.w * .2,
                                            width: S.w * .2,
                                          ),
                                          Center(
                                            child: Container(
                                                margin: EdgeInsets.symmetric(vertical: 10),
                                                width: 160,
                                                child: Text(
                                                  dataProtokol[index].name,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontSize: S.w / 34,
                                                      fontWeight: FontWeight.bold,
                                                      color: SiswamediaTheme.black),
                                                )),
                                          ),
                                        ],
                                      ),
                                    )),
                              ));
                        })),
                Titles(
                    title: 'Panduan',
                    subtitle: 'Lihat Semua',
                    size: S.w / 25,
                    callback: () => Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                            builder: (_) => MorePageAcademic(data: dataPanduan, type: 'Panduan')))),
                SizedBox(
                    height: S.w * .42,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: dataPanduan.sublist(0, 3).length,
                        itemBuilder: (_, index) {
                          return Padding(
                              padding: EdgeInsets.only(
                                  left: (index == 0) ? defaultMargin : 0,
                                  right: (index == dataPanduan.length - 1) ? defaultMargin : 12),
                              child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                        builder: (_) => StepRegistration(
                                          url: dataPanduan[index].url,
                                          title: dataPanduan[index].name,
                                          type: 'Panduan',
                                        ),
                                      ),
                                    );
                                  },
                                  child: Card(
                                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                      elevation: 5,
                                      shadowColor: Colors.grey.withOpacity(.2),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(12)),
                                      child: AspectRatio(
                                        aspectRatio: 1,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Image.asset(dataPanduan[index].icon,
                                                height: S.w * .2, width: S.w * .2),
                                            Center(
                                              child: Container(
                                                  margin: EdgeInsets.symmetric(vertical: 10),
                                                  width: 160,
                                                  child: Text(
                                                    dataPanduan[index].name,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: S.w / 34,
                                                        fontWeight: FontWeight.bold,
                                                        color: SiswamediaTheme.black),
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ))));
                        })),
                SizedBox(height: 50)
              ])),
            ]),
            AnimatedAppBar(
              type: 'MOBILE',
              isWeb: widget.isWeb,
              onLogin: () async => await _onLogin(),
              onLoginApple: () async => await authState.loginApple(context),
              isShowAppbar: _showAppbar,
              drawerTween: _drawerTween,
              colorAnimationController: _ColorAnimationController,
              colorTween: _colorTween,
              homeTween: _homeTween,
              iconTween: _iconTween,
              workOutTween: _workOutTween,
              bottomBorderTween: _bottomBorderTween,
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildTag() {
    return tagNews
        .map((e) => TagNews(
              currentIndex: currentIndex,
              title: e,
              onTap: (val) => _selectNews(val),
            ))
        .toList();
  }

  Future<void> _onLogin() async {
    SiswamediaTheme.infoLoading(context: context, info: 'Logging in');
    await auth.state.login(context).then((result) async {
      print(result);
      Navigator.pop(context);
      if (result == StatusCodeConst.noInternet) {
        return CustomFlushBar.errorFlushBar('No Internet, try again', context);
      } else if (result == StatusCodeConst.platform) {
        return CustomFlushBar.errorFlushBar('Terjadi kesalahan dalam akun google anda', context);
      } else if (result == StatusCodeConst.success) {
        await auth.setState((authState) => authState.initializeState().then((value) async =>
            await profile.setState((s) => s.retrieveData(), onError: (context, dynamic error) {
              CustomFlushBar.errorFlushBar('Login gagal, coba lagi', context);
              auth.setState((s) => s.removeToken());
              auth.refresh();
              profile.refresh();
            }, onData: (_, __) {
              auth.notify();
              FirebaseMessaging.instance.subscribeToTopic('/topics/${profile.state.profile.id}');
              Logger().d('--- FLAG LOGIN SUCCESSFULL ---');
              CustomFlushBar.successFlushBar('Login Successful', context);
            })));
        await prefs.then((value) => value.setBool(FLAG, true));
      } else if (result == StatusCodeConst.general) {
        return CustomFlushBar.errorFlushBar('Login Failed', context);
      } else if (result == StatusCodeConst.timeout) {
        return CustomFlushBar.errorFlushBar('Timeout Try again', context);
      } else if (result == 422) {
        return CustomFlushBar.errorFlushBar('Invalid credential', context);
      }
    });
  }

  void _selectNews(int i) {
    setState(() {
      currentIndex = i;
    });
  }
}

class SiswamediaNews extends StatelessWidget {
  final bool isWeb;
  final Orientation orientation;
  final double ratio;
  const SiswamediaNews({this.isWeb, this.orientation, this.ratio});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width * .9,
        child: FutureBuilder<HomeModel>(
            future: HomeServices.getData('SERTIMEDIA NEWS', typeWeb: 1),
            builder: (context, snapshot) {
              return GridView.count(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.05),
                crossAxisCount: gridCountHome(ratio),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                childAspectRatio: 3,
                children: (snapshot.hasData
                        ? snapshot.data.rows.reversed
                        : [HomeItem(), HomeItem(), HomeItem(), HomeItem()])
                    .map(
                      (e) => InkWell(
                          onTap: () => navigate(context, WebViewHome(title: e.judul, url: e.link)),
                          child: CardNews(snapshot.hasData ? e : null, e.cached)),
                    )
                    .toList()
                    .sublist(0, getSublist(ratio)),
              );
            }));
  }
}

class MobileGrid extends StatelessWidget {
  final bool isWeb;
  final bool isPortrait;
  final double ratio;
  MobileGrid({this.isWeb, this.isPortrait, this.ratio});

  @override
  Widget build(BuildContext context) {
    var auth = Injector.getAsReactive<AuthState>().state;
    return StateBuilder<AuthState>(
      observe: () => GlobalState.auth(),
      builder: (context, constrains) {
        if ((auth.isLogin ? auth.currentState == null : false)) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                    SkeletonLine(),
                  ]),
                ),
                Container(
                  height: 1,
                  width: S.w,
                  color: Colors.grey.withOpacity(.2),
                ),
                GridView.count(
                  physics: ScrollPhysics(),
                  crossAxisCount: isPortrait ? 5 : 10,
                  mainAxisSpacing: 4,
                  shrinkWrap: true,
                  padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                  childAspectRatio: 1,
                  children: (isWeb
                          ? MenuData.menuWeb.keys
                          : (auth.favoriteMenu != null)
                              ? [
                                  auth.favoriteMenu[0],
                                  auth.favoriteMenu[1],
                                  auth.favoriteMenu[2],
                                  auth.favoriteMenu[3],
                                  auth.favoriteMenu[4],
                                ]
                              : MenuData.menu.keys)
                      .map((e) => Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: SkeletonLine(),
                          ))
                      .toList(),
                ),
              ],
            ),
          );
        }
        return Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                  Expanded(
                    child: Text(
                        !auth.isLogin
                            ? 'Belum Login'
                            : auth.currentState.type == 'PERSONAL'
                                ? auth.currentState.schoolName
                                : auth.currentState.className ?? 'Pilih Kelas...',
                        style:
                            semiBlack.copyWith(fontSize: S.w / 27, color: SiswamediaTheme.white)),
                  ),
                  Row(
                    children: <Widget>[
                      //todo
                      // InkWell(
                      //   onTap: () {
                      //     var list = <String>[];
                      //     for (var i = 0; i < 9; i++) {
                      //       if (i < auth.favoriteMenu.length &&
                      //           auth.favoriteMenu[i] != null &&
                      //           auth.favoriteMenu[i] != 'Lainnya') {
                      //         list.add(auth.favoriteMenu[i]);
                      //       } else {
                      //         list.add('Empty');
                      //       }
                      //     }
                      //     if (list.contains('Lainnya')) {
                      //       list.removeWhere(
                      //           (element) => element == 'Lainnya');
                      //     }
                      //     navigate(context, MenuList(favoriteMenu: list));
                      //   },
                      //   child: Icon(Icons.add,
                      //       color: SiswamediaTheme.green, size: 20),
                      // ),
                      SizedBox(width: 10),
                      if (auth.currentState == null ? false : auth.currentState.type == 'SEKOLAH')
                        InkWell(
                            onTap: () => navigate(context, SwitchRole(),
                                settings: RouteSettings(name: '/switchrole')),
                            child: Icon(
                              Icons.people_alt_outlined,
                              color: SiswamediaTheme.white,
                            )),
                      SizedBox(width: 10),
                      if (auth.currentState == null ? false : auth.currentState.type == 'SEKOLAH')
                        InkWell(
                          onTap: () {
                            if (auth.isLogin) {
                              Navigator.push<void>(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => ChangeClass(id: auth.currentState.schoolId)));
                            } else {
                              return CustomFlushBar.errorFlushBar('Anda Belum Login', context);
                            }
                          },
                          child: Image.asset('assets/images/asset_home/ganti.png',
                              height: 20, width: 20),
                        ),
                    ],
                  ),
                ]),
              ),
              SizedBox(height: 5),
              Container(
                height: 1,
                width: MediaQuery.of(context).size.width,
                color: Colors.grey.withOpacity(.2),
              ),
              GridView.count(
                physics: ScrollPhysics(),
                crossAxisCount: gridCount(ratio),
                mainAxisSpacing: 4,
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                childAspectRatio: 1,
                children: (((auth.currentState == null
                            ? false
                            : auth.currentState.type == 'PERSONAL')
                        ? MenuData.personal.keys
                        : auth.favoriteMenu != null
                            ? [
                                auth.favoriteMenu[0],
                                auth.favoriteMenu[1],
                                auth.favoriteMenu[2],
                                auth.favoriteMenu[3],
                                auth.favoriteMenu[4],
                              ]
                            : MenuData.menu.keys))
                    .map((e) => Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () async {
                              if (!auth.isLogin) {
                                return CustomFlushBar.errorFlushBar('Anda Belum Login', context);
                              }
                              if (auth.currentState.schoolId == null) {
                                return CustomFlushBar.errorInfoFlushBar(
                                    'Hi, agar menu ini berfungsi, pastikan dulu anda sudah memilih sekolah atau kelas',
                                    context);
                              }
                              if (e == 'Kelasku') {
                                if (auth.currentState.schoolId != null &&
                                    auth.currentState.type == 'PERSONAL') {
                                  await context.push<void>(ListClassOnlyme(
                                    id: auth.currentState.schoolId,
                                    stateRouteRegister: true,
                                    schoolRole: auth.currentState.schoolRole,
                                  ));
                                } else if (auth.currentState.schoolId != null &&
                                    auth.currentState.type == 'SEKOLAH') {
                                  await context.push<void>(ListClassOnlyme(
                                    id: auth.currentState.schoolId,
                                    stateRouteRegister: true,
                                    schoolRole: auth.currentState.schoolRole,
                                  ));
                                } else {
                                  dialogDev(context,
                                      title: 'Konfirmasi',
                                      desc:
                                          'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                                }
                              } else if (auth.currentState.classId == null) {
                                return dialogDev(context,
                                    title: 'Konfirmasi',
                                    desc:
                                        'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih kelas!!');
                              } else if (e == 'Tugas') {
                                if (auth.currentState.type == 'SEKOLAH' &&
                                    auth.currentState.schoolId != null &&
                                    auth.currentState.classRole != null &&
                                    auth.currentState.classRole.isNotEmpty) {
                                  await Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (context) =>
                                              MainAssignment(role: auth.currentState.classRole)));
                                } else if (auth.currentState.type == 'PERSONAL' &&
                                    auth.currentState.schoolId != null) {
                                  print(auth.currentState.schoolRole.first);
                                  await navigate(context,
                                      MainAssignment(role: auth.currentState.schoolRole.first));
                                } else if (auth.currentState.classId == null) {
                                  dialogDev(context,
                                      title: 'Konfirmasi',
                                      desc:
                                          'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih kelas!');
                                } else {
                                  dialogDev(context,
                                      title: 'Konfirmasi',
                                      desc:
                                          'Mohon maaf menu ini tidak bisa diakses karena kamu belum memilih sekolah!');
                                }
                              } else if (e == 'Jadwal') {
                                return navigate(
                                    context,
                                    JadwalKelas(
                                      classId: auth.currentState.classId,
                                    ));
                              } else if (e == 'Absensi') {
                                return navigate(context, Absensi());
                              } else if (e == 'Dokumen') {
                                return navigate(context, MainDocument());
                              } else if (e == 'Forum') {
                                return navigate(context, MainForum());
                              } else if (e == 'Konferensi') {
                                return navigate(context, Conference());
                              } else if (e == 'Lainnya') {
                                return navigate(context, MenuList(action: 'lainnya'));
                              } else if (e == 'Ujian') {
                                if (auth.currentState.type == 'PERSONAL') {}
                                return navigate(context, QuizClass(),
                                    settings: RouteSettings(name: '/quiz_home'));
                              } else if (e == 'Nilai') {
                                return navigate(context, Score());
                              }
                            },
                            child: Container(
                              height: ratio > 1.3 ? S.w * .16 : S.w * .14,
                              width: ratio > 1.3 ? S.w * .16 : S.w * .14,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      color: SiswamediaTheme.materialGreen[100],
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                    child: Image.asset(
                                        isWeb ? MenuData.menuWeb[e] : MenuData.all[e],
                                        height: ratio > 1.3 ? S.w * .14 : S.w * .12,
                                        width: ratio > 1.3 ? S.w * .14 : S.w * .12),
                                  ),
                                  SizedBox(height: 5),
                                  Text(e,
                                      style: descBlack.copyWith(
                                          fontSize: ratio > 1.3 ? S.w / 34 : S.w / 36,
                                          color: SiswamediaTheme.white))
                                ],
                              ),
                            ),
                          ),
                        ))
                    .toList(),
              ),
            ],
          ),
        );
      },
    );
  }
}

void reInitiateSize(double ratio, BuildContext context) {
  if (ratio > 1.9) {
    S().init(context);
  } else if (ratio > 1.5) {
    S().init(context);
  } else if (ratio > .9) {
    S().init(context);
  } else {
    S().init(context);
  }
}
