part of '_assignment.dart';

class AssignmentHomeFilter extends StatefulWidget {
  @override
  _AssignmentHomeFilterState createState() => _AssignmentHomeFilterState();
}

class _AssignmentHomeFilterState extends State<AssignmentHomeFilter> {
  var filterState = Injector.getAsReactive<FilterAssignmentState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: SiswamediaAppBar(
          context: context,
          title: 'Filter',
        ),
        body: StateBuilder<FilterAssignmentState>(
          observe: () => filterState,
          builder: (context, _) {
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 20),
                    Text('Status yang Ditampilkan',
                        style: semiBlack.copyWith(fontSize: S.w / 24)),
                    buildStatus(),
                    SizedBox(height: 20),
                    Text('Urutkan Dari',
                        style: semiBlack.copyWith(fontSize: S.w / 24)),
                    buildSort(),
                    // RoundedRectangleButton(
                    //     title: 'Terapkan',
                    //     fontColor: SiswamediaTheme.white,
                    //     backgroundColor: SiswamediaTheme.green,
                    //     onTap: () {}),
                  ],
                ),
              ),
            );
          },
        ));
  }

  Widget buildSort() {
    return Column(
      children: listSort
          .map((e) => FilterHomeSort(
                value: e,
                selectedValue: filterState.state.filter.sort,
                onChanged: (e) => filterState.setState((s) => s.updateSort(e)),
              ))
          .toList(),
    );
  }

  Widget buildStatus() {
    return Column(
      children: listStatus
          .map((name) => FilterHomeStatus(
                title: name,
                value: filterState.state.filter.status[name],
                onChanged: (boolVal) =>
                    filterState.setState((s) => s.changeStatus(name)),
              ))
          .toList(),
    );
  }
}
