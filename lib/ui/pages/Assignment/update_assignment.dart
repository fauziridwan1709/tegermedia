part of '_assignment.dart';

class UpdateAssignment extends StatefulWidget {
  final GlobalKey<AnimatedListState> listKey;
  final int assessmentId;

  UpdateAssignment({this.listKey, this.assessmentId});

  @override
  _UpdateAssignmentState createState() => _UpdateAssignmentState();
}

class _UpdateAssignmentState extends State<UpdateAssignment> {
  final classState = Injector.getAsReactive<ClassState>();
  final scheduleState = Injector.getAsReactive<AssignmentState>();
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  final multiSelectClassState = GlobalState.multiSelectClass();
  TextEditingController _name;
  TextEditingController _description;
  TextEditingController _tanggalMulai;
  TextEditingController _tanggalBerakhir;
  FocusNode nameNode = FocusNode();
  FocusNode descNode = FocusNode();
  String _startDate;
  String _endDate;
  String message = '';

  List<Attachments> attachments = [];

  DataAssessmentsTeacherById tugas;

  bool isLoading = false;

  double _progressValue = 0;
  int _progressPercentValue = 0;

  bool error = false;

  @override
  void initState() {
    super.initState();
    _name = TextEditingController();
    _description = TextEditingController();
    _tanggalMulai = TextEditingController();
    _tanggalBerakhir = TextEditingController();
    getData();
  }

  @override
  void dispose() {
    _name.dispose();
    _description.dispose();
    super.dispose();
  }

  Future<void> getData() async {
    setState(() {
      isLoading = true;
    });

    var data = await AssignmentServices.getAssessmentsTeacherById(
        assessmentId: widget.assessmentId);

    if (data.statusCode == 200) {
      print(data.data.toJson());
      setState(() {
        tugas = data.data;
        attachments = data.data.attachments;
        print(tugas.startDate);
        _name.text = tugas.name;
        _description.text = tugas.description;
        _startDate = tugas.startDate.substring(0, 19);
        _endDate = tugas.endDate.substring(0, 19);
        _tanggalMulai.text = tugas.startDate.substring(0, 19) + 'Z';
        _tanggalBerakhir.text = tugas.endDate.substring(0, 19) + 'Z';
        isLoading = false;
      });
      tugas.listClassCourse.forEach((element) {
        multiSelectClassState.setState((s) => s.fetchSelected(
            DataMultiSelectClass(
                classId: element.classId,
                courseId: element.courseId,
                className: element.className,
                courseName: element.courseName)));
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  void setAttachments(DataFile file) {
    attachments.add(Attachments(file: file.file, fileName: file.fileName));
    setState(() {});
  }

  void removeAttachments(int index) {
    attachments.removeAt(index);
    setState(() {
      attachments = attachments;
    });
  }

  void initAttachments() {}

  @override
  Widget build(BuildContext context) {
    print(_tanggalMulai.value.text);
    return WillPopScope(
      onWillPop: () async {
        await progressUploadState.setState((s) => s.setProgress(0),
            silent: true);
        await GlobalState.multiSelectClass().refresh();
        return true;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 1,
            backgroundColor: Colors.white,
            shadowColor: Colors.transparent,
            centerTitle: true,
            title: TitleAppbar(
              label: 'Update Tugas',
            ),
            leading: IconButton(
                icon: Icon(
                  Icons.close,
                  color: SiswamediaTheme.green,
                ),
                onPressed: () {
                  Navigator.pop(context);
                  progressUploadState.refresh();
                  GlobalState.multiSelectClass().refresh();
                }),
          ),
          body: GestureDetector(
            onTap: () {
              if (descNode.hasFocus || nameNode.hasFocus) {
                descNode.unfocus();
                nameNode.unfocus();
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              width: S.w * .9,
              child: ListView(
                children: [
                  TextField(
                    controller: _name,
                    focusNode: nameNode,
                    decoration: InputDecoration(
                        hintText: 'Nama Tugas',
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: SiswamediaTheme.green),
                        )),
                  ),
                  DateTimePicker(
                    controller: _tanggalMulai,
                    type: DateTimePickerType.dateTime,
                    dateMask: 'MMM dd, yyyy - HH:mm',
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2100),
                    icon: Icon(Icons.calendar_today_outlined),
                    dateLabelText: 'Tanggal mulai',
                    onChanged: (val) {
                      Utils.log(_startDate, info: 'start date');
                      setState(() => _startDate = val);
                    },
                    //locale: Locale('en', 'US'),
                  ),
                  DateTimePicker(
                    controller: _tanggalBerakhir,
                    type: DateTimePickerType.dateTime,
                    dateMask: 'MMM dd, yyyy - HH:mm',
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2100),
                    icon: Icon(Icons.calendar_today_outlined),
                    dateLabelText: 'Tanggal berakhir',
                    onChanged: (val) {
                      setState(() => _endDate = val);
                      Utils.log(_endDate, info: 'end date');
                      Utils.log(DateTime.parse(_endDate).toUtc(),
                          info: 'end date utc');
                    },
                  ),
                  SizedBox(height: 20),
                  CustomText('Tentukkan Kelas', Kind.heading3),
                  SizedBox(height: 5),
                  MultiSelectClass(
                    onTap: () => navigate(
                        context,
                        MultiSelectClassPage(
                          type: 'update',
                        )),
                  ),
                  SizedBox(height: 10),
                  StateBuilder<MultiSelectClassState>(
                      observe: () => GlobalState.multiSelectClass(),
                      builder: (context, data) {
                        return Wrap(
                          spacing: 10,
                          runSpacing: 10,
                          children: data.state.selected.values
                              .map((e) => Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 14, vertical: 8),
                                    decoration: BoxDecoration(
                                        color: SiswamediaTheme.lightBorder,
                                        borderRadius: radius(6)),
                                    child: Text(
                                        '${e.className} - ${e.courseName}',
                                        style: mediumBlack.copyWith(
                                            color: Color(0xFF4B4B4B),
                                            fontSize: S.w / 30)),
                                  ))
                              .toList(),
                        );
                      }),
                  SizedBox(height: 10),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 12),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText('Deskripsi Tugas', Kind.heading3),
                          SizedBox(height: 10),
                          TextField(
                            controller: _description,
                            minLines: 3,
                            maxLines: 5,
                            focusNode: descNode,
                            decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide:
                                      BorderSide(color: SiswamediaTheme.green),
                                ),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(color: Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 20),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(
                                        width: 1.0,
                                        style: BorderStyle.solid,
                                        color: Color(0xFFD8D8D8)))),
                          ),
                        ],
                      )),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Lampiran (Opsional)'),
                      SizedBox(height: 10),
                      GestureDetector(
                          onTap: () async {
                            await uploadFile(_setUploadProgress, context);
                          },
                          child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  border:
                                      Border.all(color: SiswamediaTheme.green)),
                              child: Text('Pilih File'))),
                      if (progressUploadState.state.progress != 0)
                        Container(
                            width: S.w,
                            margin: EdgeInsets.symmetric(
                                // horizontal: width * 0.05,
                                vertical: 3),
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey[300]),
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color:
                                          SiswamediaTheme.grey.withOpacity(0.2),
                                      offset: const Offset(0.2, 0.2),
                                      blurRadius: 3.0),
                                ],
                                color: Colors.white),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Uploading... $_progressPercentValue%'),
                                  SizedBox(height: 5),
                                  LinearPercentIndicator(
                                    width: S.w - 35 * 2,
                                    lineHeight: 8.0,
                                    percent: _progressValue,
                                    backgroundColor: Colors.grey[300],
                                    progressColor: SiswamediaTheme.green,
                                  ),
                                  SizedBox(height: 5),
                                  Container(
                                    width: S.w * .8,
                                    child: Text(
                                      '${Format.formatByte(progressUploadState.state.progress)}/${Format.formatByte(progressUploadState.state.contentLength)}',
                                      textAlign: TextAlign.right,
                                    ),
                                  ),
                                ])),
                      Container(
                        child: Column(
                            children: List.generate(
                                attachments.length,
                                (index) => GestureDetector(
                                    onTap: () async {
                                      var fileName = attachments[index].file;
                                      if (await canLaunch(fileName)) {
                                        await launch(fileName);
                                      } else {
                                        throw 'Could not launch $fileName';
                                      }
                                    },
                                    child: Container(
                                        width: S.w,
                                        height: 42,
                                        margin: EdgeInsets.symmetric(
                                            // horizontal: width * 0.05,
                                            vertical: 3),
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey[300]),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            boxShadow: <BoxShadow>[
                                              BoxShadow(
                                                  color: SiswamediaTheme.grey
                                                      .withOpacity(0.2),
                                                  offset:
                                                      const Offset(0.2, 0.2),
                                                  blurRadius: 3.0),
                                            ],
                                            color: Colors.white),
                                        child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  attachments[index].fileName,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  removeAttachments(index);
                                                },
                                                child: Text(
                                                  'Hapus',
                                                  style: TextStyle(
                                                      color: Colors.red),
                                                ),
                                              )
                                              // Spacer(),
                                            ])))).toList()),
                      ),
                    ],
                  ),
                  if (!isLoading)
                    Container(
                        height: 40,
                        width: S.w,
                        margin: EdgeInsets.symmetric(vertical: 26),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [SiswamediaTheme.shadowContainer],
                            color: SiswamediaTheme.green),
                        child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () async {
                                if (progressUploadState.state.progress != 0) {
                                  return CustomFlushBar.errorFlushBar(
                                      'Ada lampiran yang sedang diupload',
                                      context);
                                }
                                if (_name.text == '') {
                                  return CustomFlushBar.errorFlushBar(
                                      'Nama tugas belum di isi!', context);
                                }
                                // if (attachments.isEmpty) {
                                //   return CustomFlushBar.errorFlushBar(
                                //       'Harap sertakan lampiran', context,scaffoldKey);
                                // }
                                if (_startDate == '') {
                                  return CustomFlushBar.errorFlushBar(
                                    'Tanggal Mulai belum di isi!',
                                    context,
                                  );
                                }
                                if (_endDate == '') {
                                  return CustomFlushBar.errorFlushBar(
                                    'Tanggal Selesai belum di isi!',
                                    context,
                                  );
                                }

                                await createTugas(context: context);
                              },
                              child: Center(
                                child: Text(
                                  'Update Tugas',
                                  style: semiWhite.copyWith(fontSize: S.w / 28),
                                ),
                              ),
                            )))
                ],
              ),
            ),
          )),
    );
  }

  Future<void> createTugas({BuildContext context}) async {
    setState(() {
      isLoading = true;
      message = 'Sedang mengecek tanggal';
    });

    SiswamediaTheme.infoLoading(context: context, info: message);
    var dateStart = DateTime.parse(_startDate).toUtc();
    var dateEnd = DateTime.parse(_endDate).toUtc();

    Utils.log(dateStart, info: 'date start');
    Utils.log(dateEnd, info: 'date end');

    if (dateEnd.isBefore(dateStart)) {
      setState(() {
        isLoading = false;
      });
      Navigator.pop(context);
      return CustomFlushBar.errorFlushBar(
        'Tanggal Selesai harus lebih besar dari tanggal mulai!',
        context,
      );
    }

    setState(() {
      message = 'mengupdate tugas';
    });
    var listAttachment = <Map<String, String>>[];
    var deletedClass = <Map<String, dynamic>>[];
    var addedClass = <Map<String, dynamic>>[];
    attachments.forEach((element) {
      listAttachment.add(<String, String>{
        'file_name': element.fileName,
        'file': element.file
      });
    });
    tugas.listClassCourse.forEach((element) {
      if (!multiSelectClassState.state.selected.containsKey(element.classId)) {
        deletedClass.add(<String, dynamic>{
          'assessment_relation_id': element.assessmentRelationId
        });
      }
    });

    multiSelectClassState.state.selected.forEach((key, value) {
      if (tugas.listClassCourse
          .where((element) => element.classId == key)
          .isEmpty) {
        addedClass.add(
            <String, dynamic>{'class_id': key, 'course_id': value.courseId});
      }
    });
    var dataMap = <String, dynamic>{
      'name': _name.text,
      'description': _description.text,
      'start_date': dateStart.toIso8601String(),
      'end_date': dateEnd.toIso8601String(),
      'attachments': listAttachment,
      'deleted_class_course': deletedClass,
      'added_class_course': addedClass,
    };
    var result =
        await AssignmentServices.updateAssessment(widget.assessmentId, dataMap);
    if (result.statusCode == 200) {
      Navigator.pop(context);
      setState(() {
        message = '';
      });

      var data = DataAssignmentReference(
        id: widget.assessmentId,
        startDate: dateStart.toIso8601String(),
        endDate: dateEnd.toIso8601String(),
        name: _name.text,
        description: _description.text,
      );

      // await Future<void>.delayed(Duration(milliseconds: 300)).then(
      //     (_) async => );
      await scheduleState
          .setState((s) => s.updateAssignment(widget.assessmentId, data));

      //todo
      Navigator.pop(context);
      CustomFlushBar.successFlushBar(
        'Berhasil mengupdate tugas',
        context,
      );
    } else {
      Navigator.pop(context);
      setState(() {
        message = '';
      });
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        result.message.toString() + '!',
        context,
      );
    }
  }

  static HttpClient getHttpClient() {
    var httpClient = HttpClient()
      ..connectionTimeout = const Duration(seconds: 10)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

    return httpClient;
  }

  Future<void> uploadFile(
      OnUploadProgressCallback onUploadProgress, BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    try {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context,
          builder: (_) {
            return Center(child: CircularProgressIndicator());
          });
      var pick = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.any,
        // allowedExtensions: ['jpg', 'pdf', 'doc']
      );
      print('aduh');
      var file = pick.files.first;
      print(file.name);
      print(file.path);

      var client2 = http.Client();
      //todo file!=null
      if (file != null) {
        var totalByteLength = File(file.path).lengthSync();
        var type = lookupMimeType(file.path);
        SiswamediaGoogleSignIn.getInstance().scopes.addAll([
          'https://www.googleapis.com/auth/drive.file',
        ]);
        await SiswamediaGoogleSignIn.getInstance().signInSilently();
        var headers =
            await SiswamediaGoogleSignIn.getInstance().currentUser.authHeaders;
        var requestHeaders = <String, String>{
          'X-Upload-Content-Type': type,
          'Content-Type': 'application/json',
          'X-Upload-Content-Length': '$totalByteLength',
        };
        headers.forEach((key, value) {
          requestHeaders[key] = value;
        });
        var resp = await client2.post(
            '$baseGoogleApiUrl/upload/drive/v3/files?uploadType=resumable',
            body: json.encode(<String, int>{
              'Content-Length': totalByteLength,
            }),
            headers: requestHeaders);
        print('response');
        print(resp.statusCode);
        print(resp.headers);
        final httpClient = getHttpClient();
        //3145727
        print(file.size);
        // if (file.size > 1048576) {
        //   throw LargeFileException(cause: 'Data lebih dari 1GB');
        // }

        final client = HttpClient();
        final response =
            await client.putUrl(Uri.parse(resp.headers['location']));

        var byteCount = 0;

        //todo
        // response.contentLength = File(file.path).lengthSync();
        response.contentLength = totalByteLength;
        print('content lenght ${response.contentLength}');
        response.headers.set(HttpHeaders.contentTypeHeader, type);
        response.headers.set(HttpHeaders.contentLengthHeader, totalByteLength);
        response.headers.set(
            'Content-Range', 'bytes 0-${totalByteLength - 1}/$totalByteLength');

        headers.forEach((key, value) {
          response.headers.add(key, value);
        });

        final stream = await File(file.path).openRead();

        Navigator.pop(context);
        var stream2 = stream.transform<List<int>>(
          StreamTransformer.fromHandlers(
            handleData: (data, sink) {
              byteCount += data.length;
              print(byteCount);
              sink.add(data);
              if (onUploadProgress != null) {
                print('uploading');
                onUploadProgress(byteCount, totalByteLength, context);
                // CALL STATUS CALLBACK;
              }
            },
            handleError: (error, stack, sink) {},
            handleDone: (sink) {
              sink.close();
            },
          ),
        );

        await response.addStream(stream2);

        final httpResponse = await response.close();
        var statusCode = httpResponse.statusCode;

        print(statusCode);
        if (statusCode ~/ 100 != 2) {
          print(statusCode);
          throw Exception(
              'Error uploading file, Status code: ${httpResponse.statusCode}');
        } else {
          return await readResponseAsString(httpResponse, file.name, headers);
        }
        // return response;
      } else {
        progressUploadState.state.progress = 0;
        // return null;
      }
    } on LargeFileException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } on ErrorUploadException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } catch (e) {
      Navigator.pop(context);
      print(e);
    }
  }

  Future<String> readResponseAsString(HttpClientResponse response,
      String filename, Map<String, String> headers) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
    }, onDone: () async {
      try {
        completer.complete(contents.toString());
        var result = await completer.future;
        var data = json.decode(result) as Map<String, dynamic>;
        var client = http.Client();
        print(data['id']);
        var resp = await client.patch(
            '$baseGoogleApiUrl/drive/v3/files/${data['id']}?fields=*',
            body: json.encode({'name': filename}),
            headers: headers);
        print(resp.body);
        await client
            .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
                body: json.encode({'role': 'reader', 'type': 'anyone'}),
                headers: headers)
            .then((value) {
          print(value.body);
          setAttachments(DataFile(
              fileName: filename,
              file:
                  'https://drive.google.com/uc?export=view&id=${data['id']}'));
          CustomFlushBar.successFlushBar(
            'Berhasil Mengupload Lampiran, jika mengupload video Kami butuh waktu untuk memproses vido anda silahkan tunggu hingga video dapat ditampilkan',
            context,
          );
          progressUploadState.state.setProgress(0);
        });
      } catch (e) {
        print('error');
        print(e);
      }
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes));
    progressUploadState.setState((s) => s.setContent(totalBytes));
    var __progressValue =
        Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }
}
