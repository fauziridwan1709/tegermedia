part of '_assignment.dart';

class ReplyAssignment extends StatefulWidget {
  final int refrenceId;
  final int assessmentsId;
  final List<ReplyAttachments> attachment;
  final List<CommentsAssignment> comments;
  final ListData data;
  final Function() refresh;

  ReplyAssignment(
      {@required this.refrenceId,
      @required this.assessmentsId,
      this.comments,
      this.data,
      this.refresh,
      this.attachment});

  @override
  _ReplyAssignmentState createState() => _ReplyAssignmentState();
}

class _ReplyAssignmentState extends State<ReplyAssignment> {
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  final assignmentState = Injector.getAsReactive<AssignmentState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _score = TextEditingController();
  var controller = TextEditingController();
  var fNodeChat = FocusNode();
  List<ReplyAttachments> dataImage = [];
  List<CommentsAssignment> comments = [];
  ModelDetailAssessments _data;
  bool _isLoading = false;
  bool error = false;
  String text = '';

  @override
  void initState() {
    super.initState();
    _refreshData();
    dataImage = widget.attachment;
    comments = widget.comments;
    assignmentState.setState((s) => s.updateComment(data: widget.data));
    WidgetsBinding.instance.addPostFrameCallback((_) {
      assignmentState.notify();
    });
  }

  @override
  void dispose() {
    _score.dispose();
    _isLoading = false;
    super.dispose();
  }

  void _refreshData() async {
    _isLoading = true;
    var data = await AssignmentServices.getDetail(
        refrenceID: widget.refrenceId, assesmentID: widget.assessmentsId);
    // widget.refresh;
    setState(() {
      _isLoading = false;
      comments = data.data.comments;
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    var hehe = Color(0xffE8F5D6);
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text('Reply', style: semiBlack.copyWith(fontSize: S.w / 22)),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        body: _isLoading
            ? Container(
                margin: EdgeInsets.symmetric(
                    horizontal: defaultMargin, vertical: defaultMargin),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(children: [
                        SkeletonLine(width: 40, height: 40, borderRadius: 60),
                        SizedBox(
                          width: 10,
                        ),
                        Column(children: [
                          SkeletonLine(width: 100, height: 10, borderRadius: 5),
                          SizedBox(
                            height: 5,
                          ),
                          SkeletonLine(width: 100, height: 8, borderRadius: 5),
                        ])
                      ]),
                      SkeletonLine(width: 100, height: 10, borderRadius: 5),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(width: 100, height: 10, borderRadius: 5),
                    ]))
            : Container(
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height),
                child: Stack(children: [
                  SingleChildScrollView(
                    child: Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: defaultMargin, vertical: defaultMargin),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 20),
                              CustomText('Reply attachment', Kind.heading3),
                              if (dataImage.isEmpty)
                                CustomText('Tidak ada reply attachment',
                                    Kind.descBlack),
                              Container(
                                child: Column(
                                    children: List.generate(
                                        dataImage.length,
                                        (index) => GestureDetector(
                                            onTap: () async {
                                              var fileName =
                                                  dataImage[index].file;
                                              if (await canLaunch(fileName)) {
                                                await launch(fileName);
                                              } else {
                                                throw 'Could not launch $fileName';
                                              }
                                            },
                                            child: Container(
                                                width: S.w,
                                                margin: EdgeInsets.symmetric(
                                                    // horizontal: width * 0.05,
                                                    vertical: 3),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 5),
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color:
                                                            Colors.grey[300]),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    boxShadow: <BoxShadow>[
                                                      BoxShadow(
                                                          color: SiswamediaTheme
                                                              .grey
                                                              .withOpacity(0.2),
                                                          offset: const Offset(
                                                              0.2, 0.2),
                                                          blurRadius: 3.0),
                                                    ],
                                                    color: Colors.white),
                                                child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Expanded(
                                                          child: Text(
                                                              dataImage[index]
                                                                  .fileName)),
                                                      Spacer(),
                                                      Text(
                                                        'Lihat',
                                                        style: semiWhite.copyWith(
                                                            fontSize: S.w / 32,
                                                            color:
                                                                SiswamediaTheme
                                                                    .green),
                                                      )
                                                    ])))).toList()),
                              ),
                              SizedBox(height: 25),
                              if (comments.isNotEmpty)
                                CustomText('Komentar', Kind.heading3),
                              ListView(
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                // mainAxisSize: MainAxisSize.max,
                                children: comments.map((e) {
                                  var date =
                                      DateTime.parse(e.sentDate).toLocal();
                                  var start = DateTime.now().toLocal();
                                  var status = e.senderUserId ==
                                      GlobalState.profile().state.profile.id;
                                  //todo or userId
                                  return Column(
                                    crossAxisAlignment: status
                                        ? CrossAxisAlignment.end
                                        : CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.symmetric(vertical: 14),
                                        constraints: BoxConstraints(
                                            maxWidth: width * .65),
                                        child: Column(
                                          crossAxisAlignment: status
                                              ? CrossAxisAlignment.end
                                              : CrossAxisAlignment.start,
                                          children: [
                                            CustomContainer(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 6, horizontal: 8),
                                              color:
                                                  status ? hehe : Colors.white,
                                              boxShadow: [
                                                SiswamediaTheme.shadowContainer
                                              ],
                                              radius: radius(12),
                                              child: Column(
                                                crossAxisAlignment: status
                                                    ? CrossAxisAlignment.end
                                                    : CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  CustomText(e.senderName,
                                                      Kind.heading3),
                                                  Text(e.comment,
                                                      textAlign: status
                                                          ? TextAlign.right
                                                          : TextAlign.left,
                                                      maxLines: 5,
                                                      softWrap: true,
                                                      style: descBlack.copyWith(
                                                          fontSize: S.w / 32)),
                                                ],
                                              ),
                                            ),

                                            // CustomText(
                                            //     e.comment, Kind.descBlack),
                                            SizedBox(height: 5),
                                            Text(
                                                '${DateTime.now().difference(date).inDays == 0 && DateTime.now().day - date.day == 0 ? "Hari ini" : start.yearMonthDay.difference(date.yearMonthDay).inDays < 2 ? "Kemarin" : start.difference(date).inDays.toString() + " Hari yang lalu"} ${date.hour < 10 ? "0" : ""}${date.hour}.${date.minute < 10 ? "0" : ""}${date.minute}',
                                                style: descBlack.copyWith(
                                                    fontSize: S.w / 36)),
                                          ],
                                        ),
                                      ),
                                    ],
                                  );
                                  // return
                                  // return Row(
                                  //   mainAxisAlignment: e.senderUserId ==
                                  //           GlobalState.profile()
                                  //               .state
                                  //               .profile
                                  //               .userId
                                  //       ? MainAxisAlignment.end
                                  //       : MainAxisAlignment.start,
                                  //   children: [
                                  //
                                  //   ],
                                  // );
                                }).toList(),
                              ),
                              SizedBox(height: 100)
                            ])),
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      child: Container(
                        decoration:
                            BoxDecoration(color: Colors.white, boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 0),
                              blurRadius: 2,
                              spreadRadius: 2,
                              color: Colors.grey.withOpacity(.2))
                        ]),
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(
                                  horizontal: S.w * .04, vertical: S.w * .05),
                              child: SingleChildScrollView(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 15),
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15),
                                        constraints: BoxConstraints(
                                            maxHeight: 100, minHeight: 40),
                                        decoration: BoxDecoration(
                                          color: Color(0xfff8f8f8),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: SingleChildScrollView(
                                            child: TextField(
                                              controller: controller,
                                              focusNode: fNodeChat,
                                              maxLines: null,
                                              // onChanged: (value) {
                                              //   print(value.length);
                                              // },
                                              style: normalText(context),
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Tulis Komentar disini',
                                                hintStyle: hintText(context),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: S.w * .1,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Material(
                                            color: Colors.transparent,
                                            child: InkWell(
                                              onTap: () async {
                                                var pref =
                                                    await SharedPreferences
                                                        .getInstance();
                                                var map = <String, String>{
                                                  "comment": controller.text,
                                                  "sent_date": DateTime.now()
                                                      .toUtc()
                                                      .toIso8601String()
                                                };
                                                var url =
                                                    '$baseUrl/$version/assessments/${widget.assessmentsId}/reference/${widget.refrenceId}/comment';
                                                var resp = await client.post(
                                                    url,
                                                    body: json.encode(map),
                                                    headers: {
                                                      'Authorization': pref
                                                          .getString(AUTH_KEY),
                                                      'Content-Type':
                                                          'application/json'
                                                    });

                                                if (resp.statusCode
                                                    .isSuccessOrCreated) {
                                                  controller.text = '';
                                                  _refreshData();
                                                }
                                              },
                                              child: Icon(Icons.send,
                                                  size: 26,
                                                  color: SiswamediaTheme.green),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      )),
                ]),
              ));
  }
}
