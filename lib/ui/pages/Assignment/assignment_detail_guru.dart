part of '_assignment.dart';

class AssignmentDetailGuru extends StatefulWidget {
  final int assessmentID;
  final String endDate;
  final String startDate;
  final String tugasName;
  final String tugasDescription;
  final List<MainAttachments> tugasAttachments;
  final Function refreshData;

  AssignmentDetailGuru(
      {@required this.assessmentID,
      this.refreshData,
      this.endDate,
      this.startDate,
      this.tugasName,
      this.tugasDescription,
      this.tugasAttachments});

  @override
  _AssignmentDetailGuruState createState() => _AssignmentDetailGuruState();
}

class _AssignmentDetailGuruState extends State<AssignmentDetailGuru> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  DataAssessmentsTeacherById tugas;
  List<Attachments> dataImage = [];
  bool isLoading = false;
  bool isProgress = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = true;
    });
    getData();
  }

  @override
  void dispose() {
    dataImage = [];
    isLoading = false;
    super.dispose();
  }

  Future<void> getData() async {
    setState(() {
      isLoading = true;
    });

    var data = await AssignmentServices.getAssessmentsTeacherById(
        assessmentId: widget.assessmentID);

    if (data.statusCode == 200) {
      print(data.data.toJson());
      setState(() {
        tugas = data.data;
        dataImage = data.data.attachments;
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  String desc =
      'Untuk soal esai yang membutuhkan rumus silahkan kerjakan di kertas, dan kumpulkan dengan cara foto jawaban per soal lalu unggah foto anda di tombol unggah di bawah ini (pastikan foto nya jelas)';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text('Tugas', style: semiBlack.copyWith(fontSize: S.w / 22)),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: isLoading
              ? Container(
                  height: 100,
                  width: S.w,
                  child: Center(child: CircularProgressIndicator()))
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      width: S.w,
                      margin: EdgeInsets.symmetric(vertical: S.w * .03),
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(tugas.courseName.capitalizeFirstOfEach,
                              style: semiBlack.copyWith(
                                  color: SiswamediaTheme.green,
                                  fontSize: S.w / 22)),
                          Text(tugas.name.capitalizeFirstOfEach,
                              style: semiBlack.copyWith(fontSize: S.w / 24)),
                          SizedBox(height: 5),
                          Text(
                              'Waktu Pengerjaan dimulai: ${formattedDate(tugas.startDate)}',
                              style: semiBlack.copyWith(
                                  color: Colors.black54,
                                  fontSize: S.w / 32,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(height: 5),
                          Text(
                              'Waktu Pengerjaan berakhir: ${formattedDate(tugas.endDate)}',
                              style: semiBlack.copyWith(
                                  color: Colors.black54,
                                  fontSize: S.w / 32,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(height: 15),
                          CustomText('Deskripsi tugas', Kind.heading3),
                          SelectableLinkify(
                              onOpen: (link) => launch(link.url),
                              text: tugas.description.isEmpty
                                  ? 'Tidak ada deskripsi'
                                  : tugas.description,
                              style: semiBlack.copyWith(
                                  color: Colors.black54,
                                  fontSize: S.w / 32,
                                  fontWeight: FontWeight.normal)),
                          SizedBox(height: 10),
                          SizedBox(
                            height: 15,
                          ),
                          CustomText('File Lampiran :', Kind.heading3),
                          SizedBox(height: 5),
                          // tugas.attachments.length > 1
                          //     ? Container(
                          //         height: 60,
                          //         color: Colors.orange.withOpacity(0.6),
                          //         child: Center(child: Text('Tidak ada file')))
                          //     :
                          Column(
                            children: tugas.attachments
                                .map((e) => GestureDetector(
                                    onTap: () async {
                                      var fileName = e.file;
                                      if (await canLaunch(fileName)) {
                                        await launch(fileName);
                                      } else {
                                        throw 'Could not launch $fileName';
                                      }
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(bottom: 2),
                                        alignment: Alignment.centerLeft,
                                        height: 40,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                  color: SiswamediaTheme.green,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          50)),
                                              child: Icon(
                                                Icons.file_download,
                                                size: 18,
                                                color: Colors.white,
                                              ),
                                            ),
                                            SizedBox(width: 10),
                                            Align(
                                                alignment: Alignment.centerLeft,
                                                child: Container(
                                                    width: S.w / 2 + 80,
                                                    child: Text(
                                                      e.fileName,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: semiBlack,
                                                    ))),
                                          ],
                                        ))))
                                .toList(),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: S.w,
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      margin: EdgeInsets.symmetric(
                          vertical: S.w * .03, horizontal: S.w * .03),
                      decoration: BoxDecoration(
                        color: Color(0xffE8F5D6),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Catatan',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: S.w / 25,
                                    fontWeight: FontWeight.w700)),
                            Text(desc,
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: S.w / 28,
                                )),
                          ]),
                    ),
                    SizedBox(height: 30),
                    Container(
                      width: S.w,
                      margin: EdgeInsets.only(
                          left: defaultMargin, bottom: defaultMargin),
                      child: Text(
                        'Beri Nilai Siswa',
                        style: semiBlack.copyWith(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                    ListStudents(tugas: tugas),
                    SizedBox(height: 150)
                  ],
                ),
        ),
      ),
    );
  }
}

class ListStudents extends StatelessWidget {
  const ListStudents({
    Key key,
    @required this.tugas,
  }) : super(key: key);

  final DataAssessmentsTeacherById tugas;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: S.w,
        child: FutureBuilder<ModelAssignmentReference>(
            future: AssignmentServices.getAssessmentsReference(
                assessmentsId: tugas.id),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Row(
                    children: List.generate(
                        4,
                        (index) => Container(
                            margin: EdgeInsets.only(
                                left: index == 0 ? defaultMargin : 5,
                                right: index == 5 ? defaultMargin : 5),
                            child: Column(
                              children: [
                                SkeletonLine(
                                  height: 60,
                                  width: 60,
                                  borderRadius: 50,
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                SkeletonLine(
                                  width: 60,
                                  height: 10,
                                )
                              ],
                            ))).toList());
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data.data.isEmpty) {
                  return Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: defaultMargin, vertical: 10),
                      width:
                          MediaQuery.of(context).size.width - defaultMargin * 2,
                      child: Text('Daftar siswa tidak tersedia...'));
                } else {
                  return GridView.count(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    crossAxisCount: getStudentAxisCount(
                        MediaQuery.of(context).size.aspectRatio),
                    padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                    childAspectRatio: .75,
                    children: snapshot.data.data
                        .map((e) => Container(
                              margin: EdgeInsets.only(left: 5, right: 5),
                              child: GestureDetector(
                                  onTap: () {
                                    context.push<void>(ScoreAssignment(
                                        refrenceId: e.id,
                                        assessmentsId: tugas.id));
                                  },
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Stack(children: [
                                          Container(
                                            height: 60,
                                            width: 60,
                                            margin: EdgeInsets.only(bottom: 5),
                                            foregroundDecoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: e.imageUser == null
                                                    ? null
                                                    : DecorationImage(
                                                        image: NetworkImage(
                                                            e.imageUser),
                                                        fit: BoxFit.cover)),
                                            decoration: BoxDecoration(
                                                // color: Colors.grey[300],
                                                shape: BoxShape.circle,
                                                image: DecorationImage(
                                                    image: AssetImage(
                                                        'assets/profile-default.png'))),
                                          ),
                                          Positioned(
                                              bottom: e.status ==
                                                          'SUDAH DIKERJAKAN' &&
                                                      e.nilai != ''
                                                  ? null
                                                  : 0,
                                              right: 0,
                                              top: e.status ==
                                                          'SUDAH DIKERJAKAN' &&
                                                      e.nilai != ''
                                                  ? 0
                                                  : null,
                                              left: e.status ==
                                                          'SUDAH DIKERJAKAN' &&
                                                      e.nilai != ''
                                                  ? 0
                                                  : null,
                                              child: e.nilai == ''
                                                  ? e.status ==
                                                          'BELUM DIKERJAKAN'
                                                      ? Container(
                                                          height: 25,
                                                          width: 25,
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  SiswamediaTheme
                                                                      .green,
                                                              shape: BoxShape
                                                                  .circle),
                                                          child: Center(
                                                              child: Icon(
                                                                  Icons.edit,
                                                                  color: Colors
                                                                      .white,
                                                                  size: 18)),
                                                        )
                                                      : Container(
                                                          height: 25,
                                                          width: 25,
                                                          decoration:
                                                              BoxDecoration(
                                                                  color: Colors
                                                                      .green,
                                                                  shape: BoxShape
                                                                      .circle),
                                                          child: Icon(
                                                              MdiIcons.checkAll,
                                                              color:
                                                                  Colors.white,
                                                              size: 18),
                                                        )
                                                  // ignore: unrelated_type_equality_checks
                                                  : Container(
                                                      height: 60,
                                                      width: 60,
                                                      margin: EdgeInsets.only(
                                                          bottom: 5),
                                                      decoration: BoxDecoration(
                                                          color: Colors.white
                                                              .withOpacity(0.9),
                                                          shape:
                                                              BoxShape.circle),
                                                      child: Center(
                                                          child: Text(
                                                              '${e.nilai}',
                                                              style: semiBlack.copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                  fontSize:
                                                                      S.w / 23,
                                                                  color: SiswamediaTheme
                                                                      .green))),
                                                    ))
                                        ]),
                                        SizedBox(
                                          width: 70,
                                          child: AutoSizeText(
                                            e.fullName,
                                            textAlign: TextAlign.center,
                                            style: semiBlack.copyWith(
                                                fontWeight: FontWeight.w600),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        )
                                      ])),
                            ))
                        .toList(),
                  );
                }
              } else {
                return Text('Terjadi Kesalahan');
              }
            }));
  }

  void dialogDev(BuildContext context,
      {String text = 'Siswa ini belum mengerjakan tugas',
      String title = 'Informasi',
      DateTime endDate,
      int referenceId}) {
    // var S.w = MediaQuery.of(context).size.width;
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              // String role = someCapitalizedString(data.role);
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .1, horizontal: S.w * .05),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            height: 100,
                            width: 170,
                            margin: EdgeInsets.only(bottom: 12),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/icons/profile/role.png'))),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(title,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w800)),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                              text +
                                  '${endDate.isBefore(DateTime.now()) ? ', dan tugas sudah berakhir' : ''}',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12)),
                          SizedBox(
                            height: 16,
                          ),
                          if (endDate.isBefore(DateTime.now()))
                            RoundedButton(
                              color: SiswamediaTheme.green,
                              text: 'Beri nilai nol siswa',
                              onPressed: () {
                                Navigator.pop(context);
                                showDialog<void>(
                                    context: context,
                                    builder: (context) => StatefulBuilder(
                                            builder: (BuildContext context,
                                                void Function(void Function())
                                                    setState) {
                                          return CustomDialog(
                                            title: 'Konfirmasi',
                                            description:
                                                'Yakin memberikan nol pada siswa ini karena tidak mengerjakan tugas?',
                                            image:
                                                'assets/dialog/pilih_kelas.png',
                                            actions: ['Yakin'],
                                            onTap: () async {
                                              var result =
                                                  await AssignmentServices
                                                      .createScoreAssignment(
                                                          assessmentsId:
                                                              tugas.id,
                                                          referenceId:
                                                              referenceId,
                                                          nilai: '90');
                                              if (result.statusCode == 200) {
                                                setState(() {
                                                  Navigator.pop(context);
                                                });
                                                CustomFlushBar.successFlushBar(
                                                  'Berhasil',
                                                  context,
                                                );
                                              } else {
                                                setState(() {
                                                  Navigator.pop(context);
                                                });
                                                CustomFlushBar.errorFlushBar(
                                                  'Gagal',
                                                  context,
                                                );
                                              }
                                            },
                                          );
                                        }));
                              },
                            )
                        ],
                      )));
            }));
  }
}

int getStudentAxisCount(double ratio) {
  if (ratio > 2.0) {
    return 8;
  } else {
    return 4;
  }
}
