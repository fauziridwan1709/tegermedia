part of '_assignment.dart';

typedef OnUploadProgressCallback = void Function(
    int sentBytes, int totalBytes, BuildContext context);

class AssignmentDetail extends StatefulWidget {
  final String role;
  final String tugasName;
  final String startDate;
  final String endDate;
  final int referenceID;
  final int assessmentID;
  final bool newComment;
  final Function refreshData;

  AssignmentDetail(
      {@required this.referenceID,
      @required this.assessmentID,
      this.role,
      this.tugasName,
      this.startDate,
      this.endDate,
      this.newComment,
      this.refreshData});

  @override
  _AssignmentDetailState createState() => _AssignmentDetailState();
}

class _AssignmentDetailState extends State<AssignmentDetail> {
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  final assignmentState = Injector.getAsReactive<AssignmentState>();
  final authState = Injector.getAsReactive<AuthState>();
  final descriptionController = TextEditingController();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  var descNode = FocusNode();
  DataDetailAssessments tugas;
  List<MainAttachments> dataImage = [];
  bool isLoading = true;
  bool isLoadingDialog = false;
  bool isProgress = false;
  bool error = false;

  double _progressValue = 0;
  int _progressPercentValue = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = true;
    });
    getData();
  }

  @override
  void dispose() {
    dataImage = [];
    isLoading = false;
    progressUploadState.refresh();
    super.dispose();
  }

  void getData() async {
    setState(() {
      isLoading = true;
    });
    print(widget.assessmentID);
    var data = await AssignmentServices.getDetail(
        refrenceID: widget.referenceID, assesmentID: widget.assessmentID);
    descriptionController.text = data.data.description;

    if (data.statusCode == 200) {
      setState(() {
        tugas = data.data;
        print(tugas.mainDescription + ' ini mainnya');
        print(tugas.mainDescription.isEmpty);
        dataImage = data.data.attachments;
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  void setAttachments(DataFile file) {
    dataImage.add(MainAttachments(file: file.file, fileName: file.fileName));
    setState(() {});
  }

  void removeAttachments(int index) {
    dataImage.removeAt(index);
    setState(() {
      dataImage = dataImage;
    });
  }

  void updateAssessmentsStudents() async {
    var data = ModelRefrenceAssessmentsStudens(
        attachments: dataImage, status: 'Dikumpulkan', description: descriptionController.text);

    var response = await AssignmentServices.postAssessmentsStudents(
        assesmentID: widget.assessmentID, refrenceID: widget.referenceID, body: data);

    if (response.statusCode == 200 || response.statusCode == 201) {
      var assignmentState = Injector.getAsReactive<AssignmentState>();
      assignmentState.state.kumpulkanAssignment(widget.assessmentID);
      assignmentState.notify();
      setState(() {
        isLoadingDialog = false;
      });

      Navigator.pop(context);
      Navigator.pop(context);
      CustomFlushBar.successFlushBar(
        'Berhasil mengumpulkan tugas',
        context,
      );
      // widget.refreshData();
    } else {
      setState(() {
        isLoadingDialog = false;
      });
      CustomFlushBar.errorFlushBar(
        'Gagal mengumpulkan tugas',
        context,
      );
      print(response.statusCode);
    }
  }

  String desc =
      'Untuk soal esai yang membutuhkan rumus silahkan kerjakan di kertas, dan kumpulkan dengan cara foto jawaban per soal lalu unggah foto anda di tombol unggah di bawah ini (pastikan foto nya jelas)';
  @override
  Widget build(BuildContext context) {
    return StateBuilder<AssignmentState>(
        observe: () => assignmentState,
        builder: (context, snapshot) => Scaffold(
              key: scaffoldKey,
              appBar: AppBar(
                backgroundColor: Colors.white,
                shadowColor: Colors.transparent,
                centerTitle: true,
                title: Text('Tugas', style: TextStyle(color: Colors.black, fontSize: S.w / 23)),
                leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: SiswamediaTheme.green,
                    ),
                    onPressed: () => Navigator.pop(context)),
                actions: [
                  Center(
                    child: InkWell(
                      onTap: () => context.push<void>(ReplyAssignment(
                          refrenceId: tugas.id,
                          assessmentsId: tugas.assessmentId,
                          comments: tugas.comments,
                          data: assignmentState.state.assignmentData.data.listData
                              .where((element) => element.assessmentId == widget.assessmentID)
                              .first,
                          refresh: () {
                            getData();
                          },
                          attachment: tugas.replyAttachments)),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Stack(
                          children: [
                            Image.asset('assets/icons/small/speech-bubble.png',
                                height: 24, width: 24),
                            if (assignmentState.state.assignmentData?.data?.listData
                                    ?.where(
                                        (element) => element.assessmentId == widget.assessmentID)
                                    ?.first
                                    ?.newComment ??
                                false)
                              Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                  width: 8,
                                  height: 8,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: SingleChildScrollView(
                child: isLoading
                    ? Container(
                        height: 100, width: S.w, child: Center(child: CircularProgressIndicator()))
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            color: Colors.white,
                            width: S.w,
                            margin: EdgeInsets.symmetric(vertical: S.w * .03),
                            padding:
                                EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                  Expanded(
                                    child: Container(
                                        child: Text(widget.tugasName,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 3,
                                            style: semiBlack.copyWith(
                                                color: SiswamediaTheme.green,
                                                fontSize: S.w / 24,
                                                fontWeight: FontWeight.w700))),
                                  ),
                                  if (tugas.status == 'SUDAH DIKERJAKAN' ||
                                      tugas.status == 'Dikembalikan' ||
                                      DateTime.parse(widget.endDate).isBefore(DateTime.now()))
                                    Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: S.w * .01, horizontal: 20),
                                      child: Center(
                                        child: Text(
                                          'Nilai : ${tugas.nilai == '' ? 'Belum Dinilai' : formatNumber(tugas.nilai)}',
                                          maxLines: 1,
                                          style: TextStyle(color: Colors.white, fontSize: S.w / 32),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        color: SiswamediaTheme.green.withOpacity(0.8),
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                    )
                                ]),
                                SizedBox(
                                  height: 5,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text('Waktu Pengerjaan dimulai : ',
                                        style: descBlack.copyWith(
                                            color: Colors.black54, fontSize: S.w / 30)),
                                    Text('${formattedDate(widget.startDate).toString()}',
                                        style: semiBlack.copyWith(
                                            color: SiswamediaTheme.green, fontSize: S.w / 30))
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text('Waktu Pengerjaan berakhir : ',
                                        style: descBlack.copyWith(
                                            color: Colors.black54, fontSize: S.w / 30)),
                                    Text('${formattedDate(widget.endDate).toString()}',
                                        style: semiBlack.copyWith(
                                            color: SiswamediaTheme.green, fontSize: S.w / 30))
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                CustomText('Deskripsi tugas', Kind.heading3),
                                SelectableLinkify(
                                    onOpen: (link) => launch(link.url),
                                    text: (tugas.mainDescription.isNotEmpty
                                            ? tugas.mainDescription
                                            : '') ??
                                        'Tidak ada deskripsi',
                                    style: semiBlack.copyWith(
                                        color: Colors.black54,
                                        fontSize: S.w / 32,
                                        fontWeight: FontWeight.normal)),
                                SizedBox(
                                  height: 15,
                                ),
                                CustomText('File Lampiran :', Kind.heading3),
                                SizedBox(height: 5),
                                TugasDetailLampiran(
                                    attachments: tugas.mainAttachments,
                                    onView: (attachment) async {
                                      var fileName = attachment.file;
                                      if (await canLaunch(fileName)) {
                                        await launch(fileName);
                                      } else {
                                        throw 'Could not launch $fileName';
                                      }
                                    })
                              ],
                            ),
                          ),
                          Container(
                            width: S.w,
                            padding:
                                EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
                            margin:
                                EdgeInsets.symmetric(vertical: S.w * .03, horizontal: S.w * .03),
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.lightGreen,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                              Text('Catatan',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: S.w / 25,
                                      fontWeight: FontWeight.w700)),
                              Text(desc,
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: S.w / 28,
                                  )),
                            ]),
                          ),
                          Container(
                            width: S.w,
                            padding:
                                EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    '${tugas.status == 'BELUM DIKERJAKAN' ? 'Unggah' : ''} Jawaban',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: S.w / 25,
                                        fontWeight: FontWeight.w700)),
                                SizedBox(height: 15),
                                Container(
                                  child: Column(
                                      children: List.generate(
                                          dataImage.length,
                                          (index) => GestureDetector(
                                              onTap: () async {
                                                var fileName = dataImage[index].file;
                                                if (await canLaunch(fileName)) {
                                                  await launch(fileName);
                                                } else {
                                                  throw 'Could not launch $fileName';
                                                }
                                              },
                                              child: Container(
                                                  width: S.w,
                                                  margin: EdgeInsets.symmetric(
                                                      // horizontal: width * 0.05,
                                                      vertical: 3),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 10, vertical: 5),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(color: Colors.grey[300]),
                                                      borderRadius: BorderRadius.circular(5),
                                                      boxShadow: <BoxShadow>[
                                                        BoxShadow(
                                                            color: SiswamediaTheme.grey
                                                                .withOpacity(0.2),
                                                            offset: const Offset(0.2, 0.2),
                                                            blurRadius: 3.0),
                                                      ],
                                                      color: Colors.white),
                                                  child: Row(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: [
                                                        Expanded(
                                                            child: Text(dataImage[index].fileName)),
                                                        Spacer(),
                                                        tugas.status == 'SUDAH DIKERJAKAN'
                                                                // ||
                                                                //     tugas.status ==
                                                                //         'Dikumpulkan'
                                                                ||
                                                                tugas.status == 'Dikembalikan' ||
                                                                authState.state.currentState
                                                                    .classRole.isOrtu
                                                            ? Text(
                                                                'Lihat',
                                                                style: TextStyle(
                                                                    color: SiswamediaTheme.green),
                                                              )
                                                            : GestureDetector(
                                                                onTap: () {
                                                                  if (dataImage.length == 1 &&
                                                                      tugas.status !=
                                                                          'BELUM DIKERJAKAN') {
                                                                    return CustomFlushBar.errorFlushBar(
                                                                        'Tidak bisa menghapus attachment, minimal harus ada sebuah attachment',
                                                                        context);
                                                                  }
                                                                  removeAttachments(index);
                                                                },
                                                                child: Text(
                                                                  'Hapus',
                                                                  style:
                                                                      TextStyle(color: Colors.red),
                                                                ),
                                                              )
                                                      ])))).toList()),
                                ),
                                if (progressUploadState.state.progress != 0)
                                  Container(
                                      width: S.w,
                                      margin: EdgeInsets.symmetric(
                                          // horizontal: width * 0.05,
                                          vertical: 3),
                                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                      decoration: BoxDecoration(
                                          border: Border.all(color: Colors.grey[300]),
                                          borderRadius: BorderRadius.circular(5),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: SiswamediaTheme.grey.withOpacity(0.2),
                                                offset: const Offset(0.2, 0.2),
                                                blurRadius: 3.0),
                                          ],
                                          color: Colors.white),
                                      child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('Uploading... $_progressPercentValue%'),
                                            SizedBox(height: 5),
                                            LinearPercentIndicator(
                                              width: S.w - 35 * 2,
                                              lineHeight: 8.0,
                                              percent: _progressValue,
                                              backgroundColor: Colors.grey[300],
                                              progressColor: SiswamediaTheme.green,
                                            ),
                                            SizedBox(height: 5),
                                            Container(
                                              width: S.w * .8,
                                              child: Text(
                                                '${Format.formatByte(progressUploadState.state.progress)}/${Format.formatByte(progressUploadState.state.contentLength)}',
                                                textAlign: TextAlign.right,
                                              ),
                                            ),
                                          ])),
                                ((DateTime.parse(widget.endDate).isBefore(DateTime.now()) ||
                                            authState.state.currentState.classRole == 'ORTU') &&
                                        tugas.status == 'BELUM DIKERJAKAN')
                                    ? CustomText(
                                        '${authState.state.currentState.classRole == 'ORTU' ? 'Anak ' : ''}anda ${DateTime.parse(widget.endDate).isBefore(DateTime.now()) ? 'tidak' : 'belum'} mengerjakan tugas ini',
                                        Kind.descBlack)
                                    : ((tugas.status == 'BELUM DIKERJAKAN' ||
                                                tugas.status == 'Dikumpulkan') &&
                                            !authState.state.currentState.classRole.isOrtu)
                                        ? Container(
                                            child: InkWell(
                                              onTap: () async {
                                                await uploadFile(_setUploadProgress, context);
                                              },
                                              child: Row(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      height: 35,
                                                      width: 35,
                                                      margin: EdgeInsets.only(
                                                          right: 10, top: 10, bottom: 10),
                                                      decoration: BoxDecoration(
                                                          color: SiswamediaTheme.white,
                                                          borderRadius: BorderRadius.circular(5),
                                                          border: Border.all(
                                                              color: SiswamediaTheme.green)),
                                                      child: Icon(Icons.add,
                                                          color: SiswamediaTheme.green),
                                                    ),
                                                    Text(
                                                      'Unggah File ',
                                                      style: mediumBlack.copyWith(
                                                          color: SiswamediaTheme.green,
                                                          fontSize: S.w / 30),
                                                    )
                                                  ]),
                                            ),
                                          )
                                        : SizedBox()
                              ],
                            ),
                          ),
                          if ((tugas.status != 'BELUM DIKERJAKAN' &&
                                  tugas.status != 'Dikumpulkan') ||
                              (DateTime.parse(widget.endDate).isBefore(DateTime.now()) ||
                                  authState.state.currentState.classRole == 'ORTU'))
                            Container(
                              width: S.w,
                              padding:
                                  EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
                              margin:
                                  EdgeInsets.symmetric(vertical: S.w * .03, horizontal: S.w * .04),
                              decoration: BoxDecoration(
                                color: SiswamediaTheme.lightGreen,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child:
                                  Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                Text('Deskripsi tugas mu',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: S.w / 25,
                                        fontWeight: FontWeight.w700)),
                                SelectableLinkify(
                                    onOpen: (link) => launch(link.url),
                                    // onTap: () => launch(),
                                    text: tugas.description.showNarration,
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: S.w / 28,
                                    )),
                              ]),
                            )
                          else
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                              child: TextField(
                                focusNode: descNode,
                                controller: descriptionController,
                                maxLines: 4,
                                minLines: 3,
                                decoration:
                                    descriptionFiled(context, hint: 'Deskripsi (bila diperlukan)')
                                        .copyWith(filled: true, fillColor: SiswamediaTheme.white),
                              ),
                            ),
                          (tugas.status == 'SUDAH DIKERJAKAN' ||
                                  // tugas.status == 'Dikumpulkan'||
                                  tugas.status == 'Dikembalikan' ||
                                  authState.state.currentState.classRole.isOrtu ||
                                  DateTime.parse(widget.endDate).isBefore(DateTime.now()))
                              ? SizedBox()
                              : Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: S.w * .05, vertical: S.w * .05),
                                  height: S.w * .12,
                                  width: S.w * .9,
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        if (progressUploadState.state.progress != 0) {
                                          CustomFlushBar.errorFlushBar(
                                            'Lampiran sedang diunggah!',
                                            context,
                                          );
                                        }
                                        // else if (dataImage.isEmpty) {
                                        //   CustomFlushBar.errorFlushBar(
                                        //     'Lampiran belum ditambahkan!',
                                        //     context,
                                        //   );
                                        // }
                                        else {
                                          if (descNode.hasFocus) {
                                            descNode.unfocus();
                                          }
                                          dialog(context);
                                        }
                                      },
                                      child: Center(
                                          child: Text(
                                              '${tugas.status == 'Dikumpulkan' ? 'Update' : 'Kumpulkan'}',
                                              style: TextStyle(
                                                  fontSize: S.w / 28, color: Colors.white))),
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                      color: SiswamediaTheme.green,
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: [SiswamediaTheme.shadowContainer])),
                          SizedBox(height: 20),
                        ],
                      ),
              ),
            ));
  }

  void dialog(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(
                builder: (BuildContext context, void Function(void Function()) setState) {
              return Dialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      width: S.w * .7,
                      padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/icons/small/image-confirm.png'))),
                          ),
                          SizedBox(height: 8),
                          CustomText('Konfirmasi!', Kind.heading1),
                          SizedBox(height: 8),
                          CustomText(
                              'Apakah Anda yakin untuk mengumpulkan tugas ini?', Kind.descBlack,
                              align: TextAlign.center),
                          SizedBox(height: 16),
                          if (isLoadingDialog)
                            Center(
                              child: CircularProgressIndicator(),
                            ),
                          if (!isLoadingDialog)
                            InkWell(
                              onTap: () {
                                setState(() {
                                  isLoadingDialog = true;
                                });
                                updateAssessmentsStudents();
                              },
                              child: Container(
                                  height: S.w * .1,
                                  width: S.w,
                                  margin: EdgeInsets.symmetric(horizontal: S.w * .1),
                                  decoration: BoxDecoration(
                                      color: SiswamediaTheme.green,
                                      borderRadius: BorderRadius.circular(4)),
                                  child: Center(
                                      child: Text('Ok',
                                          style: semiWhite.copyWith(fontSize: S.w / 30)))),
                            ),
                          SizedBox(height: 8),
                          if (!isLoadingDialog)
                            InkWell(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                    margin: EdgeInsets.symmetric(horizontal: S.w * .1),
                                    child: Text('Batal'))),
                          SizedBox(height: 20),
                        ],
                      )));
            }));
  }

  static HttpClient getHttpClient() {
    var httpClient = HttpClient()
      ..connectionTimeout = const Duration(seconds: 10)
      ..badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    return httpClient;
  }

  Future<void> uploadFile(OnUploadProgressCallback onUploadProgress, BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    try {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context,
          builder: (_) {
            return Center(child: CircularProgressIndicator());
          });
      var pick = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.any,
        // allowedExtensions: ['jpg', 'pdf', 'doc']
      );
      print('aduh');
      var file = pick.files.first;
      print(file.name);
      print(file.path);

      var client2 = http.Client();
      //todo file!=null
      if (file != null) {
        var totalByteLength = File(file.path).lengthSync();
        var type = lookupMimeType(file.path) ?? 'audio/ogg';
        Utils.log(totalByteLength, info: 'total byte');
        Utils.log(type, info: 'type');
        SiswamediaGoogleSignIn.getInstance().scopes.addAll([
          'https://www.googleapis.com/auth/drive.file',
        ]);
        await SiswamediaGoogleSignIn.getInstance().signIn();
        var headers = await SiswamediaGoogleSignIn.getInstance().currentUser.authHeaders;
        var requestHeaders = <String, String>{
          'X-Upload-Content-Type': type,
          'Content-Type': 'application/json',
          'X-Upload-Content-Length': '$totalByteLength',
        };
        headers.forEach((key, value) {
          requestHeaders[key] = value;
        });
        var resp =
            await client2.post('$baseGoogleApiUrl/upload/drive/v3/files?uploadType=resumable',
                body: json.encode(<String, int>{
                  'Content-Length': totalByteLength,
                }),
                headers: requestHeaders);
        print('response');
        print(resp.statusCode);
        print(resp.headers);
        final httpClient = getHttpClient();
        //3145727
        print(file.size);
        // if (file.size > 1048576) {
        //   throw LargeFileException(cause: 'Data lebih dari 1GB');
        // }

        final client = HttpClient();
        final response = await client.putUrl(Uri.parse(resp.headers['location']));

        var byteCount = 0;

        //todo
        // response.contentLength = File(file.path).lengthSync();
        response.contentLength = totalByteLength;
        print('content lenght ${response.contentLength}');
        response.headers.set(HttpHeaders.contentTypeHeader, type);
        response.headers.set(HttpHeaders.contentLengthHeader, totalByteLength);
        response.headers.set('Content-Range', 'bytes 0-${totalByteLength - 1}/$totalByteLength');

        headers.forEach((key, value) {
          response.headers.add(key, value);
        });

        final stream = await File(file.path).openRead();

        Navigator.pop(context);
        var stream2 = stream.transform<List<int>>(
          StreamTransformer.fromHandlers(
            handleData: (data, sink) {
              // print('--- data ---');
              // print(data);
              byteCount += data.length;
              print(byteCount);
              try {
                sink.add(data);
              } catch (e) {
                print(e);
              }
              if (onUploadProgress != null) {
                print('uploading');
                onUploadProgress(byteCount, totalByteLength, context);
                // CALL STATUS CALLBACK;
              }
            },
            handleError: (error, stack, sink) {},
            handleDone: (sink) {
              sink.close();
            },
          ),
        );

        print('masuk');
        // var streamUpload = msStream.transform<List<int>>(
        //   StreamTransformer.fromHandlers(
        //     handleData: (data, sink) {
        //       sink.add(data);
        //
        //       byteCount += data.length;
        //
        //       if (onUploadProgress != null) {
        //         print('uploading');
        //         onUploadProgress(byteCount, totalByteLength, context);
        //         // CALL STATUS CALLBACK;
        //       }
        //     },
        //     handleError: (error, stack, sink) {
        //       print('error sink');
        //       throw error;
        //     },
        //     handleDone: (sink) {
        //       print('done');
        //       sink.close();
        //       // UPLOAD DONE;
        //     },
        //   ),
        // );

        await response.addStream(stream2);

        final httpResponse = await response.close();
        var statusCode = httpResponse.statusCode;
        // var statusCode = 200;

        print(statusCode);
        if (statusCode ~/ 100 != 2) {
          print(statusCode);
          throw SiswamediaException(
              'Error uploading file, Status code: ${httpResponse.statusCode}');
        } else {
          return await readResponseAsString(httpResponse, file.name, headers);
        }
        // return response;
      } else {
        progressUploadState.state.progress = 0;
        // return null;
      }
    } on LargeFileException catch (error) {
      progressUploadState.state.progress = 0;
      // Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } on ErrorUploadException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } catch (e) {
      progressUploadState.state.progress = 0;
      // Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error is SiswamediaException ? (e as SiswamediaException).message : e.toString(),
        context,
      );
      print(e);
    }
  }

  Future<String> readResponseAsString(
      HttpClientResponse response, String filename, Map<String, String> headers) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
      // var result = json.decode(data) as Map<String, dynamic>;
      // print(result);
      // var file = ModelUploadFile.fromJson(result);
      // setAttachments(file.data);
    }, onDone: () async {
      completer.complete(contents.toString());
      var result = await completer.future;
      var data = json.decode(result) as Map<String, dynamic>;
      var client = http.Client();
      print(data['id']);
      var resp = await client.patch('$baseGoogleApiUrl/drive/v3/files/${data['id']}?fields=*',
          body: json.encode({'name': filename}), headers: headers);
      print(resp.body);
      await client
          .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
              body: json.encode({'role': 'reader', 'type': 'anyone'}), headers: headers)
          .then((value) {
        print(value.body);
        setAttachments(DataFile(
            fileName: filename, file: 'https://drive.google.com/uc?export=view&id=${data['id']}'));
        CustomFlushBar.successFlushBar('Berhasil Mengupload Lampiran', context);
        progressUploadState.setState((s) => s.setProgress(0), silent: true);
      });
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes), silent: true);
    progressUploadState.setState((s) => s.setContent(totalBytes), silent: true);
    var __progressValue = Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }
}
