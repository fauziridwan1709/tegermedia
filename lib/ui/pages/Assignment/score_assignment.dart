part of '_assignment.dart';

class ScoreAssignment extends StatefulWidget {
  final int refrenceId;
  final int assessmentsId;
  final DateTime startTime;
  final DateTime endTime;
  ScoreAssignment(
      {@required this.refrenceId,
      @required this.assessmentsId,
      this.startTime,
      this.endTime});

  @override
  _ScoreAssignmentState createState() => _ScoreAssignmentState();
}

class _ScoreAssignmentState extends State<ScoreAssignment> {
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _score = TextEditingController();
  var controller = TextEditingController();
  var fNodeChat = FocusNode();
  List<MainAttachments> dataImage = [];
  ModelDetailAssessments _data;
  bool _isLoading = false;
  bool error = false;
  String text = '';
  double _progressValue = 0;
  int _progressPercentValue = 0;

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  void dispose() {
    _score.dispose();
    _isLoading = false;
    super.dispose();
  }

  void _getData() async {
    setState(() {
      _isLoading = true;
    });
    var result = await AssignmentServices.getAssessmentsReferenceDetail(
        assessmentsId: widget.assessmentsId, referenceId: widget.refrenceId);

    if (result.statusCode == 200) {
      setState(() {
        _isLoading = false;
        _data = result;
        var list = <MainAttachments>[];
        _data.data.replyAttachments.forEach((element) {
          dataImage.add(
              MainAttachments(file: element.file, fileName: element.fileName));
        });
        // dataImage = _data.data.replyAttachments as List<MainAttachments>;
      });
    } else {
      setState(() {
        _isLoading = false;
        _data = result;
      });
    }
  }

  void _refreshData() async {
    var result = await AssignmentServices.getAssessmentsReferenceDetail(
        assessmentsId: widget.assessmentsId, referenceId: widget.refrenceId);
    setState(() => _data = result);
  }

  void setAttachments(DataFile file) {
    dataImage.add(MainAttachments(file: file.file, fileName: file.fileName));
    setState(() {});
  }

  void removeAttachments(int index) {
    DeleteDialog(context,
        title: 'Hapus Attachment',
        description: 'Yakin menghapus reply attachment ini?',
        onAgree: () async {
      // ignore: unawaited_futures
      dataImage.removeAt(index);
      setState(() {});
      updateAssessmentsStudents(isDelete: true);
    }).show();
  }

  void updateAssessmentsStudents({bool isDelete = false}) async {
    var data = ModelRefrenceAssessmentsStudens(attachments: dataImage);

    var response = await AssignmentServices.reply(
        assessmentsId: widget.assessmentsId,
        referenceId: widget.refrenceId,
        att: dataImage,
        nilai: _data.data.nilai);

    // var response = await AssignmentServices.postAssessmentsStudents(
    //     assesmentID: widget.assessmentsId,
    //     refrenceID: widget.refrenceId,
    //     body: data);

    response.statusCode.translate<void>(ifSuccess: () {
      if (isDelete) pop(context);
      CustomFlushBar.successFlushBar(
          isDelete
              ? 'Berhasil Menghapus attachment'
              : 'Berhasil me reply tugas',
          context);
    }, ifElse: () {
      if (isDelete) Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        'Gagal',
        context,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    var hehe = Color(0xffE8F5D6);
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text('Detail Tugas',
              style: semiBlack.copyWith(fontSize: S.w / 22)),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        body: _isLoading
            ? Container(
                margin: EdgeInsets.symmetric(
                    horizontal: defaultMargin, vertical: defaultMargin),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(children: [
                        SkeletonLine(width: 40, height: 40, borderRadius: 60),
                        SizedBox(
                          width: 10,
                        ),
                        Column(children: [
                          SkeletonLine(width: 100, height: 10, borderRadius: 5),
                          SizedBox(
                            height: 5,
                          ),
                          SkeletonLine(width: 100, height: 8, borderRadius: 5),
                        ])
                      ]),
                      SkeletonLine(width: 100, height: 10, borderRadius: 5),
                      SizedBox(
                        height: 5,
                      ),
                      SkeletonLine(width: 100, height: 10, borderRadius: 5),
                    ]))
            : Container(
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height),
                child: Stack(children: [
                  SingleChildScrollView(
                    child: Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: defaultMargin, vertical: defaultMargin),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                        color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                _data.data.imageUser),
                                            fit: BoxFit.cover)),
                                  ),
                                  SizedBox(width: 10),
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(_data.data.fullName),
                                        Text(_data.data.className)
                                      ])
                                ],
                              ),
                              if (_data.data.status == DIKUMPULKAN)
                                Padding(
                                  padding: EdgeInsets.only(bottom: 10, top: 30),
                                  child: Text('Jawaban'),
                                ),
                              if (_data.data.status == 'BELUM DIKERJAKAN')
                                Column(
                                  children: [
                                    SizedBox(height: 15),
                                    CustomText(
                                        'Siswa ini belum mengerjakan tugas',
                                        Kind.heading3)
                                  ],
                                ),
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: List.generate(
                                                _data.data.attachments.length,
                                                (index) => GestureDetector(
                                                    onTap: () async {
                                                      var fileName = _data
                                                          .data
                                                          .attachments[index]
                                                          .file;
                                                      if (await canLaunch(
                                                          fileName)) {
                                                        await launch(fileName);
                                                      } else {
                                                        throw 'Could not launch $fileName';
                                                      }
                                                    },
                                                    child: Container(
                                                      height: 40,
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10),
                                                      margin: EdgeInsets.only(
                                                          bottom: 5),
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              defaultMargin * 2,
                                                      decoration: BoxDecoration(
                                                          color:
                                                              Colors.grey[200],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5)),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            _data
                                                                .data
                                                                .attachments[
                                                                    index]
                                                                .fileName,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          )
                                                        ],
                                                      ),
                                                    ))).toList()),
                                        SizedBox(height: 30),
                                        _data.data.nilai == ''
                                            ? SizedBox()
                                            : Text(
                                                'Nilai : ${formatNumber(_data.data.nilai)}',
                                                style: semiBlack.copyWith(
                                                    fontSize: S.w / 24),
                                              )
                                      ])),
                              //todo attachment
                              SizedBox(height: 20),
                              CustomText('Reply attachment', Kind.heading3),

                              Container(
                                child: Column(
                                    children: List.generate(
                                        dataImage.length,
                                        (index) => GestureDetector(
                                            onTap: () async {
                                              var fileName =
                                                  dataImage[index].file;
                                              if (await canLaunch(fileName)) {
                                                await launch(fileName);
                                              } else {
                                                throw 'Could not launch $fileName';
                                              }
                                            },
                                            child: Container(
                                                width: S.w,
                                                margin: EdgeInsets.symmetric(
                                                    // horizontal: width * 0.05,
                                                    vertical: 3),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 5),
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color:
                                                            Colors.grey[300]),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    boxShadow: <BoxShadow>[
                                                      BoxShadow(
                                                          color: SiswamediaTheme
                                                              .grey
                                                              .withOpacity(0.2),
                                                          offset: const Offset(
                                                              0.2, 0.2),
                                                          blurRadius: 3.0),
                                                    ],
                                                    color: Colors.white),
                                                child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Expanded(
                                                          child: Text(
                                                              dataImage[index]
                                                                  .fileName)),
                                                      Spacer(),
                                                      GestureDetector(
                                                        onTap: () {
                                                          removeAttachments(
                                                              index);
                                                        },
                                                        child: Text(
                                                          'Hapus',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.red),
                                                        ),
                                                      )
                                                    ])))).toList()),
                              ),
                              if (progressUploadState.state.progress != 0)
                                Container(
                                    width: S.w,
                                    margin: EdgeInsets.symmetric(
                                        // horizontal: width * 0.05,
                                        vertical: 3),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    decoration: BoxDecoration(
                                        border:
                                            Border.all(color: Colors.grey[300]),
                                        borderRadius: BorderRadius.circular(5),
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                              color: SiswamediaTheme.grey
                                                  .withOpacity(0.2),
                                              offset: const Offset(0.2, 0.2),
                                              blurRadius: 3.0),
                                        ],
                                        color: Colors.white),
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                              'Uploading... $_progressPercentValue%'),
                                          SizedBox(height: 5),
                                          LinearPercentIndicator(
                                            width: S.w - 35 * 2,
                                            lineHeight: 8.0,
                                            percent: _progressValue,
                                            backgroundColor: Colors.grey[300],
                                            progressColor:
                                                SiswamediaTheme.green,
                                          ),
                                          SizedBox(height: 5),
                                          Container(
                                            width: S.w * .8,
                                            child: Text(
                                              '${Format.formatByte(progressUploadState.state.progress)}/${Format.formatByte(progressUploadState.state.contentLength)}',
                                              textAlign: TextAlign.right,
                                            ),
                                          ),
                                        ])),
                              // Container(
                              //   width: S.w,
                              //   padding: EdgeInsets.symmetric(
                              //       horizontal: S.w * .05, vertical: S.w * .05),
                              //   margin: EdgeInsets.symmetric(
                              //       vertical: S.w * .03, horizontal: S.w * .0),
                              //   decoration: BoxDecoration(
                              //     color: Colors.white,
                              //     borderRadius: BorderRadius.circular(12),
                              //   ),
                              //   child: Column(
                              //       crossAxisAlignment: CrossAxisAlignment.start,
                              //       children: [
                              //         Text('Deskripsi dari Siswa',
                              //             textAlign: TextAlign.start,
                              //             style: TextStyle(
                              //                 color: Colors.black87,
                              //                 fontSize: S.w / 25,
                              //                 fontWeight: FontWeight.w700)),
                              //         SelectableLinkify(
                              //             onOpen: (link) => launch(link.url),
                              //             // onTap: () => launch(),
                              //             text: _data
                              //                 .data.description.showNarration,
                              //             style: TextStyle(
                              //               color: Colors.black87,
                              //               fontSize: S.w / 28,
                              //             )),
                              //       ]),
                              // ),

                              SizedBox(height: 20),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Deskripsi dari Siswa',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: S.w / 25,
                                            fontWeight: FontWeight.w700)),
                                    SelectableLinkify(
                                        onOpen: (link) => launch(link.url),
                                        // onTap: () => launch(),
                                        text: _data
                                            .data.description.showNarration,
                                        style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: S.w / 28,
                                        )),
                                  ]),
                              SizedBox(height: 25),
                              if (_data.data.comments.isNotEmpty)
                                CustomText('Komentar', Kind.heading3),
                              ListView(
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                // mainAxisSize: MainAxisSize.max,
                                children: _data.data.comments.map((e) {
                                  var date =
                                      DateTime.parse(e.sentDate).toLocal();
                                  var start = DateTime.now();
                                  var status = e.senderUserId ==
                                      GlobalState.profile().state.profile.id;
                                  //todo userid
                                  return Column(
                                    crossAxisAlignment: status
                                        ? CrossAxisAlignment.end
                                        : CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        constraints: BoxConstraints(
                                            maxWidth: width * .65),
                                        margin:
                                            EdgeInsets.symmetric(vertical: 14),
                                        child: Column(
                                          crossAxisAlignment: status
                                              ? CrossAxisAlignment.end
                                              : CrossAxisAlignment.start,
                                          children: [
                                            CustomContainer(
                                              color:
                                                  status ? hehe : Colors.white,
                                              boxShadow: [
                                                SiswamediaTheme.shadowContainer
                                              ],
                                              radius: radius(12),
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 10, horizontal: 8),
                                              child: Column(
                                                crossAxisAlignment: status
                                                    ? CrossAxisAlignment.end
                                                    : CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  CustomText(e.senderName,
                                                      Kind.heading3),
                                                  Text(e.comment,
                                                      textAlign: status
                                                          ? TextAlign.right
                                                          : TextAlign.left,
                                                      maxLines: 5,
                                                      softWrap: true,
                                                      style: descBlack.copyWith(
                                                          fontSize: S.w / 32)),
                                                ],
                                              ),
                                            ),

                                            // CustomText(
                                            //     e.comment, Kind.descBlack),
                                            SizedBox(height: 5),
                                            Text(
                                                '${DateTime.now().difference(date).inDays == 0 && DateTime.now().day - date.day == 0 ? "Hari ini" : start.yearMonthDay.difference(date.yearMonthDay).inDays < 2 ? "Kemarin" : start.difference(date).inDays.toString() + " Hari yang lalu"} ${date.hour < 10 ? "0" : ""}${date.hour}.${date.minute < 10 ? "0" : ""}${date.minute}',
                                                style: descBlack.copyWith(
                                                    fontSize: S.w / 36)),
                                          ],
                                        ),
                                      ),
                                    ],
                                  );
                                  // return
                                  // return Row(
                                  //   mainAxisAlignment: e.senderUserId ==
                                  //           GlobalState.profile()
                                  //               .state
                                  //               .profile
                                  //               .userId
                                  //       ? MainAxisAlignment.end
                                  //       : MainAxisAlignment.start,
                                  //   children: [
                                  //
                                  //   ],
                                  // );
                                }).toList(),
                              ),
                              SizedBox(height: 200)
                            ])),
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      child: Container(
                        decoration:
                            BoxDecoration(color: Colors.white, boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 0),
                              blurRadius: 2,
                              spreadRadius: 2,
                              color: Colors.grey.withOpacity(.2))
                        ]),
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(
                                  horizontal: S.w * .04, vertical: S.w * .05),
                              child: SingleChildScrollView(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 15),
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15),
                                        constraints: BoxConstraints(
                                            maxHeight: 100, minHeight: 40),
                                        decoration: BoxDecoration(
                                          color: Color(0xfff8f8f8),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: SingleChildScrollView(
                                            child: TextField(
                                              controller: controller,
                                              focusNode: fNodeChat,
                                              maxLines: null,
                                              // onChanged: (value) {
                                              //   print(value.length);
                                              // },
                                              style: normalText(context),
                                              decoration: InputDecoration(
                                                hintText:
                                                    'Tulis Komentar disini',
                                                hintStyle: hintText(context),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: S.w * .1,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Material(
                                            color: Colors.transparent,
                                            child: InkWell(
                                              onTap: () async {
                                                var pref =
                                                    await SharedPreferences
                                                        .getInstance();
                                                var map = <String, String>{
                                                  "comment": controller.text,
                                                  "sent_date": DateTime.now()
                                                      .toUtc()
                                                      .toIso8601String()
                                                };
                                                var url =
                                                    '$baseUrl/$version/assessments/${widget.assessmentsId}/reference/${widget.refrenceId}/comment';
                                                var resp = await client.post(
                                                    url,
                                                    body: json.encode(map),
                                                    headers: {
                                                      'Authorization': pref
                                                          .getString(AUTH_KEY),
                                                      'Content-Type':
                                                          'application/json'
                                                    });

                                                if (resp.statusCode
                                                    .isSuccessOrCreated) {
                                                  fNodeChat.unfocus();
                                                  controller.text = '';
                                                  _refreshData();
                                                }
                                              },
                                              child: Icon(Icons.send,
                                                  size: 26,
                                                  color: SiswamediaTheme.green),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: defaultMargin),
                              child: GestureDetector(
                                  onTap: () async {
                                    if (_data.data.nilai.isNotEmpty) {
                                      await uploadFile(
                                          _setUploadProgress, context);
                                    } else {
                                      CustomFlushBar.errorFlushBar(
                                          'Beri nilai terlebih dahulu, lalu reply dengan attachment',
                                          context);
                                    }
                                  },
                                  child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 11),
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.blue,
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          boxShadow: [
                                            SiswamediaTheme.shadowContainer
                                          ]),
                                      child: Center(
                                          child: Text('Reply Attachment',
                                              style: semiWhite.copyWith(
                                                  fontSize: S.w / 28))))),
                            ),
                            SizedBox(height: 5),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: defaultMargin),
                              child: GestureDetector(
                                  onTap: () {
                                    _inputScore(
                                      context,
                                    );
                                  },
                                  child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 11),
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          boxShadow: [
                                            SiswamediaTheme.shadowContainer
                                          ]),
                                      child: Center(
                                          child: Text(
                                              '${_data.data.nilai == '' ? 'Beri Nilai' : 'Update Nilai'}',
                                              style: semiWhite.copyWith(
                                                  fontSize: S.w / 28))))),
                            ),
                          ],
                        ),
                      )),
                ]),
              ));
  }

  void _inputScore(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              // String role = someCapitalizedString(data.role);
              return Center(
                child: SingleChildScrollView(
                  // padding: MediaQuery.of(context).viewInsets,
                  child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      child: Container(
                          height: 300,
                          width: S.w * .5,
                          padding: EdgeInsets.symmetric(
                              vertical: S.w *
                                  .05, //MediaQuery.of(context).padding.bottom,
                              horizontal: S.w * .05),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('Beri Nilai Tugas',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                  'Pastikan Anda sudah memeriksa jawaban siswa, jika sudah yakin anda bisa berikan nilai di kolom dibawah ini',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12)),
                              SizedBox(
                                height: 16,
                              ),
                              Container(
                                  margin: EdgeInsets.symmetric(vertical: 5),
                                  height: 35,
                                  width: 76,
                                  child: TextField(
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    controller: _score,
                                    decoration: InputDecoration(
                                        hintText: '0-100',
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 2.0, horizontal: 10),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            borderSide: BorderSide(
                                                color: Colors.grey[300])),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            borderSide: BorderSide(
                                                color: Colors.grey[300]))),
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    onChanged: (String newVal) {
                                      text = newVal;
                                    },
                                  )),
                              SizedBox(
                                height: 16,
                              ),
                              InkWell(
                                  onTap: () async {
                                    if (_score.text == '') {
                                      _message(
                                        'Nilai tidak boleh kosong!',
                                      );
                                    } else if (_score.text != '' &&
                                        int.parse(text) > 100) {
                                      _message(
                                        'Nilai tidak boleh lebih dari 100!',
                                      );
                                    } else {
                                      setState(() {
                                        _isLoading = true;
                                      });

                                      var result = await AssignmentServices
                                          .createScoreAssignment(
                                              assessmentsId:
                                                  widget.assessmentsId,
                                              referenceId: widget.refrenceId,
                                              nilai: text);
                                      var data =
                                          ModelRefrenceAssessmentsStudens(
                                        status: 'Dikembalikan',
                                      );

                                      if (result.statusCode == 200) {
                                        // var response = await TugasServices
                                        //     .postAssesmentsStudens(
                                        //         assesmentID:
                                        //             widget.assessmentsId,
                                        //         refrenceID: widget.refrenceId,
                                        //         body: data);
                                        // if (response.statusCode == 200 ||
                                        //     response.statusCode == 201) {
                                        //
                                        // } else {
                                        //   print(response.statusCode);
                                        // }
                                        setState(() {
                                          _isLoading = false;
                                        });
                                        _getData();
                                        Navigator.pop(context);
                                      } else {
                                        setState(() {
                                          _isLoading = false;
                                        });
                                        _message(
                                          result.message,
                                        );
                                      }
                                    }
                                  },
                                  child: Container(
                                      width: S.w,
                                      height: 45,
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius:
                                              BorderRadius.circular(23)),
                                      child: Center(
                                          child: _isLoading
                                              ? Container(
                                                  height: 30,
                                                  width: 30,
                                                  child: CircularProgressIndicator(
                                                      valueColor:
                                                          AlwaysStoppedAnimation<
                                                                  Color>(
                                                              Colors.grey[300]),
                                                      backgroundColor:
                                                          Colors.white))
                                              : Text('Masukkan Nilai',
                                                  style: TextStyle(
                                                      color: SiswamediaTheme
                                                          .white)))))
                            ],
                          ))),
                ),
              );
            }));
  }

  void _message(String message) {
    return CustomFlushBar.errorFlushBar(message + '!', context);
  }

  static HttpClient getHttpClient() {
    var httpClient = HttpClient()
      ..connectionTimeout = const Duration(seconds: 10)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

    return httpClient;
  }

  Future<void> uploadFile(
      OnUploadProgressCallback onUploadProgress, BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    try {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context,
          builder: (_) {
            return Center(child: CircularProgressIndicator());
          });
      var pick = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.any,
        // allowedExtensions: ['jpg', 'pdf', 'doc']
      );
      print('aduh');
      var file = pick.files.first;
      print(file.name);
      print(file.path);

      var client2 = http.Client();
      //todo file!=null
      if (file != null) {
        var totalByteLength = File(file.path).lengthSync();
        var type = lookupMimeType(file.path);
        SiswamediaGoogleSignIn.getInstance().scopes.addAll([
          'https://www.googleapis.com/auth/drive.file',
        ]);
        await SiswamediaGoogleSignIn.getInstance().signIn();
        var headers =
            await SiswamediaGoogleSignIn.getInstance().currentUser.authHeaders;
        var requestHeaders = <String, String>{
          'X-Upload-Content-Type': type,
          'Content-Type': 'application/json',
          'X-Upload-Content-Length': '$totalByteLength',
        };
        headers.forEach((key, value) {
          requestHeaders[key] = value;
        });
        var resp = await client2.post(
            '$baseGoogleApiUrl/upload/drive/v3/files?uploadType=resumable',
            body: json.encode(<String, int>{
              'Content-Length': totalByteLength,
            }),
            headers: requestHeaders);
        print('response');
        print(resp.statusCode);
        print(resp.headers);
        final httpClient = getHttpClient();
        //3145727
        print(file.size);
        // if (file.size > 1048576) {
        //   throw LargeFileException(cause: 'Data lebih dari 1GB');
        // }

        final client = HttpClient();
        final response =
            await client.putUrl(Uri.parse(resp.headers['location']));

        var byteCount = 0;

        //todo
        // response.contentLength = File(file.path).lengthSync();
        response.contentLength = totalByteLength;
        print('content lenght ${response.contentLength}');
        response.headers.set(HttpHeaders.contentTypeHeader, type);
        response.headers.set(HttpHeaders.contentLengthHeader, totalByteLength);
        response.headers.set(
            'Content-Range', 'bytes 0-${totalByteLength - 1}/$totalByteLength');

        headers.forEach((key, value) {
          response.headers.add(key, value);
        });

        final stream = await File(file.path).openRead();

        Navigator.pop(context);
        var stream2 = stream.transform<List<int>>(
          StreamTransformer.fromHandlers(
            handleData: (data, sink) {
              byteCount += data.length;
              print(byteCount);
              sink.add(data);
              if (onUploadProgress != null) {
                print('uploading');
                onUploadProgress(byteCount, totalByteLength, context);
                // CALL STATUS CALLBACK;
              }
            },
            handleError: (error, stack, sink) {},
            handleDone: (sink) {
              sink.close();
            },
          ),
        );

        print('masuk');
        // var streamUpload = msStream.transform<List<int>>(
        //   StreamTransformer.fromHandlers(
        //     handleData: (data, sink) {
        //       sink.add(data);
        //
        //       byteCount += data.length;
        //
        //       if (onUploadProgress != null) {
        //         print('uploading');
        //         onUploadProgress(byteCount, totalByteLength, context);
        //         // CALL STATUS CALLBACK;
        //       }
        //     },
        //     handleError: (error, stack, sink) {
        //       print('error sink');
        //       throw error;
        //     },
        //     handleDone: (sink) {
        //       print('done');
        //       sink.close();
        //       // UPLOAD DONE;
        //     },
        //   ),
        // );

        await response.addStream(stream2);

        final httpResponse = await response.close();
        var statusCode = httpResponse.statusCode;

        print(statusCode);
        if (statusCode ~/ 100 != 2) {
          print(statusCode);
          throw SiswamediaException(
              'Error uploading file, Status code: ${httpResponse.statusCode}');
        } else {
          return await readResponseAsString(httpResponse, file.name, headers);
        }
        // return response;
      } else {
        progressUploadState.state.progress = 0;
        // return null;
      }
    } on LargeFileException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } on ErrorUploadException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } catch (e) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error is SiswamediaException
            ? (e as SiswamediaException).message
            : e.toString(),
        context,
      );
      print(e);
    }
  }

  Future<String> readResponseAsString(HttpClientResponse response,
      String filename, Map<String, String> headers) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
      // var result = json.decode(data) as Map<String, dynamic>;
      // print(result);
      // var file = ModelUploadFile.fromJson(result);
      // setAttachments(file.data);
    }, onDone: () async {
      completer.complete(contents.toString());
      var result = await completer.future;
      var data = json.decode(result) as Map<String, dynamic>;
      var client = http.Client();
      print(data['id']);
      var resp = await client.patch(
          '$baseGoogleApiUrl/drive/v3/files/${data['id']}?fields=*',
          body: json.encode({'name': filename}),
          headers: headers);
      print(resp.body);
      await client
          .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
              body: json.encode({'role': 'reader', 'type': 'anyone'}),
              headers: headers)
          .then((value) {
        print(value.body);
        setAttachments(DataFile(
            fileName: filename,
            file: 'https://drive.google.com/uc?export=view&id=${data['id']}'));
        updateAssessmentsStudents();
        CustomFlushBar.successFlushBar('Berhasil Mengupload Lampiran', context);
        progressUploadState.setState((s) => s.setProgress(0), silent: true);
      });
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes), silent: true);
    progressUploadState.setState((s) => s.setContent(totalBytes), silent: true);
    var __progressValue =
        Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }
}
