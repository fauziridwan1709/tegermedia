part of '_assignment.dart';

class MultiSelectClassPage extends StatefulWidget {
  final String type;
  MultiSelectClassPage({this.type = 'normal'});
  @override
  _MultiSelectClassPageState createState() => _MultiSelectClassPageState();
}

class _MultiSelectClassPageState extends State<MultiSelectClassPage> {
  final multiSelectClassState = GlobalState.multiSelectClass();
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;
  TextEditingController _searchController;
  bool _visible;

  // var selected = <DataMultiSelectClass>[];

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    _searchController = TextEditingController();
    _visible = true;
    // setState(() {
    //   selected = multiSelectClassState.state.selected;
    // });
    Initializer(
      reactiveModel: multiSelectClassState,
      rIndicator: _refreshIndicatorKey,
      state: multiSelectClassState.state.listData != null,
      cacheKey: 'time_select_class',
    ).initialize();
  }

  Future<void> retrieveData() async {
    if (multiSelectClassState.isWaiting) {
      setState(() => _visible = false);
    }

    await multiSelectClassState.setState((s) => s.retrieveData(),
        onData: (context, data) => setState(
              () => _visible = false,
            ),
        onRebuildState: (_) => setState(() => _visible = true));
  }

  @override
  Widget build(BuildContext context) {
    var selected = multiSelectClassState.state.selected;
    return WillPopScope(
      onWillPop: () async {
        if (multiSelectClassState.state.selected.isEmpty ||
            widget.type.isUpdate) {
          return true;
        } else {
          DeleteDialog(context,
              title: 'Batal memilih kelas',
              description: 'Apakah Anda yakin untuk batal memilih kelas ?',
              onAgree: () async {
            await multiSelectClassState.setState((s) => s.clearSelect());
            Navigator.pop(context);
            Navigator.pop(context);
          }).show();
          return false;
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: SiswamediaTheme.white,
        appBar: SiswamediaAppBar(
          context: context,
          title: 'Pilih Kelas',
          onBackFunction: () {
            if (multiSelectClassState.state.selected.isEmpty ||
                widget.type.isUpdate) {
              Navigator.pop(context);
            } else {
              ///delete selected class for assignment
              DeleteDialog(context,
                  title: 'Batal memilih kelas',
                  description: 'Apakah Anda yakin untuk batal memilih kelas ?',
                  onAgree: () async {
                await multiSelectClassState.setState((s) => s.clearSelect());
                Navigator.pop(context);
                Navigator.pop(context);
              }).show();
            }
          },
        ),
        body: Stack(
          children: [
            Column(
              children: [
                Container(
                    color: SiswamediaTheme.white,
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .03),
                    child: TextField(
                      controller: _searchController,
                      onChanged: (val) {
                        multiSelectClassState.state.search(val);
                        multiSelectClassState.notify();
                      },
                      decoration: InputDecoration(
                          hintText: 'Masukkan pasangan kelas dan pelajaran',
                          hintStyle: hintText(context),
                          contentPadding: EdgeInsets.symmetric(horizontal: 24),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 1.5)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: SiswamediaTheme.border))),
                    )),
                Expanded(
                  child: StateBuilder<MultiSelectClassState>(
                      observe: () => multiSelectClassState,
                      builder: (context, _) {
                        return RefreshIndicator(
                          key: _refreshIndicatorKey,
                          onRefresh: retrieveData,
                          child: WhenRebuilder<MultiSelectClassState>(
                              observe: () => multiSelectClassState,
                              onIdle: () => WaitingView(),
                              onWaiting: () => WaitingView(),
                              onError: (dynamic error) =>
                                  ErrorView(error: error),
                              onData: (data) {
                                var filteredData = data.listData
                                    .where((element) =>
                                        ('${element.className} - ${element.courseName}')
                                            .toLowerCase()
                                            .contains(multiSelectClassState
                                                .state.searchKey
                                                .toLowerCase()))
                                    .toList();
                                if (data.listData.isEmpty) {
                                  return Container();
                                }
                                return AnimatedOpacity(
                                  opacity: _visible ? 1.0 : 0.0,
                                  duration: Duration(milliseconds: 450),
                                  child: ListView.separated(
                                    padding: EdgeInsets.only(bottom: 140),
                                    itemCount: filteredData.length,
                                    itemBuilder: (context, index) {
                                      var item = filteredData[index];
                                      return ListTile(
                                        onTap: () {
                                          // if (selected.contains(item)) {
                                          //   setState(() {
                                          //     selected.remove(item);
                                          //
                                          //   });
                                          // } else {
                                          //   setState(() {
                                          //     selected.add(item);
                                          //   });
                                          // }
                                          print(
                                              '${item.classId}-${item.courseId}');
                                          multiSelectClassState.state
                                              .switchSelected(item);
                                          multiSelectClassState.notify();
                                        },
                                        visualDensity: VisualDensity(
                                            horizontal: 0,
                                            vertical:
                                                VisualDensity.minimumDensity),
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 0, horizontal: S.w * .05),
                                        title: Text(
                                          '${item.className} - ${item.courseName}',
                                          style: mediumBlack.copyWith(
                                              fontSize: S.w / 29,
                                              color: (selected.containsKey(
                                                          item.classId) &&
                                                      selected[item.classId]
                                                              .courseId !=
                                                          item.courseId)
                                                  ? Colors.black38
                                                  : Colors.black87),
                                        ),
                                        trailing: (selected.containsKey(
                                                    item.classId) &&
                                                selected[item.classId]
                                                        .courseId ==
                                                    item.courseId)
                                            ? Icon(Icons.check,
                                                color: SiswamediaTheme.green)
                                            : SizedBox(),
                                      );
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return Divider();
                                    },
                                  ),
                                );
                              }),
                        );
                      }),
                )
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: StateBuilder<MultiSelectClassState>(
                observe: () => multiSelectClassState,
                builder: (context, data) {
                  return Container(
                    height: S.w * .3,
                    padding: EdgeInsets.symmetric(
                        vertical: S.w * .05, horizontal: S.w * .05),
                    color: SiswamediaTheme.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          'Kelas Terpilih: ${selected.length}',
                          Kind.heading3,
                          align: TextAlign.left,
                        ),
                        SizedBox(height: 5),
                        Container(
                          height: 45,
                          decoration: BoxDecoration(
                              borderRadius: radius(8),
                              color: SiswamediaTheme.green,
                              boxShadow: [SiswamediaTheme.shadowContainer]),
                          child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  onTap: () async => Navigator.pop(context),
                                  child: Center(
                                      child: CustomText(
                                          'Selesai', Kind.heading4)))),
                        )
                      ],
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
