part of '_assignment.dart';

class AssignmentFilterGuru extends StatefulWidget {
  @override
  _AssignmentFilterGuruState createState() => _AssignmentFilterGuruState();
}

class _AssignmentFilterGuruState extends State<AssignmentFilterGuru> {
  var filterState = Injector.getAsReactive<FilterAssignmentState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: SiswamediaAppBar(
          context: context,
          title: 'Filter',
        ),
        body: StateBuilder<FilterAssignmentState>(
          observe: () => filterState,
          builder: (context, _) {
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    if (GlobalState.auth()
                        .state
                        .currentState
                        .classRole
                        .isWaliKelas)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Text('Status yang Ditampilkan',
                              style: semiBlack.copyWith(fontSize: S.w / 24)),
                          buildStatus(),
                        ],
                      ),
                    SizedBox(height: 20),
                    Text('Urutkan Dari',
                        style: semiBlack.copyWith(fontSize: S.w / 24)),
                    buildSort(),
                    // RoundedRectangleButton(
                    //     title: 'Terapkan',
                    //     fontColor: SiswamediaTheme.white,
                    //     backgroundColor: SiswamediaTheme.green,
                    //     onTap: () {}),
                  ],
                ),
              ),
            );
          },
        ));
  }

  Widget buildSort() {
    if (listSortGuru.isEmpty) {
      return CustomText('Belum ada Course', Kind.descBlack);
    }
    return Column(
      children: listSortGuru
          .map((e) => FilterHomeSort(
                value: e,
                selectedValue: filterState.state.filter.sort,
                onChanged: (e) => filterState.setState((s) => s.updateSort(e)),
              ))
          .toList(),
    );
  }

  Widget buildStatus() {
    return FutureBuilder<CourseByClassModel>(
      future: ClassServices.getCourseByClass(
          classId: GlobalState.auth().state.currentState.classId),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          snapshot.data.data.sort((a, b) => a.id.compareTo(b.id));

          return Column(
            children: snapshot.data.data
                .map((e) => FilterHomeStatus(
                      title: e.name,
                      value: filterState.state.filter.course[e.id],
                      onChanged: (boolVal) => filterState
                          .setState((s) => s.changeStatusCourse(e.id)),
                    ))
                .toList(),
          );
        }
        return Container();
      },
    );
    return Column(
      children: listStatus
          .map((name) => FilterHomeStatus(
                title: name,
                value: filterState.state.filter.status[name],
                onChanged: (boolVal) =>
                    filterState.setState((s) => s.changeStatus(name)),
              ))
          .toList(),
    );
  }
}
