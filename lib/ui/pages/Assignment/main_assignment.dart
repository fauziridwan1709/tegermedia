part of '_assignment.dart';

class MainAssignment extends StatefulWidget {
  final String role;

  MainAssignment({this.role = 'SISWA'});

  @override
  _MainAssignmentState createState() => _MainAssignmentState();
}

class _MainAssignmentState extends State<MainAssignment> {
  final assignmentState = Injector.getAsReactive<AssignmentState>();
  final filterState = Injector.getAsReactive<FilterAssignmentState>();
  final auth = Injector.getAsReactive<AuthState>().state;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final animatedListKey = GlobalKey<AnimatedListState>();

  int _paginationIndex;

  @override
  void initState() {
    super.initState();
    filterState.setState((s) => s.retrieveCurrentFilter());
    _paginationIndex = 1;
    Initializer(
      reactiveModel: assignmentState,
      rIndicator: refreshIndicatorKey,
      state: (widget.role == assignmentState.state.role &&
          assignmentState.state.assignmentData != null),
      cacheKey: TIME_TASKS,
    ).initialize();
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      assignmentState.state.initType(widget.role, auth.currentState);
      var filterState = Injector.getAsReactive<FilterAssignmentState>();
      var course = <int>[];
      filterState.state.filter.course.forEach((key, value) {
        if (value == true) {
          course.add(key);
        }
      });
      await assignmentState.setState(
          (s) => s.retrieveData(course: course, page: _paginationIndex));
    });
  }

  @override
  Widget build(BuildContext context) {
    var ratio = MediaQuery.of(context).size.aspectRatio;
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text('Tugas', style: semiBlack.copyWith(fontSize: S.w / 22)),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        actions: [
          IconButton(
            icon: Icon(Icons.filter_list),
            color: SiswamediaTheme.green,
            onPressed: () => navigate(
                context,
                widget.role.isSiswaOrOrtu
                    ? AssignmentHomeFilter()
                    : AssignmentFilterGuru()),
          )
        ],
      ),
      floatingActionButton: widget.role.isSiswaOrOrtu
          ? SizedBox()
          : FloatingActionButton(
              onPressed: () => navigate(
                  context,
                  CreateAssignment(
                    schoolId: auth.currentState.schoolId,
                    listKey: animatedListKey,
                  )),
              backgroundColor: SiswamediaTheme.green,
              child:
                  Center(child: Icon(Icons.add, color: SiswamediaTheme.white)),
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
          return;
        },
        child: StateBuilder<AssignmentState>(
            observe: () => assignmentState,
            builder: (_, __) {
              return RefreshIndicator(
                key: refreshIndicatorKey,
                onRefresh: retrieveData,
                child: WhenRebuilder<AssignmentState>(
                    observe: () => assignmentState,
                    onWaiting: () => SkeletonWaitingView(),
                    onIdle: () => SkeletonWaitingView(),
                    onError: (dynamic error) => ErrorView(error: error),
                    onData: (data) {
                      var tugasData = data.assignmentData;
                      return StateBuilder<FilterAssignmentState>(
                          observe: () => filterState,
                          builder: (context, constrains) {
                            var orientation =
                                MediaQuery.of(context).orientation;
                            var isPortrait =
                                orientation == Orientation.portrait;
                            var filterData = filterState.state.filter.status;
                            var filterGuru = filterState.state.filter.sort;
                            var isTerdekat = filterGuru == 'Terdekat';
                            var isNotGuru = (widget.role.isSiswaOrOrtu);
                            vUtils.setLog(tugasData.data);
                            vUtils.setLog(filterData);
                            print(tugasData.data);
                            var dataMap = tugasData.data.listData.where(
                                (element) => isNotGuru
                                    ? filterData[DateTime.parse(element.endDate)
                                            .isBefore(DateTime.now())
                                        ? 'Selesai'
                                        : element.status.capitalizeFirstOfEach]
                                    : true);
                            print('mantap2');
                            print('test ${dataMap.length}');
                            //courseModel.data.sort((a, b) => a.id.compareTo(b.id));
                            var listTugas = dataMap.toList();
                            var now = DateTime.now();
                            if (widget.role.isGuruOrWaliKelas) {
                              print('mantap');
                              if (isTerdekat) {
                                print('mantap');
                                listTugas.sort((a, b) =>
                                    DateTime.parse(a.endDate)
                                        .toLocal()
                                        .compareTo(DateTime.parse(b.endDate)));
                                var list1 = listTugas.where((element) =>
                                    DateTime.parse(element.endDate)
                                        .toLocal()
                                        .isBefore(now));
                                var list2 = listTugas.where((element) =>
                                    DateTime.parse(element.endDate)
                                        .toLocal()
                                        .isAfter(now));
                                listTugas = list2.toList().reversed.toList();
                                list2.forEach((element) {
                                  print(element.name);
                                });
                                list1.forEach((element) {
                                  print(element.name);
                                });
                                // print(list1);
                                listTugas.addAll(list1.toList().reversed);
                              } else {
                                listTugas.sort((a, b) => a.id.compareTo(b.id));
                              }
                            }
                            if (tugasData.data.listData.isEmpty) {
                              return Column(
                                children: [
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              .15),
                                  Image.asset(
                                    'assets/no_quiz.png',
                                    width: S.w * 1,
                                    height: S.w * .5,
                                  ),
                                  SizedBox(height: 20),
                                  Text(
                                    'Tidak ada tugas terbaru',
                                    style: semiBlack.copyWith(fontSize: 14),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              );
                            }
                            if (listTugas.isEmpty) {
                              return Padding(
                                padding: EdgeInsets.only(top: S.w * .35),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(height: 40),
                                    Image.asset(
                                      'assets/no_quiz.png',
                                      width: S.w * 1,
                                      height: S.w * .5,
                                    ),
                                    SizedBox(height: 20),
                                    Text(
                                      'Tidak ditemukan',
                                      style: semiBlack.copyWith(fontSize: 14),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(height: S.w * .1)
                                  ],
                                ),
                              );
                            }
                            if (isPortrait) {
                              return AnimatedList(
                                shrinkWrap: true,
                                key: animatedListKey,
                                initialItemCount: dataMap.length + 1,
                                itemBuilder: (BuildContext context, int index,
                                    Animation<double> animation) {
                                  if (index == dataMap.length) {
                                    return Padding(
                                        padding: EdgeInsets.only(
                                            left: S.w * .1,
                                            right: S.w * .1,
                                            bottom: S.w * .2),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SizedBox(height: 25),
                                            Text('Select Page',
                                                style: semiBlack.copyWith(
                                                    fontSize: S.w / 30)),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Transform.rotate(
                                                  angle: pi,
                                                  child: IconButton(
                                                    onPressed: () =>
                                                        _paginationIndex != 1
                                                            ? setState(() {
                                                                _paginationIndex--;
                                                                refreshIndicatorKey
                                                                    .currentState
                                                                    .show();
                                                              })
                                                            : null,
                                                    icon: Icon(Icons
                                                        .double_arrow_sharp),
                                                    color: _paginationIndex != 1
                                                        ? SiswamediaTheme.green
                                                        : Colors.transparent,
                                                  ),
                                                ),
                                                Row(
                                                  children: _paginationIndex +
                                                              2 <
                                                          (tugasData.data.totalData ~/
                                                                  10) +
                                                              1
                                                      ? buildNumber((tugasData
                                                                      .data
                                                                      .totalData ~/
                                                                  10) +
                                                              1)
                                                          .sublist(
                                                              _paginationIndex -
                                                                  1,
                                                              _paginationIndex +
                                                                  2)
                                                      : buildNumber((tugasData
                                                                      .data
                                                                      .totalData ~/
                                                                  10) +
                                                              1)
                                                          .sublist(_paginationIndex ==
                                                                  1
                                                              ? _paginationIndex -
                                                                  1
                                                              : _paginationIndex -
                                                                  2),
                                                ),
                                                IconButton(
                                                  onPressed: () =>
                                                      _paginationIndex !=
                                                              (tugasData.data
                                                                          .totalData ~/
                                                                      10) +
                                                                  1
                                                          ? setState(() {
                                                              _paginationIndex++;
                                                              refreshIndicatorKey
                                                                  .currentState
                                                                  .show();
                                                            })
                                                          : null,
                                                  icon: Icon(
                                                      Icons.double_arrow_sharp),
                                                  color: _paginationIndex !=
                                                          (tugasData.data
                                                                      .totalData ~/
                                                                  10) +
                                                              1
                                                      ? SiswamediaTheme.green
                                                      : Colors.transparent,
                                                )
                                              ],
                                            ),
                                          ],
                                        ));
                                  }
                                  // animatedListKey.currentState.insertItem(0);
                                  return slideAnimation(
                                      context, index, animation);
                                },
                              );
                            } else {
                              return GridView.count(
                                  crossAxisCount: gridCountHome(ratio),
                                  shrinkWrap: true,
                                  physics: ScrollPhysics(),
                                  childAspectRatio: isPortrait ? 2.4 : 2.2,
                                  children: dataMap
                                      .map((data) => CardTugas(
                                          onDelete: showDeleteDialog,
                                          data: data,
                                          role: widget.role,
                                          // ignore: unrelated_type_equality_checks,
                                          onTap: () => (widget.role ==
                                                      'SISWA' ||
                                                  widget.role == 'ORTU')
                                              ? navigate(
                                                  context,
                                                  AssignmentDetail(
                                                    referenceID: data.id,
                                                    tugasName: data.name,
                                                    startDate: data.startDate,
                                                    endDate: data.endDate,
                                                    assessmentID:
                                                        data.assessmentId,
                                                  ))
                                              : navigate(
                                                  context,
                                                  AssignmentDetailGuru(
                                                    assessmentID: data.id,
                                                    startDate: data.startDate,
                                                    endDate: data.endDate,
                                                    tugasName: data.name,
                                                    tugasDescription:
                                                        data.description,
                                                  ))))
                                      .toList());
                            }
                          });
                    }),
              );
            }),
      ),
    );
  }

  Widget slideAnimation(BuildContext context, int index, Animation animation) {
    var isNotGuru = (widget.role == 'SISWA' || widget.role == 'ORTU');
    var filterData = filterState.state.filter.status;
    var dataMap = assignmentState.state.assignmentData.data.listData
        .where((element) => isNotGuru
            ? filterData[
                DateTime.parse(element.endDate).isBefore(DateTime.now())
                    ? 'Selesai'
                    : element.status.capitalizeFirstOfEach]
            : true)
        .toList();
    var filterGuru = filterState.state.filter.sort;
    var isTerdekat = filterGuru == 'Terdekat';
    var now = DateTime.now();
    if (widget.role.isGuruOrWaliKelas) {
      print('mantap');
      if (isTerdekat) {
        print('mantap');
        dataMap.sort((a, b) => DateTime.parse(a.endDate)
            .toLocal()
            .compareTo(DateTime.parse(b.endDate)));
        var list1 = dataMap.where((element) =>
            DateTime.parse(element.endDate).toLocal().isBefore(now));
        var list2 = dataMap.where((element) =>
            DateTime.parse(element.endDate).toLocal().isAfter(now));
        dataMap = list2.toList().reversed.toList();
        list2.forEach((element) {
          print(element.name);
        });
        list1.forEach((element) {
          print(element.name);
        });
        // print(list1);
        dataMap.addAll(list1.toList().reversed);
      } else {
        dataMap.sort((a, b) => a.id.compareTo(b.id));
        dataMap = dataMap.reversed.toList();
      }
    }
    var data = dataMap[index];
    print(data);
    return SlideTransition(
        position: Tween<Offset>(
          begin: const Offset(-1, 0),
          end: Offset(0, 0),
        ).animate(CurvedAnimation(
            parent: animation,
            curve: Curves.easeInOut,
            reverseCurve: Curves.easeInOut)),
        child: CardTugas(
            onDelete: showDeleteDialog,
            data: data,
            role: widget.role,
            // ignore: unrelated_type_equality_checks,
            onTap: () => (widget.role == 'SISWA' || widget.role == 'ORTU')
                ? navigate(
                    context,
                    AssignmentDetail(
                      referenceID: data.id,
                      tugasName: data.name,
                      startDate: data.startDate,
                      endDate: data.endDate,
                      newComment: data.newComment ?? false,
                      assessmentID: data.assessmentId,
                    ))
                : navigate(
                    context,
                    AssignmentDetailGuru(
                      assessmentID: data.id,
                      startDate: data.startDate,
                      endDate: data.endDate,
                      tugasName: data.name,
                      tugasDescription: data.description,
                    ))));
  }

  void showDeleteDialog(BuildContext context, int id) {
    DeleteDialog(context,
        title: 'Hapus Tugas',
        description: 'Apakah Anda yakin untuk menghapus tugas ini?',
        onAgree: () async {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context,
          builder: (context2) => Center(child: CircularProgressIndicator()));
      await AssignmentServices.deleteAssessment(id).then((value) async {
        Navigator.pop(context);
        if (value.statusCode == 200) {
          Navigator.pop(context);
          var index = assignmentState.state.assignmentData.data.listData
              .indexWhere((element) => element.id == id);

          animatedListKey.currentState.removeItem(index,
              (context, animation) => slideAnimation(context, index, animation),
              duration: Duration(milliseconds: 600));
          await Future<void>.delayed(Duration(milliseconds: 590)).then(
              (_) async =>
                  await assignmentState.setState((s) => s.deleteAssignment(
                        index,
                        animatedListKey,
                      )));

          return CustomFlushBar.successFlushBar(
            value.message,
            context,
          );
        } else {
          Navigator.pop(context);
          return CustomFlushBar.errorFlushBar(
            value.message,
            context,
          );
        }
      });
    }).show();
  }

  Widget radioButton({String label, bool status}) {
    return Row(children: [
      Radio(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          value: 'Tersedia',
          groupValue: null,
          onChanged: null),
      Text(label),
    ]);
  }

  Widget customCheckbox({String label, bool status}) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Row(children: [
        Container(
          height: 15,
          width: 15,
          margin: EdgeInsets.only(right: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: Colors.grey),
              color: SiswamediaTheme.white),
        ),
        Text(label),
      ]),
    );
  }

  List<Widget> buildNumber(int count) {
    return List.generate(
        count,
        (index) => CustomContainer(
              radius: radius(4),
              color: _paginationIndex == index + 1
                  ? SiswamediaTheme.lightBorder
                  : Colors.transparent,
              onTap: () {
                setState(() => _paginationIndex = index + 1);
                refreshIndicatorKey.currentState.show();
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
                child: Text(
                  '${index + 1}',
                  style: semiBlack.copyWith(
                      fontSize: S.w / 26,
                      color: _paginationIndex == index + 1
                          ? SiswamediaTheme.green
                          : Colors.black87),
                ),
              ),
            ));
  }
}

class CardTugas extends StatelessWidget {
  final Function onTap;
  final ListData data;
  final String role;
  final Function(BuildContext context, int id) onDelete;

  CardTugas({this.data, this.onTap, this.role, this.onDelete});

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    return Card(
      elevation: 5,
      shadowColor: Colors.grey.withOpacity(.2),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      margin: EdgeInsets.symmetric(horizontal: S.w * .03, vertical: S.w * .025),
      child: InkWell(
        onTap: () {
          if (onTap != null) {
            if (DateTime.parse(data.startDate).isBefore(DateTime.now()) ||
                GlobalState.auth()
                    .state
                    .currentState
                    .classRole
                    .isGuruOrWaliKelas) {
              onTap();
            } else {
              CustomFlushBar.errorFlushBar(
                  'Tugas belum bisa dilihat dan dikerjakan', context);
            }
          }
        },
        child: Container(
          padding:
              EdgeInsets.symmetric(horizontal: S.w * .04, vertical: S.w * .04),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          child: Text('${data.courseName ?? data.name}',
                              style: boldBlack.copyWith(fontSize: S.w / 26))),
                      SizedBox(width: 10),
                      (role == 'SISWA' || role == 'ORTU')
                          ? Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              decoration: BoxDecoration(
                                color: getColorStatus(
                                    DateTime.parse(data.endDate)
                                            .isBefore(DateTime.now())
                                        ? 'Selesai'
                                        : data.status),
                                borderRadius: BorderRadius.circular(50),
                              ),
                        child: Center(
                          child: Text(
                            DateTime.parse(data.endDate)
                                .isBefore(DateTime.now())
                                ? 'Selesai'
                                : data.status,
                            maxLines: 1,
                            style: TextStyle(
                                color: Colors.white, fontSize: 8),
                          ),
                        ),
                      )
                          : SizedBox(),
                    ],
                  ),
                  // Image.asset('assets/icons/small/bell-green.png'),
                  if (GlobalState.auth()
                      .state
                      .currentState
                      .classRole
                      .isGuruOrWaliKelas)
                    Row(
                      children: [
                        InkWell(
                            // padding: EdgeInsets.all(0),
                            onTap: () => bottomSheet(context),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 4.0),
                              child: Icon(Icons.more_vert),
                            )),
                      ],
                    )
                  // else
                  //   Stack(
                  //     children: [
                  //       Icon(Icons.chat, color: SiswamediaTheme.green),
                  //       if (data.newComment)
                  //         Positioned(
                  //           top: 0,
                  //           right: 0,
                  //           child: Container(
                  //             width: 10,
                  //             height: 10,
                  //             decoration: BoxDecoration(
                  //               shape: BoxShape.circle,
                  //               color: Colors.red,
                  //             ),
                  //           ),
                  //         ),
                  //     ],
                  //   )
                ],
              ),
              Text(data.name,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: descBlack.copyWith(fontSize: S.w / 32)),
              SizedBox(height: 10),
              Divider(color: Colors.grey),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Terakhir Dikumpulkan : ${QuizData.hari[DateTime.parse(data.endDate).toLocal().weekday - 1]} ${formattedDate(data.endDate)}',
                      style: TextStyle(fontSize: S.w / 34)),
                  Stack(
                    children: [
                      Image.asset('assets/icons/small/speech-bubble.png',
                          height: 18, width: 18),
                      if (data.newComment ?? false)
                        Positioned(
                          top: 0,
                          right: 0,
                          child: Container(
                            width: 8,
                            height: 8,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.red,
                            ),
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void bottomSheet(BuildContext context) {
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        context: context,
        builder: (context2) => Container(
              // height: S.w / 4,
              padding: EdgeInsets.symmetric(
                  horizontal: S.w * .05, vertical: S.w * .05),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(12.0),
                      topRight: const Radius.circular(12.0))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                //mainAxisAlignment:
                //  MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 5,
                    width: 35,
                    decoration: BoxDecoration(
                        borderRadius: radius(200), color: Colors.grey[300]),
                  ),
                  SizedBox(height: 10),
                  ListTile(
                    leading: Icon(Icons.edit),
                    title: Text(
                      'Ubah Tugas',
                      style: secondHeadingBlack.copyWith(fontSize: S.w / 28),
                    ),
                    onTap: () async {
                      Navigator.pop(context);
                      await navigate(
                          context,
                          UpdateAssignment(
                            assessmentId: data.id,
                          ));
                      // title.text = judul;
                      // desc.text = deskripsi;
                      // await update(
                      //   context,
                      //   id,
                      // );
                    },
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.delete),
                    title: Text('Hapus Tugas',
                        style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                    onTap: () async {
                      Navigator.pop(context);
                      await onDelete(context, data.id);
                    },
                  ),
                ],
              ),
            ));
  }
}

Color getColorStatus(String status) {
  switch (status) {
    case TERSEDIA:
      return SiswamediaTheme.green;
      break;
    case DIKUMPULKAN:
      return Color(0xff6596F8);
      break;
    case DIKEMBALIKAN:
      return Color(0xffF89265);
      break;
    case SELESAI:
      return SiswamediaTheme.lightBlack;
      break;
    case SUDAH_DIKERJAKAN:
      return SiswamediaTheme.green;
      break;
    default:
      return Colors.red;
  }
}

const String SUDAH_DIKERJAKAN = 'SUDAH DIKERJAKAN';
const String TERSEDIA = 'TERSEDIA';
const String DIKUMPULKAN = 'Dikumpulkan';
const String DIKEMBALIKAN = 'Dikembalikan';
const String SELESAI = 'Selesai';
