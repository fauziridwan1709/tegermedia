part of '_core.dart';

class ImagePreview extends StatefulWidget {
  final String link;
  final String tag;

  ImagePreview({@required this.link, @required this.tag})
      : assert(link != null, 'link cannot be null'),
        assert(tag != null, 'tag cannot be null');

  @override
  _ImagePreviewState createState() => _ImagePreviewState();
}

class _ImagePreviewState extends State<ImagePreview> {
  @override
  Widget build(BuildContext context) {
    vUtils.setLog(widget.link);
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Hero(
        tag: widget.tag,
        child: Container(
          width: size.width,
          height: size.height,
          color: Colors.black87,
          child: Image.network(
            widget.link,
            fit: BoxFit.fitWidth,
            scale: .05,
          ),
        ),
      ),
    );
  }
}
