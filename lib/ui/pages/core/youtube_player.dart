part of '_core.dart';

class YoutubePlayer extends StatefulWidget {
  final String videoId;
  final String title;
  final String description;
  final bool isMoreInfo;

  YoutubePlayer(
      {this.videoId, this.title, this.description, this.isMoreInfo = false});

  @override
  _YoutubePlayerState createState() => _YoutubePlayerState();
}

class _YoutubePlayerState extends State<YoutubePlayer> {
  YoutubePlayerController _controller;

  @override
  void initState() {
    print(widget.videoId);
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: widget.videoId,
      params: const YoutubePlayerParams(
        // startAt: const Duration(minutes: 1, seconds: 36),
        showControls: true,
        showFullscreenButton: true,
        desktopMode: true,
        privacyEnhanced: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      // log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      Future.delayed(const Duration(seconds: 1), () {
        _controller.play();
      });
      Future.delayed(const Duration(seconds: 5), () {
        SystemChrome.setPreferredOrientations(DeviceOrientation.values);
      });
      // log('Exited Fullscreen');
    };
  }

  @override
  Widget build(BuildContext context) {
    var player = Container(color: Colors.black, child: YoutubePlayerIFrame());
    return YoutubePlayerControllerProvider(
      controller: _controller,
      child: Scaffold(body: LayoutBuilder(
        builder: (context, constraints) {
          return ListView(
            children: [
              player,
              if (widget.isMoreInfo)
                TitleAndDescriptionYoutubePlayer(
                    title: widget.title, description: widget.description)
              // const Controls(),
            ],
          );
        },
      )),
    );
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}

class TitleAndDescriptionYoutubePlayer extends StatelessWidget {
  final String title;
  final String description;

  const TitleAndDescriptionYoutubePlayer({this.title, this.description});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(title, Kind.heading3),
          SizedBox(height: 5),
          CustomText(title, Kind.descBlack),
        ],
      ),
    );
  }
}

// class Controls extends StatelessWidget {
//   ///
//   const Controls();
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(16),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           _space,
//           MetaDataSection(),
//           _space,
//           SourceInputSection(),
//           _space,
//           PlayPauseButtonBar(),
//           _space,
//           VolumeSlider(),
//           _space,
//           PlayerStateSection(),
//         ],
//       ),
//     );
//   }
//
//   Widget get _space => const SizedBox(height: 10);
// }
