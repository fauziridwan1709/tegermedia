import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:tegarmedia/utils/v_utils.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

part 'image_preview.dart';
part 'youtube_player.dart';
