part of '_academic.dart';

class MorePageAcademic extends StatefulWidget {
  final List<ModelAcademicProtocol> data;
  final String type;
  MorePageAcademic({this.data, this.type});

  @override
  _MorePageAcademicState createState() => _MorePageAcademicState();
}

class _MorePageAcademicState extends State<MorePageAcademic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.grey.withOpacity(.2),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(widget.type,
            style: TextStyle(
                color: Colors.black,
                fontSize: S.w / 22,
                fontWeight: FontWeight.w600)),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: S.w * .04),
        child: GridView.count(
          physics: ScrollPhysics(),
          crossAxisCount: 2,
          shrinkWrap: true,
          childAspectRatio: 0.92,
          scrollDirection: Axis.vertical,
          children: widget.data
              .map((e) => Card(
                    elevation: 5,
                    shadowColor: Colors.grey.withOpacity(.2),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    margin: EdgeInsets.symmetric(
                        horizontal: S.w * .02, vertical: S.w * .03),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (_) => StepRegistration(
                                type: widget.type, url: e.url, title: e.name),
                          ),
                        );
                      },
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: S.w * .05, vertical: S.w * .05),
                          // height: 100,

                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Center(child: Image.asset(e.icon)),
                              Center(
                                child: Text(e.name),
                              )
                            ],
                          )),
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }
}
