part of '_academic.dart';

class Protocol extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Protokol',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: FutureBuilder<ProtocolModel>(
            future: ProtocolServices.getData(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.rows.length,
                    itemBuilder: (BuildContext context, int index) {
                      List<Entry> litItem = snapshot.data.rows
                          .map((e) => Entry(
                                snapshot.data.rows[index].topik,
                                <Entry>[
                                  Entry(
                                      '> ${snapshot.data.rows[index].subtopik}',
                                      <Entry>[
                                        Entry(
                                            '>> ${snapshot.data.rows[index].protokol}',
                                            <Entry>[
                                              Entry(
                                                  snapshot.data.rows[index].isi)
                                            ])
                                      ]),
                                ],
                              ))
                          .toList();
                      return EntryItem(litItem[index]);
                    });
              } else {
                return Center(
                    child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green),
                ));
              }
            }),
      ),
    );
  }
}

class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;
}

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Text(root.title),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}
