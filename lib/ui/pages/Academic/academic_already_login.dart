part of '_academic.dart';

class AcademicAlreadyLogin extends StatelessWidget {
  final ScrollController controller;
  AcademicAlreadyLogin({this.controller});

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          toolbarHeight: AppBar().preferredSize.height * 1.5,
          floating: true,
          titleSpacing: 0,
          shadowColor: Colors.transparent,
          title: AppBarUI(
            onLogin: () async {
              await GlobalState.auth().state.login(context);
            },
            onTapNotif: () => dialogDev(context),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            Card(
                // width: width,
                // height: width * .24,
                margin: EdgeInsets.symmetric(horizontal: 14, vertical: 16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
                elevation: 5,
                shadowColor: Colors.grey.withOpacity(.2),
                child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .02, vertical: S.w * .02),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: dataDummy
                            .map((e) => InkWell(
                                  onTap: () {
                                    if (e.name == 'Pantau') {
                                      return Navigator.push(
                                          context,
                                          MaterialPageRoute<void>(
                                              builder: (_) => Pantau()));
                                    }
                                    return Navigator.push(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (_) => Empty()));
                                  },
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 15),
                                    // decoration: BoxDecoration(
                                    //     border: Border.all(color: Colors.black)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          height: S.w * .1,
                                          width: S.w * .1,
                                          decoration: BoxDecoration(
                                              // color: SiswamediaTheme.green,
                                              image: DecorationImage(
                                                  image: AssetImage(e.icon)),
                                              borderRadius:
                                                  BorderRadius.circular(45)),
                                        ),
                                        SizedBox(height: 5),
                                        Text(e.name)
                                      ],
                                    ),
                                  ),
                                ))
                            .toList()))),
            Titles(
              title: 'Protokol',
              subtitle: 'Lihat Semua',
              size: S.w / 25,
              callback: () => Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                      builder: (_) => MorePageAcademic(
                            data: dataProtokol,
                            type: 'Protokol',
                          ))),
            ),
            SizedBox(
                height: S.w * .42,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: dataProtokol.sublist(0, 3).length,
                    itemBuilder: (_, index) {
                      return Padding(
                          padding: EdgeInsets.only(
                              left: (index == 0) ? defaultMargin : 0,
                              right: (index == dataProtokol.length - 1)
                                  ? defaultMargin
                                  : 10),
                          child: InkWell(
                            onTap: () {
                              // if (dataDummy[index].name == 'Pendaftaran') {
                              Navigator.push(
                                context,
                                MaterialPageRoute<void>(
                                  builder: (_) => StepRegistration(
                                      type: 'Protokol',
                                      url: dataProtokol[index].url,
                                      title: dataProtokol[index].name),
                                ),
                              );
                              // }
                            },
                            child: Card(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                elevation: 5,
                                shadowColor: Colors.grey.withOpacity(.2),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                child: AspectRatio(
                                  aspectRatio: 1,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        dataProtokol[index].icon,
                                        height: S.w * .2,
                                        width: S.w * .2,
                                      ),
                                      Center(
                                        child: Container(
                                            margin: EdgeInsets.symmetric(
                                                vertical: 10),
                                            width: 160,
                                            child: Text(
                                              dataProtokol[index].name,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: S.w / 34,
                                                  fontWeight: FontWeight.bold,
                                                  color: SiswamediaTheme.green),
                                            )),
                                      ),
                                    ],
                                  ),
                                )),
                          ));
                    })),
            Titles(
                title: 'Panduan',
                subtitle: 'Lihat Semua',
                size: S.w / 25,
                callback: () => Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                        builder: (_) => MorePageAcademic(
                            data: dataPanduan, type: 'Panduan')))),
            SizedBox(
                height: S.w * .42,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: dataPanduan.sublist(0, 3).length,
                    itemBuilder: (_, index) {
                      return Padding(
                          padding: EdgeInsets.only(
                              left: (index == 0) ? defaultMargin : 0,
                              right: (index == dataPanduan.length - 1)
                                  ? defaultMargin
                                  : 12),
                          child: InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                    builder: (_) => StepRegistration(
                                      url: dataPanduan[index].url,
                                      title: dataPanduan[index].name,
                                      type: 'Panduan',
                                    ),
                                  ),
                                );
                              },
                              child: Card(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 5, vertical: 10),
                                  elevation: 5,
                                  shadowColor: Colors.grey.withOpacity(.2),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12)),
                                  child: AspectRatio(
                                    aspectRatio: 1,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(dataPanduan[index].icon,
                                            height: S.w * .2, width: S.w * .2),
                                        Center(
                                          child: Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 10),
                                              width: 160,
                                              child: Text(
                                                dataPanduan[index].name,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: S.w / 34,
                                                    fontWeight: FontWeight.bold,
                                                    color:
                                                        SiswamediaTheme.green),
                                              )),
                                        ),
                                      ],
                                    ),
                                  ))));
                    }))
          ]),
        )
      ],
    );
  }
}
