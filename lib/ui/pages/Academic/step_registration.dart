part of '_academic.dart';

class ModelProtokol {
  String title;
  String subtitle;

  ModelProtokol({this.title, this.subtitle});
}

List<ModelProtokol> dataSummary = [
  ModelProtokol(subtitle: 'Belajar Dirumah', title: 'Belajar Dirumah'),
  ModelProtokol(subtitle: 'Ujian Dirumah', title: 'Ujian Dirumah'),
  ModelProtokol(subtitle: 'Tugas Dirumah', title: 'Tugas Dirumah'),
  ModelProtokol(
      subtitle: 'Kerja Kelompok Dirumah', title: 'Kerja Kelompok Dirumah'),
  ModelProtokol(subtitle: 'Gangguan Koneksi', title: 'Gangguan Koneksi'),
];

class StepRegistration extends StatefulWidget {
  final String title;
  final String url;
  final String type;

  StepRegistration({this.title, @required this.url, this.type});

  @override
  _StepRegistrationState createState() => _StepRegistrationState();
}

class _StepRegistrationState extends State<StepRegistration> {
  bool isCollapse = false;
  int dataCollapse;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          shadowColor: Colors.transparent,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            '${widget.type} ${widget.title}',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset(
                'assets/icons/akademis/academic_background.png',
                height: S.w / 2,
                width: S.w / 2,
              ),
              FutureBuilder<AkademisModel>(
                  future: AcademicServices.getData(
                    widget.url,
                  ),
                  // ignore: missing_return
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      print(snapshot.data);
                      if ((snapshot.data.rows?.length ?? 0) < 1) {
                        return Center(child: Text('Data Tidak Tersedia'));
                      }
                      return Column(
                        children: snapshot.data.rows
                            .where((element) => element.topik != '0')
                            .map((e) => Padding(
                                  padding:
                                      EdgeInsets.symmetric(vertical: S.w * .05),
                                  child: Column(
                                    children: [
                                      Container(
                                        width: S.w * .9,
                                        child: Text(
                                          e.topik,
                                          textAlign: TextAlign.left,
                                          style: boldBlack.copyWith(
                                              fontSize: S.w / 22),
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      Column(
                                          children: snapshot.data.rows
                                              .where((element) => element
                                                  .subtopik
                                                  .toLowerCase()
                                                  .contains(
                                                      e.topik.toLowerCase()))
                                              .map(
                                                (e) => Container(
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: S.w * .05,
                                                      vertical: S.w * .02),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8),
                                                      color: Colors.white,
                                                      boxShadow: [
                                                        BoxShadow(
                                                          offset: Offset(0, 0),
                                                          spreadRadius: 2,
                                                          blurRadius: 2,
                                                          color: Colors.grey
                                                              .withOpacity(.2),
                                                        )
                                                      ]),
                                                  child: ExpansionTile(
                                                    title: Text(
                                                      '${e.subtopik} - ${e.protokol}',
                                                      style: TextStyle(
                                                          fontSize: S.w / 30),
                                                    ),
                                                    childrenPadding:
                                                        EdgeInsets.symmetric(
                                                            horizontal:
                                                                S.w * .05,
                                                            vertical: 10),
                                                    children: [Text(e.isi)],
                                                  ),
                                                ),
                                              )
                                              .toList()),
                                    ],
                                  ),
                                ))
                            .toList(),
                      );
                    } else if (snapshot.connectionState ==
                        ConnectionState.waiting) {
                      return Center(child: CircularProgressIndicator());
                    } else if (snapshot.connectionState ==
                        ConnectionState.done) {
                      return Center(child: Text('Data Tidak Tersedia'));
                      /*ListView.builder(
                          itemCount: snapshot.data.rows.length,
                          itemBuilder: (_, index) {
                            return ExpansionTile(
                              title: Text(snapshot.data.rows[index].protokol),
                              children: [Text(snapshot.data.rows[index].isi)],
                            );
                          },
                        );*/

                    }
                  }),
            ],
          ),
        ));
  }

  Widget expansion(BuildContext context, AkademisDetailModel data, int index) {
    return Container(
        width: S.w,
        margin: EdgeInsets.symmetric(horizontal: S.w * 0.05, vertical: 3),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey[300]),
            borderRadius: BorderRadius.circular(12),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: SiswamediaTheme.grey.withOpacity(0.2),
                  offset: const Offset(0.2, 0.2),
                  blurRadius: 3.0),
            ],
            color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  if (dataCollapse == index) {
                    dataCollapse = null;
                  } else {
                    dataCollapse = index;
                  }
                });
              },
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: [
                      Icon(dataCollapse == index
                          ? Icons.arrow_drop_down
                          : Icons.arrow_right),
                      Text(data.protokol)
                    ],
                  )),
            ),
            dataCollapse == index
                ? Container(
                    padding: EdgeInsets.only(left: 32, right: 10, bottom: 10),
                    child: Text(data.isi))
                : SizedBox()
          ],
        ));
  }
}

class AccordionUI extends StatefulWidget {
  final AkademisDetailModel itemAkademis;
  final int index;

  AccordionUI(this.itemAkademis, this.index);

  @override
  _AccordionUIState createState() => _AccordionUIState();
}

class _AccordionUIState extends State<AccordionUI> {
  int dataCollapse;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: S.w,
        margin: EdgeInsets.symmetric(horizontal: S.w * 0.05, vertical: 3),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey[300]),
            borderRadius: BorderRadius.circular(12),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: SiswamediaTheme.grey.withOpacity(0.2),
                  offset: const Offset(0.2, 0.2),
                  blurRadius: 3.0),
            ],
            color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  if (dataCollapse == widget.index) {
                    dataCollapse = null;
                  } else {
                    dataCollapse = widget.index;
                  }
                });
              },
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: [
                      Icon(dataCollapse == widget.index
                          ? Icons.arrow_drop_down
                          : Icons.arrow_right),
                      Text(widget.itemAkademis.protokol == '0'
                          ? widget.itemAkademis.topik
                          : widget.itemAkademis.protokol)
                    ],
                  )),
            ),
            dataCollapse == widget.index
                ? Container(
                    padding: EdgeInsets.only(left: 32, right: 10, bottom: 10),
                    child: Text(widget.itemAkademis.isi))
                : SizedBox()
          ],
        ));
  }
}
