part of '_score.dart';

class SummaryScore extends StatefulWidget {
  const SummaryScore({this.userId, this.namaSiswa, this.isGuruView});
  final int userId;
  final String namaSiswa;
  final bool isGuruView;

  @override
  _SummaryScoreState createState() => _SummaryScoreState();
}

List<String> _listsemester = ['Ganjil', 'Genap'];

class _SummaryScoreState extends State<SummaryScore> {
  final authState = Injector.getAsReactive<AuthState>();
  final profile = GlobalState.profile();
  List<DataSummaryScore> _data = [];
  String params;
  bool _isLoading = false;
  int _tahunAjaran;
  int _semester;
  bool _condition;

  @override
  void initState() {
    super.initState();
    _tahunAjaran = 0;
    _semester = 0;
    _condition = false;
    fetchData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _handleisLoading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  Future<void> fetchData() async {
    await _getDataSummaryScore().then((value) =>
        Future<void>.delayed(Duration(milliseconds: 200))
            .then((value) => setState(() => _condition = true)));
  }

  Future<void> _getDataSummaryScore({int tahun, int smt}) async {
    // var auth = context.read<UserProvider>();

    var prefs = await SharedPreferences.getInstance();
    var userId = authState.state.currentState.classRole.isGuruOrWaliKelas
        ? widget.userId.toString()
        : authState.state.currentState.classRole == 'ORTU'
            ? authState.state.anak.first.studentId.toString()
            : profile.state.profile.id.toString();
    var semester = smt == null
        ? _listsemester[0].toLowerCase()
        : _listsemester[smt].toLowerCase();
    var schoolId = authState.state.currentState.schoolId;
    var classId = authState.state.currentState.classId;
    var tahunAjaran =
        tahun == null ? listTahunAjaran()[0] : listTahunAjaran()[tahun];

    var _params = <String, String>{
      'school_id': schoolId.toString(),
      'class_id': classId.toString(),
      'tahun_ajaran': '$tahunAjaran',
      'semester': semester,
      'user_id': userId
    };
    if (authState.state.currentState.classRole == 'ORTU') {
      _params.addAll(<String, String>{'type': 'ORTU'});
    }
    setState(() {
      params =
          'class_id=${classId}&school_id=${schoolId}&tahun_ajaran=$tahunAjaran&semester=$semester&user_id=$userId&${authState.state.currentState.classRole == 'ORTU' ? 'type=ORTU' : ''}';
    });

    print(params);
    _handleisLoading();
    var data = await ScoreServices.getSummaryScore(params: _params);
    if (data.statusCode == 200) {
      _handleisLoading();
      _data = data.data;
    } else {
      _handleisLoading();
      _data = [];
    }
  }

  void _handleFilter() {
    _getDataSummaryScore(tahun: _tahunAjaran, smt: _semester);
    Navigator.pop(context);
  }

  void _handleCancelFilter() {
    setState(() {
      _tahunAjaran = 0;
      _semester = 1;
    });
    _getDataSummaryScore();
    Navigator.pop(context);
  }

  void _setStateTahunAjaran(int index) {
    setState(() {
      _tahunAjaran = index;
    });
  }

  void _setStateSemester(int index) {
    print(_listsemester[index]);
    setState(() {
      _semester = index;
    });
  }

  Future<void> delay() async {
    await Future<void>.delayed(Duration(seconds: 1))
        .then((value) => setState(() => _condition = true));
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        appBar: AppBar(
          elevation: 2,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: TitleAppbar(label: 'Rekap Nilai'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          actions: [
            Row(
              children: [
                GestureDetector(
                    onTap: () => modalFilter(context,
                        handleFilter: _handleFilter,
                        cancelFilter: _handleCancelFilter,
                        setStateTahunAjaran: _setStateTahunAjaran,
                        setStateSemester: _setStateSemester,
                        tahunAjaran: _tahunAjaran,
                        semester: _semester,
                        showSemester: true),
                    child: Container(
                        margin: EdgeInsets.only(right: isPortrait ? 5 : 20),
                        padding:
                            EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20.0),
                            border: Border.all(color: SiswamediaTheme.border)),
                        child: Row(
                          children: [
                            Text(
                                '${listTahunAjaran()[_tahunAjaran]} ${_listsemester[_semester].capitalizeFirstOfEach}',
                                style: descBlack.copyWith(fontSize: S.w / 36)),
                            Icon(
                              Icons.keyboard_arrow_down,
                              color: SiswamediaTheme.green,
                            )
                          ],
                        )))
              ],
            )
          ],
        ),
        body: Center(
          child: Container(
              width: S.w,
              child: Column(
                children: [
                  Expanded(
                      child: _isLoading
                          ? Column(
                              children:
                                  List.generate(2, (index) => SkeletonScreen()),
                            )
                          : _data
                                  .where((element) =>
                                      element.classId ==
                                      authState.state.currentState.classId)
                                  .isEmpty
                              ? Center(
                                  child: Padding(
                                      padding: EdgeInsets.only(top: S.w * .5),
                                      child: Column(
                                        children: [
                                          Image.asset('assets/no_quiz.png',
                                              height: S.w * .4,
                                              width: S.w * .4),
                                          Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 40),
                                            child: Text(
                                              'Belum ada nilai pada semester dan tahun ajaran ini, klik pojok kanan atas',
                                              textAlign: TextAlign.center,
                                              style: descBlack.copyWith(
                                                  fontSize: 12),
                                            ),
                                          ),
                                        ],
                                      )),
                                )
                              : ListView.builder(
                                  itemCount: _data
                                      .where((element) =>
                                          element.classId ==
                                          authState.state.currentState.classId)
                                      .length,
                                  itemBuilder: (context, int index) {
                                    print(index);
                                    var detail = _data
                                        .where((element) =>
                                            element.classId ==
                                            authState
                                                .state.currentState.classId)
                                        .toList()
                                        .elementAt(index);
                                    return Container(
                                        margin: EdgeInsets.only(
                                            top: index == 0 ? 10 : 0),
                                        child: CardScore(
                                          condition: _condition,
                                          namaSiswa: widget.namaSiswa,
                                          isGuruView: widget.isGuruView,
                                          params: params,
                                          courseId: detail.courseId,
                                          teachers: detail.courseTeacher,
                                          tahunAjaran:
                                              listTahunAjaran()[_tahunAjaran],
                                          namaMatapelajaran: detail.courseName,
                                          nilaiRataRata: detail.rataRataTotal,
                                          wording: detail.rataRataTotal > 80
                                              ? 'Sangat Baik'
                                              : detail.rataRataTotal > 70
                                                  ? 'Baik'
                                                  : detail.rataRataTotal > 55
                                                      ? 'Cukup'
                                                      : detail.rataRataTotal >
                                                              40
                                                          ? 'Buruk'
                                                          : 'Sangat Buruk',
                                        ));
                                  },
                                ))
                ],
              )),
        ));
  }
}

Widget modalFilter(BuildContext context,
    {@required Function handleFilter,
    @required Function cancelFilter,
    @required Function setStateTahunAjaran,
    @required int tahunAjaran,
    Function setStateSemester,
    int semester,
    bool showSemester = true}) {
  showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      ),
      builder: (context) {
        return Container(
          height: S.h * 0.4,
          child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Container(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          height: 4.04,
                          width: 60.58,
                          margin: EdgeInsets.only(top: 25, bottom: 20),
                          decoration: BoxDecoration(
                              color: Color(0xFFD8D8D8),
                              borderRadius: BorderRadius.circular(40))),
                      CustomText('Tahun Ajaran', Kind.heading3),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Column(children: [
                            showSemester
                                ? Container(
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Center(
                                        child: Text('Tahun',
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600))))
                                : SizedBox(),
                            Container(
                              height: S.w * .3,
                              width: MediaQuery.of(context).size.width /
                                  (showSemester ? 2 : 1),
                              child: CupertinoPicker(
                                  itemExtent: 30,
                                  scrollController: FixedExtentScrollController(
                                      initialItem: tahunAjaran ?? 0),
                                  onSelectedItemChanged: (int i) {
                                    setStateTahunAjaran(i);
                                  },
                                  children: List.generate(
                                      listTahunAjaran().length,
                                      (index) => Center(
                                              child: Text(
                                            listTahunAjaran()[index],
                                            style: descBlack.copyWith(
                                                fontSize: S.w / 30),
                                          )))),
                            ),
                          ]),
                          !showSemester
                              ? SizedBox()
                              : Column(children: [
                                  Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 5),
                                      width:
                                          MediaQuery.of(context).size.width / 2,
                                      child: Center(
                                          child: Text('Semester',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight:
                                                      FontWeight.w600)))),
                                  Container(
                                    height: S.w * .3,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: CupertinoPicker(
                                        scrollController:
                                            FixedExtentScrollController(
                                                initialItem: semester ?? 0),
                                        itemExtent: 30,
                                        onSelectedItemChanged: (int i) {
                                          setStateSemester(i);
                                        },
                                        children: List.generate(
                                            _listsemester.length,
                                            (index) => Center(
                                                    child: Text(
                                                  _listsemester[index],
                                                  style: descBlack.copyWith(
                                                      fontSize: S.w / 30),
                                                )))),
                                  ),
                                ])
                        ],
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: S.w * .1),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: () {
                                    cancelFilter();
                                  },
                                  child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 42, vertical: 9),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: Colors.white,
                                          border: Border.all(
                                              color: Colors.grey[300])),
                                      child: CustomText(
                                          'Batalkan', Kind.descBlack)),
                                ),
                                InkWell(
                                    onTap: () {
                                      handleFilter();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 42, vertical: 9),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: SiswamediaTheme.green,
                                          border: Border.all(
                                              color: Colors.grey[300])),
                                      child: CustomText(
                                        'Terapkan',
                                        Kind.descWhite,
                                      ),
                                    )),
                              ])),
                    ]),
              )),
        );
      });
  return null;
}

class CardScore extends StatelessWidget {
  final int courseId;
  final String namaMatapelajaran;
  final double nilaiRataRata;
  final String wording;
  final String params;
  final bool isGuruView;
  final bool condition;
  final String namaSiswa;
  final String tahunAjaran;
  final List<CourseTeacher> teachers;

  CardScore({
    this.courseId,
    this.namaMatapelajaran,
    this.nilaiRataRata,
    this.wording,
    this.params,
    this.isGuruView,
    this.condition,
    this.namaSiswa,
    this.tahunAjaran,
    this.teachers,
  });

  // ignore: missing_return
  Color _colorWording(BuildContext contex, String wording) {
    switch (wording) {
      case 'Sangat Buruk':
        return Colors.black;
      case 'Buruk':
        return Colors.red;
      case 'Cukup':
        return Colors.orange;
      case 'Baik':
        return SiswamediaTheme.lightBlue;
      case 'Sangat Baik':
        return SiswamediaTheme.green;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
            left: defaultMargin, right: defaultMargin, bottom: 15),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 5,
                offset: Offset(0, 0),
                spreadRadius: 0.2,
              )
            ]),
        child: Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (route) => DetailSummaryScore(
                          namaSiswa: isGuruView ? namaSiswa : null,
                          tahunAjaran: tahunAjaran,
                          teachers: teachers,
                          courseId: courseId,
                          title: namaMatapelajaran,
                          param: params),
                    ));
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            namaMatapelajaran,
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w600),
                          ),
                          Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              decoration: BoxDecoration(
                                  color: _colorWording(context, wording),
                                  borderRadius: BorderRadius.circular(5)),
                              child: Text(wording,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 10)))
                        ],
                      ),
                      SizedBox(height: 10),
                      CustomProgressBar(
                          condition: condition,
                          width: MediaQuery.of(context).size.width,
                          value: nilaiRataRata,
                          label: Text(
                              '${nilaiRataRata.toStringAsFixed(2)} / 100',
                              style: TextStyle(
                                  fontSize: S.w / 36,
                                  color: SiswamediaTheme.green,
                                  fontWeight: FontWeight.bold)),
                          color: _colorWording(context, wording)),
                    ]),
              ),
            )));
  }
}

class CustomProgressBar extends StatelessWidget {
  CustomProgressBar(
      {this.condition, this.width, this.value, this.color, this.label});

  final bool condition;
  final double width;
  final double value;
  final Color color;
  final Text label;

  @override
  Widget build(BuildContext context) {
    var ratio = value / 100;

    return Container(
        decoration: BoxDecoration(
          border: Border.all(color: SiswamediaTheme.green),
          borderRadius: BorderRadius.circular(15),
        ),
        child: Stack(
          children: [
            Material(
              borderRadius: BorderRadius.circular(15),
              child: AnimatedContainer(
                duration: Duration(milliseconds: 450),
                curve: Curves.easeInOut,
                height: S.w * .04,
                width: condition
                    ? ((width - (2 * defaultMargin + 32)) * ratio)
                    : 0,
                decoration: BoxDecoration(
                    color: SiswamediaTheme.green.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(15)),
                // child: Center(child: label == null ? SizedBox() : label),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: S.w * .04,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(15)),
                child: Center(child: label)),
          ],
        ));
  }
}
