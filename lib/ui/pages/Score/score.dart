part of '_score.dart';

class Score extends StatefulWidget {
  @override
  _ScoreState createState() => _ScoreState();
}

class _ScoreState extends State<Score> {
  final authState = Injector.getAsReactive<AuthState>();
  List<Map<String, String>> menu = <Map<String, String>>[
    {
      'name': 'Rekap Nilai',
      'icon': 'assets/kelas/rekap nilai.png',
    },
    // {
    //   'name': 'Nilai Rapot',
    //   'icon': 'assets/kelas/nilai rapor.png',
    // },
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // var auth = context.watch<UserProvider>();
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        appBar: AppBar(
          elevation: 2,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: TitleAppbar(label: 'Nilai'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        backgroundColor: Colors.white,
        body: Container(
          margin: EdgeInsets.symmetric(
              vertical: defaultMargin, horizontal: defaultMargin),
          child: GridView.count(
            physics: ScrollPhysics(),
            crossAxisCount: isPortrait ? 2 : 4,
            shrinkWrap: true,
            childAspectRatio: .94,
            scrollDirection: Axis.vertical,
            children: List.generate(
                menu.length,
                (index) => GestureDetector(
                      onTap: () {
                        if (index == 0) {
                          if (authState
                              .state.currentState.classRole.isSiswaOrOrtu) {
                            Navigator.push<dynamic>(
                                context,
                                MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) =>
                                        SummaryScore(
                                          isGuruView: false,
                                        )));
                          } else {
                            navigate(context, RekapSiswa());
                          }
                        } else {
                          dialogDev(context,
                              title: 'Sesaat Lagi',
                              desc:
                                  'Fitur akan tersedia sesaat lagi, tunggu update berikutnya ya');
                        }
                      },
                      child: Card(
                          elevation: 4,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          shadowColor: Colors.grey.withOpacity(.4),
                          margin: EdgeInsets.symmetric(
                              horizontal:
                                  MediaQuery.of(context).size.width * .02,
                              vertical:
                                  MediaQuery.of(context).size.width * .03),
                          child: MenuClass(
                              width: MediaQuery.of(context).size.width,
                              name: menu[index]['name'],
                              icon: menu[index]['icon'])),
                    )).toList(),
          ),
        ));
  }
}
