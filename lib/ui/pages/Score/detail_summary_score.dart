part of '_score.dart';

class DetailSummaryScore extends StatefulWidget {
  final int courseId;
  final String title;
  final String param;
  final bool isGuruView;
  final String namaSiswa;
  final String tahunAjaran;
  final List<CourseTeacher> teachers;
  DetailSummaryScore({
    @required this.courseId,
    @required this.title,
    @required this.param,
    this.isGuruView,
    this.namaSiswa,
    this.tahunAjaran,
    this.teachers,
  });

  @override
  _DetailSummaryScoreState createState() => _DetailSummaryScoreState();
}

class _DetailSummaryScoreState extends State<DetailSummaryScore> {
  DataDetailSummaryScore _data;
  int currentIndex = 0;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _getDataDetailSummaryScore();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _handleisLoading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  void _getDataDetailSummaryScore() async {
    _handleisLoading();
    print(widget.param);
    var data = await ScoreServices.getDetailSummaryScore(
        courseId: widget.courseId, params: widget.param);
    if (data.statusCode == 200) {
      _handleisLoading();
      _data = data.data;
    } else {
      _handleisLoading();
      _data = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 2,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: TitleAppbar(label: widget.title),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          actions: [
            if (widget.namaSiswa != null)
              Center(
                child: Container(
                  // height: S.w * .1,
                  // width: S.w * .1,
                  margin: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      borderRadius: radius(4), color: SiswamediaTheme.green),
                  child: Center(
                      child: IconButton(
                    color: Colors.white,
                    icon: Icon(Icons.download_rounded),
                    onPressed: () =>
                        ExportScoreServices.exportRekapPelajaranSiswaToExcel(
                      tahunAjaran: widget.tahunAjaran,
                      namaSiswa: widget.namaSiswa,
                      context: context,
                      teachers: widget.teachers,
                      isTugas: false,
                      data: _data,
                    ),
                  )),
                ),
              )
          ],
        ),
        backgroundColor: Colors.white,
        body: LayoutBuilder(
          builder: (context, constrains) {
            if (_data == null) {
              return Center(child: CircularProgressIndicator());
            }
            return Container(
                child: Column(
              children: [
                Container(
                  color: Colors.grey.shade100,
                  padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: ['Tugas', 'Ujian']
                          .map((e) => Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: S.w * .04, vertical: S.w * .02),
                                margin: EdgeInsets.symmetric(
                                    horizontal: S.w * .01, vertical: S.w * .02),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(S.w),
                                  border: Border.all(
                                    color: currentIndex ==
                                            ['Tugas', 'Ujian'].indexOf(e)
                                        ? Colors.transparent
                                        : SiswamediaTheme.border,
                                  ),
                                  color: currentIndex ==
                                          ['Tugas', 'Ujian'].indexOf(e)
                                      ? SiswamediaTheme.green
                                      : Colors.transparent,
                                ),
                                child: InkWell(
                                  onTap: () => _selectTipe(
                                      ['Tugas', 'Ujian'].indexOf(e)),
                                  child: Center(
                                      child: Text(e,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: S.w / 30,
                                              color: currentIndex ==
                                                      ['Tugas', 'Ujian']
                                                          .indexOf(e)
                                                  ? Colors.white
                                                  : Colors.black))),
                                ),
                              ))
                          .toList()),
                ),
                SizedBox(height: 10),
                Expanded(
                  child: [
                    TugasScoreDetail(
                      items: _data.listAssessment,
                    ),
                    UjianScoreDetail(
                      items: _data.listQuiz,
                    )
                  ].elementAt(currentIndex),
                ),
              ],
            ));
          },
        ));
  }

  void _selectTipe(int i) {
    setState(() {
      currentIndex = i;
    });
  }
}

class TugasScoreDetail extends StatelessWidget {
  const TugasScoreDetail({this.items});
  final List<ListAssessment> items;
  @override
  Widget build(BuildContext context) {
    if (items == null || items.isEmpty) {
      return Center(
        child: Padding(
            padding: EdgeInsets.only(top: S.w * .5),
            child: Column(
              children: [
                Image.asset('assets/no_quiz.png',
                    height: S.w * .4, width: S.w * .4),
                Text('Belum ada nilai'),
              ],
            )),
      );
    }
    return ListView.separated(
        itemBuilder: (context, int index) {
          var detail = items[index];
          return Container(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: S.w * 0.38,
                      child: Text(
                        detail.assessmentName.capitalizeFirstOfEach,
                        style: TextStyle(fontWeight: FontWeight.w600),
                      )),
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      width: S.w * 0.3,
                      child: Text(
                        formattedDate(detail.startDate),
                        style: mediumBlack.copyWith(fontSize: S.w / 30),
                      )),
                  Container(
                      width: S.w * .1,
                      child: Text(
                        formatNilai(detail.score / 1.0),
                        style: TextStyle(fontWeight: FontWeight.w600),
                      )),
                ]),
          );
        },
        separatorBuilder: (context, int index) => Divider(),
        itemCount: items.length);
  }
}

class UjianScoreDetail extends StatelessWidget {
  const UjianScoreDetail({this.items});
  final List<ListQuiz> items;
  @override
  Widget build(BuildContext context) {
    if (items == null || items.isEmpty) {
      return Center(
        child: Padding(
            padding: EdgeInsets.only(top: S.w * .5),
            child: Column(
              children: [
                Image.asset('assets/no_quiz.png',
                    height: S.w * .4, width: S.w * .4),
                Text('Belum ada nilai'),
              ],
            )),
      );
    }
    return ListView.separated(
        itemBuilder: (context, int index) {
          var detail = items[index];
          return Container(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: S.w * 0.35,
                      child: Text(
                        detail.quizName.capitalizeFirstOfEach,
                        style: semiBlack.copyWith(fontSize: S.w / 30),
                      )),
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      width: S.w * 0.3,
                      child: Text(
                        formattedDate(detail.startDate),
                        style: descBlack.copyWith(fontSize: S.w / 30),
                      )),
                  Expanded(
                    child: Container(
                        child: Text(
                      '${!detail.hasBeenDone ? 'Belum dikerjakan' : detail.score == -1 ? 'Belum Dinilai' : detail.score.roundToDouble() == detail.score ? detail.score.round() : detail.score.toStringAsFixed(2)}',
                      style: descBlack.copyWith(fontSize: S.w / 30),
                    )),
                  ),
                ]),
          );
        },
        separatorBuilder: (context, int index) => Divider(),
        itemCount: items.length);
  }
}
