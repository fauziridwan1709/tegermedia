part of '_score.dart';

class ReportCardGrades extends StatefulWidget {
  @override
  _ReportCardGradesState createState() => _ReportCardGradesState();
}

class _ReportCardGradesState extends State<ReportCardGrades> {
  final authState = Injector.getAsReactive<AuthState>();
  List<DataReportCardGrades> _data = [];
  bool _isLoading = false;
  int _tahunAjaran = 0;
  var tahunAjaran = '';

  @override
  void initState() {
    super.initState();
    _getDataDetailSummaryScore();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _handleIsLoading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  void _getDataDetailSummaryScore({int tahun}) async {
    // var provider = Provider.of<UserProvider>(context, listen: false);
    _handleIsLoading();
    var _params = {
      'school_id': authState.state.currentState.schoolId.toString(),
      'tahun_ajaran':
          tahun == null ? listTahunAjaran()[0] : listTahunAjaran()[tahun],
    };
    var data = await ScoreServices.getReportCardGrades(params: _params);
    if (data.statusCode == 200) {
      _handleIsLoading();
      _data = data.data;
    } else {
      _handleIsLoading();
      _data = [];
    }
  }

  void _handleFilter() {
    _getDataDetailSummaryScore(tahun: _tahunAjaran);
    Navigator.pop(context);
  }

  void _handleCancelFilter() {
    setState(() {
      _tahunAjaran = 0;
    });
    _getDataDetailSummaryScore();
    Navigator.pop(context);
  }

  void _setStateTahunAjaran(int index) {
    setState(() {
      _tahunAjaran = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 2,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Container(
            height: AppBar().preferredSize.height - 20,
            width: S.w * .5,
            child: GestureDetector(
                onTap: () => modalFilter(context,
                    handleFilter: _handleFilter,
                    cancelFilter: _handleCancelFilter,
                    setStateTahunAjaran: _setStateTahunAjaran,
                    tahunAjaran: _tahunAjaran,
                    showSemester: false),
                child: Container(
                    padding: EdgeInsets.symmetric(vertical: 9, horizontal: 15),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(color: Colors.grey[300])),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '${listTahunAjaran()[_tahunAjaran]}',
                          style: descBlack.copyWith(fontSize: S.w / 32),
                        ),
                        Icon(Icons.keyboard_arrow_down,
                            size: S.w / 22, color: SiswamediaTheme.green)
                      ],
                    ))),
          ),
          actions: [
            AspectRatio(
              aspectRatio: 1,
              child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: SiswamediaTheme.green,
                    borderRadius: radius(6),
                  ),
                  child: InkWell(
                    onTap: () {},
                    child: Material(
                      color: Colors.transparent,
                      child: Center(
                          child:
                              Icon(Icons.download_sharp, color: Colors.white)),
                    ),
                  )),
            )
          ],
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        backgroundColor: Colors.white,
        body: Container(
            child: Column(
          children: [
            SizedBox(height: 20),
            Row(
              children: [
                Container(
                  width: S.w * .6,
                  child: CustomText(
                    'Mapel',
                    Kind.heading3,
                    align: TextAlign.center,
                  ),
                ),
                Container(
                  width: S.w * .4,
                  child: CustomText('Nilai', Kind.heading3,
                      align: TextAlign.center),
                  // decoration: BoxDecoration(
                  //   boxShadow: [BoxShadow(
                  //     offset: Offset(0,2),
                  //     color: Colors.grey.withOpacity(.5)
                  //   )]
                  // ),
                ),
              ],
            ),
            Divider(),
            Expanded(
                child: _isLoading
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: List.generate(
                            10,
                            (index) => SkeletonLine(
                                  width: S.w * 0.9,
                                )),
                      )
                    : _data.isEmpty
                        ? Center(
                            child: Padding(
                                padding: EdgeInsets.only(top: S.w * .5),
                                child: Column(
                                  children: [
                                    Image.asset('assets/no_quiz.png',
                                        height: S.w * .4, width: S.w * .4),
                                    Text('Belum ada nilai'),
                                  ],
                                )),
                          )
                        : ListView.separated(
                            itemBuilder: (context, int index) {
                              var detail = _data[index];
                              return Container(
                                padding:
                                    EdgeInsets.symmetric(horizontal: S.w * .05),
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: S.w * .6,
                                        child: Text(
                                          detail.namaMatapelajaran
                                              .capitalizeFirstOfEach,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                      Container(
                                          width: S.w * 0.3,
                                          child: Text(
                                            '${detail.nilaiRataRata}',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600),
                                            textAlign: TextAlign.center,
                                          )),
                                    ]),
                              );
                            },
                            separatorBuilder: (context, int index) => Divider(),
                            itemCount: _data.length))
          ],
        )));
  }
}
