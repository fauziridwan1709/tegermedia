import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/models/score/_score.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/pages/School/_school.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'detail_summary_score.dart';
part 'report_card_grades.dart';
part 'score.dart';
part 'students.dart';
part 'summary_score.dart';
