part of '_score.dart';

class RekapSiswa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authState = Injector.getAsReactive<AuthState>();
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          title: TitleAppbar(label: 'Daftar Siswa'),
        ),
        body: FutureBuilder<StudentsClassroomModel>(
          future: ClassServices.getStudentClassRoom(
              id: authState.state.currentState.classId),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.data.isEmpty) {
                return Center(
                    child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/dialog/sorry_no_student.png',
                          height: S.w * .6, width: S.w * .6),
                      SizedBox(height: 10),
                      CustomText('Belum ada siswa dikelasmu', Kind.descBlack),
                      SizedBox(height: 50 + AppBar().preferredSize.height),
                    ],
                  ),
                ));
              }
              return ListView(
                shrinkWrap: true,
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .01),
                children: snapshot.data.data
                    .map((e) => InkWell(
                          onTap: () => navigate(
                              context,
                              SummaryScore(
                                userId: e.userId,
                                isGuruView: true,
                                namaSiswa: e.fullName,
                              )),
                          child: Container(
                              margin:
                                  EdgeInsets.symmetric(vertical: S.w * .025),
                              padding: EdgeInsets.symmetric(
                                  horizontal: S.w * .05, vertical: S.w * .02),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: radius(12),
                                  boxShadow: [SiswamediaTheme.shadowContainer]),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Text(e.fullName),
                                    ),
                                    Icon(Icons.arrow_forward_ios_outlined,
                                        color: SiswamediaTheme.green),
                                  ])),
                        ))
                    .toList(),
              );
            }
            return Container();
          },
        ));
  }
}
