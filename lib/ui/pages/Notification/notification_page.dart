part of '../_pages.dart';

class NotificationPage extends StatefulWidget {
  NotificationPage({Key key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  final notificationState = GlobalState.notification();
  final authState = GlobalState.auth();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(TIME_LIST_CLASS)) {
        var date = value.getString(TIME_LIST_CLASS);
        if (isDifferenceLessThanFiveMin(date) &&
            notificationState.state.notificationModel != null) {
        } else {
          await value.setString(
              TIME_LIST_CLASS, DateTime.now().toIso8601String());
          await refreshIndicatorKey.currentState.show();
        }
      } else {
        await value.setString(
            TIME_LIST_CLASS, DateTime.now().toIso8601String());
        await refreshIndicatorKey.currentState.show();
      }
    });
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await SharedPreferences.getInstance().then((value) async {
        await notificationState.setState((s) async => await s.retrieveData(),
            catchError: true, onError: (context, dynamic error) {
          print((error as SiswamediaException).message);
        }, onData: (_, data) {
          notificationState.setState((s) => s.setCount(0));
        });
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  // void getDataCount() async {
  //   var isLogin = Provider.of<UserProvider>(context, listen: false).isLogin;
  //   var notifProvider =
  //       Provider.of<NotificationProvider>(context, listen: false);
  //
  //   if (isLogin) {
  //     var countNotif = await NotificationServices.getCountNotif();
  //     var data = countNotif.data;
  //     print(data.count);
  //
  //     if (countNotif.statusCode == 200) {
  //       notifProvider.setCount(data.count);
  //     } else {
  //       notifProvider.setCount(data.count);
  //     }
  //   }
  // }

  // void getData() async {
  //   var isLogin = Provider.of<UserProvider>(context, listen: false).isLogin;
  //   var notifProvider =
  //       Provider.of<NotificationProvider>(context, listen: false);
  //
  //   if (isLogin) {
  //     var result = await NotificationServices.getNotifications();
  //
  //     var data = result.data;
  //
  //     if (result.statusCode == 200) {
  //       getDataCount();
  //       notifProvider.setNotif(data);
  //     } else {
  //       notifProvider.setNotif([]);
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SiswamediaAppBar(
        context: context,
        title: 'Notifikasi',
      ),
      // AppBar(
      //   backgroundColor: Colors.white,
      //   shadowColor: Colors.transparent,
      //   centerTitle: true,
      //   title:
      //       Text('Notifikasi', style: semiBlack.copyWith(fontSize: S.w / 22)),
      //   leading: IconButton(
      //       icon: Icon(
      //         Icons.arrow_back,
      //         color: SiswamediaTheme.green,
      //       ),
      //       onPressed: () => Navigator.pop(context)),
      // ),
      body: StateBuilder<NotificationState>(
          observe: () => notificationState,
          builder: (context, snapshot) {
            return RefreshIndicator(
              key: refreshIndicatorKey,
              onRefresh: () async {
                await retrieveData();
              },
              child: WhenRebuilder<NotificationState>(
                observe: () => notificationState,
                onWaiting: () => WaitingView(),
                onError: (dynamic error) =>
                    ErrorView(error: (error is SiswamediaException)),
                onIdle: () => WaitingView(),
                onData: (data) {
                  return ListView.separated(
                    itemCount: data.notificationModel.length,
                    itemBuilder: (context, index) {
                      var detail = data.notificationModel[index];
                      return ListTile(
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                        title: Text(detail.title,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black)),
                        subtitle: Text(detail.body,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: Colors.black)),
                        onTap: () {
                          if (detail.type == 'new-assessment') {
                            return context.push<void>(MainAssignment(
                              role: authState.state.currentState.classRole,
                            ));
                          } else if (detail.type ==
                              'new-assessment-submission') {
                            return context.push<void>(MainAssignment(
                              role: authState.state.currentState.classRole,
                            ));
                          }
                        },
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider(color: Colors.grey.withOpacity(.3));
                    },
                  );
                },
              ),
            );
          }),
    );
  }
}
