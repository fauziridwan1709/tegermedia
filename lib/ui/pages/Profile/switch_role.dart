import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/core/_core_export.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/pages/School/_school.dart';

class SwitchRole extends StatefulWidget {
  @override
  _SwitchRoleState createState() => _SwitchRoleState();
}

class _SwitchRoleState extends State<SwitchRole> {
  final schoolState = GlobalState.school();
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    Initializer(
      rIndicator: _refreshIndicatorKey,
      state: schoolState.state.listSchool != null,
      reactiveModel: schoolState,
      cacheKey: 'time_list_school',
    ).initialize();
  }

  Future<void> retrieveData() async {
    await schoolState.setState((s) => s.retrieveData());
  }

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return WillPopScope(
        onWillPop: () async {
          Navigator.pop(context);
          return;
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            shadowColor: Colors.transparent,
            centerTitle: true,
            title: Text(
              'Ubah Role',
              style: boldBlack.copyWith(fontSize: S.w / 22, color: Colors.white),
            ),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [SiswamediaTheme.materialGreen[100], SiswamediaTheme.green],
                  begin: Alignment.topCenter,
                  end: Alignment(0, 1.5),
                ),
              ),
            ),
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          ),
          body: StateBuilder<SchoolState>(
            observe: () => schoolState,
            observeMany: [() => GlobalState.auth()],
            builder: (context, _) {
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: retrieveData,
                child: WhenRebuilder<SchoolState>(
                  observe: () => schoolState,
                  onIdle: () => WaitingView(),
                  onWaiting: () => WaitingView(),
                  onError: (dynamic error) => ErrorView(error: error),
                  onData: (data) {
                    var institution =
                        data.listSchool.where((element) => element.personal.isFalse).toList();
                    var personal =
                        data.listSchool.where((element) => element.personal.isTrue).toList();
                    return ListView(
                      children: [
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: 50, vertical: 24),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    height: S.w * .6,
                                    width: S.w * .6,
                                    margin: EdgeInsets.only(bottom: 12),
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/icons/profile/ganti_sekolah.png'))),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: MediaQuery.of(context).size.width *
                                            (isPortrait ? .0 : .2)),
                                    child: Text(
                                      'Ganti Role memungkinkan Anda untuk memiliki tempat pembelajaran lebih dari satu, sehingga Kamu tidak perlu logout untuk dapat mengakses tempat pembelajaran lainnya.',
                                      textAlign: TextAlign.center,
                                      style: descBlack.copyWith(
                                          fontSize: S.w / 32, color: Colors.black54),
                                    ),
                                  )
                                ])),
                        SchoolList(mySchool: institution),
                        SchoolList(mySchool: personal, isPersonal: true)
                      ],
                    );
                  },
                ),
              );
            },
          ),
        ));
  }
}
