import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/core/_core_export.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/pages/HomePage/_home.dart';
import 'package:tegarmedia/ui/pages/Profile/edit_profile_page.dart';
import 'package:tegarmedia/ui/pages/Profile/privacy%20policy.dart';
import 'package:tegarmedia/ui/pages/Profile/switch_role.dart';
import 'package:tegarmedia/ui/pages/Profile/term_and_conditions.dart';
import 'package:tegarmedia/ui/pages/Ujian/_ujian.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage();

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var authState = Injector.getAsReactive<AuthState>();
  var notificationState = Injector.getAsReactive<NotificationState>();
  var profileState = Injector.getAsReactive<ProfileState>();
  String fullName;
  String address;
  String noTelp;
  String provinsiID;
  String kabupatenID;
  bool isSudahLengkapProfile = false;

  var classState = Injector.getAsReactive<ClassState>();

  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _getProfile() async {
    // var data = await UsersServices.getMyProfile();
    setState(() {
      // if (((profile ?? UserDetail()).fullName ?? '').isNotEmpty &&
      //     ((profile ?? UserDetail()).detailAddress ?? '').isNotEmpty &&
      //     ((profile ?? UserDetail()).noTelp ?? '').isNotEmpty &&
      //     ((profile ?? UserDetail()).provinsiId ?? '').isNotEmpty &&
      //     ((profile ?? UserDetail()).kabupatenId ?? '').isNotEmpty) {
      //   isSudahLengkapProfile = true;
      // }
      if (((profileState.state.profile ?? DataUserProfile2()).name ?? '').isNotEmpty &&
          ((profileState.state.profile ?? DataUserProfile2()).address ?? '').isNotEmpty &&
          ((profileState.state.profile ?? DataUserProfile2()).phone ?? '').isNotEmpty &&
          ((profileState.state.profile ?? DataUserProfile2()).provinceCode ?? '').isNotEmpty &&
          ((profileState.state.profile ?? DataUserProfile2()).cityCode ?? '').isNotEmpty) {
        isSudahLengkapProfile = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;

    var auth = authState.state;
    return WillPopScope(
        onWillPop: () async {
          pop(context);
          return;
        },
        child: Scaffold(
            backgroundColor: Colors.white,
            body: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  title: Text('Profile', style: semiWhite.copyWith(fontSize: S.w / 22)),
                  expandedHeight: S.w * .68,
                  pinned: true,
                  backgroundColor: SiswamediaTheme.green,
                  centerTitle: true,
                  leading: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                        )),
                  ),
                  flexibleSpace: FlexibleSpaceBar(
                    background: Container(
                        height: 280,
                        width: S.w,
                        decoration: BoxDecoration(
                            color: Colors.lightGreen[100],
                            image: DecorationImage(
                                image: AssetImage('assets/BG_profile.png'), fit: BoxFit.cover)),
                        child: Stack(children: [
                          // Positioned(
                          //     child: SafeArea(
                          //         child: Container(
                          //   margin:
                          //       EdgeInsets.only(top: 20, left: defaultMargin),
                          //   child: InkWell(
                          //     onTap: () {
                          //       Navigator.pop(context);
                          //     },
                          //     child: Container(
                          //         padding: EdgeInsets.all(5),
                          //         decoration: BoxDecoration(
                          //           color: Colors.white,
                          //           shape: BoxShape.circle,
                          //         ),
                          //         child: Icon(
                          //           Icons.arrow_back,
                          //           color: Colors.black,
                          //         )),
                          //   ),
                          // ))),
                          StateBuilder<AuthState>(
                              // observe: () => GlobalState.profile(),
                              observeMany: [() => GlobalState.profile(), () => GlobalState.auth()],
                              // future:
                              //     Future.value(context.watch<UserProvider>().profile),
                              builder: (context, snapshot) {
                                var profile = profileState.state.profile;
                                if (profile != null) {
                                  return Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(height: 50),
                                      Center(
                                        child: Hero(
                                          tag: 'image',
                                          child: Material(
                                            color: Colors.transparent,
                                            child: Container(
                                                width: 80,
                                                height: 80,
                                                decoration: BoxDecoration(
                                                    color: Colors.transparent,
                                                    borderRadius: BorderRadius.circular(32),
                                                    image: DecorationImage(
                                                        image: profile
                                                                .profileImagePresignedUrl.isNotEmpty
                                                            ? NetworkImage(
                                                                profile.profileImagePresignedUrl,
                                                              )
                                                            : AssetImage(
                                                                'assets/profile-default.png'),
                                                        fit: BoxFit.cover))),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width - 2 * defaultMargin,
                                        child: Hero(
                                          tag: 'fullname',
                                          child: Material(
                                            color: Colors.transparent,
                                            child: Text(
                                              profile.name,
                                              maxLines: 2,
                                              textAlign: TextAlign.center,
                                              overflow: TextOverflow.clip,
                                              style: semiBlack.copyWith(fontSize: S.w / 20),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          if (auth.currentState.schoolName != null)
                                            Container(
                                              margin: EdgeInsets.symmetric(horizontal: S.w * .01),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: S.w * .02, vertical: S.w * .015),
                                              decoration: BoxDecoration(
                                                color: SiswamediaTheme.green,
                                                borderRadius: BorderRadius.circular(5),
                                              ),
                                              child: Text(
                                                auth.currentState.schoolName,
                                                maxLines: 2,
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.clip,
                                                style: semiWhite.copyWith(fontSize: S.w / 30),
                                              ),
                                            ),
                                          if (auth.currentState.schoolRole.isNotEmpty)
                                            Container(
                                              margin: EdgeInsets.symmetric(horizontal: S.w * .01),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: S.w * .02, vertical: S.w * .015),
                                              decoration: BoxDecoration(
                                                color: SiswamediaTheme.green,
                                                borderRadius: BorderRadius.circular(5),
                                              ),
                                              child: Text(
                                                auth.currentState.schoolRole.join(','),
                                                maxLines: 2,
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.clip,
                                                style: semiWhite.copyWith(fontSize: S.w / 30),
                                              ),
                                            )
                                        ],
                                      ),
                                      SizedBox(height: 10),
                                      if (auth.currentState.classRole == 'ORTU')
                                        InkWell(
                                          onTap: () {
                                            Clipboard.setData(ClipboardData(text: profile.code));
                                            return CustomFlushBar.eventFlushBar(
                                                'Kode disalin', context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.symmetric(horizontal: S.w * .01),
                                            padding: EdgeInsets.symmetric(
                                                horizontal: S.w * .02, vertical: S.w * .015),
                                            decoration: BoxDecoration(
                                              color: SiswamediaTheme.lightBlack,
                                              borderRadius: BorderRadius.circular(5),
                                            ),
                                            child: Text(
                                              profile.code,
                                              maxLines: 2,
                                              textAlign: TextAlign.center,
                                              overflow: TextOverflow.clip,
                                              style: semiWhite.copyWith(fontSize: S.w / 30),
                                            ),
                                          ),
                                        ),
                                    ],
                                  );
                                } else {
                                  return Center(
                                      child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SkeletonLine(
                                        width: 100,
                                        height: 100,
                                        borderRadius: 32,
                                      ),
                                      SizedBox(height: 10),
                                      SkeletonLine(width: 112, height: 25, borderRadius: 10),
                                    ],
                                  ));
                                }
                              })
                        ])),
                    // Image.asset(
                    //   'assets/materi_topnav_background.png', // <===   Add your own image to assets or use a .network image instead.
                    //   fit: BoxFit.cover,
                    // ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 16),
                        GestureDetector(
                            onTap: () => navigate(context, EditProfilePage()),
                            child: Container(
                                height: 80,
                                width: S.w - 16 * 2,
                                padding: EdgeInsets.symmetric(horizontal: 16),
                                decoration: BoxDecoration(boxShadow: [
                                  BoxShadow(
                                      blurRadius: 10,
                                      color: Colors.grey[300],
                                      spreadRadius: 1,
                                      offset: Offset(.5, .5))
                                ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  children: [
                                    Container(
                                        width: 60,
                                        height: 70,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage('assets/lengkapi_profile.png')))),
                                    SizedBox(width: 15),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: (profileState.state.profile != null &&
                                                  profileState.state.profile.name.isNotEmpty &&
                                                  profileState.state.profile.address.isNotEmpty &&
                                                  profileState.state.profile.phone.isNotEmpty &&
                                                  profileState
                                                      .state.profile.provinceCode.isNotEmpty &&
                                                  profileState.state.profile.cityCode.isNotEmpty) ==
                                              false
                                          ? [
                                              Text('Lengkapi Data Diri Anda',
                                                  style: TextStyle(
                                                      color: SiswamediaTheme.semiBoldText,
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w600)),
                                              SizedBox(height: 8),
                                              Text(
                                                  'Segera lengkapi data diri Anda, untuk dapat\nmenikmati semua fitur kami',
                                                  maxLines: 3,
                                                  style: TextStyle(fontSize: 10))
                                            ]
                                          : [Text('Ubah Data Diri')],
                                    )
                                  ],
                                ))),
                        SizedBox(height: 16),
                        GestureDetector(
                            onTap: () {
                              // navigate(context,)
                              navigate(context, SwitchRole(),
                                  settings: RouteSettings(name: '/switchrole'));
                            },
                            child: Container(
                              height: 45,
                              width: S.w - 16 * 2,
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              decoration: BoxDecoration(boxShadow: [
                                BoxShadow(
                                    blurRadius: 10,
                                    color: Colors.grey[300],
                                    spreadRadius: 1,
                                    offset: Offset(.5, .5))
                              ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                children: <Widget>[
                                  SizedBox(
                                    width: 24,
                                    height: 24,
                                    child: Icon(
                                      Icons.people_outline,
                                      color: Colors.black87,
                                      size: 24,
                                    ),
                                  ),
                                  SizedBox(width: 16),
                                  Text('Ganti Role / Daftar Institusi',
                                      style: descBlack.copyWith(
                                          color: SiswamediaTheme.semiBoldText,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600))
                                ],
                              ),
                            )),
                        SizedBox(height: 16),
                        Container(
                          // height: 45,
                          width: S.w - 16 * 2,
                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                                blurRadius: 10,
                                color: Colors.grey[300],
                                spreadRadius: 1,
                                offset: Offset(.5, .5))
                          ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: [
                              // Row(
                              //   children: <Widget>[
                              //     SizedBox(
                              //       width: 24,
                              //       height: 24,
                              //       child: Icon(
                              //         Icons.settings,
                              //         color: Colors.black87,
                              //         size: 24,
                              //       ),
                              //     ),
                              //     SizedBox(width: 16),
                              //     Expanded(
                              //         child: Container(
                              //       padding:
                              //           EdgeInsets.only(bottom: 12, top: 12),
                              //       decoration: BoxDecoration(
                              //           border: Border(
                              //               bottom: BorderSide(
                              //                   color: Colors.grey[200],
                              //                   width: 1))),
                              //       child: Text('Pengaturan',
                              //           style: blackTextFont.copyWith(
                              //               fontSize: 12,
                              //               fontWeight: FontWeight.w600)),
                              //     ))
                              //   ],
                              // ),
                              GestureDetector(
                                onTap: () async {
                                  String url() {
                                    if (Platform.isIOS) {
                                      return 'whatsapp://wa.me/+6281316180896/?text=Hai, Siswamedia';
                                    } else {
                                      return 'whatsapp://send?phone=+6281316180896&text=Hai, Siswamedia';
                                    }
                                  }

                                  await LaunchServices.launchInBrowser(url());
                                },
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 24,
                                      height: 24,
                                      child: Icon(
                                        Icons.help_outline,
                                        color: Colors.black87,
                                        size: 24,
                                      ),
                                    ),
                                    SizedBox(width: 16),
                                    Expanded(
                                        child: Container(
                                            padding: EdgeInsets.only(bottom: 12, top: 12),
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.grey[200], width: 1))),
                                            child: Text('Bantuan',
                                                style: descBlack.copyWith(
                                                    fontSize: 12,
                                                    color: SiswamediaTheme.semiBoldText,
                                                    fontWeight: FontWeight.w600))))
                                  ],
                                ),
                              ),
                              GestureDetector(
                                  onTap: () async {
                                    var telp = 'tel:081316180896';
                                    if (await canLaunch(telp)) {
                                      await launch(telp);
                                    } else {
                                      throw 'Could not launch $telp';
                                    }
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: 24,
                                        height: 24,
                                        child: Icon(
                                          Icons.call,
                                          color: Colors.black87,
                                          size: 24,
                                        ),
                                      ),
                                      SizedBox(width: 16),
                                      Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(bottom: 12, top: 12),
                                              decoration: BoxDecoration(
                                                  border: Border(
                                                      bottom: BorderSide(
                                                          color: Colors.grey[200], width: 1))),
                                              child: Text('Hubungi Kami',
                                                  style: descBlack.copyWith(
                                                      fontSize: 12,
                                                      color: SiswamediaTheme.semiBoldText,
                                                      fontWeight: FontWeight.w600)))),
                                    ],
                                  )),
                              GestureDetector(
                                onTap: () async {
                                  var link = 'https://siswamedia.com/';
                                  await Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (_) => WebViewHome(
                                                title: 'Tentang kami',
                                                url: link,
                                              )));
                                },
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 24,
                                      height: 24,
                                      child: Icon(
                                        Icons.info_outline,
                                        color: Colors.black87,
                                        size: 24,
                                      ),
                                    ),
                                    SizedBox(width: 16),
                                    Expanded(
                                        child: Container(
                                            padding: EdgeInsets.only(bottom: 12, top: 12),
                                            child: Text('Tentang Kami',
                                                style: descBlack.copyWith(
                                                    color: SiswamediaTheme.semiBoldText,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w600))))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 16),
                        Container(
                          // height: 45,
                          width: S.w - 16 * 2,
                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                                blurRadius: 10,
                                color: Colors.grey[300],
                                spreadRadius: 1,
                                offset: Offset(.5, .5))
                          ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: [
                              InkWell(
                                onTap: () => navigate(context, PrivacyPolicy()),
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 24,
                                      height: 24,
                                      child: Icon(
                                        Icons.list,
                                        color: Colors.black87,
                                        size: 24,
                                      ),
                                    ),
                                    SizedBox(width: 16),
                                    Expanded(
                                        child: Container(
                                            padding: EdgeInsets.only(bottom: 12, top: 12),
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: Colors.grey[200], width: 1))),
                                            child: Text('Kebijakan Privasi',
                                                style: descBlack.copyWith(
                                                    fontSize: 12,
                                                    color: SiswamediaTheme.semiBoldText,
                                                    fontWeight: FontWeight.w600))))
                                  ],
                                ),
                              ),
                              InkWell(
                                  onTap: () => navigate(context, TermAndConditions()),
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: 24,
                                        height: 24,
                                        child: Icon(
                                          Icons.list,
                                          color: Colors.black87,
                                          size: 24,
                                        ),
                                      ),
                                      SizedBox(width: 16),
                                      Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(bottom: 12, top: 12),
                                              decoration: BoxDecoration(
                                                  border: Border(
                                                      bottom: BorderSide(
                                                          color: Colors.grey[200], width: 1))),
                                              child: Text('Syarat dan Ketentuan',
                                                  style: descBlack.copyWith(
                                                      fontSize: 12,
                                                      color: SiswamediaTheme.semiBoldText,
                                                      fontWeight: FontWeight.w600)))),
                                    ],
                                  )),
                              InkWell(
                                  onTap: () {
                                    launch('mailto:info@siswamedia.com?subject=pengaduan');
                                    // Navigator.push<void>(
                                    //     context,
                                    //     MaterialPageRoute(
                                    //         builder: (_) => null));
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: 24,
                                        height: 24,
                                        child: Icon(
                                          Icons.email_outlined,
                                          color: Colors.black87,
                                          size: 24,
                                        ),
                                      ),
                                      SizedBox(width: 16),
                                      Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(bottom: 12, top: 12),
                                              decoration: BoxDecoration(
                                                  border: Border(
                                                      bottom: BorderSide(
                                                          color: Colors.grey[200], width: 1))),
                                              child: Text('Pengaduan',
                                                  style: descBlack.copyWith(
                                                      color: SiswamediaTheme.semiBoldText,
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w600)))),
                                    ],
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(height: 16),
                        InkWell(
                            onTap: () {
                              showGeneralDialog<void>(
                                  context: context,
                                  transitionDuration: ActionOverlay.normalDuration,
                                  barrierDismissible: true,
                                  barrierLabel: '',
                                  pageBuilder: (context, anim1, anim2) {
                                    return;
                                  },
                                  transitionBuilder: (_, anim1, anim2, child) {
                                    final normalCurve = Curves.easeOutBack.transform(anim1.value);
                                    return Transform(
                                      transform: Matrix4.translationValues(
                                          0, 150 * (1.0 - normalCurve), 0.0),
                                      child: QuizDialog(
                                        image: 'assets/logout_popup.png',
                                        title: 'Yakin Logout?',
                                        actions: ['Ya', 'Tidak'],
                                        type: DialogType.textImage,
                                        onTapFirstAction: () async {
                                          Navigator.pop(context);
                                          SiswamediaTheme.infoLoading(
                                              context: context, info: 'Logging out');
                                          await GlobalState.auth()
                                              .state
                                              .logout(context)
                                              .then((value) async {
                                            Navigator.pop(context);
                                            if (value == StatusCodeConst.success) {
                                              Navigator.pop(context);

                                              CustomFlushBar.successFlushBar(
                                                  'Berhasil Logout', context);
                                            } else if (value == StatusCodeConst.timeout) {
                                              CustomFlushBar.errorFlushBar(
                                                  'Gagal Logout, Coba lagi', context);
                                            } else if (value == StatusCodeConst.noInternet) {
                                              CustomFlushBar.errorFlushBar(
                                                  'Gagal Logout, tidak ada koneksi internet',
                                                  context);
                                            } else {
                                              CustomFlushBar.errorFlushBar(
                                                  'Terjadi kesalahan saat logout', context);
                                            }
                                          });
                                          //     .then((_) async {
                                          //   // Navigator.pop(
                                          //   //     globalKey.currentContext);
                                          // });
                                        },
                                        onTapSecondAction: () {
                                          Navigator.pop(context);
                                        },
                                        description:
                                            'Tekan ya jika Anda ingin logout untuk mengganti akun Anda',
                                      ),
                                    );
                                  });
                            },
                            child: Container(
                              height: 45,
                              width: S.w - 16 * 2,
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              decoration: BoxDecoration(boxShadow: [
                                BoxShadow(
                                    blurRadius: 10,
                                    color: Colors.grey[300],
                                    spreadRadius: 1,
                                    offset: Offset(.5, .5))
                              ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                children: <Widget>[
                                  SizedBox(
                                    width: 24,
                                    height: 24,
                                    child: Icon(
                                      Icons.power_settings_new_sharp,
                                      size: 24,
                                      color: Colors.black87,
                                    ),
                                  ),
                                  SizedBox(width: 16),
                                  Text(
                                    'Keluar Akun',
                                    style: boldBlack.copyWith(
                                        color: Colors.red,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            )),
                        SizedBox(height: 150)
                      ],
                    ))
                  ]),
                ),
              ],
            )));
  }
}
