import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/aw/widgets/custom/_custom.dart';
import 'package:tegarmedia/core/_core_export.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

class EditProfilePage extends StatefulWidget {
  EditProfilePage();

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final profileState = Injector.getAsReactive<ProfileState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  List<DataWilayah> provinsi = <DataWilayah>[];
  List<DataWilayah> kabupaten = <DataWilayah>[];

  TextEditingController _fullName;
  TextEditingController _address;
  TextEditingController _email;
  TextEditingController _noTelp;

  DataUserProfile2 user = DataUserProfile2(profileImagePresignedUrl: '');
  BindAccount bindAccount;
  bool isLoading = false;
  bool isLoadingKab = true;
  bool _isLoadingUpdate = false;

  String provinsiID = '';
  String kabupatenID = '';
  String gender = 'male';

  @override
  void initState() {
    super.initState();
    getProfile();
    getProvinsi();
    _fullName = TextEditingController();
    _address = TextEditingController();
    _email = TextEditingController();
    _noTelp = TextEditingController();
    if (mounted) {
      provinsiID = null;
      kabupatenID = null;
      provinsi = [];
      kabupaten = [];
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    _fullName.dispose();
    _address.dispose();
    _email.dispose();
    _noTelp.dispose();

    super.dispose();
  }

  void getProfile() async {
    setState(() {
      isLoading = true;
    });
    var data = await ProfileServices.getProfile();
    var binding = await ProfileServices.getAllBindAccount();
    if (data != null) {
      setState(() {
        user = data.data;
        bindAccount = binding;
        if (user.gender != null) {
          // gender = user.gender;
        }
        _fullName = TextEditingController(text: data.data.name);
        _address = TextEditingController(text: data.data.address);
        _email = TextEditingController(text: data.data.email);
        _noTelp = TextEditingController(text: data.data.phone);
        provinsiID = data.data.provinceCode == '' ? null : data.data.provinceCode;
        isLoading = false;
        if (data.data.provinceCode != '' || data.data.provinceCode != null) {
          getKota(data.data.provinceCode).then((value) {
            kabupatenID = data.data.cityCode == '' ? null : data.data.cityCode;
          });
        }
      });
    }
  }

  Future<void> getProvinsi() async {
    var wilayah = await WilayahServices.getWilayah();
    var data = wilayah;

    if (data.statusCode == 200) {
      setState(() {
        provinsi = data.data;
        kabupaten = [];
        kabupatenID = null;
      });
    } else {
      setState(() {
        provinsi = [];
        kabupaten = [];
        provinsiID = null;
      });
    }
  }

  Future<void> getKota(String id) async {
    dynamic wilayah = await WilayahServices.getKabupaten(id);
    var data = ModelListWilayah.fromJson(wilayah);
    setState(() {
      isLoadingKab = true;
    });
    print(data.statusCode);
    if (data.statusCode == 200) {
      print(data.data);
      setState(() {
        kabupaten = data.data;
      });
    } else {
      setState(() {
        kabupaten = [];
        kabupatenID = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    // var auth = context.watch<UserProvider>();
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: TitleAppbar(label: 'Data Diri'),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      backgroundColor: SiswamediaTheme.white,
      body: bindAccount == null
          ? Center(child: CircularProgressIndicator())
          : ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      width: S.w - 16 * 2,
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                            blurRadius: 10,
                            color: SiswamediaTheme.border,
                            spreadRadius: 1,
                            offset: Offset(.5, .5))
                      ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText('Data Profile', Kind.heading2),
                          SizedBox(
                            height: 18,
                          ),
                          StateBuilder<ProfileState>(
                              observe: () => profileState,
                              builder: (context, snapshot) {
                                return GestureDetector(
                                    onTap: () async {
                                      await ProfileServices.uploadPhotoProfile(context, gender)
                                          .then((value) async {});
                                      // await Injector.getAsReactive<ProfileState>().notify();
                                    },
                                    child: isLoading
                                        ? Padding(
                                            padding: EdgeInsets.only(bottom: 10),
                                            child: SkeletonLine(
                                              height: 100,
                                              width: 100,
                                              borderRadius: 10,
                                            ),
                                          )
                                        : Stack(children: [
                                            Container(
                                                margin: EdgeInsets.only(bottom: 10),
                                                width: 100,
                                                height: 100,
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(10),
                                                    image: DecorationImage(
                                                        image: NetworkImage(profileState.state
                                                            .profile.profileImagePresignedUrl),
                                                        fit: BoxFit.cover))),
                                            Container(
                                              height: 100,
                                              width: 100,
                                              decoration: BoxDecoration(
                                                  color: Colors.black.withOpacity(0.2),
                                                  borderRadius: BorderRadius.circular(10)),
                                              child: Center(
                                                child: Container(
                                                    padding: EdgeInsets.all(10),
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Colors.white.withOpacity(0.8)),
                                                    child: Icon(
                                                      Icons.edit,
                                                      color: Colors.red,
                                                    )),
                                              ),
                                            ),
                                          ]));
                              }),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              child: TextField(
                                controller: _fullName,
                                style: normalText(context),
                                decoration: textField(context, hint: 'Nama Lengkap...')
                                    .copyWith(prefixIcon: Icon(Icons.people_alt_outlined)),
                              )),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              child: DropdownButtonFormField<String>(
                                value: gender,
                                icon: Icon(Icons.keyboard_arrow_down,
                                    size: 30, color: SiswamediaTheme.green),
                                iconSize: 24,
                                elevation: 16,
                                decoration:
                                    textField(context, hint: 'Pilih Jenis kelamin').copyWith(
                                        prefixIcon: Icon(gender == 'male'
                                            ? MdiIcons.genderMale
                                            : gender == 'female'
                                                ? MdiIcons.genderFemale
                                                : MdiIcons.genderMaleFemale)),
                                style: greenText(context),
                                onChanged: (String newValue) {
                                  setState(() {
                                    gender = newValue;
                                  });
                                },
                                items: [
                                  DropdownMenuItem(
                                    value: '',
                                    child: Text('Pilih Jenis Kelamin'),
                                  ),
                                  DropdownMenuItem(
                                    value: 'male',
                                    child: Text('Laki-Laki'),
                                  ),
                                  DropdownMenuItem(
                                    value: 'female',
                                    child: Text('Perempuan'),
                                  ),
                                ],
                              )),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              child: TextField(
                                controller: _noTelp,
                                style: normalText(context),
                                decoration: textField(context, hint: 'Nomor Telepon...')
                                    .copyWith(prefixIcon: Icon(MdiIcons.phoneOutline)),
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                              )),
                          if (bindAccount.data
                              .where((element) => element.type == 'apple')
                              .isNotEmpty)
                            Container(
                              width: S.w,
                              margin: EdgeInsets.symmetric(vertical: 5),
                              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
                              decoration: BoxDecoration(
                                  color: SiswamediaTheme.border,
                                  borderRadius: BorderRadius.circular(12),
                                  border: Border.all(color: SiswamediaTheme.border)),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(bindAccount.data
                                        .where((element) => element.type == 'apple')
                                        .first
                                        .email)
                                  ]),
                            ),
                          //todo masbro
                          SizedBox(height: 10),
                          bindAccount.data.where((element) => element.type == 'gmail').isEmpty
                              ? Center(
                                  child: CustomContainer(
                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                    color: SiswamediaTheme.green,
                                    onTap: () {
                                      ProfileServices.bindGMail(context);
                                      setState(() {});
                                    },
                                    child: Text(
                                      'bind gmail',
                                      style: semiWhite.copyWith(fontSize: S.w / 22),
                                    ),
                                  ),
                                )
                              : Container(
                                  width: S.w,
                                  margin: EdgeInsets.symmetric(vertical: 5),
                                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
                                  decoration: BoxDecoration(
                                      color: SiswamediaTheme.border,
                                      borderRadius: BorderRadius.circular(12),
                                      border: Border.all(color: SiswamediaTheme.border)),
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(bindAccount.data
                                            .where((element) => element.type == 'gmail')
                                            .first
                                            .email)
                                      ]),
                                ),
                        ],
                      ),
                    ),
                    Container(
                      // height: 40,
                      width: S.w - 16 * 2,
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                            blurRadius: 10,
                            color: SiswamediaTheme.border,
                            spreadRadius: 1,
                            offset: Offset(.5, .5))
                      ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText('Detail Alamat', Kind.heading2),
                          SizedBox(
                            height: 18,
                          ),
                          // Container(
                          //     margin: EdgeInsets.symmetric(vertical: 5),
                          //     child: DropdownButtonFormField<String>(
                          //       value: provinsiID,
                          //       icon: Icon(
                          //         Icons.keyboard_arrow_down,
                          //         size: 30,
                          //         color: SiswamediaTheme.green,
                          //       ),
                          //       iconSize: 24,
                          //       elevation: 16,
                          //       decoration: textField(context)
                          //           .copyWith(prefixIcon: Icon(Icons.location_pin)),
                          //       style: normalText(context),
                          //       hint:
                          //           Text('Pilih Provinsi', style: hintText(context)),
                          //       onChanged: (String newValue) {
                          //         setState(() {
                          //           provinsiID = newValue;
                          //           kabupaten = [];
                          //           kabupatenID = null;
                          //         });
                          //         getKota(newValue);
                          //       },
                          //       items: provinsi.map((item) {
                          //         return DropdownMenuItem(
                          //           child: Text(item.nama),
                          //           value: item.id.toString(),
                          //         );
                          //       }).toList(),
                          //     )),
                          // Container(
                          //     margin: EdgeInsets.symmetric(vertical: 5),
                          //     child: DropdownButtonFormField<String>(
                          //       hint: Text(
                          //         'Pilih Kota',
                          //         style: hintText(context),
                          //       ),
                          //       value: kabupatenID,
                          //       icon: Icon(Icons.keyboard_arrow_down,
                          //           size: 30, color: SiswamediaTheme.green),
                          //       iconSize: 24,
                          //       elevation: 16,
                          //       decoration: textField(context)
                          //           .copyWith(prefixIcon: Icon(MdiIcons.city)),
                          //       style: normalText(context),
                          //       onChanged: (String newValue) {
                          //         setState(() {
                          //           kabupatenID = newValue;
                          //         });
                          //       },
                          //       items: kabupaten.map((item) {
                          //         return DropdownMenuItem(
                          //           child: Text(item.nama),
                          //           value: item.id.toString(),
                          //         );
                          //       }).toList(),
                          //     )),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              child: TextField(
                                controller: _address,
                                maxLines: 4,
                                style: normalText(context),
                                decoration: textField(context, hint: 'Alamat...'),
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
                _isLoadingUpdate
                    ? Center(child: CircularProgressIndicator())
                    : Container(
                        width: S.w,
                        margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 20),
                        height: S.w * .12,
                        child: RaisedButton(
                          elevation: 0,
                          // padding: EdgeInsets.symmetric(vertical: 10),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                          color: SiswamediaTheme.green,

                          onPressed: () async {
                            setState(() => _isLoadingUpdate = true);
                            user.name = _fullName.text;
                            user.provinceCode = provinsiID;
                            user.cityCode = kabupatenID;
                            user.phone = _noTelp.text;
                            user.gender = gender;
                            user.address = _address.text;

                            var model = <String, dynamic>{
                              'name': _fullName.text,
                              'gender': gender,
                              'phone': _noTelp.text,
                              'address': _address.text,
                              'profile_image_id': profileState.state.profile.profileImageId
                            };

                            var result = await ProfileServices.updateProfile(model);

                            result.keys.first.translate<void>(ifSuccess: () async {
                              setState(() => _isLoadingUpdate = false);
                              CustomFlushBar.successFlushBar(
                                'Data diri berhasil di perbaharui',
                                context,
                              );
                              await Injector.getAsReactive<ProfileState>()
                                  .setState((s) => s.retrieveData());
                            }, ifElse: () {
                              setState(() => _isLoadingUpdate = false);
                              CustomFlushBar.errorFlushBar(
                                'Data yang anda masukkan tidak valid',
                                context,
                              );
                            });
                          },
                          child: Text(
                            'Simpan Data Diri',
                            style: semiWhite.copyWith(
                                fontSize: S.w / 28,
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
              ],
            ),
    );
  }
}
