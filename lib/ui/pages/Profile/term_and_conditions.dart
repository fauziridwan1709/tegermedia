import 'package:flutter/material.dart';
import 'package:tegarmedia/core/_core_export.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

class TermAndConditions extends StatefulWidget {
  TermAndConditions();

  @override
  _TermAndConditionsState createState() => _TermAndConditionsState();
}

class _TermAndConditionsState extends State<TermAndConditions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Syarat dan Ketentuan'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        backgroundColor: Color(0xFFF9F9F9),
        body: FutureBuilder<ModelTermAndConditions>(
            future: TermAndConditionsServices.getData(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    itemCount: snapshot.data.rows.length,
                    itemBuilder: (context, index) {
                      var obj = snapshot.data.rows[index];

                      return Container(
                        margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .01),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 0),
                                spreadRadius: .5,
                                blurRadius: 2,
                                color: Colors.grey[300],
                              )
                            ]),
                        child: ExpansionTile(
                          title: Text(
                            obj.subtopik,
                            style: TextStyle(fontSize: S.w / 30),
                          ),
                          childrenPadding:
                              EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 10),
                          children: [Text(obj.isi)],
                        ),
                      );
                    });
              } else {
                return Column(
                  children: List.generate(5, (index) => SkeletonScreen()),
                );
              }
            }));
  }
}
