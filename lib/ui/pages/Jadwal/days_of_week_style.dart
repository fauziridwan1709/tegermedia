part of '_calendar.dart';

class DaysOfWeekStyle {
  final TextBuilder dowTextBuilder;

  final TextStyle weekdayStyle;

  final TextStyle weekendStyle;

  const DaysOfWeekStyle({
    this.dowTextBuilder,
    this.weekdayStyle =
        const TextStyle(color: const Color(0xFF616161)), // Material grey[700]
    this.weekendStyle =
        const TextStyle(color: Colors.white), // Material red[500]
  });
}
