part of "_jadwal.dart";

class DayView extends ZoomableHeadersWidget<DayViewStyle, DayViewController> {
  final List<FlutterWeekViewEvent> events;

  final DateTime date;

  final DayBarStyle dayBarStyle;

  DayView({
    List<FlutterWeekViewEvent> events,
    @required DateTime date,
    DayViewStyle style,
    HoursColumnStyle hoursColumnStyle,
    DayBarStyle dayBarStyle,
    DayViewController controller,
    bool inScrollableWidget,
    HourMinute minimumTime,
    HourMinute maximumTime,
    HourMinute initialTime,
    bool userZoomable,
    CurrentTimeIndicatorBuilder currentTimeIndicatorBuilder,
    HoursColumnTapCallback onHoursColumnTappedDown,
    DayBarTapCallback onDayBarTappedDown,
  })  : assert(date != null),
        date = date.yearMonthDay,
        events = events ?? [],
        dayBarStyle = dayBarStyle ?? DayBarStyle.fromDate(date: date),
        super(
          style: style ?? DayViewStyle.fromDate(date: date),
          hoursColumnStyle: hoursColumnStyle ?? const HoursColumnStyle(),
          controller: controller ?? DayViewController(),
          inScrollableWidget: inScrollableWidget ?? true,
          minimumTime: minimumTime ?? HourMinute.MIN,
          maximumTime: maximumTime ?? HourMinute.MAX,
          initialTime: initialTime?.atDate(date) ??
              (Utils.sameDay(date) ? HourMinute.now() : const HourMinute())
                  .atDate(date),
          userZoomable: userZoomable ?? true,
          currentTimeIndicatorBuilder: currentTimeIndicatorBuilder ??
              DefaultBuilders.defaultCurrentTimeIndicatorBuilder,
          onHoursColumnTappedDown: onHoursColumnTappedDown,
          onDayBarTappedDown: onDayBarTappedDown,
        );

  @override
  State<StatefulWidget> createState() => _DayViewState();
}

class _DayViewState extends ZoomableHeadersWidgetState<DayView> {
  final Map<FlutterWeekViewEvent, EventDrawProperties> eventsDrawProperties =
      HashMap();

  List<FlutterWeekViewEvent> events;

  @override
  void initState() {
    super.initState();
    scheduleScrollToInitialTime();
    reset();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) {
        setState(createEventsDrawProperties);
      }
    });
  }

  @override
  void didUpdateWidget(DayView oldWidget) {
    super.didUpdateWidget(oldWidget);

    reset();
    createEventsDrawProperties();
  }

  @override
  Widget build(BuildContext context) {
    Widget mainWidget = createMainWidget();
    if (widget.style.headerSize > 0 || widget.hoursColumnStyle.width > 0) {
      mainWidget = Stack(
        children: [
          mainWidget,
        ],
      );
    }

    if (!isZoomable) {
      return mainWidget;
    }

    return GestureDetector(
      onScaleStart: (_) => widget.controller.scaleStart(),
      onScaleUpdate: widget.controller.scaleUpdate,
      child: mainWidget,
    );
  }

  @override
  void onZoomFactorChanged(
      DayViewController controller, ScaleUpdateDetails details) {
    super.onZoomFactorChanged(controller, details);

    if (mounted) {
      setState(createEventsDrawProperties);
    }
  }

  @override
  DayViewStyle get currentDayViewStyle => widget.style;

  Widget createMainWidget() {
    List<Widget> children = eventsDrawProperties.entries
        .map((entry) => entry.value.createWidget(context, widget, entry.key))
        .toList();
    if (widget.hoursColumnStyle.width > 0) {
      children.add(Positioned(
        top: 0,
        left: 0,
        child: HoursColumn.fromHeadersWidgetState(parent: this),
      ));
    }

    if (Utils.sameDay(widget.date) &&
        widget.minimumTime.atDate(widget.date).isBefore(DateTime.now()) &&
        widget.maximumTime.atDate(widget.date).isAfter(DateTime.now())) {
      Widget currentTimeIndicator = (widget.currentTimeIndicatorBuilder ??
              DefaultBuilders.defaultCurrentTimeIndicatorBuilder)(
          widget.style, calculateTopOffset, widget.hoursColumnStyle.width);
      if (currentTimeIndicator != null) {
        children.add(currentTimeIndicator);
      }
    }

    Widget mainWidget = SizedBox(
      height: calculateHeight(),
      child: Stack(children: children..insert(0, createBackground())),
    );

    if (verticalScrollController != null) {
      mainWidget = NoGlowBehavior.noGlow(
        child: SingleChildScrollView(
          controller: verticalScrollController,
          child: mainWidget,
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.only(top: 0),
      child: mainWidget,
    );
  }

  Widget createBackground() => Positioned.fill(
        child: CustomPaint(
          painter: widget.style.createBackgroundPainter(
            dayView: widget,
            topOffsetCalculator: calculateTopOffset,
          ),
        ),
      );

  void reset() {
    eventsDrawProperties.clear();
    events = List.of(widget.events)..sort();
  }

  void createEventsDrawProperties() {
    EventGrid eventsGrid = EventGrid();
    for (FlutterWeekViewEvent event in List.of(events)) {
      EventDrawProperties drawProperties =
          eventsDrawProperties[event] ?? EventDrawProperties(widget, event);
      if (!drawProperties.shouldDraw) {
        events.remove(event);
        continue;
      }

      drawProperties.calculateTopAndHeight(calculateTopOffset);
      if (drawProperties.left == null || drawProperties.width == null) {
        eventsGrid.add(drawProperties);
      }

      eventsDrawProperties[event] = drawProperties;
    }

    if (eventsGrid.drawPropertiesList.isNotEmpty) {
      double eventsColumnWidth =
          (context.findRenderObject() as RenderBox).size.width -
              widget.hoursColumnStyle.width;
      eventsGrid.processEvents(
          widget.hoursColumnStyle.width, eventsColumnWidth);
    }
  }
}
