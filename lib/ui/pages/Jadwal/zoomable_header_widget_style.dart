part of "_jadwal.dart";

typedef DateFormatter = String Function(int year, int month, int day);

typedef TimeFormatter = String Function(HourMinute time);

typedef VerticalDividerBuilder = VerticalDivider Function(DateTime date);

class ZoomableHeaderWidgetStyle {
  final double headerSize;

  const ZoomableHeaderWidgetStyle({
    double headerSize,
  }) : headerSize = (headerSize ?? 40) < 0 ? 0 : (headerSize ?? 40);
}
