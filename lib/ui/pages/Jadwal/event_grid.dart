part of "_jadwal.dart";

class EventGrid {
  List<EventDrawProperties> drawPropertiesList = [];

  void add(EventDrawProperties drawProperties) =>
      drawPropertiesList.add(drawProperties);

  void processEvents(double hoursColumnWidth, double eventsColumnWidth) {
    List<List<EventDrawProperties>> columns = [];
    DateTime lastEventEnding;
    for (EventDrawProperties drawProperties in drawPropertiesList) {
      if (lastEventEnding != null &&
          drawProperties.start.isAfter(lastEventEnding)) {
        packEvents(columns, hoursColumnWidth, eventsColumnWidth);
        columns.clear();
        lastEventEnding = null;
      }

      bool placed = false;
      for (List<EventDrawProperties> column in columns) {
        if (!column.last.collidesWith(drawProperties)) {
          column.add(drawProperties);
          placed = true;
          break;
        }
      }

      if (!placed) {
        columns.add([drawProperties]);
      }

      if (lastEventEnding == null ||
          drawProperties.end.compareTo(lastEventEnding) > 0) {
        lastEventEnding = drawProperties.end;
      }
    }

    if (columns.isNotEmpty) {
      packEvents(columns, hoursColumnWidth, eventsColumnWidth);
    }
  }

  void packEvents(List<List<EventDrawProperties>> columns,
      double hoursColumnWidth, double eventsColumnWidth) {
    for (int columnIndex = 0; columnIndex < columns.length; columnIndex++) {
      List<EventDrawProperties> column = columns[columnIndex];
      for (EventDrawProperties drawProperties in column) {
        drawProperties.left = hoursColumnWidth +
            (columnIndex / columns.length) * eventsColumnWidth;
        int colSpan = calculateColSpan(columns, drawProperties, columnIndex);
        drawProperties.width = (eventsColumnWidth * colSpan) / (columns.length);
      }
    }
  }

  int calculateColSpan(List<List<EventDrawProperties>> columns,
      EventDrawProperties drawProperties, int column) {
    int colSpan = 1;
    for (int columnIndex = column + 1;
        columnIndex < columns.length;
        columnIndex++) {
      List<EventDrawProperties> column = columns[columnIndex];
      for (EventDrawProperties other in column) {
        if (drawProperties.collidesWith(other)) {
          return colSpan;
        }
      }
      colSpan++;
    }

    return colSpan;
  }
}

class EventDrawProperties {
  double top;

  double height;

  double left;

  double width;

  DateTime start;

  DateTime end;

  EventDrawProperties(DayView dayView, FlutterWeekViewEvent event) {
    DateTime minimum = dayView.minimumTime.atDate(dayView.date);
    DateTime maximum = dayView.maximumTime.atDate(dayView.date);

    if (shouldDraw ||
        (event.start.isBefore(minimum) && event.end.isBefore(minimum)) ||
        (event.start.isAfter(maximum) && event.end.isAfter(maximum))) {
      return;
    }

    start = event.start;
    end = event.end;

    if (start.isBefore(minimum)) {
      start = minimum;
    }

    if (end.isAfter(maximum)) {
      end = maximum;
    }
  }

  bool get shouldDraw => start != null && end != null;

  void calculateTopAndHeight(
      double Function(HourMinute time, {HourMinute minimumTime})
          topOffsetCalculator) {
    top = topOffsetCalculator(HourMinute.fromDateTime(dateTime: start));
    height = topOffsetCalculator(
            HourMinute.fromDuration(duration: end.difference(start)),
            minimumTime: HourMinute.MIN) +
        1;
  }

  bool collidesWith(EventDrawProperties other) {
    if (!shouldDraw || !other.shouldDraw) {
      return false;
    }

    return end.isAfter(other.start) && start.isBefore(other.end);
  }

  Widget createWidget(
          BuildContext context, DayView dayView, FlutterWeekViewEvent event) =>
      Positioned(
        top: top,
        height: height,
        left: left,
        width: width,
        child: event.build(context, dayView, height, width),
      );
}
