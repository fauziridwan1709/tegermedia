part of '_calendar.dart';

class _CustomIconButton extends StatelessWidget {
  final VoidCallback onTap;
  final EdgeInsets margin;
  final AnimationController controller;
  final Animation animation;

  const _CustomIconButton({
    Key key,
    @required this.onTap,
    this.animation,
    this.margin,
    this.controller,
  })  : assert(onTap != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(100.0),
        child: AnimatedBuilder(
            animation: controller,
            builder: (context, snapshot) {
              return Transform.rotate(
                angle: animation.value,
                child: Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.white,
                ),
              );
            }),
      ),
    );
  }
}
