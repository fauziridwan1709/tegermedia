part of '_calendar.dart';

class HeaderStyle {
  final bool centerHeaderTitle;

  final bool formatButtonVisible;

  final bool formatButtonShowsNext;

  final TextBuilder titleTextBuilder;

  final TextStyle titleTextStyle;

  final TextStyle formatButtonTextStyle;

  final Decoration formatButtonDecoration;

  final EdgeInsets headerPadding;

  final EdgeInsets headerMargin;

  final EdgeInsets formatButtonPadding;

  final EdgeInsets leftChevronPadding;

  final EdgeInsets rightChevronPadding;

  final EdgeInsets leftChevronMargin;

  final EdgeInsets rightChevronMargin;

  final Icon leftChevronIcon;

  final Icon rightChevronIcon;

  final BoxDecoration decoration;

  const HeaderStyle({
    this.centerHeaderTitle = false,
    this.formatButtonVisible = true,
    this.formatButtonShowsNext = true,
    this.titleTextBuilder,
    this.titleTextStyle = const TextStyle(fontSize: 17.0),
    this.formatButtonTextStyle = const TextStyle(),
    this.formatButtonDecoration = const BoxDecoration(
      border: const Border(
          top: BorderSide(),
          bottom: BorderSide(),
          left: BorderSide(),
          right: BorderSide()),
      borderRadius: const BorderRadius.all(Radius.circular(12.0)),
    ),
    this.headerMargin,
    this.headerPadding = const EdgeInsets.symmetric(vertical: 8.0),
    this.formatButtonPadding =
        const EdgeInsets.symmetric(horizontal: 10.0, vertical: 4.0),
    this.leftChevronPadding = const EdgeInsets.all(12.0),
    this.rightChevronPadding = const EdgeInsets.all(12.0),
    this.leftChevronMargin = const EdgeInsets.symmetric(horizontal: 8.0),
    this.rightChevronMargin = const EdgeInsets.symmetric(horizontal: 8.0),
    this.leftChevronIcon = const Icon(Icons.chevron_left, color: Colors.black),
    this.rightChevronIcon =
        const Icon(Icons.chevron_right, color: Colors.black),
    this.decoration = const BoxDecoration(),
  });
}
