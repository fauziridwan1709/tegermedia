part of '_jadwal.dart';

class DayBarStyle {
  final DateFormatter dateFormatter;

  final TextStyle textStyle;

  final Color color;

  final Decoration decoration;

  final Alignment textAlignment;

  const DayBarStyle({
    DateFormatter dateFormatter,
    this.textStyle,
    Color color,
    this.decoration,
    Alignment textAlignment,
  })  : dateFormatter = dateFormatter ?? DefaultBuilders.defaultDateFormatter,
        color = color ?? const Color(0xFFEBEBEB),
        textAlignment = textAlignment ?? Alignment.center;

  DayBarStyle.fromDate({
    @required DateTime date,
    DateFormatter dateFormatter,
    TextStyle textStyle,
    Color color,
    Decoration decoration,
    Alignment textAlignment,
  }) : this(
          dateFormatter: dateFormatter,
          textStyle: textStyle ??
              TextStyle(
                color: Utils.sameDay(date) ? Colors.blue[800] : Colors.black54,
                fontWeight: FontWeight.bold,
              ),
          color: color,
          decoration: decoration,
          textAlignment: textAlignment,
        );

  DayBarStyle copyWith({
    DateFormatter dateFormatter,
    TextStyle textStyle,
    Color color,
    Decoration decoration,
    Alignment textAlignment,
  }) =>
      DayBarStyle(
        dateFormatter: dateFormatter ?? this.dateFormatter,
        textStyle: textStyle ?? this.textStyle,
        color: color ?? this.color,
        decoration: decoration ?? this.decoration,
        textAlignment: textAlignment ?? this.textAlignment,
      );
}
