part of "_jadwal.dart";

class HourMinute {
  static const HourMinute ZERO = HourMinute._internal(hour: 0, minute: 0);

  static const HourMinute MIN = ZERO;

  static const HourMinute MAX = HourMinute._internal(hour: 24, minute: 0);

  final int hour;

  final int minute;

  const HourMinute._internal({
    @required this.hour,
    @required this.minute,
  });

  const HourMinute({
    int hour = 0,
    int minute = 0,
  }) : this._internal(
          hour: hour == null ? 0 : (hour < 0 ? 0 : (hour > 23 ? 23 : hour)),
          minute: minute == null
              ? 0
              : (minute < 0 ? 0 : (minute > 59 ? 59 : minute)),
        );

  HourMinute.fromDateTime({
    @required DateTime dateTime,
  }) : this._internal(hour: dateTime.hour, minute: dateTime.minute);

  factory HourMinute.fromDuration({
    @required Duration duration,
  }) {
    int hour = 0;
    int minute = duration.inMinutes;
    while (minute >= 60) {
      hour += 1;
      minute -= 60;
    }
    return HourMinute._internal(hour: hour, minute: minute);
  }

  HourMinute.now() : this.fromDateTime(dateTime: dateTimeGetter.now());

  HourMinute add(HourMinute other) {
    int hour = this.hour + other.hour;
    int minute = this.minute + other.minute;
    while (minute > 59) {
      hour++;
      minute -= 60;
    }
    return HourMinute._internal(hour: hour, minute: minute);
  }

  HourMinute subtract(HourMinute other) {
    int hour = math.max(this.hour - other.hour, 0);
    int minute = this.minute - other.minute;
    while (minute < 0) {
      if (hour == 0) {
        return HourMinute.ZERO;
      }
      hour--;
      minute += 60;
    }
    return HourMinute._internal(hour: hour, minute: minute);
  }

  @override
  String toString() => jsonEncode({'hour': hour, 'minute': minute});

  @override
  bool operator ==(dynamic other) {
    if (other is! HourMinute) {
      return false;
    }
    return identical(this, other) ||
        (hour == other.hour && minute == other.minute);
  }

  bool operator <(dynamic other) {
    if (other is! HourMinute) {
      return false;
    }

    return _calculateDifference(other) < 0;
  }

  bool operator <=(dynamic other) {
    if (other is! HourMinute) {
      return false;
    }
    return _calculateDifference(other) <= 0;
  }

  bool operator >(dynamic other) {
    if (other is! HourMinute) {
      return false;
    }
    return _calculateDifference(other) > 0;
  }

  bool operator >=(dynamic other) {
    if (other is! HourMinute) {
      return false;
    }
    return _calculateDifference(other) >= 0;
  }

  DateTime atDate(DateTime date) => date.yearMonthDay.add(asDuration);

  Duration get asDuration => Duration(hours: hour, minutes: minute);

  @override
  int get hashCode => hour.hashCode + minute.hashCode;

  int _calculateDifference(HourMinute other) =>
      (hour * 60 - other.hour * 60) + (minute - other.minute);
}
