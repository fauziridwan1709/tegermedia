part of "_jadwal.dart";

class HoursColumn extends StatelessWidget {
  final HourMinute minimumTime;

  final HourMinute maximumTime;

  final TopOffsetCalculator topOffsetCalculator;

  final HoursColumnStyle style;

  final HoursColumnTapCallback onHoursColumnTappedDown;

  final List<HourMinute> _sideTimes;

  HoursColumn({
    this.minimumTime = HourMinute.MIN,
    this.maximumTime = HourMinute.MAX,
    TopOffsetCalculator topOffsetCalculator,
    HoursColumnStyle style,
    this.onHoursColumnTappedDown,
  })  : assert(minimumTime != null),
        assert(maximumTime != null),
        assert(minimumTime < maximumTime),
        topOffsetCalculator =
            topOffsetCalculator ?? DefaultBuilders.defaultTopOffsetCalculator,
        style = style ?? const HoursColumnStyle(),
        _sideTimes = getSideTimes(minimumTime, maximumTime, style.interval);

  HoursColumn.fromHeadersWidgetState({
    @required ZoomableHeadersWidgetState parent,
  }) : this(
          minimumTime: parent.widget.minimumTime,
          maximumTime: parent.widget.maximumTime,
          topOffsetCalculator: parent.calculateTopOffset,
          style: parent.widget.hoursColumnStyle,
          onHoursColumnTappedDown: parent.widget.onHoursColumnTappedDown,
        );

  @override
  Widget build(BuildContext context) {
    Widget child = Container(
      height: topOffsetCalculator(maximumTime),
      width: style.width,
      color: style.decoration == null ? style.color : null,
      decoration: style.decoration,
      child: Stack(
        children: _sideTimes
            .map(
              (time) => Positioned(
                top: topOffsetCalculator(time) -
                    ((style.textStyle?.fontSize ?? 14) / 2),
                left: 0,
                right: 0,
                child: Align(
                  alignment: style.textAlignment,
                  child: Text(
                    style.timeFormatter(time),
                    style: style.textStyle,
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );

    if (onHoursColumnTappedDown == null) {
      return child;
    }

    return GestureDetector(
      onTapDown: (details) {
        var hourRowHeight =
            topOffsetCalculator(minimumTime.add(const HourMinute(hour: 1)));
        double hourMinutesInHour = details.localPosition.dy / hourRowHeight;

        int hour = hourMinutesInHour.floor();
        int minute = ((hourMinutesInHour - hour) * 60).round();
        onHoursColumnTappedDown(
            minimumTime.add(HourMinute(hour: hour, minute: minute)));
      },
      child: child,
    );
  }

  static List<HourMinute> getSideTimes(
      HourMinute minimumTime, HourMinute maximumTime, Duration interval) {
    List<HourMinute> sideTimes = [];
    HourMinute currentHour = HourMinute(hour: minimumTime.hour + 1);
    while (currentHour < maximumTime) {
      sideTimes.add(currentHour);
      currentHour =
          currentHour.add(HourMinute.fromDuration(duration: interval));
    }
    return sideTimes;
  }
}
