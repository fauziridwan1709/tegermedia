part of '_calendar.dart';

class Timeline extends StatefulWidget {
  const Timeline({
    @required this.children,
    this.cController,
    this.key,
    this.startDate,
    this.endDate,
    this.date,
    this.indicators,
    this.isLeftAligned = true,
    this.itemGap = 12.0,
    this.gutterSpacing = 4.0,
    this.padding = const EdgeInsets.all(8),
    this.controller,
    this.lineColor = Colors.grey,
    this.physics,
    this.shrinkWrap = true,
    this.primary = false,
    this.reverse = false,
    this.indicatorSize = 30.0,
    this.lineGap = 0.0,
    this.indicatorColor = Colors.blue,
    this.indicatorStyle = PaintingStyle.fill,
    this.strokeCap = StrokeCap.butt,
    this.strokeWidth = 2.0,
    this.style = PaintingStyle.stroke,
  })  : assert(itemGap >= 0),
        assert(lineGap >= 0),
        assert(indicators == null || children.length == indicators.length);

  @override
  final Key key;
  final CalendarController cController;
  final DateTime startDate;
  final DateTime endDate;
  final Map<DateTime, List<ScheduleItem>> date;
  final List<Widget> children;
  final double itemGap;
  final double gutterSpacing;
  final List<Widget> indicators;
  final bool isLeftAligned;
  final EdgeInsets padding;
  final ScrollController controller;
  final ScrollPhysics physics;
  final bool shrinkWrap;
  final bool primary;
  final bool reverse;
  final Color lineColor;
  final double lineGap;
  final double indicatorSize;
  final Color indicatorColor;
  final PaintingStyle indicatorStyle;
  final StrokeCap strokeCap;
  final double strokeWidth;
  final PaintingStyle style;

  @override
  _TimelineState createState() => _TimelineState();
}

class _TimelineState extends State<Timeline> {
  final authState = Injector.getAsReactive<AuthState>();
  final scheduleState = Injector.getAsReactive<ScheduleState>();
  ScrollController sController = ScrollController();
  double ak = 0;

  List offs = <dynamic>[];

  @override
  void initState() {
    super.initState();
  }

  // void getPositions(GlobalKey keyr, int e) async {
  //   var jadwal = context.read<JadwalProvider>();
  //   final RenderBox renderBoxRed = keyr.currentContext.findRenderObject();
  //   final positionRed = renderBoxRed.localToGlobal(Offset.fromDirection(2200));
  //   print('POSITION of $e: ${positionRed.dy} ');
  //   await jadwal.addData(positionRed.dy);
  // }

  @override
  Widget build(BuildContext context) {
    var total = widget.endDate.difference(widget.startDate).inDays + 1;
    // var jadwal = Provider.of<JadwalProvider>(context);
    // var auth = Provider.of<UserProvider>(context);
    // scheduleState.setState((s) => s.reset());
    // print('offset $offs');
    var nl = <int>[];
    for (var i = 0; i < total; i++) {
      nl.add(i);
    }
    var events = scheduleState.state;
    // print(total);
    return Padding(
      padding: EdgeInsets.only(bottom: 20.0),
      child: Column(
        children: nl.map((e) {
          var count = widget
                  .date[DateTime.parse(
                          widget.startDate.toString().substring(0, 10))
                      .add(Duration(days: e))]
                  ?.length ??
              0;
          if (nl.indexOf(e) > 0 &&
              widget.startDate.add(Duration(days: nl.indexOf(e))).month !=
                  widget.startDate
                      .add(Duration(days: nl.indexOf(e) - 1))
                      .month) {
            var keyr = GlobalKey();
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
              print(timeStamp);
              // if (timeStamp.inSeconds > Duration(seconds: 12).inSeconds) {
              //   print('bisa');
              //   await getPositions(keyr, e);
              // }
            });
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (nl.indexOf(e) > 0 &&
                    widget.startDate.add(Duration(days: nl.indexOf(e))).month !=
                        widget.startDate
                            .add(Duration(days: nl.indexOf(e) - 1))
                            .month)
                  Container(
                      key: keyr,
                      height: 60,
                      padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                      width: S.w,
                      color: SiswamediaTheme.green,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          '${QuizData.bulan[widget.startDate.add(Duration(days: nl.indexOf(e))).month - 1]} ${widget.startDate.add(Duration(days: nl.indexOf(e))).year}',
                          textAlign: TextAlign.left,
                          style: semiWhite.copyWith(
                              color: Colors.white, fontSize: S.w / 22),
                        ),
                      )),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text(
                    '${QuizData.hari[widget.startDate.add(Duration(days: nl.indexOf(e))).weekday - 1 % 7]}, ${widget.startDate.add(Duration(days: nl.indexOf(e))).day < 10 ? '0' : ''}${widget.startDate.add(Duration(days: nl.indexOf(e))).day} ${QuizData.bulan[widget.startDate.add(Duration(days: nl.indexOf(e))).month - 1]} ${widget.startDate.add(Duration(days: nl.indexOf(e))).year}',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: S.w / 26),
                  ),
                ),
                ListView.separated(
                  padding: widget.padding,
                  separatorBuilder: (_, __) => SizedBox(height: widget.itemGap),
                  physics: widget.physics,
                  shrinkWrap: widget.shrinkWrap,
                  itemCount: count,
                  controller: widget.controller,
                  reverse: widget.reverse,
                  primary: widget.primary,
                  itemBuilder: (context, index) {
                    Widget indicator;
                    if (widget.indicators != null) {
                      indicator = widget.indicators[index];
                    }
                    var c = DateTime.parse(
                            widget.startDate.toString().substring(0, 10))
                        .add(Duration(days: nl.indexOf(e)));
                    ScheduleItem time;
                    ScheduleItem before;
                    if (widget.date[c] != null) {
                      widget.date[c].sort((a, b) => DateTime.parse(a.startDate)
                          .compareTo(DateTime.parse(b.startDate)));
                      time = widget.date[c][index];
                      before = null;
                      if (index != 0) {
                        before = widget.date[c][index - 1];
                      }
                    }

                    final isFirst = index == 0;
                    final isLast = index == count - 1;

                    var timeStart = DateTime.parse(time.startDate).toLocal();
                    var timeEnd = DateTime.parse(time.endDate).toLocal();
                    //DateTime beforeStart = DateTime.parse(before.startDate);
                    //DateTime beforeEnd = DateTime.parse(before.endDate);

                    var timeSplit = time.description.split('~');

                    final timelineTile = time != null
                        ? <Widget>[
                            (before != null)
                                ? timeStart
                                            .add(Duration(seconds: 1))
                                            .difference(DateTime.parse(
                                                before.startDate)) >
                                        Duration(seconds: 0)
                                    ? Container(
                                        width: 80,
                                        child: Text(
                                          '${timeStart.hour < 10 ? '0' : ''}${timeStart.hour}.${timeStart.minute < 10 ? '0' : ''}${timeStart.minute}-${timeEnd.hour < 10 ? '0' : ''}${timeEnd.hour}.${timeEnd.minute < 10 ? '0' : ''}${timeEnd.minute}',
                                          style: descBlack.copyWith(
                                              color: Color(0xffC4C4C4)),
                                        ),
                                      )
                                    : Container(
                                        width: 80,
                                      )
                                : Container(
                                    width: 80,
                                    child: Text(
                                      '${timeStart.hour < 10 ? '0' : ''}${timeStart.hour}.${timeStart.minute < 10 ? '0' : ''}${timeStart.minute}-${timeEnd.hour < 10 ? '0' : ''}${timeEnd.hour}.${timeEnd.minute < 10 ? '0' : ''}${timeEnd.minute}',
                                      style: descBlack.copyWith(
                                          color: Color(0xffC4C4C4)),
                                    ),
                                  ),
                            CustomPaint(
                              foregroundPainter: _TimelinePainter(
                                hideDefaultIndicator: indicator != null,
                                lineColor: widget.lineColor,
                                indicatorColor: widget.indicatorColor,
                                indicatorSize: widget.indicatorSize,
                                indicatorStyle: widget.indicatorStyle,
                                time: time,
                                before: before,
                                isFirst: isFirst,
                                isLast: isLast,
                                lineGap: widget.lineGap,
                                strokeCap: widget.strokeCap,
                                strokeWidth: widget.strokeWidth,
                                style: widget.style,
                                itemGap: widget.itemGap,
                              ),
                              child: SizedBox(
                                height: double.infinity,
                                width: widget.indicatorSize,
                                child: indicator,
                              ),
                            ),
                            SizedBox(width: widget.gutterSpacing),
                            Expanded(
                                child: Container(
                              margin: EdgeInsets.only(left: 10),
                              height: time.name != 'Istirahat' ? 80 : 40,
                              decoration:
                                  SiswamediaTheme.whiteRadiusShadow.copyWith(
                                      color: time.category == 0
                                          ? Colors.white
                                          : time.category == 1
                                              ? Color(0xffFCCF3E)
                                              : time.category == 2
                                                  ? Color(0xffE1DC4B)
                                                  : time.category == 3
                                                      ? Color(0xffE14B4B)
                                                      : SiswamediaTheme.green),
                              child: InkWell(
                                onTap: () {
                                  if (time.name != 'Istirahat') {
                                    events.setJadwalModel(time.name, timeStart,
                                        timeEnd, time.description);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (_) => JadwalInfo(
                                                  id: time.iD,
                                                  judul: time.name,
                                                  deskripsi: time.description,
                                                  kategori: time.category,
                                                  start: DateTime.parse(
                                                          time.startDate)
                                                      .toLocal(),
                                                  end: DateTime.parse(
                                                          time.endDate)
                                                      .toLocal(),
                                                )));
                                  }
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  child: time.name != 'Istirahat'
                                      ? Row(
                                          children: [
                                            Image.asset(
                                              'assets/icons/materi/${iconMateri[time.iconData]}.png',
                                              height: 40,
                                              width: 40,
                                            ),
                                            SizedBox(width: 10),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(time.name,
                                                    style: semiBlack.copyWith(
                                                        color: (time.category !=
                                                                    0 &&
                                                                time.category !=
                                                                    1)
                                                            ? Colors.white
                                                            : Colors.black,
                                                        fontSize: S.w / 28)),
                                                Container(
                                                  width: S.w * .4,
                                                  child: RichText(
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    text: TextSpan(
                                                        style: TextStyle(
                                                            color: (timeSplit[
                                                                            1] !=
                                                                        '0' &&
                                                                    timeSplit[
                                                                            1] !=
                                                                        '1')
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: S.w / 32),
                                                        text: time.description
                                                            .split('~')[2]),
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        )
                                      : Text(
                                          time.name,
                                          style: descWhite.copyWith(
                                              fontSize: S.w / 28),
                                        ),
                                ),
                              ),
                            )),
                          ]
                        : <Widget>[];

                    return Column(
                      children: [
                        IntrinsicHeight(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: widget.isLeftAligned
                                ? timelineTile
                                : timelineTile.reversed.toList(),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ],
            );
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Text(
                  '${QuizData.hari[widget.startDate.add(Duration(days: nl.indexOf(e))).weekday - 1 % 7]}, ${widget.startDate.add(Duration(days: nl.indexOf(e))).day < 10 ? '0' : ''}${widget.startDate.add(Duration(days: nl.indexOf(e))).day} ${QuizData.bulan[widget.startDate.add(Duration(days: nl.indexOf(e))).month - 1]} ${widget.startDate.add(Duration(days: nl.indexOf(e))).year}',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: S.w / 26),
                ),
              ),
              ListView.separated(
                padding: widget.padding,
                separatorBuilder: (_, __) => SizedBox(height: widget.itemGap),
                physics: widget.physics,
                shrinkWrap: widget.shrinkWrap,
                itemCount: count,
                controller: widget.controller,
                reverse: widget.reverse,
                primary: widget.primary,
                itemBuilder: (context, index) {
                  Widget indicator;
                  if (widget.indicators != null) {
                    indicator = widget.indicators[index];
                  }
                  var c = DateTime.parse(
                          widget.startDate.toString().substring(0, 10))
                      .add(Duration(days: nl.indexOf(e)));
                  ScheduleItem time;
                  ScheduleItem before;
                  if (widget.date[c] != null) {
                    widget.date[c].sort((a, b) => DateTime.parse(a.startDate)
                        .compareTo(DateTime.parse(b.startDate)));
                    time = widget.date[c][index];
                    before = null;
                    if (index != 0) {
                      before = widget.date[c][index - 1];
                    }
                  }

                  final isFirst = index == 0;
                  final isLast = index == count - 1;

                  var timeStart = DateTime.parse(time.startDate).toLocal();
                  var timeEnd = DateTime.parse(time.endDate).toLocal();
                  //DateTime beforeStart = DateTime.parse(before.startDate);
                  //DateTime beforeEnd = DateTime.parse(before.endDate);

                  var timeSplit = time.description.split('~');

                  final timelineTile = time != null
                      ? <Widget>[
                          (before != null)
                              ? timeStart.add(Duration(seconds: 1)).difference(
                                          DateTime.parse(before.startDate)) >
                                      Duration(seconds: 0)
                                  ? Container(
                                      width: 80,
                                      child: Text(
                                        '${timeStart.hour < 10 ? '0' : ''}${timeStart.hour}.${timeStart.minute < 10 ? '0' : ''}${timeStart.minute}-${timeEnd.hour < 10 ? '0' : ''}${timeEnd.hour}.${timeEnd.minute < 10 ? '0' : ''}${timeEnd.minute}',
                                        style: descBlack.copyWith(
                                            color: Color(0xffC4C4C4)),
                                      ),
                                    )
                                  : Container(
                                      width: 80,
                                    )
                              : Container(
                                  width: 80,
                                  child: Text(
                                    '${timeStart.hour < 10 ? '0' : ''}${timeStart.hour}.${timeStart.minute < 10 ? '0' : ''}${timeStart.minute}-${timeEnd.hour < 10 ? '0' : ''}${timeEnd.hour}.${timeEnd.minute < 10 ? '0' : ''}${timeEnd.minute}',
                                    style: descBlack.copyWith(
                                        color: Color(0xffC4C4C4)),
                                  ),
                                ),
                          CustomPaint(
                            foregroundPainter: _TimelinePainter(
                              hideDefaultIndicator: indicator != null,
                              lineColor: widget.lineColor,
                              indicatorColor: widget.indicatorColor,
                              indicatorSize: widget.indicatorSize,
                              indicatorStyle: widget.indicatorStyle,
                              time: time,
                              before: before,
                              isFirst: isFirst,
                              isLast: isLast,
                              lineGap: widget.lineGap,
                              strokeCap: widget.strokeCap,
                              strokeWidth: widget.strokeWidth,
                              style: widget.style,
                              itemGap: widget.itemGap,
                            ),
                            child: SizedBox(
                              height: double.infinity,
                              width: widget.indicatorSize,
                              child: indicator,
                            ),
                          ),
                          SizedBox(width: widget.gutterSpacing),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 10),
                              // height: time.name != 'Istirahat' ? 80 : 40,
                              decoration:
                                  SiswamediaTheme.whiteRadiusShadow.copyWith(
                                      color: time.category == 0
                                          ? Colors.white
                                          : time.category == 1
                                              ? Color(0xffFCCF3E)
                                              : time.category == 2
                                                  ? Color(0xffE1DC4B)
                                                  : time.category == 3
                                                      ? Color(0xffE14B4B)
                                                      : SiswamediaTheme.green),
                              child: InkWell(
                                onTap: () {
                                  if (time.name != 'Istirahat') {
                                    events.setJadwalModel(time.name, timeStart,
                                        timeEnd, time.description);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (_) => JadwalInfo(
                                                  id: time.iD,
                                                  judul: time.name,
                                                  deskripsi: time.description,
                                                  kategori: time.category,
                                                  start: DateTime.parse(
                                                          time.startDate)
                                                      .toLocal(),
                                                  end: DateTime.parse(
                                                          time.endDate)
                                                      .toLocal(),
                                                )));
                                  }
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  child: time.name != 'Istirahat'
                                      ? Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Image.asset(
                                              'assets/icons/materi/${iconMateri[time.iconData]}.png',
                                              height: 40,
                                              width: 40,
                                            ),
                                            SizedBox(width: 10),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(time.name,
                                                      style: semiBlack.copyWith(
                                                          color: (time.category !=
                                                                      0 &&
                                                                  time.category !=
                                                                      1)
                                                              ? Colors.white
                                                              : Colors.black,
                                                          fontSize: S.w / 28)),
                                                  Container(
                                                    width: S.w * .275,
                                                    child: RichText(
                                                      maxLines: 2,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      text: TextSpan(
                                                          style: descBlack.copyWith(
                                                              color: (time.category !=
                                                                          0 &&
                                                                      time.category !=
                                                                          1)
                                                                  ? Colors.white
                                                                  : Colors
                                                                      .black,
                                                              fontSize:
                                                                  S.w / 32),
                                                          text:
                                                              time.description),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            if (!authState.state.currentState
                                                    .classRole.isOrtu &&
                                                authState.state.currentState
                                                        .type !=
                                                    'PERSONAL' &&
                                                time.category != 3)
                                              ButtonAbsen(
                                                  time: time, role: time.role),
                                          ],
                                        )
                                      : Text(
                                          time.name,
                                          style: descWhite.copyWith(
                                              fontSize: S.w / 28),
                                        ),
                                ),
                              ),
                            ),
                          ),
                        ]
                      : <Widget>[];

                  return Column(
                    children: [
                      IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: widget.isLeftAligned
                              ? timelineTile
                              : timelineTile.reversed.toList(),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ],
          );
        }).toList(),
      ),
    );
  }
}

class _TimelinePainter extends CustomPainter {
  _TimelinePainter({
    @required this.hideDefaultIndicator,
    @required this.indicatorColor,
    @required this.indicatorStyle,
    @required this.indicatorSize,
    @required this.lineGap,
    @required this.strokeCap,
    @required this.strokeWidth,
    @required this.style,
    @required this.lineColor,
    @required this.isFirst,
    @required this.isLast,
    @required this.itemGap,
    @required this.time,
    @required this.before,
  })  : linePaint = Paint()
          ..color = lineColor
          ..strokeCap = strokeCap
          ..strokeWidth = strokeWidth
          ..style = style,
        circlePaint = Paint()
          ..color = indicatorColor
          ..style = indicatorStyle;

  final ScheduleItem time;
  final ScheduleItem before;
  final bool hideDefaultIndicator;
  final Color indicatorColor;
  final PaintingStyle indicatorStyle;
  final double indicatorSize;
  final double lineGap;
  final StrokeCap strokeCap;
  final double strokeWidth;
  final PaintingStyle style;
  final Color lineColor;
  final Paint linePaint;
  final Paint circlePaint;
  final bool isFirst;
  final bool isLast;
  final double itemGap;

  @override
  void paint(Canvas canvas, Size size) {
    final indicatorRadius = indicatorSize / 2;
    final halfItemGap = itemGap / 2;
    final indicatorMargin = 0.0;

    final top = size.topLeft(Offset(indicatorRadius, 0.0 - halfItemGap));
    final centerTop = size.centerLeft(
      Offset(indicatorRadius, -indicatorMargin),
    );

    final bottom = size.bottomLeft(Offset(indicatorRadius, 0.0 + halfItemGap));
    final centerBottom = size.centerLeft(
      Offset(indicatorRadius, indicatorMargin),
    );

    if (!isFirst) canvas.drawLine(top, centerTop, linePaint);
    if (!isLast) canvas.drawLine(centerBottom, bottom, linePaint);
    //print('aku');
    if (before != null) {
      if ((DateTime.parse(time.startDate)
              .add(Duration(seconds: 1))
              .difference(DateTime.parse(before.endDate)) >
          Duration(seconds: 0))) {
        final offsetCenter = size.centerLeft(Offset(indicatorRadius, 0));

        canvas.drawCircle(offsetCenter, 4, circlePaint);
      }
    } else {
      final offsetCenter = size.centerLeft(Offset(indicatorRadius, 0));
      canvas.drawCircle(offsetCenter, 4, circlePaint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

// class ScrollViewa extends ScrollView {
//   @override
//   List<Widget> buildSlivers(BuildContext context) {
//     // TODO: implement buildSlivers
//     return GestureDetector(a);
//   }
// }
