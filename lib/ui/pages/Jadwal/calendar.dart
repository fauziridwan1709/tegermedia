part of '_calendar.dart';

typedef void OnDaySelected(DateTime day, List<JadwalModel> events);

typedef void OnVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format);

typedef void OnCalendarCreated(DateTime first, DateTime last, CalendarFormat format);

typedef void HeaderGestureCallback(DateTime focusedDay);

typedef String TextBuilder(DateTime date, dynamic locale);

typedef bool EnabledDayPredicate(DateTime day);

enum CalendarFormat { month, week }

enum FormatAnimation { slide, scale }

enum StartingDayOfWeek { monday, tuesday, wednesday, thursday, friday, saturday, sunday }

int _getWeekdayNumber(StartingDayOfWeek weekday) {
  return StartingDayOfWeek.values.indexOf(weekday) + 1;
}

enum AvailableGestures { none, verticalSwipe, horizontalSwipe, all }

class TableCalendar extends StatefulWidget {
  final CalendarController calendarController;
  final dynamic locale;
  final Map<DateTime, List> events;
  final Map<DateTime, List> holidays;
  final OnDaySelected onDaySelected;
  final OnDaySelected onDayLongPressed;
  final VoidCallback onUnavailableDaySelected;
  final VoidCallback onUnavailableDayLongPressed;
  final HeaderGestureCallback onHeaderTapped;
  final HeaderGestureCallback onHeaderLongPressed;
  final OnVisibleDaysChanged onVisibleDaysChanged;
  final OnCalendarCreated onCalendarCreated;
  final DateTime initialSelectedDay;
  final DateTime startDay;
  final DateTime endDay;
  final List<int> weekendDays;
  final CalendarFormat initialCalendarFormat;
  final Map<CalendarFormat, String> availableCalendarFormats;
  final bool headerVisible;
  final EnabledDayPredicate enabledDayPredicate;
  final double rowHeight;
  final FormatAnimation formatAnimation;
  final StartingDayOfWeek startingDayOfWeek;
  final HitTestBehavior dayHitTestBehavior;
  final AvailableGestures availableGestures;
  final SimpleSwipeConfig simpleSwipeConfig;
  final CalendarStyle calendarStyle;
  final DaysOfWeekStyle daysOfWeekStyle;
  final HeaderStyle headerStyle;
  final CalendarBuilders builders;
  final AnimationController controller;
  final VoidCallback handleIconPressed;

  TableCalendar({
    Key key,
    @required this.calendarController,
    @required this.controller,
    this.handleIconPressed,
    this.locale,
    this.events = const {},
    this.holidays = const {},
    this.onDaySelected,
    this.onDayLongPressed,
    this.onUnavailableDaySelected,
    this.onUnavailableDayLongPressed,
    this.onHeaderTapped,
    this.onHeaderLongPressed,
    this.onVisibleDaysChanged,
    this.onCalendarCreated,
    this.initialSelectedDay,
    this.startDay,
    this.endDay,
    this.weekendDays = const [DateTime.saturday, DateTime.sunday],
    this.initialCalendarFormat = CalendarFormat.month,
    this.availableCalendarFormats = const {
      CalendarFormat.month: 'Month',
      CalendarFormat.week: 'Week',
    },
    this.headerVisible = true,
    this.enabledDayPredicate,
    this.rowHeight,
    this.formatAnimation = FormatAnimation.slide,
    this.startingDayOfWeek = StartingDayOfWeek.sunday,
    this.dayHitTestBehavior = HitTestBehavior.deferToChild,
    this.availableGestures = AvailableGestures.all,
    this.simpleSwipeConfig = const SimpleSwipeConfig(
      verticalThreshold: 25.0,
      swipeDetectionBehavior: SwipeDetectionBehavior.continuousDistinct,
    ),
    this.calendarStyle = const CalendarStyle(),
    this.daysOfWeekStyle = const DaysOfWeekStyle(),
    this.headerStyle = const HeaderStyle(),
    this.builders = const CalendarBuilders(),
  })  : assert(calendarController != null),
        assert(availableCalendarFormats.keys.contains(initialCalendarFormat)),
        assert(availableCalendarFormats.length <= CalendarFormat.values.length),
        assert(weekendDays != null),
        assert(weekendDays.isNotEmpty
            ? weekendDays.every((day) => day >= DateTime.monday && day <= DateTime.sunday)
            : true),
        assert(controller != null),
        super(key: key);

  @override
  _TableCalendarState createState() => _TableCalendarState();
}

class _TableCalendarState extends State<TableCalendar> with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();

    widget.calendarController._init(
      events: widget.events,
      holidays: widget.holidays,
      initialDay: widget.initialSelectedDay,
      initialFormat: widget.initialCalendarFormat,
      availableCalendarFormats: widget.availableCalendarFormats,
      useNextCalendarFormat: widget.headerStyle.formatButtonShowsNext,
      startingDayOfWeek: widget.startingDayOfWeek,
      selectedDayCallback: _selectedDayCallback,
      onVisibleDaysChanged: widget.onVisibleDaysChanged,
      onCalendarCreated: widget.onCalendarCreated,
      includeInvisibleDays: widget.calendarStyle.outsideDaysVisible,
    );
  }

  @override
  void didUpdateWidget(TableCalendar oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.events != widget.events) {
      widget.calendarController._events = widget.events;
    }

    if (oldWidget.holidays != widget.holidays) {
      widget.calendarController._holidays = widget.holidays;
    }
  }

  void _selectedDayCallback(DateTime day) {
    if (widget.onDaySelected != null) {
      widget.onDaySelected(day, widget.calendarController.visibleEvents[_getEventKey(day)] ?? []);
    }
  }

  void _selectPrevious() {
    setState(() {
      widget.calendarController._selectPrevious();
    });
  }

  void _selectNext() {
    setState(() {
      widget.calendarController._selectNext();
    });
  }

  void _selectDay(DateTime day) {
    setState(() {
      widget.calendarController.setSelectedDay(day, isProgrammatic: false);
      _selectedDayCallback(day);
    });
  }

  void _onDayLongPressed(DateTime day) {
    if (widget.onDayLongPressed != null) {
      widget.onDayLongPressed(
          day, widget.calendarController.visibleEvents[_getEventKey(day)] ?? []);
    }
  }

  void _toggleCalendarFormat() {
    setState(() {
      widget.calendarController.toggleCalendarFormat();
    });
  }

  void _onHorizontalSwipe(DismissDirection direction) {
    if (direction == DismissDirection.startToEnd) {
      // Swipe right
      _selectPrevious();
    } else {
      // Swipe left
      _selectNext();
    }
  }

  void _onUnavailableDaySelected() {
    if (widget.onUnavailableDaySelected != null) {
      widget.onUnavailableDaySelected();
    }
  }

  void _onUnavailableDayLongPressed() {
    if (widget.onUnavailableDayLongPressed != null) {
      widget.onUnavailableDayLongPressed();
    }
  }

  // void _onHeaderTapped() {
  //   if (widget.onHeaderTapped != null) {
  //     widget.onHeaderTapped(widget.calendarController.focusedDay);
  //   }
  // }

  // // void _onHeaderLongPressed() {
  //   if (widget.onHeaderLongPressed != null) {
  //     widget.onHeaderLongPressed(widget.calendarController.focusedDay);
  //   }
  // }

  bool _isDayUnavailable(DateTime day) {
    return (widget.startDay != null &&
            day.isBefore(widget.calendarController._normalizeDate(widget.startDay))) ||
        (widget.endDay != null &&
            day.isAfter(widget.calendarController._normalizeDate(widget.endDay))) ||
        (!_isDayEnabled(day));
  }

  bool _isDayEnabled(DateTime day) {
    return widget.enabledDayPredicate == null ? true : widget.enabledDayPredicate(day);
  }

  DateTime _getEventKey(DateTime day) {
    return widget.calendarController._getEventKey(day);
  }

  DateTime _getHolidayKey(DateTime day) {
    return widget.calendarController._getHolidayKey(day);
  }

  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: Container(
          color: SiswamediaTheme.materialGreen[100],
          child: Column(
            children: [
              InkWell(onTap: widget.handleIconPressed, child: _buildHeader()),
              CustomExpansionTile(
                isExpanded: true,
                controller: widget.controller,
                trailing: SizedBox(),
                children: [
                  SizedBox(height: 10),
                  _buildCalendarContent(),
                  Container(
                      height: S.w / 10,
                      color: SiswamediaTheme.materialGreen[100],
                      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                        Container(
                            height: 15,
                            width: 15,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(2),
                              border: Border.all(color: Colors.white),
                            )),
                        Text('hari ini',
                            style: TextStyle(
                                fontSize: S.w / 40,
                                color: Colors.white,
                                fontWeight: FontWeight.w600)),
                        Container(
                          height: 10,
                          width: 10,
                          decoration:
                              BoxDecoration(shape: BoxShape.circle, color: Color(0xffFCCF3E)),
                        ),
                        Text('Ujian',
                            style: TextStyle(
                                fontSize: S.w / 40,
                                color: Colors.white,
                                fontWeight: FontWeight.w600)),
                        Container(
                          height: 10,
                          width: 10,
                          decoration:
                              BoxDecoration(shape: BoxShape.circle, color: Color(0xffE1DC4B)),
                        ),
                        Text('Ekstrakurikuler',
                            style: TextStyle(
                                fontSize: S.w / 40,
                                color: Colors.white,
                                fontWeight: FontWeight.w600)),
                        Container(
                          height: 10,
                          width: 10,
                          decoration:
                              BoxDecoration(shape: BoxShape.circle, color: Color(0xffE14B4B)),
                        ),
                        Text('Acara',
                            style: TextStyle(
                                fontSize: S.w / 40,
                                color: Colors.white,
                                fontWeight: FontWeight.w600)),
                      ])),
                ],
              ),
            ],
          ) /*Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (widget.headerVisible) _buildHeader(),

          ],
        ),*/
          ),
    );
  }

  void handleOnPressed() {}

  Widget _buildHeader() {
    final children = [
      SizedBox(width: 8),
      Text(
        widget.headerStyle.titleTextBuilder != null
            ? widget.headerStyle
                .titleTextBuilder(widget.calendarController.focusedDay, widget.locale)
            : DateFormat.yMMMM(widget.locale).format(widget.calendarController.focusedDay),
        style: widget.headerStyle.titleTextStyle,
        textAlign: TextAlign.start,
      ),
      _CustomIconButton(
        onTap: widget.handleIconPressed,
        margin: EdgeInsets.symmetric(vertical: 15),
        controller: widget.controller,
        animation: Tween<double>(begin: 0.0, end: 22 / 7)
            .animate(CurvedAnimation(parent: widget.controller, curve: Curves.easeInOut)),
      ),
    ];

    if (widget.headerStyle.formatButtonVisible && widget.availableCalendarFormats.length > 1) {
      children.insert(3, _buildFormatButton());
    }

    return Container(
      decoration: widget.headerStyle.decoration,
      margin: EdgeInsets.only(bottom: 0),
      //padding: widget.headerStyle.headerPadding,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: children,
      ),
    );
  }

  Widget _buildFormatButton() {
    return GestureDetector(
      onTap: _toggleCalendarFormat,
      child: Container(
        decoration: widget.headerStyle.formatButtonDecoration,
        padding: widget.headerStyle.formatButtonPadding,
        child: Text(
          widget.calendarController._getFormatButtonText(),
          style: widget.headerStyle.formatButtonTextStyle,
        ),
      ),
    );
  }

  Widget _buildCalendarContent() {
    if (widget.formatAnimation == FormatAnimation.slide) {
      return AnimatedSize(
        duration: Duration(
            milliseconds:
                widget.calendarController.calendarFormat == CalendarFormat.month ? 330 : 220),
        curve: Curves.fastOutSlowIn,
        alignment: Alignment(0, -1),
        vsync: this,
        child: _buildWrapper(),
      );
    } else {
      return AnimatedSwitcher(
        duration: const Duration(milliseconds: 350),
        transitionBuilder: (child, animation) {
          return SizeTransition(
            sizeFactor: animation,
            child: ScaleTransition(
              scale: animation,
              child: child,
            ),
          );
        },
        child: _buildWrapper(
          key: ValueKey(widget.calendarController.calendarFormat),
        ),
      );
    }
  }

  Widget _buildWrapper({Key key}) {
    var wrappedChild = _buildTable();

    switch (widget.availableGestures) {
      case AvailableGestures.all:
        wrappedChild = _buildVerticalSwipeWrapper(
          child: _buildHorizontalSwipeWrapper(
            child: wrappedChild,
          ),
        );
        break;
      case AvailableGestures.verticalSwipe:
        wrappedChild = _buildVerticalSwipeWrapper(
          child: wrappedChild,
        );
        break;
      case AvailableGestures.horizontalSwipe:
        wrappedChild = _buildHorizontalSwipeWrapper(
          child: wrappedChild,
        );
        break;
      case AvailableGestures.none:
        break;
    }

    return Container(
      key: key,
      child: wrappedChild,
    );
  }

  Widget _buildVerticalSwipeWrapper({Widget child}) {
    return SimpleGestureDetector(
      child: child,
      onVerticalSwipe: (direction) {
        setState(() {
          widget.calendarController.swipeCalendarFormat(isSwipeUp: direction == SwipeDirection.up);
        });
      },
      swipeConfig: widget.simpleSwipeConfig,
    );
  }

  Widget _buildHorizontalSwipeWrapper({Widget child}) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 350),
      switchInCurve: Curves.decelerate,
      transitionBuilder: (child, animation) {
        return SlideTransition(
          position:
              Tween<Offset>(begin: Offset(widget.calendarController._dx, 0), end: Offset(0, 0))
                  .animate(animation),
          child: child,
        );
      },
      layoutBuilder: (currentChild, _) => currentChild,
      child: Dismissible(
        key: ValueKey(widget.calendarController._pageId),
        resizeDuration: null,
        onDismissed: _onHorizontalSwipe,
        direction: DismissDirection.horizontal,
        child: child,
      ),
    );
  }

  Widget _buildTable() {
    final daysInWeek = 7;
    final children = <TableRow>[
      if (widget.calendarStyle.renderDaysOfWeek) _buildDaysOfWeek(),
    ];

    int x = 0;
    while (x < widget.calendarController._visibleDays.value.length) {
      children.add(_buildTableRow(
          widget.calendarController._visibleDays.value.skip(x).take(daysInWeek).toList()));
      x += daysInWeek;
    }

    return Table(
      // Makes this Table fill its parent horizontally
      defaultColumnWidth: FractionColumnWidth(1.0 / daysInWeek),
      children: children,
    );
  }

  TableRow _buildDaysOfWeek() {
    return TableRow(
      children: widget.calendarController._visibleDays.value.take(7).map((date) {
        final weekdayString = widget.daysOfWeekStyle.dowTextBuilder != null
            ? widget.daysOfWeekStyle.dowTextBuilder(date, widget.locale)
            : DateFormat.E(widget.locale).format(date);
        final isWeekend = widget.calendarController._isWeekend(date, widget.weekendDays);

        if (isWeekend && widget.builders.dowWeekendBuilder != null) {
          return widget.builders.dowWeekendBuilder(context, weekdayString);
        }
        if (widget.builders.dowWeekdayBuilder != null) {
          return widget.builders.dowWeekdayBuilder(context, weekdayString);
        }
        return Center(
          child: Text(
            weekdayString,
            style: isWeekend
                ? widget.daysOfWeekStyle.weekendStyle
                : widget.daysOfWeekStyle.weekdayStyle,
          ),
        );
      }).toList(),
    );
  }

  TableRow _buildTableRow(List<DateTime> days) {
    return TableRow(children: days.map((date) => _buildTableCell(date)).toList());
  }

  // TableCell will have equal width and height
  Widget _buildTableCell(DateTime date) {
    return LayoutBuilder(
      builder: (context, constraints) => ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: widget.rowHeight ?? constraints.maxWidth,
          minHeight: widget.rowHeight ?? constraints.maxWidth,
        ),
        child: _buildCell(date),
      ),
    );
  }

  Widget _buildCell(DateTime date) {
    if (!widget.calendarStyle.outsideDaysVisible &&
        widget.calendarController._isExtraDay(date) &&
        widget.calendarController.calendarFormat == CalendarFormat.month) {
      return Container();
    }

    Widget content = _buildCellContent(date);

    final eventKey = _getEventKey(date);
    final holidayKey = _getHolidayKey(date);
    final key = eventKey ?? holidayKey;

    if (key != null) {
      final children = <Widget>[content];

      final events =
          eventKey != null ? widget.calendarController.visibleEvents[eventKey] : <dynamic>[];
      final holidays =
          holidayKey != null ? widget.calendarController.visibleHolidays[holidayKey] : <dynamic>[];

      if (!_isDayUnavailable(date)) {
        if (widget.builders.markersBuilder != null) {
          children.addAll(
            widget.builders.markersBuilder(
              context,
              key,
              events,
              holidays,
            ),
          );
        } else {
          children.add(
            Positioned(
              top: widget.calendarStyle.markersPositionTop,
              bottom: widget.calendarStyle.markersPositionBottom,
              left: widget.calendarStyle.markersPositionLeft,
              right: widget.calendarStyle.markersPositionRight,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: events
                    .take(widget.calendarStyle.markersMaxAmount)
                    .map((dynamic event) => _buildMarker(eventKey, event))
                    .toList(),
              ),
            ),
          );
        }
      }

      if (children.length > 1) {
        content = Stack(
          alignment: widget.calendarStyle.markersAlignment,
          children: children,
          overflow: widget.calendarStyle.canEventMarkersOverflow ? Overflow.visible : Overflow.clip,
        );
      }
    }

    return GestureDetector(
      behavior: widget.dayHitTestBehavior,
      onTap: () => _isDayUnavailable(date) ? _onUnavailableDaySelected() : _selectDay(date),
      onLongPress: () =>
          _isDayUnavailable(date) ? _onUnavailableDayLongPressed() : _onDayLongPressed(date),
      child: content,
    );
  }

  Widget _buildCellContent(DateTime date) {
    // final eventKey = _getEventKey(date);

    final tIsUnavailable = _isDayUnavailable(date);
    final tIsSelected = widget.calendarController.isSelected(date);
    final tIsToday = widget.calendarController.isToday(date);
    final tIsOutside = widget.calendarController._isExtraDay(date);
    final tIsHoliday = widget.calendarController.visibleHolidays.containsKey(_getHolidayKey(date));
    final tIsWeekend = widget.calendarController._isWeekend(date, widget.weekendDays);

    // if (isUnavailable) {
    //   return widget.builders.unavailableDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isSelected && widget.calendarStyle.renderSelectedFirst) {
    //   return widget.builders.selectedDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isToday) {
    //   return widget.builders.todayDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isSelected) {
    //   return widget.builders.selectedDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isOutsideHoliday) {
    //   return widget.builders.outsideHolidayDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isHoliday) {
    //   return widget.builders.holidayDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isOutsideWeekend) {
    //   return widget.builders.outsideWeekendDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isOutside) {
    //   return widget.builders.outsideDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (isWeekend) {
    //   return widget.builders.weekendDayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else if (widget.builders.dayBuilder != null) {
    //   return widget.builders.dayBuilder(
    //       context, date, widget.calendarController.visibleEvents[eventKey]);
    // } else {
    return _CellWidget(
      text: '${date.day}',
      isUnavailable: tIsUnavailable,
      isSelected: tIsSelected,
      isToday: tIsToday,
      isWeekend: tIsWeekend,
      isOutsideMonth: tIsOutside,
      isHoliday: tIsHoliday,
      calendarStyle: widget.calendarStyle,
    );
    //}
  }

  Widget _buildMarker(DateTime date, dynamic event) {
    if (widget.builders.singleMarkerBuilder != null) {
      return widget.builders.singleMarkerBuilder(context, date, event);
    } else {
      return Container(
        width: 8.0,
        height: 8.0,
        margin: const EdgeInsets.symmetric(horizontal: 0.3),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: event.category == 0
              ? Color(0xff7ba939)
              : event.category == 1
                  ? Color(0xfffccf3e)
                  : event.category == 2
                      ? Colors.white
                      : widget.calendarStyle.markersColor,
        ),
      );
    }
  }
}
