part of '_jadwal.dart';

class DayBar extends StatelessWidget {
  final DateTime date;

  final DayBarStyle style;

  final double height;

  final double width;

  final DayBarTapCallback onDayBarTappedDown;

  DayBar({
    @required DateTime date,
    @required this.style,
    this.height,
    this.width,
    this.onDayBarTappedDown,
  })  : assert(date != null),
        assert(style != null),
        date = date.yearMonthDay;

  DayBar.fromHeadersWidgetState({
    @required ZoomableHeadersWidget parent,
    @required DateTime date,
    @required DayBarStyle style,
    double width,
  }) : this(
          date: date,
          style: style,
          height: parent.style.headerSize,
          width: width,
          onDayBarTappedDown: parent.onDayBarTappedDown,
        );

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTapDown: (details) => (onDayBarTappedDown ?? (date) {})(date),
        child: Container(
          height: height,
          width: width,
          color: style.decoration == null ? style.color : null,
          decoration: style.decoration,
          alignment: style.textAlignment,
          child: Text(
            style.dateFormatter(date.year, date.month, date.day),
            style: style.textStyle,
          ),
        ),
      );
}
