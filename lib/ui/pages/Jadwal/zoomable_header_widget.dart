part of '_jadwal.dart';

typedef TopOffsetCalculator = double Function(HourMinute time);

typedef HoursColumnTapCallback = Function(HourMinute time);

typedef DayBarTapCallback = Function(DateTime date);

typedef CurrentTimeIndicatorBuilder = Widget Function(DayViewStyle dayViewStyle,
    TopOffsetCalculator topOffsetCalculator, double hoursColumnWidth);

abstract class ZoomableHeadersWidget<S extends ZoomableHeaderWidgetStyle,
    C extends ZoomController> extends StatefulWidget {
  final S style;

  final HoursColumnStyle hoursColumnStyle;

  final bool inScrollableWidget;

  final HourMinute minimumTime;

  final HourMinute maximumTime;

  final DateTime initialTime;

  final bool userZoomable;

  final CurrentTimeIndicatorBuilder currentTimeIndicatorBuilder;

  final HoursColumnTapCallback onHoursColumnTappedDown;

  final DayBarTapCallback onDayBarTappedDown;

  final C controller;

  const ZoomableHeadersWidget({
    @required this.style,
    @required this.hoursColumnStyle,
    @required this.inScrollableWidget,
    @required this.minimumTime,
    @required this.maximumTime,
    @required this.initialTime,
    @required this.userZoomable,
    this.currentTimeIndicatorBuilder,
    this.onHoursColumnTappedDown,
    this.onDayBarTappedDown,
    @required this.controller,
  })  : assert(style != null),
        assert(minimumTime != null),
        assert(maximumTime != null),
        assert(minimumTime < maximumTime),
        assert(initialTime != null),
        assert(inScrollableWidget != null),
        assert(userZoomable != null);
}

abstract class ZoomableHeadersWidgetState<W extends ZoomableHeadersWidget>
    extends State<W> with ZoomControllerListener {
  double hourRowHeight;

  ScrollController verticalScrollController;

  @override
  void initState() {
    super.initState();
    hourRowHeight = _calculateHourRowHeight();
    widget.controller.addListener(this);

    if (widget.inScrollableWidget) {
      verticalScrollController = ScrollController();
    }
  }

  @override
  void didUpdateWidget(W oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.controller.zoomFactor != oldWidget.controller.zoomFactor) {
      widget.controller
          .changeZoomFactor(oldWidget.controller.zoomFactor, notify: false);
    }

    hourRowHeight = _calculateHourRowHeight();
    oldWidget.controller.removeListener(this);
    widget.controller.addListener(this);
  }

  @override
  void onZoomFactorChanged(
      ZoomController controller, ScaleUpdateDetails details) {
    if (!mounted) {
      return;
    }

    double hourRowHeight = _calculateHourRowHeight(controller);
    if (verticalScrollController != null) {
      double widgetHeight =
          (context.findRenderObject() as RenderBox).size.height;
      double maxPixels = calculateHeight(hourRowHeight) -
          widgetHeight +
          widget.style.headerSize;

      if (hourRowHeight < this.hourRowHeight &&
          verticalScrollController.position.pixels > maxPixels) {
        verticalScrollController.jumpTo(maxPixels);
      } else {
        verticalScrollController
            .jumpTo(math.min(maxPixels, details.localFocalPoint.dy));
      }
    }

    setState(() {
      this.hourRowHeight = hourRowHeight;
    });
  }

  @override
  void dispose() {
    widget.controller.dispose();
    verticalScrollController?.dispose();
    super.dispose();
  }

  DayViewStyle get currentDayViewStyle;

  void scheduleScrollToInitialTime() {
    if (shouldScrollToInitialTime) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => scrollToInitialTime());
    }
  }

  bool get shouldScrollToInitialTime =>
      widget.minimumTime
          .atDate(widget.initialTime)
          .isBefore(widget.initialTime) &&
      widget.maximumTime.atDate(widget.initialTime).isAfter(widget.initialTime);

  void scrollToInitialTime() {
    if (mounted && verticalScrollController != null) {
      double topOffset = calculateTopOffset(
          HourMinute.fromDateTime(dateTime: widget.initialTime));
      verticalScrollController.jumpTo(math.min(
          topOffset, verticalScrollController.position.maxScrollExtent));
    }
  }

  bool get isZoomable =>
      widget.userZoomable && widget.controller.zoomCoefficient > 0;

  double calculateTopOffset(HourMinute time,
          {HourMinute minimumTime, double hourRowHeight}) =>
      DefaultBuilders.defaultTopOffsetCalculator(time,
          minimumTime: minimumTime ?? widget.minimumTime,
          hourRowHeight: hourRowHeight ?? this.hourRowHeight);

  double calculateHeight([double hourRowHeight]) =>
      calculateTopOffset(widget.maximumTime, hourRowHeight: hourRowHeight);

  double _calculateHourRowHeight([ZoomController controller]) =>
      currentDayViewStyle.hourRowHeight *
      (controller ?? widget.controller).zoomFactor;
}
