part of "_jadwal.dart";

abstract class ZoomController {
  final double zoomCoefficient;

  final double minZoom;

  final double maxZoom;

  final Set<ZoomControllerListener> _listeners = HashSet();

  @protected
  double previousZoomFactor = 1;

  double _zoomFactor = 1;

  ZoomController({
    double zoomCoefficient = 0.8,
    double minZoom,
    double maxZoom,
  })  : zoomCoefficient = math.max(0, zoomCoefficient ?? 0),
        minZoom = math.max(0, minZoom ?? 0.4),
        maxZoom = math.max(0, minZoom ?? 1.6) {
    assert(this.minZoom <= this.maxZoom);
  }

  void addListener(ZoomControllerListener listener) => _listeners.add(listener);

  void removeListener(ZoomControllerListener listener) =>
      _listeners.remove(listener);

  double calculateZoomFactor(double scale) {
    double zoomFactor = previousZoomFactor * scale * zoomCoefficient;
    if (zoomFactor < minZoom) {
      zoomFactor = minZoom;
    }

    if (zoomFactor > maxZoom) {
      zoomFactor = maxZoom;
    }

    return zoomFactor;
  }

  void scaleStart() => previousZoomFactor = zoomFactor;

  void scaleUpdate(ScaleUpdateDetails details) =>
      changeZoomFactor(calculateZoomFactor(details.scale), details: details);

  double get scale => zoomFactor / (previousZoomFactor * zoomCoefficient);

  double get zoomFactor => _zoomFactor;

  @protected
  set zoomFactor(double zoomFactor) => _zoomFactor = zoomFactor;

  void changeZoomFactor(double zoomFactor,
      {bool notify = true, ScaleUpdateDetails details}) {
    bool hasChanged = this.zoomFactor != zoomFactor;
    if (hasChanged) {
      _zoomFactor = zoomFactor;
      if (notify) {
        details ??= ScaleUpdateDetails(scale: scale);
        _listeners
            .forEach((listener) => listener.onZoomFactorChanged(this, details));
      }
    }
  }

  void dispose() {
    _listeners.clear();
  }
}

mixin ZoomControllerListener {
  void onZoomFactorChanged(
      covariant ZoomController controller, ScaleUpdateDetails details);
}
