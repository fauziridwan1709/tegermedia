part of "_jadwal.dart";

class HoursColumnStyle {
  final TimeFormatter timeFormatter;

  final TextStyle textStyle;

  final double width;

  final Color color;

  final Decoration decoration;

  final Alignment textAlignment;

  final Duration interval;

  const HoursColumnStyle({
    TimeFormatter timeFormatter,
    TextStyle textStyle,
    double width,
    Color color,
    this.decoration,
    Alignment textAlignment,
    Duration interval,
  })  : timeFormatter = timeFormatter ?? DefaultBuilders.defaultTimeFormatter,
        textStyle = textStyle ?? const TextStyle(color: Colors.black54),
        width = (width ?? 60) < 0 ? 0 : (width ?? 60),
        color = color ?? Colors.white,
        textAlignment = textAlignment ?? Alignment.center,
        interval = interval ?? const Duration(hours: 1);

  HoursColumnStyle copyWith({
    TimeFormatter timeFormatter,
    TextStyle textStyle,
    double width,
    Color color,
    Decoration decoration,
    Alignment textAlignment,
    Duration interval,
  }) =>
      HoursColumnStyle(
        timeFormatter: timeFormatter ?? this.timeFormatter,
        textStyle: textStyle ?? this.textStyle,
        width: width ?? this.width,
        color: color ?? this.color,
        decoration: decoration ?? this.decoration,
        textAlignment: textAlignment ?? this.textAlignment,
        interval: interval ?? this.interval,
      );
}
