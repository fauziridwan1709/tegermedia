part of '_jadwal.dart';

class Utils {
  static String addLeadingZero(int number) =>
      (number < 10 ? '0' : '') + number.toString();

  static bool sameDay(DateTime date, [DateTime target]) {
    target = target ?? dateTimeGetter.now();
    return target.year == date.year &&
        target.month == date.month &&
        target.day == date.day;
  }

  static String removeLastWord(String string) {
    var words = string.split(' ');
    if (words.isEmpty) {
      return '';
    }

    return words.getRange(0, words.length - 1).join(' ');
  }
}

class NowDateTimeGetter {
  DateTime now() => DateTime.now();
}

NowDateTimeGetter dateTimeGetter = NowDateTimeGetter();

void injectDateTimeGetterForTest(NowDateTimeGetter testDateTimeGetter) {
  dateTimeGetter = testDateTimeGetter;
}
