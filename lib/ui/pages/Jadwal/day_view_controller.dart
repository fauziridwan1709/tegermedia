part of "_jadwal.dart";

class DayViewController extends ZoomController {
  final Function(DayViewController controller) onDisposed;

  DayViewController({
    double zoomCoefficient = 0.8,
    double minZoom,
    double maxZoom,
    this.onDisposed,
  }) : super(
          zoomCoefficient: zoomCoefficient,
          minZoom: minZoom,
          maxZoom: maxZoom,
        );

  @override
  void dispose() {
    super.dispose();
    if (onDisposed != null) {
      onDisposed(this);
    }
  }
}
