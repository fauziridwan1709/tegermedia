part of '_absensi.dart';

class AbsensiSiswa extends StatefulWidget {
  @override
  _AbsensiSiswaState createState() => _AbsensiSiswaState();
}

class _AbsensiSiswaState extends State<AbsensiSiswa>
    with TickerProviderStateMixin {
  final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final refreshIndicatorKey2 = GlobalKey<RefreshIndicatorState>();
  var auth = Injector.getAsReactive<AuthState>().state;
  var absensiState = Injector.getAsReactive<AbsensiState>();
  var scheduleState = Injector.getAsReactive<ScheduleState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  Animation<double> animation;
  AnimationController controller;
  TabController tabController;

  bool isOrtu;
  @override
  void initState() {
    super.initState();
    tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    controller =
        AnimationController(duration: Duration(milliseconds: 700), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeInOut));
    controller.forward();

    Initializer(
      state: scheduleState.state.items != null,
      cacheKey: TIME_ABSENSI_SCHEDULE_SISWA,
      reactiveModel: scheduleState,
      rIndicator: refreshIndicatorKey,
    ).initialize();

    Initializer(
      state: absensiState.state.model != null,
      cacheKey: TIME_ABSENSI_SISWA,
      reactiveModel: absensiState,
      rIndicator: refreshIndicatorKey2,
    ).initialize();
  }

  Future<void> retrieveData() async {
    await absensiState.setState((s) => s.getSummary(auth.currentState.classId));
    await scheduleState.setState((s) => s.fetchItems(
        auth.currentState.classId, auth.currentState.classRole.isOrtu));
  }

  @override
  Widget build(BuildContext context) {
    //godo
    // var data = absensiState.state.model.data.coursesAbsenSummary;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: SiswamediaTheme.background,
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Absen',
            style: TextStyle(
                color: Colors.black,
                fontSize: S.w / 22,
                fontWeight: FontWeight.w700)),
        actions: [
          if (auth.currentState.classRole.isOrtu)
            Center(
              child: InkWell(
                onTap: () => context.push<void>(IzinGuru()),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Text('Izin',
                      style: TextStyle(
                          color: SiswamediaTheme.green,
                          fontSize: S.w / 28,
                          fontWeight: FontWeight.w600)),
                ),
              ),
            )
        ],
        bottom: TabBar(
            controller: tabController,
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.black,
            indicatorWeight: 4,
            isScrollable: true,
            tabs: [Tab(text: 'Hari Ini'), Tab(text: 'Total')]),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          StateBuilder<ScheduleState>(
              observe: () => scheduleState,
              builder: (context, _) {
                return RefreshIndicator(
                  key: refreshIndicatorKey,
                  onRefresh: retrieveData,
                  child: WhenRebuilder<ScheduleState>(
                      observe: () => scheduleState,
                      onWaiting: () => WaitingView(),
                      onIdle: () => WaitingView(),
                      onError: (dynamic error) => ErrorView(error: error),
                      onData: (data) {
                        if (data.datamap == null) {
                          return Center(child: CircularProgressIndicator());
                        }
                        return ListView(
                          children: [
                            Timeline(
                              padding:
                                  EdgeInsets.symmetric(horizontal: S.w * .05),
                              indicatorColor: Color(0xffE1E1E1),
                              lineColor: Color(0xffE1E1E1),
                              startDate: DateTime.now(),
                              endDate: DateTime.now(),
                              date: data.datamap,
                              indicatorSize: 10,
                              children: [],
                            ),
                          ],
                        );
                      }),
                );
              }),
          StateBuilder<AbsensiState>(
              observe: () => absensiState,
              builder: (context, _) {
                return RefreshIndicator(
                  key: refreshIndicatorKey2,
                  onRefresh: retrieveData,
                  child: WhenRebuilder<AbsensiState>(
                      observe: () => absensiState,
                      onWaiting: () => WaitingView(),
                      onIdle: () => WaitingView(),
                      onError: (dynamic error) => ErrorView(error: error),
                      onData: (data) {
                        if (data.model.data == null) {
                          return ListView(
                            children: [
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * .25),
                              Center(
                                  child: Text('Data Masih Kosong',
                                      style: semiBlack.copyWith(
                                          fontSize: S.w / 28))),
                            ],
                          );
                        }
                        return ListView(children: [
                          Highlight(
                            animation: animation,
                            controller: controller,
                            title: 'Rekap Absensi',
                          ),
                          Column(
                            children: data.model.data.coursesAbsenSummary
                                .map((e) => Container(
                                      width: S.w * .9,
                                      height: S.w * .25,
                                      margin: EdgeInsets.symmetric(
                                          vertical: S.w * .025,
                                          horizontal: S.w * .05),
                                      padding: EdgeInsets.symmetric(
                                          vertical: S.w * .025,
                                          horizontal: S.w * .05),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          boxShadow: [
                                            BoxShadow(
                                                offset: Offset(2, 2),
                                                spreadRadius: 2,
                                                blurRadius: 2,
                                                color:
                                                    Colors.grey.withOpacity(.2))
                                          ]),
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              e.courseName,
                                              style: TextStyle(
                                                fontSize: S.w / 25,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black87,
                                              ),
                                            ),
                                            Row(children: [
                                              RichText(
                                                text: TextSpan(
                                                  style: TextStyle(
                                                    fontSize: 14.0,
                                                    color: Colors.black,
                                                  ),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: 'Hadir : ',
                                                        style: TextStyle(
                                                            fontSize: S.w / 28,
                                                            color: Colors
                                                                .grey[600])),
                                                    TextSpan(
                                                        text:
                                                            '${e.attend} / ${e.totalExistSchedule}',
                                                        style: TextStyle(
                                                            fontSize: S.w / 32,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color: Colors
                                                                .black87)),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(width: 20),
                                              RichText(
                                                text: TextSpan(
                                                  style: TextStyle(
                                                    fontSize: 14.0,
                                                    color: Colors.black,
                                                  ),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: 'Izin : ',
                                                        style: TextStyle(
                                                            fontSize: S.w / 28,
                                                            color: Colors
                                                                .grey[600])),
                                                    TextSpan(
                                                        text: '${e.permit}',
                                                        style: TextStyle(
                                                            fontSize: S.w / 32,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color: Colors
                                                                .black87)),
                                                  ],
                                                ),
                                              ),
                                            ]),
                                            Stack(
                                              children: [
                                                Container(
                                                    height: 15,
                                                    width: (S.w * .8) *
                                                        ((e.totalExistSchedule ==
                                                                    0
                                                                ? 0
                                                                : e.attend *
                                                                    100 ~/
                                                                    e.totalExistSchedule) /
                                                            100),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              S.w),
                                                      color: Color(0xffDCEBCD),
                                                    )),
                                                Container(
                                                  height: 15,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            S.w),
                                                    border: Border.all(
                                                        color: SiswamediaTheme
                                                            .green),
                                                  ),
                                                  child: Center(
                                                      child: Text(
                                                          '${e.totalExistSchedule == 0 ? 0 : e.attend * 100 ~/ e.totalExistSchedule}%',
                                                          style: TextStyle(
                                                              color:
                                                                  SiswamediaTheme
                                                                      .green,
                                                              fontSize:
                                                                  S.w / 40,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700))),
                                                ),
                                              ],
                                            )
                                          ]),
                                    ))
                                .toList(),
                          )
                        ]);
                      }),
                );
              }),
        ],
      ),
    );
  }
}

class Highlight extends StatelessWidget {
  Highlight(
      {this.animation, this.controller, this.button, this.title, this.item});
  final Animation<double> animation;
  final AnimationController controller;
  final Widget button;
  final String title;
  final SummaryAbsensiItem item;

  @override
  Widget build(BuildContext context) {
    var absensiState = Injector.getAsReactive<AbsensiState>();
    SummaryAbsensiItem data;
    if (item != null) {
      data = item;
    } else {
      data = absensiState.state.model.data;
    }
    var attend = data.generalAbsenSummary.attend;
    var totalExist = data.generalAbsenSummary.totalExistSchedule;
    var total = totalExist.isZero ? 0.0 : attend / totalExist;
    return Container(
      width: S.w * .9,
      height: S.w * .45,
      margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .035),
      padding: EdgeInsets.symmetric(horizontal: S.w * .04, vertical: S.w * .04),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
                offset: Offset(2, 2),
                spreadRadius: 2,
                blurRadius: 2,
                color: Colors.grey.withOpacity(.2))
          ]),
      child: Row(
        children: [
          AbsenCurve(
            animation: animation,
            controller: controller,
            angle: (360 * total).round(),
            progress: (100 * total).round(),
          ),
          SizedBox(width: S.w * .05),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(title,
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: S.w / 25,
                        fontWeight: FontWeight.w600)),
                Text('$attend / $totalExist Hadir',
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: S.w / 32,
                        fontWeight: FontWeight.w400)),
                //button
                button ?? SizedBox(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
