part of '_absensi.dart';

class AbsensiTeacher extends StatefulWidget {
  @override
  _AbsensiTeacherState createState() => _AbsensiTeacherState();
}

class _AbsensiTeacherState extends State<AbsensiTeacher>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Absen',
            style: TextStyle(color: Colors.black, fontSize: S.w / 22)),
        bottom: TabBar(
            controller: tabController,
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.black,
            indicatorWeight: 4,
            isScrollable: true,
            tabs: [Tab(text: 'Hari Ini'), Tab(text: 'Total')]),
      ),
      body: Container(),
    );
  }
}
