part of '_absensi.dart';

class EditAbsenSelectedMonth extends StatefulWidget {
  final int selectedMonth;
  final int courseId;

  EditAbsenSelectedMonth({this.selectedMonth, this.courseId});

  @override
  _EditAbsenSelectedMonthState createState() => _EditAbsenSelectedMonthState();
}

class _EditAbsenSelectedMonthState extends State<EditAbsenSelectedMonth> {
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final editState = GlobalState.editAbsensiSelected();

  ScrollController _header;
  ScrollController _body;
  LinkedScrollControllerGroup _controllers;

  @override
  void initState() {
    super.initState();
    _controllers = LinkedScrollControllerGroup();
    _header = _controllers.addAndGet();
    _body = _controllers.addAndGet();

    Initializer(
      rIndicator: refreshIndicatorKey,
      reactiveModel: editState,
      state: editState.state.dataMap.containsKey(widget.selectedMonth),
      cacheKey: 'time_edit_absen_month_${widget.selectedMonth}',
    ).initialize();
  }

  Future<void> retrieveData() async {
    await editState
        .setState((s) => s.retrieveData(widget.courseId, widget.selectedMonth));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SiswamediaAppBar(
        context: context,
        title: 'Absen ${bulanFull.elementAt(widget.selectedMonth)}',
      ),
      body: StateBuilder<EditAbsenSelectedState>(
        observe: () => editState,
        builder: (_, snapshot) {
          return WhenRebuilder<EditAbsenSelectedState>(
            observe: () => editState,
            onWaiting: () => WaitingView(),
            onIdle: () => WaitingView(),
            onError: (dynamic error) => ViewErrorClassroom(error: error),
            onData: (data) {
              return Column(
                children: [
                  HeaderInfoEditAbsen(),
                  Expanded(
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          child: Stack(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SingleChildScrollView(
                                controller: _body,
                                scrollDirection: Axis.horizontal,
                                child: Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 80.0, left: 160),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: _buildRows(editState
                                          .state
                                          .editAbsenModel
                                          .data
                                          .studentsFullName
                                          .length),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey[300],
                                      offset: Offset(2, 0),
                                      spreadRadius: 2,
                                      blurRadius: 2)
                                ]),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 80.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: _buildCellsName(editState
                                        .state
                                        .dataMap[widget.selectedMonth]
                                        .studentsFullName
                                        .length),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SingleChildScrollView(
                            controller: _header,
                            scrollDirection: Axis.horizontal,
                            child: HeaderDate()),
                        HeaderName(),
                      ],
                    ),
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }

  List<Widget> _buildCells(int count, int baseIndex) {
    return List.generate(count, (index) {
      var flag = editState.state.dataMap[widget.selectedMonth].recordAbsen
          .where((element) =>
              element.scheduleMonth.toInteger == DateTime.now().month &&
              element.scheduleDate.toInteger == index + 1 &&
              element.userId ==
                  editState.state.dataMap[widget.selectedMonth].studentsFullName
                      .elementAt(baseIndex)
                      .userId)
          .isNotEmpty;
      return Container(
        alignment: Alignment.center,
        width: 50.0,
        height: 50.0,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(bottom: BorderSide(color: Colors.grey[300]))),
        padding: EdgeInsets.all(4.0),
        child: StatusAbsen(
            status: flag
                ? editState.state.dataMap[widget.selectedMonth].recordAbsen
                    .where((element) =>
                        int.parse(element.scheduleMonth) ==
                            DateTime.now().month &&
                        int.parse(element.scheduleDate) == index + 1 &&
                        element.userId ==
                            editState.state.dataMap[widget.selectedMonth]
                                .studentsFullName
                                .elementAt(baseIndex)
                                .userId)
                    .first
                    .status
                : 3,
            exist: flag),
      );
    });
  }

  List<Widget> _buildCellsName(int count) {
    return List.generate(
      count,
      (index) {
        return Container(
          alignment: Alignment.centerLeft,
          width: 160.0,
          height: 50.0,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: index == 0
                  ? [
                      BoxShadow(
                          color: Colors.grey[300],
                          offset: Offset(4, 4),
                          spreadRadius: 2,
                          blurRadius: 2)
                    ]
                  : []),
          child: Text(
              '${editState.state.dataMap[widget.selectedMonth].studentsFullName.elementAt(index).fullName}',
              textAlign: TextAlign.left,
              style: descBlack.copyWith(fontSize: S.w / 28)),
        );
      },
    );
  }

  List<Widget> _buildRows(int count) {
    return List.generate(
      count,
      (index) {
        return Row(
          children: _buildCells(DateTime.now().totalDayInMonth, index),
        );
      },
    );
  }
}
