part of '../_absensi.dart';

class PermohonanIzin extends StatefulWidget {
  final DataStudentPermit data;

  const PermohonanIzin({Key key, this.data}) : super(key: key);

  @override
  _PermohonanIzinState createState() => _PermohonanIzinState();
}

class _PermohonanIzinState extends State<PermohonanIzin> {
  final authState = GlobalState.auth();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: vText('Permohonan Izin'),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.primary,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          margin: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 2),
                  child: vText('Permohonan Izin Absensi',
                      fontSize: 14, fontWeight: FontWeight.bold),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: vText('${formattedDate(widget.data.createdAt)}',
                      fontSize: 10, fontWeight: FontWeight.bold),
                ),
                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: '${widget.data.parentName}',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.black87)),
                      TextSpan(
                          text: ' sebagai Wali dari ',
                          style: TextStyle(
                            fontSize: 12,
                            color: SiswamediaTheme.textGrey[260],
                          )),
                      TextSpan(
                          text: '${widget.data.studentName}',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.black87)),
                      TextSpan(
                          text: ' mengajukan permohonan izin untuk :',
                          style: TextStyle(
                            fontSize: 12,
                            color: SiswamediaTheme.textGrey[260],
                          )),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16, top: 16),
                  child: vText('Waktu :',
                      fontSize: 14, fontWeight: FontWeight.bold),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: vText('${formattedDate(widget.data.from)}',
                      fontSize: 12, fontWeight: FontWeight.bold),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: vText(
                    'Hingga',
                    fontSize: 14,
                    color: SiswamediaTheme.textGrey[260],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: vText('${formattedDate(widget.data.to)}',
                      fontSize: 12, fontWeight: FontWeight.bold),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: vText(
                    'Lamanya hari',
                    color: SiswamediaTheme.textGrey[260],
                    fontSize: 14,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: vText('${widget.data.dayDuration} Hari',
                      fontSize: 12, fontWeight: FontWeight.bold),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child:
                      vText('Pesan', fontSize: 12, fontWeight: FontWeight.bold),
                ),
                vText(
                  widget.data.message,
                  fontSize: 12,
                ),
                SizedBox(height: 16),
                Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: vText('Lampiran:',
                      fontSize: 12, fontWeight: FontWeight.bold),
                ),
                Column(
                  children: widget.data.attachments
                      .map((e) => InkWell(
                            onTap: () => launch(e.link),
                            child: vText(e.name,
                                fontSize: 12, color: SiswamediaTheme.green),
                          ))
                      .toList(),
                ),
                SizedBox(height: 32),
                if (widget.data.isBelumDiRespon)
                  Row(
                    children: [
                      buttonIzin('Terima Permohonan', SiswamediaTheme.primary,
                          () {
                        dialogSave('Terima Permohonan');
                      }),
                    ],
                  ),
                if (widget.data.isBelumDiRespon)
                  Row(
                    children: [
                      buttonIzin(
                          'Tolak Permohonan', SiswamediaTheme.greybgButton, () {
                        dialogSave('Tolak Permohonan');
                      }),
                    ],
                  ),
              ],
            ),
          ),
        ));
  }

  void dialogSave(String title) {
    int typeIzin = 4;
    TextEditingController description = TextEditingController();

    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              // String role = someCapitalizedString(data.role);

              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(
                              height: 15,
                            ),
                            Center(
                              child: Text(title,
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w800)),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text(
                                title == 'Terima Permohonan'
                                    ? 'Pilih status absensi dan beri alasan jika diperlukan '
                                    : "Absensi siswa akan otomatis menjadi 'Tidak Hadir / Absen', beri alasan untuk tolak permohonan",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12)),
                            SizedBox(
                              height: 16,
                            ),
                            if (title == 'Terima Permohonan')
                              Row(
                                children: [
                                  buttonIzin(
                                      'Izin',
                                      typeIzin == 4
                                          ? SiswamediaTheme.textLightBlue
                                          : SiswamediaTheme.greybgButton,
                                      () async {
                                    setState(() {
                                      typeIzin = 4;
                                    });
                                  }),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  buttonIzin(
                                      'Sakit',
                                      typeIzin == 5
                                          ? SiswamediaTheme.textLightBlue
                                          : SiswamediaTheme.greybgButton,
                                      () async {
                                    setState(() {
                                      typeIzin = 5;
                                    });
                                  }),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  buttonIzin(
                                      '1/2 Harizin',
                                      typeIzin == 2
                                          ? SiswamediaTheme.textLightBlue
                                          : SiswamediaTheme.greybgButton,
                                      () async {
                                    print(typeIzin.toString());
                                    setState(() {
                                      typeIzin = 2;
                                    });
                                  }),
                                ],
                              ),
                            SizedBox(
                              height: 16,
                            ),
                            Composer(
                              description: description,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            title == 'Terima Permohonan'
                                ? Row(
                                    children: [
                                      buttonIzin('Perbarui Status',
                                          SiswamediaTheme.primary, () async {
                                        var dataMap = <String, dynamic>{
                                          'message': description.text,
                                          'status': 1,
                                          'student_absent_status': typeIzin,
                                        };
                                        var result =
                                            await CreateAbsensi.responseGuru(
                                                body: dataMap,
                                                id: widget.data.studentPermitId
                                                    .toString());
                                        print(dataMap);
                                        if (result.statusCode < 300) {
                                          CustomFlushBar.successFlushBar(
                                            'Berhasil',
                                            context,
                                          );
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                        } else {
                                          CustomFlushBar.errorFlushBar(
                                            result.message,
                                            context,
                                          );
                                        }
                                      }),
                                    ],
                                  )
                                : Row(
                                    children: [
                                      buttonIzin(
                                          'Kirim', SiswamediaTheme.primary,
                                          () async {
                                        if (description.text == '') {
                                          CustomFlushBar.errorFlushBar(
                                            'Mohon mengisi catatan',
                                            context,
                                          );
                                        } else {
                                          var dataMap = <String, dynamic>{
                                            'message': description.text,
                                            'status': 2,
                                          };
                                          var result =
                                              await CreateAbsensi.responseGuru(
                                                  body: dataMap,
                                                  id: widget
                                                      .data.studentPermitId
                                                      .toString());
                                          print(dataMap);
                                          if (result.statusCode < 300) {
                                            CustomFlushBar.successFlushBar(
                                              'Berhasil',
                                              context,
                                            );

                                            Navigator.pop(context);
                                            Navigator.pop(context);
                                            Navigator.pop(context);
                                          }
                                        }
                                      }),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      buttonIzin('Batalkan',
                                          SiswamediaTheme.greybgButton,
                                          () async {
                                        Navigator.pop(context);
                                      }),
                                    ],
                                  )
                          ],
                        ),
                      )));
            }));
  }

  Widget buttonIzin(String title, Color color, Function onTap) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(8)),
          alignment: Alignment.center,
          padding: EdgeInsets.all(11),
          margin: EdgeInsets.only(bottom: 12),
          child: vText(title, color: Colors.white),
        ),
      ),
    );
  }
}
