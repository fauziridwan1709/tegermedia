part of '../_absensi.dart';

class KotakMasuk extends StatefulWidget {
  @override
  _KotakMasukState createState() => _KotakMasukState();
}

class _KotakMasukState extends State<KotakMasuk>
    with SingleTickerProviderStateMixin {
  // String _choose1;

  final absensiState = Injector.getAsReactive<AbsensiState>();
  String strDate = '';
  TextEditingController description = TextEditingController();
  String endDate = '';

  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;

  Animation<double> animation;
  AnimationController controller;
  final authState = GlobalState.auth();

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    controller =
        AnimationController(duration: Duration(milliseconds: 700), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeInOut));
    controller.forward();
    Initializer(
      reactiveModel: absensiState,
      cacheKey: 'time_list_absen_izin',
      state: absensiState.state.model != null,
      rIndicator: _refreshIndicatorKey,
    ).initialize();
  }

  Future<void> retrieveData() async {
    await absensiState.setState((s) => s.getIzin());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: vText('Kotak Masuk'),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.primary,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: StateBuilder<AbsensiState>(
            observe: () => absensiState,
            builder: (context, constrains) {
              return RefreshIndicator(
                  onRefresh: retrieveData,
                  key: _refreshIndicatorKey,
                  child: WhenRebuilder<AbsensiState>(
                    observe: () => absensiState,
                    onWaiting: () => WaitingView(),
                    onIdle: () => WaitingView(),
                    onError: (dynamic error) => ErrorView(error: error),
                    onData: (dataSnapshot) {
                      if (dataSnapshot.dataIzin.isEmpty) {
                        return ErrorView(
                            error:
                                SiswamediaException('Tidak ada Kotak masuk'));
                      }
                      return ListView.builder(
                        itemBuilder: (c, i) => _item(dataSnapshot.dataIzin[i]),
                        itemCount: dataSnapshot.dataIzin.length,
                      );
                    },
                  ));
            }));
  }

  Widget _item(DataStudentPermit data) {
    return InkWell(
      onTap: () {
        Navigator.push<void>(
          context,
          MaterialPageRoute(
              builder: (context) => PermohonanIzin(
                    data: data,
                  )),
        ).then((value) => _refreshIndicatorKey.currentState.show());
      },
      child: Container(
        margin: EdgeInsets.only(top: 10, right: 16, left: 16),
        padding: EdgeInsets.symmetric(vertical: 12),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                  offset: Offset(2, 2),
                  spreadRadius: 2,
                  blurRadius: 2,
                  color: Colors.grey.withOpacity(.2))
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  vText(data.respondMessage.toString(),
                      color: SiswamediaTheme.textGrey[500]),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                    decoration: BoxDecoration(
                        color: data.status == 0
                            ? SiswamediaTheme.textError
                            : SiswamediaTheme.textLightBlue,
                        borderRadius: BorderRadius.circular(3)),
                    child: vText(data.status == 0 ? 'Baru' : 'Sudah direspon',
                        fontSize: 8, color: Colors.white),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: [
                  Container(
                    width: 50,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        vText('Dari'),
                        SizedBox(
                          height: 2,
                        ),
                        vText('Wali'),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      vText(data.studentName),
                      SizedBox(
                        height: 2,
                      ),
                      vText(data.parentName),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: vText('${formattedDate(data.createdAt)}'),
            )
          ],
        ),
      ),
    );
  }
}
