part of '_absensi.dart';

class AbsensiWaliKelas extends StatefulWidget {
  @override
  _AbsensiWaliKelasState createState() => _AbsensiWaliKelasState();
}

class _AbsensiWaliKelasState extends State<AbsensiWaliKelas>
    with SingleTickerProviderStateMixin {
  final authState = Injector.getAsReactive<AuthState>();
  final absensiState = Injector.getAsReactive<AbsensiState>();
  final classState = Injector.getAsReactive<ClassState>();

  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;

  Animation<double> animation;
  AnimationController controller;

  String value = 'Januari';

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    controller =
        AnimationController(duration: Duration(milliseconds: 700), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeInOut));
    controller.forward();
    Initializer(
      reactiveModel: absensiState,
      cacheKey: 'time_list_absen',
      state: false,
      rIndicator: _refreshIndicatorKey,
    ).initialize();
  }

  Future<void> retrieveData() async {
    await absensiState
        .setState((s) => s.getSummary(authState.state.currentState.classId));
  }

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Absen',
            style: TextStyle(
                color: Colors.black,
                fontSize: S.w / 22,
                fontWeight: FontWeight.w700)),
        actions: [
          // Center(
          //   child: InkWell(
          //     onTap: () {
          //       context.push(IzinGuru());
          //     },
          //     child: Padding(
          //       padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          //       child: Text('Izin',
          //           style: TextStyle(
          //               color: SiswamediaTheme.green,
          //               fontSize: S.w / 28,
          //               fontWeight: FontWeight.w600)),
          //     ),
          //   ),
          // )
        ],
      ),
      body: StateBuilder<AbsensiState>(
          observe: () => absensiState,
          builder: (context, constrains) {
            return RefreshIndicator(
                onRefresh: retrieveData,
                key: _refreshIndicatorKey,
                child: WhenRebuilder<AbsensiState>(
                  observe: () => absensiState,
                  onWaiting: () => WaitingView(),
                  onIdle: () => WaitingView(),
                  onError: (dynamic error) => ErrorView(error: error),
                  onData: (dataSnapshot) {
                    if (dataSnapshot.model == null ||
                        dataSnapshot.model.data == null) {
                      return EmptyRecordText();
                    }
                    var data =
                        absensiState.state.model.data.coursesAbsenSummary;
                    if (!isPortrait) {
                      return Center(
                        child: AbsensiView(
                            value: value,
                            onChange: (s) => setState(() => value = s),
                            animation: animation,
                            controller: controller,
                            data: data,
                            userState: authState.state.currentState,
                            listClass:
                                classState.state.classMap.values.toList(),
                            onTap: () =>
                                navigate(context, AbsensiWalikelasDetail())),
                      );
                    } else {
                      return AbsensiView(
                          value: value,
                          onChange: (s) => setState(() => value = s),
                          animation: animation,
                          controller: controller,
                          data: data,
                          userState: authState.state.currentState,
                          listClass: classState.state.classMap.values.toList(),
                          onTap: () =>
                              navigate(context, AbsensiWalikelasDetail()));
                    }
                  },
                ));
          }),
    );
  }
}
