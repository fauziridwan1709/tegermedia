part of '_absensi.dart';

class EditAbsen extends StatefulWidget {
  final CoursesAbsenSummary course;

  EditAbsen({@required this.course}) : assert(course != null);

  @override
  _EditAbsenState createState() => _EditAbsenState();
}

class _EditAbsenState extends State<EditAbsen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  ScrollController _header;
  ScrollController _body;
  LinkedScrollControllerGroup _controllers;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;
  GlobalKey<ScaffoldState> _scaffoldKey;
  ReactiveModel<EditAbsensiState> _editAbsenState;
  //todo
  // int _selectedMonth;

  var listAbsen = ['Hadir', '1/2 Hari', 'Absen', 'Izin', 'Sakit'];

  var desc = TextEditingController();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    _controllers = LinkedScrollControllerGroup();
    _header = _controllers.addAndGet();
    _body = _controllers.addAndGet();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    _scaffoldKey = GlobalKey<ScaffoldState>();
    _editAbsenState = Injector.getAsReactive<EditAbsensiState>();
    // _selectedMonth = DateTime.now().month;
    Initializer(
            reactiveModel: _editAbsenState,
            cacheKey: TIME_EDIT_ABSENSI,
            rIndicator: _refreshIndicatorKey,
            state: _editAbsenState.state.editAbsenModel == null)
        .initialize();
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await _editAbsenState.setState(
          (s) async => await s.getAbsen(widget.course.courseId),
          catchError: true, onError: (context, dynamic error) {
        print(error);
      });
      // completer.complete();
    });
  }

  String getStatusName(int status) {
    if (status == 1) {
      return 'Hadir';
    } else if (status == 2) {
      return 'Setengah Hari';
    } else if (status == 3) {
      return 'Absen';
    } else if (status == 4) {
      //todo izin
      return 'Izin';
    } else {
      //todo sakit
      return 'Sakit';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: SiswamediaAppBar(
        context: context,
        title: widget.course.courseName.capitalizeFirstOfEach,
        actions: [
          IconButton(
            color: Colors.grey,
            icon: Icon(Icons.info),
            onPressed: () => showDialog<void>(
                context: context,
                builder: (_) {
                  return Dialog(
                    shape: RoundedRectangleBorder(borderRadius: radius(12)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .07, vertical: S.w * .06),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CustomText('Petunjuk Kehadiran', Kind.heading3),
                          SizedBox(height: 20),
                          GridView.count(
                            crossAxisCount: 2,
                            shrinkWrap: true,
                            childAspectRatio: 3,
                            children: [1, 2, 4, 5, 3]
                                .map((e) => Row(
                                      children: [
                                        StatusAbsen(status: e, exist: true),
                                        SizedBox(width: 20),
                                        Expanded(
                                          child: Text(getStatusName(e),
                                              style: semiBlack.copyWith(
                                                  fontSize: S.w / 32)),
                                        )
                                      ],
                                    ))
                                .toList(),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ),
          // IconButton(
          //   icon: Icon(Icons.history),
          //   color: SiswamediaTheme.green,
          //   onPressed: () => navigate(
          //       context,
          //       EditAbsenSelectedMonth(
          //           courseId: widget.course.courseId,
          //           selectedMonth: _selectedMonth)),
          // )
        ],
        bottom: TabBar(
          controller: _tabController,
          unselectedLabelColor: Colors.grey,
          labelColor: Colors.black,
          indicatorWeight: 4,
          isScrollable: true,
          tabs: [
            Tab(text: 'Hari Ini'),
            Tab(text: 'Bulan ini'),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          StateBuilder<EditAbsensiState>(
            observe: () => _editAbsenState,
            builder: (_, snapshot) {
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: retrieveData,
                child: WhenRebuilder<EditAbsensiState>(
                  observe: () => _editAbsenState,
                  onWaiting: () => WaitingView(),
                  onIdle: () => WaitingView(),
                  onError: (dynamic error) => ErrorView(error: error),
                  onData: (data) {
                    return ListView(
                      children: [
                        // HeaderInfoEditAbsen(),
                        Stack(
                          children: [
                            SingleChildScrollView(
                              child: Stack(
                                // crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 80.0, left: 160),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: _buildEditAbsen(
                                            _editAbsenState.state.editAbsenModel
                                                .data.studentsFullName.length),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey[300],
                                          offset: Offset(2, 0),
                                          spreadRadius: 2,
                                          blurRadius: 2)
                                    ]),
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 80.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: _buildCellsName(
                                            _editAbsenState.state.editAbsenModel
                                                .data.studentsFullName.length),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 160.0),
                              child: HeaderKehadiran(),
                            ),
                            HeaderName(),
                          ],
                        ),
                      ],
                    );
                  },
                ),
              );
            },
          ),
          StateBuilder<EditAbsensiState>(
            observe: () => _editAbsenState,
            builder: (_, snapshot) {
              return WhenRebuilder<EditAbsensiState>(
                observe: () => _editAbsenState,
                onWaiting: () => WaitingView(),
                onIdle: () => WaitingView(),
                onError: (dynamic error) => ViewErrorClassroom(error: error),
                onData: (data) {
                  return Column(
                    children: [
                      // HeaderInfoEditAbsen(),
                      Expanded(
                        child: Stack(
                          children: [
                            SingleChildScrollView(
                              child: Stack(
                                // crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SingleChildScrollView(
                                    controller: _body,
                                    scrollDirection: Axis.horizontal,
                                    child: Container(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 80.0, left: 160),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: _buildRows(_editAbsenState
                                              .state
                                              .editAbsenModel
                                              .data
                                              .studentsFullName
                                              .length),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey[300],
                                          offset: Offset(2, 0),
                                          spreadRadius: 2,
                                          blurRadius: 2)
                                    ]),
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 80.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: _buildCellsName(
                                            _editAbsenState.state.editAbsenModel
                                                .data.studentsFullName.length),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SingleChildScrollView(
                                controller: _header,
                                scrollDirection: Axis.horizontal,
                                child: HeaderDate()),
                            HeaderName(),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          )
        ],
      ),
    );
  }

  List<Widget> _buildCells(int count, int baseIndex) {
    return List.generate(count, (index) {
      var data = _editAbsenState.state.editAbsenModel.data.recordAbsen.where(
          (element) =>
              element.scheduleMonth.toInteger == DateTime.now().month &&
              element.scheduleDate.toInteger == index + 1 &&
              element.userId ==
                  _editAbsenState.state.editAbsenModel.data.studentsFullName
                      .elementAt(baseIndex)
                      .userId);
      var flag = data.isNotEmpty;
      var selected = flag ? listAbsen[data.first.status - 1] : null;
      return Container(
        alignment: Alignment.center,
        width: 50.0,
        height: 50.0,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(bottom: BorderSide(color: Colors.grey[300]))),
        padding: EdgeInsets.all(4.0),
        child: StatusAbsen(
            onTap: (a, b, c) => showDialog<void>(
                context: context,
                builder: (_) => StatefulBuilder(
                      builder: (__, void Function(void Function()) setState) {
                        return Dialog(
                          shape:
                              RoundedRectangleBorder(borderRadius: radius(12)),
                          child: Container(
                            width: S.w * .75,
                            padding: EdgeInsets.symmetric(
                                horizontal: S.w * .05, vertical: S.w * .05),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                CustomText('Ubah Kehadiran', Kind.heading2,
                                    align: TextAlign.center),
                                SizedBox(height: 20),
                                Row(
                                  children: [
                                    Text('Pilih Kehadiran',
                                        style: semiBlack.copyWith(
                                            fontSize: S.w / 30),
                                        textAlign: TextAlign.left),
                                  ],
                                ),
                                GridView.count(
                                  crossAxisCount: 3,
                                  childAspectRatio: 2,
                                  shrinkWrap: true,
                                  mainAxisSpacing: 8,
                                  crossAxisSpacing: 8,
                                  children: listAbsen
                                      .map((e) => CustomContainer(
                                            color: e == selected
                                                ? SiswamediaTheme.green
                                                : Colors.white,
                                            radius: radius(12),
                                            onTap: () =>
                                                setState(() => selected = e),
                                            border: Border.all(
                                                color: e == selected
                                                    ? SiswamediaTheme.green
                                                    : SiswamediaTheme.border),
                                            child: Center(
                                                child: Text(
                                              e,
                                              style: semiWhite.copyWith(
                                                  fontSize: S.w / 30,
                                                  color: e == selected
                                                      ? Colors.white
                                                      : Colors.black87),
                                            )),
                                          ))
                                      .toList(),
                                ),
                                SizedBox(height: 20),
                                Row(
                                  children: [
                                    Text('Catatan (opsional)',
                                        style: semiBlack.copyWith(
                                            fontSize: S.w / 30),
                                        textAlign: TextAlign.left),
                                  ],
                                ),
                                TextField(
                                  controller: desc,
                                  style: normalText(context),
                                  maxLines: 4,
                                  minLines: 3,
                                  decoration: InputDecoration(
                                    hintText: 'Catatan',
                                    hintStyle: descBlack.copyWith(
                                        fontSize: S.w / 30,
                                        color: Color(0xffB9B9B9)),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 25),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xffD8D8D8)),
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: SiswamediaTheme.green),
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15),
                                CustomContainer(
                                  color: SiswamediaTheme.green,
                                  radius: radius(S.w),
                                  // width: S.w * .6,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 6),
                                  onTap: () async {
                                    var model = ChangeAbsenModel(
                                        absenId: data.first.absenId,
                                        status: listAbsen.indexOf(selected) + 1,
                                        updateTime: true,
                                        hours: DateTime.now().hour,
                                        minute: DateTime.now().minute,
                                        notes: desc.text);
                                    await EditAbsensiServices.updateAbsen(
                                            model: model)
                                        .then((value) async {
                                      print(value.statusCode);
                                      // Navigator.pop(context);
                                      if (value.statusCode.isSuccessOrCreated) {
                                        await _editAbsenState.setState((s) =>
                                            s.editAbsen(data.first.userId,
                                                listAbsen.indexOf(selected) + 1,
                                                isToday: false,
                                                tanggal: index + 1));
                                        Navigator.pop(context);
                                        return CustomFlushBar.successFlushBar(
                                            value.message, context);
                                      } else {
                                        Navigator.pop(context);
                                        return CustomFlushBar.errorFlushBar(
                                            'Terjadi kesalahan', context);
                                      }
                                    });
                                  },
                                  child: Center(
                                      child: Text('Simpan',
                                          style: semiWhite.copyWith(
                                              fontSize: S.w / 28))),
                                ),
                                SizedBox(height: 15),
                                //todo
                              ],
                            ),
                          ),
                        );
                      },
                    )),
            status: flag
                ? _editAbsenState.state.editAbsenModel.data.recordAbsen
                    .where((element) =>
                        int.parse(element.scheduleMonth) ==
                            DateTime.now().month &&
                        int.parse(element.scheduleDate) == index + 1 &&
                        element.userId ==
                            _editAbsenState
                                .state.editAbsenModel.data.studentsFullName
                                .elementAt(baseIndex)
                                .userId)
                    .first
                    .status
                : 3,
            exist: flag),
      );
    });
  }

  List<Widget> _buildCellsName(int count) {
    return List.generate(
      count,
      (index) {
        return Container(
          alignment: Alignment.centerLeft,
          width: 160.0,
          height: 50.0,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: index == 0
                  ? [
                      BoxShadow(
                          color: Colors.grey[300],
                          offset: Offset(4, 4),
                          spreadRadius: 2,
                          blurRadius: 2)
                    ]
                  : []),
          child: Text(
              '${_editAbsenState.state.editAbsenModel.data.studentsFullName.elementAt(index).fullName}',
              textAlign: TextAlign.left,
              style: descBlack.copyWith(fontSize: S.w / 28)),
        );
      },
    );
  }

  List<Widget> _buildRows(int count) {
    return List.generate(
      count,
      (index) {
        return Row(
          children: _buildCells(DateTime.now().totalDayInMonth, index),
        );
      },
    );
  }

  List<Widget> _buildEditAbsen(int count) {
    return List.generate(
      count,
      (baseIndex) {
        var data = _editAbsenState.state.editAbsenModel.data.recordAbsen.where(
            (element) =>
                int.parse(element.scheduleMonth) == DateTime.now().month &&
                int.parse(element.scheduleDate) == DateTime.now().day &&
                element.userId ==
                    _editAbsenState.state.editAbsenModel.data.studentsFullName
                        .elementAt(baseIndex)
                        .userId);
        var isExist = data.isNotEmpty;
        var selected = isExist ? listAbsen[data.first.status - 1] : null;
        desc.text = isExist ? data.first.notes : null;
        // print('mantap' + data.first.notes);
        if (data.isNotEmpty && !data.first.checkInTimeHour.isNegative) {
          return CustomContainer(
            height: 50,
            color: Colors.white,
            border: Border(bottom: BorderSide(color: Colors.grey[300])),
            padding: EdgeInsets.all(2.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                StatusAbsen(
                  data: isExist ? data.first : RecordAbsen(),
                  userId: _editAbsenState
                      .state.editAbsenModel.data.studentsFullName
                      .elementAt(baseIndex)
                      .userId,
                  select: true,
                  isToday: true,
                  exist: isExist,
                  status: data.first.status,
                ),
                SizedBox(width: 20),
                CustomContainer(
                    color: SiswamediaTheme.lightBorder,
                    radius: radius(4),
                    onTap: () => showDialog<void>(
                        context: context,
                        builder: (_) => StatefulBuilder(
                              builder: (__,
                                  void Function(void Function()) setState) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: radius(12)),
                                  child: Container(
                                    width: S.w * .75,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: S.w * .05,
                                        vertical: S.w * .05),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        CustomText(
                                            'Ubah Kehadiran', Kind.heading2,
                                            align: TextAlign.center),
                                        SizedBox(height: 20),
                                        Row(
                                          children: [
                                            Text('Pilih Kehadiran',
                                                style: semiBlack.copyWith(
                                                    fontSize: S.w / 30),
                                                textAlign: TextAlign.left),
                                          ],
                                        ),
                                        GridView.count(
                                          crossAxisCount: 3,
                                          childAspectRatio: 2,
                                          shrinkWrap: true,
                                          mainAxisSpacing: 8,
                                          crossAxisSpacing: 8,
                                          children: listAbsen
                                              .map((e) => CustomContainer(
                                                    color: e == selected
                                                        ? SiswamediaTheme.green
                                                        : Colors.white,
                                                    radius: radius(12),
                                                    onTap: () => setState(
                                                        () => selected = e),
                                                    border: Border.all(
                                                        color: e == selected
                                                            ? SiswamediaTheme
                                                                .green
                                                            : SiswamediaTheme
                                                                .border),
                                                    child: Center(
                                                        child: Text(
                                                      e,
                                                      style: semiWhite.copyWith(
                                                          fontSize: S.w / 30,
                                                          color: e == selected
                                                              ? Colors.white
                                                              : Colors.black87),
                                                    )),
                                                  ))
                                              .toList(),
                                        ),
                                        SizedBox(height: 20),
                                        Row(
                                          children: [
                                            Text('Catatan (opsional)',
                                                style: semiBlack.copyWith(
                                                    fontSize: S.w / 30),
                                                textAlign: TextAlign.left),
                                          ],
                                        ),
                                        TextField(
                                          controller: desc,
                                          style: normalText(context),
                                          maxLines: 4,
                                          minLines: 3,
                                          decoration: InputDecoration(
                                            hintText: 'Catatan',
                                            hintStyle: descBlack.copyWith(
                                                fontSize: S.w / 30,
                                                color: Color(0xffB9B9B9)),
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    vertical: 10.0,
                                                    horizontal: 25),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xffD8D8D8)),
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: SiswamediaTheme.green),
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 15),
                                        CustomContainer(
                                          color: SiswamediaTheme.green,
                                          radius: radius(S.w),
                                          // width: S.w * .6,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 6),
                                          onTap: () async {
                                            var model = ChangeAbsenModel(
                                                absenId: data.first.absenId,
                                                status: listAbsen
                                                        .indexOf(selected) +
                                                    1,
                                                updateTime: true,
                                                hours: DateTime.now().hour,
                                                minute: DateTime.now().minute,
                                                notes: desc.text);
                                            await EditAbsensiServices
                                                    .updateAbsen(model: model)
                                                .then((value) async {
                                              print(value.statusCode);
                                              // Navigator.pop(context);
                                              if (value.statusCode == 200 ||
                                                  value.statusCode == 201) {
                                                await _editAbsenState.setState(
                                                    (s) => s.editAbsen(
                                                        data.first.userId,
                                                        listAbsen.indexOf(
                                                                selected) +
                                                            1));
                                                Navigator.pop(context);
                                                return CustomFlushBar
                                                    .successFlushBar(
                                                        value.message, context);
                                              } else {
                                                Navigator.pop(context);
                                                return CustomFlushBar
                                                    .errorFlushBar(
                                                        'Terjadi kesalahan',
                                                        context);
                                              }
                                            });
                                          },
                                          child: Center(
                                              child: Text('Simpan',
                                                  style: semiWhite.copyWith(
                                                      fontSize: S.w / 28))),
                                        ),
                                        SizedBox(height: 15),
                                        //todo
                                      ],
                                    ),
                                  ),
                                );
                              },
                            )),
                    padding: EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                    child: Text('Ubah')),
              ],
            ),
          );
        }
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(5, (index) {
            return Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border:
                        Border(bottom: BorderSide(color: Colors.grey[300]))),
                padding: EdgeInsets.all(4.0),
                child: StatusAbsen(
                  onTap: (data, userId, status) => showDialog<void>(
                      context: context,
                      builder: (context2) => Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                          child: Container(
                            padding: EdgeInsets.all(S.w * .05),
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text('Mengubah Absen ?',
                                      style: secondHeadingBlack.copyWith(
                                          fontSize: S.w / 28)),
                                  SizedBox(height: 15),
                                  Text(
                                      'Apakah Anda yakin untuk mengupdate absen siswa ini ?',
                                      style: descBlack.copyWith(
                                          fontSize: S.w / 34),
                                      textAlign: TextAlign.center),
                                  SizedBox(height: 20),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: S.w * .01),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Container(
                                            height: S.w / 10,
                                            width: S.w / 3.5,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                              color: SiswamediaTheme.green,
                                            ),
                                            child: InkWell(
                                              onTap: () async {
                                                SiswamediaTheme.infoLoading(
                                                    context: context,
                                                    info: 'Mengupdate...');
                                                // print(absenId);

                                                if (data.absenId == 0) {
                                                  await CreateAbsensi.create(
                                                      model: CreateAbsensiModel(
                                                    checkInTime: DateTime.now()
                                                        .toUtc()
                                                        .toIso8601String(),
                                                    classId: GlobalState.auth()
                                                        .state
                                                        .currentState
                                                        .classId,
                                                    userId: userId,
                                                    courseId: data.courseId,
                                                    scheduleId: data.scheduleId,
                                                  )).then((value) async {
                                                    Navigator.pop(context);
                                                    print(value.id);
                                                    if (value.statusCode ==
                                                        200) {
                                                      Navigator.pop(context);
                                                      var model =
                                                          ChangeAbsenModel(
                                                        absenId: value.id,
                                                        status: status,
                                                        updateTime: true,
                                                        hours:
                                                            DateTime.now().hour,
                                                        minute: DateTime.now()
                                                            .minute,
                                                      );
                                                      await EditAbsensiServices
                                                          .updateAbsen(
                                                              model: model);
                                                      await _editAbsenState
                                                          .setState((s) =>
                                                              s.editAbsen(
                                                                  userId,
                                                                  status,
                                                                  isNew: true,
                                                                  absenId: value
                                                                      .id));
                                                      CustomFlushBar
                                                          .successFlushBar(
                                                        'Berhasil Diubah',
                                                        context,
                                                      );
                                                    } else {
                                                      Navigator.pop(context);
                                                      return CustomFlushBar
                                                          .errorFlushBar(
                                                              'Terjadi kesalahan',
                                                              context);
                                                    }
                                                  });
                                                } else {
                                                  var model = ChangeAbsenModel(
                                                    absenId: data.absenId,
                                                    status: status,
                                                    updateTime: true,
                                                    hours: DateTime.now().hour,
                                                    minute:
                                                        DateTime.now().minute,
                                                  );
                                                  await EditAbsensiServices
                                                          .updateAbsen(
                                                              model: model)
                                                      .then((value) async {
                                                    print(value.statusCode);
                                                    pop(context);
                                                    if (value.statusCode
                                                        .isSuccessOrCreated) {
                                                      await _editAbsenState
                                                          .setState((s) =>
                                                              s.editAbsen(
                                                                  userId,
                                                                  status,
                                                                  desc: desc
                                                                      .text));
                                                      pop(context);
                                                      return CustomFlushBar
                                                          .successFlushBar(
                                                              value.message,
                                                              context);
                                                    } else {
                                                      pop(context);
                                                      return CustomFlushBar
                                                          .errorFlushBar(
                                                              'Terjadi kesalahan',
                                                              context);
                                                    }
                                                  });
                                                }

                                                //Navigator.push(context,
                                                //  MaterialPageRoute<void>(builder: (_) => null));
                                              },
                                              child: Center(
                                                child: Text('Ya',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                              color: SiswamediaTheme.lightBlack,
                                            ),
                                            height: S.w / 10,
                                            width: S.w / 3.5,
                                            child: InkWell(
                                              onTap: () =>
                                                  Navigator.pop(context),
                                              child: Center(
                                                child: Text('Tidak',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                              ),
                                            ),
                                          ),
                                        ]),
                                  ),
                                  SizedBox(height: 10),
                                ]),
                          ))),
                  data: isExist ? data.first : RecordAbsen(),
                  userId: _editAbsenState
                      .state.editAbsenModel.data.studentsFullName
                      .elementAt(baseIndex)
                      .userId,
                  select: index + 1 == (isExist ? data.first.status : null),
                  isToday: true,
                  exist: isExist,
                  status: index + 1,
                ),
              ),
            );
          }),
        );
      },
    );
  }
}

class HeaderDate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      padding: const EdgeInsets.only(left: 160),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colors.grey[300],
            offset: Offset(0, -2),
            spreadRadius: 2,
            blurRadius: 2)
      ]),
      child: Row(
        children: List.generate(DateTime.now().totalDayInMonth, (index) {
          var date = DateTime.now().startMonth;
          var currentDate = date.add(Duration(days: index));
          var day = listOfDays[currentDate.weekday - 1];
          return Container(
            alignment: Alignment.center,
            width: 50.0,
            height: 80.0,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(bottom: BorderSide(color: Colors.grey[300]))),
            padding: EdgeInsets.all(4.0),
            child: Text(
                '$day\n${currentDate.day < 10 ? '0${currentDate.day}' : currentDate.day}',
                style: semiBlack.copyWith(fontSize: S.w / 28, height: 2),
                textAlign: TextAlign.center),
          );
        }),
      ),
    );
  }
}

const listStudent = [
  'fauzi',
  'fauziridwan',
  'ridwan nulhakim',
  'steven',
  'manokwari',
  'fauzi',
  'fauziridwan',
  'ridwan nulhakim',
  'steven',
  'manokwari',
  'fauzi',
  'fauziridwan',
  'ridwan nulhakim',
  'steven',
  'manokwari',
  'fauzi',
  'fauziridwan',
  'ridwan nulhakim',
  'steven',
  'manokwari'
];

const listOfDays = ['Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min'];

class StatusAbsen extends StatelessWidget {
  final int status;
  final int userId;
  final RecordAbsen data;
  // final int absenId;
  final int courseId;
  final int scheduleId;
  final bool exist;
  final bool isToday;
  final bool select;
  final Function(RecordAbsen, int, int) onTap;

  StatusAbsen(
      {this.status = 3,
      this.userId,
      this.data,
      this.onTap,
      // this.absenId,
      this.courseId,
      this.scheduleId,
      this.exist = false,
      this.isToday = false,
      this.select = false});
  @override
  Widget build(BuildContext context) {
    // var _editAbsenState = Injector.getAsReactive<EditAbsensiState>();
    if (exist) {
      return Container(
        height: 25,
        width: 25,
        decoration: BoxDecoration(
            border: Border.all(color: getColorStatus(status)),
            shape: BoxShape.rectangle,
            borderRadius: radius(4),
            color: getColorStatus(status)),
        child: InkWell(
          onTap: () => onTap(data, userId, status),
          child: Center(
            child: Text(
              getStatusName(status),
              style: boldWhite.copyWith(
                  fontFamily: 'RockWell',
                  fontSize: S.w / 32,
                  color: Colors.white),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  String getStatusName(int status) {
    if (status == 1) {
      return 'H';
    } else if (status == 2) {
      return '1/2';
    } else if (status == 3) {
      return 'A';
    } else if (status == 4) {
      return 'I';
    } else {
      return 'S';
    }
  }

  Color getColorStatus(int status) {
    if (status == 1) {
      // return SiswamediaTheme.green;
      return Color(0xFF9AC163);
    } else if (status == 2) {
      // return Color(0xffEFDE14);
      return Color(0xFFEFC553);
    } else if (status == 4) {
      // return Color(0xffD51717);
      return Color(0xFFEFE353);
    } else if (status == 5) {
      return Color(0xFF9DD1F2);
    } else {
      return Color(0xFFF29D9D);
    }
  }
}

class StatusAbsenEdit extends StatelessWidget {
  final int status;
  StatusAbsenEdit({this.status = 3});
  @override
  Widget build(BuildContext context) {
    return CustomContainer(
      height: 25,
      width: 25,
      shape: BoxShape.rectangle,
      radius: radius(8),
      color: getColorStatus(status),
      child: Center(
        child: Text(
          getStatusName(status),
          style: boldWhite.copyWith(fontSize: S.w / 32),
        ),
      ),
    );
  }

  String getStatusName(int status) {
    if (status == 1) {
      return 'P';
    } else if (status == 2) {
      return 'H';
    } else {
      return 'A';
    }
  }

  Color getColorStatus(int status) {
    if (status == 1) {
      return SiswamediaTheme.green;
    } else if (status == 2) {
      return Color(0xffEFDE14);
    } else if (status == 3) {
      return Color(0xffD51717);
    } else if (status == 4) {
      //todo izin
      return SiswamediaTheme.blue;
    } else {
      //todo sakit
      return SiswamediaTheme.blue;
    }
  }
}

class HeaderInfoEditAbsen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showDialog<void>(
            context: context,
            builder: (_) {
              return Dialog(
                shape: RoundedRectangleBorder(borderRadius: radius(12)),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .07, vertical: S.w * .06),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CustomText('Petunjuk Kehadiran', Kind.heading3),
                      SizedBox(height: 20),
                      GridView.count(
                        crossAxisCount: 2,
                        shrinkWrap: true,
                        childAspectRatio: 3,
                        children: [1, 2, 4, 5, 3]
                            .map((e) => Row(
                                  children: [
                                    StatusAbsen(status: e, exist: true),
                                    SizedBox(width: 20),
                                    Expanded(
                                      child: Text(getStatusName(e),
                                          style: semiBlack.copyWith(
                                              fontSize: S.w / 30)),
                                    )
                                  ],
                                ))
                            .toList(),
                      )
                    ],
                  ),
                ),
              );
            });
      },
      child: Container(
          color: Colors.grey[200],
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: S.w * .05),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(Icons.info),
              SizedBox(width: 20),
              CustomText('Petunjuk Kehadiran', Kind.heading3),
            ],
          )
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //   children: [1, 2, 3]
          //       .map((e) => Row(
          //             children: [
          //               StatusAbsen(status: e, exist: true),
          //               Text(' = ${getStatusName(e)}')
          //             ],
          //           ))
          //       .toList(),
          // ),
          ),
    );
  }

  String getStatusName(int status) {
    if (status == 1) {
      return 'Hadir';
    } else if (status == 2) {
      return 'Setengah Hari';
    } else if (status == 3) {
      return 'Absen';
    } else if (status == 4) {
      //todo izin
      return 'Izin';
    } else {
      return 'Sakit';
      //todo sakit
    }
  }
}

class HeaderName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      width: 160,
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colors.grey[300],
            offset: Offset(0, -2),
            spreadRadius: 2,
            blurRadius: 2)
      ]),
      child: Center(
          child: Text(
        'Nama',
        style: boldBlack.copyWith(fontSize: S.w / 28),
      )),
    );
  }
}

class HeaderKehadiran extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colors.grey[300],
            offset: Offset(0, -2),
            spreadRadius: 2,
            blurRadius: 2)
      ]),
      child: Center(
          child: Text(
        'Kehadiran',
        style: boldBlack.copyWith(fontSize: S.w / 28),
      )),
    );
  }
}
