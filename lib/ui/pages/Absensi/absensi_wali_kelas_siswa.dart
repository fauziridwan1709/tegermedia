part of '_absensi.dart';

class WaliKelasSiswa extends StatelessWidget {
  const WaliKelasSiswa({this.title, this.onChange, this.value});
  final String title;
  final Function(String s) onChange;
  final String value;

  @override
  Widget build(BuildContext context) {
    var auth = Injector.getAsReactive<AuthState>().state;
    var absensiState = Injector.getAsReactive<AbsensiState>();
    String value = 'Januari';
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          title: TitleAppbar(label: title),
          actions: [
            IconButton(
              color: SiswamediaTheme.green,
              icon: Icon(Icons.download_rounded),
              onPressed: () => showDialog<void>(
                  context: context,
                  builder: (_) => StatefulBuilder(builder:
                          (__, void Function(void Function()) setState) {
                        // String value = 'Januari';
                        return Dialog(
                          shape:
                              RoundedRectangleBorder(borderRadius: radius(12)),
                          child: SizedBox(
                            width: S.w * .7,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: S.w * .06, horizontal: S.w * .05),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  CustomText('Pilih Bulan', Kind.heading3),
                                  SizedBox(height: 30),
                                  Container(
                                    width: S.w * .6,
                                    height: S.w * .1,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: S.w * .05),
                                    decoration:
                                        SiswamediaTheme.dropDownDecoration,
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                          icon: Icon(
                                            Icons.arrow_drop_down,
                                            color: SiswamediaTheme.green,
                                          ),
                                          hint: Text('Bulan'),
                                          value: value,
                                          onChanged: (s) =>
                                              setState(() => value = s),
                                          style: descBlack.copyWith(
                                              fontSize: S.w / 30),
                                          items: bulanFull
                                              .map((e) => DropdownMenuItem(
                                                    value: e,
                                                    child: Text(e),
                                                  ))
                                              .toList()),
                                    ),
                                  ),
                                  SizedBox(height: 15),
                                  CustomContainer(
                                    color: SiswamediaTheme.green,
                                    radius: radius(S.w),
                                    width: S.w * .6,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 6),
                                    onTap: () => absensiState.state
                                        .exportToExcel(
                                            context,
                                            bulanFull.indexOf(value) + 1,
                                            title),
                                    child: Center(
                                        child: Text('export',
                                            style: semiWhite.copyWith(
                                                fontSize: S.w / 28))),
                                  ),
                                  SizedBox(height: 15),
                                ],
                              ),
                            ),
                          ),
                        );
                      })),
            )
          ],
        ),
        body: FutureBuilder<StudentsClassroomModel>(
          future:
              ClassServices.getStudentClassRoom(id: auth.currentState.classId),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.data.isEmpty) {
                return Center(
                    child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/dialog/sorry_no_student.png',
                          height: S.w * .6, width: S.w * .6),
                      SizedBox(height: 10),
                      CustomText('Belum ada siswa dikelasmu', Kind.descBlack),
                      SizedBox(height: 50 + AppBar().preferredSize.height),
                    ],
                  ),
                ));
              }
              return ListView(
                shrinkWrap: true,
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .01),
                children: snapshot.data.data
                    .map((e) => InkWell(
                          onTap: () => navigate(
                              context, SiswaWaliKelas(userId: e.userId)),
                          child: Container(
                              margin:
                                  EdgeInsets.symmetric(vertical: S.w * .025),
                              padding: EdgeInsets.symmetric(
                                  horizontal: S.w * .05, vertical: S.w * .02),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: radius(12),
                                  boxShadow: [SiswamediaTheme.shadowContainer]),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Text(e.fullName),
                                    ),
                                    Icon(Icons.arrow_forward_ios_outlined,
                                        color: SiswamediaTheme.green),
                                  ])),
                        ))
                    .toList(),
              );
            }
            return Center(child: CircularProgressIndicator());
          },
        ));
  }
}
