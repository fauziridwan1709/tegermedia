part of '../_absensi.dart';

class DetailIzinScreen extends StatefulWidget {
  final DataStudentPermit data;

  const DetailIzinScreen({Key key, this.data}) : super(key: key);

  @override
  _DetailIzinScreenState createState() => _DetailIzinScreenState();
}

class _DetailIzinScreenState extends State<DetailIzinScreen> {
  final authState = GlobalState.auth();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: vText("Permohonan Izin"),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.primary,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          margin: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: vText("Permohonan Izin Absensi",
                      fontSize: 14, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 2),
                ),
                Container(
                  child: vText("${formattedDate(widget.data.createdAt)}",
                      fontSize: 10, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: '${widget.data.parentName}',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.black87)),
                      TextSpan(
                          text: ' sebagai Wali dari ',
                          style: TextStyle(
                            fontSize: 12,
                            color: SiswamediaTheme.textGrey[260],
                          )),
                      TextSpan(
                          text: '${widget.data.studentName}',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.black87)),
                      TextSpan(
                          text: ' mengajukan permohonan izin untuk :',
                          style: TextStyle(
                            fontSize: 12,
                            color: SiswamediaTheme.textGrey[260],
                          )),
                    ],
                  ),
                ),
                Container(
                  child: vText("Waktu :",
                      fontSize: 14, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16, top: 16),
                ),
                Container(
                  child: vText("${formattedDate(widget.data.from)}",
                      fontSize: 12, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: vText(
                    "Hingga",
                    fontSize: 14,
                    color: SiswamediaTheme.textGrey[260],
                  ),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: vText("${formattedDate(widget.data.to)}",
                      fontSize: 12, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: vText(
                    "Lamanya hari",
                    color: SiswamediaTheme.textGrey[260],
                    fontSize: 14,
                  ),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: vText("${widget.data.dayDuration} Hari",
                      fontSize: 12, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child:
                      vText("Pesan", fontSize: 12, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: vText(
                    widget.data.message,
                    fontSize: 12,
                  ),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: vText("Response / Jawaban",
                      fontSize: 12, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Container(
                  child: vText(
                    widget.data.respondMessage,
                    fontSize: 12,
                  ),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                SizedBox(height: 16),
                Container(
                  child: vText("Lampiran:",
                      fontSize: 12, fontWeight: FontWeight.bold),
                  margin: EdgeInsets.only(bottom: 16),
                ),
                Column(
                  children: widget.data.attachments
                      .map((e) => InkWell(
                            onTap: () => launch(e.link),
                            child: vText(e.name,
                                fontSize: 12, color: SiswamediaTheme.green),
                          ))
                      .toList(),
                ),
                SizedBox(height: 32),
              ],
            ),
          ),
        ));
  }
}
