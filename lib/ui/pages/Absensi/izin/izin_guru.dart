part of '../_absensi.dart';

class IzinGuru extends StatefulWidget {
  @override
  _IzinGuruState createState() => _IzinGuruState();
}

class _IzinGuruState extends State<IzinGuru>
    with SingleTickerProviderStateMixin {
  String _choose1;

  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  final absensiState = Injector.getAsReactive<AbsensiState>();
  // String strDate = "";
  // TextEditingController description = TextEditingController();
  // String endDate = "";
  //
  // List<MainAttachments> dataImage = [];
  //
  // double _progressValue = 0;
  // int _progressPercentValue = 0;

  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;

  Animation<double> animation;
  AnimationController controller;
  final authState = GlobalState.auth();

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    controller =
        AnimationController(duration: Duration(milliseconds: 700), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeInOut));
    controller.forward();
    Initializer(
      reactiveModel: absensiState,
      cacheKey: 'time_list_absen',
      state: absensiState.state.model != null,
      rIndicator: _refreshIndicatorKey,
    )..initialize();
  }

  Future<void> retrieveData() async {
    await absensiState.setState((s) => s.getIzin());
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          shadowColor: Colors.transparent,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Permohonan Izin',
              style: TextStyle(color: Colors.black, fontSize: S.w / 22)),
          actions: [
            IconButton(
              icon: Icon(Icons.add, color: SiswamediaTheme.green),
              onPressed: () => context.push<void>(CreateIzin()),
            )
          ],
        ),
        body: StateBuilder<AbsensiState>(
            observe: () => absensiState,
            builder: (context, constrains) {
              return RefreshIndicator(
                  onRefresh: retrieveData,
                  key: _refreshIndicatorKey,
                  child: WhenRebuilder<AbsensiState>(
                    observe: () => absensiState,
                    onWaiting: () => WaitingView(),
                    onIdle: () => WaitingView(),
                    onError: (dynamic error) => ErrorView(error: error),
                    onData: (dataSnapshot) {
                      if (dataSnapshot.dataIzin.isEmpty) {
                        return ListView(
                          children: [
                            if (MediaQuery.of(context).orientation ==
                                Orientation.portrait)
                              SizedBox(height: width * .6),
                            Image.asset(
                              'assets/images/png/izin_kosong.png',
                              width: width * .4,
                              height: width * .3,
                            ),
                            // SVGImage(
                            //   'izin_kosong.svg',
                            //   width: width * 1,
                            //   height: width * .65,
                            // ),
                            SizedBox(height: 20),
                            Text(
                              'Masih Kosong',
                              style: semiBlack.copyWith(
                                  fontSize: 12, color: Colors.grey),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 40),
                          ],
                        );
                      }
                      return ListView.builder(
                          itemCount: dataSnapshot.dataIzin.length,
                          itemBuilder: (c, i) =>
                              body(dataSnapshot.dataIzin[i]));
                    },
                  ));
            }));
  }

  Widget body(DataStudentPermit data) {
    return InkWell(
      onTap: () {
        Navigator.push<void>(
          context,
          MaterialPageRoute(
              builder: (context) => DetailIzinScreen(
                    data: data,
                  )),
        );
      },
      child: Container(
        width: S.w * .9,
        height: S.w * .4,
        margin:
            EdgeInsets.symmetric(vertical: S.w * .025, horizontal: S.w * .05),
        padding:
            EdgeInsets.symmetric(vertical: S.w * .025, horizontal: S.w * .05),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                  offset: Offset(2, 2),
                  spreadRadius: 2,
                  blurRadius: 2,
                  color: Colors.grey.withOpacity(.2))
            ]),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                data.studentName,
                style: TextStyle(
                  fontSize: S.w / 25,
                  fontWeight: FontWeight.w600,
                  color: Colors.black87,
                ),
              ),
              RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'tgl. permohonan : ',
                        style: TextStyle(
                            fontSize: S.w / 28, color: Colors.grey[600])),
                    TextSpan(
                        text: formattedDate(data.createdAt).toString(),
                        style: TextStyle(
                            fontSize: S.w / 32,
                            fontWeight: FontWeight.w600,
                            color: Colors.black87)),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'tgl. Izin : ',
                        style: TextStyle(
                            fontSize: S.w / 28, color: Colors.grey[600])),
                    TextSpan(
                        text:
                            '${formattedDate(data.from)} - ${formattedDate(data.to)}',
                        style: TextStyle(
                            fontSize: S.w / 32,
                            fontWeight: FontWeight.w600,
                            color: Colors.black87)),
                  ],
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Status : ',
                              style: TextStyle(
                                  fontSize: S.w / 28, color: Colors.grey[600])),
                          TextSpan(
                              text: data.status == 0
                                  ? 'Menunggu Persetujuan'
                                  : data.status == 1
                                      ? 'Izin Diterima'
                                      : 'Izin Ditolak',
                              style: TextStyle(
                                  fontSize: S.w / 32,
                                  fontWeight: FontWeight.w600,
                                  color: data.status == 0
                                      ? SiswamediaTheme.textBlue
                                      : data.status == 1
                                          ? SiswamediaTheme.green
                                          : SiswamediaTheme.textError)),
                        ],
                      ),
                    ),
                  ),
                  if (data.status == 0)
                    IconButton(
                      icon: Icon(Icons.delete_outlined),
                      onPressed: () async {
                        var deleted = await absensiState.state
                            .deleteIzin(data.studentPermitId.toString());

                        if (deleted.statusCode == 200) {
                          _refreshIndicatorKey.currentState.show();
                        } else {
                          CustomFlushBar.errorFlushBar(
                            deleted.message,
                            context,
                          );
                        }
                      },
                    )
                ],
              ),
            ]),
      ),
    );
  }

  // void callback() {
  //   var start = DateTime.now();
  //   var end = DateTime.now();
  //
  //   showModalBottomSheet<Widget>(
  //       context: context,
  //       isScrollControlled: true,
  //       shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.only(
  //               topLeft: const Radius.circular(20.0),
  //               topRight: const Radius.circular(20.0))),
  //       builder: (context) => StatefulBuilder(
  //             builder: (BuildContext context,
  //                     void Function(void Function()) setState) =>
  //                 Wrap(
  //               children: [
  //                 Container(
  //                   height: S.h * .9,
  //                   padding: EdgeInsets.symmetric(
  //                       horizontal: S.w * .05, vertical: S.w * .05),
  //                   decoration: BoxDecoration(
  //                       color: Colors.white,
  //                       borderRadius: BorderRadius.only(
  //                           topLeft: const Radius.circular(20.0),
  //                           topRight: const Radius.circular(20.0))),
  //                   child: Column(
  //                     crossAxisAlignment: CrossAxisAlignment.start,
  //                     children: [
  //                       Container(
  //                         child: Row(
  //                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                             children: [
  //                               Expanded(
  //                                 child: IconButton(
  //                                   icon: Icon(
  //                                     Icons.clear,
  //                                     color: SiswamediaTheme.primary,
  //                                   ),
  //                                   alignment: Alignment.centerLeft,
  //                                   iconSize: 22,
  //                                   padding: EdgeInsets.all(0),
  //                                   onPressed: () => Navigator.pop(context),
  //                                 ),
  //                               ),
  //                               vText('Buat Surat Izin',
  //                                   fontWeight: FontWeight.w500,
  //                                   color: SiswamediaTheme.textGrey[500],
  //                                   fontSize: 16),
  //                               Expanded(child: Container()),
  //                             ]),
  //                         height: 20,
  //                         margin: EdgeInsets.only(top: 20),
  //                       ),
  //                       SizedBox(
  //                         height: 10,
  //                       ),
  //                       Container(
  //                         color: Colors.grey,
  //                         height: 1,
  //                         width: S.w,
  //                       ),
  //                       SizedBox(
  //                         height: 20,
  //                       ),
  //                       Text('Tentukkan Waktu',
  //                           style: TextStyle(
  //                               color: Colors.black87,
  //                               fontSize: S.w / 23,
  //                               fontWeight: FontWeight.w600)),
  //                       SizedBox(
  //                         height: 16,
  //                       ),
  //                       _date('Untuk Tanggal', (value) {
  //                         setState(() {
  //                           strDate = value;
  //                           print(value);
  //                         });
  //                       }),
  //                       // SizedBox(
  //                       //   height: 8,
  //                       // ),
  //                       // Divider(color: Colors.grey),
  //                       SizedBox(
  //                         height: 16,
  //                       ),
  //                       _date('Hingga Tanggal', (value) {
  //                         endDate = value;
  //                         print(value);
  //                         setState(() {});
  //                       }),
  //                       SizedBox(
  //                         height: 8,
  //                       ),
  //                       // Divider(color: Colors.grey),
  //                       // SizedBox(
  //                       //   height: 16,
  //                       // ),
  //                       Composer(
  //                         description: description,
  //                       ),
  //                       Container(
  //                         child: Column(
  //                             children: List.generate(
  //                                 dataImage.length,
  //                                 (index) => GestureDetector(
  //                                     onTap: () async {
  //                                       var fileName = dataImage[index].file;
  //                                       if (await canLaunch(fileName)) {
  //                                         await launch(fileName);
  //                                       } else {
  //                                         throw 'Could not launch $fileName';
  //                                       }
  //                                     },
  //                                     child: Container(
  //                                         width: S.w,
  //                                         margin: EdgeInsets.symmetric(
  //                                             // horizontal: width * 0.05,
  //                                             vertical: 3),
  //                                         padding: EdgeInsets.symmetric(
  //                                             horizontal: 10, vertical: 5),
  //                                         decoration: BoxDecoration(
  //                                             border: Border.all(
  //                                                 color: Colors.grey[300]),
  //                                             borderRadius:
  //                                                 BorderRadius.circular(5),
  //                                             boxShadow: <BoxShadow>[
  //                                               BoxShadow(
  //                                                   color: SiswamediaTheme.grey
  //                                                       .withOpacity(0.2),
  //                                                   offset:
  //                                                       const Offset(0.2, 0.2),
  //                                                   blurRadius: 3.0),
  //                                             ],
  //                                             color: Colors.white),
  //                                         child: Row(
  //                                             crossAxisAlignment:
  //                                                 CrossAxisAlignment.center,
  //                                             children: [
  //                                               Expanded(
  //                                                   child: Text(dataImage[index]
  //                                                       .fileName)),
  //                                               Spacer(),
  //                                               Text(
  //                                                 'Lihat',
  //                                                 style: TextStyle(
  //                                                     color: SiswamediaTheme
  //                                                         .green),
  //                                               ),
  //                                               GestureDetector(
  //                                                 onTap: () {
  //                                                   if (dataImage.length == 1) {
  //                                                     return CustomFlushBar
  //                                                         .errorFlushBar(
  //                                                             'Tidak bisa menghapus atachment, minimal harus ada sebuah attachment',
  //                                                             context);
  //                                                   }
  //                                                   removeAttachments(index);
  //                                                 },
  //                                                 child: Text(
  //                                                   'Hapus',
  //                                                   style: TextStyle(
  //                                                       color: Colors.red),
  //                                                 ),
  //                                               )
  //                                             ])))).toList()),
  //                       ),
  //                       SizedBox(height: 20),
  //                       Container(
  //                         child: InkWell(
  //                           onTap: () async {
  //                             await uploadFile(_setUploadProgress, context);
  //                           },
  //                           child: Row(
  //                               crossAxisAlignment: CrossAxisAlignment.center,
  //                               children: [
  //                                 Container(
  //                                   height: 35,
  //                                   width: 35,
  //                                   margin: EdgeInsets.only(
  //                                       right: 10, top: 10, bottom: 10),
  //                                   decoration: BoxDecoration(
  //                                       color: SiswamediaTheme.white,
  //                                       borderRadius: BorderRadius.circular(5),
  //                                       border: Border.all(
  //                                           color: SiswamediaTheme.green)),
  //                                   child: Icon(Icons.add,
  //                                       color: SiswamediaTheme.green),
  //                                 ),
  //                                 Text(
  //                                   'Unggah File ',
  //                                   style: mediumBlack.copyWith(
  //                                       color: SiswamediaTheme.green,
  //                                       fontSize: S.w / 30),
  //                                 )
  //                               ]),
  //                         ),
  //                       ),
  //                       SizedBox(height: 20),
  //                       if (progressUploadState.state.progress != 0)
  //                         Container(
  //                             width: S.w,
  //                             margin: EdgeInsets.symmetric(
  //                                 // horizontal: width * 0.05,
  //                                 vertical: 3),
  //                             padding: EdgeInsets.symmetric(
  //                                 horizontal: 10, vertical: 5),
  //                             decoration: BoxDecoration(
  //                                 border: Border.all(color: Colors.grey[300]),
  //                                 borderRadius: BorderRadius.circular(5),
  //                                 boxShadow: <BoxShadow>[
  //                                   BoxShadow(
  //                                       color: SiswamediaTheme.grey
  //                                           .withOpacity(0.2),
  //                                       offset: const Offset(0.2, 0.2),
  //                                       blurRadius: 3.0),
  //                                 ],
  //                                 color: Colors.white),
  //                             child: Column(
  //                                 crossAxisAlignment: CrossAxisAlignment.start,
  //                                 children: [
  //                                   Text(
  //                                       'Uploading... $_progressPercentValue%'),
  //                                   SizedBox(height: 5),
  //                                   LinearPercentIndicator(
  //                                     width: S.w - 35 * 2,
  //                                     lineHeight: 8.0,
  //                                     percent: _progressValue,
  //                                     backgroundColor: Colors.grey[300],
  //                                     progressColor: SiswamediaTheme.green,
  //                                   ),
  //                                   SizedBox(height: 5),
  //                                   Container(
  //                                     width: S.w * .8,
  //                                     child: Text(
  //                                       '${Format.formatByte(progressUploadState.state.progress)}/${Format.formatByte(progressUploadState.state.contentLength)}',
  //                                       textAlign: TextAlign.right,
  //                                     ),
  //                                   ),
  //                                 ])),
  //                       Expanded(
  //                         child: Container(),
  //                       ),
  //                       InkWell(
  //                         onTap: () {
  //                           if (strDate == "" || endDate == "") {
  //                             CustomFlushBar.errorFlushBar(
  //                               'Mohon mengisi semua data',
  //                               context,
  //                             );
  //                           } else {
  //                             showDialog<void>(
  //                                 context: context,
  //                                 builder: (context) => StatefulBuilder(builder:
  //                                         (BuildContext context,
  //                                             void Function(void Function())
  //                                                 setState) {
  //                                       // String role = someCapitalizedString(data.role);
  //                                       return Dialog(
  //                                           shape: RoundedRectangleBorder(
  //                                               borderRadius:
  //                                                   BorderRadius.circular(12)),
  //                                           child: Container(
  //                                               width: S.w * .5,
  //                                               padding: EdgeInsets.symmetric(
  //                                                   vertical: S.w * .05,
  //                                                   horizontal: S.w * .05),
  //                                               child: Column(
  //                                                 mainAxisSize:
  //                                                     MainAxisSize.min,
  //                                                 mainAxisAlignment:
  //                                                     MainAxisAlignment.center,
  //                                                 children: [
  //                                                   SizedBox(
  //                                                     height: 15,
  //                                                   ),
  //                                                   Text("Kirim Surat Izin",
  //                                                       style: TextStyle(
  //                                                           fontSize: 18,
  //                                                           fontWeight:
  //                                                               FontWeight
  //                                                                   .w800)),
  //                                                   SizedBox(
  //                                                     height: 15,
  //                                                   ),
  //                                                   Text(
  //                                                       'Apakah Anda sudah yakin dengan data yang Anda masukkan sudah benar ?',
  //                                                       textAlign:
  //                                                           TextAlign.center,
  //                                                       style: TextStyle(
  //                                                           fontSize: 12)),
  //                                                   SizedBox(
  //                                                     height: 16,
  //                                                   ),
  //                                                   Row(
  //                                                     children: [
  //                                                       buttonIzin(
  //                                                           "Yakin",
  //                                                           SiswamediaTheme
  //                                                               .primary,
  //                                                           () async {
  //                                                         var dateStart =
  //                                                             DateTime.parse(
  //                                                                     strDate)
  //                                                                 .toUtc();
  //                                                         var dateEnd =
  //                                                             DateTime.parse(
  //                                                                     endDate)
  //                                                                 .toUtc();
  //                                                         var attach = <
  //                                                             Map<String,
  //                                                                 dynamic>>[];
  //                                                         dataImage.forEach(
  //                                                             (element) {
  //                                                           attach.add(<String,
  //                                                               dynamic>{
  //                                                             'name': element
  //                                                                 .fileName,
  //                                                             'link':
  //                                                                 element.file
  //                                                           });
  //                                                         });
  //                                                         var dataMap =
  //                                                             <String, dynamic>{
  //                                                           "message":
  //                                                               description
  //                                                                   .text,
  //                                                           "from": dateStart
  //                                                               .toIso8601String(),
  //                                                           "to": dateEnd
  //                                                               .toIso8601String(),
  //                                                           "class_id": authState
  //                                                               .state
  //                                                               .currentState
  //                                                               .classId,
  //                                                           "attachments":
  //                                                               attach
  //                                                         };
  //
  //                                                         var result =
  //                                                             await CreateAbsensi
  //                                                                 .createIzin(
  //                                                                     body:
  //                                                                         dataMap);
  //                                                         print(dataMap
  //                                                             .toString());
  //                                                         result;
  //                                                         if (result.statusCode
  //                                                             .isSuccessOrCreated) {
  //                                                           _refreshIndicatorKey
  //                                                               .currentState
  //                                                               .show();
  //
  //                                                           Navigator.pop(
  //                                                               context);
  //                                                           Navigator.pop(
  //                                                               context);
  //                                                         } else {
  //                                                           CustomFlushBar
  //                                                               .errorFlushBar(
  //                                                             result.message,
  //                                                             context,
  //                                                           );
  //                                                         }
  //                                                       }),
  //                                                       buttonIzin(
  //                                                           "Priksa Lagi",
  //                                                           SiswamediaTheme
  //                                                               .greybgButton,
  //                                                           () {
  //                                                         Navigator.pop(
  //                                                             context);
  //                                                       }),
  //                                                     ],
  //                                                   )
  //                                                 ],
  //                                               )));
  //                                     }));
  //                           }
  //                         },
  //                         child: Container(
  //                           width: double.infinity,
  //                           decoration: BoxDecoration(
  //                               color: SiswamediaTheme.primary,
  //                               borderRadius: BorderRadius.circular(8)),
  //                           margin: EdgeInsets.all(21),
  //                           alignment: Alignment.center,
  //                           padding: EdgeInsets.all(11),
  //                           child: vText("Kirim Surat Izin",
  //                               color: Colors.white,
  //                               fontWeight: FontWeight.bold),
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ));
  // }

  Widget buttonIzin(String title, Color color, Function onTap) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          child: vText(title, color: Colors.white),
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(8)),
          alignment: Alignment.center,
          padding: EdgeInsets.all(11),
          margin: EdgeInsets.all(4),
        ),
      ),
    );
  }
}
