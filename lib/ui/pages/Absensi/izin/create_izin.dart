part of '../_absensi.dart';

class CreateIzin extends StatefulWidget {
  @override
  _CreateIzinState createState() => _CreateIzinState();
}

class _CreateIzinState extends State<CreateIzin> {
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  final absensiState = Injector.getAsReactive<AbsensiState>();
  final authState = GlobalState.auth();
  String strDate = '';
  TextEditingController description = TextEditingController();
  String endDate = '';

  List<MainAttachments> dataImage = [];

  double _progressValue = 0;
  int _progressPercentValue = 0;

  void setAttachments(DataFile file) {
    dataImage.add(MainAttachments(file: file.file, fileName: file.fileName));
    setState(() {});
  }

  void removeAttachments(int index) {
    dataImage.removeAt(index);
    setState(() {
      dataImage = dataImage;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 20,
                margin: EdgeInsets.only(top: 20),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: IconButton(
                          icon: Icon(
                            Icons.clear,
                            color: SiswamediaTheme.primary,
                          ),
                          alignment: Alignment.centerLeft,
                          iconSize: 22,
                          padding: EdgeInsets.all(0),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ),
                      vText('Buat Surat Izin',
                          fontWeight: FontWeight.w500,
                          color: SiswamediaTheme.textGrey[500],
                          fontSize: 16),
                      Expanded(child: Container()),
                    ]),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                color: Colors.grey,
                height: 1,
                width: S.w,
              ),
              SizedBox(
                height: 20,
              ),
              Text('Tentukkan Waktu',
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: S.w / 23,
                      fontWeight: FontWeight.w600)),
              SizedBox(
                height: 16,
              ),
              _date('Untuk Tanggal', (value) {
                setState(() {
                  strDate = value;
                  print(value);
                });
              }),
              // SizedBox(
              //   height: 8,
              // ),
              // Divider(color: Colors.grey),
              SizedBox(
                height: 16,
              ),
              _date('Hingga Tanggal', (value) {
                if(strDate== value){
                  CustomFlushBar.errorFlushBar("Silahkan pilih Jam berbeda atau tanggal yang berbeda", context);
                }else{
                endDate = value;}
                print(value);
                setState(() {});
              }),
              SizedBox(
                height: 8,
              ),
              // Divider(color: Colors.grey),
              // SizedBox(
              //   height: 16,
              // ),
              Composer(
                description: description,
              ),
              Container(
                child: Column(
                    children: List.generate(
                        dataImage.length,
                        (index) => GestureDetector(
                            onTap: () async {
                              // int data = 0;
                              var fileName = dataImage[index].file;
                              if (await canLaunch(fileName)) {
                                await launch(fileName);
                              } else {
                                throw 'Could not launch $fileName';
                              }
                            },
                            child: Container(
                                width: S.w,
                                margin: EdgeInsets.symmetric(
                                    // horizontal: width * 0.05,
                                    vertical: 3),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey[300]),
                                    borderRadius: BorderRadius.circular(5),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: SiswamediaTheme.grey
                                              .withOpacity(0.2),
                                          offset: const Offset(0.2, 0.2),
                                          blurRadius: 3.0),
                                    ],
                                    color: Colors.white),
                                child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                          child:
                                              Text(dataImage[index].fileName)),
                                      Spacer(),
                                      Text(
                                        'Lihat',
                                        style: TextStyle(
                                            color: SiswamediaTheme.green),
                                      ),
                                      SizedBox(width: 10),
                                      GestureDetector(
                                        onTap: () {
                                          // if (dataImage.length == 1) {
                                          //   return CustomFlushBar.errorFlushBar(
                                          //       'Tidak bisa menghapus atachment, minimal harus ada sebuah attachment',
                                          //       context);
                                          // }
                                          removeAttachments(index);
                                        },
                                        child: Text(
                                          'Hapus',
                                          style: TextStyle(color: Colors.red),
                                        ),
                                      )
                                    ])))).toList()),
              ),
              SizedBox(height: 20),
              Container(
                child: InkWell(
                  onTap: () async {
                    await uploadFile(_setUploadProgress, context);
                  },
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 35,
                          width: 35,
                          margin:
                              EdgeInsets.only(right: 10, top: 10, bottom: 10),
                          decoration: BoxDecoration(
                              color: SiswamediaTheme.white,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: SiswamediaTheme.green)),
                          child: Icon(Icons.add, color: SiswamediaTheme.green),
                        ),
                        Text(
                          'Unggah File ',
                          style: mediumBlack.copyWith(
                              color: SiswamediaTheme.green, fontSize: S.w / 30),
                        )
                      ]),
                ),
              ),
              SizedBox(height: 20),
              if (progressUploadState.state.progress != 0)
                Container(
                    width: S.w,
                    margin: EdgeInsets.symmetric(
                        // horizontal: width * 0.05,
                        vertical: 3),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: SiswamediaTheme.grey.withOpacity(0.2),
                              offset: const Offset(0.2, 0.2),
                              blurRadius: 3.0),
                        ],
                        color: Colors.white),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Uploading... $_progressPercentValue%'),
                          SizedBox(height: 5),
                          LinearPercentIndicator(
                            width: S.w - 35 * 2,
                            lineHeight: 8.0,
                            percent: _progressValue,
                            backgroundColor: Colors.grey[300],
                            progressColor: SiswamediaTheme.green,
                          ),
                          SizedBox(height: 5),
                          Container(
                            width: S.w * .8,
                            child: Text(
                              '${Format.formatByte(progressUploadState.state.progress)}/${Format.formatByte(progressUploadState.state.contentLength)}',
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ])),
              Expanded(
                child: Container(),
              ),
              InkWell(
                onTap: () {
                  if (strDate == '' ||
                      endDate == '' ||
                      description.text == '') {
                    CustomFlushBar.errorFlushBar(
                      'Mohon mengisi semua data',
                      context,
                    );
                  } else {
                    showDialog<void>(
                        context: context,
                        builder: (context) => StatefulBuilder(builder:
                                (BuildContext context,
                                    void Function(void Function()) setState) {
                              // String role = someCapitalizedString(data.role);
                              return Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12)),
                                  child: Container(
                                      width: S.w * .5,
                                      padding: EdgeInsets.symmetric(
                                          vertical: S.w * .05,
                                          horizontal: S.w * .05),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Text('Kirim Surat Izin',
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w800)),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Text(
                                              'Apakah Anda sudah yakin dengan data yang Anda masukkan sudah benar ?',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontSize: 12)),
                                          SizedBox(
                                            height: 16,
                                          ),
                                          Row(
                                            children: [
                                              buttonIzin('Yakin',
                                                  SiswamediaTheme.primary,
                                                  () async {
                                                var dateStart =
                                                    DateTime.parse(strDate)
                                                        .toUtc();
                                                var dateEnd =
                                                    DateTime.parse(endDate)
                                                        .toUtc();
                                                var attach =
                                                    <Map<String, dynamic>>[];
                                                // for()
                                                for (var data in dataImage) {
                                                  attach.add(<String, dynamic>{
                                                    'name': data.fileName,
                                                    'link': data.file
                                                  });
                                                }
                                                var dataMap = <String, dynamic>{
                                                  'message': description.text,
                                                  'from': dateStart
                                                      .toIso8601String(),
                                                  'to':
                                                      dateEnd.toIso8601String(),
                                                  'class_id': authState.state
                                                      .currentState.classId,
                                                  'attachments': attach
                                                };

                                                var result = await CreateAbsensi
                                                    .createIzin(body: dataMap);
                                                print(dataMap.toString());
                                                result;
                                                if (result.statusCode
                                                    .isSuccessOrCreated) {
                                                  await absensiState.setState(
                                                      (s) => s.getIzin());

                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                } else {
                                                  CustomFlushBar.errorFlushBar(
                                                    result.message,
                                                    context,
                                                  );
                                                }
                                              }),
                                              buttonIzin('Periksa Lagi',
                                                  SiswamediaTheme.greybgButton,
                                                  () {
                                                Navigator.pop(context);
                                              }),
                                            ],
                                          )
                                        ],
                                      )));
                            }));
                  }
                },
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: SiswamediaTheme.primary,
                      borderRadius: BorderRadius.circular(8)),
                  margin: EdgeInsets.all(21),
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(11),
                  child: vText('Kirim Surat Izin',
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _date(String title, ValueChanged<String> valueChanged) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Text(title),
          // SizedBox(
          //   width: 8,
          // ),
          Expanded(
            child: DateTimePicker(
              fieldHintText: title,
              dateHintText: title,
              type: DateTimePickerType.dateTime,
              dateMask: 'dd MMM yyyy - HH:mm',
              // decoration: InputDecoration(
              //     border: InputBorder.none, focusedBorder: InputBorder.none),
              firstDate: DateTime.now(),
              lastDate: DateTime(2100),
              dateLabelText: title,
              fieldLabelText: title,
              onChanged: (val) {
                setState(() {
                  valueChanged(val);
                  print(val);
                });
              },
              // decoration: InputDecoration(
              //   border: InputBorder.none,
              // ),
            ),
          ),
        ]);
  }

  static HttpClient getHttpClient() {
    var httpClient = HttpClient()
      ..connectionTimeout = const Duration(seconds: 10)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

    return httpClient;
  }

  Future<void> uploadFile(
      OnUploadProgressCallback onUploadProgress, BuildContext context) async {
    try {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context,
          builder: (_) {
            return Center(child: CircularProgressIndicator());
          });
      var pick = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.any,
        // allowedExtensions: ['jpg', 'pdf', 'doc']
      );
      print('aduh');
      var file = pick.files.first;
      print(file.name);
      print(file.path);

      var client2 = Client();
      //todo file!=null
      if (file != null) {
        var totalByteLength = File(file.path).lengthSync();
        var type = lookupMimeType(file.path);
        SiswamediaGoogleSignIn.getInstance().scopes.addAll([
          'https://www.googleapis.com/auth/drive.file',
        ]);
        await SiswamediaGoogleSignIn.getInstance().signInSilently();
        var headers =
            await SiswamediaGoogleSignIn.getInstance().currentUser.authHeaders;
        var requestHeaders = <String, String>{
          'X-Upload-Content-Type': type ?? 'audio/ogg',
          'Content-Type': 'application/json',
          'X-Upload-Content-Length': '$totalByteLength',
        };
        headers.forEach((key, value) {
          requestHeaders[key] = value;
        });
        var resp = await client2.post(
            '$baseGoogleApiUrl/upload/drive/v3/files?uploadType=resumable',
            body: json.encode(<String, int>{
              'Content-Length': totalByteLength,
            }),
            headers: requestHeaders);
        print('response');
        print(resp.statusCode);
        print(resp.headers);
        // final httpClient = getHttpClient();
        //3145727
        print(file.size);
        // if (file.size > 1048576) {
        //   throw LargeFileException(cause: 'Data lebih dari 1GB');
        // }

        final client = HttpClient();
        final response =
            await client.putUrl(Uri.parse(resp.headers['location']));

        var byteCount = 0;

        //todo
        // response.contentLength = File(file.path).lengthSync();
        response.contentLength = totalByteLength;
        print('content lenght ${response.contentLength}');
        response.headers.set(HttpHeaders.contentTypeHeader, type);
        response.headers.set(HttpHeaders.contentLengthHeader, totalByteLength);
        response.headers.set(
            'Content-Range', 'bytes 0-${totalByteLength - 1}/$totalByteLength');

        headers.forEach((key, value) {
          response.headers.add(key, value);
        });

        final stream = File(file.path).openRead();

        Navigator.pop(context);
        var stream2 = stream.transform<List<int>>(
          StreamTransformer.fromHandlers(
            handleData: (data, sink) {
              byteCount += data.length;
              print(byteCount);
              sink.add(data);
              if (onUploadProgress != null) {
                print('uploading');
                onUploadProgress(byteCount, totalByteLength, context);
                // CALL STATUS CALLBACK;
              }
            },
            handleError: (error, stack, sink) {},
            handleDone: (sink) {
              sink.close();
            },
          ),
        );

        await response.addStream(stream2);

        final httpResponse = await response.close();
        var statusCode = httpResponse.statusCode;

        print(statusCode);
        if (statusCode ~/ 100 != 2) {
          print(statusCode);
          throw SiswamediaException(
              'Error uploading file, Status code: ${httpResponse.statusCode}');
        } else {
          return await readResponseAsString(httpResponse, file.name, headers);
        }
        // return response;
      } else {
        progressUploadState.state.progress = 0;
        // return null;
      }
    } on LargeFileException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } on ErrorUploadException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } catch (e) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        e.toString(),
        context,
      );
      print(e);
    }
  }

  Future<String> readResponseAsString(HttpClientResponse response,
      String filename, Map<String, String> headers) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
      // var result = json.decode(data) as Map<String, dynamic>;
      // print(result);
      // var file = ModelUploadFile.fromJson(result);
      // setAttachments(file.data);
    }, onDone: () async {
      completer.complete(contents.toString());
      var result = await completer.future;
      var data = json.decode(result) as Map<String, dynamic>;
      var client = Client();
      print(data['id']);
      var resp = await client.patch(
          '$baseGoogleApiUrl/drive/v3/files/${data['id']}?fields=*',
          body: json.encode({'name': filename}),
          headers: headers);
      print(resp.body);
      await client
          .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
              body: json.encode({'role': 'reader', 'type': 'anyone'}),
              headers: headers)
          .then((value) {
        print(value.body);
        CustomFlushBar.successFlushBar('Berhasil Mengupload Lampiran', context);
        setAttachments(DataFile(
            fileName: filename,
            file: 'https://drive.google.com/uc?export=view&id=${data['id']}'));
        progressUploadState.setState((s) => s.setProgress(0), silent: true);
      });
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes), silent: true);
    progressUploadState.setState((s) => s.setContent(totalBytes), silent: true);
    var __progressValue =
        Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }

  Widget buttonIzin(String title, Color color, Function onTap) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(8)),
          alignment: Alignment.center,
          padding: EdgeInsets.all(11),
          margin: EdgeInsets.all(4),
          child: vText(title, color: Colors.white),
        ),
      ),
    );
  }
}
