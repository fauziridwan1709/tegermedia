part of '../_absensi.dart';

class IzinSiswa extends StatefulWidget {
  @override
  _IzinSiswaState createState() => _IzinSiswaState();
}

class _IzinSiswaState extends State<IzinSiswa> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text("Absen",
            style: TextStyle(color: Colors.black, fontSize: S.w / 22)),
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: SiswamediaTheme.green),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
