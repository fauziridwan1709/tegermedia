part of '_absensi.dart';

class AbsensiWalikelasDetail extends StatefulWidget {
  @override
  _AbsensiWalikelasDetailState createState() => _AbsensiWalikelasDetailState();
}

class _AbsensiWalikelasDetailState extends State<AbsensiWalikelasDetail> {
  var auth = Injector.getAsReactive<AuthState>().state;
  var absensiState = Injector.getAsReactive<AbsensiState>();

  @override
  Widget build(BuildContext context) {
    var data = absensiState.state.model.data.coursesAbsenSummary;
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Absen Saya',
            style: TextStyle(color: Colors.black, fontSize: S.w / 22)),
        // actions: [
        //   Center(
        //     child: InkWell(
        //       onTap: () => Navigator.push(
        //           context, MaterialPageRoute<void>(builder: (_) => IzinGuru())),
        //       child: Padding(
        //         padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        //         child: Text('Izin',
        //             style: TextStyle(
        //                 color: SiswamediaTheme.green,
        //                 fontSize: S.w / 28,
        //                 fontWeight: FontWeight.w600)),
        //       ),
        //     ),
        //   )
        //   /*IconButton(
        //     icon: Icon(Icons.add, color: SiswamediaTheme.green),
        //     onPressed: () => callback(),
        //   )*/
        // ],
      ),
      body: SingleChildScrollView(
          child: Column(
        children: data
            .map((e) => Container(
                  width: S.w * .9,
                  height: S.w * .25,
                  margin: EdgeInsets.symmetric(
                      vertical: S.w * .025, horizontal: S.w * .05),
                  padding: EdgeInsets.symmetric(
                      vertical: S.w * .025, horizontal: S.w * .05),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(2, 2),
                            spreadRadius: 2,
                            blurRadius: 2,
                            color: Colors.grey.withOpacity(.2))
                      ]),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          e.courseName,
                          style: TextStyle(
                            fontSize: S.w / 25,
                            fontWeight: FontWeight.w600,
                            color: Colors.black87,
                          ),
                        ),
                        Row(children: [
                          RichText(
                            text: TextSpan(
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Hadir : ',
                                    style: TextStyle(
                                        fontSize: S.w / 28,
                                        color: Colors.grey[600])),
                                TextSpan(
                                    text:
                                        '${e.attend} / ${e.totalExistSchedule}',
                                    style: TextStyle(
                                        fontSize: S.w / 32,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black87)),
                              ],
                            ),
                          ),
                          SizedBox(width: 20),
                          RichText(
                            text: TextSpan(
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Izin : ',
                                    style: TextStyle(
                                        fontSize: S.w / 28,
                                        color: Colors.grey[600])),
                                TextSpan(
                                    text: '${e.halfAttend}',
                                    style: TextStyle(
                                        fontSize: S.w / 32,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black87)),
                              ],
                            ),
                          ),
                        ]),
                        Stack(
                          children: [
                            Container(
                                height: 15,
                                width: (S.w * .8) *
                                    ((e.totalExistSchedule == 0
                                            ? 0
                                            : e.attend *
                                                100 ~/
                                                e.totalExistSchedule) /
                                        100),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(S.w),
                                  color: Color(0xffDCEBCD),
                                )),
                            Container(
                              height: 15,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(S.w),
                                border:
                                    Border.all(color: SiswamediaTheme.green),
                              ),
                              child: Center(
                                  child: Text(
                                      '${e.totalExistSchedule == 0 ? 0 : e.attend * 100 ~/ e.totalExistSchedule}%',
                                      style: TextStyle(
                                          color: SiswamediaTheme.green,
                                          fontSize: S.w / 40,
                                          fontWeight: FontWeight.w700))),
                            ),
                          ],
                        )
                      ]),
                ))
            .toList(),
      )),
    );
  }

/*void callback() {
    int currentIndex = 0;
    double width = MediaQuery.of(context).size.width;
    DateTime start = widget.start;
    DateTime end = widget.end;
    DateTime day = DateTime.now();
    var data = Provider.of<JadwalProvider>(context, listen: false);
    var judul = widget.judul;

    showModalBottomSheet(
        context: context,
        builder: (context) => StatefulBuilder(
              builder: (BuildContext context,
                      void Function(void Function()) setState) =>
                  Container(
                color: Color(0xFF737373),
                child: Container(
                  height: width * 1.3,
                  padding: EdgeInsets.symmetric(
                      horizontal: width * .05, vertical: width * .05),
                  decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(20.0),
                          topRight: const Radius.circular(20.0))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              icon: Icon(
                                Icons.clear,
                                color: SiswamediaTheme.green,
                              ),
                              onPressed: () => Navigator.pop(context),
                            ),
                            Text('Ubah jadwal ini'),
                            Container(
                                width: width * .2,
                                height: width * .07,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7),
                                    color: SiswamediaTheme.green),
                                child: InkWell(
                                  onTap: () {
                                    data.ubahJadwal(
                                        JadwalModel(
                                            judul: widget.judul,
                                            start: widget.start,
                                            end: widget.end,
                                            kategori: widget.kategori),
                                        judul,
                                        start,
                                        end,
                                        currentIndex);
                                    Navigator.pop(context);
                                  },
                                  child: Center(
                                    child: Text('simpan',
                                        style: TextStyle(color: Colors.white)),
                                  ),
                                ))
                          ]),
                      Container(
                          height: width * .13,
                          width: width * .9,
                          child: TextField(
                            controller: _editingController,
                            onChanged: (valie) {
                              setState(() => judul = valie);
                            },
                            decoration: InputDecoration(
                              hintText: 'Judul',
                            ),
                          )),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Atur Setiap Hari'),
                            Switch(
                              value: false,
                              onChanged: (bool value) {},
                            )
                          ]),
                      Divider(color: Colors.grey),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            DatePicker.showDateTimePicker(context,
                                showTitleActions: true,
                                minTime:
                                    DateTime.now().subtract(Duration(days: 10)),
                                maxTime:
                                    DateTime.now().add(Duration(days: 366)),
                                onChanged: (date) {
                              print('change $date in time zone ' +
                                  date.timeZoneOffset.inHours.toString());
                            }, onConfirm: (date) {
                              setState(() {
                                start = date;
                              });
                              print('confirm $date');
                            }, locale: LocaleType.id);
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text('Waktu Mulai'),
                                Text(
                                    '${start.day} ${bulan[start.month - 1]} ${start.year}   ${start.hour < 10 ? '0' : '}${start.hour}.${start.minute < 10 ? '0' : '}${start.minute}')
                              ]),
                        ),
                      ),
                      Divider(color: Colors.grey),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            DatePicker.showDateTimePicker(context,
                                showTitleActions: true,
                                minTime:
                                    DateTime.now().subtract(Duration(days: 10)),
                                maxTime:
                                    DateTime.now().add(Duration(days: 366)),
                                onChanged: (date) {
                              print('change $date in time zone ' +
                                  date.timeZoneOffset.inHours.toString());
                            }, onConfirm: (date) {
                              setState(() {
                                end = date;
                              });
                              print('confirm $date');
                            }, locale: LocaleType.id);
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text('Waktu Selesai'),
                                Text(
                                    '${end.day} ${bulan[end.month - 1]} ${end.year}   ${end.hour < 10 ? '0' : '}${end.hour}.${end.minute < 10 ? '0' : '}${end.minute}')
                              ]),
                        ),
                      ),
                      Divider(color: Colors.grey),
                      Text('Pilih Kategori'),
                      Wrap(
                        alignment: WrapAlignment.start,
                        runAlignment: WrapAlignment.start,
                        runSpacing: 0.0,
                        spacing: width / 72,
                        children: kategori
                            .map((e) => ActionChip(
                                label: Text(
                                  e,
                                  style: TextStyle(fontSize: width / 30),
                                ),
                                labelStyle: TextStyle(
                                  color: currentIndex == kategori.indexOf(e)
                                      ? Colors.white
                                      : Colors.black,
                                ),
                                backgroundColor:
                                    currentIndex == kategori.indexOf(e)
                                        ? SiswamediaTheme.green
                                        : Colors.grey.shade200,
                                onPressed: () {
                                  setState(
                                      () => currentIndex = kategori.indexOf(e));
                                }))
                            .toList(),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }*/
}
