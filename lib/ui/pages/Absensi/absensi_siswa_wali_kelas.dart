part of '_absensi.dart';

class SiswaWaliKelas extends StatefulWidget {
  const SiswaWaliKelas({this.userId});
  final int userId;
  @override
  _SiswaWaliKelasState createState() => _SiswaWaliKelasState();
}

class _SiswaWaliKelasState extends State<SiswaWaliKelas>
    with TickerProviderStateMixin {
  var auth = Injector.getAsReactive<AuthState>().state;
  var absensiState = Injector.getAsReactive<AbsensiState>();
  Animation<double> animation;
  AnimationController controller;
  TabController tabController;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: Duration(milliseconds: 700), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeInOut));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    //todo
    // var data = absensiState.state.model.data.coursesAbsenSummary;
    return Scaffold(
        backgroundColor: SiswamediaTheme.background,
        appBar: AppBar(
          shadowColor: Colors.transparent,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Absen',
              style: TextStyle(color: Colors.black, fontSize: S.w / 22)),
        ),
        body: FutureBuilder<AbsensiSummaryModel>(
            future: SummaryAbsensi.getSummaryUserId(
                classId: auth.currentState.classId, userId: widget.userId),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.data == null) {
                  return Center(
                    child: Text('no Record Found',
                        style: descBlack.copyWith(fontSize: S.w / 28)),
                  );
                }
                return SingleChildScrollView(
                  child: Column(children: [
                    Highlight(
                      animation: animation,
                      controller: controller,
                      title: 'Rekap Absensi',
                      item: snapshot.data.data,
                    ),
                    Column(
                      children: snapshot.data.data.coursesAbsenSummary
                          .map((e) => Container(
                                width: S.w * .9,
                                height: S.w * .25,
                                margin: EdgeInsets.symmetric(
                                    vertical: S.w * .025,
                                    horizontal: S.w * .05),
                                padding: EdgeInsets.symmetric(
                                    vertical: S.w * .025,
                                    horizontal: S.w * .05),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(2, 2),
                                          spreadRadius: 2,
                                          blurRadius: 2,
                                          color: Colors.grey.withOpacity(.2))
                                    ]),
                                child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        e.courseName,
                                        style: TextStyle(
                                          fontSize: S.w / 25,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black87,
                                        ),
                                      ),
                                      Row(children: [
                                        RichText(
                                          text: TextSpan(
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black,
                                            ),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: 'Hadir : ',
                                                  style: TextStyle(
                                                      fontSize: S.w / 28,
                                                      color: Colors.grey[600])),
                                              TextSpan(
                                                  text:
                                                      '${e.attend} / ${e.totalExistSchedule}',
                                                  style: TextStyle(
                                                      fontSize: S.w / 32,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Colors.black87)),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 20),
                                        RichText(
                                          text: TextSpan(
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black,
                                            ),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: 'Izin : ',
                                                  style: TextStyle(
                                                      fontSize: S.w / 28,
                                                      color: Colors.grey[600])),
                                              TextSpan(
                                                  text: '${e.permit}',
                                                  style: TextStyle(
                                                      fontSize: S.w / 32,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Colors.black87)),
                                            ],
                                          ),
                                        ),
                                      ]),
                                      Stack(
                                        children: [
                                          Container(
                                              height: 15,
                                              width: (S.w * .8) *
                                                  ((e.totalExistSchedule == 0
                                                          ? 0
                                                          : e.attend *
                                                              100 ~/
                                                              e.totalExistSchedule) /
                                                      100),
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(S.w),
                                                color: Color(0xffDCEBCD),
                                              )),
                                          Container(
                                            height: 15,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(S.w),
                                              border: Border.all(
                                                  color: SiswamediaTheme.green),
                                            ),
                                            child: Center(
                                                child: Text(
                                                    '${e.totalExistSchedule == 0 ? 0 : e.attend * 100 ~/ e.totalExistSchedule}%',
                                                    style: TextStyle(
                                                        color: SiswamediaTheme
                                                            .green,
                                                        fontSize: S.w / 40,
                                                        fontWeight:
                                                            FontWeight.w700))),
                                          ),
                                        ],
                                      )
                                    ]),
                              ))
                          .toList(),
                    ),
                  ]),
                );
              }
              return Center(child: CircularProgressIndicator());
            }));
  }
}
