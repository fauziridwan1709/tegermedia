part of '_absensi.dart';

class Absensi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var auth = Injector.getAsReactive<AuthState>().state;
    if (auth.currentState.classRole == 'GURU') {
      return AbsensiGuru();
    } else if (auth.currentState.classRole == 'WALIKELAS') {
      return AbsensiWaliKelas();
    } else {
      print(auth.currentState.classRole);
      return AbsensiSiswa();
    }
  }

  Widget sectionNotLogin(BuildContext context) {
    return Container(
        height: S.h,
        width: S.w,
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              child: Image.asset(
                'assets/dev.png',
                alignment: Alignment.bottomCenter,
                height: S.w * .6,
                width: S.w * 1.1,
                fit: BoxFit.contain,
              ),
            ),
            Column(
              children: [
                Container(
                  margin:
                      EdgeInsets.only(top: S.h / 4 - 20, left: 40, right: 40),
                  child: Text(
                      'Fitur ini hanya bisa diakses jika kamu terdaftar disekolah',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: S.w / 28,
                          fontWeight: FontWeight.w700,
                          color: Colors.black87)),
                ),
                InkWell(
                  onTap: () => LaunchServices.launchInBrowser(
                      'https://docs.google.com/forms/d/e/1FAIpQLSdwdj7asTE6oE12vGnitHM5Nxx95NjFjV5zO4NwKOuT62nw2g/viewform'),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    margin: EdgeInsets.only(top: 15),
                    decoration: BoxDecoration(
                        color: SiswamediaTheme.green,
                        borderRadius: BorderRadius.circular(6)),
                    child: Text('Daftarkan Sekolah',
                        style: TextStyle(color: Colors.white)),
                  ),
                )
              ],
            ),
          ],
        ));
  }
}
