part of '_absensi.dart';

class AbsensiGuru extends StatefulWidget {
  @override
  _AbsensiGuruState createState() => _AbsensiGuruState();
}

class _AbsensiGuruState extends State<AbsensiGuru>
    with SingleTickerProviderStateMixin {
  var auth = Injector.getAsReactive<AuthState>().state;
  var absensiState = Injector.getAsReactive<AbsensiState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: Duration(milliseconds: 700), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeInOut));
    controller.forward();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {});
    Initializer(
            reactiveModel: absensiState,
            cacheKey: 'time_absensi_key',
            rIndicator: refreshIndicatorKey,
            state: false)
        .initialize();
  }

  Future<void> retrieveData() async {
    await absensiState.setState((s) => s.getSummary(auth.currentState.classId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Absen',
            style: TextStyle(
                color: Colors.black,
                fontSize: S.w / 22,
                fontWeight: FontWeight.w700)),
        actions: [
          /*Center(
            child: InkWell(
              onTap: () => Navigator.push(
                  context, MaterialPageRoute<void>(builder: (_) => IzinGuru())),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Text('Izin',
                    style: TextStyle(
                        color: SiswamediaTheme.green,
                        fontSize: width / 28,
                        fontWeight: FontWeight.w600)),
              ),
            ),
          )*/
        ],
      ),
      body: StateBuilder<AbsensiState>(
          observe: () => absensiState,
          builder: (context, constrains) {
            return RefreshIndicator(
              onRefresh: retrieveData,
              key: refreshIndicatorKey,
              child: WhenRebuilder<AbsensiState>(
                observe: () => absensiState,
                onWaiting: () => WaitingView(),
                onIdle: () => WaitingView(),
                onError: (dynamic error) => ErrorView(error: error),
                onData: (data) {
                  if (absensiState.state.model.data == null) {
                    return EmptyRecordText();
                  }
                  var data = absensiState.state.model.data.coursesAbsenSummary;
                  return ListView(children: [
                    Highlight(
                      animation: animation,
                      controller: controller,
                      title: 'Absensi Saya',
                      button: Container(
                        height: S.w * .09,
                        decoration: BoxDecoration(
                          color: SiswamediaTheme.green,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: InkWell(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                  builder: (_) => AbsensiWalikelasDetail())),
                          child: Center(
                              child: Text('Selengkapnya',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: S.w / 30,
                                      fontWeight: FontWeight.normal))),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                      child: Text('Absensi Pelajaran Saya',
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: S.w / 23,
                              fontWeight: FontWeight.w600)),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                      child: Column(
                        children: data
                            .map((e) => Container(
                                  width: S.w * .9,
                                  height: S.w * .2,
                                  margin: EdgeInsets.symmetric(
                                      vertical: S.w * .025),
                                  padding: EdgeInsets.symmetric(
                                      vertical: S.w * .025,
                                      horizontal: S.w * .05),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: [
                                        BoxShadow(
                                            offset: Offset(2, 2),
                                            spreadRadius: 2,
                                            blurRadius: 2,
                                            color: Colors.grey.withOpacity(.2))
                                      ]),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          e.courseName,
                                          style: TextStyle(
                                            fontSize: S.w / 25,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black87,
                                          ),
                                        ),
                                        SizedBox(height: 8),
                                        RichText(
                                          text: TextSpan(
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black,
                                            ),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: 'Sekolah : ',
                                                  style: TextStyle(
                                                      fontSize: S.w / 28,
                                                      color: Colors.grey[600])),
                                              TextSpan(
                                                  text: auth
                                                      .currentState.schoolName,
                                                  style: TextStyle(
                                                      fontSize: S.w / 32,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Colors.black87)),
                                            ],
                                          ),
                                        ),
                                      ]),
                                ))
                            .toList(),
                      ),
                    ),
                  ]);
                },
              ),
            );
          }),
    );
  }
}
