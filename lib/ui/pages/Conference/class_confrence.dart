part of '_conference.dart';

class Conference extends StatefulWidget {
  @override
  _ConferenceState createState() => _ConferenceState();
}

class _ConferenceState extends State<Conference> {
  final authState = Injector.getAsReactive<AuthState>();
  final classState = Injector.getAsReactive<ClassState>();
  final conferenceState = Injector.getAsReactive<ConferenceState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var present = DateTime.now();
  var loading = true;

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(TIME_LIST_CONFERENCE)) {
        var date = value.getString(TIME_LIST_CONFERENCE);
        if (isDifferenceLessThanFiveMin(date) && conferenceState.state.conferenceModel != null) {
        } else {
          await value.setString(TIME_LIST_CONFERENCE, DateTime.now().toIso8601String());
          await refreshIndicatorKey.currentState.show();
        }
      } else {
        await value.setString(TIME_LIST_CONFERENCE, DateTime.now().toIso8601String());
        await refreshIndicatorKey.currentState.show();
      }
    });
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await SharedPreferences.getInstance().then((value) async {
        await conferenceState.setState(
            (s) async => await s.retrieveData(authState.state.currentState.classRole == 'ORTU'),
            catchError: true, onError: (context, dynamic error) {
          print((error as SiswamediaException).message);
        }, onData: (_, data) async {
          //todo check again this
          // if (classState.state.classModel == null ||
          //     !isDifferenceLessThanFiveMin(value.getString(TIME_LIST_CLASS))) {
          //   await classState.setState((s) =>
          //       s.retrieveData(authState.state.currentState.schoolId, true));
          //   conferenceState.notify();
          // }
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          shadowColor: Colors.transparent,
          brightness: Brightness.light,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Konferensi',
              style: semiBlack.copyWith(color: Colors.black, fontSize: S.w / 22)),
          actions: [
            if (authState.state.currentState.classRole.isGuruOrWaliKelas ||
                (authState.state.currentState.type == 'PERSONAL'
                    ? authState.state.currentState.schoolRole.first != 'SISWA'
                    : false))
              IconButton(
                icon: Icon(Icons.add),
                color: SiswamediaTheme.green,
                onPressed: () => context.push<void>(CreateConference()),
              )
          ],
        ),
        body: StateBuilder<ConferenceState>(
          observeMany: [() => conferenceState, () => classState],
          builder: _buildListConferences,
        ));
  }

  Widget _buildListConferences(BuildContext context, ReactiveModel<ConferenceState> snapshot) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return RefreshIndicator(
      key: refreshIndicatorKey,
      onRefresh: () async {
        await retrieveData();
      },
      child: WhenRebuilder<ConferenceState>(
        observeMany: [() => conferenceState, () => classState],
        onWaiting: () => WaitingView(),
        onError: (dynamic error) => ErrorView(error: (error)),
        onIdle: () => WaitingView(),
        onData: (data) {
          return ListView.builder(
              itemCount: 1,
              itemBuilder: (context, index) {
                if (data != null) {
                  if (data.conferenceModel
                      .where((element) =>
                          DateTime.parse(element.dateStart.substring(0, 19)).difference(present) >
                              Duration(days: -1) &&
                          ((element.classId == authState.state.currentState.classId)))
                      .toList()
                      .isEmpty) {
                    return Padding(
                      padding: EdgeInsets.only(top: S.w * .180),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/icons/kelas/conference_empty.png',
                              width: S.w * .6, height: S.w * .6),
                          Text(
                            'Belum ada Konferensi Terbaru',
                            style: descBlack.copyWith(fontSize: S.w / 28),
                          )
                        ],
                      ),
                    );
                  }
                  return SingleChildScrollView(
                    child: GridView.count(
                      crossAxisSpacing: 10,
                      crossAxisCount: gridCountHome(MediaQuery.of(context).size.aspectRatio),
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      childAspectRatio: isPortrait ? 1.85 : 1.6,
                      children: data.conferenceModel
                          .where((element) =>
                              (DateTime.parse(element.dateStart.substring(0, 19))
                                      .difference(DateTime.now()) >
                                  Duration(days: -1)) &&
                              ((element.classId == authState.state.currentState.classId)))
                          .map((e) {
                        var dateStart = DateTime.parse(e.dateStart).toLocal();
                        var dateStartEnd = DateTime.parse(e.dateStart)
                            .add(Duration(minutes: e.duration))
                            .toLocal();
                        var availability = checkAvailability(
                            dateStart.toUtc().difference(DateTime.now().toUtc()),
                            Duration(minutes: -e.duration));

                        return CardConference(
                          context: context,
                          availability: availability,
                          dateStart: dateStart,
                          dateStartEnd: dateStartEnd,
                          width: S.w,
                          data: e,
                          onTapOption: conferenceCardAction,
                          onTap: joinConference,

                          ///ConferenceListModelData
                        );
                      }).toList(),
                    ),
                  );
                }
                return Padding(
                  padding: EdgeInsets.only(top: S.w * .180),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/icons/kelas/conference_empty.png',
                          width: S.w * .6, height: S.w * .6),
                      Text(
                        'Belum ada Konferensi Terbaru',
                        style: descBlack.copyWith(fontSize: S.w / 28),
                      )
                    ],
                  ),
                );
              });
        },
      ),
    );
  }

  Widget sectionNotLogin(BuildContext context) {
    return Container(
        height: S.h,
        width: S.w,
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              child: Image.asset(
                'assets/dev.png',
                alignment: Alignment.bottomCenter,
                height: S.w * .6,
                width: S.w,
                fit: BoxFit.contain,
              ),
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: S.h / 4 - 20, left: 40, right: 40),
                  child: Text('Oops kelas Personal tidak dapat mengakses fitur ini',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: S.w / 30, fontWeight: FontWeight.w700, color: Colors.black87)),
                ),
              ],
            ),
          ],
        ));
  }

  void conferenceCardAction(BuildContext context, int id, String link) {
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        context: context,
        builder: (context2) => Container(
              padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(20.0), topRight: const Radius.circular(20.0))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                //todo edit conference
                children: [
                  HeightSpace(20),
                  ListTile(
                    leading: Icon(Icons.edit),
                    title: Text(
                      'Share Konferensi',
                      style: secondHeadingBlack.copyWith(fontSize: S.w / 28),
                    ),
                    onTap: () {
                      ShareService.share(context, link, 'Share konferensi ke');
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.delete),
                    title: Text('Hapus Konferensi',
                        style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                    onTap: () async {
                      Navigator.pop(context2);
                      await SiswamediaTheme.infoLoading(
                          context: context, info: 'Menghapus Konferensi');
                      await deleteConference(id: id).then((value) {
                        var statusCode = value.statusCode;
                        var message = value.message;
                        Navigator.pop(context);
                        if (statusCode == StatusCodeConst.success) {
                          conferenceState.setState((s) =>
                              s.retrieveData(authState.state.currentState.classRole == 'ORTU'));
                          CustomFlushBar.successFlushBar(
                            message,
                            context,
                          );
                        } else {
                          CustomFlushBar.errorFlushBar(
                            message,
                            context,
                          );
                        }
                      });
                    },
                  ),
                ],
              ),
            ));
  }

  void joinConference(Availability availability, String url) {
    if (availability.isAvailable) {
      if (authState.state.currentState.classRole == 'ORTU') {
        showDialog<Widget>(
            context: context,
            builder: (context) {
              return CustomDialog(
                title: 'Tunggu!',
                description: 'Anda ortu, tetap ingin join konferensi?',
                actions: ['Tetap Join'],
                onTap: () {
                  Navigator.pop(context);
                  LaunchServices.launchInBrowser(url);
                },
              );
            });
      } else {
        LaunchServices.launchInBrowser(url);
      }
    } else {
      CustomFlushBar.errorFlushBar(
        'Conference Belum Dimulai',
        context,
      );
    }
  }

  ///return general result API
  Future<GeneralResultAPI> deleteConference({int id}) async {
    var url = '$baseUrl/$version/confrences/$id';
    var client = http.Client();
    try {
      var prefs = await SharedPreferences.getInstance();
      var resp = await client
          .delete(url, headers: {'Authorization': prefs.getString(AUTH_KEY)}).timeout(
              Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try again'));

      print(resp.body);
      return GeneralResultAPI(message: 'Berhasil Menghapus', statusCode: StatusCodeConst.success);
    } on SocketException {
      return GeneralResultAPI(message: 'error', statusCode: StatusCodeConst.noInternet);
    } on TimeoutException catch (e) {
      ///e.message berisi 'Timeout, try again'
      return GeneralResultAPI(message: e.message, statusCode: StatusCodeConst.timeout);
    } catch (e) {
      return GeneralResultAPI(message: 'error', statusCode: StatusCodeConst.general);
    }
  }
}

class GeneralResultAPI {
  String message;
  int statusCode;
  dynamic data;

  GeneralResultAPI({this.message, this.statusCode, this.data});

  GeneralResultAPI.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    statusCode = json['status_code'];
    data = json['data'];
  }
}
