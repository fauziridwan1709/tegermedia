part of '_conference.dart';

class CreateConference extends StatefulWidget {
  @override
  _CreateConferenceState createState() => _CreateConferenceState();
}

class _CreateConferenceState extends State<CreateConference> {
  final authState = Injector.getAsReactive<AuthState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var prefs = SharedPreferences.getInstance();
  var isLoading = true;

  // Form
  DateTime start = DateTime.now().toUtc();
  var mapelIndex = 0;
  var _course = 0;
  var duration = 20;
  var classId = 0;
  final TextEditingController _title = TextEditingController();
  final TextEditingController _duration = TextEditingController();
  final TextEditingController _description = TextEditingController();

  var fNode = FocusNode();
  var dNode = FocusNode();
  var descNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _title.dispose();
    _duration.dispose();
    _description.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (fNode.hasFocus || dNode.hasFocus || descNode.hasFocus) {
          fNode.unfocus();
          dNode.unfocus();
          descNode.unfocus();
          return Future.value(false);
        }
        return Future.value(true);
      },
      child: Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: SiswamediaAppBar(
          context: context,
          title: 'Buat Konferensi',
          icon: Icons.clear,
          additionalFunction: () {
            if (fNode.hasFocus || dNode.hasFocus || descNode.hasFocus) {
              fNode.unfocus();
              dNode.unfocus();
              descNode.unfocus();
            }
          },
        ),
        body: GestureDetector(
          onTap: () {
            if (fNode.hasFocus || dNode.hasFocus || descNode.hasFocus) {
              fNode.unfocus();
              dNode.unfocus();
              descNode.unfocus();
            }
          },
          child: Center(
            child: SingleChildScrollView(
              child: Container(
                constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height),
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .07, vertical: S.w * .05),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(20.0),
                        topRight: const Radius.circular(20.0))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        height: S.w * .13,
                        width: S.w * .9,
                        child: TextField(
                          controller: _title,
                          focusNode: fNode,
                          onChanged: (value) {
                            // setState(() => judul = value);
                          },
                          decoration: InputDecoration(
                              hintText: 'Judul',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: SiswamediaTheme.green))),
                        )),
                    Container(
                        height: S.w * .13,
                        width: S.w * .9,
                        child: TextField(
                          controller: _description,
                          focusNode: descNode,
                          onChanged: (value) {
                            // setState(() => description = value);
                          },
                          decoration: InputDecoration(
                              hintText: 'Deskripsi',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: SiswamediaTheme.green))),
                        )),
                    SizedBox(
                      height: 16,
                    ),
                    SizedBox(
                      width: S.w * .9,
                      child: DateTimePicker(
                        type: DateTimePickerType.dateTime,
                        dateMask: 'MMM dd, yyyy - HH:mm',
                        firstDate: DateTime.now(),
                        lastDate: DateTime(2100),
                        icon: Icon(Icons.calendar_today_outlined),
                        dateLabelText: 'Waktu Mulai',
                        onChanged: (val) =>
                            setState(() => start = DateTime.parse(val).toUtc()),
                      ),
                    ),
                    //TODO
                    // Container(
                    //   width: S.w * .9,
                    //   child: Material(
                    //     color: Colors.transparent,
                    //     child: InkWell(
                    //       onTap: () {
                    //         fNode.unfocus();
                    //         // DatePicker.showDateTimePicker(context,
                    //         //     showTitleActions: true,
                    //         //     minTime: DateTime.now(),
                    //         //     maxTime:
                    //         //         DateTime.now().add(Duration(days: 366)),
                    //         //     onChanged: (date) {
                    //         //   print('change $date in time zone ' +
                    //         //       date.timeZoneOffset.inHours.toString());
                    //         // }, onConfirm: (date) {
                    //         //   setState(() {
                    //         //     start = date.toUtc();
                    //         //   });
                    //         // }, locale: LocaleType.id);
                    //       },
                    //
                    //       child: Row(
                    //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //           crossAxisAlignment: CrossAxisAlignment.center,
                    //           children: [
                    //             Text(
                    //               'Waktu Mulai',
                    //               style: descBlack.copyWith(fontSize: S.w / 26),
                    //             ),
                    //             Text(
                    //               '${start.toLocal().day} ${bulan[start.toLocal().month - 1]} ${start.toLocal().year}   ${start.toLocal().hour < 10 ? '0' : ''}${start.toLocal().hour}.${start.toLocal().minute < 10 ? '0' : ''}${start.toLocal().minute}',
                    //               style: descBlack.copyWith(fontSize: S.w / 26),
                    //             )
                    //           ]),
                    //     ),
                    //   ),
                    // ),

                    ///divider
                    // Container(
                    //     width: S.w * .9, child: Divider(color: Colors.grey)),
                    SizedBox(height: 10),
                    Container(
                        height: S.w * .13,
                        width: S.w * .9,
                        child: TextField(
                          controller: _duration,
                          focusNode: dNode,
                          onChanged: (value) {
                            setState(() => duration = int.parse(value));
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          decoration: InputDecoration(
                              hintText: 'Durasi',
                              suffix: Text('Menit'),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: SiswamediaTheme.green))),
                        )),
                    SizedBox(height: 20),
                    Injector(
                        inject: [Inject(() => CourseState())],
                        builder: _buildMataPelajaran),

                    Container(
                      alignment: Alignment.bottomCenter,
                      padding: EdgeInsets.symmetric(vertical: S.w * .02),
                      color: Colors.white,
                      child: Container(
                          width: S.w * .9,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          height: S.w * .16,
                          child: RaisedButton(
                              elevation: 0,
                              // padding: EdgeInsets.symmetric(vertical: 10),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Text(
                                'Buat Link Ruangan',
                                style: descWhite.copyWith(
                                    fontSize: S.w / 28, color: Colors.white),
                              ),
                              color: SiswamediaTheme.green,
                              onPressed: () async {
                                await onCreateEvents(
                                  context,
                                );
                              })),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onCreateEvents(BuildContext context) async {
    var courseState = Injector.getAsReactive<CourseState>();
    var conferenceState = Injector.getAsReactive<ConferenceState>();
    var profile = Injector.getAsReactive<ProfileState>().state.profile;
    if ((authState.state.currentState.classId != null) &&
        _title.text != '' &&
        _description.text != '' &&
        _duration.text != '' &&
        start != DateTime.now() &&
        courseState.state.courseModel.data.isNotEmpty &&
        courseState.state.courseModel.data[mapelIndex] != null) {
      final reqBody = <String, dynamic>{
        'start': {'dateTime': '${start.toIso8601String()}'},
        'end': {
          'dateTime':
              '${start.add(Duration(minutes: int.parse(_duration.text))).toIso8601String()}'
        },
        'conferenceData': {
          'createRequest': {
            'requestId':
                '${authState.state.currentState.classId}${_duration.text}$start',
            'conferenceSolutionKey': {'type': 'hangoutsMeet'}
          }
        },
        'summary': _title.text,
        'description': _description.text
      };
      await GoogleMeet.createEvent(
              context: context, id: profile.email, reqBody: reqBody)
          .then((value) async {
        if (value.statusCode == 200) {
          var data = ConferenceDetail(
              title: _title.text,
              url: '${value.hangoutLink}',
              description: '${_description.text}',
              duration: int.parse(_duration.text),
              courseId: _course,
              date: start.toIso8601String(),
              classId: authState.state.currentState.classId);
          await ConferenceServices.createConference(reqBody: data, courseID: 0)
              .then((e) async {
            print(e.statusCode);
            Navigator.pop(context);
            if (e.statusCode == 201 || e.statusCode == 200) {
              await showDialog<void>(
                  context: context,
                  builder: (context) => StatefulBuilder(builder:
                          (BuildContext context,
                              void Function(void Function()) setState) {
                        // String role = someCapitalizedString(data.role);
                        return Dialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12)),
                            child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: S.w * .05, horizontal: S.w * .05),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(height: 25),
                                    Container(
                                      height: 100,
                                      width: 170,
                                      margin: EdgeInsets.only(bottom: 12),
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/icons/kelas/conference_success.png'))),
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Text('Ruangan Berhasil Dibuat',
                                        style: TextStyle(
                                            fontSize: S.w / 24,
                                            fontWeight: FontWeight.w600)),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                              height: S.w / 12,
                                              width: S.w / 12,
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 5, vertical: 5),
                                              decoration: BoxDecoration(
                                                  color: SiswamediaTheme.green,
                                                  borderRadius:
                                                      BorderRadius.circular(4)),
                                              child: Icon(
                                                Icons.attach_file,
                                                size: S.w / 24,
                                                color: Colors.white,
                                              )),
                                        ),
                                        Container(
                                          height: S.w / 12,
                                          width: S.w * .5,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 5),
                                          decoration: BoxDecoration(
                                              color: Color(0xFFECECEC),
                                              borderRadius:
                                                  BorderRadius.circular(4),
                                              border: Border.all(
                                                  color: Color(0xFFECECEC))),
                                          child: Center(
                                            child: Text(value.hangoutLink,
                                                style: TextStyle(
                                                    fontSize: S.w / 32,
                                                    color:
                                                        SiswamediaTheme.green)),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                  ],
                                )));
                      })).then((_) {
                Navigator.pop(context);
                Clipboard.setData(ClipboardData(text: value.hangoutLink));
                CustomFlushBar.eventFlushBar(
                    'Url meeting otomatis disalin ke clipboard', context);
              });
              await conferenceState.setState((s) => s.retrieveData(
                  authState.state.currentState.classRole == 'ORTU'));
            } else {
              CustomFlushBar.errorFlushBar(
                'Gagal Membuat Conference',
                context,
              );
            }
          });
        } else {
          CustomFlushBar.errorFlushBar(
            'Gagal singkron ke google meet',
            context,
          );
        }
      });
      setState(() {
        _description.clear();
        _title.clear();
        start = DateTime.now();
        duration = 20;
      });
    } else if (authState.state.currentState.classId == null) {
      CustomFlushBar.errorFlushBar(
        'Belum Punya kelas',
        context,
      );
    } else if (_title.text == '') {
      CustomFlushBar.errorFlushBar(
        'Judul harus di isi',
        context,
      );
    } else if (_course == 0) {
      CustomFlushBar.errorFlushBar(
        'Pilih Mata Pelajaran',
        context,
      );
    } else if (_description.text == '') {
      CustomFlushBar.errorFlushBar('Deskripsi harus di isi', context);
    } else if (_duration.text == '') {
      CustomFlushBar.errorFlushBar(
        'Durasi harus di isi',
        context,
      );
    } else if (courseState.state.courseModel.data.isEmpty) {
      CustomFlushBar.errorFlushBar(
        'Mata pelajaran dibutuhkan',
        context,
      );
    }
  }

  Widget successDialog() {
    return Container();
  }

  Widget _buildMataPelajaran(BuildContext context) {
    var courseState = Injector.getAsReactive<CourseState>();
    if (courseState.state.courseModel == null) {
      courseState.setState((s) => s.retrieveData(
          data: CourseByClassModel(
              param: GetCourseModel(
                  classId: authState.state.currentState.classId))));
    }
    return StateBuilder<CourseState>(
      observe: () => courseState,
      builder: (context, snapshot) {
        return WhenRebuilder<CourseState>(
            observe: () => snapshot,
            onWaiting: () => Center(child: CircularProgressIndicator()),
            onError: (dynamic error) => Error(error: (error)),
            onIdle: () => Container(),
            onData: (data) {
              var courselist = data.courseModel.data;
              var course = courselist
                  .where((element) =>
                      (element.role == 'GURU') ||
                      authState.state.currentState.classRole.isWaliKelas)
                  .toList();
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: S.w * .9,
                    child: Text(
                      authState.state.currentState.type != 'PERSONAL'
                          ? 'Pilih Mata Pelajaran'
                          : 'Pilih Course',
                      style: semiBlack.copyWith(fontSize: S.w / 26),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(height: 10),
                  if (course.isNotEmpty)
                    SizedBox(
                      width: S.w * .6,
                      child: DropdownButtonFormField<int>(
                        isExpanded: true,
                        value: _course,
                        icon: Icon(Icons.keyboard_arrow_down,
                            color: SiswamediaTheme.green),
                        iconSize: 24,
                        elevation: 16,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 0.0, horizontal: 25),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffD8D8D8)),
                            borderRadius: BorderRadius.circular(S.w),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: SiswamediaTheme.green),
                            borderRadius: BorderRadius.circular(S.w),
                          ),
                        ),
                        style: descBlack.copyWith(
                            fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                        onChanged: (int newValue) {
                          setState(() {
                            _course = newValue;
                          });
                        },
                        items: [
                          DropdownMenuItem(
                            child: Text('Pilih'),
                            value: 0,
                          ),
                          for (CourseByClassData value in course)
                            DropdownMenuItem(
                              value: value.id,
                              child: Text('${value.name}'),
                            )
                        ],
                      ),
                    ),
                  if (course.isEmpty)
                    Text(
                        'Kamu Belum terdaftar di mata pelajaran manapun daftar dulu',
                        style: descBlack.copyWith(fontSize: S.w / 32)),
                ],
              );
            });
      },
    );
  }
}
