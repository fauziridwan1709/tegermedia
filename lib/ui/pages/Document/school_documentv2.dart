part of '_document.dart';

class SchoolDocumentv2 extends StatefulWidget {
  SchoolDocumentv2({this.classId, this.type, this.onUpload});

  final String type;
  final int classId;
  final Function(String parent) onUpload;

  @override
  _SchoolDocumentv2State createState() => _SchoolDocumentv2State();
}

class _SchoolDocumentv2State extends State<SchoolDocumentv2> {
  final document = Injector.getAsReactive<DocumentState>();
  final auth = Injector.getAsReactive<AuthState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var data = document.state;
    var listData = data.schoolDocuments;
    Utils.log(listData, info: 'list data');
    var parent =
        data.listDocSchool.isEmpty ? data.schoolDoc : data.listDocSchool.last;
    return FutureBuilder<AllSubFile>(
        future: GoogleDrive.getAllFile(parent: parent),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasData) {
            if (snapshot.data.files.isEmpty) {
              return Column(
                children: [
                  SizedBox(height: 100),
                  Image.asset(
                    'assets/icons/dokumen/empty.png',
                    height: S.w * .4,
                    width: S.w * .4,
                  ),
                  Text('Belum ada dokumen',
                      style: TextStyle(color: SiswamediaTheme.green)),
                ],
              );
            }
            if (document.state.listDocSchool.isNotEmpty &&
                auth.state.currentState.isAdmin) {
              snapshot.data.files
                  .add(Files(name: 'Upload', mimeType: 'upload'));
            }
            return GridView.count(
              physics: ScrollPhysics(),
              scrollDirection: Axis.vertical,
              crossAxisCount: 2,
              childAspectRatio: 1,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              padding: EdgeInsets.symmetric(horizontal: 10),
              shrinkWrap: true,
              children: snapshot.data.files
                  .map((e) => Container(
                        margin: EdgeInsets.symmetric(vertical: S.w * .02),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 0),
                                spreadRadius: 2,
                                blurRadius: 2,
                                color: Colors.grey.withOpacity(.2),
                              )
                            ]),
                        child: Stack(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: InkWell(
                                onTap: () {
                                  if (e.mimeType ==
                                      'application/vnd.google-apps.folder') {
                                    document
                                        .setState((s) => s.addToStack(e.id));
                                    setState(() {});
                                  } else if (e.mimeType == 'upload') {
                                    callback(document.state.listDocSchool.last);
                                  } else {
                                    context.push<void>(WebViewHome(
                                      url: e.webViewLink,
                                      title: e.name,
                                    ));
                                  }
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      e.mimeType ==
                                              'application/vnd.google-apps.folder'
                                          ? Icons.folder
                                          : e.mimeType == 'upload'
                                              ? Icons.upload_rounded
                                              : Icons.file_copy_outlined,
                                      color: SiswamediaTheme.green,
                                      size: 40,
                                    ),
                                    HeightSpace(12),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 8.0),
                                      child: SimpleText(e.name,
                                          textAlign: TextAlign.center,
                                          style: SiswamediaTextStyle.subtitle),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            if (e.mimeType != 'upload' &&
                                auth.state.currentState.isAdmin)
                              Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                    onPressed: () {
                                      showDelete(context, e.id);
                                    },
                                    icon: Icon(
                                      Icons.restore_from_trash,
                                      color: SiswamediaTheme.red,
                                    )),
                              )
                          ],
                        ),
                      ))
                  .toList(),
            );
          }
          return Center(child: CircularProgressIndicator());
        });
    return GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 1,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        padding: EdgeInsets.symmetric(horizontal: 10),
        shrinkWrap: true,
        children: listData.keys
            .map((e) => Container(
                  margin: EdgeInsets.symmetric(vertical: S.w * .02),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 0),
                          spreadRadius: 2,
                          blurRadius: 2,
                          color: Colors.grey.withOpacity(.2),
                        )
                      ]),
                  child: ExpansionTile(
                      title: Text(
                        e.name,
                        style: semiBlack.copyWith(fontSize: S.w / 24),
                      ),
                      children: [
                        if (listData[e] != null && listData[e].isNotEmpty)
                          Divider(color: Colors.grey),

                        ///list of file base on the certain folder
                        Column(
                          children: (listData[e] ?? <Files>[])
                              .map((f) => ListTile(
                                    onTap: () => context.push<void>(WebViewHome(
                                      url: f.webViewLink,
                                      title: f.name,
                                    )),
                                    leading: Icon(Icons.file_copy_outlined,
                                        color: SiswamediaTheme.green),
                                    title: Text(f.name.toString()),
                                    trailing: InkWell(
                                      onTap: () async {
                                        showDialog<void>(
                                            context: context,
                                            builder: (_) {
                                              return Dialog(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 15),
                                                  width: 240,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: <Widget>[
                                                      SimpleText(
                                                        'Hapus Dokumen?',
                                                        style:
                                                            SiswamediaTextStyle
                                                                .title,
                                                        fontSize: 16,
                                                      ),
                                                      HeightSpace(25),
                                                      Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child:
                                                                AutoLayoutButton(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          5,
                                                                      vertical:
                                                                          4),
                                                              text: 'No',
                                                              onTap: () =>
                                                                  context.pop(),
                                                              textColor:
                                                                  SiswamediaTheme
                                                                      .white,
                                                              backGroundColor:
                                                                  SiswamediaTheme
                                                                      .nearBlack,
                                                              radius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          6),
                                                            ),
                                                          ),
                                                          WidthSpace(10),
                                                          Expanded(
                                                            flex: 1,
                                                            child:
                                                                AutoLayoutButton(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          5,
                                                                      vertical:
                                                                          4),
                                                              text: 'Yes',
                                                              onTap: () async =>
                                                                  await GoogleDrive
                                                                      .deleteFile(
                                                                          id: f
                                                                              .id),
                                                              textColor:
                                                                  SiswamediaTheme
                                                                      .white,
                                                              backGroundColor:
                                                                  SiswamediaTheme
                                                                      .green,
                                                              radius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          6),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            });
                                      },
                                      child: Icon(
                                        Icons.restore_from_trash_outlined,
                                        color: SiswamediaTheme.red,
                                      ),
                                    ),
                                  ))
                              .toList(),
                        ),

                        ///if role is GURU or WALIKELAS button upload file to certain folder will appear
                        if (auth.state.currentState.schoolRole
                                .contains('GURU') ||
                            auth.state.currentState.schoolRole
                                .contains('WALIKELAS'))
                          ListTile(
                            onTap: () {
                              // callback(e, e.document);
                            },
                            leading:
                                Icon(Icons.add, color: SiswamediaTheme.green),
                            title: Text('Upload File'),
                          )
                      ] //List.generate(document.data[e].length + 2,
                      ),
                ))
            .toList());
  }

  void callback(String parent) {
    if (auth.state.currentState.schoolRole.contains('GURU') ||
        auth.state.currentState.schoolRole.contains('WALIKELAS')) {
      showDialog<Widget>(
          context: context,
          builder: (context) => StatefulBuilder(
                builder: (BuildContext context,
                    void Function(void Function()) setState) {
                  return Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    child: Container(
                      height: S.w * .6,
                      width: S.w * .7,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //
                          Text('Unggah Dokumen',
                              style: TextStyle(
                                  fontSize: S.w / 26,
                                  fontWeight: FontWeight.w600)),
                          Text('Pilih dokumen yang ingin Anda unggah melalui',
                              style: TextStyle(
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: Color(0xff4B4B4B),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(parent);
                                // await Kiki.getFile(model, context);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('Google Drive',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(parent);
                                // await Kiki.getFile(model, context);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('File Manager',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ));
    }
  }
}
