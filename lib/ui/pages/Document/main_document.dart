part of '_document.dart';

class MainDocument extends StatefulWidget {
  @override
  _MainDocumentState createState() => _MainDocumentState();
}

class _MainDocumentState extends State<MainDocument> {
  final documentState = Injector.getAsReactive<DocumentState>();
  final authState = Injector.getAsReactive<AuthState>();
  var refreshIndicator = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    Initializer(
      reactiveModel: documentState,
      cacheKey: TIME_DOCUMENTS,
      rIndicator: refreshIndicator,
      state: documentState.state.schoolDocuments != null,
    )..initialize();
    // WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
    //   await fetchItems();
    // });
  }

  @override
  void dispose() {
    super.dispose();
    // documentState.refresh();
  }

  // Future<void> fetchItems() async {
  //   var document = Provider.of<DocumentProvider>(context, listen: false);
  //   var auth = context.read<UserProvider>();
  //   print('fetch');
  //   print(await document.data.then((value) => value == null));
  //   // if (await document.data.then((value) => value == null)) {
  //   print('asd');
  //   await document.fetchItems(auth.currentState.schoolId);
  //   // }
  // }

  Future<void> retrieveData() async {
    var schoolId = authState.state.currentState.schoolId;
    await documentState.setState((s) => s.retrieveClassDocument(schoolId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shadowColor: Colors.grey.withOpacity(.2),
        brightness: Brightness.light,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Dokumen',
            style: TextStyle(
                color: Colors.black,
                fontSize: S.w / 22,
                fontWeight: FontWeight.w600)),
      ),
      body: StateBuilder<DocumentState>(
        observe: () => documentState,
        builder: (_, snapshot) {
          return RefreshIndicator(
            key: refreshIndicator,
            onRefresh: () async => retrieveData(),
            child: WhenRebuilder<DocumentState>(
              observe: () => snapshot,
              onWaiting: () => WaitingView(),
              onIdle: () => WaitingView(),
              onError: (dynamic error) => ErrorView(error: error),
              onData: (data) {
                if (GlobalState.classes().state.classMap.isEmpty) {
                  return ListView(
                    padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                    children: [
                      SizedBox(height: MediaQuery.of(context).size.height * .2),
                      Image.asset(
                        'assets/icons/dokumen/keperluan.png',
                        height: S.w / 2.2,
                        width: S.w / 2.2,
                        fit: BoxFit.contain,
                      ),
                      SizedBox(height: 15),
                      documentSchoolEmpty(),
                    ],
                  );
                }
                return ListView(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                  children: [
                    Image.asset(
                      'assets/icons/dokumen/keperluan.png',
                      height: S.w / 2.2,
                      width: S.w / 2.2,
                      fit: BoxFit.contain,
                    ),
                    Container(
                      width: S.w,
                      child: Text(
                        'Disini Anda dapat menemukan keperluan dokumen sesuai dengan kelas yang Anda ikuti, untuk mengakses pastikan sudah memiliki kelas',
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 15),
                    Divider(color: Colors.grey),
                    SizedBox(height: 15),
                    authState.state.currentState.type == 'Personal'
                        ? Text(
                            'Oops kelas Personal tidak dapat mengakses fitur ini',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: S.w / 30,
                                fontWeight: FontWeight.w700,
                                color: Colors.black87))
                        : documentSchoolList(
                            width: S.w,
                            context: context,
                          )
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget documentSchoolList({double width, BuildContext context}) {
    return Column(
        children: GlobalState.classes()
            .state
            .classMap
            .values
            .where((element) =>
                element.schoolId == authState.state.currentState.schoolId)
            .map((e) => Container(
                  width: width * .9,
                  height: width * .15,
                  margin: EdgeInsets.symmetric(
                      horizontal: width * .03, vertical: width * .02),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 0),
                          spreadRadius: 2,
                          blurRadius: 2,
                          color: Colors.grey.withOpacity(.2))
                    ],
                    color: Colors.white,
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        print(e.id);
                        Navigator.push(
                            context,
                            MaterialPageRoute<void>(
                                builder: (_) => TypeDocument(classId: e.id)));
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * .05),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              Icon(
                                Icons.animation,
                                color: SiswamediaTheme.green,
                              ),
                              SizedBox(width: 15),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: e.name,
                                        style: TextStyle(
                                            fontSize: width / 30,
                                            color: Colors.grey[500])),
                                    TextSpan(
                                        text: ' (${e.jurusan})',
                                        style: TextStyle(
                                            fontSize: width / 30,
                                            fontWeight: FontWeight.w700,
                                            color: SiswamediaTheme.green)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ))
            .toList());
  }

  Widget documentSchoolEmpty() {
    return Center(child: Text('Anda belum terdaftar di kelas manapun'));
  }
}

class DocumentData {
  static List<String> documentType = ['sekolah', 'kelas'];
}
