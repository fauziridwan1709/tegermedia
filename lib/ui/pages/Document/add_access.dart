part of '_document.dart';

class AddAccess extends StatefulWidget {
  AddAccess({this.parent});
  final String parent;
  @override
  _AddAccessState createState() => _AddAccessState();
}

class _AddAccessState extends State<AddAccess> {
  final TextEditingController _title = TextEditingController();
  FocusNode fNode = FocusNode();

  var scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (fNode.hasFocus) {
          fNode.unfocus();
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        key: scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          brightness: Brightness.light,
          shadowColor: Colors.grey.withOpacity(.2),
          leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Tambah akses guru',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: S.w / 22,
                  fontWeight: FontWeight.w600)),
        ),
        body: GestureDetector(
          onTap: () {
            if (fNode.hasFocus) {
              fNode.unfocus();
            }
          },
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: S.w * .05),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: S.w * .19),
                  Container(
                      child: Image.asset(
                    'assets/icons/dokumen/keperluan.png',
                    height: S.w / 1.5,
                    width: S.w / 1.5,
                  )),
                  Text(
                    'Masukkan email guru yang ingin diberikan akses untuk mengelola folder google drive',
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 20),
                  Container(
                      width: S.w * .85,
                      child: TextField(
                        controller: _title,
                        focusNode: fNode,
                        decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.email_outlined,
                              color: fNode.hasFocus
                                  ? SiswamediaTheme.green
                                  : Colors.grey,
                            ),
                            filled: true,
                            fillColor: Colors.transparent,
                            focusColor: SiswamediaTheme.green,
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                    color: Colors.grey.withOpacity(.8))),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide:
                                    BorderSide(color: SiswamediaTheme.green)),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide:
                                    BorderSide(color: SiswamediaTheme.green)),
                            hintText: 'email guru',
                            hintStyle: descBlack.copyWith(
                                fontSize: S.w / 26, color: Colors.grey)),
                      )),
                  SizedBox(height: 30),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: S.w * .85,
                        height: S.w * .125,
                        decoration: BoxDecoration(
                          color: SiswamediaTheme.green,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () async {
                              fNode.unfocus();
                              if (_title.text != '') {
                                final reqBody2 = <String, dynamic>{
                                  'role': 'writer',
                                  'type': 'user',
                                  'emailAddress': _title.text
                                };
                                await GoogleDrive.addPermissions(
                                        context: context,
                                        reqBody: reqBody2,
                                        parent: widget.parent)
                                    .then((value) {
                                  if (value.statusCode == 200) {
                                    Navigator.pop(context);
                                    CustomFlushBar.successFlushBar(
                                      'Berhasil Memberikan akses ke ${_title.text}',
                                      context,
                                    );
                                  } else {
                                    Navigator.pop(context);
                                    CustomFlushBar.errorFlushBar(
                                      'Gagal terhubung ke google drive!',
                                      context,
                                    );
                                  }
                                });
                              } else {
                                CustomFlushBar.errorFlushBar(
                                  'Email Kosong...',
                                  context,
                                );
                              }
                            },
                            child: Center(
                              child: Text('Tambah',
                                  style: TextStyle(
                                      fontSize: S.w / 26,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white)),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: S.w * .9,
                        height: S.w * .12,
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Center(
                              child: Text('Batal',
                                  style: TextStyle(
                                      fontSize: S.w / 26,
                                      fontWeight: FontWeight.w700,
                                      color: SiswamediaTheme.green)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: S.w * .3),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
