part of '_document.dart';

class SchoolDocument extends StatefulWidget {
  SchoolDocument({this.classId, this.type, this.onUpload});

  final String type;
  final int classId;
  final Function(DocumentModel model) onUpload;

  @override
  _SchoolDocumentState createState() => _SchoolDocumentState();
}

class _SchoolDocumentState extends State<SchoolDocument> {
  final documentState = Injector.getAsReactive<DocumentState>();
  final authState = Injector.getAsReactive<AuthState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var data = documentState.state;
    var listData = data.schoolDocuments;
    Utils.log(listData, info: 'list data');
    if (listData?.keys?.isEmpty ?? true) {
      return Column(
        children: [
          SizedBox(height: 100),
          Image.asset(
            'assets/icons/dokumen/empty.png',
            height: S.w * .4,
            width: S.w * .4,
          ),
          Text('Belum ada dokumen',
              style: TextStyle(color: SiswamediaTheme.green)),
        ],
      );
    }
    return Column(
        children: listData.keys
            .map((e) => Container(
                  margin: EdgeInsets.symmetric(vertical: S.w * .02),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 0),
                          spreadRadius: 2,
                          blurRadius: 2,
                          color: Colors.grey.withOpacity(.2),
                        )
                      ]),
                  child: ExpansionTile(
                      title: Text(
                        e.name,
                        style: semiBlack.copyWith(fontSize: S.w / 24),
                      ),
                      children: [
                        if (listData[e] != null && listData[e].isNotEmpty)
                          Divider(color: Colors.grey),

                        ///list of file base on the certain folder
                        Column(
                          children: (listData[e] ?? <Files>[])
                              .map((f) => ListTile(
                                    onTap: () => context.push<void>(WebViewHome(
                                      url: f.webViewLink,
                                      title: f.name,
                                    )),
                                    leading: Icon(Icons.file_copy_outlined,
                                        color: SiswamediaTheme.green),
                                    title: Text(f.name.toString()),
                                    trailing: InkWell(
                                      onTap: () async {
                                        showDialog<void>(
                                            context: context,
                                            builder: (_) {
                                              return Dialog(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 15),
                                                  width: 240,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: <Widget>[
                                                      SimpleText(
                                                        'Hapus Dokumen?',
                                                        style:
                                                            SiswamediaTextStyle
                                                                .title,
                                                        fontSize: 16,
                                                      ),
                                                      HeightSpace(25),
                                                      Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child:
                                                                AutoLayoutButton(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          5,
                                                                      vertical:
                                                                          4),
                                                              text: 'No',
                                                              onTap: () =>
                                                                  context.pop(),
                                                              textColor:
                                                                  SiswamediaTheme
                                                                      .white,
                                                              backGroundColor:
                                                                  SiswamediaTheme
                                                                      .nearBlack,
                                                              radius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          6),
                                                            ),
                                                          ),
                                                          WidthSpace(10),
                                                          Expanded(
                                                            flex: 1,
                                                            child:
                                                                AutoLayoutButton(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          5,
                                                                      vertical:
                                                                          4),
                                                              text: 'Yes',
                                                              onTap: () async =>
                                                                  await GoogleDrive
                                                                      .deleteFile(
                                                                          id: f
                                                                              .id),
                                                              textColor:
                                                                  SiswamediaTheme
                                                                      .white,
                                                              backGroundColor:
                                                                  SiswamediaTheme
                                                                      .green,
                                                              radius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          6),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            });
                                      },
                                      child: Icon(
                                        Icons.restore_from_trash_outlined,
                                        color: SiswamediaTheme.red,
                                      ),
                                    ),
                                  ))
                              .toList(),
                        ),

                        ///if role is GURU or WALIKELAS button upload file to certain folder will appear
                        if (authState.state.currentState.schoolRole
                                .contains('GURU') ||
                            authState.state.currentState.schoolRole
                                .contains('WALIKELAS'))
                          ListTile(
                            onTap: () {
                              callback(e, e.document);
                            },
                            leading:
                                Icon(Icons.add, color: SiswamediaTheme.green),
                            title: Text('Upload File'),
                          )
                      ] //List.generate(document.data[e].length + 2,
                      ),
                ))
            .toList());
  }

  void callback(DocumentModel model, String parent) {
    if (authState.state.currentState.schoolRole.contains('GURU') ||
        authState.state.currentState.schoolRole.contains('WALIKELAS')) {
      showDialog<Widget>(
          context: context,
          builder: (context) => StatefulBuilder(
                builder: (BuildContext context,
                    void Function(void Function()) setState) {
                  return Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    child: Container(
                      height: S.w * .6,
                      width: S.w * .7,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //
                          Text('Unggah Dokumen',
                              style: TextStyle(
                                  fontSize: S.w / 26,
                                  fontWeight: FontWeight.w600)),
                          Text('Pilih dokumen yang ingin Anda unggah melalui',
                              style: TextStyle(
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: Color(0xff4B4B4B),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(model);
                                // await Kiki.getFile(model, context);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('Google Drive',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(model);
                                // await Kiki.getFile(model, context);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('File Manager',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ));
    }
  }
}
