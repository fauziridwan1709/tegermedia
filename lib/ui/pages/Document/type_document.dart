part of '_document.dart';

class TypeDocument extends StatefulWidget {
  TypeDocument({this.classId});
  final int classId;

  @override
  _TypeDocumentState createState() => _TypeDocumentState();
}

class _TypeDocumentState extends State<TypeDocument> {
  final auth = Injector.getAsReactive<AuthState>();
  final document = Injector.getAsReactive<DocumentState>();
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  int currentIndex = 0;

  var _progressValue = 0.0;
  var _progressPercentValue = 0;

  List<String> type = ['sekolah', 'kelas'];

  @override
  void dispose() {
    super.dispose();

    document.setState((s) => s.resetStack());
  }

  Future<void> retrieveData() async {
    var schoolId = auth.state.currentState.schoolId;
    await document.setState((s) => s.retrieveClassDocument(schoolId));
  }

  @override
  Widget build(BuildContext context) {
    // var auth = Provider.of<UserProvider>(context);
    var progress = progressUploadState.state;
    return WillPopScope(
      onWillPop: () {
        //document.reset();
        return Future.value(true);
      },
      child: StateBuilder(
          observe: () => document,
          builder: (_, snapshot) {
            return Scaffold(
              key: scaffoldKey,
              backgroundColor: Colors.white,
              appBar: AppBar(
                brightness: Brightness.light,
                shadowColor: Colors.grey.withOpacity(.2),
                leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: SiswamediaTheme.green,
                    ),
                    onPressed: () {
                      //document.reset();
                      Navigator.pop(context);
                    }),
                actions: [
                  if ((currentIndex == 0 &&
                          document.state.listDocSchool.isNotEmpty) ||
                      (currentIndex == 1 &&
                          document.state.listDocClass.isNotEmpty))
                    IconButton(
                      icon: Icon(Icons.subdirectory_arrow_left),
                      color: SiswamediaTheme.green,
                      onPressed: () {
                        if (currentIndex == 0) {
                          document.setState(
                              (s) => s.removeFromStack(type: 'school'));
                        } else {
                          document.setState(
                              (s) => s.removeFromStack(type: 'class'));
                        }
                      },
                    )
                ],
                centerTitle: true,
                backgroundColor: Colors.white,
                title: Text('Dokumen',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: S.w / 22,
                        fontWeight: FontWeight.w600)),
              ),
              body: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        width: S.w,
                        child: StateBuilder<DocumentState>(
                            observe: () => document,
                            builder: (_, __) {
                              return RefreshIndicator(
                                onRefresh: retrieveData,
                                key: refreshIndicatorKey,
                                child: WhenRebuilder<DocumentState>(
                                    observe: () => document,
                                    onWaiting: () => WaitingView(),
                                    onIdle: () => WaitingView(),
                                    onError: (dynamic error) =>
                                        ErrorView(error: error),
                                    onData: (data) {
                                      return ListView(
                                        // crossAxisAlignment: CrossAxisAlignment.center,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: S.w * .05),
                                        children: [
                                          Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: listTypeDocument
                                                  .map((e) => Container(
                                                        width: S.w * .42,
                                                        height: S.w * .1,
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                horizontal:
                                                                    S.w * .01,
                                                                vertical:
                                                                    S.w * .02),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      S.w),
                                                          border: Border.all(
                                                            color: currentIndex ==
                                                                    listTypeDocument
                                                                        .indexOf(
                                                                            e)
                                                                ? Colors
                                                                    .transparent
                                                                : Color(
                                                                    0xffE6E6E6),
                                                          ),
                                                          color: currentIndex ==
                                                                  listTypeDocument
                                                                      .indexOf(
                                                                          e)
                                                              ? SiswamediaTheme
                                                                  .green
                                                              : Colors
                                                                  .transparent,
                                                        ),
                                                        child: InkWell(
                                                          onTap: () =>
                                                              _selectyaaa(
                                                                  listTypeDocument
                                                                      .indexOf(
                                                                          e)),
                                                          child: Center(
                                                              child: Text(e,
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      fontSize:
                                                                          S.w /
                                                                              30,
                                                                      color: currentIndex ==
                                                                              listTypeDocument.indexOf(
                                                                                  e)
                                                                          ? Colors
                                                                              .white
                                                                          : Colors
                                                                              .black))),
                                                        ),
                                                      ))
                                                  .toList()),
                                          [
                                            SchoolDocumentv2(
                                                onUpload: (parent) {
                                                  Navigator.pop(context);
                                                  uploadFile(
                                                      _setUploadProgress,
                                                      context,
                                                      parent,
                                                      'sekolah');
                                                },
                                                classId: widget.classId,
                                                type: 'sekolah'),
                                            ClassDocumentv2(
                                                onUpload: (parent) {
                                                  Navigator.pop(context);
                                                  uploadFile(_setUploadProgress,
                                                      context, parent, 'kelas');
                                                },
                                                classId: widget.classId,
                                                type: 'kelas')
                                          ].elementAt(currentIndex),
                                          SizedBox(height: 15),
                                          if (auth.state.currentState.schoolRole
                                                  .contains('GURU') ||
                                              auth.state.currentState.schoolRole
                                                  .contains('WALIKELAS'))
                                            Container(
                                                height: S.w * .12,
                                                width: S.w * .9,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  border: Border.all(
                                                      color: Color(0xffD9D9D9)),
                                                  color: Colors.white,
                                                ),
                                                child: InkWell(
                                                  onTap: () => context.push<
                                                          void>(
                                                      CreateDocumentFolder(
                                                          classId:
                                                              widget.classId,
                                                          parent: getParent(),
                                                          type: DocumentData
                                                                  .documentType[
                                                              currentIndex])),
                                                  child: Center(
                                                    child: Container(
                                                      height: S.w * .08,
                                                      width: S.w * .08,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            Color(0xff4B4B4B),
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(Icons.add,
                                                          color: SiswamediaTheme
                                                              .green),
                                                    ),
                                                  ),
                                                )),
                                          SizedBox(height: 20),
                                          if (auth.state.currentState.schoolRole
                                                  .contains('GURU') ||
                                              auth.state.currentState.schoolRole
                                                  .contains('WALIKELAS'))
                                            StreamBuilder<
                                                    Map<DocumentModel,
                                                        List<Files>>>(
                                                // stream: document.data.asStream(),
                                                builder: (context, snapshot) {
                                              return Container(
                                                  height: S.w * .12,
                                                  width: S.w * .9,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                    border: Border.all(
                                                        color:
                                                            Color(0xffD9D9D9)),
                                                    color: Colors.white,
                                                  ),
                                                  child: InkWell(
                                                    onTap: () {
                                                      var val =
                                                          type[currentIndex] ==
                                                                  'sekolah'
                                                              ? document.state
                                                                  .schoolDoc
                                                              : document.state
                                                                      .classDoc[
                                                                  widget
                                                                      .classId];
                                                      if (val == null) {
                                                        CustomFlushBar
                                                            .errorFlushBar(
                                                                'Buat folder ${type[currentIndex]} terlebih dahulu!',
                                                                context);
                                                      } else {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute<
                                                                    void>(
                                                                builder: (_) =>
                                                                    AddAccess(
                                                                        parent:
                                                                            val)));
                                                      }
                                                    },
                                                    child: Center(
                                                      child: Text(
                                                        'tambah izin akses ${type[currentIndex]} untuk guru',
                                                        style: descWhite.copyWith(
                                                            color:
                                                                SiswamediaTheme
                                                                    .green,
                                                            fontSize: S.w / 26),
                                                      ),
                                                    ),
                                                  ));
                                            }),
                                          HeightSpace(20)
                                        ],
                                      );
                                    }),
                              );
                            })),
                  ),
                  if (progress.progress == 0)
                    Container()
                  else
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: StateBuilder<ProgressUploadState>(
                        observe: () => progressUploadState,
                        builder: (context, data) {
                          if (progress.progress == 0) {
                            return Container();
                          }
                          return Container(
                              width: S.w,
                              height: 65,
                              padding: EdgeInsets.only(bottom: 5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey[300]),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: SiswamediaTheme.grey
                                            .withOpacity(0.2),
                                        offset: const Offset(0, -4),
                                        blurRadius: 3.5,
                                        spreadRadius: 2),
                                  ],
                                  color: Colors.white),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    LinearPercentIndicator(
                                      padding: EdgeInsets.all(0),
                                      lineHeight: 8.0,
                                      percent: _progressValue,
                                      backgroundColor: Colors.grey[300],
                                      progressColor: SiswamediaTheme.green,
                                    ),
                                    SizedBox(height: 10),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: S.w * .05),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                                'Uploading... $_progressPercentValue%'),
                                            Text(
                                              '${Format.formatByte(progress.progress)}/${Format.formatByte(progress.contentLength)}',
                                              textAlign: TextAlign.right,
                                            ),
                                          ],
                                        )),
                                  ]));
                        },
                      ),
                    ),
                ],
              ),
            );
          }),
    );
  }

  String getParent() {
    if (currentIndex == 0) {
      if (document.state.listDocSchool.isEmpty) {
        return document.state.schoolDoc;
      } else if (document.state.schoolDoc == null) {
        return null;
      } else {
        return document.state.listDocSchool.last;
      }
    } else {
      if (document.state.listDocClass.isEmpty) {
        return document.state.classDoc[widget.classId];
      } else if (document.state.classDoc[widget.classId] == null) {
        return null;
      } else {
        return document.state.listDocClass.last;
      }
    }
  }

  void _selectyaaa(int i) {
    setState(() {
      currentIndex = i;
    });
  }

  Future<void> uploadFile(OnUploadProgressCallback onUploadProgress,
      BuildContext context, String parent, String documentType) async {
    // var provider = Provider.of<ProgressProvider>(context, listen: false);
    try {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context,
          builder: (_) {
            return Center(child: CircularProgressIndicator());
          });
      var pick = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.any,
      );
      print('aduh');
      var file = pick.files.first;
      print(file.name);
      print(file.path);

      var client2 = http.Client();
      //todo file!=null
      if (file != null) {
        var totalByteLength = File(file.path).lengthSync();
        var type = lookupMimeType(file.path);
        SiswamediaGoogleSignIn.getInstance().scopes.addAll([
          'https://www.googleapis.com/auth/drive.file',
        ]);
        await SiswamediaGoogleSignIn.getInstance().signInSilently();
        var headers =
            await SiswamediaGoogleSignIn.getInstance().currentUser.authHeaders;
        var requestHeaders = <String, String>{
          'X-Upload-Content-Type': type,
          'Content-Type': 'application/json',
          'X-Upload-Content-Length': '$totalByteLength',
        };
        headers.forEach((key, value) {
          requestHeaders[key] = value;
        });
        var resp = await client2.post(
            '$baseGoogleApiUrl/upload/drive/v3/files?uploadType=resumable',
            body: json.encode(<String, int>{
              'Content-Length': totalByteLength,
            }),
            headers: requestHeaders);
        print('response');
        print(resp.statusCode);
        print(resp.headers);
        //3145727
        print(file.size);
        // if (file.size > 1048576) {
        //   throw LargeFileException(cause: 'Data lebih dari 1GB');
        // }

        final client = HttpClient();
        final response =
            await client.putUrl(Uri.parse(resp.headers['location']));

        var byteCount = 0;

        //todo
        // response.contentLength = File(file.path).lengthSync();
        response.contentLength = totalByteLength;
        print('content lenght ${response.contentLength}');
        response.headers.set(HttpHeaders.contentTypeHeader, type);
        response.headers.set(HttpHeaders.contentLengthHeader, totalByteLength);
        response.headers.set(
            'Content-Range', 'bytes 0-${totalByteLength - 1}/$totalByteLength');

        headers.forEach((key, value) {
          response.headers.add(key, value);
        });

        final stream = await File(file.path).openRead();

        Navigator.pop(context);
        var stream2 = stream.transform<List<int>>(
          StreamTransformer.fromHandlers(
            handleData: (data, sink) {
              byteCount += data.length;
              print(byteCount);
              sink.add(data);
              if (onUploadProgress != null) {
                print('uploading');
                onUploadProgress(byteCount, totalByteLength, context);
                // CALL STATUS CALLBACK;
              }
            },
            handleError: (error, stack, sink) {},
            handleDone: (sink) {
              sink.close();
            },
          ),
        );

        print('masuk');

        await response.addStream(stream2);

        final httpResponse = await response.close();
        var statusCode = httpResponse.statusCode;

        print(statusCode);
        if (statusCode ~/ 100 != 2) {
          print(statusCode);
          throw Exception(
              'Error uploading file, Status code: ${httpResponse.statusCode}');
        } else {
          return await readResponseAsString(
              httpResponse, file.name, headers, parent, documentType);
        }
        // return response;
      } else {
        await progressUploadState.setState((s) => s.setProgress(0));
        // return null;
      }
    } on LargeFileException catch (error) {
      await progressUploadState.setState((s) => s.setProgress(0));
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } on ErrorUploadException catch (error) {
      await progressUploadState.setState((s) => s.setProgress(0));
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } catch (e) {
      Navigator.pop(context);
      print(e);
    }
  }

  Future<String> readResponseAsString(
      HttpClientResponse response,
      String filename,
      Map<String, String> headers,
      String parent,
      String type) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
    }, onDone: () async {
      try {
        completer.complete(contents.toString());
        var result = await completer.future;
        var data = json.decode(result) as Map<String, dynamic>;
        var client = http.Client();
        print(data['id']);

        ///case ketika udah upload
        ///create data di firebase 1 aja, buat folder sekolah
        ///folder kelas 1 aja juga isinya si folder id.
        ///[folderA => folderB]
        var resp = await client.patch(
            '$baseGoogleApiUrl/drive/v3/files/${data['id']}?addParents=${parent}&fields=*',
            body: json.encode({'name': filename}),
            headers: headers);

        //todo log resp. body
        await client
            .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
                body: json.encode({'role': 'reader', 'type': 'anyone'}),
                headers: headers)
            .then((value) {
          print(value.body);
          var data = json.decode(resp.body) as Map<String, dynamic>;
          var file = Files(
              mimeType: data['mimeType'],
              name: filename,
              webViewLink: data['webViewLink']);
          document
              .setState((s) => s.addFile(widget.classId, parent, file, type));
          CustomFlushBar.successFlushBar(
            'Berhasil Mengupload Lampiran, jika mengupload video Kami butuh waktu untuk memproses vido anda silahkan tunggu hingga video dapat ditampilkan',
            context,
          );
          progressUploadState.setState((s) => s.setProgress(0), silent: true);
        });
      } catch (e) {
        print('error');
        print(e);
      }
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes), silent: true);
    progressUploadState.setState((s) => s.setContent(totalBytes), silent: true);
    var __progressValue =
        Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }
}

const List<String> listTypeDocument = <String>[
  'Dokumen Sekolah',
  'Dokumen Kelas'
];
