part of '_document.dart';

class CreateDocumentFolder extends StatefulWidget {
  CreateDocumentFolder({this.type, this.classId, this.parent});

  final String type;
  final String parent;
  final int classId;

  @override
  _CreateDocumentFolderState createState() => _CreateDocumentFolderState();
}

class _CreateDocumentFolderState extends State<CreateDocumentFolder> {
  final document = Injector.getAsReactive<DocumentState>();
  final auth = Injector.getAsReactive<AuthState>();
  final TextEditingController _title = TextEditingController();
  var scaffoldKey = GlobalKey<ScaffoldState>();

  FocusNode fNode = FocusNode();
  int _radioValue1 = 0;
  int _radioValue2 = 0;

  String choose1;

  final fireStoreInstance = FirebaseFirestore.instance;

  Future<void> createInstance({DocumentModel data}) async {
    await fireStoreInstance.collection('document').add(<String, dynamic>{
      'class_id': data.classId,
      'document_list': data.document,
      'name': data.name,
      'type': data.type,
      'school_id': data.schoolId,
      'parent': data.parent
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (fNode.hasFocus) {
          fNode.unfocus();
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
      child: Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          brightness: Brightness.light,
          shadowColor: Colors.grey.withOpacity(.2),
          leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Buat Folder Dokumen',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: S.w / 22,
                  fontWeight: FontWeight.w600)),
        ),
        body: GestureDetector(
          onTap: () {
            if (fNode.hasFocus) {
              fNode.unfocus();
            }
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: S.w * .05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        height: S.w * .13,
                        width: S.w * .9,
                        child: TextField(
                          controller: _title,
                          focusNode: fNode,
                          decoration: InputDecoration(
                              hintText: 'Nama Folder',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: SiswamediaTheme.green))),
                        )),
                    SizedBox(height: 20),
                    Text('Siapa yang boleh Unggah',
                        style: TextStyle(
                            fontSize: S.w / 25,
                            color: Colors.black87,
                            fontWeight: FontWeight.w600)),
                    SizedBox(height: 15),
                    Container(
                      height: 35,
                      child: Row(
                        children: [
                          Radio(
                            value: 0,
                            activeColor: SiswamediaTheme.green,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          Text('Semua pengguna diperbolehkan',
                              style: TextStyle(
                                fontSize: S.w / 30,
                                color: Colors.black87,
                              )),
                        ],
                      ),
                    ),
                    Container(
                      height: 35,
                      child: Row(
                        children: [
                          Radio(
                            value: 1,
                            activeColor: SiswamediaTheme.green,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          Text('Hanya Admin',
                              style: TextStyle(
                                fontSize: S.w / 30,
                                color: Colors.black87,
                              )),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    // Text('Siapa yang boleh Unduh',
                    //     style: TextStyle(
                    //         fontSize: S.w / 25,
                    //         color: Colors.black87,
                    //         fontWeight: FontWeight.w600)),
                    // SizedBox(height: 15),
                    // Container(
                    //   height: 35,
                    //   child: Row(
                    //     children: [
                    //       Radio(
                    //         value: 0,
                    //         activeColor: SiswamediaTheme.green,
                    //         groupValue: _radioValue2,
                    //         onChanged: _handleRadioValueChange2,
                    //       ),
                    //       Text('Semua pengguna diperbolehkan',
                    //           style: TextStyle(
                    //             fontSize: S.w / 30,
                    //             color: Colors.black87,
                    //           )),
                    //     ],
                    //   ),
                    // ),
                    // Container(
                    //   height: 35,
                    //   child: Row(
                    //     children: [
                    //       Radio(
                    //         value: 1,
                    //         activeColor: SiswamediaTheme.green,
                    //         groupValue: _radioValue2,
                    //         onChanged: _handleRadioValueChange2,
                    //       ),
                    //       Text('Hanya Admin',
                    //           style: TextStyle(
                    //             fontSize: S.w / 30,
                    //             color: Colors.black87,
                    //           )),
                    //     ],
                    //   ),
                    // ),
                    SizedBox(
                      height: 10,
                    ),
                    // if (widget.type == 'kelas')
                    //   Column(
                    //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     children: [
                    //       Text('Untuk Kelas',
                    //           style: TextStyle(
                    //               fontSize: S.w / 25,
                    //               color: Colors.black87,
                    //               fontWeight: FontWeight.w600)),
                    //       SizedBox(height: 15),
                    //       Padding(
                    //         padding: EdgeInsets.symmetric(vertical: S.w * .03),
                    //         child: Container(
                    //           width: S.w * .65,
                    //           height: S.w * .1,
                    //           padding:
                    //               EdgeInsets.symmetric(horizontal: S.w * .05),
                    //           decoration: BoxDecoration(
                    //             border: Border.all(color: Colors.grey),
                    //             borderRadius: BorderRadius.circular(S.w),
                    //           ),
                    //           child: DropdownButtonHideUnderline(
                    //             child: DropdownButton(
                    //                 icon: Icon(
                    //                   Icons.keyboard_arrow_down,
                    //                   color: SiswamediaTheme.green,
                    //                 ),
                    //                 hint: Text(
                    //                   'Pilih Kelas',
                    //                   style: TextStyle(fontSize: S.w / 30),
                    //                 ),
                    //                 value: choose1,
                    //                 onChanged: (String value) {
                    //                   setState(() {
                    //                     choose1 = value;
                    //                   });
                    //                 },
                    //                 items: [
                    //                   DropdownMenuItem(
                    //                     value: 'no Data',
                    //                     child: SizedBox(
                    //                         width: S.w * .3,
                    //                         child: Text('Pilih Kelas')),
                    //                   ),
                    //                   for (final e in GlobalState.classes()
                    //                       .state
                    //                       .classMap
                    //                       .values
                    //                       .where((element) =>
                    //                           element.schoolId ==
                    //                           auth.state.currentState.schoolId))
                    //                     DropdownMenuItem(
                    //                       value: '${e.id}',
                    //                       child: SizedBox(
                    //                           width: S.w * .45,
                    //                           child: Text(e.name)),
                    //                     )
                    //                 ]),
                    //           ),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: S.w * .9,
                      height: S.w * .12,
                      decoration: BoxDecoration(
                        color: SiswamediaTheme.green,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () async {
                            String parent = widget.parent;
                            // if (widget.type == 'sekolah') {
                            //   parent = document.state.li.keys.first.parent;
                            // } else {
                            //   parent =
                            //       document.state.classDoc[int.parse(choose1)];
                            // }
                            var isParent = widget.parent != null;

                            print(isParent);
                            if (_title.text != '') {
                              SiswamediaTheme.infoLoading(
                                  context: context,
                                  info: 'Sedang membuat folder');
                              Map<String, dynamic> reqBody;
                              if (isParent) {
                                reqBody = <String, dynamic>{
                                  'name': _title.text,
                                  'mimeType':
                                      'application/vnd.google-apps.folder',
                                  'parents': [parent]
                                };
                              } else {
                                reqBody = <String, dynamic>{
                                  'name': _title.text,
                                  'mimeType':
                                      'application/vnd.google-apps.folder'
                                };
                              }

                              final reqBody2 = <String, dynamic>{
                                'role': _radioValue1 == 0 ? 'writer' : 'reader',
                                'type': 'anyone'
                              };
                              await GoogleDrive.createDriveFolder(
                                      context: context,
                                      reqBody: reqBody,
                                      reqBody2: reqBody2)
                                  .then((value) async {
                                if (value.statusCode == 200) {
                                  if (isParent) {
                                    var documentModel = DocumentModel(
                                      schoolId:
                                          auth.state.currentState.schoolId,
                                      classId: widget.type == 'kelas'
                                          ? widget.classId
                                          : -1,
                                      document: value.id,
                                      parent: parent,
                                      name: _title.text,
                                      type: widget.type,
                                    );
                                    await createInstance(data: documentModel);
                                    await document.setState((s) => s.addFolder(
                                        widget.type == 'sekolah'
                                            ? 0
                                            : widget.classId,
                                        widget.type,
                                        documentModel));
                                    Navigator.pop(context);
                                    CustomFlushBar.successFlushBar(
                                      'Folder dengan nama ${_title.text} berhasil dibuat!',
                                      context,
                                    );
                                  } else {
                                    final reqP = <String, dynamic>{
                                      'name': _title.text,
                                      'mimeType':
                                          'application/vnd.google-apps.folder',
                                      'parents': [value.id.toString()]
                                    };
                                    await GoogleDrive.createDriveFolder(
                                      context: context,
                                      reqBody: reqP,
                                      reqBody2: reqBody2,
                                    ).then((value2) async {
                                      if (value2.statusCode == 200) {
                                        var documentModel = DocumentModel(
                                          schoolId:
                                              auth.state.currentState.schoolId,
                                          classId: widget.type == 'kelas'
                                              ? widget.classId
                                              : -1,
                                          document: value2.id,
                                          parent: value.id,
                                          name: _title.text,
                                          type: widget.type,
                                        );
                                        await createInstance(
                                            data: documentModel);
                                        await document.setState((s) =>
                                            s.addFolder(widget.classId,
                                                widget.type, documentModel));
                                        Navigator.pop(context);
                                        CustomFlushBar.successFlushBar(
                                          'Berhasil membuat folder',
                                          context,
                                        );
                                      } else {
                                        Navigator.pop(context);
                                        CustomFlushBar.errorFlushBar(
                                          'Terjadi kesalahan, coba ulang',
                                          context,
                                        );
                                      }
                                    });
                                  }
                                } else if (value.statusCode ==
                                    StatusCodeConst.timeout) {
                                  Navigator.pop(context);
                                  CustomFlushBar.errorFlushBar(
                                    'Timeout, try again',
                                    context,
                                  );
                                } else {
                                  Navigator.pop(context);
                                  CustomFlushBar.errorFlushBar(
                                    'Anda sebagai guru belum punya akses di folder ini',
                                    context,
                                  );
                                }
                              });
                            } else {
                              CustomFlushBar.errorFlushBar(
                                'Nama folder tidak boleh kosong',
                                context,
                              );
                            }
                          },
                          child: Center(
                            child: Text('Buat Folder',
                                style: TextStyle(
                                    fontSize: S.w / 30,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white)),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: S.w * .9,
                      height: S.w * .12,
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Text('Batal',
                                style: TextStyle(
                                    fontSize: S.w / 30,
                                    fontWeight: FontWeight.w700,
                                    color: SiswamediaTheme.green)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;
    });
  }
}
