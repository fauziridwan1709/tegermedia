part of '_document.dart';

class ClassDocument extends StatefulWidget {
  ClassDocument({this.classId, this.type, this.onUpload});

  final String type;
  final int classId;
  final Function(DocumentModel model) onUpload;
  @override
  _ClassDocumentState createState() => _ClassDocumentState();
}

class _ClassDocumentState extends State<ClassDocument> {
  final documentState = Injector.getAsReactive<DocumentState>();
  final authState = Injector.getAsReactive<AuthState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StateBuilder<DocumentState>(
        observe: () => documentState,
        builder: (_, __) {
          return WhenRebuilder<DocumentState>(
              observe: () => documentState,
              onWaiting: () => WaitingView(),
              onIdle: () => WaitingView(),
              onError: (dynamic error) => ErrorView(error: error),
              onData: (data) {
                var listData = (data.classDocumentsMap ??
                            HashMap<int,
                                HashMap<DocumentModel, List<Files>>>())[
                        widget.classId]
                    ?.keys;
                var listValue = (data.classDocumentsMap ??
                            HashMap<int,
                                HashMap<DocumentModel, List<Files>>>())[
                        widget.classId] ??
                    HashMap<int, HashMap<DocumentModel, List<Files>>>();
                if (listData?.isEmpty ?? true) {
                  return Column(
                    children: [
                      SizedBox(height: 100),
                      Image.asset(
                        'assets/icons/dokumen/empty.png',
                        height: S.w * .4,
                        width: S.w * .4,
                      ),
                      Text('Belum ada dokumen',
                          style: TextStyle(color: SiswamediaTheme.green)),
                    ],
                  );
                }
                // } else if (ConnectionState.waiting == snapshot.connectionState) {
                //   return Center(child: CircularProgressIndicator());
                // }
                return Column(
                    children: listData
                        .map((e) => Container(
                              margin: EdgeInsets.symmetric(vertical: S.w * .02),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      offset: Offset(0, 0),
                                      spreadRadius: 2,
                                      blurRadius: 2,
                                      color: Colors.grey.withOpacity(.2),
                                    )
                                  ]),
                              child: ExpansionTile(
                                  title: Text(
                                    e.name,
                                    style:
                                        semiBlack.copyWith(fontSize: S.w / 24),
                                  ),
                                  children: [
                                    Divider(color: Colors.grey),
                                    Column(
                                      children: ((listValue)[e]
                                                  as List<Files> ??
                                              <Files>[])
                                          .map((f) => ListTile(
                                                onTap: () => Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                        builder: (_) =>
                                                            WebViewHome(
                                                              url:
                                                                  f.webViewLink,
                                                              title: f.name,
                                                            ))),
                                                leading: Icon(
                                                    Icons.file_copy_outlined,
                                                    color:
                                                        SiswamediaTheme.green),
                                                title: Text(f.name.toString()),
                                              ))
                                          .toList(),
                                    ),
                                    if (authState.state.currentState.classRole
                                        .isGuruOrWaliKelas)
                                      ListTile(
                                        onTap: () {
                                          callback(e);
                                        },
                                        leading: Icon(Icons.add,
                                            color: SiswamediaTheme.green),
                                        title: Text('Upload File'),
                                      )
                                  ] //List.generate(document.data[e].length + 2,
                                  //     (index) {
                                  //   if (index == 0) {
                                  //     return Divider(color: Colors.grey);
                                  //   } else if ((index == fs.length + 1) &&
                                  //       auth.role.contains('GURU')) {
                                  //     return ListTile(
                                  //       onTap: () {
                                  //         //TODO upload data
                                  //       },
                                  //       leading: Icon(Icons.add,
                                  //           color: SiswamediaTheme.green),
                                  //       title: Text('Upload File'),
                                  //     );
                                  //   }
                                  //   return ListTile(
                                  //     onTap: callback,
                                  //     leading: Icon(Icons.file_copy_outlined,
                                  //         color: SiswamediaTheme.green),
                                  //     title: Text(
                                  //         document.data[e][index - 1].name.toString()),
                                  //   );
                                  // }
                                  //)
                                  ),
                            ))
                        .toList());
              });
        });
  }

  void callback(DocumentModel model) {
    if (authState.state.currentState.schoolRole.contains('GURU') ||
        authState.state.currentState.schoolRole.contains('WALIKELAS')) {
      // var S.w = MediaQuery.of(context).size.width;
      showDialog<void>(
          context: context,
          builder: (context) => StatefulBuilder(
                builder: (BuildContext context,
                    void Function(void Function()) setState) {
                  return Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    child: Container(
                      height: S.w * .6,
                      width: S.w * .7,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //
                          Text('Unggah Dokumen',
                              style: TextStyle(
                                  fontSize: S.w / 26,
                                  fontWeight: FontWeight.w600)),
                          Text('Pilih dokumen yang ingin Anda unggah melalui',
                              style: TextStyle(
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: Color(0xff4B4B4B),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(model);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('Google Drive',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(model);
                                // await Kiki.getFile(model, context);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('File Manager',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ));
    }
  }
}

class Kiki {
  static Future<void> getFile(DocumentModel model, BuildContext context) async {
    try {
      var files = await FilePicker.platform.pickFiles(allowMultiple: false);
      var file = files.files.first;
      var fi = lookupMimeType(file.path);
      var name = file.name;
      print('tipe $fi');
      var data = File(file.path);
      var bytes = await data.readAsBytesSync();
      print(bytes.length);
      await GoogleDrive.uploadFile(
              name: name,
              context: context,
              parent: model.document,
              coba: bytes,
              contentType: fi,
              contentLength: bytes.length.toString())
          .then((value) async {
        if (value.statusCode == 200) {
          await GoogleDrive.getAllFile(parent: model.document).then((value2) {
            Navigator.pop(context);
            Navigator.pop(context);
            CustomFlushBar.successFlushBar('Berhasil mengupload file', context);
          });
        } else {
          throw Exception();
        }
      });
    } catch (e) {
      print(e);
      print('Error');
      CustomFlushBar.errorFlushBar('error', context);
    }
  }
}
