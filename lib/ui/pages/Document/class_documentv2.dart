part of '_document.dart';

class ClassDocumentv2 extends StatefulWidget {
  ClassDocumentv2({this.classId, this.type, this.onUpload});

  final String type;
  final int classId;
  final Function(String parent) onUpload;
  @override
  _ClassDocumentv2State createState() => _ClassDocumentv2State();
}

class _ClassDocumentv2State extends State<ClassDocumentv2> {
  final document = Injector.getAsReactive<DocumentState>();
  final auth = Injector.getAsReactive<AuthState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var data = document.state;
    data.classDoc.forEach((key, value) {
      print(key);
      print(value);
    });
    print('class ${widget.classId}');
    var parent = data.listDocClass.isEmpty
        ? data.classDoc[widget.classId]
        : data.listDocClass.last;
    print('parent $parent');
    return FutureBuilder<AllSubFile>(
        future: GoogleDrive.getAllFile(parent: parent),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasData) {
            if (document.state.listDocClass.isNotEmpty &&
                auth.state.currentState.classRole.isGuruOrWaliKelas) {
              snapshot.data.files
                  .add(Files(name: 'Upload', mimeType: 'upload'));
            }
            if (snapshot.data.files == null || snapshot.data.files.isEmpty) {
              return Column(
                children: [
                  SizedBox(height: 100),
                  Image.asset(
                    'assets/icons/dokumen/empty.png',
                    height: S.w * .4,
                    width: S.w * .4,
                  ),
                  Text('Belum ada dokumen',
                      style: TextStyle(color: SiswamediaTheme.green)),
                ],
              );
            }

            return GridView.count(
              physics: ScrollPhysics(),
              scrollDirection: Axis.vertical,
              crossAxisCount: 2,
              childAspectRatio: 1,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              padding: EdgeInsets.symmetric(horizontal: 10),
              shrinkWrap: true,
              children: snapshot.data.files
                  .map((e) => Container(
                        margin: EdgeInsets.symmetric(vertical: S.w * .02),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 0),
                                spreadRadius: 2,
                                blurRadius: 2,
                                color: Colors.grey.withOpacity(.2),
                              )
                            ]),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: InkWell(
                                onTap: () {
                                  if (e.mimeType ==
                                      'application/vnd.google-apps.folder') {
                                    document.setState((s) =>
                                        s.addToStack(e.id, type: 'class'));
                                    setState(() {});
                                  } else if (e.mimeType == 'upload') {
                                    callback(document.state.listDocClass.last);
                                  } else {
                                    context.push<void>(WebViewHome(
                                      url: e.webViewLink,
                                      title: e.name,
                                    ));
                                  }
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      e.mimeType ==
                                              'application/vnd.google-apps.folder'
                                          ? Icons.folder
                                          : e.mimeType == 'upload'
                                              ? Icons.upload_rounded
                                              : Icons.file_copy_outlined,
                                      color: SiswamediaTheme.green,
                                      size: 40,
                                    ),
                                    HeightSpace(12),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 8.0),
                                      child: SimpleText(e.name,
                                          textAlign: TextAlign.center,
                                          style: SiswamediaTextStyle.subtitle),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            if (e.mimeType != 'upload' &&
                                (auth.state.currentState.classRole
                                    .isGuruOrWaliKelas))
                              Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                    onPressed: () {
                                      showDelete(context, e.id);
                                    },
                                    icon: Icon(
                                      Icons.restore_from_trash,
                                      color: SiswamediaTheme.red,
                                    )),
                              )
                          ],
                        ),
                      ))
                  .toList(),
            );
          }
          return Center(child: CircularProgressIndicator());
        });
  }

  void callback(String parent) {
    if (auth.state.currentState.schoolRole.contains('GURU') ||
        auth.state.currentState.schoolRole.contains('WALIKELAS')) {
      // var S.w = MediaQuery.of(context).size.width;
      showDialog<void>(
          context: context,
          builder: (context) => StatefulBuilder(
                builder: (BuildContext context,
                    void Function(void Function()) setState) {
                  return Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    child: Container(
                      height: S.w * .6,
                      width: S.w * .7,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //
                          Text('Unggah Dokumen',
                              style: TextStyle(
                                  fontSize: S.w / 26,
                                  fontWeight: FontWeight.w600)),
                          Text('Pilih dokumen yang ingin Anda unggah melalui',
                              style: TextStyle(
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: Color(0xff4B4B4B),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(parent);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('Google Drive',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                          Container(
                            width: S.w * .5,
                            height: S.w * .1,
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: InkWell(
                              onTap: () async {
                                widget.onUpload(parent);
                                // await Kiki.getFile(model, context);
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));*/
                              },
                              child: Center(
                                  child: Text('File Manager',
                                      style: TextStyle(
                                        fontSize: S.w / 30,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ))),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ));
    }
  }
}

void showDelete(BuildContext context, String id) {
  showDialog<void>(
      context: context,
      builder: (_) {
        return Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            width: 240,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SimpleText(
                  'Hapus Dokumen?',
                  style: SiswamediaTextStyle.title,
                  fontSize: 16,
                ),
                HeightSpace(25),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: AutoLayoutButton(
                        padding:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 4),
                        text: 'No',
                        onTap: () => context.pop(),
                        textColor: SiswamediaTheme.white,
                        backGroundColor: SiswamediaTheme.nearBlack,
                        textAlign: TextAlign.center,
                        radius: BorderRadius.circular(6),
                      ),
                    ),
                    WidthSpace(10),
                    Expanded(
                      flex: 1,
                      child: AutoLayoutButton(
                        padding:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 4),
                        text: 'Yes',
                        onTap: () async => await GoogleDrive.deleteFile(id: id),
                        textColor: SiswamediaTheme.white,
                        backGroundColor: SiswamediaTheme.green,
                        textAlign: TextAlign.center,
                        radius: BorderRadius.circular(6),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      });
}
