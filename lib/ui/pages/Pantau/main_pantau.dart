part of '_pantau.dart';

class Pantau extends StatefulWidget {
  @override
  _PantauState createState() => _PantauState();
}

class _PantauState extends State<Pantau> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var kode = TextEditingController();
  var fNode = FocusNode();
  var loading = false;
  // final ChromeSafariBrowser browser =
  //     ChromeSafariBrowser(bFallback: InAppBrowser());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        key: scaffoldKey,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: TitleAppbar(
          label: 'Pantau Aktivitas Siswa',
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back),
          color: SiswamediaTheme.green,
        ),
      ),
      body: GestureDetector(
        onTap: () {
          if (fNode.hasFocus) {
            fNode.unfocus();
          }
        },
        child: Container(
          height: S.h,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: S.h * .1),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Hero(
                      tag: 'pantau',
                      child: Image.asset('assets/icons/pantau/pantau.png',
                          height: S.w * .5, width: S.w * .5),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: S.w * .1, vertical: S.w * .02),
                      child: CustomText(
                        'Silahkan tuliskan kode yang tertera pada profil  wali murid',
                        Kind.heading3,
                        align: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: S.w * .75,
                      child: TextField(
                        focusNode: fNode,
                        controller: kode,
                        style: blackTextFont,
                        decoration: InputDecoration(
                          hintText: 'Masukkan kode wali murid.. ',
                          hintStyle:
                              descBlack.copyWith(fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                          contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffD8D8D8)),
                            borderRadius: BorderRadius.circular(S.w),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: SiswamediaTheme.green),
                            borderRadius: BorderRadius.circular(S.w),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: S.h * .1),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    loading
                        ? Center(child: CircularProgressIndicator())
                        : Container(
                            width: S.w * .75,
                            height: S.w * .12,
                            margin: EdgeInsets.symmetric(vertical: S.w * .01),
                            decoration: BoxDecoration(
                                color: SiswamediaTheme.green,
                                border: Border.all(color: SiswamediaTheme.green),
                                borderRadius: BorderRadius.circular(10)),
                            child: InkWell(
                              onTap: () async {
                                setState(() {
                                  loading = true;
                                });
                                await PantauServices.verify(
                                        code: kode.text,
                                        classId: GlobalState.auth().state.currentState.classId)
                                    .then((value) async {
                                  if (value.statusCode == 200) {
                                    // await browser
                                    //     .open(
                                    //         url: 'https://activity.google.com/',
                                    //         options: ChromeSafariBrowserClassOptions(
                                    //             android:
                                    //                 AndroidChromeCustomTabsOptions(
                                    //                     addDefaultShareMenuItem:
                                    //                         true,
                                    //                     keepAliveEnabled: true),
                                    //             ios: IOSSafariOptions(
                                    //                 dismissButtonStyle:
                                    //                     IOSSafariDismissButtonStyle
                                    //                         .CLOSE,
                                    //                 presentationStyle:
                                    //                     IOSUIModalPresentationStyle
                                    //                         .OVER_FULL_SCREEN)))
                                    //     .then((value) {
                                    //   setState(() {
                                    //     loading = false;
                                    //   });
                                    // });
                                    //
                                    // await navigate(
                                    //     context,
                                    //     WebViewHome(
                                    //       title: 'Pantau',
                                    //       url: 'https://activity.google.com/',
                                    //     ));
                                  } else {
                                    setState(() {
                                      loading = false;
                                    });
                                    CustomFlushBar.errorFlushBar(
                                      'Unable to verify, pastikan masukkan kode di akun anak anda',
                                      context,
                                    );
                                  }
                                });
                              },
                              child: Material(
                                color: Colors.transparent,
                                child: Center(
                                    child: Text('Lanjutkan',
                                        style: TextStyle(color: SiswamediaTheme.white))),
                              ),
                            )),
                    Container(
                        width: S.w * .75,
                        height: S.w * .12,
                        margin: EdgeInsets.symmetric(vertical: S.w * .01),
                        decoration: BoxDecoration(
                            color: SiswamediaTheme.lightBlack,
                            borderRadius: BorderRadius.circular(10)),
                        child: InkWell(
                          onTap: () => Navigator.pop(context),
                          child: Center(
                              child: Text('Batal', style: TextStyle(color: SiswamediaTheme.white))),
                        )),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
