part of '_school.dart';

class DeleteSchoolValidator {
  final _nameController = BehaviorSubject<String>();

  Function(String) get onSchoolNameChanged => _nameController.sink.add;

  Stream<String> validateSchoolName(String name) => _nameController.stream
          .transform(StreamTransformer<String, String>.fromHandlers(
              handleData: (String inputString, EventSink<String> sink) {
        try {
          if (name != inputString) {
            sink.addError('Not Match');
          } else {
            sink.add(inputString);
          }
        } catch (e) {
          sink.addError('input wrong');
        }
      }));
}
