part of '_school.dart';

class DetailSchool extends StatefulWidget {
  // final int id;
  // DetailSchool({@required this.id});

  @override
  _DetailSchoolState createState() => _DetailSchoolState();
}

class _DetailSchoolState extends State<DetailSchool>
    with DeleteSchoolValidator {
  final auth = GlobalState.auth();
  TextEditingController _schoolNameController;
  bool _isLoading;

  @override
  void initState() {
    super.initState();
    _schoolNameController = TextEditingController();
    _isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    var isPersonal = auth.state.currentState.type == 'PERSONAL';
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            shadowColor: Colors.transparent,
            centerTitle: true,
            title: TitleAppbar(
                label:
                    'Detail ${isPersonal ? 'Media Pembelajaran' : 'Sekolah'}'),
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: SiswamediaTheme.green,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            actions: [
              Center(
                child: CustomContainer(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                    radius: BorderRadius.circular(4),
                    color: SiswamediaTheme.red,
                    onTap: () => showDialog<void>(
                        context: context,
                        builder: (context) => StatefulBuilder(builder:
                                (BuildContext context,
                                    void Function(void Function()) setState) {
                              // String role = someCapitalizedString(data.role);
                              return Center(
                                child: SingleChildScrollView(
                                  // padding: MediaQuery.of(context).viewInsets,
                                  child: Dialog(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(12)),
                                      child: Container(
                                          width: S.w * .5,
                                          padding: EdgeInsets.symmetric(
                                              vertical: S.w *
                                                  .05, //MediaQuery.of(context).padding.bottom,
                                              horizontal: S.w * .05),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Text('Menghapus Sekolah',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16)),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Text(
                                                  'Pastikan anda sudah memeriksa semua isi dari instansi ini. Apakah anda yakin untuk menghapus instansi ini? ketikkan nama dibawah!',
                                                  textAlign: TextAlign.center,
                                                  style:
                                                      TextStyle(fontSize: 12)),
                                              SizedBox(
                                                height: 16,
                                              ),
                                              Container(
                                                  margin: EdgeInsets.symmetric(
                                                      vertical: 5),
                                                  child: StreamBuilder<String>(
                                                      stream:
                                                          validateSchoolName(
                                                              auth
                                                                  .state
                                                                  .currentState
                                                                  .schoolName),
                                                      builder:
                                                          (context, snapshot) {
                                                        return TextField(
                                                          controller:
                                                              _schoolNameController,
                                                          decoration: InputDecoration(
                                                              hintText:
                                                                  '${auth.state.currentState.schoolName}',
                                                              hintStyle:
                                                                  descBlack,
                                                              errorText: snapshot
                                                                  .error,
                                                              contentPadding:
                                                                  EdgeInsets.symmetric(
                                                                      vertical:
                                                                          2.0,
                                                                      horizontal:
                                                                          10),
                                                              border: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius.circular(
                                                                          5.0),
                                                                  borderSide: BorderSide(
                                                                      color: Colors.grey[
                                                                          300])),
                                                              enabledBorder: OutlineInputBorder(
                                                                  borderRadius:
                                                                      BorderRadius.circular(
                                                                          5.0),
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color: Colors.grey[300]))),
                                                          onChanged:
                                                              onSchoolNameChanged,
                                                        );
                                                      })),
                                              SizedBox(
                                                height: 16,
                                              ),
                                              StreamBuilder<String>(
                                                  stream: validateSchoolName(
                                                      auth.state.currentState
                                                          .schoolName),
                                                  builder: (context, snapshot) {
                                                    return InkWell(
                                                        onTap:
                                                            (snapshot.hasError ||
                                                                    snapshot.data ==
                                                                        null)
                                                                ? null
                                                                : () async {
                                                                    setState(() =>
                                                                        _isLoading =
                                                                            true);
                                                                    var url =
                                                                        '$baseUrl/$version/schools/${auth.state.currentState.schoolId}';
                                                                    var resp = await client.delete(
                                                                        url,
                                                                        headers:
                                                                            await Pref.getHeader());
                                                                    if (resp
                                                                        .statusCode
                                                                        .isSuccessOrCreated) {
                                                                      context
                                                                          .pop();
                                                                      context
                                                                          .pop();
                                                                      CustomFlushBar.successFlushBar(
                                                                          'Berhasil menghapus instansi',
                                                                          context);
                                                                    } else {
                                                                      context
                                                                          .pop();
                                                                      CustomFlushBar.errorFlushBar(
                                                                          'Gagal menghapus instansi',
                                                                          context);
                                                                    }

                                                                    setState(() =>
                                                                        _isLoading =
                                                                            false);
                                                                  },
                                                        child: Container(
                                                            height: 45,
                                                            decoration: BoxDecoration(
                                                                color: (snapshot
                                                                            .hasError ||
                                                                        snapshot.data ==
                                                                            null)
                                                                    ? Colors
                                                                        .grey
                                                                    : SiswamediaTheme
                                                                        .red,
                                                                borderRadius:
                                                                    BorderRadius.circular(
                                                                        23)),
                                                            child: Center(
                                                                child: _isLoading
                                                                    ? Container(
                                                                        height:
                                                                            30,
                                                                        width: 30,
                                                                        child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.grey[300]), backgroundColor: Colors.white))
                                                                    : Text('Hapus', style: TextStyle(color: SiswamediaTheme.white)))));
                                                  })
                                            ],
                                          ))),
                                ),
                              );
                            })),
                    child: SimpleText('Hapus',
                        style: SiswamediaTextStyle.subtitle,
                        color: SiswamediaTheme.white,
                        fontSize: 12)),
              ),
            ]),
        body: ListView(
          children: [
            FutureBuilder<ResultSchoolModel>(
                future: ClassServices.detailSchool(
                    id: auth.state.currentState.schoolId),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Padding(
                      padding: EdgeInsets.only(top: S.w / 3),
                      child: Center(child: CircularProgressIndicator()),
                    );
                  } else {
                    var data = snapshot.data.data;
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Center(
                            child: Container(
                          margin: EdgeInsets.only(
                              left: 20, right: 20, top: 24, bottom: 8),
                          child: Column(
                            children: [
                              Text(
                                data.name,
                                textAlign: TextAlign.center,
                                style: boldBlack.copyWith(fontSize: S.w / 18),
                              ),
                              SizedBox(height: 10),
                              Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  margin: EdgeInsets.only(bottom: 16),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: data.isVerified
                                          ? SiswamediaTheme.green
                                          : Colors.red),
                                  child: Text(
                                    data.isVerified
                                        ? 'Sudah Terverifikasi'
                                        : 'Belum Terverifikasi',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 10),
                                  )),
                              Text(
                                '${isPersonal ? 'Penanggung Jawab' : 'Kepala Sekolah'} : ${data.namaKepalaSekolah}',
                                style: semiBlack.copyWith(fontSize: S.w / 28),
                              ),
                              SizedBox(height: 4),
                              Text('Kontak : ${data.phoneNumber}',
                                  style:
                                      semiBlack.copyWith(fontSize: S.w / 28)),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 24),
                                child: Divider(
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        )),
                        Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, bottom: 8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Alamat : ',
                                  style: semiBlack.copyWith(fontSize: S.w / 24),
                                ),
                                Text(data.address,
                                    style:
                                        descBlack.copyWith(fontSize: S.w / 32)),
                                SizedBox(height: 20),
                                Text(
                                  'Kabupaten : ',
                                  style: semiBlack.copyWith(fontSize: S.w / 24),
                                ),
                                Text(data.namaKabupaten,
                                    style:
                                        descBlack.copyWith(fontSize: S.w / 32)),
                                SizedBox(height: 20),
                                Text(
                                  'Provinsi : ',
                                  style: semiBlack.copyWith(fontSize: S.w / 24),
                                ),
                                Text(data.namaProvinsi,
                                    style:
                                        descBlack.copyWith(fontSize: S.w / 32)),
                                SizedBox(height: 20),
                                Text(
                                  'Email : ',
                                  style: semiBlack.copyWith(fontSize: S.w / 24),
                                ),
                                Text(data.schoolEmail,
                                    style:
                                        descBlack.copyWith(fontSize: S.w / 32)),
                                SizedBox(height: 20),
                                Text(
                                  'Tipe ${isPersonal ? '' : 'Sekolah'} : ',
                                  style: semiBlack.copyWith(fontSize: S.w / 24),
                                ),
                                Text(
                                  data.type,
                                  style: descBlack.copyWith(fontSize: S.w / 32),
                                ),
                              ],
                            )),
                        SizedBox(height: 20),
                      ],
                    );
                  }
                })
          ],
        ));
  }
}
