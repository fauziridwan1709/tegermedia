part of '_school.dart';

class CreateClassInstansi extends StatefulWidget {
  final bool isPersonal;

  CreateClassInstansi({this.isPersonal = false});

  @override
  _CreateClassInstansiState createState() => _CreateClassInstansiState();
}

class _CreateClassInstansiState extends State<CreateClassInstansi> with SchoolNameValidator {
  final authState = GlobalState.auth();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController namaKepalaSekolah = TextEditingController();
  TextEditingController phoneNumber = TextEditingController();
  TextEditingController schoolEmail = TextEditingController();

  String type = '';
  String grade = '';

  List<String> listTypeSchool = ['Negeri', 'Swasta', 'Lainnya'];
  List<String> listJenjang = ['TK', 'SD', 'SMP', 'SMA', 'SMK', 'UMUM'];

  String provinsiID = '';
  String kotaID = '';
  ModelListWilayah provinsi = ModelListWilayah(data: [], statusCode: null);
  ModelListWilayah kabupaten = ModelListWilayah(data: [], statusCode: null);

  String selectedLang = 'English';
  String prov = 'Kota Langsa';

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getProvinsi();
    if (kotaID != '') {
      getKota(kotaID);
    }
  }

  final fireStoreInstance = FirebaseFirestore.instance;

  Future<void> createInstance({DocumentModel data}) async {
    await fireStoreInstance.collection('document').add(<String, dynamic>{
      'class_id': data.classId,
      'school_id': data.schoolId,
      'document_list': data.document,
      'name': data.name,
      'type': data.type,
      'parent': data.parent
    });
  }

  void getProvinsi() async {
    var wilayah = await WilayahServices.getWilayah();
    setState(() {
      provinsi = wilayah;
      kabupaten = ModelListWilayah(statusCode: null, data: []);
    });
  }

  void getKota(String id) async {
    dynamic wilayah = await WilayahServices.getKabupaten(id);
    var data = ModelListWilayah.fromJson(wilayah);

    if (data.statusCode == 200) {
      setState(() {
        kabupaten = data;
      });
    } else {
      setState(() {
        kabupaten = ModelListWilayah(statusCode: null, data: []);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        return;
      },
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title:
              TitleAppbar(label: 'Daftar ${widget.isPersonal ? 'Media Pembelajaran' : 'Instansi'}'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: Center(
          child: Container(
              width: S.w,
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .08),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: defaultMargin),
                      Column(
                        children: <Widget>[
                          DropdownButtonFormField<String>(
                            value: provinsiID,
                            icon: Icon(Icons.keyboard_arrow_down, color: SiswamediaTheme.green),
                            iconSize: 24,
                            elevation: 16,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                borderRadius: BorderRadius.circular(S.w),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: SiswamediaTheme.green),
                                borderRadius: BorderRadius.circular(S.w),
                              ),
                            ),
                            style: descBlack.copyWith(fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                            onChanged: (String newValue) {
                              setState(() {
                                kotaID = '';
                                provinsiID = newValue.toString();
                                kabupaten = ModelListWilayah(statusCode: null, data: []);
                              });
                              getKota(newValue.toString());
                            },
                            items: [
                              DropdownMenuItem(
                                child: Text('Pilih Provinsi'),
                                value: '',
                              ),
                              for (DataWilayah value in provinsi.data)
                                DropdownMenuItem(
                                  value: value.id.toString(),
                                  child: Text('${value.nama}'),
                                )
                            ],
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          DropdownButtonFormField<String>(
                            value: kotaID,
                            icon: Icon(Icons.keyboard_arrow_down, color: SiswamediaTheme.green),
                            iconSize: 24,
                            elevation: 16,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                borderRadius: BorderRadius.circular(S.w),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: SiswamediaTheme.green),
                                borderRadius: BorderRadius.circular(S.w),
                              ),
                            ),
                            style: descBlack.copyWith(fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                            onChanged: (String newValue) {
                              kotaID = newValue.toString();
                              // setState(() => kotaID = newValue),
                            },
                            items: [
                              DropdownMenuItem(
                                child: Text('Pilih Kota'),
                                value: '',
                              ),
                              for (DataWilayah value in kabupaten.data)
                                DropdownMenuItem(
                                  value: value.id.toString(),
                                  child: Text('${value.nama}'),
                                )
                            ],
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          if (!widget.isPersonal)
                            DropdownButtonFormField<String>(
                              value: type,
                              icon: Icon(Icons.keyboard_arrow_down, color: SiswamediaTheme.green),
                              iconSize: 24,
                              elevation: 16,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: SiswamediaTheme.green),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                              ),
                              style:
                                  descBlack.copyWith(fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                              onChanged: (String newValue) {
                                if (newValue == 'Lainnya' || newValue == '') {
                                  setState(() {
                                    type = newValue.toString();
                                  });
                                } else {
                                  setState(() {
                                    type = newValue.toString();
                                  });
                                }
                              },
                              items: [
                                DropdownMenuItem(
                                  child: Text('Pilih Jenis Sekolah / Instansi'),
                                  value: '',
                                ),
                                for (String value in listTypeSchool)
                                  DropdownMenuItem(
                                    value: value.toString(),
                                    child: Text(value),
                                  )
                              ],
                            ),
                          if (!widget.isPersonal)
                            SizedBox(
                              height: 16,
                            ),
                          Column(children: [
                            if (!widget.isPersonal)
                              DropdownButtonFormField<String>(
                                value: grade,
                                icon: Icon(Icons.keyboard_arrow_down, color: SiswamediaTheme.green),
                                iconSize: 24,
                                elevation: 16,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                    borderRadius: BorderRadius.circular(S.w),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: SiswamediaTheme.green),
                                    borderRadius: BorderRadius.circular(S.w),
                                  ),
                                ),
                                style: descBlack.copyWith(
                                    fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                                onChanged: (String newValue) => setState(() => grade = newValue),
                                items: [
                                  DropdownMenuItem(
                                    child: Text('Pilih Jenjang Sekolah / Instansi'),
                                    value: '',
                                  ),
                                  for (String value in listJenjang)
                                    DropdownMenuItem(
                                      value: value.toString(),
                                      child: Text(value),
                                    )
                                ],
                              ),
                            SizedBox(height: 16),
                            StreamBuilder<String>(
                                stream: validateThisSchoolName,
                                builder: (context, snapshot) {
                                  return TextField(
                                    controller: name,
                                    onChanged: onSchoolNameChanged,
                                    style: descBlack.copyWith(fontSize: S.w / 30),
                                    decoration: InputDecoration(
                                      errorText: snapshot.error,
                                      suffixIcon: Icon(
                                        snapshot.hasError || name.text.isEmpty
                                            ? Icons.clear
                                            : Icons.check_circle_rounded,
                                        color: snapshot.hasError || name.text.isEmpty
                                            ? Colors.red
                                            : SiswamediaTheme.green,
                                      ),
                                      hintText:
                                          'Nama ${widget.isPersonal ? 'Media Pembelajaran' : 'Sekolah / Instansi'}',
                                      hintStyle: descBlack.copyWith(
                                          fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                        borderRadius: BorderRadius.circular(S.w),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.red),
                                        borderRadius: BorderRadius.circular(S.w),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.red),
                                        borderRadius: BorderRadius.circular(S.w),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: SiswamediaTheme.green),
                                        borderRadius: BorderRadius.circular(S.w),
                                      ),
                                    ),
                                  );
                                }),
                            SizedBox(
                              height: 16,
                            ),
                            TextField(
                              controller: address,
                              style: descBlack.copyWith(fontSize: S.w / 30),
                              maxLines: 4,
                              minLines: 3,
                              decoration: InputDecoration(
                                hintText: 'Alamat ${widget.isPersonal ? '' : 'Sekolah / Instansi'}',
                                hintStyle: descBlack.copyWith(
                                    fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                contentPadding:
                                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 25),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: SiswamediaTheme.green),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            TextField(
                              controller: namaKepalaSekolah,
                              style: descBlack.copyWith(fontSize: S.w / 30),
                              decoration: InputDecoration(
                                hintText:
                                    'Nama ${widget.isPersonal ? 'Penanggung Jawab' : 'Kepala Sekolah / Instansi'}',
                                hintStyle: descBlack.copyWith(
                                    fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: SiswamediaTheme.green),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            TextField(
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              controller: phoneNumber,
                              style: descBlack.copyWith(fontSize: S.w / 30),
                              decoration: InputDecoration(
                                hintText:
                                    'No Telp. ${widget.isPersonal ? '' : 'Sekolah / Instansi'}',
                                hintStyle: descBlack.copyWith(
                                    fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: SiswamediaTheme.green),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            TextField(
                              keyboardType: TextInputType.emailAddress,
                              controller: schoolEmail,
                              style: descBlack.copyWith(fontSize: S.w / 30),
                              decoration: InputDecoration(
                                hintText:
                                    'Email ${widget.isPersonal ? 'Media' : 'Sekolah / Instansi'}',
                                hintStyle: descBlack.copyWith(
                                    fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: SiswamediaTheme.green),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                              ),
                            ),
                          ]),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () {
                          print('mantap');
                          Pattern pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          var regex = RegExp(pattern);
                          if (!regex.hasMatch(schoolEmail.text)) {
                            return CustomFlushBar.errorFlushBar(
                              'Email tidak valid',
                              context,
                            );
                          }

                          if (name.text == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Nama sekolah belum di isi',
                              context,
                            );
                            // return _message('Nama sekolah belum di isi');
                          }
                          if (address.text == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Alamat sekolah belum di isi',
                              context,
                            );
                          }
                          if (namaKepalaSekolah.text == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Nama kepala sekolah belum di isi',
                              context,
                            );
                          }
                          if (phoneNumber.text == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Nomor telpon sekolah belum di isi',
                              context,
                            );
                          }

                          if (schoolEmail.text == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Email sekolah belum di isi',
                              context,
                            );
                          }

                          if (grade == '' && !widget.isPersonal) {
                            return CustomFlushBar.errorFlushBar(
                              'Jenjang sekolah belum di isi',
                              context,
                            );
                          }

                          if (type == '' && !widget.isPersonal) {
                            return CustomFlushBar.errorFlushBar(
                              'Jenis sekolah belum di isi',
                              context,
                            );
                          }

                          if (provinsiID == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Provinsi sekolah belum di isi',
                              context,
                            );
                          }

                          if (kotaID == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Kota sekolah belum di isi',
                              context,
                            );
                          }
                          dialog(context);
                        },
                        child: Container(
                          width: S.w * .7,
                          height: S.w * .13,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: SiswamediaTheme.green),
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                          child: Center(
                              child: Text('Lanjutkan',
                                  style: TextStyle(
                                      fontSize: S.w / 28,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600))),
                        ),
                      ),
                      SizedBox(height: 50)
                    ],
                  ),
                ),
              )),
        ),
      ),
    );
  }

  void dialog(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context2) => StatefulBuilder(
                builder: (BuildContext context2, void Function(void Function()) setState) {
              return Dialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      width: S.w * .8,
                      padding: EdgeInsets.symmetric(vertical: S.w * .1, horizontal: S.w * .05),
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('Kirim Permohonan',
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                          Container(
                            width: S.w - 48 * 2,
                            height: 100,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/icons/kelas/newsletter.png'))),
                          ),
                          SizedBox(height: 15),
                          Text(
                              'Anda akan mendapatkan email dari\nkami mengenai verifikasi paling lama\n7 hari kerja. Pastikan email yang anda\nmasukkan adalah email Aktif.',
                              style: TextStyle(fontSize: 12),
                              textAlign: TextAlign.center),
                          SizedBox(height: 15),
                          InkWell(
                            onTap: () async {
                              SiswamediaTheme.loading(context, isDismissible: false);
                              await Injector.getAsReactive<ClassState>().refresh();
                              var field = <String, dynamic>{
                                'name': name.text,
                                'phone_number': phoneNumber.text,
                                'provinsi_id': provinsiID,
                                'kota_id': kotaID,
                                'type': widget.isPersonal ? 'Lainnya' : type,
                                'grade': widget.isPersonal ? 'UMUM' : grade,
                                'address': address.text,
                                'school_email': schoolEmail.text,
                                'nama_kepala_sekolah': namaKepalaSekolah.text,
                                'penanggung_jawab_email': schoolEmail.text,
                                'personal': widget.isPersonal
                              };

                              var data = await ClassServices.createSchool(reqBody: field);

                              if (data.statusCode.isSuccessOrCreated) {
                                var school = await ClassServices.getMeSchool(cache: false);

                                var newSchool = school.data
                                    .where((element) => element.id == data.data.id)
                                    .first;
                                var currentState = UserCurrentState(
                                    schoolId: data.data.id,
                                    schoolName: newSchool.name,
                                    schoolRole: newSchool.roles,
                                    type: 'SEKOLAH',
                                    className: null,
                                    classId: null,
                                    classRole: null,
                                    isAdmin: newSchool.isAdmin);
                                await authState.setState((s) => s.choseSchoolById(data.data.id));
                                await authState
                                    .setState((s) => s.changeCurrentState(state: currentState));
                                Navigator.pop(context);
                                Navigator.pop(context);
                                if (widget.isPersonal) {
                                  Navigator.popUntil(context, (route) => route.isFirst);
                                  CustomFlushBar.successFlushBar(
                                    'Sekolah ${newSchool.name} berhasil dibuat!',
                                    context,
                                  );
                                  await GlobalState.school().setState((s) => s.retrieveData());
                                } else {
                                  var reqBody = <String, dynamic>{
                                    'name': name.text.toString(),
                                    'mimeType': 'application/vnd.google-apps.folder',
                                  };

                                  var reqBody2 = <String, dynamic>{
                                    'role': 'reader',
                                    'type': 'anyone',
                                  };
                                  dialogFolder(context);

                                  ///root folder nama foldernya itu nama sekolahnya
                                  await GoogleDrive.createDriveFolder(
                                          context: context, reqBody: reqBody, reqBody2: reqBody2)
                                      .then((value) async {
                                    if (value.statusCode == 200) {
                                      print('success create drive');
                                      Navigator.pop(context);
                                      var reqP = <String, dynamic>{
                                        'name': 'Informasi Sekolah',
                                        'mimeType': 'application/vnd.google-apps.folder',
                                        'parents': [value.id.toString()]
                                      };
                                      dialogFolder(context);

                                      ///default folder di dalem root folder nama default nya "informasi sekolah"
                                      await GoogleDrive.createDriveFolder(
                                        context: context,
                                        reqBody: reqP,
                                        reqBody2: reqBody2,
                                      ).then((value2) async {
                                        var documentModel = DocumentModel(
                                            classId: -1,
                                            schoolId: data.data.id,
                                            name: 'Informasi Sekolah',
                                            parent: value.id,
                                            document: value2.id,
                                            type: 'sekolah');
                                        await createInstance(data: documentModel)
                                            .then((value) async {
                                          //todo
                                          // await documentProvider
                                          //     .addFolder(documentModel);

                                          var result = await ClassServices.setCurrentState(
                                              data: json.encode(currentState.toJson()));
                                          Navigator.pop(context);
                                          if (result.statusCode != 200) {
                                            CustomFlushBar.errorFlushBar(
                                              result.message,
                                              context,
                                            );
                                          } else {
                                            Navigator.popUntil(context, (route) => route.isFirst);
                                            await GlobalState.school()
                                                .setState((s) => s.retrieveData());
                                            CustomFlushBar.successFlushBar(
                                              'Folder sekolah berhasil dibuat!',
                                              context,
                                            );
                                          }

                                          // Navigator.pushReplacement(
                                          //   context,
                                          //   MaterialPageRoute<void>(
                                          //       builder: (context) =>
                                          //           ProfilePage()),
                                          // );
                                        });
                                      });
                                    } else {
                                      var result = await ClassServices.setCurrentState(
                                          data: json.encode(currentState.toJson()));
                                      Navigator.pop(context);
                                      if (result.statusCode != 200) {
                                        CustomFlushBar.errorFlushBar(
                                          result.message,
                                          context,
                                        );
                                      } else {
                                        Navigator.popUntil(context, (route) => route.isFirst);
                                        await GlobalState.school()
                                            .setState((s) => s.retrieveData());
                                      }
                                      CustomFlushBar.errorFlushBar(
                                        'Terjadi Kesalahan dalam mengenerate Google Drive folder, anda bisa membuat folder sekolah manual di menu dokumen',
                                        context,
                                      );
                                    }
                                  });
                                }
                              } else {
                                Navigator.of(context).pop();
                                CustomFlushBar.errorFlushBar(
                                  'Sepertinya Sekolah tersebut telah terdaftar',
                                  context,
                                );
                              }
                            },
                            child: Container(
                                height: 35,
                                width: 147,
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.green,
                                    borderRadius: BorderRadius.circular(4)),
                                child: Center(
                                    child: Text('Kirim Formulir',
                                        style: TextStyle(color: Colors.white)))),
                          ),
                        ],
                      )));
            }));
  }
}
