part of '_school.dart';

class SchoolList extends StatelessWidget {
  final List<DataSchoolModel> mySchool;
  final bool isPersonal;
  SchoolList({this.mySchool, this.isPersonal = false});

  final classState = Injector.getAsReactive<ClassState>();
  @override
  Widget build(BuildContext context) {
    final auth = GlobalState.auth();
    var currentState = auth.state.currentState;
    //todo later penting
    return Column(
      children: [
        Container(
            margin: EdgeInsets.symmetric(
              horizontal: S.w * .05,
            ),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(height: 10),
              Text(isPersonal ? 'Merdeka Belajar' : 'Sekolah Media Pembelajaran',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
              SizedBox(height: 16),
              Container(
                  width: S.w,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: List.generate(
                          mySchool.length,
                          (index) => Padding(
                                padding: EdgeInsets.symmetric(vertical: 6),
                                child: InkWell(
                                    onTap: () async {
                                      SiswamediaTheme.infoLoading(
                                          context: context, info: 'Sedang mengganti kelas');
                                      await changeSchool(
                                          context: context, meSchool: mySchool[index]);
                                      Cleaner().cleanState();
                                    },
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 13),
                                      decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                                color: SiswamediaTheme.grey.withOpacity(0.15),
                                                offset: const Offset(0.2, 0.2),
                                                blurRadius: 8.0,
                                                spreadRadius: 1),
                                          ],
                                          color: currentState.schoolId == mySchool[index].id
                                              ? SiswamediaTheme.green
                                              : Colors.white,
                                          borderRadius: BorderRadius.circular(10),
                                          border: Border.all(color: Color(0xFF00000029))),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text('${mySchool[index].name}',
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight: FontWeight.w600,
                                                          color: auth.state.currentState.schoolId ==
                                                                  mySchool[index].id
                                                              ? Colors.white
                                                              : Colors.black)),
                                                  HeightSpace(5),
                                                  Row(
                                                    children: <Widget>[
                                                      Text('Sebagai : ',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.w500,
                                                              fontSize: 12,
                                                              color: currentState.schoolId ==
                                                                      mySchool[index].id
                                                                  ? Colors.white
                                                                  : Colors.black)),
                                                      Text(
                                                          '${mySchool[index].roles.isEmpty ? 'belum ada role kelas' : mySchool[index].roles.join('/')}',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.w600,
                                                              fontSize: 12,
                                                              color: currentState.schoolId ==
                                                                      mySchool[index].id
                                                                  ? Colors.white
                                                                  : Colors.black)),
                                                    ],
                                                  ),
                                                ]),
                                          ),
                                          if (mySchool[index].isAdmin) AdminLabel(),
                                        ],
                                      ),
                                    )),
                              )).toList()))
            ])),
        SizedBox(height: 5),
        InkWell(
          onTap: () => context.push<void>(SelectEntitasInstansi(isPersonal: isPersonal)),
          child: Container(
            width: S.w * .9,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xFFD9D9D9)),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Circle(
                  size: 35,
                  color: Color(0xFF4b4b4b),
                  child: Icon(Icons.add, size: 35, color: SiswamediaTheme.green)),
            ),
          ),
        ),
        SizedBox(
          height: 25,
        )
      ],
    );
  }

  ///button event handler
  Future<void> changeSchool({BuildContext context, DataSchoolModel meSchool}) async {
    final authState = GlobalState.auth();
    try {
      var currentState = UserCurrentState(
          schoolId: meSchool.id,
          schoolName: meSchool.name,
          schoolRole: meSchool.roles,
          type: meSchool.personal ? 'PERSONAL' : 'SEKOLAH',
          className: null,
          classId: null,
          classRole: null,
          isAdmin: meSchool.isAdmin);
      await ClassServices.setCurrentState(data: json.encode(currentState.toJson()))
          .then((value) async {
        Navigator.pop(context);
        if (value.statusCode == 200 || value.statusCode == 201) {
          authState.state.changeCurrentState(state: currentState);
          await authState.state.choseSchoolById(meSchool.id);
          authState.notify();
          await classState.refresh();
          CustomFlushBar.successFlushBar('Mengganti ke sekolah ${meSchool.name}', context);
        } else if (value.statusCode == StatusCodeConst.noInternet) {
          CustomFlushBar.errorFlushBar(value.message, context);
        } else if (value.statusCode == StatusCodeConst.timeout) {
          CustomFlushBar.errorFlushBar(value.message, context);
        } else {
          CustomFlushBar.errorFlushBar('terjadi kesalahan', context);
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
