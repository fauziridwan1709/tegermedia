part of '_school.dart';

class DetailSchoolHome extends StatefulWidget {
  const DetailSchoolHome({this.keyPilihKelas});
  final GlobalKey keyPilihKelas;
  @override
  _DetailSchoolHomeState createState() => _DetailSchoolHomeState();
}

class _DetailSchoolHomeState extends State<DetailSchoolHome> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // var auth = context.watch<UserProvider>();
    final authState = GlobalState.auth();
    return LayoutBuilder(builder: (context, snapshot) {
      if (authState.state.chosenSchool != null) {
        var data = authState.state.chosenSchool;

        return Container(
          margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .03),
          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
          decoration: SiswamediaTheme.whiteRadiusShadow,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      var auth = GlobalState.auth().state;
                      var classId = auth.currentState.classId;
                      if (classId != null) {
                        navigate(context, DetailClassInstansi(id: classId),
                            settings: RouteSettings(name: '/detail-class'));
                      } else {
                        CustomFlushBar.errorFlushBar('Pilih kelas terlebih dahulu', context);
                      }
                    },
                    child: Image.asset(
                      'assets/images/asset_kelas/kelas.png',
                      height: S.w * .35,
                      width: S.w * .35,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 8),
                          child: Text(
                            authState.state.currentState.type == 'PERSONAL'
                                ? 'Media ${data.name}'
                                : 'Sekolah ' + data.type + ' ' + data.name,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: S.w / 28, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                            margin: EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: data.isVerified ? SiswamediaTheme.green : Colors.red),
                            child: Text(
                              data.isVerified ? 'Sudah Terverifikasi' : 'Belum Terverifikasi',
                              style: TextStyle(color: Colors.white, fontSize: S.w / 38),
                            )),
                        if (authState.state.currentState.className != null)
                          Text(
                            authState.state.currentState.className,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: S.w / 30, fontWeight: FontWeight.w600),
                          ),
                        SizedBox(
                          height: 20,
                        ),
                        ButtonDetail(
                          keyPilihKelas: widget.keyPilihKelas,
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                          child: Divider(),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      } else {
        return Padding(
          padding: EdgeInsets.only(top: S.w / 3),
          child: Center(child: CircularProgressIndicator()),
        );
      }
    });
  }
}

void dialogDev(BuildContext context, {String title, String desc, String image}) {
  showDialog<void>(
      context: context,
      builder: (context) =>
          StatefulBuilder(builder: (BuildContext context, void Function(void Function()) setState) {
            // String role = someCapitalizedString(data.role);
            return Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                child: Container(
                    height: 300,
                    width: S.w * .5,
                    padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .05),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 100,
                          width: 170,
                          margin: EdgeInsets.only(bottom: 12),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(image ?? 'assets/icons/profile/role.png'))),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(title, style: TextStyle(fontSize: 18, fontWeight: FontWeight.w800)),
                        SizedBox(
                          height: 15,
                        ),
                        Text(desc, textAlign: TextAlign.center, style: TextStyle(fontSize: 12)),
                        SizedBox(
                          height: 16,
                        ),
                      ],
                    )));
          }));
  return null;
}

class ButtonDetail extends StatelessWidget {
  ButtonDetail({this.keyPilihKelas});

  final GlobalKey keyPilihKelas;
  final authState = GlobalState.auth();

  @override
  Widget build(BuildContext context) {
    var text =
        'Anda bisa ganti sekolah / role yang Anda butuhkan di halaman profile, Anda juga bisa membuat / mendaftarkan sekolah baru';
    var title = 'Ubah Role';
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (authState.state.currentState.schoolId != null)
          ButtonSmall(
              onTap: () {
                // Navigator.push(context, TutorialOverlay());
                Navigator.push(context,
                    MaterialPageRoute<void>(builder: (BuildContext context) => DetailSchool()));
              },
              icon: 'assets/images/asset_kelas/first.png'),
        ButtonSmall(
            onTap: () =>
                navigate(context, SwitchRole(), settings: RouteSettings(name: '/switchrole')),
            icons: Icons.people_alt_outlined),
        if (authState.state.currentState.schoolId != null)
          ButtonSmall(
              key: keyPilihKelas,
              onTap: () {
                navigate(context, ChangeClass(id: authState.state.currentState.schoolId));
              },
              icon: 'assets/images/asset_kelas/second.png'),
      ],
    );
  }
}

class ButtonSmall extends StatelessWidget {
  final Function onTap;
  final String icon;
  final IconData icons;

  ButtonSmall({Key key, this.onTap, this.icon, this.icons}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: Container(
          width: S.w * .1,
          height: S.w * .1,
          margin: EdgeInsets.symmetric(horizontal: 5),
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: SiswamediaTheme.grey.withOpacity(0.2),
                  offset: const Offset(0.2, 0.2),
                  blurRadius: 3.0),
            ],
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ),
          child: icons != null
              ? Icon(
                  icons,
                  color: SiswamediaTheme.green,
                  size: S.w / 20,
                )
              : Container(
                  width: S.w / 20,
                  height: S.w / 20,
                  margin: EdgeInsets.symmetric(horizontal: 24 / 2),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(image: DecorationImage(image: AssetImage(icon)))),
        ));
  }
}
