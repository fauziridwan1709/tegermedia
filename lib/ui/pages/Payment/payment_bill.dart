part of '_payment.dart';

class Bill extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Bills(),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                width: S.w,
                height: S.w * .15,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(.2),
                      spreadRadius: 2,
                      offset: Offset(0, 0))
                ]),
                child: CheckboxListTile(
                  title: CustomText('Pilih semua', Kind.descBlack),
                  controlAffinity: ListTileControlAffinity.leading,
                  value: true,
                  onChanged: (bool value) {},
                )),
            Container(),
          ],
        ),
      ],
    );
  }
}
