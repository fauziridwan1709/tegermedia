part of '_payment.dart';

class MainPayment extends StatefulWidget {
  @override
  _MainPaymentState createState() => _MainPaymentState();
}

class _MainPaymentState extends State<MainPayment>
    with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = TabController(initialIndex: 0, length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        title: TitleAppbar(label: 'Pembayaran'),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back),
          color: SiswamediaTheme.green,
        ),
        bottom: TabBar(
            controller: controller,
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.black,
            indicatorWeight: 4,
            tabs: [Tab(text: 'Tagihan'), Tab(text: 'riwayat')]),
      ),
      body: TabBarView(
        controller: controller,
        children: <Widget>[
          Bill(),
          Container(),
        ],
      ),
    );
  }
}
