import 'package:flutter/material.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'main_payment.dart';
part 'bills.dart';
part 'payment_bill.dart';
part 'payment_history.dart';
part 'bills_text_info.dart';
part 'payment_pay.dart';
part 'new_pay.dart';
