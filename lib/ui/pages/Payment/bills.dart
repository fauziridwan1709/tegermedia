part of '_payment.dart';

class Bills extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var paymentState = Injector.getAsReactive<PaymentState>();

    return StateBuilder(
      observe: () => paymentState,
      builder: (context, snapshot) {
        return ListView(
          padding: EdgeInsets.symmetric(vertical: S.w * .15),
          children: [
            PaymentModel(name: 'SPP', price: 200000),
            PaymentModel(name: 'Paket siswamedia', price: 1000000)
          ]
              .map((e) => Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: S.w * .04, vertical: S.w * .02),
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .05),
                    width: S.w * .9,
                    decoration: SiswamediaTheme.whiteRadiusShadow,
                    child: Row(
                      children: [
                        Checkbox(
                            value:
                                paymentState.state.payments.containsKey(e.name),
                            onChanged: (bool val) {
                              if (val) {
                                paymentState.setState((s) => s.addPayment(e));
                              } else {
                                paymentState
                                    .setState((s) => s.removePayment(e));
                              }
                            }),
                        BillsTextInfo(data: e)
                      ],
                    ),
                  ))
              .toList(),
        );
      },
    );
  }
}
