part of '_ujian.dart';

enum QuizAssessmentType { auto, manual }

class QuizAssessmentUser extends StatefulWidget {
  final Students user;
  final QuizAssessmentType type;
  final int quizId;
  final List<QuizModelDetail> data;
  const QuizAssessmentUser({this.type, this.user, this.quizId, this.data});

  @override
  _QuizAssessmentUserState createState() => _QuizAssessmentUserState();
}

class _QuizAssessmentUserState extends State<QuizAssessmentUser> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  int myIndex = 0;

  var soalBenar = <int>[];

  var manual = <int, int>{};
  var controller = TextEditingController();
  int totalBobot;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      var data = await QuizServices.answerSheet(
          userId: widget.user.userId, quizId: widget.quizId);
      var detail = <QuizAnswerData, bool>{};
      for (var i = 0; i < data.data.length; i++) {
        if (data.data[i].correctAnswer ==
            data.data[i].quizAnswer.answerDescription) {
          detail[data.data[i]] = true;
          soalBenar.add(data.data[i].id);
        } else {
          detail[data.data[i]] = false;
        }
      }
      await Provider.of<QuizProviderKelas>(context, listen: false)
          .setData(detail);
      var dai = widget.data.fold<int>(
          0, (previousValue, element) => previousValue + element.score);
      totalBobot = dai;
    });
  }

  @override
  Widget build(BuildContext context) {
    var penilaian = Provider.of<QuizProviderKelas>(context);
    print(manual);
    print(soalBenar);
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        elevation: 2,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: TitleAppbar(label: 'Penilaian Kuis'),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        actions: [
          Center(
            child: InkWell(
                onTap: () {
                  var dai = 0;
                  if (widget.type == QuizAssessmentType.manual) {
                    penilaian.addNilai(manual.values.fold<int>(0,
                        (previousValue, element) => previousValue + element));
                  } else {
                    penilaian.currentAssessment.then((value) {
                      print(value.keys.first.score);
                      dai = widget.data
                          .where((element) => soalBenar.contains(element.id))
                          .fold<int>(
                              0,
                              (previousValue, element) =>
                                  previousValue + element.score);

                      print(dai);
                      penilaian.addNilai(dai);
                    });
                  }
                  // print(penilaian.nilai);
                  if (penilaian.nilai > totalBobot) {
                    return CustomFlushBar.errorFlushBar(
                      'bobot yang anda berikan melebihi total bobot $totalBobot',
                      context,
                    );
                  } else {
                    showDialog<void>(
                        context: context,
                        builder: (context) => QuizDialog(
                              type: DialogType.onlyText,
                              actions: ['Selesai', 'Batal'],
                              title: 'yakin untuk menyelesaikan?',
                              description:
                                  'Yakin untuk memberikan bobot nilai ${penilaian.nilai} dari $totalBobot kepada siswa ${widget.user.fullName} ? nilai siswa ${((penilaian.nilai / totalBobot) * 100).roundToDouble() == ((penilaian.nilai / totalBobot) * 100).toDouble() ? ((penilaian.nilai / totalBobot) * 100).round() : ((penilaian.nilai / totalBobot) * 100).toStringAsFixed(2)}',
                              onTapFirstAction: () async {
                                if (widget.type == QuizAssessmentType.manual) {
                                  var listNilai = <Map<String, dynamic>>[];
                                  var nilaiLembarJawab = <QuizAnswerData>[];
                                  await penilaian.currentAssessment.then(
                                    (value) => setState(() {
                                      nilaiLembarJawab = value.keys
                                          .where((element) =>
                                              manual.keys.contains(element.id))
                                          .toList();
                                    }),
                                  );

                                  for (var i = 0;
                                      i < nilaiLembarJawab.length;
                                      i++) {
                                    listNilai.add(<String, dynamic>{
                                      'user_id': widget.user.userId,
                                      'quiz_answer_ref_id':
                                          nilaiLembarJawab[i].quizAnswer.id,
                                      'nilai': manual[nilaiLembarJawab[i].id],
                                      'penilaian_type': 'MANUAL'
                                    });
                                  }
                                  var result =
                                      await PenilaianServices.nilaiLembarJawab(
                                          quizId: widget.quizId,
                                          nilai: listNilai);
                                  if (result.statusCode == 200) {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    CustomFlushBar.successFlushBar(
                                      'Berhasil',
                                      context,
                                    );
                                    await penilaian.addNilai(0);
                                  } else if (result.statusCode == 409) {
                                    CustomFlushBar.errorFlushBar(
                                      'Penilaian sudah diberikan otomatis untuk jawaban berbentuk pilihan ganda, nilailah essay jika perlu',
                                      context,
                                    );
                                  } else {
                                    CustomFlushBar.errorFlushBar(
                                      'Tidak dapat menilai, jika memberi nilai 0, kosongkan saja (tidak perlu mengisi 0)',
                                      context,
                                    );
                                  }
                                } else {
                                  var listNilai = <Map<String, dynamic>>[];
                                  var nilaiLembarJawab = <QuizAnswerData>[];
                                  await penilaian.currentAssessment.then(
                                    (value) => setState(() {
                                      nilaiLembarJawab = value.keys
                                          .where((element) =>
                                              soalBenar.contains(element.id))
                                          .toList();
                                    }),
                                  );
                                  print(nilaiLembarJawab);

                                  for (var i = 0;
                                      i < nilaiLembarJawab.length;
                                      i++) {
                                    listNilai.add(<String, dynamic>{
                                      'user_id': widget.user.userId,
                                      'quiz_answer_ref_id':
                                          nilaiLembarJawab[i].quizAnswer.id,
                                      // 'nilai': widget.data
                                      //     .firstWhere((element) =>
                                      //         element.id ==
                                      //         nilaiLembarJawab[i].id)
                                      //     .score,
                                      'penilaian_type': 'OTOMATIS'
                                    });
                                  }
                                  var result =
                                      await PenilaianServices.nilaiLembarJawab(
                                          quizId: widget.quizId,
                                          nilai: listNilai);
                                  if (result.statusCode == 200 ||
                                      result.statusCode == 201) {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    CustomFlushBar.successFlushBar(
                                      'Berhasil',
                                      context,
                                    );
                                    await penilaian.addNilai(0);
                                  } else {
                                    Navigator.pop(context);
                                    CustomFlushBar.errorFlushBar(
                                      'Tidak dapat menilai, jika siswa mengisi jawaban yang kosong tidak bisa dibenarkan',
                                      context,
                                    );
                                  }
                                }
                              },
                              onTapSecondAction: () {
                                Navigator.pop(context);
                              },
                            ));
                  }
                },
                child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                    child: Text('Selesai',
                        style: descBlack.copyWith(
                            fontSize: S.w / 26,
                            color: SiswamediaTheme.green)))),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: StreamBuilder<Map<QuizAnswerData, bool>>(
            stream: penilaian.currentAssessment.asStream(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                // print(snapshot.data);
                var key = snapshot.data.keys.toList();
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: S.w * .05),
                  child: Column(
                    children: [
                      Consumer<QuizProviderKelas>(
                        builder: (context, state, child) => SizedBox(
                          height: 50,
                          width: S.w,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.keys.length,
                            itemBuilder: (context, index) => Container(
                                height: 40,
                                width: 40,
                                margin: EdgeInsets.only(
                                    left: index == 0 ? S.w * .05 : 6.0,
                                    top: 6,
                                    right:
                                        index == snapshot.data.keys.length - 1
                                            ? S.w * .05
                                            : 6,
                                    bottom: 6),
                                decoration: BoxDecoration(
                                    border: myIndex == index
                                        ? Border.all(
                                            color: SiswamediaTheme.green)
                                        : snapshot.data[key[index]]
                                            ? null
                                            : Border.all(color: Colors.red),
                                    borderRadius: BorderRadius.circular(12),
                                    color: snapshot.data[key[index]]
                                        ? SiswamediaTheme.green
                                        : Colors.white),
                                child: Material(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.circular(12),
                                  child: InkWell(
                                    onTap: () {
                                      setState(() => myIndex = index);
                                    },
                                    child: Container(
                                      child: Center(
                                          child: Text('${index + 1}',
                                              style: TextStyle(
                                                  color:
                                                      snapshot.data[key[index]]
                                                          ? Colors.white
                                                          : myIndex == index
                                                              ? SiswamediaTheme
                                                                  .green
                                                              : Colors.red,
                                                  fontSize: 14))),
                                    ),
                                  ),
                                )),
                          ),
                        ),
                      ),
                      (key[myIndex].image != '-' && key[myIndex].image != '0')
                          ? Stack(
                              children: [
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.grey[200],
                                  enabled: true,
                                  child: Container(
                                    height: 170,
                                    width: 170,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color: Colors.grey[300]),
                                  ),
                                ),
                                Container(
                                  height: 170,
                                  width: 170,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: SiswamediaTheme.white),
                                  foregroundDecoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      image: DecorationImage(
                                        // fit: BoxFit.cover,
                                        image: NetworkImage(key[myIndex].image),
                                      )),
                                ),
                              ],
                            )
                          : Container(
                              height: 0,
                            ),
                      Card(
                        margin: EdgeInsets.symmetric(
                            horizontal: S.w * .03, vertical: S.w * .02),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Container(
                          width: S.w * .94,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: S.w * .03, horizontal: S.w * .05),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Soal',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: S.w / 22,
                                            fontWeight: FontWeight.w600)),
                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: S.w * .03,
                                            vertical: S.w * .01),
                                        decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius: radius(4),
                                        ),
                                        child: Center(
                                            child: Text(
                                          'Bobot: ${widget.data.firstWhere((element) => element.id == key[myIndex].id).score}',
                                          style: boldBlack.copyWith(
                                              fontSize: S.w / 32,
                                              color: Colors.white),
                                        )))
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 20),
                                  child: Text(
                                      snapshot.data.keys
                                          .toList()[myIndex]
                                          .questionDescription,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      if (key[myIndex].questionType == 'ESSAY')
                        Card(
                          margin: EdgeInsets.symmetric(
                              horizontal: S.w * .03, vertical: S.w * .02),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Container(
                            width: S.w * .94,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: S.w * .03, horizontal: S.w * .05),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Jawaban',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: S.w / 22,
                                          fontWeight: FontWeight.w600)),
                                  Padding(
                                    padding: EdgeInsets.symmetric(vertical: 20),
                                    child: Text(
                                        key[myIndex]
                                                .quizAnswer
                                                .answerDescription ??
                                            'tidak ada jawaban',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      if (key[myIndex].quizAnswer.answerDescription == '')
                        Text('Jawaban user kosong pada soal ini'),
                      if ((key[myIndex].quizPgReferences.isNotEmpty))
                        Consumer<QuizProviderKelas>(
                          builder: (context, state, child) {
                            List<String> sampleData;
                            if (key[myIndex].quizPgReferences.isNotEmpty) {
                              sampleData = <String>[
                                key[myIndex].quizPgReferences[0].pgDescription,
                                key[myIndex].quizPgReferences[1].pgDescription,
                                key[myIndex].quizPgReferences[2].pgDescription,
                              ];
                              if (key[myIndex].quizPgReferences.length > 3) {
                                sampleData.add(
                                  key[myIndex]
                                      .quizPgReferences[3]
                                      .pgDescription,
                                );
                                if (key[myIndex].quizPgReferences.length > 4) {
                                  sampleData.add(
                                    key[myIndex]
                                        .quizPgReferences[4]
                                        .pgDescription,
                                  );
                                }
                              }
                            }
                            return Container(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: sampleData.map((opt) {
                                  // print(key[myIndex].correctAnswer);
                                  return Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 0, vertical: 7),
                                    decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(.2),
                                              offset: Offset(0, 0),
                                              spreadRadius: 2,
                                              blurRadius: 2)
                                        ],
                                        borderRadius: BorderRadius.circular(22),
                                        color: (key[myIndex].correctAnswer ==
                                                opt)
                                            ? Color(0xffE8F5D6)
                                            : (key[myIndex]
                                                        .quizAnswer
                                                        .answerDescription ==
                                                    opt)
                                                ? key[myIndex].correctAnswer ==
                                                        key[myIndex]
                                                            .quizAnswer
                                                            .answerDescription
                                                    ? Color(0xffE8F5D6)
                                                    : Colors.red
                                                : Colors.white),
                                    child: Material(
                                      color: Colors.transparent,
                                      borderRadius: BorderRadius.circular(22),
                                      child: InkWell(
                                        onTap: () {},
                                        child: Container(
                                          width: S.w,
                                          padding: EdgeInsets.all(16),
                                          child: Row(
                                            children: [
                                              Text(QuizChoice.choice[
                                                  sampleData.indexOf(opt)]),
                                              Expanded(
                                                child: Row(
                                                  children: [
                                                    (opt.toString().contains(
                                                                'https://') ||
                                                            opt
                                                                .toString()
                                                                .contains(
                                                                    'http://'))
                                                        ? Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 16),
                                                            constraints:
                                                                BoxConstraints(
                                                                    maxWidth:
                                                                        150),
                                                            child: AspectRatio(
                                                                aspectRatio: 1,
                                                                child:
                                                                    Container(
                                                                  child: Image
                                                                      .network(
                                                                          '$opt'),
                                                                )),
                                                          )
                                                        : Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 16),
                                                            child: Text(
                                                              opt.toString(),
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize: 12),
                                                            ),
                                                          ),
                                                    SizedBox(width: S.w * .05),
                                                    if (key[myIndex]
                                                            .quizAnswer
                                                            .answerDescription ==
                                                        opt)
                                                      Container(
                                                          padding: EdgeInsets.symmetric(
                                                              horizontal: 6,
                                                              vertical: 5),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius.circular(
                                                                      4),
                                                              border: Border.all(
                                                                  color: Color(
                                                                      0xffCFCFCF),
                                                                  width: 1),
                                                              color: Color(
                                                                  0xffF6FFE9)),
                                                          child: Text('Jawaban',
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      S.w / 36))),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            );
                          },
                        ),
                      if (widget.type == QuizAssessmentType.auto &&
                          key[myIndex].questionType != 'PG')
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: S.w * .03),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: RaisedButton(
                                  color: SiswamediaTheme.green,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: S.w * .14,
                                      vertical: S.w * .03),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(S.w)),
                                  onPressed: () {
                                    penilaian.addBool(key[myIndex], true);
                                    if (!soalBenar.contains(key[myIndex].id)) {
                                      soalBenar.add(key[myIndex].id);
                                    }
                                    if (myIndex != key.length - 1) {
                                      myIndex++;
                                    }
                                  },
                                  child: Text(
                                    'Benar',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Container(
                                child: RaisedButton(
                                  color: Color(0xff4B4B4B),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: S.w * .14,
                                      vertical: S.w * .03),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(S.w)),
                                  onPressed: () {
                                    penilaian.addBool(key[myIndex], false);
                                    if (soalBenar.contains(key[myIndex].id)) {
                                      soalBenar.remove(key[myIndex].id);
                                    }
                                    if (myIndex != key.length - 1) {
                                      myIndex++;
                                    }
                                  },
                                  child: Text(
                                    'Salah',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      if (widget.type == QuizAssessmentType.manual)
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: S.w * .03),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: RaisedButton(
                                  color: SiswamediaTheme.green,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: S.w * .02,
                                      vertical: S.w * .03),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(S.w)),
                                  onPressed: () {
                                    // penilaian.addBool(key[myIndex], true);
                                    // if (!soalBenar.contains(key[myIndex].id)) {
                                    //   soalBenar.add(key[myIndex].id);
                                    // }
                                    if (myIndex != 0) {
                                      setState(() {
                                        myIndex--;
                                      });
                                    }
                                    try {
                                      if (manual.keys.where((element) =>
                                                  element == key[myIndex].id) !=
                                              null &&
                                          manual.keys
                                                  .where((element) =>
                                                      element ==
                                                      key[myIndex].id)
                                                  .first !=
                                              null) {
                                        setState(() {
                                          controller.text = manual[manual.keys
                                                  .firstWhere((element) =>
                                                      element ==
                                                      key[myIndex].id)]
                                              .toString();
                                        });
                                      } else {
                                        setState(() {
                                          controller.text = '';
                                        });
                                      }
                                    } catch (e) {
                                      setState(() {
                                        controller.text = '';
                                      });
                                    }
                                  },
                                  child: Text(
                                    'Prev',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextField(
                                  controller: controller,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  onChanged: (text) {
                                    print(text);
                                    setState(() {
                                      penilaian.addBool(key[myIndex], true);
                                      manual[key[myIndex].id] = int.parse(text);
                                    });
                                    if (controller.text == '') {
                                      setState(() {
                                        manual.remove(key[myIndex].id);
                                      });
                                    }
                                  },
                                  style: descBlack.copyWith(fontSize: S.w / 30),
                                  maxLines: 1,
                                  minLines: 1,
                                  decoration: InputDecoration(
                                    hintText: 'Masukkan nilai..',
                                    hintStyle: descBlack.copyWith(
                                        fontSize: S.w / 30,
                                        color: Color(0xffB9B9B9)),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 25),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xffD8D8D8)),
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: SiswamediaTheme.green),
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                  ),
                                ),
                              ),
                              if (myIndex == widget.data.length - 1)
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: S.w * .02,
                                      vertical: S.w * .03),
                                ),
                              if (myIndex != widget.data.length - 1)
                                Container(
                                  child: RaisedButton(
                                    elevation: myIndex == widget.data.length - 1
                                        ? 0
                                        : 4,
                                    color: myIndex == widget.data.length - 1
                                        ? Colors.transparent
                                        : Color(0xff4B4B4B),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: S.w * .02,
                                        vertical: S.w * .03),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(S.w)),
                                    onPressed: () {
                                      // penilaian.addBool(key[myIndex], false);
                                      // if (soalBenar.contains(key[myIndex].id)) {
                                      //   soalBenar.remove(key[myIndex].id);
                                      // }

                                      if (myIndex != key.length - 1) {
                                        setState(() {
                                          myIndex++;
                                        });
                                      }
                                      try {
                                        if (manual.keys.where((element) =>
                                                    element ==
                                                    key[myIndex].id) !=
                                                null &&
                                            manual.keys
                                                    .where((element) =>
                                                        element ==
                                                        key[myIndex].id)
                                                    .first !=
                                                null) {
                                          setState(() {
                                            controller.text = manual[manual.keys
                                                    .firstWhere((element) =>
                                                        element ==
                                                        key[myIndex].id)]
                                                .toString();
                                          });
                                        } else {
                                          setState(() {
                                            controller.text = '';
                                          });
                                        }
                                      } catch (e) {
                                        setState(() {
                                          controller.text = '';
                                        });
                                      }
                                    },
                                    child: Text(
                                      'Next',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        )
                    ],
                  ),
                );
              }
              return Center(child: CircularProgressIndicator());
            }),
      ),
    );
  }
}
