part of '_ujian.dart';

class QuizKomposisi extends StatefulWidget {
  QuizKomposisi({this.type = 'BANK SOAL'});
  final String type;

  @override
  _QuizKomposisiState createState() => _QuizKomposisiState();
}

class _QuizKomposisiState extends State<QuizKomposisi> {
  int myIndex = 0;
  Map<String, dynamic> correction = <String, dynamic>{};
  Map<String, dynamic> currentAnswer = <String, dynamic>{};
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  bool addAll = false;

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    return ChangeNotifierProvider(
      create: (_) => QuizState(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          brightness: Brightness.light,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
            color: SiswamediaTheme.green,
          ),
          actions: [
            if (widget.type == 'BANK SOAL')
              Center(
                child: InkWell(
                  onTap: () => showDialog<void>(
                      context: context,
                      builder: (context) {
                        String contentText =
                            'Apakah anda yakin untuk menyelesaikannya?';
                        return StatefulBuilder(
                            builder: (context, setState) => AlertDialog(
                                  title: Text('Warning!'),
                                  content: Text(contentText),
                                  actions: [
                                    FlatButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: Text('Batal'),
                                    ),
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                        data.setSelected(currentAnswer);
                                        Navigator.pop(context);
                                      },
                                      child: Text('Tentu'),
                                    )
                                  ],
                                ));
                      }),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                    child: Text(
                      'Selesai',
                      style: TextStyle(
                          color: SiswamediaTheme.green, fontSize: S.w / 30),
                    ),
                  ),
                ),
              ),
            if (widget.type == 'BANK SOAL')
              Consumer(
                builder:
                    (BuildContext context, QuizState quizState, Widget child) {
                  return Checkbox(
                      activeColor: SiswamediaTheme.green,
                      checkColor: Colors.white,
                      value: (quizState.selected == currentAnswer) &&
                          (quizState.selected.length == data.list.length),
                      onChanged: (value) {
                        if (quizState.selected.length != data.list.length) {
                          for (int i = 0; i < data.list.length; i++) {
                            quizState.adding((i + 1).toString(), data.list[i]);
                          }
                          currentAnswer = quizState.selected;
                        } else {
                          print('ada');
                          quizState.set(<String, dynamic>{});
                          currentAnswer = quizState.selected;
                        }
                      });
                },
              ),
          ],
          title: Text(
            'Preview Soal',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: S.w / 25,
                color: Colors.black),
          ),
        ),
        key: _scaffoldKey,
        backgroundColor: SiswamediaTheme.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: S.w * .05),
            child: Column(
              children: [
                Consumer<QuizState>(
                  builder: (context, state, child) => SizedBox(
                    height: 50,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: data.list.length,
                      itemBuilder: (context, index) => Container(
                          height: 40,
                          width: 40,
                          margin: EdgeInsets.only(
                              left: index == 0 ? S.w * .05 : 6.0,
                              top: 6,
                              right:
                                  index == data.list.length - 1 ? S.w * .05 : 6,
                              bottom: 6),
                          decoration: BoxDecoration(
                              border: myIndex == index
                                  ? Border.all(
                                      color: state.selected[
                                                  (index + 1).toString()] !=
                                              null
                                          ? Colors.black
                                          : SiswamediaTheme.green)
                                  : null,
                              borderRadius: BorderRadius.circular(12),
                              color:
                                  state.selected[(index + 1).toString()] != null
                                      ? SiswamediaTheme.green
                                      : Colors.white),
                          child: Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(12),
                            child: InkWell(
                              onTap: () {
                                setState(() => myIndex = index);
                              },
                              child: Container(
                                child: Center(
                                    child: Text('${index + 1}',
                                        style: TextStyle(
                                            color: state.selected[(index + 1)
                                                        .toString()] !=
                                                    null
                                                ? Colors.white
                                                : SiswamediaTheme.green,
                                            fontSize: 14))),
                              ),
                            ),
                          )),
                    ),
                  ),
                ),
                data.list[myIndex].attpertanyaan.contains('http')
                    ? Container(
                        child: Image.network(
                          data.list[myIndex].attpertanyaan,
                          height: 170,
                          width: 170,
                        ),
                      )
                    : Container(
                        height: 0,
                      ),
                Card(
                  margin: EdgeInsets.symmetric(
                      horizontal: S.w * .03, vertical: S.w * .02),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    width: S.w * .94,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .03, horizontal: S.w * .05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Soal',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: S.w / 22,
                                  fontWeight: FontWeight.w600)),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: Text(data.list[myIndex].pertanyaan,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500)),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: Text(
                                'Jawaban: ${data.list[myIndex].jawaban}',
                                style: TextStyle(
                                    color: SiswamediaTheme.green,
                                    fontWeight: FontWeight.w500)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                // Consumer<QuizState>(
                //   builder: (context, state, child) {
                //     List<dynamic> sampleData;
                //     sampleData = [
                //       data.list[myIndex].pilihana,
                //       data.list[myIndex].pilihanb,
                //       data.list[myIndex].pilihanc,
                //     ];
                //     if (data.list[myIndex].pilihand != '-') {
                //       sampleData.add(data.list[myIndex].pilihand);
                //       if (data.list[myIndex].pilihane != '-') {
                //         sampleData.add(data.list[myIndex].pilihane);
                //       }
                //     }
                //
                //     return Container(
                //       padding: EdgeInsets.all(10),
                //       child: Column(
                //         mainAxisAlignment: MainAxisAlignment.start,
                //         children: sampleData.map((opt) {
                //           return Container(
                //             height: 70,
                //             margin: EdgeInsets.symmetric(
                //                 horizontal: 0, vertical: 7),
                //             decoration: BoxDecoration(
                //               boxShadow: [
                //                 BoxShadow(
                //                     color: Colors.grey.withOpacity(.2),
                //                     offset: Offset(0, 0),
                //                     spreadRadius: 2,
                //                     blurRadius: 2)
                //               ],
                //               borderRadius: BorderRadius.circular(22),
                //               color: state.selected[(myIndex + 1).toString()] ==
                //                       opt
                //                   ? Color(0xffE8F5D6)
                //                   : Colors.white,
                //             ),
                //             child: Material(
                //               color: Colors.transparent,
                //               borderRadius: BorderRadius.circular(22),
                //               child: InkWell(
                //                 onTap: () {
                //                   /*
                //                   state.adding((myIndex + 1).toString(), opt);
                //                   currentAnswer = state.selected;
                //                   correction[(myIndex + 1).toString()] =
                //                       data.list[myIndex].jawaban.substring(3);*/
                //                 },
                //                 child: Container(
                //                   padding: EdgeInsets.all(16),
                //                   child: Row(
                //                     children: [
                //                       Text(QuizChoice
                //                           .choice[sampleData.indexOf(opt)]),
                //                       Expanded(
                //                         child: Container(
                //                           margin: EdgeInsets.only(left: 16),
                //                           child: Text(
                //                             opt,
                //                             style: TextStyle(
                //                                 color: Colors.black,
                //                                 fontWeight: FontWeight.w500,
                //                                 fontSize: 12),
                //                           ),
                //                         ),
                //                       )
                //                     ],
                //                   ),
                //                 ),
                //               ),
                //             ),
                //           );
                //         }).toList(),
                //       ),
                //     );
                //   },
                // ),
                Container(
                  width: S.w * .94,
                  margin: EdgeInsets.symmetric(
                      horizontal: S.w * .03, vertical: S.w * .02),
                  decoration: SiswamediaTheme.whiteRadiusShadow,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: RaisedButton(
                          color: Colors.white,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                          onPressed: () {
                            setState(() {
                              myIndex--;
                            });
                          },
                          child: Text(
                            'back',
                            style: TextStyle(
                                color: SiswamediaTheme.green,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      if (widget.type == 'BANK SOAL')
                        Consumer<QuizState>(
                          builder: (context, state, child) => Row(
                            children: [
                              Text('Pilih'),
                              Checkbox(
                                  activeColor: SiswamediaTheme.green,
                                  checkColor: Colors.white,
                                  value: state
                                          .selected[(myIndex + 1).toString()] ==
                                      data.list[myIndex],
                                  onChanged: (value) {
                                    if (state.selected[
                                            (myIndex + 1).toString()] !=
                                        data.list[myIndex]) {
                                      state.adding((myIndex + 1).toString(),
                                          data.list[myIndex]);
                                      currentAnswer = state.selected;
                                    } else {
                                      state.remove((myIndex + 1).toString());
                                      currentAnswer = state.selected;
                                    }
                                  }),
                            ],
                          ),
                        ),
                      myIndex == data.list.length - 1
                          ? widget.type == 'BANK SOAL'
                              ? Container(
                                  child: RaisedButton(
                                    elevation: 0,
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    onPressed: () {
                                      showDialog<void>(
                                          context: context,
                                          builder: (context) {
                                            String contentText =
                                                'Apakah anda yakin untuk menyelesaikannya?';
                                            return StatefulBuilder(
                                                builder: (context, setState) =>
                                                    AlertDialog(
                                                      title: Text(
                                                        'Warning!',
                                                        style: semiBlack.copyWith(
                                                            fontSize: S.w / 28,
                                                            color:
                                                                SiswamediaTheme
                                                                    .green),
                                                      ),
                                                      content:
                                                          Text(contentText),
                                                      actions: [
                                                        TextButton(
                                                          onPressed: () =>
                                                              Navigator.pop(
                                                                  context),
                                                          child: Text('Batal',
                                                              style: semiBlack.copyWith(
                                                                  fontSize:
                                                                      S.w / 28,
                                                                  color: SiswamediaTheme
                                                                      .green)),
                                                        ),
                                                        TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(
                                                                context);
                                                            data.setSelected(
                                                                currentAnswer);
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child: Text('Tentu'),
                                                        )
                                                      ],
                                                    ));
                                          });
                                    },
                                    child: Text(
                                      'Finish',
                                      style: TextStyle(
                                          color: SiswamediaTheme.green,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                )
                              : SizedBox()
                          : Container(
                              child: RaisedButton(
                                elevation: 0,
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                onPressed: () {
                                  setState(() {
                                    myIndex++;
                                  });
                                },
                                child: Text(
                                  'next',
                                  style: TextStyle(
                                      color: SiswamediaTheme.green,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class QuizState with ChangeNotifier {
  bool _progress;
  Map<String, dynamic> _selected = <String, dynamic>{};
  final PageController controller = PageController();

  bool get progress => _progress;
  Map<String, dynamic> get selected => _selected;

  set progress(bool newValue) {
    _progress = newValue;
    notifyListeners();
  }

  void set(Map<String, dynamic> newValue) {
    _selected = newValue;
    notifyListeners();
  }

  void adding(String key, QuizItem value) {
    _selected[key] = value;
    notifyListeners();
  }

  void remove(String oldValue) {
    _selected.remove(oldValue);
    notifyListeners();
  }

  void nextPage() async {
    await controller.nextPage(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }
}
