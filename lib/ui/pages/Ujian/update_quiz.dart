part of '_ujian.dart';

class UpdateQuiz extends StatefulWidget {
  UpdateQuiz({
    this.title,
    this.id,
    this.startDate,
    this.duration,
  });

  final int id;
  final int duration;
  final String title;
  final String startDate;

  @override
  _UpdateQuizState createState() => _UpdateQuizState();
}

class _UpdateQuizState extends State<UpdateQuiz> {
  final authState = GlobalState.auth();
  final classState = GlobalState.classes();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  final List<CourseByClassData> _selectedCategorys = <CourseByClassData>[];
  String _start;
  String classId;
  bool loading = false;

  List<CourseByClassData> course = [];

  TextEditingController controller = TextEditingController();
  TextEditingController duration = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController _tanggalMulai;

  @override
  void initState() {
    super.initState();
    _tanggalMulai = TextEditingController();
    controller.text = widget.title;
    duration.text = widget.duration.toString();
    _tanggalMulai.text = widget.startDate.substring(0, 19) + 'Z';
    _start = widget.startDate;
  }

  Future<void> _getCourseByClass(int id) async {
    var result = await ClassServices.getCourseByClass(classId: id);
    if (result.statusCode == 200) {
      setState(() {
        course = result.data;
      });
    } else {
      setState(() {
        course = [];
      });
    }
  }

  // void fetchSoal() {
  //   Map<String, dynamic> ko =  Map<String, dynamic>();
  // }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    return WillPopScope(
      onWillPop: () {
        data.setSelected(<String, dynamic>{});
        return Future.value(true);
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: Color(0xfffefefe),
        appBar: AppBar(
          shadowColor: Colors.grey.withOpacity(.2),
          brightness: Brightness.light,
          leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                data.setSelected(<String, dynamic>{});
                Navigator.pop(context);
              }),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Update Quiz',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: S.w / 22,
                  fontWeight: FontWeight.w600)),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Padding(
              //   padding: EdgeInsets.only(
              //       left: S.w * .05, right: S.w * .05, top: 10.0),
              //   child: Text('Soal',
              //       style: TextStyle(
              //           fontSize: S.w / 25,
              //           color: Colors.black87,
              //           fontWeight: FontWeight.w600)),
              // ),
              // FutureBuilder<QuizModel>(
              //     future: MateriQuizServices.getQuizData(
              //         info.kelas, info.matapelajaran, info.materi),
              //     builder: (context, snapshot) {
              //       if (snapshot.hasData) {
              //         return InkWell(
              //           onTap: () {
              //             // print(snapshot.data.rows);
              //             data.setQuestion(snapshot.data.rows
              //                 .where((element) =>
              //                     element.materi == data.item.materi)
              //                 .toList());
              //             Navigator.push(
              //                 context,
              //                 MaterialPageRoute<void>(
              //                     builder: (_) => QuizKomposisi()));
              //           },
              //           child: Container(
              //             height: S.w * .25,
              //             padding: EdgeInsets.symmetric(
              //                 horizontal: S.w * .05, vertical: S.w * .03),
              //             margin: EdgeInsets.symmetric(
              //                 horizontal: S.w * .05, vertical: S.w * .02),
              //             decoration: BoxDecoration(
              //                 color: Colors.white,
              //                 borderRadius: BorderRadius.circular(12),
              //                 boxShadow: [
              //                   BoxShadow(
              //                     offset: Offset(2, 2),
              //                     spreadRadius: 2,
              //                     blurRadius: 2,
              //                     color: Colors.grey.withOpacity(.2),
              //                   )
              //                 ]),
              //             child: Column(
              //               crossAxisAlignment: CrossAxisAlignment.start,
              //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //               children: [
              //                 Text(
              //                     '${data.item.kelas} ${data.item.matapelajaran}',
              //                     style: TextStyle(
              //                         color: Colors.black87,
              //                         fontWeight: FontWeight.w700,
              //                         fontSize: S.w / 25)),
              //                 Text('Kode : ${data.item.materi}-1928',
              //                     style: TextStyle(
              //                         color: Colors.black54,
              //                         fontWeight: FontWeight.w600,
              //                         fontSize: S.w / 30)),
              //                 Row(
              //                   mainAxisAlignment:
              //                       MainAxisAlignment.spaceBetween,
              //                   children: [
              //                     Text(
              //                         '${snapshot.data.rows.where((element) => element.materi == data.item.materi).toList().length} soal - Dipilih ${data.selected.length}'),
              //                     Container(
              //                       height: S.w * .07,
              //                       width: S.w * .2,
              //                       decoration: BoxDecoration(
              //                         color: SiswamediaTheme.green,
              //                         borderRadius: BorderRadius.circular(6),
              //                       ),
              //                       child: Material(
              //                         color: Colors.transparent,
              //                         child: InkWell(
              //                           onTap: () {
              //                             Navigator.pop(context);
              //                             data.setSelected(<String, dynamic>{});
              //                           },
              //                           child: Center(
              //                               child: Text('Ganti',
              //                                   style: TextStyle(
              //                                       color: Colors.white,
              //                                       fontWeight: FontWeight.w600,
              //                                       fontSize: S.w / 32))),
              //                         ),
              //                       ),
              //                     ),
              //                   ],
              //                 )
              //               ],
              //             ),
              //           ),
              //         );
              //       }
              //       return Center(child: CircularProgressIndicator());
              //     }),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .09, vertical: S.w * .02),
                child: Column(
                  children: [
                    TextField(
                      controller: controller,
                      decoration: InputDecoration.collapsed(
                          hintText: 'Nama Kuis',
                          hintStyle: TextStyle(
                              fontSize: S.w / 25,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey.withOpacity(.4))),
                    ),
                    SizedBox(height: 10),
                    Divider(color: Colors.grey),
                    DateTimePicker(
                      controller: _tanggalMulai,
                      type: DateTimePickerType.dateTime,
                      dateMask: 'MMM dd, yyyy - HH:mm ',
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2100),
                      icon: Icon(Icons.calendar_today_outlined),
                      dateLabelText: 'Tanggal mulai',
                      onChanged: (val) => setState(() => _start = val),
                    ),
                    SizedBox(height: 25),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Waktu Pengerjaan (menit)'),
                        Container(
                          width: S.w / 3,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            style: TextStyle(
                              fontSize: S.w / 25,
                            ),
                            controller: duration,
                            textAlign: TextAlign.end,
                            decoration: InputDecoration.collapsed(
                                hintText: '30',
                                hintStyle: TextStyle(
                                    fontSize: S.w / 25,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey.withOpacity(.4))),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Divider(color: Colors.grey),
                  ],
                ),
              ),
              /*Container(
                  height: width *
                      .2 *
                      ((auth.allCourse
                                  .where((element) =>
                                      element.classId.toString() == _choose2)
                                  .length +
                              1) ~/
                          2),
                  child: GridView.count(
                    crossAxisCount: 2,
                    children: auth.allCourse
                        .where(
                            (element) => element.classId.toString() == _choose2)
                        .map(
                          (e) => CheckboxListTile(
                            value: _selecteCategorys.contains(e.id),
                            onChanged: (f) {
                              _onCategorySelected(f, e.id);
                            },
                            title: Text(e.name,
                                style: TextStyle(fontSize: width / 30)),
                          ),
                        )
                        .toList(),
                  )),*/
              ///acak soal

              ///hide or no
              Composer(),
              SizedBox(height: 10),
              loading
                  ? Center(child: CircularProgressIndicator())
                  : Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .01, horizontal: S.w * .05),
                      child: Container(
                        width: MediaQuery.of(context).size.width * .9,
                        height: S.w * .12,
                        decoration: BoxDecoration(
                          color: SiswamediaTheme.green,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () async {
                              if (controller.text != '' &&
                                  duration.text != '') {
                                setState(() {
                                  loading = true;
                                });
                                await updateQuiz().then((value) {
                                  if (value.isSuccessOrCreated) {
                                    setState(() {
                                      loading = false;
                                    });
                                    showDialog<void>(
                                        context: context,
                                        builder: (context) => StatefulBuilder(
                                              builder: (BuildContext context,
                                                  void Function(void Function())
                                                      setState) {
                                                return Dialog(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              12)),
                                                  child: Container(
                                                    height: S.w * .65,
                                                    width: S.w * .65,
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: S.w * .05,
                                                            horizontal:
                                                                S.w * .1),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        Image.asset(
                                                            'assets/izin.png',
                                                            height: S.w * .25,
                                                            width: S.w * .25,
                                                            fit:
                                                                BoxFit.contain),
                                                        Text(
                                                            'Yey Quiz Berhasil diubah!'),
                                                        Container(
                                                          height: S.w * .1,
                                                          width: S.w * .18,
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4),
                                                            color:
                                                                SiswamediaTheme
                                                                    .green,
                                                          ),
                                                          child: InkWell(
                                                            onTap: () {
                                                              pop(context);
                                                              // pop(context);
                                                              data.setSelected(<
                                                                  String,
                                                                  dynamic>{});
                                                            },
                                                            child: Center(
                                                                child: Text(
                                                                    'Close',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white))),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                            )).then(
                                      (value) {
                                        pop(context);
                                      },
                                    );
                                  } else {
                                    setState(() {
                                      loading = false;
                                    });
                                    CustomFlushBar.errorFlushBar(
                                      'Terjadi Kesalahan',
                                      context,
                                    );
                                  }
                                });
                              } else if (controller.text == '') {
                                CustomFlushBar.errorFlushBar(
                                  'Nama ujian harus diisi',
                                  context,
                                );
                              } else if (duration.text == '') {
                                CustomFlushBar.errorFlushBar(
                                  'Durasi harus diisi',
                                  context,
                                );
                              }
                            },
                            child: Center(
                              child: Text('Update Kuis',
                                  style: TextStyle(
                                      fontSize: S.w / 30,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white)),
                            ),
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  void _onCategorySelected(bool selected, CourseByClassData value) {
    if (selected == true) {
      setState(() {
        _selectedCategorys.add(value);
      });
    } else {
      setState(() {
        _selectedCategorys.remove(value);
      });
    }
  }

  Future<int> updateQuiz() async {
    try {
      var result = await QuizServices.updateQuiz(widget.id, <String, dynamic>{
        'name': controller.text,
        'total_time': duration.text.toInteger,
        'start_date': DateTime.parse(_start).toUtc().toIso8601String(),
      });
      return result.statusCode;
    } catch (e) {
      print(e);
      return 400;
    }
  }
}
