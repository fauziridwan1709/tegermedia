part of '_ujian.dart';

class QuizStartEssay extends StatefulWidget {
  final int duration;
  QuizStartEssay({this.duration});

  @override
  _QuizStartEssayState createState() => _QuizStartEssayState();
}

class _QuizStartEssayState extends State<QuizStartEssay> {
  Timer timer;
  int _timerValue;

  int myIndex = 0;
  Map<String, dynamic> correction = <String, dynamic>{};
  Map<String, dynamic> currentAnswer = <String, dynamic>{};
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _timerValue = widget.duration * 60;
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      startTimer();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  Timer _timer;

  void startTimer() async {
    print('kita');
    _timer = Timer.periodic(
        Duration(seconds: 1),
        (Timer timer) => setState(() {
              print(_timerValue);
              print('kita');
              if (_timerValue < 1) {
                _timer.cancel();
                showDialog<void>(
                    context: context,
                    builder: (context) {
                      return Dialog(
                        child: Container(
                            height: S.w * .63,
                            width: S.w / 2.4,
                            padding: EdgeInsets.symmetric(
                                horizontal: S.w * .05, vertical: S.w * .05),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Waktu Habis!',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: S.w / 28,
                                        fontWeight: FontWeight.w700)),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: S.w * .01),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      color: SiswamediaTheme.green,
                                    ),
                                    height: S.w / 10,
                                    width: S.w / 3,
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.pop(context);
                                        // Navigator.push(
                                        //     context,
                                        //     MaterialPageRoute<void>(
                                        //         builder: (_) => ResultPage(
                                        //           duration: _timerValue,
                                        //           currentAnswer:
                                        //           currentAnswer,
                                        //           rightAnswer: correction,
                                        //         )));
                                      },
                                      child: Center(
                                        child: Text('Lihat Nilai',
                                            style:
                                                TextStyle(color: Colors.white)),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )),
                      );
                    }).then(
                  (value) {
                    Navigator.pop(context);
                    _timer.cancel();
                  },
                  // Navigator.push(
                  // context,
                  // MaterialPageRoute<void>(
                  //     builder: (_) => ResultPage(
                  //       duration: _timerValue,
                  //       currentAnswer: currentAnswer,
                  //       rightAnswer: correction,
                  //     )))
                );
                Navigator.pop(context);
                _timer.cancel();
                // Navigator.pushReplacement(
                //     context, MaterialPageRoute<void>(builder: (context) => Layout()));
              } else {
                _timerValue = _timerValue - 1;
                print(_timerValue);
              }
            }));
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    var quiz = data.listQuiz;
    return ChangeNotifierProvider(
      create: (_) => QuizKelas(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          brightness: Brightness.light,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: SiswamediaTheme.green,
            onPressed: () {
              _timer.cancel();
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          actions: [
            Center(
              child: InkWell(
                onTap: () => showDialog<void>(
                    context: context,
                    builder: (context) {
                      var contentText =
                          'Apakah anda yakin untuk menyelesaikannya?';
                      return StatefulBuilder(
                          builder: (context, setState) => AlertDialog(
                                title: Text('Warning!'),
                                content: Text(contentText),
                                actions: [
                                  FlatButton(
                                    onPressed: () => Navigator.pop(context),
                                    child: Text('Batal'),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      _timer.cancel();
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      /*Navigator.push(
                                          context,
                                          MaterialPageRoute<void>(
                                              builder: (_) => ResultPage(
                                                    currentAnswer:
                                                        currentAnswer,
                                                    rightAnswer: correction,
                                                  )));*/
                                    },
                                    child: Text('Tentu'),
                                  )
                                ],
                              ));
                    }),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                  child: Text(
                    'Selesai',
                    style: TextStyle(
                        color: SiswamediaTheme.green, fontSize: S.w / 30),
                  ),
                ),
              ),
            )
          ],
          title: Text(
            'Kuis',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: S.w / 25,
                color: Colors.black),
          ),
        ),
        key: _scaffoldKey,
        backgroundColor: SiswamediaTheme.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: S.w * .05),
            child: Column(
              children: [
                Consumer<QuizKelas>(
                  builder: (context, state, child) => SizedBox(
                    height: 50,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: quiz.length,
                      itemBuilder: (context, index) => Container(
                          height: 40,
                          width: 40,
                          margin: EdgeInsets.only(
                              left: index == 0 ? S.w * .05 : 6.0,
                              top: 6,
                              right: index == quiz.length - 1 ? S.w * .05 : 6,
                              bottom: 6),
                          decoration: BoxDecoration(
                              border: myIndex == index
                                  ? Border.all(color: SiswamediaTheme.green)
                                  : null,
                              borderRadius: BorderRadius.circular(12),
                              color:
                                  state.selected[(index + 1).toString()] != null
                                      ? SiswamediaTheme.green
                                      : Colors.white),
                          child: Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(12),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  myIndex = index;
                                  controller.text =
                                      currentAnswer[(myIndex + 1).toString()]
                                          .toString();
                                });
                                print(currentAnswer[(myIndex + 1).toString()]);
                              },
                              child: Container(
                                child: Center(
                                    child: Text('${index + 1}',
                                        style: TextStyle(
                                            color: state.selected[(index + 1)
                                                        .toString()] !=
                                                    null
                                                ? Colors.white
                                                : SiswamediaTheme.green,
                                            fontSize: 14))),
                              ),
                            ),
                          )),
                    ),
                  ),
                ),
                (quiz[myIndex].image == '-')
                    ? Container(
                        height: 0,
                      )
                    : Container(
                        child: Image(
                          image: NetworkImage(
                            quiz[myIndex].image,
                          ),
                          height: 170,
                          width: 170,
                        ),
                      ),
                Card(
                  margin: EdgeInsets.symmetric(
                      horizontal: S.w * .03, vertical: S.w * .02),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    width: S.w * .94,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .03, horizontal: S.w * .05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Soal',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: S.w / 22,
                                  fontWeight: FontWeight.w600)),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: Text(quiz[myIndex].questionDesc,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Consumer<QuizKelas>(
                  builder: (context, state, child) {
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: S.w * .03),
                      child: TextField(
                        controller: controller,
                        onChanged: (text) {
                          state.adding((myIndex + 1).toString(), text);
                          currentAnswer = state.selected;
                          correction[(myIndex + 1).toString()] =
                              quiz[myIndex].correctAnswer;
                        },
                        style: descBlack.copyWith(fontSize: S.w / 30),
                        maxLines: 4,
                        minLines: 3,
                        decoration: InputDecoration(
                          hintText: 'jawaban..',
                          hintStyle: descBlack.copyWith(
                              fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 25),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffD8D8D8)),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: SiswamediaTheme.green),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: RaisedButton(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                          child: Text(
                            'back',
                            style: TextStyle(
                                color: SiswamediaTheme.green,
                                fontWeight: FontWeight.w600),
                          ),
                          onPressed: () {
                            setState(() {
                              myIndex--;
                            });
                          },
                        ),
                      ),
                      Container(
                        height: S.w * .1,
                        padding: EdgeInsets.symmetric(
                            horizontal: S.w * .05, vertical: S.w * .01),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(S.w),
                        ),
                        child: Container(
                          height: 60,
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                    '${_timerValue ~/ 60}:${_timerValue % 60}',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black)),
                              ),
                              Positioned(
                                  right: S.w * .09,
                                  top: 0,
                                  bottom: 0,
                                  child: SizedBox(
                                    height: 40,
                                    width: 40,
                                    child:
                                        Icon(Icons.timer, color: Colors.white),
                                  ))
                            ],
                          ),
                        ),
                      ),
                      myIndex == quiz.length - 1
                          ? Container(
                              child: RaisedButton(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                onPressed: () {
                                  showDialog<void>(
                                      context: context,
                                      builder: (context) {
                                        String contentText =
                                            'Apakah anda yakin untuk menyelesaikannya?';
                                        return StatefulBuilder(
                                            builder: (context, setState) =>
                                                AlertDialog(
                                                  title: Text('Warning!'),
                                                  content: Text(contentText),
                                                  actions: [
                                                    FlatButton(
                                                      onPressed: () =>
                                                          Navigator.pop(
                                                              context),
                                                      child: Text('Batal'),
                                                    ),
                                                    FlatButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                        Navigator.pop(context);
                                                        /* Navigator.push(
                                                            context,
                                                            MaterialPageRoute<void>(
                                                                builder: (_) =>
                                                                    ResultPage(
                                                                      currentAnswer:
                                                                          currentAnswer,
                                                                      rightAnswer:
                                                                          correction,
                                                                    )));*/
                                                      },
                                                      child: Text('Tentu'),
                                                    )
                                                  ],
                                                ));
                                      });
                                },
                                child: Text(
                                  'Finish',
                                  style: TextStyle(
                                      color: SiswamediaTheme.green,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            )
                          : Container(
                              child: RaisedButton(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                onPressed: () {
                                  setState(() {
                                    myIndex++;
                                  });
                                },
                                child: Text(
                                  'next',
                                  style: TextStyle(
                                      color: SiswamediaTheme.green,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
