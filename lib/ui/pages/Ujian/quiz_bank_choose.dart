part of '_ujian.dart';

class BankChoose extends StatefulWidget {
  BankChoose({this.listClass, this.type, this.courseId});

  final ModelListCourse listClass;
  final String type;
  final int courseId;

  @override
  _BankChooseState createState() => _BankChooseState();
}

class _BankChooseState extends State<BankChoose> {
  final authState = GlobalState.auth();
  final classState = GlobalState.classes();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  final List<CourseByClassData> _selectedCategorys = <CourseByClassData>[];
  DateTime start = DateTime.now();
  DateTime end = DateTime.now();
  String _choose1;
  String _choose2 = '-1';
  String classId;
  bool loading = false;
  int _radioValue1 = 0;
  int _radioValue2 = 0;

  List<CourseByClassData> course = [];

  TextEditingController controller = TextEditingController();
  TextEditingController duration = TextEditingController();
  TextEditingController description = TextEditingController();

  Future<void> _getCourseByClass(int id) async {
    var result = await ClassServices.getCourseByClass(classId: id);
    if (result.statusCode == 200) {
      setState(() {
        course = result.data;
      });
    } else {
      setState(() {
        course = [];
      });
    }
  }

  // void fetchSoal() {
  //   Map<String, dynamic> ko =  Map<String, dynamic>();
  // }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    // var auth = Provider.of<UserProvider>(context);
    var info = data.item;
    print(_choose2);
    return WillPopScope(
      onWillPop: () {
        data.setSelected(<String, dynamic>{});
        return Future.value(true);
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: Color(0xfffefefe),
        appBar: AppBar(
          shadowColor: Colors.grey.withOpacity(.2),
          brightness: Brightness.light,
          leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                data.setSelected(<String, dynamic>{});
                Navigator.pop(context);
              }),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Bank Soal',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: S.w / 22,
                  fontWeight: FontWeight.w600)),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    left: S.w * .05, right: S.w * .05, top: 10.0),
                child: Text('Soal',
                    style: TextStyle(
                        fontSize: S.w / 25,
                        color: Colors.black87,
                        fontWeight: FontWeight.w600)),
              ),
              FutureBuilder<QuizModel>(
                  future: MateriQuizServices.getQuizData(
                      info.kelas, info.matapelajaran, info.materi),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return InkWell(
                        onTap: () {
                          // print(snapshot.data.rows);
                          data.setQuestion(snapshot.data.rows
                              .where((element) =>
                                  element.materi == data.item.materi)
                              .toList());
                          Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                  builder: (_) => QuizKomposisi()));
                        },
                        child: Container(
                          height: S.w * .25,
                          padding: EdgeInsets.symmetric(
                              horizontal: S.w * .05, vertical: S.w * .03),
                          margin: EdgeInsets.symmetric(
                              horizontal: S.w * .05, vertical: S.w * .02),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(2, 2),
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  color: Colors.grey.withOpacity(.2),
                                )
                              ]),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  '${data.item.kelas} ${data.item.matapelajaran}',
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w700,
                                      fontSize: S.w / 25)),
                              Text('Kode : ${data.item.materi}-1928',
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontWeight: FontWeight.w600,
                                      fontSize: S.w / 30)),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                      '${snapshot.data.rows.where((element) => element.materi == data.item.materi).toList().length} soal - Dipilih ${data.selected.length}'),
                                  Container(
                                    height: S.w * .07,
                                    width: S.w * .2,
                                    decoration: BoxDecoration(
                                      color: SiswamediaTheme.green,
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                    child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                          data.setSelected(<String, dynamic>{});
                                        },
                                        child: Center(
                                            child: Text('Ganti',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: S.w / 32))),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    }
                    return Center(child: CircularProgressIndicator());
                  }),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .09, vertical: S.w * .02),
                child: Column(
                  children: [
                    TextField(
                      controller: controller,
                      decoration: InputDecoration.collapsed(
                          hintText: 'Nama Kuis',
                          hintStyle: TextStyle(
                              fontSize: S.w / 25,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey.withOpacity(.4))),
                    ),
                    SizedBox(height: 10),
                    Divider(color: Colors.grey),
                    DateTimePicker(
                        type: DateTimePickerType.dateTime,
                        dateMask: 'MMM dd, yyyy - HH:mm ',
                        firstDate: DateTime.now(),
                        lastDate: DateTime(2100),
                        icon: Icon(Icons.calendar_today_outlined),
                        dateLabelText: 'Tanggal mulai',
                        onChanged: (val) {
                          if (DateTime.now().isAfter(DateTime.parse(val))) {
                            CustomFlushBar.errorFlushBar(
                                "Anda memilih waktu yang sudah berlalu",
                                context);
                          } else {
                            setState(() => start = DateTime.parse(val));
                          }
                        }),
                    if (widget.type == 'journal')
                      Column(
                        children: <Widget>[
                          DateTimePicker(
                            type: DateTimePickerType.dateTime,
                            dateMask: 'MMM dd, yyyy - HH:mm ',
                            firstDate: DateTime(2000),
                            lastDate: DateTime(2100),
                            icon: Icon(Icons.calendar_today_outlined),
                            dateLabelText: 'Tanggal selesai',
                            onChanged: (val) =>
                                setState(() => end = DateTime.parse(val)),
                          ),
                          SizedBox(height: 25),
                        ],
                      )
                    else
                      HeightSpace(20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Waktu Pengerjaan (menit)'),
                        Container(
                          width: S.w / 3,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            style: TextStyle(
                              fontSize: S.w / 25,
                            ),
                            controller: duration,
                            textAlign: TextAlign.end,
                            decoration: InputDecoration.collapsed(
                                hintText: '30',
                                hintStyle: TextStyle(
                                    fontSize: S.w / 25,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey.withOpacity(.4))),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Divider(color: Colors.grey),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .01),
                child: Container(
                  width: S.w * .45,
                  height: S.w * .1,
                  padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(S.w),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                        icon: Icon(
                          Icons.keyboard_arrow_down,
                          color: SiswamediaTheme.green,
                        ),
                        hint: Text(
                          'Pilih Tipe Ujian',
                          style: TextStyle(fontSize: S.w / 30),
                        ),
                        value: _choose1,
                        onChanged: (String o) {
                          setState(() {
                            _choose1 = o;
                          });
                        },
                        items: ['PG']
                            .map((e) => DropdownMenuItem(
                                  value: e,
                                  child: Text(e,
                                      style: TextStyle(fontSize: S.w / 30)),
                                ))
                            .toList()),
                  ),
                ),
              ),
              if (widget.type == 'normal')
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .03),
                  child: Container(
                    width: S.w * .7,
                    height: S.w * .1,
                    padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(S.w),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                          icon: Icon(
                            Icons.keyboard_arrow_down,
                            color: SiswamediaTheme.green,
                          ),
                          hint: Text(
                            'Pilih Kelas',
                            style: TextStyle(fontSize: S.w / 30),
                          ),
                          value: _choose2,
                          onChanged: (String o) {
                            _getCourseByClass(int.parse(o));
                            setState(() {
                              _choose2 = o;
                            });
                          },
                          items: [
                            ///Default dropdown item named [noData]
                            DropdownMenuItem(
                              value: '-1',
                              child: Text('Pilih Kelas',
                                  style: TextStyle(fontSize: S.w / 30)),
                            ),

                            ///e as CourseDetail
                            for (ClassBySchoolData e in classState
                                .state.classMap.values
                                .where((element) =>
                                    element.schoolId ==
                                    authState.state.currentState.schoolId))
                              DropdownMenuItem(
                                value: '${e.id}',
                                child: Text('${e.name}',
                                    style: TextStyle(fontSize: S.w / 30)),
                              )
                          ]),
                    ),
                  ),
                ),
              if (widget.type == 'normal' && course.isEmpty && _choose2 != null)
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                    child: Row(
                      children: [
                        Icon(Icons.warning, color: Colors.red),
                        SizedBox(width: 10),
                        Text('Tidak ada mata pelajaran di kelas ini'),
                      ],
                    )),
              if (course.isNotEmpty)
                GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio: 4,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  children: course
                      .where((element) => element.role.isGuru)
                      .map((e) => CheckboxListTile(
                            title: Text(
                              e.name,
                              style: descBlack.copyWith(fontSize: S.w / 28),
                            ),
                            controlAffinity: ListTileControlAffinity.leading,
                            onChanged: (bool value) {
                              _onCategorySelected(value, e);
                              print(_selectedCategorys);
                            },
                            value: _selectedCategorys.contains(e),
                          ))
                      .toList(),
                ),
              /*Container(
                  height: width *
                      .2 *
                      ((auth.allCourse
                                  .where((element) =>
                                      element.classId.toString() == _choose2)
                                  .length +
                              1) ~/
                          2),
                  child: GridView.count(
                    crossAxisCount: 2,
                    children: auth.allCourse
                        .where(
                            (element) => element.classId.toString() == _choose2)
                        .map(
                          (e) => CheckboxListTile(
                            value: _selecteCategorys.contains(e.id),
                            onChanged: (f) {
                              _onCategorySelected(f, e.id);
                            },
                            title: Text(e.name,
                                style: TextStyle(fontSize: width / 30)),
                          ),
                        )
                        .toList(),
                  )),*/
              ///acak soal
              Padding(
                padding: EdgeInsets.only(
                    left: S.w * .05, right: S.w * .05, top: 10.0),
                child: Text('Acak Soal',
                    style: TextStyle(
                        fontSize: S.w / 25,
                        color: Colors.black87,
                        fontWeight: FontWeight.w600)),
              ),
              Padding(
                padding: EdgeInsets.only(left: S.w * .1),
                child: Row(
                  children: [
                    Radio(
                      value: 1,
                      activeColor: SiswamediaTheme.green,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    Text('Ya',
                        style: TextStyle(
                          fontSize: S.w / 30,
                          color: Colors.black87,
                        )),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: S.w * .1),
                child: Row(
                  children: [
                    Radio(
                      value: 0,
                      activeColor: SiswamediaTheme.green,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    Text('Tidak',
                        style: TextStyle(
                          fontSize: S.w / 30,
                          color: Colors.black87,
                        )),
                  ],
                ),
              ),

              ///hide or no
              Padding(
                padding: EdgeInsets.only(
                    left: S.w * .05, right: S.w * .05, top: 10.0),
                child: Text('Sembunyikan Nilai',
                    style: TextStyle(
                        fontSize: S.w / 25,
                        color: Colors.black87,
                        fontWeight: FontWeight.w600)),
              ),
              Padding(
                padding: EdgeInsets.only(left: S.w * .1),
                child: Row(
                  children: [
                    Radio(
                      value: 1,
                      activeColor: SiswamediaTheme.green,
                      groupValue: _radioValue2,
                      onChanged: _handleRadioValueChange2,
                    ),
                    Text('Ya',
                        style: TextStyle(
                          fontSize: S.w / 30,
                          color: Colors.black87,
                        )),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: S.w * .1),
                child: Row(
                  children: [
                    Radio(
                      value: 0,
                      activeColor: SiswamediaTheme.green,
                      groupValue: _radioValue2,
                      onChanged: _handleRadioValueChange2,
                    ),
                    Text('Tidak',
                        style: TextStyle(
                          fontSize: S.w / 30,
                          color: Colors.black87,
                        )),
                  ],
                ),
              ),
              if (widget.type == 'normal') Composer(),
              SizedBox(height: 10),
              loading
                  ? Center(child: CircularProgressIndicator())
                  : Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .01, horizontal: S.w * .05),
                      child: Container(
                        width: MediaQuery.of(context).size.width * .9,
                        height: S.w * .12,
                        decoration: BoxDecoration(
                          color: SiswamediaTheme.green,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () async {
                              if (data.selected.keys.isEmpty) {
                                return CustomFlushBar.errorFlushBar(
                                  'Pilih soal terlebih dahulu, Klik card soal',
                                  context,
                                );
                              } else if (controller.text != '' &&
                                  duration.text != '' &&
                                  (widget.type == 'journal' ||
                                      _selectedCategorys.isNotEmpty) &&
                                  (widget.type == 'journal' ||
                                      _choose2 != null) &&
                                  (widget.type == 'journal' ||
                                      _choose2 != '-1')) {
                                setState(() {
                                  loading = true;
                                });
                                await createQuiz(data, start).then((value) {
                                  if (value == 200) {
                                    setState(() {
                                      loading = false;
                                    });
                                    showDialog<void>(
                                        context: context,
                                        builder: (context) => StatefulBuilder(
                                              builder: (BuildContext context,
                                                  void Function(void Function())
                                                      setState) {
                                                return Dialog(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              12)),
                                                  child: Container(
                                                    height: S.w * .65,
                                                    width: S.w * .65,
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: S.w * .05,
                                                            horizontal:
                                                                S.w * .1),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        Image.asset(
                                                            'assets/izin.png',
                                                            height: S.w * .25,
                                                            width: S.w * .25,
                                                            fit:
                                                                BoxFit.contain),
                                                        Text(
                                                            'Yey Quiz Berhasil Dibuat!'),
                                                        Container(
                                                          height: S.w * .1,
                                                          width: S.w * .18,
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4),
                                                            color:
                                                                SiswamediaTheme
                                                                    .green,
                                                          ),
                                                          child: InkWell(
                                                            onTap: () {
                                                              if (widget.type ==
                                                                  'normal') {
                                                                popUntil(
                                                                    context,
                                                                    '/quiz_home');
                                                              } else {
                                                                popUntil(
                                                                    context,
                                                                    '/journal');
                                                              }
                                                              data.setSelected(<
                                                                  String,
                                                                  dynamic>{});
                                                            },
                                                            child: Center(
                                                                child: Text(
                                                                    'Close',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white))),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                            )).then(
                                      (value) {
                                        if (widget.type == 'normal') {
                                          popUntil(context, '/quiz_home');
                                        } else {
                                          popUntil(context, '/journal');
                                        }
                                        data.setSelected(<String, dynamic>{});
                                      },
                                    );
                                  } else {
                                    setState(() {
                                      loading = false;
                                    });
                                    CustomFlushBar.errorFlushBar(
                                      'Terjadi Kesalahan',
                                      context,
                                    );
                                  }
                                });
                              } else if (widget.type == 'normal' &&
                                  _choose2 == '-1') {
                                CustomFlushBar.errorFlushBar(
                                  'Belum memilih kelas atau Anda belum terdaftar di mata pelajaran manapun',
                                  context,
                                );
                              } else if (controller.text == '') {
                                CustomFlushBar.errorFlushBar(
                                  'Nama ujian harus diisi',
                                  context,
                                );
                              } else if (duration.text == '') {
                                CustomFlushBar.errorFlushBar(
                                  'Durasi harus diisi',
                                  context,
                                );
                              } else if (widget.type == 'normal' &&
                                  course.isEmpty) {
                                CustomFlushBar.errorButtonFlushBar(
                                    'Kelas belum memiliki mata pelajaran',
                                    context, onPressed: () {
                                  Navigator.popUntil(
                                      context, (route) => route.isFirst);
                                  data.setSelected(<String, dynamic>{});
                                  navigate(
                                      context,
                                      ListCourseByClass(
                                        schoolId: authState
                                            .state.currentState.schoolId,
                                        classId: authState
                                            .state.currentState.classId,
                                        id: authState
                                            .state.currentState.classId,
                                        role: authState
                                            .state.currentState.classRole,
                                      ));
                                });
                              } else if (widget.type == 'normal' &&
                                  _selectedCategorys.isEmpty) {
                                CustomFlushBar.errorFlushBar(
                                  'Belum memilih mata pelajaran',
                                  context,
                                );
                              }
                            },
                            child: Center(
                              child: Text('Buat Kuis',
                                  style: TextStyle(
                                      fontSize: S.w / 30,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white)),
                            ),
                          ),
                        ),
                      ),
                    ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: S.w * .01, horizontal: S.w * .05),
                child: Container(
                  width: MediaQuery.of(context).size.width * .9,
                  height: S.w * .12,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Center(
                        child: Text('Batal',
                            style: TextStyle(
                                fontSize: S.w / 30,
                                fontWeight: FontWeight.w700,
                                color: SiswamediaTheme.green)),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  void _onCategorySelected(bool selected, CourseByClassData value) {
    if (selected == true) {
      setState(() {
        _selectedCategorys.add(value);
      });
    } else {
      setState(() {
        _selectedCategorys.remove(value);
      });
    }
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;
    });
  }

  Future<int> createQuiz(QuizProviderKelas data, DateTime time) async {
    var koko = <QuizItem>[];
    var total = 0;
    data.selected.forEach((key, dynamic value) {
      koko.add(data.selected[key]);
      total++;
    });

    var classId = _choose2;
    var courseId = _selectedCategorys;
    //List<int> dataId =  List<int>();
    try {
      for (var i = 0;
          i < (widget.type == 'normal' ? courseId.length : 1);
          i++) {
        var result = await QuizServices.createQuiz(
            value: BankModel(
          schoolId: authState.state.currentState.schoolId,
          courseId: widget.type == 'normal' ? courseId[i].id : widget.courseId,
          classId: widget.type == 'normal'
              ? int.parse(classId)
              : authState.state.currentState.classId,
          name: controller.text,
          totalTime: int.parse(duration.text),
          startDate: time.toUtc().toIso8601String(),
          endDate: widget.type == 'normal'
              ? time
                  .toUtc()
                  .add(Duration(minutes: int.parse(duration.text)))
                  .toIso8601String()
              : end.toUtc().toIso8601String(),
          totalPg: total,
          totalEssay: -1,
          quizType: 'SEKOLAH',
          isHidden: _radioValue2 == 1,
          isRandom: _radioValue1 == 1,
        ));
        if (widget.type != 'normal') {
          GlobalState.dummy().setState((s) => s.changeQuiz({
                result.data.id: Quizzes(
                    type: 'new',
                    name: controller.text,
                    startTime: start.toUtc().toIso8601String(),
                    endTime: end.toUtc().toIso8601String(),
                    duration: duration.text.toInteger)
              }));
        }

        var bankSoal = <Map<String, dynamic>>[];
        for (var i = 0; i < koko.length; i++) {
          // if (koko[i].typesoal == 'PG') {
          bankSoal.add(<String, dynamic>{
            'score': 100 ~/ koko.length,
            'correct_answer': koko[i].jawaban,
            'question_type': 'PG',
            'image': koko[i].attpertanyaan,
            'question_description': koko[i].pertanyaan,
            'quiz_pg_references': [
              {'order_of_question': 'A', 'pg_description': koko[i].pilihana},
              {'order_of_question': 'B', 'pg_description': koko[i].pilihanb},
              {'order_of_question': 'C', 'pg_description': koko[i].pilihanc},
              if (koko[i].pilihand != '-')
                {'order_of_question': 'D', 'pg_description': koko[i].pilihand},
              if (koko[i].pilihane != '-')
                {'order_of_question': 'E', 'pg_description': koko[i].pilihane}
            ]
          });
          // }
        }
        await QuizServices.createBankSoalBatch(result.data.id, item: bankSoal);
        //dataId.add(result.data.id);
      }
      return 200;
    } catch (e) {
      print(e);
      return 400;
    }
  }
}

class Composer extends StatelessWidget {
  final TextEditingController description;
  const Composer({this.description});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                offset: const Offset(4, 4),
                blurRadius: 8),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(25),
          child: Container(
            padding: const EdgeInsets.all(4.0),
            constraints: const BoxConstraints(minHeight: 140, maxHeight: 140),
            color: Colors.white,
            child: SingleChildScrollView(
              padding:
                  const EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 0),
              child: TextField(
                controller: description,
                maxLines: null,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black54,
                ),
                cursorColor: Colors.blue,
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'Catatan...'),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
