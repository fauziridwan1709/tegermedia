part of '_ujian.dart';

enum DialogType { image, onlyText, textImage, widget }

class QuizDialog extends StatelessWidget {
  final String title;
  final String description;
  final DialogType type;
  final String image;
  final List<String> actions;
  final Function onTapFirstAction;
  final Function onTapSecondAction;

  QuizDialog({
    @required this.title,
    this.description,
    this.type,
    this.image,
    this.actions,
    this.onTapFirstAction,
    this.onTapSecondAction,
  }) : assert(
          title != null,
          type != null,
        );

  @override
  Widget build(BuildContext context) {
    if (type == DialogType.image) {
      return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Container(
          height: S.w / 1.5,
          width: S.w / 2,
          padding:
              EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text('Pilih File',
                  style: TextStyle(
                      fontSize: S.w / 30, fontWeight: FontWeight.w600)),
              Container(
                width: S.w * .6,
                height: S.w * .1,
                decoration: BoxDecoration(
                  color: Color(0xff707070),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context,
                        MaterialPageRoute<void>(builder: (_) => QuizUpload()));
                  },
                  child: Center(
                      child: Text('Google Drive',
                          style: TextStyle(
                            fontSize: S.w / 30,
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ))),
                ),
              ),
              Container(
                width: S.w * .6,
                height: S.w * .1,
                decoration: BoxDecoration(
                  color: SiswamediaTheme.green,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute<void>(builder: (_) => BankSoal()));
                  },
                  child: Center(
                      child: Text('Bank Soal',
                          style: TextStyle(
                            fontSize: S.w / 30,
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ))),
                ),
              ),
              InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .03),
                    child: Text('Batal'),
                  ))
            ],
          ),
        ),
      );
    } else if (type == DialogType.onlyText) {
      return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Container(
            height: S.w / 1.5,
            width: S.w / 2,
            padding: EdgeInsets.symmetric(
                horizontal: S.w * .05, vertical: S.w * .05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(title,
                    textAlign: TextAlign.center,
                    style: boldBlack.copyWith(fontSize: S.w / 26)),
                Text(description,
                    textAlign: TextAlign.center,
                    style: descBlack.copyWith(fontSize: S.w / 30)),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .01),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: SiswamediaTheme.green,
                          ),
                          height: S.w / 10,
                          width: S.w / 4,
                          child: InkWell(
                            onTap: () {
                              onTapFirstAction();
                            },
                            child: Center(
                              child: Text(actions.first,
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                        Container(
                          height: S.w / 10,
                          width: S.w / 4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: Color(0xff4B4B4B),
                          ),
                          child: InkWell(
                            onTap: () {
                              onTapSecondAction();
                            },
                            child: Center(
                              child: Text(actions.last,
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                      ]),
                )
              ],
            )),
      );
    } else if (type == DialogType.textImage) {
      return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(14)),
        child: Container(
            width: S.w * .6,
            padding: EdgeInsets.symmetric(
                horizontal: S.w * .05, vertical: S.w * .05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 15),
                Image.asset(
                  image,
                  width: S.w * .35,
                ),
                SizedBox(height: 15),
                Text(title,
                    textAlign: TextAlign.center,
                    style: boldBlack.copyWith(fontSize: S.w / 26)),
                SizedBox(height: 5),
                Text(description,
                    textAlign: TextAlign.center,
                    style: descBlack.copyWith(fontSize: S.w / 30)),
                SizedBox(height: 25),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .01),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: SiswamediaTheme.green,
                            ),
                            height: S.w / 10,
                            child: InkWell(
                              onTap: () {
                                onTapFirstAction();
                              },
                              child: Center(
                                child: Text(actions.first,
                                    style: TextStyle(color: Colors.white)),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: S.w / 10,
                            width: S.w / 4,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: Color(0xff4B4B4B),
                            ),
                            child: InkWell(
                              onTap: () {
                                onTapSecondAction();
                              },
                              child: Center(
                                child: Text(actions.last,
                                    style: TextStyle(color: Colors.white)),
                              ),
                            ),
                          ),
                        ),
                      ]),
                ),
                SizedBox(height: 20),
              ],
            )),
      );
    } else if (type == DialogType.widget) {}
    return Container();
  }
}
