part of '_ujian.dart';

class QuizUpload extends StatefulWidget {
  @override
  _QuizUploadState createState() => _QuizUploadState();
}

class _QuizUploadState extends State<QuizUpload> {
  final authState = GlobalState.auth();
  final List<CourseByClassData> _selectedCategorys = <CourseByClassData>[];
  DateTime start = DateTime.now();
  String _choose1;
  String _choose2;
  bool uploaded = false;
  bool loading = false;
  bool loadingCourse = false;
  int _radioValue1 = 0;
  int _radioValue2 = 0;

  List<CourseByClassData> course = [];

  TextEditingController controller = TextEditingController();
  TextEditingController duration = TextEditingController();

  FocusNode cNode = FocusNode();
  FocusNode durNode = FocusNode();

  var scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _getCourseByClass(int id) async {
    setState(() {
      loadingCourse = true;
    });
    var result = await ClassServices.getCourseByClass(classId: id);
    if (result.statusCode == 200) {
      setState(() {
        course = result.data;
        loadingCourse = false;
      });
    } else {
      setState(() {
        course = [];
        loadingCourse = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(TIME_QUIZ_UPLOAD) &&
          isDifferenceLessThanThirtyMin(value.getString(TIME_QUIZ_UPLOAD))) {
        // if (list.listClass == null) {
        //   await list.getMyClass();
        // }
        //do nothing
      } else {
        // await list.getMyClass();
        await value.setString(TIME_QUIZ_UPLOAD, DateTime.now().timeInString);
      }
      // value.setString()
    });
  }

  Future<void> fetchData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((value) async {
      // await list.getMyClass();
    });
  }

  @override
  Widget build(BuildContext context) {
    var data = context.watch<QuizProviderKelas>();
    return WillPopScope(
      onWillPop: () {
        data.setSelected(<String, dynamic>{});
        return Future.value(true);
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: Color(0xfffefefe),
        appBar: AppBar(
          shadowColor: Colors.grey.withOpacity(.2),
          brightness: Brightness.light,
          leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                data.setSelected(<String, dynamic>{});
                Navigator.pop(context);
              }),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Upload Ujian',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: S.w / 22,
                  fontWeight: FontWeight.w600)),
        ),
        body: GestureDetector(
          onTap: () {
            if (durNode.hasFocus || cNode.hasFocus) {
              durNode.unfocus();
              cNode.unfocus();
            }
          },
          child: RefreshIndicator(
            onRefresh: () async {
              await fetchData();
            },
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () => LaunchServices.launchInBrowser(
                        'https://drive.google.com/uc?export=download&id=10WvlSVd67iJQIRgPfvw_1AVdCevXobpG'),
                    child: Container(
                        width: S.w,
                        padding: EdgeInsets.symmetric(
                            horizontal: S.w * .03, vertical: S.w * .03),
                        margin: EdgeInsets.symmetric(
                            horizontal: S.w * .25, vertical: S.w * .03),
                        decoration: BoxDecoration(
                          color: Color(0xff4B4B4B),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Center(
                            child: Text('Unduh Template Soal',
                                style: secondHeadingWhite.copyWith(
                                    fontSize: S.w / 30)))),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: S.w * .05, right: S.w * .05, top: 10.0),
                    child: Text('Soal',
                        style: TextStyle(
                            fontSize: S.w / 25,
                            color: Colors.black87,
                            fontWeight: FontWeight.w600)),
                  ),
                  !uploaded
                      ? Padding(
                          padding: EdgeInsets.only(
                              left: S.w * .05, right: S.w * .05, top: 10.0),
                          child: InkWell(
                            onTap: () async {
                              if (_choose1 == '' || _choose1 == null) {
                                CustomFlushBar.errorFlushBar(
                                    'Pilih tipe ujian terlebih dahulu',
                                    context);
                              } else {
                                // ignore: unawaited_futures
                                showDialog<void>(
                                    context: context,
                                    builder: (context) => Dialog(
                                        elevation: 0,
                                        backgroundColor: Colors.transparent,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(12)),
                                        child: Center(
                                            child:
                                                CircularProgressIndicator())));
                                var value = await getFile(_choose1);
                                print('ini value');
                                print(value);
                                if (value) {
                                  setState(() {
                                    uploaded = true;
                                    Navigator.pop(context);
                                    showDialog<void>(
                                        context: context,
                                        builder: (context) => Dialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(12)),
                                            child: Container(
                                              height: S.w / 1.8,
                                              width: S.w / 2,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                              ),
                                              child: Padding(
                                                  padding:
                                                      EdgeInsets.all(S.w * .05),
                                                  child: Column(children: [
                                                    Image.asset(
                                                        'assets/izin.png',
                                                        height: S.w * .25,
                                                        width: S.w * .25,
                                                        fit: BoxFit.contain),
                                                    Text(
                                                        'Berhasil mengupload\nsoal',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style:
                                                            descBlack.copyWith(
                                                                fontSize:
                                                                    S.w / 30))
                                                  ])),
                                            )));
                                  });
                                } else {
                                  setState(() {
                                    uploaded = false;
                                    // Navigator.pop(context);
                                  });
                                }
                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(14),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: SiswamediaTheme.green,
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.attach_file,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 15),
                                Text(
                                  'Unggah soal (.xlsx)',
                                  style: TextStyle(
                                      color: Colors.grey.withOpacity(.5)),
                                ),
                              ],
                            ),
                          ))
                      : InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute<void>(
                                    builder: (_) =>
                                        QuizKomposisi(type: 'UPLOAD')));
                          },
                          child: Container(
                            height: S.w * .2,
                            padding: EdgeInsets.symmetric(
                                horizontal: S.w * .05, vertical: S.w * .03),
                            margin: EdgeInsets.symmetric(
                                horizontal: S.w * .05, vertical: S.w * .02),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12),
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(2, 2),
                                    spreadRadius: 2,
                                    blurRadius: 2,
                                    color: Colors.grey.withOpacity(.2),
                                  )
                                ]),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Preview Soal',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w700,
                                        fontSize: S.w / 25)),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('${data.list.length} soal'),
                                    Container(
                                      height: S.w * .07,
                                      width: S.w * .2,
                                      decoration: BoxDecoration(
                                        color: SiswamediaTheme.green,
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                      child: Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute<void>(
                                                    builder: (_) =>
                                                        QuizKomposisi(
                                                            type: 'UPLOAD')));
                                          },
                                          child: Center(
                                              child: Text('Lihat',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: S.w / 32))),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .09, vertical: S.w * .02),
                    child: Column(
                      children: [
                        TextField(
                          controller: controller,
                          focusNode: cNode,
                          decoration: InputDecoration.collapsed(
                              hintText: 'Nama Kuis',
                              hintStyle: TextStyle(
                                  fontSize: S.w / 25,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.grey.withOpacity(.4))),
                        ),
                        SizedBox(height: 10),
                        Divider(color: Colors.grey),
                        DateTimePicker(
                          type: DateTimePickerType.dateTime,
                          dateMask: 'MMM dd, yyyy - HH:mm ',
                          firstDate: DateTime(2000),
                          lastDate: DateTime(2100),
                          icon: Icon(Icons.calendar_today_outlined),
                          dateLabelText: 'Tanggal mulai',
                          onChanged: (val) =>
                              setState(() => start = DateTime.parse(val)),
                        ),
                        SizedBox(height: 25),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Waktu Pengerjaan (menit)',
                                style: descBlack.copyWith(fontSize: S.w / 30)),
                            Container(
                              width: S.w / 3,
                              child: TextField(
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                controller: duration,
                                focusNode: durNode,
                                textAlign: TextAlign.end,
                                decoration: InputDecoration.collapsed(
                                    hintText: '30',
                                    hintStyle: TextStyle(
                                        fontSize: S.w / 25,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.grey.withOpacity(.4))),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Divider(color: Colors.grey),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .01),
                    child: Container(
                      width: S.w * .45,
                      height: S.w * .1,
                      padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(S.w),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                            icon: Icon(
                              Icons.keyboard_arrow_down,
                              color: SiswamediaTheme.green,
                            ),
                            hint: Text(
                              'Pilih Tipe Ujian',
                              style: TextStyle(fontSize: S.w / 30),
                            ),
                            value: _choose1,
                            onChanged: (String o) {
                              setState(() {
                                _choose1 = o;
                              });
                            },
                            items: ['PG', 'ESSAY', 'PG & ESSAY']
                                .map((e) => DropdownMenuItem(
                                      value: e,
                                      child: Text(e,
                                          style: TextStyle(fontSize: S.w / 30)),
                                    ))
                                .toList()),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: S.w * .05, right: S.w * .05, top: 10.0),
                    child: Text('Pilih Kelas',
                        style: TextStyle(
                            fontSize: S.w / 25,
                            color: Colors.black87,
                            fontWeight: FontWeight.w600)),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .03),
                    child: Container(
                      width: S.w * .7,
                      height: S.w * .1,
                      padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(S.w),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                            icon: Icon(
                              Icons.keyboard_arrow_down,
                              color: SiswamediaTheme.green,
                            ),
                            hint: Text(
                              'Pilih Kelas',
                              style: TextStyle(fontSize: S.w / 30),
                            ),
                            value: _choose2,
                            onChanged: (String o) {
                              _getCourseByClass(int.parse(o));
                              setState(() {
                                _choose2 = o;
                              });
                            },
                            items: [
                              ///Default dropdown item named [noData]
                              DropdownMenuItem(
                                value: '-1',
                                child: Text('Pilih Kelas',
                                    style: TextStyle(fontSize: S.w / 30)),
                              ),

                              ///e as CourseDetail
                              for (ClassBySchoolData e in GlobalState.classes()
                                  .state
                                  .classMap
                                  .values
                                  .where((element) =>
                                      element.schoolId ==
                                      authState.state.currentState.schoolId))
                                DropdownMenuItem(
                                  value: '${e.id}',
                                  child: Text('${e.name}',
                                      style: TextStyle(fontSize: S.w / 30)),
                                )
                            ]),
                      ),
                    ),
                  ),
                  if (loadingCourse)
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                      child: Row(children: [
                        Container(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator()),
                        SizedBox(width: 5),
                        Text('Loading mata pelajaran')
                      ]),
                    ),
                  if (course.isEmpty && loadingCourse == false)
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                        child: Text('Tidak ada mata pelajaran di kelas ini')),
                  if (course.isNotEmpty && loadingCourse == false)
                    GridView.count(
                      crossAxisCount: 2,
                      childAspectRatio: 4,
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      children: course
                          .where((element) => element.role.isGuru)
                          .map((e) => CheckboxListTile(
                                title: Text(
                                  e.name,
                                  style: descBlack.copyWith(fontSize: S.w / 28),
                                ),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (bool value) {
                                  _onCategorySelected(value, e);
                                  print(_selectedCategorys);
                                },
                                value: _selectedCategorys.contains(e),
                              ))
                          .toList(),
                    ),
                  /*Container(
                      height: width *
                          .2 *
                          ((auth.allCourse
                                      .where((element) =>
                                          element.classId.toString() == _choose2)
                                      .length +
                                  1) ~/
                              2),
                      child: GridView.count(
                        physics: ScrollPhysics(),
                        crossAxisCount: 2,
                        children: auth.allCourse
                            .where(
                                (element) => element.classId.toString() == _choose2)
                            .map(
                              (e) => CheckboxListTile(
                                value: _selecteCategorys.contains(e.id),
                                onChanged: (f) {
                                  _onCategorySelected(f, e.id);
                                },
                                title: Text(e.name,
                                    style: TextStyle(fontSize: width / 30)),
                              ),
                            )
                            .toList(),
                      )),*/
                  Padding(
                    padding: EdgeInsets.only(
                        left: S.w * .05, right: S.w * .05, top: 10.0),
                    child: Text('Acak Soal',
                        style: TextStyle(
                            fontSize: S.w / 25,
                            color: Colors.black87,
                            fontWeight: FontWeight.w600)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: S.w * .1),
                    child: Row(
                      children: [
                        Radio(
                          value: 0,
                          activeColor: SiswamediaTheme.green,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        Text('Ya',
                            style: TextStyle(
                              fontSize: S.w / 30,
                              color: Colors.black87,
                            )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: S.w * .1),
                    child: Row(
                      children: [
                        Radio(
                          value: 1,
                          activeColor: SiswamediaTheme.green,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        Text('Tidak',
                            style: TextStyle(
                              fontSize: S.w / 30,
                              color: Colors.black87,
                            )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: S.w * .05, right: S.w * .05, top: 10.0),
                    child: Text('Tampilkan Nilai ke Siswa',
                        style: TextStyle(
                            fontSize: S.w / 25,
                            color: Colors.black87,
                            fontWeight: FontWeight.w600)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: S.w * .1),
                    child: Row(
                      children: [
                        Radio(
                          value: 1,
                          activeColor: SiswamediaTheme.green,
                          groupValue: _radioValue2,
                          onChanged: _handleRadioValueChange2,
                        ),
                        Text('Ya',
                            style: TextStyle(
                              fontSize: S.w / 30,
                              color: Colors.black87,
                            )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: S.w * .1),
                    child: Row(
                      children: [
                        Radio(
                          value: 0,
                          activeColor: SiswamediaTheme.green,
                          groupValue: _radioValue2,
                          onChanged: _handleRadioValueChange2,
                        ),
                        Text('Tidak',
                            style: TextStyle(
                              fontSize: S.w / 30,
                              color: Colors.black87,
                            )),
                      ],
                    ),
                  ),
                  Composer(),
                  SizedBox(height: 10),
                  loading
                      ? Center(child: CircularProgressIndicator())
                      : Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: S.w * .01, horizontal: S.w * .05),
                          child: Container(
                            width: S.w * .9,
                            height: S.w * .12,
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () async {
                                  if (controller.text != '' &&
                                      duration.text != '' &&
                                      _choose2 != null &&
                                      _selectedCategorys.isNotEmpty &&
                                      _choose2 != '-1') {
                                    setState(() {
                                      loading = true;
                                    });
                                    await createQuiz(
                                            data, start, context, _choose1)
                                        .then((value) {
                                      if (value == 200 || value == 201) {
                                        setState(() {
                                          loading = false;
                                        });
                                        showDialog<void>(
                                            context: context,
                                            builder: (context) =>
                                                StatefulBuilder(
                                                  builder: (BuildContext
                                                          context,
                                                      void Function(
                                                              void Function())
                                                          setState) {
                                                    return Dialog(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12)),
                                                      child: Container(
                                                        height: S.w * .65,
                                                        width: S.w * .65,
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                vertical:
                                                                    S.w * .05,
                                                                horizontal:
                                                                    S.w * .1),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            Image.asset(
                                                                'assets/izin.png',
                                                                height:
                                                                    S.w * .25,
                                                                width:
                                                                    S.w * .25,
                                                                fit: BoxFit
                                                                    .contain),
                                                            Text(
                                                                'Yey Quiz Berhasil Dibuat!'),
                                                            Container(
                                                              height: S.w * .1,
                                                              width: S.w * .18,
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            4),
                                                                color:
                                                                    SiswamediaTheme
                                                                        .green,
                                                              ),
                                                              child: InkWell(
                                                                onTap: () {
                                                                  Navigator.popUntil(
                                                                      context,
                                                                      (route) =>
                                                                          route
                                                                              .isFirst);
                                                                },
                                                                child: Center(
                                                                    child: Text(
                                                                        'Close',
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.white))),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                ));
                                      } else {
                                        setState(() {
                                          loading = false;
                                        });
                                        CustomFlushBar.errorFlushBar(
                                            'Terjadi Kesalahan', context);
                                      }
                                    });
                                  } else if (_choose2 == '-1') {
                                    CustomFlushBar.errorFlushBar(
                                        'Belum memilih kelas atau Anda belum terdaftar di mata pelajaran manapun',
                                        context);
                                  } else if (controller.text == '') {
                                    CustomFlushBar.errorFlushBar(
                                        'Nama ujian harus diisi', context);
                                  } else if (duration.text == '') {
                                    CustomFlushBar.errorFlushBar(
                                        'Durasi harus diisi', context);
                                  } else if (course.isEmpty) {
                                    CustomFlushBar.errorButtonFlushBar(
                                        'Kelas belum memiliki mata pelajaran',
                                        context, onPressed: () {
                                      Navigator.popUntil(
                                          context, (route) => route.isFirst);
                                      data.setSelected(<String, dynamic>{});
                                      navigate(
                                          context,
                                          ListCourseByClass(
                                            schoolId: authState
                                                .state.currentState.schoolId,
                                            classId: authState
                                                .state.currentState.classId,
                                            id: authState
                                                .state.currentState.classId,
                                            role: authState
                                                .state.currentState.classRole,
                                          ));
                                    });
                                  } else if (_selectedCategorys.isEmpty) {
                                    CustomFlushBar.errorFlushBar(
                                        'Belum memilih mata pelajaran',
                                        context);
                                  }
                                },
                                child: Center(
                                  child: Text('Buat Kuis',
                                      style: TextStyle(
                                          fontSize: S.w / 30,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white)),
                                ),
                              ),
                            ),
                          ),
                        ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: S.w * .01, horizontal: S.w * .05),
                    child: Container(
                      width: S.w * .9,
                      height: S.w * .12,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                            child: Text('Batal',
                                style: TextStyle(
                                    fontSize: S.w / 30,
                                    fontWeight: FontWeight.w700,
                                    color: SiswamediaTheme.green)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String className(CourseDetail e) {
    if (true //auth.allClass.where((element) => element.id == e.classId).length != 0
        ) {
      // return auth.allClass
      //     .where((element) => element.id == e.classId)
      //     .first
      //     .name;
    }
    return '';
  }

  void _onCategorySelected(bool selected, dynamic value) {
    if (selected == true) {
      setState(() {
        _selectedCategorys.add(value);
      });
    } else {
      setState(() {
        _selectedCategorys.remove(value);
      });
    }
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;
    });
  }

  Future<int> createQuiz(QuizProviderKelas data, DateTime time,
      BuildContext context, String type) async {
    var quizProvider = context.read<QuizProviderKelas>();
    // ignore: unawaited_futures
    showDialog<void>(
        context: context,
        builder: (_) => Center(child: CircularProgressIndicator()));
    var koko = <QuizItem>[];
    var total = 0;
    data.selected.forEach((key, dynamic value) {
      koko.add(data.selected[key]);
      total++;
    });
    var classId = _choose2;
    var courseId = _selectedCategorys;
    print(total);
    print(type);
    try {
      for (var i = 0; i < courseId.length; i++) {
        var result = await QuizServices.createQuiz(
            value: BankModel(
          courseId: courseId[i].id,
          classId: int.parse(classId),
          name: controller.text,
          totalTime: int.parse(duration.text),
          startDate: time.toUtc().toIso8601String(),
          endDate: time
              .toUtc()
              .add(Duration(minutes: int.parse(duration.text)))
              .toIso8601String(),
          totalPg: data.list
              .where((element) => element.typesoal == 'PG')
              .toList()
              .length,
          totalEssay: data.list
              .where((element) => element.typesoal == 'ESSAY')
              .toList()
              .length,
          quizType: 'SEKOLAH',
        ));
        var bankSoal = <Map<String, dynamic>>[];
        for (var i = 0; i < data.list.length; i++) {
          if (data.list[i].typesoal == 'PG') {
            bankSoal.add(<String, dynamic>{
              'score': double.parse(data.list[i].bobot) ~/ 1 ?? 0,
              'correct_answer': data.list[i].jawaban,
              'question_type': 'PG',
              'image': data.list[i].attpertanyaan,
              'question_description': data.list[i].pertanyaan,
              'quiz_pg_references': [
                {
                  'order_of_question': 'A',
                  'pg_description': data.list[i].pilihana
                },
                {
                  'order_of_question': 'B',
                  'pg_description': data.list[i].pilihanb
                },
                {
                  'order_of_question': 'C',
                  'pg_description': data.list[i].pilihanc
                },
                if (data.list[i].pilihand != '-')
                  {
                    'order_of_question': 'D',
                    'pg_description': data.list[i].pilihand
                  },
                if (data.list[i].pilihane != '-')
                  {
                    'order_of_question': 'E',
                    'pg_description': data.list[i].pilihane
                  }
              ]
            });
          } else {
            bankSoal.add(<String, dynamic>{
              'score': double.parse(data.list[i].bobot) ~/ 1 ?? 0,
              'correct_answer': data.list[i].jawaban,
              'question_type': 'ESSAY',
              'question_description': data.list[i].pertanyaan,
              'image': data.list[i].attpertanyaan
            });
          }
        }

        await QuizServices.createBankSoalBatch(result.data.id, item: bankSoal);
      }
      Navigator.pop(context);
      return 200;
    } catch (e) {
      print(e);
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar('Terjadi Kesalahan', context);
      return 400;
    }
  }

  Future<bool> getFile(String type) async {
    try {
      print(type);
      var quizProvider = context.read<QuizProviderKelas>();

      var files = await FilePicker.platform
          .pickFiles(type: FileType.custom, allowedExtensions: ['xlsx']);
      var file = files.files.first;
      var bytes = File(file.path).readAsBytesSync();
      print('call excel');
      // print(bytes);
      var excel = Excel.decodeBytes(bytes);
      print('test');
      for (var table in excel.tables.keys) {
        print(table); //sheet Name
        print(excel.tables[table].maxCols);
        print(excel.tables[table].maxRows);
        var list = <QuizItem>[];

        for (var row in excel.tables[table].rows) {
          String attpertanyaan = row[2] ?? '0';
          String pertanyaan = row[3] ?? '0';
          print('$row');
          if (row[0] == null &&
              row[1] == null &&
              row[2] == null &&
              row[3] == null &&
              row[4] == null &&
              row[5] == null &&
              row[6] == null &&
              row[7] == null &&
              row[8] == null &&
              row[9] == null) {
            throw GeneralException(
                cause:
                    'dimohon untuk jangan ada baris kosong antar dua nomor soal pada excel Anda');
          }
          if (row[1] == 'PG') {
            if (row[10] / 1.0 != row[10]) {
              throw GeneralException(
                  cause: 'pengisian bobot tidak sesuai format untuk PG');
            }
            var data = QuizItem(
              materi: row[0],
              typesoal: row[1],
              attpertanyaan: attpertanyaan,
              pertanyaan: pertanyaan.toString(),
              pilihana: row[4].toString(),
              pilihanb: row[5].toString(),
              pilihanc: row[6].toString(),
              pilihand: row[7].toString(),
              pilihane: row[8].toString(),
              jawaban: row[9].toString(),
              bobot: row[10].toString(),
            );
            list.add(data);
          } else {
            if (row[4] / 1.0 != row[4]) {
              throw GeneralException(
                  cause: 'pengisian bobot tidak sesuai format untuk ESSAY');
            }
            String attpertanyaan = row[2] ?? '-';
            String pertanyaan = row[3] ?? '-';
            var data = QuizItem(
                materi: row[0],
                typesoal: row[1],
                jawaban: row[5].toString(),
                attpertanyaan: attpertanyaan.toString(),
                pertanyaan: pertanyaan,
                bobot: row[4].toString());
            list.add(data);
          }
        }
        await quizProvider.setQuestion(list);
        print(quizProvider.list.length);
        print(quizProvider.list.last.typesoal);
      }
      return true;
    } on GeneralException catch (e) {
      print(e);
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(e.cause, context);
      return false;
    } catch (e) {
      print('Error');
      await FilePicker.platform.clearTemporaryFiles();
      print(e);
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
          'Data yang diunggah belum sesuai format', context);
      return false;
    }
  }

  void callback(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(
              builder: (BuildContext context,
                  void Function(void Function()) setState) {
                return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                    height: S.w * .95,
                    width: S.w * .9,
                    padding: EdgeInsets.symmetric(
                        vertical: S.w * .05, horizontal: S.w * .1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Yang kamu butuhkan'),
                        ButtonTerapkan(
                            title: 'Terapkan',
                            callback: () {
                              Navigator.pop(context, true);
                              setState(() {});
                            }),
                        ButtonTerapkan(
                            title: 'Terapkan',
                            callback: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => BankSoal()));
                              setState(() {});
                            })
                      ],
                    ),
                  ),
                );
              },
            ));
  }
}
