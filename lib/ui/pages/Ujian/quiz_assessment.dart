part of '_ujian.dart';

class QuizAssessment extends StatefulWidget {
  final int id;
  QuizAssessment({this.id});

  @override
  _QuizAssessmentState createState() => _QuizAssessmentState();
}

class _QuizAssessmentState extends State<QuizAssessment> {
  List<QuizModelDetail> listQuestion;

  @override
  void initState() {
    super.initState();
    fetchQuestion();
  }

  Future<void> fetchQuestion() async {
    var data = await QuizServices.getBankSoalByQuiz(widget.id);
    setState(() {
      listQuestion = data.data;
    });
    print(listQuestion);
  }

  @override
  Widget build(BuildContext context) {
    var penilaianProvider = Provider.of<QuizProviderKelas>(context);
    var detail = penilaianProvider.detail;
    var orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      backgroundColor: SiswamediaTheme.background,
      appBar: AppBar(
        shadowColor: Colors.transparent,
        brightness: Brightness.light,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title:
            Text('Detail Kuis', style: semiBlack.copyWith(fontSize: S.w / 22)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              width: S.w,
              margin: EdgeInsets.symmetric(vertical: S.w * .03),
              padding: EdgeInsets.symmetric(
                  horizontal: S.w * .05, vertical: S.w * .05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(detail.courseName ?? '',
                      style: TextStyle(
                          color: SiswamediaTheme.green,
                          fontSize: S.w / 25,
                          fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                      'Jumlah Soal: ${(detail.totalPg > 0 && detail.totalEssay > 0) ? detail.totalPg + detail.totalEssay : detail.totalPg == -1 ? detail.totalEssay : detail.totalPg}',
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: S.w / 30,
                          fontWeight: FontWeight.w600)),
                  Text(
                      'Soal Pilihan Ganda : ${detail.totalPg == -1 ? "-" : detail.totalPg}',
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: S.w / 30,
                          fontWeight: FontWeight.w600)),
                  Text(
                      'Soal Esai : ${detail.totalEssay == -1 ? "-" : detail.totalEssay}',
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: S.w / 30,
                          fontWeight: FontWeight.w600)),
                  SizedBox(
                    height: 5,
                  ),
                  Text('Batas Waktu Pengerjaan : ${detail.totalTime} menit',
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: S.w / 30,
                          fontWeight: FontWeight.normal)),
                  SizedBox(height: 20),
                  CustomText('Keterangan', Kind.heading3),
                  SizedBox(height: 5),
                  Row(children: [
                    Container(
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: SiswamediaTheme.green,
                      ),
                      child: Icon(Icons.check, size: 18, color: Colors.white),
                    ),
                    SizedBox(width: 5),
                    CustomText('Siswa sudah mengerjakan', Kind.descBlack),
                  ])
                ],
              ),
            ),
            Container(
              color: Colors.white,
              height: S.w * .5,
              width: S.w,
              margin: EdgeInsets.symmetric(vertical: S.w * .03),
              padding: EdgeInsets.symmetric(
                  horizontal: S.w * .05, vertical: S.w * .05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text('Setting',
                      style: TextStyle(
                          color: SiswamediaTheme.green,
                          fontSize: S.w / 25,
                          fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: 5,
                  ),
                  SwitchListTile(
                    title: CustomText('Sembunyikan Nilai', Kind.heading3),
                    value: detail.isHidden,
                    onChanged: (val) {
                      penilaianProvider.switchHidden(context);
                    },
                  ),
                  SwitchListTile(
                    title: CustomText('Acak Soal', Kind.heading3),
                    value: detail.isRandom,
                    onChanged: (val) {
                      penilaianProvider.switchRandom(context);
                    },
                  ),
                ],
              ),
            ),
            Container(
              width: S.w,
              child: FutureBuilder<QuizByIdModel>(
                  future: QuizServices.getQuizById(widget.id),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return listStudents(
                        context: context,
                        orientation: orientation,
                        snapshot: snapshot.data.data.students ?? [],
                        size: S.w,
                        quizId: widget.id,
                        data: listQuestion,
                      );
                    }
                    return Center(child: CircularProgressIndicator());
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Widget listStudents({
    BuildContext context,
    List<Students> snapshot,
    double size,
    Orientation orientation,
    int quizId,
    List<QuizModelDetail> data,
  }) {
    var isPortrait = orientation == Orientation.portrait;
    var width = size;
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: width * .05, vertical: width * .03),
            child: Text('Beri Nilai Ujian Siswa',
                style: semiBlack.copyWith(fontSize: width / 24)),
          ),
          GridView.count(
            crossAxisCount: 4,
            padding: EdgeInsets.symmetric(
                horizontal: width * .02, vertical: width * .05),
            childAspectRatio: 1,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            children: (snapshot ??
                    [
                      Students(),
                    ])
                .map((e) => Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          if (e.hasBeenDone) {
                            showDialog<void>(
                              context: context,
                              builder: (_) => QuizDialog(
                                title: 'Opsi Penilaian',
                                description:
                                    'Penilaian memiliki 2 opsi, yaitu penilaian otomatis dan manual. Pilih manual untuk memberi penilaian sesuai dengan analisis Anda',
                                type: DialogType.onlyText,
                                actions: ['Otomatis', 'Manual'],
                                onTapFirstAction: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (_) => QuizAssessmentUser(
                                                data: data,
                                                type: QuizAssessmentType.auto,
                                                user: e,
                                                quizId: quizId,
                                              )));
                                },
                                onTapSecondAction: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (_) => QuizAssessmentUser(
                                                data: data,
                                                type: QuizAssessmentType.manual,
                                                user: e,
                                                quizId: quizId,
                                              )));
                                },
                              ),
                            );
                          } else {
                            showDialog<void>(
                              context: context,
                              builder: (_) => QuizDialog(
                                title: 'User ini Belum Mengerjakan Ujian!',
                                description:
                                    'Penilaian memiliki 2 opsi, yaitu penilaian otomatis dan manual. Pilih manual untuk memberi penilaian sesuai dengan analisis Anda',
                                type: DialogType.onlyText,
                                actions: ['Otomatis', 'Manual'],
                                onTapFirstAction: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (_) => QuizAssessmentUser(
                                                data: data,
                                                type: QuizAssessmentType.auto,
                                                user: e,
                                                quizId: quizId,
                                              )));
                                },
                                onTapSecondAction: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (_) => QuizAssessmentUser(
                                                data: data,
                                                type: QuizAssessmentType.manual,
                                                user: e,
                                                quizId: quizId,
                                              )));
                                },
                              ),
                            );
                          }
                        },
                        child: Column(
                          children: [
                            Stack(
                              children: [
                                Container(
                                  height: width * .15,
                                  width: width * .15,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                      image: DecorationImage(
                                          image: NetworkImage(
                                            e.image,
                                          ),
                                          fit: BoxFit.cover)),
                                  child: (e.hasBeenDone &&
                                          !e.totalNilai.isNegative)
                                      ? Container(
                                          height: width * .15,
                                          width: width * .15,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white.withOpacity(.5),
                                          ),
                                          child: Center(
                                              child: Text(
                                                  e.totalNilai.roundToDouble() ==
                                                          e.totalNilai
                                                      ? e.totalNilai
                                                          .round()
                                                          .toString()
                                                      : e.totalNilai
                                                          .toStringAsFixed(2),
                                                  style: boldBlack.copyWith(
                                                      fontSize: S.w / 22))),
                                        )
                                      : Container(),
                                ),
                                if (e.hasBeenDone)
                                  Positioned(
                                      top: 0,
                                      right: 0,
                                      child: Container(
                                        padding: EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: SiswamediaTheme.green,
                                        ),
                                        child: Icon(Icons.check,
                                            size: 18, color: Colors.white),
                                      ))
                              ],
                            ),
                            Expanded(
                                child: Text(
                              e.fullName,
                              style: descBlack.copyWith(fontSize: width / 34),
                              textAlign: TextAlign.center,
                            ))
                          ],
                        ),
                      ),
                    ))
                .toList(),
          ),
        ],
      ),
    );
  }
}
