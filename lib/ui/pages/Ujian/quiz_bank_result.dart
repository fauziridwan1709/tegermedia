part of '_ujian.dart';

class BankResult extends StatefulWidget {
  const BankResult({this.type, this.courseId});

  final String type;
  final int courseId;

  @override
  _BankResultState createState() => _BankResultState();
}

class _BankResultState extends State<BankResult> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _getListClass();
    });
  }

  void _getListClass() async {
    listClass = await ClassServices.getMeCourse();
    print(listClass);
  }

  ModelListCourse listClass;
  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    // var S.w = MediaQuery.of(context).size.width;
    print(data.filter.matapelajaran);
    print(data.filter.kelas);
    print(data.filter.kurikulum);
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
      backgroundColor: SiswamediaTheme.background,
      appBar: AppBar(
        brightness: Brightness.light,
        shadowColor: Colors.grey.withOpacity(.2),
        leading: IconButton(
            icon: Icon(
              Icons.clear,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Bank Soal',
            style: TextStyle(
                color: Colors.black,
                fontSize: S.w / 22,
                fontWeight: FontWeight.w600)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(),
            FutureBuilder<MateriQuizModel>(
                future: MateriQuizServices.getData(value: data.filter),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    print('dataa');
                    print(data.filter.matapelajaran);
                    print(data.filter.matapelajaran);
                    print(data.filter.kelas
                        .substring(data.filter.kelas.indexOf(' ') + 1));
                    var list = snapshot.data.rows
                        .where((element) =>
                            element.kelas.toString() ==
                                data.filter.kelas.substring(
                                    data.filter.kelas.indexOf(' ') + 1) &&
                            element.matapelajaran
                                .contains(data.filter.matapelajaran))
                        .toList();
                    if (list.isEmpty) {
                      return Padding(
                        padding: EdgeInsets.only(top: S.w / 3),
                        child: Text(
                          'Bank Soal Kosong!',
                          style: descBlack.copyWith(fontSize: S.w / 30),
                        ),
                      );
                    }
                    return GridView.count(
                        crossAxisCount: isPortrait ? 1 : 2,
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        crossAxisSpacing: 10,
                        childAspectRatio: 3,
                        children: list
                            .map(
                              (e) => Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: S.w * .05, vertical: S.w * .03),
                                margin: EdgeInsets.symmetric(
                                    horizontal: S.w * .05, vertical: S.w * .02),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(12),
                                    boxShadow: [
                                      BoxShadow(
                                        offset: Offset(2, 2),
                                        spreadRadius: 2,
                                        blurRadius: 2,
                                        color: Colors.grey.withOpacity(.2),
                                      )
                                    ]),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("${e.kelas} ${e.matapelajaran}",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontWeight: FontWeight.w700,
                                            fontSize: S.w / 25)),
                                    Text("Kode : ${e.materi}-1928",
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontWeight: FontWeight.w600,
                                            fontSize: S.w / 30)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("${e.jumlahsoal} soal"),
                                        Container(
                                          height: S.w * .07,
                                          width: S.w * .2,
                                          decoration: BoxDecoration(
                                            color: SiswamediaTheme.green,
                                            borderRadius:
                                                BorderRadius.circular(6),
                                          ),
                                          child: Material(
                                            color: Colors.transparent,
                                            child: InkWell(
                                              onTap: () {
                                                data.chooseItem(
                                                    MateriQuizDetail(
                                                  jumlahsoal: e.jumlahsoal,
                                                  kelas: e.kelas,
                                                  materi: e.materi,
                                                  matapelajaran:
                                                      e.matapelajaran,
                                                ));
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                        builder: (_) =>
                                                            BankChoose(
                                                              courseId: widget
                                                                  .courseId,
                                                              type: widget.type,
                                                              listClass:
                                                                  listClass,
                                                            )));
                                              },
                                              child: Center(
                                                  child: Text("Pilih",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: S.w / 32))),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            )
                            .toList());
                  }
                  return Padding(
                    padding: EdgeInsets.only(top: S.w / 3),
                    child: Center(child: CircularProgressIndicator()),
                  );
                })
          ],
        ),
      ),
    );
  }
}
