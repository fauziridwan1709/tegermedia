part of '_ujian.dart';

class QuizMediaSekolah extends StatefulWidget {
  @override
  _QuizMediaSekolahState createState() => _QuizMediaSekolahState();
}

class _QuizMediaSekolahState extends BasePaginationState<QuizMediaSekolah, QuizMediaState> {

 @override
  void init() {
    // TODO: implement init
  }

  @override
  Future<void> retrieveData() async {
    var query = QueryQuiz(page: 1);
    await thisState.setState((s) => s.retrieveData(query));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return EmptyAppBar();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, ReactiveModel<QuizMediaState> snapshot, SizingInformation sizeInfo) {
    return Container(
      child: RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: retrieveData,
        child: WhenRebuilder<QuizMediaState>(
          observe: () => thisState,
          onIdle: () => WaitingView(),
          onWaiting: () => WaitingView(),
          onError: (dynamic error) => ErrorView(error: error),
          onData: (data) {
            return EmptyView(
                title: 'Empty',
                description: 'Tidak ditemukan quiz',
                isEmpty: (data.listData ?? []).isEmpty,
                child: ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  shrinkWrap: true,
                  controller: scrollController,
                  itemCount:
                  data.hasReachedMax ? data.listData.length : data.listData.length + 1,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  itemBuilder: (_, index) {
                    if (data.listData.length == index) {
                      return LoadingView();
                    }
                    var e = data.listData[index];
                    return _buildCardQuizSekolah(e);
                  },
                ));
          },
        ),
      ),
    );
  }

 Widget _buildCardQuizSekolah(QuizListDetail data) {
   return CardQuizSekolah(
     data: data,
     // onDelete: bottomSheet,
     // authState: authState,
     onTap: (id) {},
   );
 }

  @override
  Widget buildWideLayout(BuildContext context, ReactiveModel<QuizMediaState> snapshot, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, snapshot, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  void onScroll() {
    completer.complete();
    thisState.state.retrieveMoreData().then((value) {
      completer = Completer<Null>();
      thisState.notify();
    });
  }

  @override
  bool scrollCondition() {
    return !thisState.state.hasReachedMax;
  }
}
