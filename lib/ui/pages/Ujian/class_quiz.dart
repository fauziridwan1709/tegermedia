part of '_ujian.dart';

class QuizClass extends StatefulWidget {
  @override
  _QuizClassState createState() => _QuizClassState();
}

class _QuizClassState extends State<QuizClass> with TickerProviderStateMixin {
  final authState = Injector.getAsReactive<AuthState>();
  String _choose1;
  String _choose2;
  String _choose3;
  String msg;
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  TabController tabController;

  @override
  void initState() {
    super.initState();
    _getClassBySchoolId(context);
    tabController = TabController(initialIndex: 0, length: 2, vsync: this);
  }

  void _getClassBySchoolId(BuildContext context) async {
    var result = await ClassServices.getClassBySchoolID(
        id: authState.state.currentState.schoolId, param: true);
    //todo getclass
    // if (result.statusCode == 200) {
    //   provider.setMeClass(result.data);
    // } else {
    //   provider.setMeClass([]);
    // }
  }

  @override
  Widget build(BuildContext context) {
    var auth = authState.state;
    var penilaianProvider = Provider.of<QuizProviderKelas>(context);
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    var ratio = MediaQuery.of(context).size.width / MediaQuery.of(context).size.height;
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        shadowColor: Colors.transparent,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        title: TitleAppbar(label: 'Ujian'),
        actions: [
          auth.currentState.schoolRole.isEmpty
              ? SizedBox()
              : (auth.currentState.classRole.isGuruOrWaliKelas)
                  ? IconButton(
                      icon: Icon(Icons.add),
                      color: SiswamediaTheme.green,
                      onPressed: () => callback(context),
                    )
                  : SizedBox(),
        ],
        bottom: TabBar(
          controller: tabController,
          unselectedLabelColor: Colors.grey,
          labelColor: Colors.black,
          indicatorWeight: 4,
          tabs: [
            Tab(text: 'Quiz'),
            Tab(text: 'Media Sekolah'),
          ],
        ),
      ),
      body: TabBarView(controller: tabController, children: [
        SingleChildScrollView(
          child: Column(
            children: [
              ///for filter
              // Padding(
              //   padding: EdgeInsets.symmetric(horizontal: width * .05),
              //   child: RaisedButton(
              //       shape: RoundedRectangleBorder(borderRadius: radius(200)),
              //       onPressed: () {
              //         showModalBottomSheet<void>(
              //             shape: RoundedRectangleBorder(
              //               borderRadius: BorderRadius.circular(12),
              //             ),
              //             context: context,
              //             builder: (context) => Container(
              //                   height: width / 4,
              //                   padding: EdgeInsets.symmetric(
              //                       horizontal: width * .05,
              //                       vertical: width * .05),
              //                   decoration: BoxDecoration(
              //                       color: Colors.white,
              //                       borderRadius: BorderRadius.only(
              //                           topLeft: const Radius.circular(20.0),
              //                           topRight: const Radius.circular(20.0))),
              //                   child: Column(
              //                     //mainAxisAlignment:
              //                     //  MainAxisAlignment.spaceEvenly,
              //                     children: [
              //                       // ListTile(
              //                       //   leading: Icon(Icons.edit),
              //                       //   title: Text(
              //                       //     'Ubah Forum',
              //                       //     style: secondHeadingBlack.copyWith(fontSize: width / 28),
              //                       //   ),
              //                       //   onTap: () {},
              //                       // ),
              //                       ListTile(
              //                         leading: Icon(Icons.delete),
              //                         title: Text('Hapus Forum',
              //                             style: secondHeadingBlack.copyWith(
              //                                 fontSize: width / 28)),
              //                         onTap: () async {
              //                           await delete(context, id);
              //                         },
              //                       ),
              //                     ],
              //                   ),
              //                 ));
              //       },
              //       color: SiswamediaTheme.green,
              //       child: Row(
              //           mainAxisAlignment: MainAxisAlignment.start,
              //           children: [
              //             Icon(Icons.filter_list_sharp, color: Colors.white),
              //             SizedBox(width: 5),
              //             Text('Filter',
              //                 style: semiWhite.copyWith(fontSize: width / 30))
              //           ])),
              // ),
              FutureBuilder<QuizListModel>(
                  future: QuizServices.getQuiz(
                      classId: auth.currentState.classId,
                      isOrtu: auth.currentState.classRole.isOrtu),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return ErrorView();
                    } else if (snapshot.hasData && snapshot.data.data.isNotEmpty) {
                      return Container(
                        margin: EdgeInsets.symmetric(vertical: S.w * .03),
                        child: GridView.count(
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          crossAxisCount: gridCountHome(ratio),
                          childAspectRatio: isPortrait ? 2 : 2,
                          children: snapshot.data.data.map(_buildCardQuiz).toList(),
                        ),
                      );
                    } else {
                      if (ConnectionState.waiting == snapshot.connectionState) {
                        return Center(
                            child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green)));
                      }
                    }
                    return Center(
                      child: Padding(
                          padding: EdgeInsets.only(top: S.w * .5),
                          child: Column(
                            children: [
                              Image.asset('assets/no_quiz.png', height: S.w * .4, width: S.w * .4),
                              Text('Belum ada quiz'),
                            ],
                          )),
                    );
                  })
            ],
          ),
        ),
        QuizMediaSekolah(),
      ]),
    );
  }

  Widget _buildCardQuiz(QuizListDetail data) {
    print('mantap');
    return CardQuiz(
      data: data,
      onDelete: bottomSheet,
      authState: authState,
      onTap: (id) {},
    );
  }



  Widget sectionNotLogin(BuildContext context) {
    var height = MediaQuery.of(context).size.height;

    return Container(
        height: height,
        width: S.w,
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              child: Image.asset(
                'assets/dev.png',
                alignment: Alignment.bottomCenter,
                height: S.w * .6,
                width: S.w,
                fit: BoxFit.contain,
              ),
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: height / 4 - 20, left: 40, right: 40),
                  child: Text('Oops kelas Personal tidak dapat mengakses fitur ini',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: S.w / 30, fontWeight: FontWeight.w700, color: Colors.black87)),
                ),
              ],
            ),
          ],
        ));
  }

  void bottomSheet(BuildContext context, QuizListDetail data) {
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        context: context,
        builder: (context2) => Container(
              // height: S.w / 4,
              padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(12.0), topRight: const Radius.circular(12.0))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                //mainAxisAlignment:
                //  MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 5,
                    width: 35,
                    decoration: BoxDecoration(borderRadius: radius(200), color: Colors.grey[300]),
                  ),
                  SizedBox(height: 10),
                  ListTile(
                    leading: Icon(Icons.edit),
                    title: Text(
                      'Ubah Quiz',
                      style: secondHeadingBlack.copyWith(fontSize: S.w / 28),
                    ),
                    onTap: () async {
                      Navigator.pop(context);
                      await navigate(
                          context,
                          UpdateQuiz(
                              title: data.name,
                              duration: data.totalTime,
                              startDate: data.startDate,
                              id: data.id));
                      // title.text = judul;
                      // desc.text = deskripsi;
                      // await update(
                      //   context,
                      //   id,
                      // );
                    },
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.delete),
                    title:
                        Text('Hapus Quiz', style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                    onTap: () async {
                      Navigator.pop(context);
                      await showDeleteDialog(context, data.id);
                    },
                  ),
                ],
              ),
            ));
  }

  void showDeleteDialog(BuildContext context, int id) {
    //todo
    DeleteDialog(context,
        title: 'Menghapus Quiz',
        description: 'Apakah Anda yakin untuk menghapus quiz ini?', onAgree: () async {
      SiswamediaTheme.loading(context);
      await QuizServices.deleteQuiz(id).then((value) async {
        Navigator.pop(context);
        if (value.statusCode == 200) {
          Navigator.pop(context);
          //todo
          // await tugasState.setState((s) => s.deleteTugas(id));
          setState(() {});
          return CustomFlushBar.successFlushBar(
            value.message,
            context,
          );
        } else {
          Navigator.pop(context);
          return CustomFlushBar.errorFlushBar(
            value.message,
            context,
          );
        }
      });
    })
      ..show();
  }

  void callback(BuildContext context) {
    _choose1 = null;
    _choose2 = null;
    _choose3 = null;
    msg = '';
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(
              builder: (BuildContext context, void Function(void Function()) setState) {
                return Dialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                  child: Container(
                    width: S.w * .7,
                    padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Pilih Metode',
                            style: TextStyle(fontSize: S.w / 30, fontWeight: FontWeight.w600)),
                        SizedBox(height: 8),
                        Container(
                          width: S.w * .6,
                          height: S.w * .1,
                          decoration: BoxDecoration(
                            color: Color(0xff707070),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context, MaterialPageRoute<void>(builder: (_) => QuizUpload()));
                            },
                            child: Center(
                                child: Text('Google Drive',
                                    style: TextStyle(
                                      fontSize: S.w / 30,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ))),
                          ),
                        ),
                        SizedBox(height: 8),
                        Container(
                          width: S.w * .6,
                          height: S.w * .1,
                          decoration: BoxDecoration(
                            color: SiswamediaTheme.green,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: InkWell(
                            onTap: () => context.push<void>(BankSoal()),
                            child: Center(
                                child: Text('Bank Soal',
                                    style: TextStyle(
                                      fontSize: S.w / 30,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ))),
                          ),
                        ),
                        // SizedBox(height: 8),
                        // Container(
                        //   width: S.w * .6,
                        //   height: S.w * .1,
                        //   decoration: BoxDecoration(
                        //     color: SiswamediaTheme.green,
                        //     borderRadius: BorderRadius.circular(8),
                        //   ),
                        //   child: InkWell(
                        //     onTap: () {
                        //       Navigator.push(
                        //           context,
                        //           MaterialPageRoute<void>(
                        //               builder: (_) => BankSoal()));
                        //     },
                        //     child: Center(
                        //         child: Text('Buat Soal',
                        //             style: TextStyle(
                        //               fontSize: S.w / 30,
                        //               color: Colors.white,
                        //               fontWeight: FontWeight.w600,
                        //             ))),
                        //   ),
                        // ),
                        InkWell(
                            onTap: () => Navigator.pop(context),
                            child: Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .03),
                              child: Text('Batal'),
                            ))
                      ],
                    ),
                  ),
                );
              },
            ));
  }

  Future<void> getFile() async {
    try {
      var files = await FilePicker.platform
          .pickFiles(allowMultiple: false, type: FileType.custom, allowedExtensions: ['xlsx']);
      var file = files.files.first;
      var bytes = file.bytes;
      var excel = Excel.decodeBytes(bytes);
      for (var table in excel.tables.keys) {
        print(table); //sheet Name
        print(excel.tables[table].maxCols);
        print(excel.tables[table].maxRows);
        for (var row in excel.tables[table].rows) {
          print('$row');
        }
      }
    } catch (e) {
      print('Error');
    }
  }

  void filePick() {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(
              builder: (BuildContext context, void Function(void Function()) setState) {
                return Dialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                  child: Container(
                    height: S.w * .95,
                    width: S.w * .9,
                    padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Yang kamu butuhkan'),
                        Container(
                          width: S.w * .6,
                          height: S.w * .1,
                          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                          decoration: SiswamediaTheme.dropDownDecoration,
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                                icon: Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.blue,
                                ),
                                hint: Text('Mata Pelajaran'),
                                value: _choose1,
                                onChanged: (String o) {
                                  setState(() {
                                    _choose1 = o;
                                  });
                                },
                                items: QuizList.filterPelajaran
                                    .map((e) => DropdownMenuItem(
                                          value: e,
                                          child: Text(e),
                                        ))
                                    .toList()),
                          ),
                        ),
                        Container(
                          width: S.w * .6,
                          height: S.w * .1,
                          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                          decoration: SiswamediaTheme.dropDownDecoration,
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                                icon: Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.blue,
                                ),
                                hint: Text('Status Quiz'),
                                value: _choose2,
                                onChanged: (String o) {
                                  setState(() {
                                    _choose2 = o;
                                  });
                                },
                                items: QuizList.filterStatus
                                    .map((e) => DropdownMenuItem(
                                          value: e,
                                          child: Text(e),
                                        ))
                                    .toList()),
                          ),
                        ),
                        Column(children: [
                          Text(msg, style: TextStyle(color: Colors.red)),
                          SizedBox(height: S.w / 60),
                          ButtonTerapkan(
                              title: 'Terapkan',
                              callback: () {
                                if (_choose1 == null || _choose2 == null || _choose3 == null) {
                                  setState(() => msg = 'isi semua data yaa');
                                } else {
                                  Navigator.pop(context, true);
                                  setState(() {});
                                }
                              })
                        ]),
                      ],
                    ),
                  ),
                );
              },
            ));
  }
}

class QuizList {
  static const filterPelajaran = ['Matematika', 'IPA', 'IPS'];
  static const filterStatus = ['Mulai', 'Selesai', 'Dinilai'];
}
