part of '_ujian.dart';

class QuizInfoKelas extends StatefulWidget {
  @override
  _QuizInfoKelasState createState() => _QuizInfoKelasState();
}

class _QuizInfoKelasState extends State<QuizInfoKelas> {
  final authState = GlobalState.auth();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var desc =
      'Untuk soal esai yang membutuhkan rumus silahkan kerjakan di kertas, dan kumpulkan dengan cara foto jawaban per soal lalu unggah foto anda di tombol unggah di bawah ini (pastikan foto nya jelas)';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    var detail = data.detail;
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    var hehe = Color(0xffE8F5D6);
    var time = DateTime.parse(data.detail.startDate)
        .toLocal()
        .add(Duration(minutes: data.detail.totalTime));
    print(time);
    print(DateTime.now());
    print(time.isAfter(DateTime.now()));
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        brightness: Brightness.light,
        centerTitle: true,
        title: Text('${detail.name}',
            style: TextStyle(color: Colors.black, fontSize: S.w / 23)),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
      ),
      body: SingleChildScrollView(
        child: FutureBuilder<QuizModelKelas>(
            future: QuizServices.getBankSoalByQuiz(detail.id),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                print(snapshot.data.data);
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      height: S.w * .5,
                      width: S.w,
                      margin: EdgeInsets.symmetric(vertical: S.w * .03),
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(detail.courseName,
                              style: TextStyle(
                                  color: SiswamediaTheme.green,
                                  fontSize: S.w / 25,
                                  fontWeight: FontWeight.w700)),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                              'Jumlah Soal: ${(detail.totalPg > 0 && detail.totalEssay > 0) ? detail.totalPg + detail.totalEssay : detail.totalPg == -1 ? detail.totalEssay : detail.totalPg}',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w600)),
                          Text(
                              "Soal Pilihan Ganda : ${detail.totalPg == -1 ? "-" : detail.totalPg}",
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w600)),
                          Text(
                              "Soal Esai : ${detail.totalEssay == -1 ? "-" : detail.totalEssay}",
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w600)),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                              'Batas Waktu Pengerjaan : ${detail.totalTime} menit',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.normal)),
                          !detail.hasBeenDone
                              ? SizedBox()
                              : Row(
                                  children: [
                                    Text(
                                        'nilai${authState.state.currentState.classRole.isOrtu ? ' anak' : ''}: '),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: S.w * .02, horizontal: 10),
                                      child: Center(
                                        child: Text(
                                          '${detail.isHidden ? 'Not Permitted' : detail.totalNilai == -1 ? 'Belum Dinilai' : formatNilai(detail.totalNilai)}',
                                          maxLines: 1,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: S.w / 32),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        color: SiswamediaTheme.green
                                            .withOpacity(0.8),
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                    ),
                                  ],
                                )
                        ],
                      ),
                    ),

                    ///petunjuk
                    // Container(
                    //   width: width,
                    //   color: Colors.white,
                    //   height: width * .6,
                    //   padding: EdgeInsets.symmetric(
                    //       horizontal: width * .05, vertical: width * .05),
                    //   margin: EdgeInsets.symmetric(vertical: width * .03),
                    //   child: Column(
                    //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    //     children: [
                    //       Text('Petunjuk',
                    //           style: TextStyle(
                    //               fontSize: width / 25,
                    //               color: Colors.black,
                    //               fontWeight: FontWeight.w600)),
                    //       Container(
                    //         height: width * .4,
                    //         child: Center(
                    //             child: Container(
                    //                 width: width * .1,
                    //                 child: Center(
                    //                   child: Icon(
                    //                     Icons.play_arrow,
                    //                     color: Colors.white,
                    //                   ),
                    //                 ),
                    //                 decoration: BoxDecoration(
                    //                     shape: BoxShape.circle,
                    //                     color: SiswamediaTheme.green,
                    //                     border: Border.all(
                    //                       color: Colors.white,
                    //                       width: 2,
                    //                     )))),
                    //         decoration: BoxDecoration(
                    //           borderRadius: BorderRadius.circular(width / 30),
                    //           color: Colors.grey,
                    //         ),
                    //       )
                    //     ],
                    //   ),
                    // ),
                    Container(
                      height: S.w * .42,
                      width: S.w,
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      margin: EdgeInsets.symmetric(
                          vertical: S.w * .03,
                          horizontal: isPortrait ? S.w * .05 : 0.0),
                      decoration: BoxDecoration(
                        color: hehe,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Column(children: [
                        Text('Catatan',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: S.w / 25,
                                fontWeight: FontWeight.w700)),
                        Text(desc,
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: S.w / 28,
                            )),
                      ]),
                    ),
                    if (authState.state.currentState.classRole == 'SISWA' &&
                        !data.detail.hasBeenDone &&
                        DateTime.parse(data.detail.startDate)
                            .toLocal()
                            .add(Duration(minutes: data.detail.totalTime))
                            .isAfter(DateTime.now()))
                      Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: S.w * .05, vertical: S.w * .05),
                          height: S.w * .12,
                          width: S.w * .9,
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                data.setQuizQuestion(snapshot.data.data);
                                print(detail.totalPg);
                                if (snapshot.data.data.isEmpty) {
                                  return CustomFlushBar.errorFlushBar(
                                    'Aduh ga ada soal di quiz ini',
                                    context,
                                  );
                                }
                                if (detail.hasBeenDone) {
                                  return CustomFlushBar.errorFlushBar(
                                    'Anda sudah mengerjakan Ujian ini',
                                    context,
                                  );
                                }
                                if (detail.totalPg != -1) {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (_) => QuizStart(
                                              quizId: detail.id,
                                              duration: DateTime.parse(
                                                      data.detail.startDate)
                                                  .toLocal()
                                                  .add(Duration(
                                                      minutes: data
                                                          .detail.totalTime))
                                                  .difference(DateTime.now())
                                                  .inMinutes)));
                                } else {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (_) => QuizStartEssay(
                                              duration: DateTime.parse(
                                                      data.detail.startDate)
                                                  .toLocal()
                                                  .add(Duration(
                                                      minutes: data
                                                          .detail.totalTime))
                                                  .difference(DateTime.now())
                                                  .inMinutes)));
                                }
                              },
                              child: Center(
                                  child: Text('mulai',
                                      style: TextStyle(
                                          fontSize: S.w / 28,
                                          color: Colors.white))),
                            ),
                          ),
                          decoration: BoxDecoration(
                            color: SiswamediaTheme.green,
                            borderRadius: BorderRadius.circular(S.w / 30),
                          )),
                    if (isPortrait) SizedBox(height: S.w * .5),
                  ],
                );
              }
              return Padding(
                padding: EdgeInsets.only(top: S.w / 3),
                child: Center(child: CircularProgressIndicator()),
              );
            }),
      ),
    );
  }
}
