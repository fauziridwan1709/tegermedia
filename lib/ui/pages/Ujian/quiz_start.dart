part of '_ujian.dart';

class QuizStart extends StatefulWidget {
  final int duration;
  final int quizId;
  QuizStart({this.duration, this.quizId});

  @override
  _QuizStartState createState() => _QuizStartState();
}

class _QuizStartState extends State<QuizStart> {
  final authState = GlobalState.auth();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  Timer timer;
  int _timerValue;

  int myIndex = 0;
  Map<int, dynamic> correction = <int, dynamic>{};
  Map<String, dynamic> currentAnswer = <String, dynamic>{};

  TextEditingController controller = TextEditingController();
  var prefs = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();
    _timerValue = widget.duration * 60;
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await startTimer();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  Timer _timer;

  void startTimer() async {
    print('kita');
    _timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      setState(() {
        print(_timerValue);
        print('kita');
      });
      if (_timerValue < 1) {
        _timer.cancel();
        showDialog<void>(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return WillPopScope(
                onWillPop: () async => false,
                child: Dialog(
                  shape: RoundedRectangleBorder(borderRadius: radius(20)),
                  child: Container(
                      width: S.w / 3,
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('Waktu Habis!',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: S.w / 28,
                                  fontWeight: FontWeight.w700)),
                          SizedBox(height: 20),
                          Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: S.w * .01),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: SiswamediaTheme.green,
                              ),
                              height: S.w / 10,
                              width: S.w / 3,
                              child: InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute<void>(
                                  //         builder: (_) => ResultPage(
                                  //           duration: _timerValue,
                                  //           currentAnswer:
                                  //           currentAnswer,
                                  //           rightAnswer: correction,
                                  //         )));
                                },
                                child: Center(
                                  child: Text('Close',
                                      style: TextStyle(color: Colors.white)),
                                ),
                              ),
                            ),
                          )
                        ],
                      )),
                ),
              );
            }).then(
          (value) async {
            // ignore: unawaited_futures
            showDialog<void>(
                barrierDismissible: false,
                context: context,
                builder: (context) => WillPopScope(
                      onWillPop: () async => false,
                      child: Center(child: CircularProgressIndicator()),
                    ));
            var jawabans = <Map<String, dynamic>>[];
            correction.forEach((key, dynamic value) {
              jawabans.add(<String, dynamic>{
                'quiz_bank_soal_id': key,
                'answer_description': correction[key],
                'noted': 'ini adalah jawaban'
              });
            });
            var submit = await PenilaianServices.submitJawaban(
                quizId: widget.quizId, jawaban: jawabans);
            if (submit.statusCode == 200) {
              Navigator.pop(context);
              Navigator.pop(context);
              Navigator.pop(context);
              CustomFlushBar.successFlushBar(
                'Jawabanmu telah tersubmit',
                context,
              );
            } else {
              Navigator.pop(context);
              Navigator.pop(context);
              Navigator.pop(context);
              CustomFlushBar.errorFlushBar(
                'Terjadi Kesalahan',
                context,
              );
            }
          },
          // Navigator.push(
          // context,
          // MaterialPageRoute<void>(
          //     builder: (_) => ResultPage(
          //       duration: _timerValue,
          //       currentAnswer: currentAnswer,
          //       rightAnswer: correction,
          //     )))
        );
        // Navigator.pushReplacement(
        //     context, MaterialPageRoute<void>(builder: (context) => Layout()));
      } else {
        _timerValue = _timerValue - 1;
        print(_timerValue);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    // var auth = context.watch<UserProvider>();
    var quiz = data.listQuiz;
    print(currentAnswer);
    print('a');
    print(correction);
    return ChangeNotifierProvider(
      create: (_) => QuizKelas(),
      child: WillPopScope(
        onWillPop: () async {
          return showDialog<bool>(
              context: context,
              builder: (context) {
                return Dialog(
                  shape: RoundedRectangleBorder(borderRadius: radius(20)),
                  child: Container(
                      width: S.w / 3,
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('Tinggalkan ujian?!',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: S.w / 28,
                                  fontWeight: FontWeight.w700)),
                          SizedBox(height: 20),
                          Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: S.w * .01),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: SiswamediaTheme.green,
                              ),
                              height: S.w / 10,
                              width: S.w / 3,
                              child: InkWell(
                                onTap: () async {
                                  // ignore: unawaited_futures
                                  showDialog<void>(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (context) => WillPopScope(
                                            onWillPop: () async => false,
                                            child: Center(
                                                child:
                                                    CircularProgressIndicator()),
                                          ));
                                  var jawabans = <Map<String, dynamic>>[];
                                  correction.forEach((key, dynamic value) {
                                    jawabans.add(<String, dynamic>{
                                      'quiz_bank_soal_id': key,
                                      'answer_description': correction[key],
                                      'noted': 'ini adalah jawaban'
                                    });
                                  });
                                  var submit =
                                      await PenilaianServices.submitJawaban(
                                          quizId: widget.quizId,
                                          jawaban: jawabans);
                                  if (submit.statusCode == 200) {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    CustomFlushBar.successFlushBar(
                                      'Jawabanmu telah tersubmit',
                                      context,
                                    );
                                    return true;
                                  } else {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    CustomFlushBar.errorFlushBar(
                                      'Terjadi Kesalahan',
                                      context,
                                    );
                                    return false;
                                  }
                                },
                                child: Center(
                                  child: Text('Ya',
                                      style: TextStyle(color: Colors.white)),
                                ),
                              ),
                            ),
                          )
                        ],
                      )),
                );
              });
        },
        child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.white,
            shadowColor: Colors.transparent,
            brightness: Brightness.light,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: SiswamediaTheme.green,
              onPressed: () {
                _timer.cancel();
                Navigator.pop(context);
              },
            ),
            centerTitle: true,
            actions: [
              Center(
                child: InkWell(
                  onTap: () => showDialog<void>(
                      context: context,
                      builder: (context) {
                        var contentText =
                            'Apakah anda yakin untuk menyelesaikannya?';
                        return StatefulBuilder(
                            builder: (context, setState) => AlertDialog(
                                  title: Text('Warning!'),
                                  content: Text(contentText),
                                  actions: [
                                    FlatButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: Text('Batal'),
                                    ),
                                    FlatButton(
                                      onPressed: () async {
                                        // await SubmitJawaban
                                        // ignore: unawaited_futures
                                        showDialog<void>(
                                            context: context,
                                            builder: (context) => Center(
                                                child:
                                                    CircularProgressIndicator()));
                                        var jawabans = <Map<String, dynamic>>[];
                                        correction
                                            .forEach((key, dynamic value) {
                                          jawabans.add(<String, dynamic>{
                                            'quiz_bank_soal_id': key,
                                            'answer_description':
                                                correction[key],
                                            'noted': 'ini adalah jawaban'
                                          });
                                        });
                                        var submit = await PenilaianServices
                                            .submitJawaban(
                                                quizId: quiz.first.quizId,
                                                jawaban: jawabans);
                                        if (submit.statusCode == 200) {
                                          _timer.cancel();
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          CustomFlushBar.successFlushBar(
                                            'Jawabanmu telah tersubmit',
                                            context,
                                          );
                                          await prefs.then((value) async {
                                            ///userId from shared preferences
                                            // var userId = int.parse(
                                            //     value.getString('user_id'));
                                            // var answerSheet =
                                            //     await QuizServices.answerSheet(
                                            //         userId: userId,
                                            //         quizId: data.detail.id);
                                            // var listNilai =
                                            //     <Map<String, dynamic>>[];
                                            //
                                            // answerSheet.data.forEach((element) {
                                            //   if (element.quizAnswer.id != 0) {
                                            //     listNilai.add(<String, dynamic>{
                                            //       'user_id': userId,
                                            //       'quiz_answer_ref_id':
                                            //           element.quizAnswer.id,
                                            //       'penilaian_type': 'OTOMATIS'
                                            //     });
                                            //   }
                                            // });

                                            ///quiz id from provider
                                            // var result = await PenilaianServices
                                            //     .nilaiLembarJawab(
                                            //         quizId: data.detail.id,
                                            //         nilai: listNilai);
                                            // if (result == 200 || result == 201) {
                                            //   Navigator.pop(context);
                                            //   Navigator.pop(context);
                                            //   Navigator.pop(context);
                                            //   Navigator.pop(context);
                                            //   CustomFlushBar.successFlushBar(
                                            //       'Jawabanmu otomatis dinilai',
                                            //       context);
                                            // } else {
                                            //   Navigator.pop(context);
                                            //   Navigator.pop(context);
                                            //   CustomFlushBar.errorFlushBar(
                                            //       'Tidak dapat menilai', context);
                                            // }
                                          });
                                        } else {
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          CustomFlushBar.errorFlushBar(
                                            'Terjadi Kesalahan',
                                            context,
                                          );
                                        }

                                        /*Navigator.push(
                                            context,
                                            MaterialPageRoute<void>(
                                                builder: (_) => ResultPage(
                                                      currentAnswer:
                                                          currentAnswer,
                                                      rightAnswer: correction,
                                                    )));*/
                                      },
                                      child: Text('Tentu'),
                                    )
                                  ],
                                ));
                      }),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                    child: Text(
                      'Selesai',
                      style: TextStyle(
                          color: SiswamediaTheme.green, fontSize: S.w / 30),
                    ),
                  ),
                ),
              )
            ],
            title: Text(
              'Ujian',
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: S.w / 25,
                  color: Colors.black),
            ),
          ),
          backgroundColor: SiswamediaTheme.background,
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: S.w * .05),
              child: Column(
                children: [
                  Consumer<QuizKelas>(
                    builder: (context, state, child) => SizedBox(
                      height: 50,
                      width: S.w,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: quiz.length,
                        itemBuilder: (context, index) => Container(
                            height: 40,
                            width: 40,
                            margin: EdgeInsets.only(
                                left: index == 0 ? S.w * .05 : 6.0,
                                top: 6,
                                right: index == quiz.length - 1 ? S.w * .05 : 6,
                                bottom: 6),
                            decoration: BoxDecoration(
                                border: myIndex == index
                                    ? Border.all(color: SiswamediaTheme.green)
                                    : null,
                                borderRadius: BorderRadius.circular(12),
                                color: state.selected[(index + 1).toString()] !=
                                        null
                                    ? SiswamediaTheme.green
                                    : Colors.white),
                            child: Material(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(12),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    myIndex = index;
                                    controller.text = '';
                                    controller.text =
                                        currentAnswer['${myIndex + 1}'];
                                  });
                                },
                                child: Container(
                                  child: Center(
                                      child: Text('${index + 1}',
                                          style: TextStyle(
                                              color: state.selected[(index + 1)
                                                          .toString()] !=
                                                      null
                                                  ? Colors.white
                                                  : SiswamediaTheme.green,
                                              fontSize: 14))),
                                ),
                              ),
                            )),
                      ),
                    ),
                  ),
                  if (quiz[myIndex].image != '-' && quiz[myIndex].image != '0')
                    Stack(
                      children: [
                        Shimmer.fromColors(
                          baseColor: Colors.grey[300],
                          highlightColor: Colors.grey[200],
                          enabled: true,
                          child: Container(
                            height: 170,
                            width: 170,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.grey[300]),
                          ),
                        ),
                        Container(
                          height: 170,
                          width: 170,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: SiswamediaTheme.white),
                          foregroundDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              image: DecorationImage(
                                // fit: BoxFit.cover,
                                image: NetworkImage(quiz[myIndex].image),
                              )),
                        ),
                      ],
                    ),
                  // Container(
                  //   height: 170,
                  //   width: 170,
                  //   decoration: BoxDecoration(color: Colors.grey[300]),
                  //   foregroundDecoration: BoxDecoration(
                  //       color: SiswamediaTheme.background,
                  //       image: DecorationImage(
                  //           image: NetworkImage(quiz[myIndex].image))),
                  // ),
                  Card(
                    margin: EdgeInsets.symmetric(
                        horizontal: S.w * .03, vertical: S.w * .02),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Container(
                      width: S.w * .94,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: S.w * .03, horizontal: S.w * .05),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Soal',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: S.w / 22,
                                    fontWeight: FontWeight.w600)),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 20),
                              child: Text(quiz[myIndex].questionDesc,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  if (quiz[myIndex].questionType == 'ESSAY')
                    Consumer<QuizKelas>(
                      builder: (context, state, child) {
                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: S.w * .03),
                          child: TextField(
                            controller: controller,
                            onChanged: (text) {
                              state.adding((myIndex + 1).toString(), text);
                              currentAnswer = state.selected;
                              correction[quiz[myIndex].id] = text;
                            },
                            style: descBlack.copyWith(fontSize: S.w / 30),
                            maxLines: 4,
                            minLines: 3,
                            decoration: InputDecoration(
                              hintText: 'jawaban..',
                              hintStyle: descBlack.copyWith(
                                  fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 25),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xffD8D8D8)),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: SiswamediaTheme.green),
                                borderRadius: BorderRadius.circular(12),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  if (quiz[myIndex].questionType == 'PG')
                    Consumer<QuizKelas>(
                      builder: (context, state, child) {
                        var sampleData = <String>[
                          quiz[myIndex].reference[0].pgDescription,
                          quiz[myIndex].reference[1].pgDescription,
                          quiz[myIndex].reference[2].pgDescription,
                        ];
                        if (quiz[myIndex].reference.length > 3) {
                          sampleData
                              .add(quiz[myIndex].reference[3].pgDescription);
                          if (quiz[myIndex].reference.length > 4) {
                            sampleData
                                .add(quiz[myIndex].reference[4].pgDescription);
                          }
                        }
                        return Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: sampleData.map((opt) {
                              return Container(
                                width: S.w,
                                margin: EdgeInsets.symmetric(
                                    horizontal: 0, vertical: 7),
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey.withOpacity(.2),
                                        offset: Offset(0, 0),
                                        spreadRadius: 2,
                                        blurRadius: 2)
                                  ],
                                  borderRadius: BorderRadius.circular(22),
                                  color: state.selected[
                                              (myIndex + 1).toString()] ==
                                          opt
                                      ? Color(0xffE8F5D6)
                                      : Colors.white,
                                ),
                                child: Material(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.circular(22),
                                  child: InkWell(
                                    onTap: () {
                                      state.adding(
                                          (myIndex + 1).toString(), opt);
                                      currentAnswer = state.selected;
                                      correction[quiz[myIndex].id] = opt;
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(16),
                                      child: Row(
                                        children: [
                                          Text(QuizChoice
                                              .choice[sampleData.indexOf(opt)]),
                                          Expanded(
                                            child: Container(
                                              margin: EdgeInsets.only(left: 16),
                                              child: (opt.toString().contains(
                                                          'https://') ||
                                                      opt
                                                          .toString()
                                                          .contains('http://'))
                                                  ? AspectRatio(
                                                      aspectRatio: 1,
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                                color: Colors
                                                                    .grey[300]),
                                                        foregroundDecoration: BoxDecoration(
                                                            color:
                                                                SiswamediaTheme
                                                                    .background,
                                                            image: DecorationImage(
                                                                image:
                                                                    NetworkImage(
                                                                        '$opt'))),
                                                      ))
                                                  : Text(
                                                      opt.toString(),
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 12),
                                                    ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        );
                      },
                    ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 35),
                    child: Container(
                      width: S.w,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: RaisedButton(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12)),
                              onPressed: () {
                                if (myIndex != 0) {
                                  setState(() {
                                    myIndex--;
                                    controller.text = '';
                                    controller.text =
                                        currentAnswer['${myIndex + 1}'];
                                  });
                                }
                              },
                              child: Text(
                                'back',
                                style: TextStyle(
                                    color: SiswamediaTheme.green,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                          Container(
                            height: S.w * .1,
                            padding: EdgeInsets.symmetric(
                                horizontal: S.w * .05, vertical: S.w * .01),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(S.w),
                            ),
                            child: Container(
                              height: 60,
                              child: Stack(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                        '${_timerValue ~/ 60}:${_timerValue % 60}',
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black)),
                                  ),
                                  Positioned(
                                      right: S.w * .09,
                                      top: 0,
                                      bottom: 0,
                                      child: SizedBox(
                                        height: 40,
                                        width: 40,
                                        child: Icon(Icons.timer,
                                            color: Colors.white),
                                      ))
                                ],
                              ),
                            ),
                          ),
                          myIndex == quiz.length - 1
                              ? Container(
                                  child: RaisedButton(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    onPressed: () {
                                      showDialog<void>(
                                          context: context,
                                          builder: (context) {
                                            var contentText =
                                                'Apakah anda yakin untuk menyelesaikannya?';
                                            return StatefulBuilder(
                                                builder: (context, setState) =>
                                                    AlertDialog(
                                                      title: Text('Warning!'),
                                                      content:
                                                          Text(contentText),
                                                      actions: [
                                                        FlatButton(
                                                          onPressed: () =>
                                                              Navigator.pop(
                                                                  context),
                                                          child: Text('Batal'),
                                                        ),
                                                        FlatButton(
                                                          onPressed: () async {
                                                            // await SubmitJawaban
                                                            // ignore: unawaited_futures
                                                            showDialog<void>(
                                                                context:
                                                                    context,
                                                                builder: (context) =>
                                                                    Center(
                                                                        child:
                                                                            CircularProgressIndicator()));
                                                            var jawabans = <
                                                                Map<String,
                                                                    dynamic>>[];
                                                            correction.forEach(
                                                                (key,
                                                                    dynamic
                                                                        value) {
                                                              jawabans.add(<
                                                                  String,
                                                                  dynamic>{
                                                                'quiz_bank_soal_id':
                                                                    key,
                                                                'answer_description':
                                                                    correction[
                                                                        key],
                                                                'noted':
                                                                    'ini adalah jawaban'
                                                              });
                                                            });
                                                            var submit = await PenilaianServices
                                                                .submitJawaban(
                                                                    quizId: quiz
                                                                        .first
                                                                        .quizId,
                                                                    jawaban:
                                                                        jawabans);
                                                            if (submit
                                                                    .statusCode ==
                                                                200) {
                                                              _timer.cancel();
                                                              Navigator.pop(
                                                                  context);
                                                              Navigator.pop(
                                                                  context);
                                                              Navigator.pop(
                                                                  context);
                                                              Navigator.pop(
                                                                  context);
                                                              CustomFlushBar
                                                                  .successFlushBar(
                                                                'Jawabanmu telah tersubmit',
                                                                context,
                                                              );
                                                              await prefs.then(
                                                                  (value) async {});
                                                            } else {
                                                              Navigator.pop(
                                                                  context);
                                                              Navigator.pop(
                                                                  context);
                                                              Navigator.pop(
                                                                  context);
                                                              Navigator.pop(
                                                                  context);
                                                              CustomFlushBar
                                                                  .errorFlushBar(
                                                                'Terjadi Kesalahan',
                                                                context,
                                                              );
                                                            }

                                                            /*Navigator.push(
                                              context,
                                              MaterialPageRoute<void>(
                                                  builder: (_) => ResultPage(
                                                        currentAnswer:
                                                            currentAnswer,
                                                        rightAnswer: correction,
                                                      )));*/
                                                          },
                                                          child: Text('Tentu'),
                                                        )
                                                      ],
                                                    ));
                                          });
                                    },
                                    child: Text(
                                      'Finish',
                                      style: TextStyle(
                                          color: SiswamediaTheme.green,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                )
                              : Container(
                                  child: RaisedButton(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    onPressed: () {
                                      setState(() {
                                        myIndex++;
                                        controller.text = '';
                                        controller.text =
                                            currentAnswer['${myIndex + 1}'];
                                      });
                                      print(currentAnswer['${myIndex + 1}']);
                                    },
                                    child: Text(
                                      'next',
                                      style: TextStyle(
                                          color: SiswamediaTheme.green,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class QuizKelas with ChangeNotifier {
  bool _progress;
  Map<String, dynamic> _selected = <String, dynamic>{};
  final PageController controller = PageController();

  bool get progress => _progress;
  Map<String, dynamic> get selected => _selected;

  set progress(bool newValue) {
    _progress = newValue;
    notifyListeners();
  }

  set selected(Map<String, dynamic> newValue) {
    _selected = newValue;
    notifyListeners();
  }

  void adding(String key, String value) {
    _selected[key] = value;
    notifyListeners();
  }

  void remove(String oldValue) {
    _selected.remove(oldValue);
    notifyListeners();
  }

  void nextPage() async {
    await controller.nextPage(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }
}
