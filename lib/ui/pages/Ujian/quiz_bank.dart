part of '_ujian.dart';

class BankSoal extends StatefulWidget {
  BankSoal({this.type = 'normal', this.courseId = 0});

  final String type;
  final int courseId;

  @override
  _BankSoalState createState() => _BankSoalState();
}

class _BankSoalState extends State<BankSoal> {
  int currentIndex = 0;
  int currentIndexKur = 0;
  int currentIndexPel = 0;

  List<String> kurikulum = ['2013', '2013 ktsp', 'Cambridge'];
  List<String> pelajaran = [
    'Matematika',
    'Bahasa Inggris',
    'Bahasa Indonesia',
    'Fisika',
    'Kimia',
    'Biologi',
    'Sejarah',
    'Seni Budaya',
    'Ekonomi',
    'Pancasila',
    'Penjaskes',
    'Agama'
  ];

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<QuizProviderKelas>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shadowColor: Colors.grey.withOpacity(.2),
        brightness: Brightness.light,
        leading: IconButton(
            icon: Icon(
              Icons.clear,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Bank Soal',
            style: TextStyle(
                color: Colors.black,
                fontSize: S.w / 22,
                fontWeight: FontWeight.w600)),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*ActionChip(
                  padding: EdgeInsets.symmetric(
                      horizontal: width * .025,
                      vertical: width * .025),
                  label: Text(
                    e,
                    style: TextStyle(fontSize: width / 30),
                  ),
                  labelStyle: TextStyle(
                    color: currentIndex ==
                        JenjangData.semuaKelas.indexOf(e)
                        ? Colors.white
                        : Colors.black,
                  ),
                  backgroundColor: currentIndex ==
                      JenjangData.semuaKelas.indexOf(e)
                      ? SiswamediaTheme.green
                      : Colors.white,
                  onPressed: () {
                    _selectyaaa(JenjangData.semuaKelas.indexOf(e));
                  }),*/
              Padding(
                padding: EdgeInsets.symmetric(vertical: S.w * .03),
                child: Text('Pilih Kelas',
                    style: TextStyle(
                        fontSize: S.w / 22,
                        fontWeight: FontWeight.w700,
                        color: Colors.black.withOpacity(.9))),
              ),
              Wrap(
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.start,
                runSpacing: 0.0,
                spacing: S.w / 90,
                children: JenjangData.semuaKelas
                    .map((e) => Container(
                          width: S.w * .27,
                          padding: EdgeInsets.symmetric(vertical: S.w * .02),
                          margin: EdgeInsets.symmetric(
                              horizontal: S.w * .01, vertical: S.w * .02),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(S.w),
                            border: Border.all(
                              color: currentIndex ==
                                      JenjangData.semuaKelas.indexOf(e)
                                  ? Colors.transparent
                                  : Colors.grey[400],
                            ),
                            color: currentIndex ==
                                    JenjangData.semuaKelas.indexOf(e)
                                ? SiswamediaTheme.green
                                : Colors.transparent,
                          ),
                          child: InkWell(
                            onTap: () =>
                                _selectyaaa(JenjangData.semuaKelas.indexOf(e)),
                            child: Center(
                                child: Text(e,
                                    style: TextStyle(
                                        fontSize: S.w / 32,
                                        color: currentIndex ==
                                                JenjangData.semuaKelas
                                                    .indexOf(e)
                                            ? Colors.white
                                            : Colors.black))),
                          ),
                        ))
                    .toList(),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: S.w * .03),
                child: Text('Pilih Kurikulum',
                    style: TextStyle(
                        fontSize: S.w / 22,
                        fontWeight: FontWeight.w700,
                        color: Colors.black.withOpacity(.9))),
              ),
              Wrap(
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.start,
                runSpacing: 0.0,
                spacing: S.w / 90,
                children: kurikulum
                    .map((e) => Container(
                          width: S.w * .27,
                          padding: EdgeInsets.symmetric(vertical: S.w * .02),
                          margin: EdgeInsets.symmetric(
                              horizontal: S.w * .01, vertical: S.w * .02),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(S.w),
                            border: Border.all(
                              color: currentIndexKur == kurikulum.indexOf(e)
                                  ? Colors.transparent
                                  : Colors.grey[400],
                            ),
                            color: currentIndexKur == kurikulum.indexOf(e)
                                ? SiswamediaTheme.green
                                : Colors.transparent,
                          ),
                          child: InkWell(
                            onTap: () => _selectyaaak(kurikulum.indexOf(e)),
                            child: Center(
                                child: Text(e,
                                    style: TextStyle(
                                        fontSize: S.w / 32,
                                        color: currentIndexKur ==
                                                kurikulum.indexOf(e)
                                            ? Colors.white
                                            : Colors.black))),
                          ),
                        ))
                    .toList(),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: S.w * .03),
                child: Text('Pilih Mata Pelajaran',
                    style: TextStyle(
                        fontSize: S.w / 22,
                        fontWeight: FontWeight.w700,
                        color: Colors.black.withOpacity(.9))),
              ),
              Wrap(
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.start,
                runSpacing: 0.0,
                spacing: S.w / 90,
                children: pelajaran
                    .map((e) => Container(
                          width: S.w * .27,
                          padding: EdgeInsets.symmetric(vertical: S.w * .02),
                          margin: EdgeInsets.symmetric(
                              horizontal: S.w * .01, vertical: S.w * .02),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(S.w),
                            border: Border.all(
                              color: currentIndexPel == pelajaran.indexOf(e)
                                  ? Colors.transparent
                                  : Colors.grey[400],
                            ),
                            color: currentIndexPel == pelajaran.indexOf(e)
                                ? SiswamediaTheme.green
                                : Colors.transparent,
                          ),
                          child: InkWell(
                            onTap: () => _selectyaaap(pelajaran.indexOf(e)),
                            child: Center(
                                child: Text(
                                    e.contains('Bahasa')
                                        ? 'Bhs. ${e.split('' '')[1]}'
                                        : e,
                                    style: TextStyle(
                                        fontSize: S.w / 32,
                                        color: currentIndexPel ==
                                                pelajaran.indexOf(e)
                                            ? Colors.white
                                            : Colors.black))),
                          ),
                        ))
                    .toList(),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: S.w * .05),
                child: Container(
                  width: MediaQuery.of(context).size.width * .9,
                  height: S.w * .12,
                  decoration: BoxDecoration(
                    color: SiswamediaTheme.green,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        data.setFilter(FilterBankSoal(
                          kelas: JenjangData.semuaKelas[currentIndex],
                          kurikulum: kurikulum[currentIndexKur],
                          matapelajaran: pelajaran[currentIndexPel],
                        ));
                        context.push<void>(BankResult(
                            courseId: widget.courseId, type: widget.type));
                      },
                      child: Center(
                        child: Text('Tampilkan',
                            style: TextStyle(
                                fontSize: S.w / 30,
                                fontWeight: FontWeight.w700,
                                color: Colors.white)),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _selectyaaa(int i) {
    setState(() {
      currentIndex = i;
    });
    print(currentIndex);
  }

  void _selectyaaak(int i) {
    setState(() {
      currentIndexKur = i;
    });
    print(currentIndexKur);
  }

  void _selectyaaap(int i) {
    setState(() {
      currentIndexPel = i;
    });
    print(currentIndexPel);
  }
}
