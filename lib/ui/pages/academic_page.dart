part of '_pages.dart';

class ModelAcademicMenu {
  String name;
  String icon;
  ModelAcademicMenu({this.name, this.icon});
}

class ModelAcademicProtocol {
  String name;
  String icon;
  String url;
  ModelAcademicProtocol({this.name, this.icon, this.url});
}

List<ModelAcademicMenu> dataDummy = [
  // ModelAcademicMenu(icon: 'assets/academic/nilai.png', name: 'Nilai'),
  // ModelAcademicMenu(icon: 'assets/academic/grafik.png', name: 'Grafik'),
  // ModelAcademicMenu(icon: 'assets/academic/bayar.png', name: 'Bayar'),
  ModelAcademicMenu(icon: 'assets/academic/alumni.png', name: 'Pantau'),
];

List<ModelAcademicProtocol> dataProtokol = [
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/zona.png',
      name: 'Zona',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=1'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/belajar covid.png',
      name: 'Belajar Covid',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=2'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/belajar online.png',
      name: 'Belajar Online',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=3'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/belajar offline.png',
      name: 'Belajar Offline',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=4'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/belajar kombinasi.png',
      name: 'Belajar Kombinasi',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=5'),
];

List<ModelAcademicProtocol> dataPanduan = [
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/registrasi.png',
      name: 'Registrasi',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=6'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/tugas.png',
      name: 'Tugas',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=7'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/materi.png',
      name: 'Materi',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=false&sheet=8'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/kuis.png',
      name: 'Kuis',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=9'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/absen.png',
      name: 'Absen',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=10'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/pembayaran.png',
      name: 'Pembayaran',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=11'),
  ModelAcademicProtocol(
      icon: 'assets/icons/akademis/confrence.png',
      name: 'Confrence',
      url:
          'https://api.siswamedia.com/g2j/?id=1LW-GevGfL8dqzZByIp8ixeoEJtGu8OdafWTQoLGmbtE&columns=true&sheet=12'),
];

class AcademicHomePage extends StatefulWidget {
  @override
  _AcademicHomePageState createState() => _AcademicHomePageState();
}

class _AcademicHomePageState extends State<AcademicHomePage> {
  var authState = Injector.getAsReactive<AuthState>();
  final ScrollController scrollController = ScrollController();
  bool isScrollingDown = false;
  SharedPreferences prefs;
  Size size;
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    super.initState();
    _scaffoldKey = GlobalKey<ScaffoldState>();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.dark,
    ));
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return LayoutBuilder(builder: (context, constrains) {
      if (authState.state.isLogin != null && authState.state.isLogin) {
        return Scaffold(
            body: SafeArea(
          child: AcademicAlreadyLogin(),
        ));
      } else if (authState.state.isLogin != null && !authState.state.isLogin) {
        return Scaffold(
            backgroundColor: Colors.white,
            key: _scaffoldKey,
            body: SafeArea(
              child: sectionNotLogin(
                  context: context,
                  login: () async {
                    prefs = await SharedPreferences.getInstance();
                    await authState.setState((s) => s.login(context).then((value) {
                          if (prefs.getBool('is_no_school')) {
                            ActionOverlay.noSchoolOverlay(context);
                          }
                        }));
                  }),
            ));
      }
      print('loading');
      return Center(child: CircularProgressIndicator());
    });
  }
}
