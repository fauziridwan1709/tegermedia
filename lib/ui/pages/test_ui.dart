part of '_pages.dart';

class TestUi extends StatefulWidget {
  @override
  _TestUiState createState() => _TestUiState();
}

class _TestUiState extends State<TestUi> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    controller = TabController(vsync: this, length: 2);
    super.initState();
  }

  List<String> provider = [
    'Telkomsel',
    'XL',
    'Indosat',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SiswamediaTheme.background,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: SiswamediaTheme.primary,
          ),
        ),
        bottom: TabBar(controller: controller, isScrollable: true, tabs: <Tab>[
          Tab(
            child: vText('Pulsa', color: SiswamediaTheme.primary),
          ),
          Tab(
            child: vText('Paket Data', color: SiswamediaTheme.primary),
          ),
        ]),
        centerTitle: true,
        title: vText('Pulsa'),
      ),
      body: TabBarView(controller: controller, children: <Widget>[
        Container(child: _pulsa()),
        Container(child: _paketData()),
      ]),
    );
  }

  List<bool> listValue = [false, false, false];

  Widget _paketData() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemBuilder: (c, i) => _listGame(i),
              shrinkWrap: true,
              itemCount: 3,
            ),
          ),
          _countPayment()
        ],
      ),
    );
  }

  Widget _listGame(int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 26,
        ),
        _title('Game Max', paddingHorizontal: 0.0),
        SizedBox(
          height: 26,
        ),
        ListView.builder(
          itemBuilder: (c, i) => Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.only(bottom: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                vText('Internet Combo 10 GB',
                    fontSize: 14,
                    color: selected == i + 1
                        ? Colors.white
                        : SiswamediaTheme.black400,
                    fontWeight: FontWeight.w500),
                vText('2GB Kota Utama + 1.5GB YouTube + 30GB Games',
                    fontSize: 12,
                    color: selected == i + 1
                        ? SiswamediaTheme.textGrey
                        : SiswamediaTheme.black400,
                    fontWeight: FontWeight.w400),
                SizedBox(
                  height: 23,
                ),
                vText('Rp . ' + payment[i],
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: selected == i + 1
                        ? Colors.white
                        : SiswamediaTheme.primary),
              ],
            ),
          ),
          physics: NeverScrollableScrollPhysics(),
          itemCount: 3,
          shrinkWrap: true,
        )
      ],
    );
  }

  int selected = 0;

  Widget _pulsa() {
    return ListView(children: [
      SizedBox(
        height: 20,
      ),
      _title('Pilih Provider'),
      ListView.builder(
        itemCount: listValue.length,
        shrinkWrap: true,
        itemBuilder: (c, i) => CheckboxListTile(
          title: Text(provider[i]),
          value: listValue[i],
          onChanged: (newValue) {
            setState(() {
              listValue[i] = newValue;
            });
          },

          controlAffinity:
              ListTileControlAffinity.leading, //  <-- leading Checkbox
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(hintText: 'Masukan Nomor HP'),
            ),
            SizedBox(
              height: 12,
            ),
            GridView.builder(
                itemCount: title.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, childAspectRatio: 2 / 1),
                itemBuilder: (c, i) => _listPembayaran(i)),
          ],
        ),
      ),
      SizedBox(
        height: 12,
      ),
      _title('Masukan Kode Promo'),
      SizedBox(
        height: 6,
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Colors.white,
              border: Border.all(color: Colors.grey)),
          child: TextFormField(
            decoration: InputDecoration(
                border: InputBorder.none, hintText: ' Promo 123'),
          ),
        ),
      ),
      SizedBox(
        height: 6,
      ),
      if (selected != 0) _countPayment()
    ]);
  }

  Widget _countPayment() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          _title('Total pembayaran pulsa'),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: vText('Rp 15.500',
                fontWeight: FontWeight.bold,
                fontSize: 14,
                color: SiswamediaTheme.nearBlack),
          ),
          SizedBox(
            height: 10,
          ),
          _title('Total Discount'),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: vText('- Rp 2.500',
                fontWeight: FontWeight.bold,
                fontSize: 14,
                color: SiswamediaTheme.nearBlack),
          ),
          SizedBox(
            height: 28,
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _title('Total Pembayaran'),
                    SizedBox(
                      height: 4,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: vText('- Rp 2.500',
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: SiswamediaTheme.nearBlack),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: SiswamediaTheme.primary,
                    ),
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(vertical: 9),
                    child: vText('Lanjut', color: Colors.white, fontSize: 17),
                  ),
                ),
              ),
              SizedBox(
                width: 16,
              )
            ],
          ),
          SizedBox(
            height: 23,
          ),
        ],
      ),
    );
  }

  Widget _title(String title, {double paddingHorizontal = 20.0}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddingHorizontal),
      child: vText(
        title,
      ),
    );
  }

  List<String> payment = [
    '15.500',
    '20.500',
    '25.500',
    '30.500',
    '50.500',
    '75.500',
    '100.500',
    '150.500',
  ];

  List<String> title = [
    '15.000',
    '20.000',
    '25.000',
    '30.000',
    '50.000',
    '75.000',
    '100.000',
    '150.000',
  ];

  Widget _listPembayaran(int i) {
    return InkWell(
      onTap: () {
        if (selected == i + 1) {
          selected = 0;
        } else {
          selected = i + 1;
        }
        setState(() {});
      },
      child: Card(
          color: selected == i + 1 ? SiswamediaTheme.primary : Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                vText(title[i],
                    fontSize: 20,
                    color: selected == i + 1
                        ? Colors.white
                        : SiswamediaTheme.black400,
                    fontWeight: FontWeight.w400),
                vText('Rp . ' + payment[i],
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                    color: selected == i + 1
                        ? Colors.white
                        : SiswamediaTheme.primary),
              ],
            ),
          )),
    );
  }
}
