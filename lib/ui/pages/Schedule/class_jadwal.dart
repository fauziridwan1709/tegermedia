part of '_schedule.dart';

final Map<DateTime, List<String>> _holidays = {
  DateTime(2020, 1, 1): ['New Year\'s Day'],
  DateTime(2020, 8, 6): ['Crazy'],
  DateTime(2020, 8, 14): ['gila\'s Day'],
  DateTime(2020, 8, 17): ['Indonesian Independece day'],
  DateTime(2020, 8, 22): ['Easter Monday'],
};

class JadwalKelas extends StatefulWidget {
  JadwalKelas({this.classId});
  final int classId;

  @override
  _JadwalKelasState createState() => _JadwalKelasState();
}

class _JadwalKelasState extends State<JadwalKelas> with TickerProviderStateMixin {
  final authState = GlobalState.auth();
  final scheduleState = GlobalState.schedule();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var profile = Injector.getAsReactive<ProfileState>().state.profile;
  List<JadwalModel> dateTime;
  bool _flag = false;

  ScrollController sController = ScrollController();
  AnimationController _animationController;
  CalendarController _calendarController;

  String data1;
  String data2;
  String data3;
  String data4;

  double ak = 0;
  void getOffset() {
    setState(() {
      ak = sController.offset;
      print(ak);
    });
  }

  @override
  void initState() {
    super.initState();
    sController.addListener(getOffset);
    _calendarController = CalendarController();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
    _animationController.forward();
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    if (scheduleState.state.items == null) {
      Initializer(
        reactiveModel: scheduleState,
        cacheKey: 'time_jadwal',
        state: false,
        rIndicator: refreshIndicatorKey,
      ).initialize();
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
  }

  void _onCalendarCreated(DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onCalendarCreated');
  }

  double update = 0;

  bool _onStartScroll(ScrollMetrics metrics) {
    return null;
  }

  bool _onUpdateScroll(ScrollMetrics metrics) {
    if (metrics.extentBefore > metrics.extentAfter) {
      _animationController.reverse();
      return true;
    } else {
      _animationController.forward();
      return false;
      //_animationController.reverse();
    }
    // if (metrics.pixels > jadwal.offs[0]) {
    //   print(true);
    //   _calendarController.selectNextMonthScroll();
    // }
  }

  bool _onEndScroll(ScrollMetrics metrics) {
    print(metrics.extentBefore);
    return false;
  }

  Future<void> retrieveData() async {
    // await GlobalState.students()
    //     .setState((s) => s.retrieveData(widget.classId));
    var dataCourse = CourseByClassModel(param: GetCourseModel(classId: widget.classId));
    await GlobalState.course().setState((s) => s.retrieveData(data: dataCourse));
    await scheduleState.setState(
        (s) => s.fetchItems(widget.classId, authState.state.currentState.classRole.isOrtu));
  }

  @override
  Widget build(BuildContext context) {
    // var jadwal = context.watch<JadwalProvider>();
    // var auth = context.watch<UserProvider>();
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    print('building');
    return WillPopScope(
      onWillPop: () async {
        // await scheduleState.refresh();
        // await jadwal.resetItems();
        return Future.value(true);
      },
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          shadowColor: Colors.transparent,
          brightness: Brightness.light,
          title: Text(
            'Jadwal Sekolah',
            style: semiBlack.copyWith(fontSize: S.w / 22),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: SiswamediaTheme.green),
              onPressed: () {
                // Navigator.pop(context);
                // popUntil(context, '/layout');
                Navigator.popUntil(context, (route) => route.isFirst);
                scheduleState.refresh();
                // jadwal.resetItems();
              }),
          actions: [
            (authState.state.currentState.classRole.isWaliKelas ||
                    authState.state.currentState.isAdmin)
                ? IconButton(
                    icon: Icon(
                      Icons.add,
                      color: SiswamediaTheme.green,
                    ),
                    onPressed: () => showModalBottomSheet<void>(
                        context: context,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(20.0),
                                topRight: const Radius.circular(20.0))),
                        builder: (_) => Container(
                              height: S.w * .65,
                              padding:
                                  EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  DashTopBottomSheet(),
                                  Container(
                                    height: S.w * .13,
                                    width: S.w * .9,
                                    decoration: BoxDecoration(
                                        color: SiswamediaTheme.green,
                                        borderRadius: BorderRadius.circular(12)),
                                    child: InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                          //todo
                                          manual(
                                            context: context,
                                            scaffoldKey: scaffoldKey,
                                            classId: widget.classId,
                                            isPortrait: isPortrait,
                                          );
                                        },
                                        child: Center(
                                            child: Text('Buat Jadwal Baru',
                                                style: TextStyle(color: Colors.white)))),
                                  ),
                                  Container(
                                      height: S.w * .13,
                                      width: S.w * .9,
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius: BorderRadius.circular(12)),
                                      child: InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                            ScheduleAction.import(
                                                context: context,
                                                scaffoldKey: scaffoldKey,
                                                classId: widget.classId);
                                          },
                                          child: Center(
                                              child: Text('Import File',
                                                  style: TextStyle(color: Colors.white))))),
                                  Container(
                                    height: S.w * .13,
                                    width: S.w * .9,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      border: Border.all(color: Colors.black),
                                    ),
                                    child: InkWell(
                                        onTap: () => Navigator.pop(context),
                                        child: Center(child: Text('Batal'))),
                                  ),
                                ],
                              ),
                            )))
                : SizedBox()
          ],
        ),
        body: StateBuilder<ScheduleState>(
            observe: () => scheduleState,
            builder: (context, snapshot) {
              return RefreshIndicator(
                  onRefresh: retrieveData,
                  key: refreshIndicatorKey,
                  child: WhenRebuilder<ScheduleState>(
                      observe: () => scheduleState,
                      onIdle: () => WaitingView(),
                      onWaiting: () => WaitingView(),
                      onError: (dynamic error) => ErrorView(error: error),
                      onData: (data) {
                        print(data.datamap);
                        if (isPortrait) {
                          return Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              TableCalendar(
                                controller: _animationController,
                                onDaySelected: (val, data) {
                                  setState(() => startDate = val);
                                },
                                handleIconPressed: () {
                                  if (_flag) {
                                    _animationController.forward();
                                  } else {
                                    _animationController.reverse();
                                  }
                                  _flag = !_flag;
                                },
                                calendarController: _calendarController,
                                events: scheduleState.state.datamap,
                                holidays: _holidays,
                                startingDayOfWeek: StartingDayOfWeek.monday,
                                daysOfWeekStyle: DaysOfWeekStyle(
                                  weekdayStyle:
                                      TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                  weekendStyle:
                                      TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                ),
                                calendarStyle: CalendarStyle(
                                  selectedColor: Colors.white,
                                  markersColor: Colors.white,
                                  outsideStyle: TextStyle(color: Colors.white),
                                  outsideDaysVisible: false,
                                  todayStyle:
                                      TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                  weekendStyle:
                                      TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                  weekdayStyle:
                                      TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                  selectedStyle: TextStyle(
                                      color: SiswamediaTheme.green, fontWeight: FontWeight.w600),
                                  outsideHolidayStyle: TextStyle(color: SiswamediaTheme.green),
                                  markersPositionBottom: 0,
                                ),
                                headerStyle: HeaderStyle(
                                  decoration: BoxDecoration(
                                    color: SiswamediaTheme.green,
                                  ),
                                  titleTextStyle: TextStyle(
                                      fontSize: S.w / 22,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                  centerHeaderTitle: true,
                                  formatButtonVisible: false,
                                ),
                                onVisibleDaysChanged: _onVisibleDaysChanged,
                                onCalendarCreated: _onCalendarCreated,
                              ),
                              Expanded(
                                child: Container(
                                  child: NotificationListener<ScrollNotification>(
                                    onNotification: (scrollNotification) {
                                      if (scrollNotification is ScrollStartNotification) {
                                        return _onStartScroll(scrollNotification.metrics);
                                      } else if (scrollNotification is ScrollUpdateNotification) {
                                        return _onUpdateScroll(scrollNotification.metrics);
                                      } else {
                                        return _onEndScroll(scrollNotification.metrics);
                                      }
                                    },
                                    child: ListView(
                                      children: [
                                        InkWell(
                                            onTap: () {
                                              showDemoDialog(context: context);
                                            },
                                            child: Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 15, horizontal: 20),
                                              width: 130,
                                              height: 40,
                                              decoration: SiswamediaTheme.dropDownDecoration,
                                              child: Center(
                                                  child: Text(
                                                      '${startDate.day < 10 ? '0' : ''}${startDate.day} ${QuizData.bulanSmall[startDate.month - 1]} - ${endDate.day < 10 ? '0' : ''}${endDate.day} ${QuizData.bulanSmall[endDate.month - 1]}')),
                                            )),
                                        Timeline(
                                          cController: _calendarController,
                                          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                                          indicatorColor: Color(0xffE1E1E1),
                                          lineColor: Color(0xffE1E1E1),
                                          startDate: startDate,
                                          endDate: endDate,
                                          date: scheduleState.state.datamap,
                                          indicatorSize: 10,
                                          children: [],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          );
                        } else {
                          print('yow');
                          return Row(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: ListView(children: [
                                  Container(
                                    width: S.w * .8,
                                    padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                                    decoration: BoxDecoration(
                                      borderRadius: radius(8),
                                    ),
                                    child: ClipRRect(
                                      borderRadius: radius(8),
                                      child: TableCalendar(
                                        controller: _animationController,
                                        handleIconPressed: () {
                                          if (_flag) {
                                            _animationController.forward();
                                          } else {
                                            _animationController.reverse();
                                          }
                                          _flag = !_flag;
                                        },
                                        calendarController: _calendarController,
                                        events: scheduleState.state.datamap,
                                        holidays: _holidays,
                                        startingDayOfWeek: StartingDayOfWeek.monday,
                                        daysOfWeekStyle: DaysOfWeekStyle(
                                          weekdayStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: S.w / 34,
                                              fontWeight: FontWeight.w600),
                                          weekendStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: S.w / 34,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        calendarStyle: CalendarStyle(
                                          selectedColor: Colors.white,
                                          markersColor: Colors.white,
                                          outsideStyle: TextStyle(color: Colors.white),
                                          outsideDaysVisible: false,
                                          todayStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: S.w / 34,
                                              fontWeight: FontWeight.w600),
                                          weekendStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: S.w / 34,
                                              fontWeight: FontWeight.w600),
                                          weekdayStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: S.w / 34,
                                              fontWeight: FontWeight.w600),
                                          selectedStyle: TextStyle(
                                              color: SiswamediaTheme.green,
                                              fontWeight: FontWeight.w600),
                                          outsideHolidayStyle:
                                              TextStyle(color: SiswamediaTheme.green),
                                          markersPositionBottom: 0,
                                        ),
                                        headerStyle: HeaderStyle(
                                          decoration: BoxDecoration(
                                            color: Color(0xff7BA939),
                                          ),
                                          titleTextStyle: TextStyle(
                                              fontSize: S.w / 28,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.white),
                                          centerHeaderTitle: true,
                                          formatButtonVisible: false,
                                        ),
                                        onVisibleDaysChanged: _onVisibleDaysChanged,
                                        onCalendarCreated: _onCalendarCreated,
                                      ),
                                    ),
                                  )
                                ]),
                              ),
                              Expanded(
                                child: Container(
                                  child: ListView(
                                    children: [
                                      InkWell(
                                          onTap: () {
                                            showDemoDialog(context: context);
                                          },
                                          child: Container(
                                            margin:
                                                EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                                            width: 130,
                                            height: 40,
                                            decoration: SiswamediaTheme.dropDownDecoration,
                                            child: Center(
                                                child: Text(
                                                    '${startDate.day < 10 ? '0' : ''}${startDate.day} ${QuizData.bulanSmall[startDate.month - 1]} - ${endDate.day < 10 ? '0' : ''}${endDate.day} ${QuizData.bulanSmall[endDate.month - 1]}')),
                                          )),
                                      Timeline(
                                        cController: _calendarController,
                                        padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                                        indicatorColor: Color(0xffE1E1E1),
                                        lineColor: Color(0xffE1E1E1),
                                        startDate: startDate,
                                        endDate: endDate,
                                        date: scheduleState.state.datamap,
                                        indicatorSize: 10,
                                        children: [],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                      }));
            }),
      ),
    );
  }

  void manual(
      {BuildContext context, GlobalKey<ScaffoldState> scaffoldKey, int classId, bool isPortrait}) {
    var _editingController = TextEditingController();
    var _descController = TextEditingController();
    var currentIndex = 0;
    // var mapelIndex = 0;
    var iconIndex = 0;

    var _course = 0;

    var start = DateTime.now();
    var end = DateTime.now();

    final authState = GlobalState.auth();
    final scheduleState = GlobalState.schedule();
    var course = GlobalState.course()
        .state
        .courseMap
        .values
        .where((element) =>
            authState.state.currentState.classRole.isWaliKelas ? true : element.role.isGuru)
        .toList();
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        context: context,
        isScrollControlled: true,
        builder: (_) => StatefulBuilder(
              builder: (BuildContext _, void Function(void Function()) setState) => Container(
                width: S.w,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * .05,
                    right: MediaQuery.of(context).size.width * .05,
                    top: S.w * .05),
                height: MediaQuery.of(context).size.height * .9,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(20.0),
                        topRight: const Radius.circular(20.0))),
                child: Column(
                  children: [
                    DashTopBottomSheet(),
                    SizedBox(height: S.w * .05),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: S.w * .05),
                            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                              IconButton(
                                icon: Icon(
                                  Icons.clear,
                                  color: SiswamediaTheme.green,
                                ),
                                onPressed: () => Navigator.pop(context),
                              ),
                              Text('Tambah Jadwal Baru',
                                  style: descBlack.copyWith(fontSize: S.w / 26)),
                              Container(
                                  width: S.w * .2,
                                  height: S.w * .08,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: SiswamediaTheme.green,
                                      boxShadow: [SiswamediaTheme.shadowContainer]),
                                  child: InkWell(
                                    onTap: () async {
                                      if (end.difference(start) < Duration(seconds: 0)) {
                                        CustomFlushBar.errorFlushBar(
                                            'Menyetel waktu jadwal tidak valid', context);
                                      } else if (_editingController.text == '') {
                                        return CustomFlushBar.errorFlushBar(
                                            'Judul harus diisi', context);
                                      } else if (_descController.text == '') {
                                        return CustomFlushBar.errorFlushBar(
                                            'Deskripsi harus diisi', context);
                                      } else if (course.isEmpty) {
                                        return CustomFlushBar.errorFlushBar(
                                            'Belum ada mata pelajaran di kelas ini', context);
                                      } else if (_course == 0) {
                                        return CustomFlushBar.errorFlushBar(
                                            'Pilih mata pelajaran', context);
                                      } else {
                                        Navigator.pop(context);
                                        SiswamediaTheme.infoLoading(
                                            context: context, info: 'Membuat Jadwal');
                                        var item = CreateScheduleModel(
                                          name: _editingController.text,
                                          courseId: _course,
                                          iconData: iconIndex,
                                          category: currentIndex,
                                          description: '${_descController.text}',
                                          schoolId: authState.state.currentState.schoolId,
                                          classId: classId,
                                          startDate: start.toUtc().toIso8601String(),
                                          endDate: end.toUtc().toIso8601String(),
                                        );
                                        await SiswamediaGoogleSignIn.requestCalendar()
                                            .then((isSuccess) async {
                                          if (isSuccess) {
                                            await scheduleState.state
                                                .createEvents(item, profile.email, context)
                                                .then((value) async {
                                              if (value.statusCode == StatusCodeConst.success) {
                                                await fetchItem(classId);
                                                CustomFlushBar.successFlushBar(
                                                    'Berhasil membuat jadwal', context);
                                              } else {
                                                if (value.data) {
                                                  var resultDialog = await showDialog<bool>(
                                                      context: context,
                                                      builder: (_) {
                                                        return QuizDialog(
                                                          image:
                                                              'assets/images/png/calendar_confirm.png',
                                                          title: 'Jadwal konflik',
                                                          actions: ['Ya', 'Tidak'],
                                                          type: DialogType.textImage,
                                                          onTapFirstAction: () async {
                                                            Navigator.pop(context, true);
                                                          },
                                                          onTapSecondAction: () {
                                                            Navigator.pop(context, false);
                                                          },
                                                          description:
                                                              'Terdeteksi ada jadwal yang konflik, apakah anda yakin untuk melanjutkan?',
                                                        );
                                                      });
                                                  print('nagasaka');
                                                  print(resultDialog);
                                                  if (resultDialog) {
                                                    print('mantapa');
                                                    await scheduleState.state
                                                        .createEvents(item, profile.email, context,
                                                            overlap: true)
                                                        .then((value) async {
                                                      if (value.statusCode ==
                                                          StatusCodeConst.success) {
                                                        await fetchItem(classId);
                                                        CustomFlushBar.successFlushBar(
                                                            'Berhasil membuat jadwal', context);
                                                      } else {
                                                        Navigator.pop(context);
                                                        CustomFlushBar.errorFlushBar(
                                                            value.message, context);
                                                      }
                                                    });
                                                  } else {
                                                    Navigator.pop(context);
                                                    CustomFlushBar.errorFlushBar(
                                                        value.message, context);
                                                  }
                                                } else {
                                                  Navigator.pop(context);
                                                  CustomFlushBar.errorFlushBar(
                                                      value.message, context);
                                                }
                                              }
                                            });
                                          } else {
                                            Navigator.pop(context);
                                            CustomFlushBar.errorFlushBar(
                                                'Tidak diizinkan', context);
                                          }
                                        });
                                      }
                                    },
                                    child: Center(
                                      child: Text('simpan',
                                          style:
                                              TextStyle(color: Colors.white, fontSize: S.w / 28)),
                                    ),
                                  ))
                            ]),
                            Container(
                                height: S.w * .13,
                                width: MediaQuery.of(context).size.width * .9,
                                child: TextField(
                                  controller: _editingController,
                                  decoration: InputDecoration(
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: SiswamediaTheme.green)),
                                    hintText: 'Judul',
                                  ),
                                )),
                            Container(
                                height: S.w * .13,
                                width: MediaQuery.of(context).size.width * .9,
                                child: TextField(
                                  controller: _descController,
                                  decoration: InputDecoration(
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: SiswamediaTheme.green)),
                                    hintText: 'Deskripsi Singkat (Boleh diisi nama guru)',
                                  ),
                                )),
                            DateTimePicker(
                              type: DateTimePickerType.dateTime,
                              dateMask: 'MMM dd, yyyy - HH:mm ',
                              firstDate: DateTime.now(),
                              lastDate: DateTime(2100),
                              icon: Icon(Icons.calendar_today_outlined),
                              dateLabelText: 'Tanggal mulai',
                              onChanged: (val) => setState(() => start = DateTime.parse(val)),
                            ),
                            DateTimePicker(
                              type: DateTimePickerType.dateTime,
                              dateMask: 'MMM dd, yyyy - HH:mm ',
                              firstDate: DateTime.now(),
                              lastDate: DateTime(2100),
                              icon: Icon(Icons.calendar_today_outlined),
                              dateLabelText: 'Tanggal berakhir',
                              onChanged: (val) => setState(() => end = DateTime.parse(val)),
                            ),
                            SizedBox(height: 15),
                            Text('Pilih Kategori', style: semiBlack.copyWith(fontSize: S.w / 26)),
                            Wrap(
                              alignment: WrapAlignment.start,
                              runAlignment: WrapAlignment.start,
                              runSpacing: 0.0,
                              spacing: S.w / 72,
                              children: kategori
                                  .map((e) => ActionChip(
                                      label: Text(
                                        e,
                                        style: TextStyle(fontSize: S.w / 30),
                                      ),
                                      labelStyle: TextStyle(
                                        color: currentIndex == kategori.indexOf(e)
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      backgroundColor: currentIndex == kategori.indexOf(e)
                                          ? SiswamediaTheme.green
                                          : Colors.grey.shade200,
                                      onPressed: () {
                                        setState(() => currentIndex = kategori.indexOf(e));
                                      }))
                                  .toList(),
                            ),
                            Divider(color: Colors.grey),
                            Text('Pilih Mata Pelajaran',
                                style: semiBlack.copyWith(fontSize: S.w / 26)),
                            if (course.isNotEmpty && currentIndex != 3)
                              SizedBox(
                                width: S.w * .7,
                                child: DropdownButtonFormField<int>(
                                  isExpanded: true,
                                  value: _course,
                                  icon:
                                      Icon(Icons.keyboard_arrow_down, color: SiswamediaTheme.green),
                                  iconSize: 24,
                                  elevation: 16,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 0.0, horizontal: 25),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                      borderRadius: BorderRadius.circular(S.w),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: SiswamediaTheme.green),
                                      borderRadius: BorderRadius.circular(S.w),
                                    ),
                                  ),
                                  style: descBlack.copyWith(
                                      fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                                  onChanged: (int newValue) {
                                    setState(() {
                                      _course = newValue;
                                    });
                                  },
                                  items: [
                                    DropdownMenuItem(
                                      value: 0,
                                      child: Text('Pilih Pelajaran'),
                                    ),
                                    for (CourseByClassData value in course)
                                      DropdownMenuItem(
                                        value: value.id,
                                        child: Text('${value.name}'),
                                      )
                                  ],
                                ),
                              ),
                            if (course.isEmpty)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  const Text(
                                      'Kamu Belum terdaftar di mata pelajaran manapun daftar dulu',
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'OpenSans')),
                                  SimpleText(
                                    'Mata pelajaran belum ada',
                                    fontSize: 12,
                                    color: SiswamediaTheme.red,
                                    style: SiswamediaTextStyle.description,
                                  )
                                ],
                              ),
                            SizedBox(height: 10),
                            Text('Pilih Icon', style: semiBlack.copyWith(fontSize: S.w / 26)),
                            Container(
                              child: GridView.count(
                                physics: ScrollPhysics(),
                                crossAxisCount:
                                    gridCountIcon(MediaQuery.of(context).size.aspectRatio),
                                shrinkWrap: true,
                                childAspectRatio: 1,
                                scrollDirection: Axis.vertical,
                                children: iconMateri
                                    .map((e) => Container(
                                        height: S.w * .1,
                                        width: S.w * .1,
                                        margin: EdgeInsets.all(S.w * .015),
                                        decoration: BoxDecoration(
                                          color: iconIndex == iconMateri.indexOf(e)
                                              ? SiswamediaTheme.green
                                              : Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                                offset: Offset(0, 0),
                                                spreadRadius: 2,
                                                blurRadius: 2,
                                                color: Colors.grey.withOpacity(.2))
                                          ],
                                          borderRadius: BorderRadius.circular(12),
                                        ),
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              print(iconIndex);
                                              iconIndex = iconMateri.indexOf(e);
                                            });
                                          },
                                          child: Padding(
                                            padding: EdgeInsets.all(S.w * .02),
                                            child: Image.asset(
                                              'assets/icons/materi/$e.png',
                                              height: S.w * .06,
                                              width: S.w * .06,
                                            ),
                                          ),
                                        )))
                                    .toList(),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now().add(const Duration(days: 2));

  void showDemoDialog({BuildContext context}) {
    showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) => CalendarPopupView(
        barrierDismissible: true,
        minimumDate: DateTime.now().subtract(Duration(days: 1000)),
        //  maximumDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day + 10),
        initialEndDate: DateTime.parse(endDate.toString().substring(0, 10)),
        initialStartDate: DateTime.parse(startDate.toString().substring(0, 10)),
        onApplyClick: (DateTime startData, DateTime endData) {
          setState(() {
            if (startData != null && endData != null) {
              startDate = startData;
              endDate = endData;
            }
          });
        },
        onCancelClick: () {},
      ),
    );
  }

  // void callback({bool isPortrait, BuildContext context}) {
  //
  // }

  List<String> dummy = ['Pelajaran', 'ujian', 'eskul'];

  Future<void> fetchItem(int classId) async {
    await scheduleState.state
        .fetchItems(classId, authState.state.currentState.classRole.isOrtu)
        .then((value) => Navigator.pop(context));
    scheduleState.notify();
  }
  // 'Acara Sekolah'];
}

/*void _onDaySelected(DateTime day, List<JadwalModel> events) {
    print('CALLBACK: $events');
    setState(() {
      _selectedEvents = events;
      _dynamic = day;
    });
    print(_dynamic);
  }

   */
