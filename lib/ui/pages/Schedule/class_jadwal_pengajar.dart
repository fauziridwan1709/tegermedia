part of '_schedule.dart';

class ClassJadwalPengajar extends StatelessWidget {
  final authState = GlobalState.auth();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shadowColor: Colors.grey.shade50,
        brightness: Brightness.light,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'Jadwal Kelas',
          style: semiBlack.copyWith(fontSize: S.w / 22),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: S.w * .05),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/kelas/Jadwal.png',
                height: S.w / 2.2,
                width: S.w / 2.2,
                fit: BoxFit.contain,
              ),
              Text(
                'Disini Anda dapat menemukan keperluan untuk jadwal berikut adalah list kelas anda mengajar, pilih untuk melanjutkan',
                textAlign: TextAlign.center,
                style: descBlack.copyWith(
                    fontSize: S.w / 30, color: Color(0xff101010)),
              ),
              SizedBox(height: 15),
              Divider(color: Colors.grey.shade300),
              SizedBox(height: 15),
              //todo list class
              // GlobalState.a
              //         .where((element) =>
              //             element.schoolId ==
              //             authState.state.currentState.schoolId)
              //         .isNotEmpty
              //     ? documentSchoolList(
              //         context,
              //         listClass: list.listClass,
              //         width: S.w,
              //       )
              //     : documentSchoolEmpty()
            ],
          ),
        ),
      ),
    );
  }

  Widget documentSchoolList(BuildContext context,
      {List<ClassMeData> listClass, double width}) {
    return Column(
        children: listClass
            .where((element) =>
                element.schoolId == authState.state.currentState.schoolId)
            .map((e) => Container(
                  width: width * .9,
                  height: width * .12,
                  margin: EdgeInsets.symmetric(
                      horizontal: width * .03, vertical: width * .02),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 0),
                          spreadRadius: 2,
                          blurRadius: 2,
                          color: Colors.grey.withOpacity(.2))
                    ],
                    color: Colors.white,
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        //auth.choseClass(e);
                        Navigator.push(
                            context,
                            MaterialPageRoute<void>(
                                builder: (_) => JadwalKelas(classId: e.id)));
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * .05),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              Icon(
                                Icons.date_range,
                                color: SiswamediaTheme.green,
                              ),
                              SizedBox(width: 15),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: e.name,
                                        style: TextStyle(
                                            fontSize: width / 28,
                                            color: Colors.grey[500])),
                                    TextSpan(
                                        text: ' ${e.jurusan}',
                                        style: TextStyle(
                                            fontSize: width / 28,
                                            fontWeight: FontWeight.w700,
                                            color: SiswamediaTheme.green)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ))
            .toList());
  }

  Widget documentSchoolEmpty() {
    return Center(child: Text('Anda belum memiliki kelas di sekolah ini'));
  }
}
