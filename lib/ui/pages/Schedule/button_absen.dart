part of '_schedule.dart';

class ButtonAbsen extends StatelessWidget {
  const ButtonAbsen({@required this.time, @required this.role});
  final ScheduleItem time;
  final String role;

  @override
  Widget build(BuildContext context) {
    var absensiState = Injector.getAsReactive<AbsensiState>();
    final authState = GlobalState.auth();
    final scheduleState = GlobalState.schedule();
    var start = DateTime.parse(time.startDate).toLocal();
    var end = DateTime.parse(time.endDate).toLocal();
    print('role');
    print(role);
    if (role == 'SISWA') {
      return Container(
        height: S.w * .1,
        width: S.w * .1,
        decoration: BoxDecoration(
          color: (start.isAfter(DateTime.now()) || end.isBefore(DateTime.now()))
              ? Color(0xffafafaf)
              : time.checkedInId == 0
                  ? Color(0xff62D07B)
                  : SiswamediaTheme.green,
          borderRadius: radius(6),
        ),
        child: InkWell(
          onTap: () {
            if (start.isAfter(DateTime.now())) {
              return CustomFlushBar.errorFlushBar(
                'Jadwal kamu belum mulai silahkan klik sesuai waktu jadwal yang disediakan',
                context,
              );
            }
            if (end.isBefore(DateTime.now())) {
              return CustomFlushBar.errorFlushBar(
                'Maaf jadwal kamu sudah lewat, tidak bisa absen',
                context,
              );
            }
            if (time.checkedInId == 0) {
              showDialog<void>(
                  context: context,
                  builder: (_) => Dialog(
                        shape: RoundedRectangleBorder(borderRadius: radius(8)),
                        child: Container(
                          width: S.w * .8,
                          height: S.w * .7,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Selamat Datang',
                                style: semiBlack.copyWith(fontSize: S.w / 26),
                              ),
                              Text(
                                'Silahkan tekan button check in dibawah ini untuk memulai kelas',
                                style: descBlack.copyWith(fontSize: S.w / 32),
                                textAlign: TextAlign.center,
                              ),
                              Container(
                                margin: EdgeInsets.all(4),
                                height: S.w * .3,
                                width: S.w * .3,
                                decoration: BoxDecoration(
                                  color: time.checkedInId == 0
                                      ? Color(0xff62D07B)
                                      : time.checkedOut
                                          ? SiswamediaTheme.green
                                          : Color(0xffD51717),
                                  borderRadius: radius(S.w * .03),
                                ),
                                child: InkWell(
                                  onTap: () async {
                                    await CreateAbsensi.create(
                                        model: CreateAbsensiModel(
                                      checkInTime: DateTime.now().toUtc().toIso8601String(),
                                      classId: time.classId,
                                      courseId: time.courseId,
                                      scheduleId: time.iD,
                                    )).then((value) async {
                                      if (value.statusCode == 200) {
                                        Navigator.pop(context);
                                        await CreateAbsensi.update(model: <String, dynamic>{
                                          'absen_id': value.id,
                                          'update_time': true,
                                          'hours': DateTime.now().hour,
                                          'minute': DateTime.now().minute,
                                          'status': 1
                                        });
                                        await scheduleState.state.fetchItems(time.classId,
                                            authState.state.currentState.classRole.isOrtu);
                                        await absensiState
                                            .setState((s) => s.getSummary(time.classId));
                                        CustomFlushBar.successFlushBar(
                                          'Berhasil Absen',
                                          context,
                                        );
                                        await scheduleState.notify();
                                      } else {
                                        Navigator.pop(context);
                                        CustomFlushBar.errorFlushBar(
                                          'Gagal Absen',
                                          context,
                                        );
                                      }
                                    });
                                  },
                                  child: Center(
                                    child: Icon(
                                      Icons.exit_to_app,
                                      size: S.w * .2,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: S.w * .05),
                                height: S.w * .1,
                                decoration: BoxDecoration(
                                    borderRadius: radius(8), color: SiswamediaTheme.green),
                                child: Center(
                                    child: Text(
                                  'Kembali',
                                  style: descWhite.copyWith(fontSize: S.w / 32),
                                )),
                              )
                            ],
                          ),
                        ),
                      ));
            }
          },
          child: Center(
            child: Icon(
              time.checkedInId == 0 ? Icons.exit_to_app : Icons.check,
              color: Colors.white,
            ),
          ),
        ),
      );
    } else if (role == 'GURU') {
      return Container(
        height: S.w * .1,
        width: S.w * .1,
        decoration: BoxDecoration(
          color: (start.isAfter(DateTime.now()) || end.isBefore(DateTime.now()))
              ? Color(0xffAFAFAF)
              : time.checkedInId == 0
                  ? Color(0xff62D07B)
                  : time.checkedOut
                      ? SiswamediaTheme.green
                      : Color(0xffD51717),
          borderRadius: radius(6),
        ),
        child: InkWell(
          onTap: () {
            Utils.log(time.checkedInId, info: 'time check id');
            if (start.isAfter(DateTime.now())) {
              return CustomFlushBar.errorFlushBar(
                'Jadwal kamu belum mulai silahkan klik sesuai waktu jadwal yang disediakan',
                context,
              );
            }
            if (end.isBefore(DateTime.now())) {
              return CustomFlushBar.errorFlushBar(
                'Maaf jadwal kamu sudah lewat, tidak bisa absen',
                context,
              );
            }
            if (!time.checkedOut) {
              showDialog<void>(
                  context: context,
                  builder: (_) => Dialog(
                        shape: RoundedRectangleBorder(borderRadius: radius(8)),
                        child: Container(
                          width: S.w * .8,
                          height: S.w * .7,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Selamat Datang',
                                style: semiBlack.copyWith(fontSize: S.w / 26),
                              ),
                              Text(
                                'Silahkan tekan button check ini dibawah ini untuk memulai kelas',
                                style: descBlack.copyWith(fontSize: S.w / 32),
                                textAlign: TextAlign.center,
                              ),
                              Container(
                                margin: EdgeInsets.all(4),
                                height: S.w * .3,
                                width: S.w * .3,
                                decoration: BoxDecoration(
                                  color: time.checkedInId == 0
                                      ? Color(0xff62D07B)
                                      : time.checkedOut
                                          ? SiswamediaTheme.green
                                          : Color(0xffD51717),
                                  borderRadius: radius(S.w * .03),
                                ),
                                child: InkWell(
                                  onTap: () async {
                                    if (time.checkedInId == 0) {
                                      var result = await CreateAbsensi.create(
                                          model: CreateAbsensiModel(
                                        checkInTime: DateTime.now().toUtc().toIso8601String(),
                                        classId: time.classId,
                                        courseId: time.courseId,
                                        scheduleId: time.iD,
                                      ));
                                      if (result.statusCode == 200) {
                                        Navigator.pop(context);
                                        // await scheduleState.setState(
                                        //     (s) => s.fetchItems(
                                        //         time.classId,
                                        //         authState.state.currentState
                                        //             .classRole.isOrtu),
                                        //     silent: true,
                                        //     onData: (context, data) =>
                                        //         CustomFlushBar.successFlushBar(
                                        //           'Berhasil Absen',
                                        //           context,
                                        //         ));
                                        await scheduleState.state.fetchItems(time.classId,
                                            authState.state.currentState.classRole.isOrtu);
                                        await scheduleState.notify();
                                        CustomFlushBar.successFlushBar('Berhasil Absen', context);
                                      } else {
                                        Navigator.pop(context);
                                        CustomFlushBar.errorFlushBar(
                                          'Gagal Absen Masuk',
                                          context,
                                        );
                                      }
                                    } else if (!time.checkedOut) {
                                      var result = await CheckoutAbsensi.create(
                                          model: CheckoutModel(
                                              checkInId: time.checkedInId,
                                              checkOutTime:
                                                  DateTime.now().toUtc().toIso8601String()));
                                      if (result.statusCode == StatusCodeConst.success ||
                                          result.statusCode == StatusCodeConst.created) {
                                        Navigator.pop(context);
                                        await scheduleState.state.fetchItems(time.classId,
                                            authState.state.currentState.classRole.isOrtu);
                                        await scheduleState.notify();
                                        CustomFlushBar.successFlushBar(
                                          'Berhasil Checkout Absen',
                                          context,
                                        );
                                      } else {
                                        Navigator.pop(context);
                                        CustomFlushBar.errorFlushBar(
                                          'Gagal Checkout Absen',
                                          context,
                                        );
                                      }
                                    }
                                  },
                                  child: Center(
                                    child: Icon(
                                      !time.checkedOut ? Icons.exit_to_app : Icons.check,
                                      size: S.w * .2,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: S.w * .05),
                                height: S.w * .1,
                                decoration: BoxDecoration(
                                    borderRadius: radius(8), color: SiswamediaTheme.green),
                                child: Center(
                                    child: Text(
                                  'Kembali',
                                  style: descWhite.copyWith(fontSize: S.w / 32),
                                )),
                              )
                            ],
                          ),
                        ),
                      ));
            }
          },
          child: Center(
            child: Icon(
              !time.checkedOut ? Icons.exit_to_app : Icons.check,
              color: Colors.white,
            ),
          ),
        ),
      );
    }
    return Container();
  }
}
