part of '_schedule.dart';

class ScheduleAction {
  static final authState = GlobalState.auth();
  static final scheduleState = GlobalState.schedule();
  static var profile = Injector.getAsReactive<ProfileState>().state.profile;

  static void manual(
      {BuildContext context, GlobalKey<ScaffoldState> scaffoldKey, int classId, bool isPortrait}) {
    var _editingController = TextEditingController();
    var _descController = TextEditingController();
    var currentIndex = 0;
    var mapelIndex = 0;
    var iconIndex = 0;

    var start = DateTime.now();
    var end = DateTime.now();
    var _course = 0;
    var course = GlobalState.course().state.courseMap;
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        context: context,
        isScrollControlled: true,
        builder: (_) => StatefulBuilder(
              builder: (BuildContext _, void Function(void Function()) setState) => Container(
                width: S.w,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * .05,
                    right: MediaQuery.of(context).size.width * .05,
                    top: S.w * .05),
                height: MediaQuery.of(context).size.height * .9,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(20.0),
                        topRight: const Radius.circular(20.0))),
                child: Column(
                  children: [
                    DashTopBottomSheet(),
                    SizedBox(height: S.w * .05),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: S.w * .05),
                            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                              IconButton(
                                icon: Icon(
                                  Icons.clear,
                                  color: SiswamediaTheme.green,
                                ),
                                onPressed: () => context.pop(),
                              ),
                              Container(
                                  width: S.w * .2,
                                  height: S.w * .08,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: SiswamediaTheme.green,
                                      boxShadow: [SiswamediaTheme.shadowContainer]),
                                  child: InkWell(
                                    onTap: () async {
                                      if (end.difference(start) < Duration(seconds: 0)) {
                                        CustomFlushBar.errorFlushBar(
                                          'Menyetel waktu jadwal tidak valid',
                                          context,
                                        );
                                      } else if (_editingController.text == '') {
                                        return CustomFlushBar.errorFlushBar(
                                          'Judul harus diisi',
                                          context,
                                        );
                                      } else if (_course == 0) {
                                        return CustomFlushBar.errorFlushBar(
                                          'Pilih Mata Pelajaran',
                                          context,
                                        );
                                      } else if (_descController.text == '') {
                                        return CustomFlushBar.errorFlushBar(
                                          'Deskripsi harus diisi',
                                          context,
                                        );
                                      } else if (course.isEmpty) {
                                        return CustomFlushBar.errorFlushBar(
                                          'Belum ada mata pelajaran di kelas ini',
                                          context,
                                        );
                                      } else {
                                        Navigator.pop(context);
                                        SiswamediaTheme.infoLoading(
                                            context: context, info: 'Membuat Jadwal');
                                        var item = CreateScheduleModel(
                                          name: _editingController.text,
                                          courseId: course.values.toList()[mapelIndex].id,
                                          iconData: iconIndex,
                                          category: currentIndex,
                                          description: '${_descController.text}',
                                          schoolId: authState.state.currentState.schoolId,
                                          classId: classId,
                                          startDate: start.toUtc().toIso8601String(),
                                          endDate: end.toUtc().toIso8601String(),
                                        );
                                        await scheduleState.state
                                            .createEvents(item, profile.email, context)
                                            .then((value) async {
                                          if (value.statusCode == StatusCodeConst.success) {
                                            await scheduleState.setState((s) => s
                                                    .fetchItems(
                                                        classId,
                                                        authState.state.currentState.classRole ==
                                                            'ORTU')
                                                    .then((_) {
                                                  Navigator.pop(context);

                                                  CustomFlushBar.successFlushBar(
                                                      value.message, context);
                                                }));
                                          } else {
                                            Navigator.pop(context);
                                            CustomFlushBar.errorFlushBar(value.message, context);
                                          }
                                        });
                                      }
                                    },
                                    child: Center(
                                      child: Text('simpan',
                                          style:
                                              TextStyle(color: Colors.white, fontSize: S.w / 28)),
                                    ),
                                  ))
                            ]),
                            Container(
                                height: S.w * .13,
                                width: MediaQuery.of(context).size.width * .9,
                                child: TextField(
                                  controller: _editingController,
                                  decoration: InputDecoration(
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: SiswamediaTheme.green)),
                                    hintText: 'Judul',
                                  ),
                                )),
                            Container(
                                height: S.w * .13,
                                width: MediaQuery.of(context).size.width * .9,
                                child: TextField(
                                  controller: _descController,
                                  decoration: InputDecoration(
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: SiswamediaTheme.green)),
                                    hintText: 'Deskripsi Singkat (Boleh diisi nama guru)',
                                  ),
                                )),
                            DateTimePicker(
                              type: DateTimePickerType.dateTime,
                              dateMask: 'MMM dd, yyyy - HH:mm ',
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2100),
                              icon: Icon(Icons.calendar_today_outlined),
                              dateLabelText: 'Tanggal mulai',
                              onChanged: (val) => setState(() => start = DateTime.parse(val)),
                            ),
                            DateTimePicker(
                              type: DateTimePickerType.dateTime,
                              dateMask: 'MMM dd, yyyy - HH:mm ',
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2100),
                              icon: Icon(Icons.calendar_today_outlined),
                              dateLabelText: 'Tanggal berakhir',
                              onChanged: (val) => setState(() => end = DateTime.parse(val)),
                            ),
                            SizedBox(height: 15),
                            Text('Pilih Kategori', style: semiBlack.copyWith(fontSize: S.w / 26)),
                            Wrap(
                              alignment: WrapAlignment.start,
                              runAlignment: WrapAlignment.start,
                              runSpacing: 0.0,
                              spacing: S.w / 72,
                              children: kategori
                                  .map((e) => ActionChip(
                                      label: Text(
                                        e,
                                        style: TextStyle(fontSize: S.w / 30),
                                      ),
                                      labelStyle: TextStyle(
                                        color: currentIndex == kategori.indexOf(e)
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      backgroundColor: currentIndex == kategori.indexOf(e)
                                          ? SiswamediaTheme.green
                                          : Colors.grey.shade200,
                                      onPressed: () {
                                        setState(() => currentIndex = kategori.indexOf(e));
                                      }))
                                  .toList(),
                            ),
                            Divider(color: Colors.grey),
                            Text('Pilih Mata Pelajaran',
                                style: semiBlack.copyWith(fontSize: S.w / 26)),
                            if (course.isNotEmpty)
                              SizedBox(
                                width: S.w * .6,
                                child: DropdownButtonFormField<int>(
                                  isExpanded: true,
                                  value: _course,
                                  icon:
                                      Icon(Icons.keyboard_arrow_down, color: SiswamediaTheme.green),
                                  iconSize: 24,
                                  elevation: 16,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 0.0, horizontal: 25),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                      borderRadius: BorderRadius.circular(S.w),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: SiswamediaTheme.green),
                                      borderRadius: BorderRadius.circular(S.w),
                                    ),
                                  ),
                                  style: descBlack.copyWith(
                                      fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                                  onChanged: (int newValue) {
                                    setState(() {
                                      _course = newValue;
                                    });
                                  },
                                  items: [
                                    DropdownMenuItem(
                                      child: Text('Pilih Course'),
                                      value: 0,
                                    ),
                                    for (CourseByClassData value in course.values.toList())
                                      DropdownMenuItem(
                                        value: value.id,
                                        child: Text('${value.name}'),
                                      )
                                  ],
                                ),
                              ),
                            if (course.isEmpty)
                              Text('Kamu Belum terdaftar di mata pelajaran manapun daftar dulu',
                                  style: descBlack.copyWith(fontSize: S.w / 32)),
                            Divider(color: Colors.grey),
                            Text('Pilih Icon', style: semiBlack.copyWith(fontSize: S.w / 26)),
                            Container(
                              child: GridView.count(
                                physics: ScrollPhysics(),
                                crossAxisCount:
                                    gridCountIcon(MediaQuery.of(context).size.aspectRatio),
                                shrinkWrap: true,
                                childAspectRatio: 1,
                                scrollDirection: Axis.vertical,
                                children: iconMateri
                                    .map((e) => Container(
                                        height: S.w * .1,
                                        width: S.w * .1,
                                        margin: EdgeInsets.all(S.w * .015),
                                        decoration: BoxDecoration(
                                          color: iconIndex == iconMateri.indexOf(e)
                                              ? SiswamediaTheme.green
                                              : Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                                offset: Offset(0, 0),
                                                spreadRadius: 2,
                                                blurRadius: 2,
                                                color: Colors.grey.withOpacity(.2))
                                          ],
                                          borderRadius: BorderRadius.circular(12),
                                        ),
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              print(iconIndex);
                                              iconIndex = iconMateri.indexOf(e);
                                            });
                                          },
                                          child: Padding(
                                            padding: EdgeInsets.all(S.w * .02),
                                            child: Image.asset(
                                              'assets/icons/materi/$e.png',
                                              height: S.w * .06,
                                              width: S.w * .06,
                                            ),
                                          ),
                                        )))
                                    .toList(),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  static void import({BuildContext context, GlobalKey<ScaffoldState> scaffoldKey, int classId}) {
    var currentIndex = 0;
    var mapelIndex = 0;
    var _course = 0;
    var course = GlobalState.course().state.courseMap;
    showModalBottomSheet<void>(
        context: context,
        builder: (_) =>
            StatefulBuilder(builder: (BuildContext _, void Function(void Function()) setState) {
              var message = '';
              var progress = 0;
              var total = 0;

              return WillPopScope(
                onWillPop: () async {
                  await scheduleState.setState((s) => s.setName('Pilih File'));
                  return true;
                },
                child: Container(
                  color: Color(0xFF737373),
                  child: Container(
                    height: S.w * 1.3,
                    padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(20.0),
                            topRight: const Radius.circular(20.0))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          IconButton(
                            icon: Icon(
                              Icons.clear,
                              color: SiswamediaTheme.green,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                              scheduleState.setState((s) => s.setName('Pilih File'));
                            },
                          ),
                          Text('Tambah Jadwal Baru'),
                          Container(
                              width: S.w * .2,
                              height: S.w * .07,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(7))),
                        ]),
                        SizedBox(height: 20),
                        CustomContainer(
                            color: SiswamediaTheme.nearBlack,
                            onTap: () {
                              final Uri url = Uri.parse(
                                  'https://drive.google.com/uc?export=download&id=1BFbWSv78dP1KBI7YWpLILSfD6AfYmat1OArJDGymJRw'
                                      .trimLeft());
                              print("log.d => " + url.toString());
                              return launch(
                                  'https://drive.google.com/uc?export=download&id=1BFbWSv78dP1KBI7YWpLILSfD6AfYmat1OArJDGymJRw');
                            },
                            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                            radius: radius(6),
                            child: Text(
                              'Download Template',
                              style: semiWhite.copyWith(fontSize: S.w / 30),
                            )),
                        SizedBox(height: 20),
                        InkWell(
                          onTap: () => getFile(
                            context,
                          ).then((value) {
                            setState(() {});
                          }),
                          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                            SizedBox(
                                height: S.w * .1,
                                width: S.w * .1,
                                child: Image.asset('assets/jadwal_upload.png')),
                            SizedBox(width: S.w * .02),
                            Text(scheduleState.state.fileName,
                                style: TextStyle(color: SiswamediaTheme.green)),
                          ]),
                        ),
                        // Divider(color: Colors.grey),
                        // Text('Pilih Tahun'),
                        // Wrap(
                        //   alignment: WrapAlignment.start,
                        //   runAlignment: WrapAlignment.start,
                        //   runSpacing: 0.0,
                        //   spacing: S.w / 72,
                        //   children: tahun
                        //       .map((e) => ActionChip(
                        //           label: Text(
                        //             e,
                        //             style: TextStyle(fontSize: S.w / 30),
                        //           ),
                        //           labelStyle: TextStyle(
                        //             color: currentIndex == kategori.indexOf(e)
                        //                 ? Colors.white
                        //                 : Colors.black,
                        //           ),
                        //           backgroundColor:
                        //               currentIndex == kategori.indexOf(e)
                        //                   ? SiswamediaTheme.green
                        //                   : Colors.grey.shade200,
                        //           onPressed: () {
                        //             setState(() =>
                        //                 currentIndex = kategori.indexOf(e));
                        //           }))
                        //       .toList(),
                        // ),
                        Divider(color: Colors.grey),
                        Text('Pilih Mata Pelajaran', style: semiBlack.copyWith(fontSize: S.w / 26)),
                        if (course.isNotEmpty)
                          SizedBox(
                            width: S.w * .6,
                            child: DropdownButtonFormField<int>(
                              isExpanded: true,
                              value: _course,
                              icon: Icon(Icons.keyboard_arrow_down, color: SiswamediaTheme.green),
                              iconSize: 24,
                              elevation: 16,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 25),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: SiswamediaTheme.green),
                                  borderRadius: BorderRadius.circular(S.w),
                                ),
                              ),
                              style:
                                  descBlack.copyWith(fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                              onChanged: (int newValue) {
                                setState(() {
                                  _course = newValue;
                                });
                              },
                              items: [
                                DropdownMenuItem(
                                  child: Text('Pilih Course'),
                                  value: 0,
                                ),
                                for (CourseByClassData value in course.values.toList())
                                  DropdownMenuItem(
                                    value: value.id,
                                    child: Text('${value.name}'),
                                  )
                              ],
                            ),
                          ),
                        if (course.isEmpty)
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const Text(
                                  'Kamu Belum terdaftar di mata pelajaran manapun daftar dulu',
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'OpenSans')),
                              SimpleText(
                                'Mata pelajaran belum ada',
                                fontSize: 12,
                                color: SiswamediaTheme.red,
                                style: SiswamediaTextStyle.description,
                              )
                            ],
                          ),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () async {
                              var progressState = Injector.getAsReactive<ProgressMessageState>();
                              if (_course != 0) {
                                try {
                                  await progressState.setState(
                                      (s) => s.updateProgress('Memvalidasi'),
                                      silent: true);
                                  var progressDialog = ProgressDialog(
                                      observer: progressState, message: message, context: context);
                                  progressDialog..infoLoading();
                                  for (var i = 1;
                                      i < scheduleState.state.dataList.length - 1;
                                      i++) {
                                    setState(() {
                                      total++;
                                    });
                                    dynamic data = scheduleState.state.dataList[i];
                                    print('here');
                                    print(data);
                                    print(i);
                                    if (DateTime.parse(data[3]).isBefore(DateTime.parse(data[2]))) {
                                      print(DateTime.parse(data[3]));
                                      print(DateTime.parse(data[2]));
                                      print('masuk sini');
                                      throw SiswamediaException(
                                          'Waktu tidak valid, waktu selesai tidak boleh lebih cepat dari waktu mulai');
                                    }
                                    if (data[4] is! int && data[4] is! double) {
                                      throw SiswamediaException('Kategori tidak valid');
                                    }
                                    if (data[5] is! int && data[5] is! double) {
                                      throw SiswamediaException('Icon data tidak valid');
                                    }
                                  }
                                  for (var i = 1;
                                      i < scheduleState.state.dataList.length - 1;
                                      i++) {
                                    dynamic data = scheduleState.state.dataList[i];
                                    await progressState.setState(
                                        (s) => s.updateProgress(
                                            'Mengimpor jadwal ${data[0]} ${i + 1}/$total'),
                                        silent: true);
                                    var item = CreateScheduleModel(
                                        name: data[0],
                                        description: '${data[1]}',
                                        courseId: _course,
                                        schoolId: authState.state.currentState.schoolId,
                                        classId: classId,
                                        startDate:
                                            DateTime.parse(data[2]).toUtc().toIso8601String(),
                                        endDate: DateTime.parse(data[3]).toUtc().toIso8601String(),
                                        category: data[4] ~/ 1,
                                        iconData: data[5] ~/ 1);
                                    await scheduleState.setState(
                                        (s) => s.createEvents(item, profile.email, context));
                                    //data.writeJadwal(
                                    //_editingController.text, start, end, currentIndex);
                                  }
                                  await scheduleState.setState((s) => s.setList());
                                  await scheduleState.setState((s) => s
                                          .fetchItems(classId,
                                              authState.state.currentState.classRole == 'ORTU')
                                          .then((value) {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        setState(() {
                                          scheduleState.setState((s) => s.setName('Pilih File'));
                                        });
                                        CustomFlushBar.successFlushBar(
                                          'Berhasil mengimpor jadwal',
                                          context,
                                        );
                                      }));
                                } on SiswamediaException catch (e) {
                                  Navigator.pop(context);
                                  await scheduleState.setState((s) => s.setList());
                                  setState(() {
                                    scheduleState.setState((s) => s.setName('Pilih File'));
                                  });
                                  CustomFlushBar.errorFlushBar(
                                    e.message,
                                    context,
                                  );
                                } catch (e) {
                                  print(e);
                                  await scheduleState.setState((s) => s.setList());
                                  setState(() {
                                    scheduleState.setState((s) => s.setName('Pilih File'));
                                  });
                                  Navigator.pop(context);
                                  CustomFlushBar.errorFlushBar(
                                    'Menyetel jadwal tidak valid',
                                    context,
                                  );
                                }
                              } else {
                                CustomFlushBar.errorFlushBar(
                                  'Pilih mata pelajaran',
                                  context,
                                );
                              }
                              print(scheduleState.state.datamap);
                            },
                            child: Container(
                                height: S.w * .13,
                                width: S.w * .9,
                                child: Center(
                                    child: Text(
                                  'Tambahkan',
                                  style: semiWhite.copyWith(fontSize: S.w / 26),
                                )),
                                decoration: BoxDecoration(
                                  color: SiswamediaTheme.green,
                                  borderRadius: BorderRadius.circular(12),
                                  // border: Border.all(color: Colors.black),
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }));
  }

  static Future<void> getFile(BuildContext context) async {
    try {
      var files = await FilePicker.platform.pickFiles(type: FileType.any, allowMultiple: false);
      var file = files.files.first;
      var bytes = File(file.path);
      var excel = Excel.decodeBytes(bytes.readAsBytesSync());
      for (var table in excel.tables.keys) {
        print(table); //sheet Name
        print(excel.tables[table].maxCols);
        print(excel.tables[table].maxRows);
        for (var row in excel.tables[table].rows) {
          print('$row');
          await scheduleState.setState((s) => s.addList(row));
        }
      }
      await scheduleState.setState((s) => s.setName(file.name));
    } catch (e) {
      print(e);
      print('Error');
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar('File yang Anda pilih bukan Excel', context);
    }
  }
}

int gridCountIcon(double ratio) {
  if (ratio > 1.9) {
    return 15;
  } else if (ratio > 1.5) {
    return 10;
  } else if (ratio > .9) {
    return 8;
  } else {
    return 5;
  }
}
