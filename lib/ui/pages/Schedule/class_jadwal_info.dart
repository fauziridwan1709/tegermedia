part of '_schedule.dart';

class JadwalInfo extends StatefulWidget {
  JadwalInfo(
      {this.id,
      this.judul,
      this.start,
      this.end,
      this.kategori,
      this.deskripsi});

  final int id;
  final int kategori;
  final String judul;
  final DateTime start;
  final DateTime end;
  final String deskripsi;

  @override
  _JadwalInfoState createState() => _JadwalInfoState();
}

class _JadwalInfoState extends State<JadwalInfo> {
  TextEditingController _editingController;
  List<String> kategori = ['Pelajaran', 'Ujian', 'Ekskul'];
  // 'Acara Sekolah'];
  List<String> index = ['1', '2', '3', '4'];

  final authState = GlobalState.auth();

  List<String> hari = [
    'Senin',
    'Selasa',
    'Rabu',
    'Kamis',
    'Jumat',
    'Sabtu',
    'Minggu'
  ];

  List<String> bulan = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  ];

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    Color green = Color(0xFF7BA939);

    // var data = Provider.of<JadwalProvider>(context);
    final scheduleState = GlobalState.schedule();
    var data = scheduleState.state;
    print(data.solo.judul);
    print(data.solo.start);
    print(data.solo.end);
    print(data.solo.kategori);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        title: Text('Detail Jadwal',
            style: semiBlack.copyWith(fontSize: S.w / 22)),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: green),
            onPressed: () => Navigator.pop(context)),
        actions: [
          // IconButton(
          //   icon: Icon(Icons.edit),
          //   color: SiswamediaTheme.green,
          //   onPressed: () {
          //     manual(
          //       context: context,
          //       scaffoldKey: scaffoldKey,
          //       classId: widget.classId,
          //       isPortrait: isPortrait,
          //     );
          //   },
          // ),
        ],
      ),
      body: Container(
          width: width,
          padding: EdgeInsets.symmetric(
              horizontal: width * .05, vertical: width * .05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${kategori[widget.kategori]} ${data.solo.judul}',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: width / 20),
                  ),
                  Divider(color: Colors.grey),
                  Text(
                    '${hari[data.solo.start.weekday - 1]}, ${data.solo.start.day} ${bulan[data.solo.start.month - 1]} ${data.solo.start.year}',
                    style: descBlack.copyWith(fontSize: width / 30),
                  ),
                  Text(
                    'Dari Pukul ${data.solo.start.hour < 10 ? '0' : ''}${data.solo.start.hour}.${data.solo.start.minute < 10 ? '0' : ''}${data.solo.start.minute} hingga ${data.solo.end.hour < 10 ? '0' : ''}${data.solo.end.hour}.${data.solo.end.minute < 10 ? '0' : ''}${data.solo.end.minute}',
                    style: descBlack.copyWith(fontSize: width / 30),
                  ),
                  Text(
                    '${widget.deskripsi}',
                    style: descBlack.copyWith(fontSize: width / 30),
                  ),
                ],
              ),
              Container(
                width: width * .9,
                height: width * .9,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/jadwal_detail.png')),
                ),
              ),
              //TODO implement hapus jadwal
              if (authState.state.currentState.classRole.isGuruOrWaliKelas)
                Container(
                  height: S.w * .12,
                  width: S.w * .9,
                  decoration: BoxDecoration(
                    boxShadow: [SiswamediaTheme.shadowContainer],
                    borderRadius: BorderRadius.circular(12),
                    color: green,
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        // data.hapusJadwal(widget.judul, widget.start, widget.end,
                        //     widget.kategori);
                        showDialog<void>(
                            context: context,
                            builder: (_) => Dialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                child: Container(
                                  padding: EdgeInsets.all(S.w * .05),
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text('Menghapus Jadwal ?',
                                            style: secondHeadingBlack.copyWith(
                                                fontSize: S.w / 28)),
                                        SizedBox(height: 15),
                                        Text(
                                            'Apakah Anda yakin untuk menghapus event ini ?',
                                            style: descBlack.copyWith(
                                                fontSize: S.w / 34),
                                            textAlign: TextAlign.center),
                                        SizedBox(height: 20),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: S.w * .01),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  height: S.w / 10,
                                                  width: S.w / 3.5,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            6),
                                                    color:
                                                        SiswamediaTheme.green,
                                                  ),
                                                  child: InkWell(
                                                    onTap: () async {
                                                      SiswamediaTheme.loading(
                                                          context);
                                                      await GlobalState
                                                              .schedule()
                                                          .setState((s) =>
                                                              s.deleteSchedule(
                                                                  context,
                                                                  widget.id));
                                                    },
                                                    child: Center(
                                                      child: Text('Ya',
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .white)),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            6),
                                                    color: SiswamediaTheme
                                                        .lightBlack,
                                                  ),
                                                  height: S.w / 10,
                                                  width: S.w / 3.5,
                                                  child: InkWell(
                                                    onTap: () => pop(context),
                                                    child: Center(
                                                      child: Text('Tidak',
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .white)),
                                                    ),
                                                  ),
                                                ),
                                              ]),
                                        ),
                                        SizedBox(height: 10),
                                      ]),
                                )));
                      },
                      child: Center(
                          child: Text('Hapus',
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
            ],
          )),
    );
  }

  void callback() {
    var currentIndex = 0;
    var width = MediaQuery.of(context).size.width;
    var start = widget.start;
    var end = widget.end;

    showModalBottomSheet<void>(
        context: context,
        builder: (context) => StatefulBuilder(
              builder: (BuildContext context,
                      void Function(void Function()) setState) =>
                  Container(
                color: Color(0xFF737373),
                child: Container(
                  height: width * 1.3,
                  padding: EdgeInsets.symmetric(
                      horizontal: width * .05, vertical: width * .05),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(20.0),
                          topRight: const Radius.circular(20.0))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              icon: Icon(
                                Icons.clear,
                                color: SiswamediaTheme.green,
                              ),
                              onPressed: () => Navigator.pop(context),
                            ),
                            Text('Ubah jadwal ini'),
                            Container(
                                width: width * .2,
                                height: width * .07,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7),
                                    color: SiswamediaTheme.green),
                                child: InkWell(
                                  onTap: () {
                                    // data.ubahJadwal(
                                    //     JadwalModel(
                                    //         judul: widget.judul,
                                    //         start: widget.start,
                                    //         end: widget.end,
                                    //         kategori: widget.kategori),
                                    //     judul,
                                    //     start,
                                    //     end,
                                    //     currentIndex);
                                    Navigator.pop(context);
                                  },
                                  child: Center(
                                    child: Text('simpan',
                                        style: TextStyle(color: Colors.white)),
                                  ),
                                ))
                          ]),
                      Container(
                          height: width * .13,
                          width: width * .9,
                          child: TextField(
                            controller: _editingController,
                            decoration: InputDecoration(
                              hintText: 'Judul',
                            ),
                          )),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Atur Setiap Hari'),
                            Switch(
                              value: false,
                              onChanged: (bool value) {},
                            )
                          ]),
                      Divider(color: Colors.grey),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            // DatePicker.showDateTimePicker(context,
                            //     showTitleActions: true,
                            //     minTime:
                            //         DateTime.now().subtract(Duration(days: 10)),
                            //     maxTime:
                            //         DateTime.now().add(Duration(days: 366)),
                            //     onChanged: (date) {
                            //   print('change $date in time zone ' +
                            //       date.timeZoneOffset.inHours.toString());
                            // }, onConfirm: (date) {
                            //   setState(() {
                            //     start = date;
                            //   });
                            //   print('confirm $date');
                            // }, locale: LocaleType.id);
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text('Waktu Mulai'),
                                Text(
                                    '${start.day} ${bulan[start.month - 1]} ${start.year}   ${start.hour < 10 ? '0' : ''}${start.hour}.${start.minute < 10 ? '0' : ''}${start.minute}')
                              ]),
                        ),
                      ),
                      Divider(color: Colors.grey),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            // DatePicker.showDateTimePicker(context,
                            //     showTitleActions: true,
                            //     minTime:
                            //         DateTime.now().subtract(Duration(days: 10)),
                            //     maxTime:
                            //         DateTime.now().add(Duration(days: 366)),
                            //     onChanged: (date) {
                            //   print('change $date in time zone ' +
                            //       date.timeZoneOffset.inHours.toString());
                            // }, onConfirm: (date) {
                            //   setState(() {
                            //     end = date;
                            //   });
                            //   print('confirm $date');
                            // }, locale: LocaleType.id);
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text('Waktu Selesai'),
                                Text(
                                    '${end.day} ${bulan[end.month - 1]} ${end.year}   ${end.hour < 10 ? '0' : ''}${end.hour}.${end.minute < 10 ? '0' : ''}${end.minute}')
                              ]),
                        ),
                      ),
                      Divider(color: Colors.grey),
                      Text('Pilih Kategori'),
                      Wrap(
                        alignment: WrapAlignment.start,
                        runAlignment: WrapAlignment.start,
                        runSpacing: 0.0,
                        spacing: width / 72,
                        children: kategori
                            .map((e) => ActionChip(
                                label: Text(
                                  e,
                                  style: TextStyle(fontSize: width / 30),
                                ),
                                labelStyle: TextStyle(
                                  color: currentIndex == kategori.indexOf(e)
                                      ? Colors.white
                                      : Colors.black,
                                ),
                                backgroundColor:
                                    currentIndex == kategori.indexOf(e)
                                        ? SiswamediaTheme.green
                                        : Colors.grey.shade200,
                                onPressed: () {
                                  setState(
                                      () => currentIndex = kategori.indexOf(e));
                                }))
                            .toList(),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }

  // void manual(
  //     {BuildContext context,
  //     GlobalKey<ScaffoldState> scaffoldKey,
  //     int classId,
  //     bool isPortrait}) {
  //   var _editingController = TextEditingController();
  //   var _descController = TextEditingController();
  //   var currentIndex = 0;
  //   var iconIndex = 0;
  //   var _course = 0;
  //
  //   var start = DateTime.now();
  //   var end = DateTime.now();
  //
  //   final authState = GlobalState.auth();
  //   final scheduleState = GlobalState.schedule();
  //   var course = GlobalState.course()
  //       .state
  //       .courseMap
  //       .values
  //       .where((element) => authState.state.currentState.classRole.isWaliKelas
  //           ? true
  //           : element.role.isGuru)
  //       .toList();
  //   showModalBottomSheet<void>(
  //       shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.circular(20),
  //       ),
  //       context: context,
  //       isScrollControlled: true,
  //       builder: (_) => StatefulBuilder(
  //             builder:
  //                 (BuildContext _, void Function(void Function()) setState) =>
  //                     Container(
  //               width: S.w,
  //               padding: EdgeInsets.only(
  //                   left: MediaQuery.of(context).size.width * .05,
  //                   right: MediaQuery.of(context).size.width * .05,
  //                   top: S.w * .05),
  //               height: MediaQuery.of(context).size.height * .9,
  //               decoration: BoxDecoration(
  //                   color: Colors.white,
  //                   borderRadius: BorderRadius.only(
  //                       topLeft: const Radius.circular(20.0),
  //                       topRight: const Radius.circular(20.0))),
  //               child: Column(
  //                 children: [
  //                   DashTopBottomSheet(),
  //                   SizedBox(height: S.w * .05),
  //                   Expanded(
  //                     child: SingleChildScrollView(
  //                       child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                         crossAxisAlignment: CrossAxisAlignment.start,
  //                         children: [
  //                           SizedBox(height: S.w * .05),
  //                           Row(
  //                               mainAxisAlignment:
  //                                   MainAxisAlignment.spaceBetween,
  //                               children: [
  //                                 IconButton(
  //                                   icon: Icon(
  //                                     Icons.clear,
  //                                     color: SiswamediaTheme.green,
  //                                   ),
  //                                   onPressed: () => Navigator.pop(context),
  //                                 ),
  //                                 Text('Tambah Jadwal Baru',
  //                                     style: descBlack.copyWith(
  //                                         fontSize: S.w / 26)),
  //                                 Container(
  //                                     width: S.w * .2,
  //                                     height: S.w * .08,
  //                                     decoration: BoxDecoration(
  //                                         borderRadius:
  //                                             BorderRadius.circular(7),
  //                                         color: SiswamediaTheme.green,
  //                                         boxShadow: [
  //                                           SiswamediaTheme.shadowContainer
  //                                         ]),
  //                                     child: InkWell(
  //                                       onTap: () async {
  //                                         if (end.difference(start) <
  //                                             Duration(seconds: 0)) {
  //                                           CustomFlushBar.errorFlushBar(
  //                                               'Menyetel waktu jadwal tidak valid',
  //                                               context);
  //                                         } else if (_editingController.text ==
  //                                             '') {
  //                                           return CustomFlushBar.errorFlushBar(
  //                                               'Judul harus diisi', context);
  //                                         } else if (_descController.text ==
  //                                             '') {
  //                                           return CustomFlushBar.errorFlushBar(
  //                                               'Deskripsi harus diisi',
  //                                               context);
  //                                         } else if (course.isEmpty) {
  //                                           return CustomFlushBar.errorFlushBar(
  //                                               'Belum ada mata pelajaran di kelas ini',
  //                                               context);
  //                                         } else if (_course == 0) {
  //                                           return CustomFlushBar.errorFlushBar(
  //                                               'Pilih mata pelajaran',
  //                                               context);
  //                                         } else {
  //                                           Navigator.pop(context);
  //                                           SiswamediaTheme.infoLoading(
  //                                               context: context,
  //                                               info: 'Membuat Jadwal');
  //                                           var item = CreateScheduleModel(
  //                                             name: _editingController.text,
  //                                             courseId: _course,
  //                                             iconData: iconIndex,
  //                                             category: currentIndex,
  //                                             description:
  //                                                 '${_descController.text}',
  //                                             schoolId: authState
  //                                                 .state.currentState.schoolId,
  //                                             classId: classId,
  //                                             startDate: start
  //                                                 .toUtc()
  //                                                 .toIso8601String(),
  //                                             endDate:
  //                                                 end.toUtc().toIso8601String(),
  //                                           );
  //                                           await SiswamediaGoogleSignIn
  //                                                   .requestCalendar()
  //                                               .then((isSuccess) async {
  //                                             if (isSuccess) {
  //                                               await scheduleState.state
  //                                                   .createEvents(item,
  //                                                       profile.email, context)
  //                                                   .then((value) async {
  //                                                 if (value.statusCode ==
  //                                                     SUCCESS) {
  //                                                   await scheduleState.state
  //                                                       .fetchItems(
  //                                                           classId,
  //                                                           authState
  //                                                               .state
  //                                                               .currentState
  //                                                               .classRole
  //                                                               .isOrtu)
  //                                                       .then((value) =>
  //                                                           Navigator.pop(
  //                                                               context));
  //                                                   await scheduleState
  //                                                       .notify();
  //                                                   CustomFlushBar.successFlushBar(
  //                                                       'Berhasil membuat jadwal',
  //                                                       context);
  //                                                 } else {
  //                                                   Navigator.pop(context);
  //                                                   CustomFlushBar
  //                                                       .errorFlushBar(
  //                                                           value.message,
  //                                                           context);
  //                                                 }
  //                                               });
  //                                             } else {
  //                                               Navigator.pop(context);
  //                                               CustomFlushBar.errorFlushBar(
  //                                                   'Tidak diizinkan', context);
  //                                             }
  //                                           });
  //                                         }
  //                                       },
  //                                       child: Center(
  //                                         child: Text('simpan',
  //                                             style: TextStyle(
  //                                                 color: Colors.white,
  //                                                 fontSize: S.w / 28)),
  //                                       ),
  //                                     ))
  //                               ]),
  //                           Container(
  //                               height: S.w * .13,
  //                               width: MediaQuery.of(context).size.width * .9,
  //                               child: TextField(
  //                                 controller: _editingController,
  //                                 decoration: InputDecoration(
  //                                   focusedBorder: UnderlineInputBorder(
  //                                       borderSide: BorderSide(
  //                                           color: SiswamediaTheme.green)),
  //                                   hintText: 'Judul',
  //                                 ),
  //                               )),
  //                           Container(
  //                               height: S.w * .13,
  //                               width: MediaQuery.of(context).size.width * .9,
  //                               child: TextField(
  //                                 controller: _descController,
  //                                 decoration: InputDecoration(
  //                                   focusedBorder: UnderlineInputBorder(
  //                                       borderSide: BorderSide(
  //                                           color: SiswamediaTheme.green)),
  //                                   hintText:
  //                                       'Deskripsi Singkat (Boleh diisi nama guru)',
  //                                 ),
  //                               )),
  //                           DateTimePicker(
  //                             type: DateTimePickerType.dateTime,
  //                             dateMask: 'MMM dd, yyyy - HH:mm ',
  //                             firstDate: DateTime(2000),
  //                             lastDate: DateTime(2100),
  //                             icon: Icon(Icons.calendar_today_outlined),
  //                             dateLabelText: 'Tanggal mulai',
  //                             onChanged: (val) =>
  //                                 setState(() => start = DateTime.parse(val)),
  //                           ),
  //                           DateTimePicker(
  //                             type: DateTimePickerType.dateTime,
  //                             dateMask: 'MMM dd, yyyy - HH:mm ',
  //                             firstDate: DateTime(2000),
  //                             lastDate: DateTime(2100),
  //                             icon: Icon(Icons.calendar_today_outlined),
  //                             dateLabelText: 'Tanggal berakhir',
  //                             onChanged: (val) =>
  //                                 setState(() => end = DateTime.parse(val)),
  //                           ),
  //                           SizedBox(height: 15),
  //                           Text('Pilih Kategori',
  //                               style: semiBlack.copyWith(fontSize: S.w / 26)),
  //                           Wrap(
  //                             alignment: WrapAlignment.start,
  //                             runAlignment: WrapAlignment.start,
  //                             runSpacing: 0.0,
  //                             spacing: S.w / 72,
  //                             children: kategori
  //                                 .map((e) => ActionChip(
  //                                     label: Text(
  //                                       e,
  //                                       style: TextStyle(fontSize: S.w / 30),
  //                                     ),
  //                                     labelStyle: TextStyle(
  //                                       color:
  //                                           currentIndex == kategori.indexOf(e)
  //                                               ? Colors.white
  //                                               : Colors.black,
  //                                     ),
  //                                     backgroundColor:
  //                                         currentIndex == kategori.indexOf(e)
  //                                             ? SiswamediaTheme.green
  //                                             : Colors.grey.shade200,
  //                                     onPressed: () {
  //                                       setState(() =>
  //                                           currentIndex = kategori.indexOf(e));
  //                                     }))
  //                                 .toList(),
  //                           ),
  //                           Divider(color: Colors.grey),
  //                           Text('Pilih Mata Pelajaran',
  //                               style: semiBlack.copyWith(fontSize: S.w / 26)),
  //                           if (course.isNotEmpty && currentIndex != 3)
  //                             SizedBox(
  //                               width: S.w * .7,
  //                               child: DropdownButtonFormField<int>(
  //                                 isExpanded: true,
  //                                 value: _course,
  //                                 icon: Icon(Icons.keyboard_arrow_down,
  //                                     color: SiswamediaTheme.green),
  //                                 iconSize: 24,
  //                                 elevation: 16,
  //                                 decoration: InputDecoration(
  //                                   contentPadding: EdgeInsets.symmetric(
  //                                       vertical: 0.0, horizontal: 25),
  //                                   enabledBorder: OutlineInputBorder(
  //                                     borderSide:
  //                                         BorderSide(color: Color(0xffD8D8D8)),
  //                                     borderRadius: BorderRadius.circular(S.w),
  //                                   ),
  //                                   focusedBorder: OutlineInputBorder(
  //                                     borderSide: BorderSide(
  //                                         color: SiswamediaTheme.green),
  //                                     borderRadius: BorderRadius.circular(S.w),
  //                                   ),
  //                                 ),
  //                                 style: descBlack.copyWith(
  //                                     fontSize: S.w / 30,
  //                                     color: Color(0xff4B4B4B)),
  //                                 onChanged: (int newValue) {
  //                                   setState(() {
  //                                     _course = newValue;
  //                                   });
  //                                 },
  //                                 items: [
  //                                   DropdownMenuItem(
  //                                     child: Text('Pilih Pelajaran'),
  //                                     value: 0,
  //                                   ),
  //                                   for (CourseByClassData value in course)
  //                                     DropdownMenuItem(
  //                                       value: value.id,
  //                                       child: Text('${value.name}'),
  //                                     )
  //                                 ],
  //                               ),
  //                             ),
  //                           if (course.isEmpty)
  //                             Text(
  //                                 'Kamu Belum terdaftar di mata pelajaran manapun daftar dulu',
  //                                 style:
  //                                     descBlack.copyWith(fontSize: S.w / 32)),
  //                           SizedBox(height: 10),
  //                           Text('Pilih Icon',
  //                               style: semiBlack.copyWith(fontSize: S.w / 26)),
  //                           Container(
  //                             child: GridView.count(
  //                               physics: ScrollPhysics(),
  //                               crossAxisCount: gridCountIcon(
  //                                   MediaQuery.of(context).size.aspectRatio),
  //                               shrinkWrap: true,
  //                               childAspectRatio: 1,
  //                               scrollDirection: Axis.vertical,
  //                               children: iconMateri
  //                                   .map((e) => Container(
  //                                       height: S.w * .1,
  //                                       width: S.w * .1,
  //                                       margin: EdgeInsets.all(S.w * .015),
  //                                       decoration: BoxDecoration(
  //                                         color:
  //                                             iconIndex == iconMateri.indexOf(e)
  //                                                 ? SiswamediaTheme.green
  //                                                 : Colors.white,
  //                                         boxShadow: [
  //                                           BoxShadow(
  //                                               offset: Offset(0, 0),
  //                                               spreadRadius: 2,
  //                                               blurRadius: 2,
  //                                               color:
  //                                                   Colors.grey.withOpacity(.2))
  //                                         ],
  //                                         borderRadius:
  //                                             BorderRadius.circular(12),
  //                                       ),
  //                                       child: InkWell(
  //                                         onTap: () {
  //                                           setState(() {
  //                                             print(iconIndex);
  //                                             iconIndex = iconMateri.indexOf(e);
  //                                           });
  //                                         },
  //                                         child: Padding(
  //                                           padding: EdgeInsets.all(S.w * .02),
  //                                           child: Image.asset(
  //                                             'assets/icons/materi/$e.png',
  //                                             height: S.w * .06,
  //                                             width: S.w * .06,
  //                                           ),
  //                                         ),
  //                                       )))
  //                                   .toList(),
  //                             ),
  //                           )
  //                         ],
  //                       ),
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ));
  // }
}
