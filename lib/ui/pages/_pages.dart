import 'dart:async';

import 'dart:convert';

// import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:logger/logger.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/features/studyJournal/presentation/pages/_pages.dart';
import 'package:tegarmedia/services/share_service.dart';
import 'package:tegarmedia/ui/pages/Pantau/_pantau.dart';
import 'core/_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/user/_user.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/base/_base.dart';
import 'package:tegarmedia/ui/pages/Absensi/_absensi.dart';
import 'package:tegarmedia/ui/pages/Academic/_academic.dart';
import 'package:tegarmedia/ui/pages/ClassPage/_class_page.dart';
import 'package:tegarmedia/ui/pages/Conference/_conference.dart';
import 'package:tegarmedia/ui/pages/Document/_document.dart';
import 'package:tegarmedia/ui/pages/Forum/_forum.dart';
import 'package:tegarmedia/ui/pages/HomePage/_home.dart';
import 'package:tegarmedia/ui/pages/InteraksiBelajar/_interaksi.dart';
import 'package:tegarmedia/ui/pages/Materi/_materi.dart';
import 'package:tegarmedia/ui/pages/Profile/_profile.dart';
import 'package:tegarmedia/ui/pages/Schedule/_schedule.dart';
import 'package:tegarmedia/ui/pages/School/_school.dart';
import 'package:tegarmedia/ui/pages/Score/_score.dart';
import 'package:tegarmedia/ui/pages/Assignment/_assignment.dart';
import 'package:tegarmedia/ui/pages/Ujian/_ujian.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/ui/widgets/class/_class.dart';
import 'package:tegarmedia/ui/widgets/home/_home.dart';
import 'package:tegarmedia/utils/v_widget.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:tegarmedia/utils/v_utils.dart';
import '../widgets/_widgets.dart';
import '../../models/_models.dart';

export 'package:tegarmedia/data/_static_data.dart';
export 'package:tegarmedia/ui/base/_base.dart';

part 'materi_page.dart';
part 'class_page.dart';

part 'Course/course_list.dart';
part 'Course/detail_course_instansi.dart';
part 'Course/list_course_by_class.dart';
part 'Course/card_daftar_guru.dart';

part 'Notification/notification_page.dart';
part 'home_page.dart';
part 'test_ui.dart';
part 'HomePage/menu_list.dart';
part 'academic_page.dart';
part 'pdf_viewer.dart';
