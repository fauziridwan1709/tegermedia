part of '_pages.dart';

class Materi extends StatefulWidget {
  @override
  _MateriState createState() => _MateriState();
}

class _MateriState extends State<Materi> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return MateriPage(width: S.w);
  }
}

class MateriPage extends StatefulWidget {
  const MateriPage({Key key, this.width}) : super(key: key);

  final double width;
  @override
  _MateriPageState createState() => _MateriPageState();
}

class _MateriPageState extends State<MateriPage> with TickerProviderStateMixin {
  final materiState = Injector.getAsReactive<MateriState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final ScrollController scrollController = ScrollController();
  bool _showAppbar = true;
  bool isScrollingDown = false;

  Animation<double> topBarAnimation;
  Animation<double> jenjangAnimation;

  List<Widget> listViews = <Widget>[];

  var topBarOpacity = 0.0;

  bool isOpen = false;
  bool _isExpanded = false;

  AnimationController _animationController;

  String jenjang;
  String kurikulum;
  String kelas;
  String jurusan;

  String _choose1;
  String _choose2;
  String _choose3;
  String msg = '';

  @override
  void initState() {
    _animationController =
        AnimationController(duration: const Duration(milliseconds: 700), vsync: this);
    jenjangAnimation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(parent: _animationController, curve: Curves.easeIn));
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.userScrollDirection == ScrollDirection.reverse) {
        if (!isScrollingDown) {
          setState(() {
            isScrollingDown = true;
            _showAppbar = false;
          });
        }
      }

      if (scrollController.position.userScrollDirection == ScrollDirection.forward) {
        if (isScrollingDown) {
          setState(() {
            isScrollingDown = false;
            _showAppbar = true;
          });
        }
      }
    });
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  void handleOnPress() {
    if (isOpen) {
      setState(() {
        _animationController.reverse();
        isOpen = !isOpen;
      });
    } else {
      setState(() {
        _animationController.forward();
        isOpen = !isOpen;
      });
    }
  }

  TextEditingController controller = TextEditingController();
  FocusNode fNodeSearch = FocusNode();

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    print(jenjangAnimation.value);
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return ScreenTypeLayout(
      mobile: StateBuilder<MateriState>(
          observe: () => GlobalState.materi(),
          builder: (context, data) {
            print('---data---');
            print(data.state.materi.name);
            return Scaffold(
                key: scaffoldKey,
                backgroundColor: Colors.white,
                body: SafeArea(
                  child: NestedScrollView(
                    floatHeaderSlivers: true,
                    headerSliverBuilder: (_, val) => [
                      SliverAppBar(
                        title: Container(
                          padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border(bottom: BorderSide(color: Colors.grey[300]))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () => handleOnTap(),
                                child:
                                    Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                  CircleAvatar(
                                    backgroundImage: AssetImage(
                                        'assets/icons/materi/${materiState.state.materi.jenjang ?? 'smk'}.png'),
                                    // child:
                                    //     InkWell(onTap: () => handleOnTap()),
                                  ),
                                  SizedBox(width: S.w / 36),
                                  Text('${materiState.state.materi.name ?? 'Pilih Jenjang'}',
                                      style: descBlack.copyWith(fontSize: S.w / 32)),
                                  if (materiState.state.materi.name == null)
                                    Icon(
                                      Icons.arrow_drop_down,
                                      color: SiswamediaTheme.green,
                                      // size: 14,
                                    )
                                ]),
                              ),
                              Hero(
                                tag: 'ad',
                                child: Container(
                                  width: isPortrait ? S.w * .45 : S.w * .6,
                                  height: S.w * .1,
                                  color: Colors.white,
                                  child: Container(
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: SiswamediaTheme.white,
                                        borderRadius: BorderRadius.all(Radius.circular(S.w)),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey.withOpacity(.2),
                                              offset: Offset(0, 0),
                                              spreadRadius: 2,
                                              blurRadius: 2)
                                        ]),
                                    child: Center(
                                      child: Material(
                                        color: Colors.transparent,
                                        child: TextField(
                                          focusNode: fNodeSearch,
                                          onTap: () {
                                            if (materiState.state.materi.jenjang != null) {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute<void>(
                                                      builder: (_) => SearchPage()));
                                              fNodeSearch.unfocus();
                                            } else {
                                              CustomFlushBar.errorFlushBar(
                                                  'Anda Belum memilih jenjang!', context);
                                              fNodeSearch.unfocus();
                                            }
                                          },
                                          controller: controller,
                                          style: TextStyle(fontSize: 16),
                                          decoration: InputDecoration(
                                            hintText: 'Cari Materi...',
                                            hintStyle: descBlack.copyWith(
                                                fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                            contentPadding:
                                                EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.transparent),
                                              borderRadius: BorderRadius.circular(S.w),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.transparent),
                                              borderRadius: BorderRadius.circular(S.w),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              //todo uncomment
                              //TODO implement search materi
                              // Material(
                              //   color: Colors.transparent,
                              //   child: InkWell(
                              //     onTap: () {
                              //       dialogDev(context,
                              //           title: 'Pengembangan',
                              //           desc:
                              //               'Kalau yang ini lagi tahap penyempurnaan ya! tunggu beberapa hari lagi');
                              //       // Navigator.push(context,
                              //       //     MaterialPageRoute<void>(builder: (_) => MateriMe()));
                              //     },
                              //     child: Icon(
                              //       Icons.bookmark_border,
                              //       size: 26,
                              //       color: Colors.black,
                              //     ),
                              //   ),
                              // )
                            ],
                          ),
                        ),
                        toolbarHeight: AppBar().preferredSize.height * 1.5,
                        floating: true,
                        // pinned: true,
                        // snap: val,
                        titleSpacing: 0,
                        shadowColor: Colors.transparent,
                      ),
                    ],
                    body: Stack(
                      children: [
                        ListView(children: [
                          Pelajaran(
                            scaffoldKey: scaffoldKey,
                            orientation: orientation,
                          ),
                          SizedBox(height: 10),
                          MateriUmum(),
                          Titles(title: 'Materi Terpopuler'),
                          Om(),
                          Titles(title: 'Jadwal Tayangan Pendidikan TVRI'),
                          JadwalTV(jenis: 'rri'),
                          //todo uncomment this
                          // Titles(title: 'Berbagi ilmu'),
                          // BeritaUntukKamu(),
                          Titles(title: 'Game Edu'),
                          GameEdu(),
                          SizedBox(height: 20),
                          // Titles(title: 'Aplikasi materi belajar lain'),
                          // AplikasiLain(),
                        ]),
                        jenjangWidget(size: S.w),
                      ],
                    ),
                  ),
                ));
          }),
    );
  }

  Widget paddingJenjang() {
    return CustomExpansionTile(
      isExpanded: _isExpanded,
      controller: _animationController,
      children: [
        Container(
            height: widget.width / 3.7,
            padding: EdgeInsets.symmetric(
              horizontal: widget.width * .05,
            )),
      ],
    );
  }

  Widget jenjangWidget({double size}) {
    return CustomExpansionTile(
      isExpanded: _isExpanded,
      controller: _animationController,
      children: [
        Card(
          shape: RoundedRectangleBorder(
              // borderRadius: BorderRadius.only(
              //     bottomRight: Radius.circular(size / size),
              //     bottomLeft: Radius.circular(size / 30))
              ),
          margin: EdgeInsets.only(bottom: size / 120),
          child: Container(
              height: size / 3.6,
              padding: EdgeInsets.symmetric(
                horizontal: size * .05,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: size * .05,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: JenjangData.jenjang
                          .map((tingkat) => Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: size / 8.4,
                                    height: size / 8.4,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey.withOpacity(.2),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/asset_materi/$tingkat.png')),
                                    ),
                                    child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          callback(jenjang: tingkat.toString(), context: context);
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: size / 72),
                                  Text(tingkat.toUpperCase(),
                                      style: TextStyle(fontSize: size / 30)),
                                ],
                              ))
                          .toList()),
                ],
              )),
        )
      ],
    );
  }

  void handleOnTap() {
    setState(() {
      _isExpanded = !_isExpanded;
      if (_isExpanded) {
        print('forward');
        _animationController.forward().then<void>((void value) {
          if (!mounted) return;
          // setState(() {});
        });
      } else {
        _animationController.reverse().then<void>((void value) {
          if (!mounted) return;
          // setState(() {});
        });
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
  }

  void callback({String jenjang, BuildContext context, Map<dynamic, dynamic> data}) {
    // var S.w = MediaQuery.of(context).size.width;
    _choose1 = null;
    _choose2 = null;
    _choose3 = null;
    msg = '';
    if (jenjang == 'sma' || jenjang == 'smp' || jenjang == 'sd' || jenjang == 'tk') {
      showDialog<void>(
          context: context,
          builder: (context) => StatefulBuilder(
                builder: (BuildContext context, void Function(void Function()) setState) {
                  return Dialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                    child: Container(
                      height: jenjang == 'smk' ? S.w * .95 : S.w * .8,
                      width: S.w * .9,
                      padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Yang kamu butuhkan'),
                          Container(
                            width: S.w * .6,
                            height: S.w * .1,
                            padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                            decoration: SiswamediaTheme.dropDownDecoration,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                  icon: Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.blue,
                                  ),
                                  hint: Text('kelas'),
                                  value: _choose1,
                                  onChanged: (String o) {
                                    setState(() {
                                      _choose1 = o;
                                    });
                                  },
                                  items: JenjangData.getKelas(jenjang)
                                      .map((e) => DropdownMenuItem(
                                            value: e.toString(),
                                            child: Text(e),
                                          ))
                                      .toList()),
                            ),
                          ),
                          Container(
                            width: S.w * .6,
                            height: S.w * .1,
                            padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                            decoration: SiswamediaTheme.dropDownDecoration,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                  icon: Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.blue,
                                  ),
                                  hint: Text('Kurikulum'),
                                  value: _choose2,
                                  onChanged: (String o) {
                                    setState(() {
                                      _choose2 = o;
                                    });
                                  },
                                  items: JenjangData.kurikulum
                                      .map((e) => DropdownMenuItem(
                                            value: e,
                                            child: Text(e),
                                          ))
                                      .toList()),
                            ),
                          ),
                          if (jenjang == 'smk')
                            Container(
                              width: S.w * .6,
                              height: S.w * .1,
                              padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                              decoration: SiswamediaTheme.dropDownDecoration,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                    icon: Icon(
                                      Icons.keyboard_arrow_down,
                                      color: Colors.blue,
                                    ),
                                    hint: Text('jurusan'),
                                    value: _choose3,
                                    onChanged: (String o) {
                                      setState(() {
                                        _choose3 = o;
                                      });
                                    },
                                    items: JenjangData.jurusan
                                        .map((e) => DropdownMenuItem(
                                              value: e,
                                              child: Text(e),
                                            ))
                                        .toList()),
                              ),
                            ),
                          Column(children: [
                            Text(msg, style: TextStyle(color: Colors.red)),
                            SizedBox(height: S.w / 60),
                            ButtonTerapkan(callback: () {
                              if (_choose1 == null ||
                                  _choose2 == null ||
                                  (jenjang == 'smk' ? _choose3 == null : false)) {
                                setState(() => msg = 'isi semua data yaa');
                              } else {
                                Navigator.pop(context, true);
                                print(jenjang);
                                materiState.setState(
                                    (s) => s.setMateri(MateriDetail(
                                        jenjang: jenjang,
                                        kelas: _choose1,
                                        kurikulum: _choose2,
                                        pelajaran: null,
                                        pengguna: '0')),
                                    onData: (context, data) =>
                                        print('---data${data.materi.jenjang}'));
                                handleOnTap();
                              }
                            })
                          ]),
                        ],
                      ),
                    ),
                  );
                },
              ));
    }
  }
}

class BeritaUntukKamu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    return Container(
        margin: EdgeInsets.only(left: S.w * .05, right: S.w * .05, bottom: S.w * .03),
        height: 120,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
            boxShadow: [
              BoxShadow(
                  offset: Offset(2, 2),
                  spreadRadius: 2,
                  blurRadius: 4,
                  color: Colors.grey.withOpacity(.2))
            ]),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => FABAction.floatingAction(context),
              child: Stack(children: [
                Positioned(
                    left: 15,
                    child: Container(
                        height: 120,
                        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                          Text('Mari Berbagi Ilmu',
                              style: secondHeadingWhite.copyWith(
                                fontSize: S.w / 26,
                                color: SiswamediaTheme.green,
                              )),
                          Text('Unggah materimu disini',
                              style: descBlack.copyWith(
                                fontSize: S.w / 34,
                              )),
                        ]))),
                Positioned(
                  right: 5,
                  bottom: 0,
                  child: Image.asset(
                    'assets/beritauntukkamu.png',
                    height: S.w / 2.6,
                    width: S.w / 2.6,
                  ),
                ),
                Positioned(
                  left: 0,
                  bottom: -(S.w * .11),
                  child: Image.asset(
                    'assets/beritauntuk.png',
                    height: S.w / 2.5,
                    width: S.w / 2.5,
                  ),
                ),
              ]),
            ),
          ),
        ));
  }
}
