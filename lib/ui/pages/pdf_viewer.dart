part of '_pages.dart';

class CustomPdfViewer extends StatefulWidget {
  final String url;
  final String title;

  CustomPdfViewer({this.url, this.title});

  @override
  _CustomPdfViewerState createState() => _CustomPdfViewerState();
}

class _CustomPdfViewerState extends State<CustomPdfViewer> {
  // PDFDocument doc;
  bool _isLoading = true;
  @override
  void initState() {
    // Load from URL

    pdfLoader();
    super.initState();
  }

  Future<void> pdfLoader() async {
    // doc = await PDFDocument.fromURL(widget.url);
    _isLoading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: vText(
          widget.title,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_outlined,
            color: SiswamediaTheme.primary,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: <Widget>[
            // Expanded(
            //     child: _isLoading
            //         ? Center(child: CircularProgressIndicator())
            //         : PDFViewer(
            //             document: doc,
            //           )),
          ],
        ),
      ),
    );
  }
}
