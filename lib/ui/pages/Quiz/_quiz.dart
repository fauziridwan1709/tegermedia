import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:tegarmedia/app.dart';
import 'package:tegarmedia/main.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/provider/_provider.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'quiz_info.dart';
part 'quis_start.dart';
part 'quis_result.dart';
part 'quiz_jawaban.dart';
