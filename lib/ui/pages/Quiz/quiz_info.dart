part of '_quiz.dart';

class QuizInfo extends StatefulWidget {
  @override
  _QuizInfoState createState() => _QuizInfoState();
}

class _QuizInfoState extends State<QuizInfo> {
  var desc =
      'Untuk soal esai yang membutuhkan rumus silahkan kerjakan di kertas, dan kumpulkan dengan cara foto jawaban per soal lalu unggah foto anda di tombol unggah di bawah ini (pastikan foto nya jelas)';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //todo later
    // var data = Provider.of<QuizProvider>(context);
    // var auth = context.watch<UserProvider>();
    // var detail = data.detailClass;
    // var hehe = Color(0xffE8F5D6);
    MateriQuizDetail detail = MateriQuizDetail();
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text('Kuis ${detail.materi}',
            style: TextStyle(color: Colors.black, fontSize: S.w / 23)),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
      ),
      body: SingleChildScrollView(
        child: FutureBuilder<QuizModel>(
            future: MateriQuizServices.getQuizData(
                detail.kelas, detail.matapelajaran, detail.materi),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      height: S.w * .5,
                      width: S.w,
                      margin: EdgeInsets.symmetric(vertical: S.w * .03),
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(detail.materi,
                              style: TextStyle(
                                  color: SiswamediaTheme.green,
                                  fontSize: S.w / 25,
                                  fontWeight: FontWeight.w700)),
                          SizedBox(
                            height: 5,
                          ),
                          Text('Jumlah Soal: ${detail.jumlahsoal}',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w600)),
                          Text('Soal Pilihan Ganda : ${detail.jumlahsoal}',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w600)),
                          Text('Soal Esai : - Butir',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.w600)),
                          SizedBox(
                            height: 5,
                          ),
                          Text('Batas Waktu Pengerjaan : ${detail.menit} menit',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: S.w / 30,
                                  fontWeight: FontWeight.normal)),
                        ],
                      ),
                    ),
                    Container(
                      width: S.w,
                      color: Colors.white,
                      height: S.w * .6,
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      margin: EdgeInsets.symmetric(vertical: S.w * .03),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Petunjuk',
                              style: TextStyle(
                                  fontSize: S.w / 25,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600)),
                          Container(
                            height: S.w * .4,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(S.w / 30),
                                color: Colors.grey,
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'http://img.youtube.com/vi/3WOCNVYxnzQ/mqdefault.jpg'))),
                            child: InkWell(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => VideoScreenQuiz(
                                          url:
                                              'https://youtu.be/3WOCNVYxnzQ'))),
                              child: Center(
                                  child: Container(
                                width: S.w * .1,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: SiswamediaTheme.green,
                                    border: Border.all(
                                      color: Colors.white,
                                      width: 2,
                                    )),
                                child: Center(
                                  child: Icon(
                                    Icons.play_arrow,
                                    color: Colors.white,
                                  ),
                                ),
                              )),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: S.w * .42,
                      width: S.w,
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      margin: EdgeInsets.symmetric(
                          vertical: S.w * .03, horizontal: S.w * .03),
                      decoration: BoxDecoration(
                        color: SiswamediaTheme.lightGreen,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Column(children: [
                        Text('Catatan',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: S.w / 25,
                                fontWeight: FontWeight.w700)),
                        Text(desc,
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: S.w / 28,
                            )),
                      ]),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: S.w * .05, vertical: S.w * .05),
                      height: S.w * .12,
                      width: S.w * .9,
                      decoration: BoxDecoration(
                        color: SiswamediaTheme.green,
                        borderRadius: BorderRadius.circular(S.w / 30),
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            //todo later setquestion
                            // data.setQuestion(snapshot.data.rows);
                            Navigator.push(
                                context,
                                MaterialPageRoute<void>(
                                    builder: (_) => QuizStart(
                                          duration: detail.menit,
                                        )));
                          },
                          child: Center(
                              child: Text('mulai',
                                  style: TextStyle(
                                      fontSize: S.w / 28,
                                      color: Colors.white))),
                        ),
                      ),
                    )
                  ],
                );
              }
              return Padding(
                padding: EdgeInsets.only(top: S.w / 3),
                child: Center(child: CircularProgressIndicator()),
              );
            }),
      ),
    );
  }
}
