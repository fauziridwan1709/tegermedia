part of '_quiz.dart';

class ResultPage extends StatelessWidget {
  ResultPage({Key key, this.rightAnswer, this.currentAnswer, this.duration})
      : super(key: key);

  final Map<String, String> rightAnswer;
  final Map<String, String> currentAnswer;
  final int duration;

  @override
  Widget build(BuildContext context) {
    var nilai = 0;
    var salah = 0;
    rightAnswer.forEach((String key, String value) {
      if (value == currentAnswer[key]) {
        nilai++;
      } else if (value != currentAnswer[key]) {
        salah++;
      }
    });

    var width = MediaQuery.of(context).size.width;
    //todo penting implement quiz di materi
    // var data = Provider.of<QuizProvider>(context);
    // MateriQuizDetail detail = data.detailClass;
    // print(data.item.length);
    // int dilewati = data.item.length - nilai - salah;
    MateriQuizDetail detail;
    var dilewati = 0;

    return WillPopScope(
      onWillPop: () {
        Navigator.popUntil(context, (route) => route.isFirst);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.popUntil(context, (route) => route.isFirst);
                // Navigator.pushReplacement(
                // context, MaterialPageRoute<void>(builder: (_) => Layout()));
              }),
          title: Text(
            'Hasil Latihan Quiz',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: width / 25,
                color: Colors.black),
          ),
        ),
        body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: width * .05, vertical: width * .05),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: width / 1.4,
                    padding: EdgeInsets.all(width * .04),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.2),
                            offset: Offset(2, 2),
                            spreadRadius: 2,
                            blurRadius: 2,
                          )
                        ]),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            'Latihan Soal ${detail.materi}',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: width / 28,
                                fontWeight: FontWeight.w700),
                          ),
                          RichText(
                            text: TextSpan(
                              // Note: Styles for TextSpans must be explicitly defined.
                              // Child text spans will inherit styles from parent
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Waktu pengerjaan : ',
                                    style: TextStyle(
                                        fontSize: width / 28,
                                        color: Colors.grey[600])),
                                TextSpan(
                                    text:
                                        ' ${format(int.parse(detail.menit) * 60 - duration)}',
                                    style: TextStyle(
                                        fontSize: width / 28,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.black87)),
                              ],
                            ),
                          ),
                          Divider(color: Colors.grey),
                          Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: width * .05),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 80,
                                      child: Center(
                                        child: Text(
                                          'Benar',
                                          style: TextStyle(
                                              color: SiswamediaTheme.green,
                                              fontSize: width / 30,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      '$nilai',
                                      style: TextStyle(
                                          color: SiswamediaTheme.green,
                                          fontSize: width / 30,
                                          fontWeight: FontWeight.w700),
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 80,
                                      child: Center(
                                        child: Text(
                                          'Salah',
                                          style: TextStyle(
                                              color: Colors.red,
                                              fontSize: width / 30,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      '$salah',
                                      style: TextStyle(
                                          color: SiswamediaTheme.green,
                                          fontSize: width / 30,
                                          fontWeight: FontWeight.w700),
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 80,
                                      child: Center(
                                        child: Text(
                                          'Dilewati',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: width / 30,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      '$dilewati',
                                      style: TextStyle(
                                          color: SiswamediaTheme.green,
                                          fontSize: width / 30,
                                          fontWeight: FontWeight.w700),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: 1),
                          Text('Skor'),
                          Container(
                              padding: EdgeInsets.all(width / 22),
                              child: Center(
                                  child: Text(
                                '${(nilai * 100)}',
                                //todo penting
                                // data.item.length}',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: width / 18,
                                    fontWeight: FontWeight.w700),
                              )),
                              decoration: BoxDecoration(
                                  color: SiswamediaTheme.green,
                                  shape: BoxShape.circle)),
                        ]),
                  ),
                  Column(children: [
                    Container(
                      width: width * .9,
                      height: width * .11,
                      decoration: BoxDecoration(
                        color: SiswamediaTheme.green,
                        borderRadius: BorderRadius.circular(width),
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.popUntil(context, (route) => route.isFirst);
                          //Navigator.pushReplacement(context,
                          //  MaterialPageRoute<void>(builder: (_) => Layout()));
                        },
                        child: Center(
                            child: Text('Kembali',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                    fontSize: width / 30))),
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      width: width * .9,
                      height: width * .11,
                      decoration: BoxDecoration(
                        color: Color(0xff4B4B4B),
                        borderRadius: BorderRadius.circular(width),
                      ),
                      child: InkWell(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute<void>(
                                builder: (_) => QuizJawaban())),
                        child: Center(
                            child: Text('Jawaban',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                    fontSize: width / 30))),
                      ),
                    ),
                  ])
                ])),
      ),
    );
  }

  //#4B4B4B
  String format(int duration) {
    if (duration < 60) {
      return '$duration detik';
    } else if (duration < 3600) {
      return '${duration ~/ 60} Menit ${duration % 60} detik';
    } else {
      return '${duration ~/ 3600} Jam ${duration ~/ 60} Menit ${duration % 60} detik';
    }
  }
}
/*Text('Nilai anda ${(nilai * 100) ~/ data.item.length}')*/
