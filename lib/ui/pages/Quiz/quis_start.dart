part of '_quiz.dart';

class data {
  static List<QuizItem> item = <QuizItem>[];
}

class QuizStart extends StatefulWidget {
  final String duration;
  QuizStart({this.duration});

  @override
  _QuizStartState createState() => _QuizStartState();
}

class _QuizStartState extends State<QuizStart> {
  Timer timer;
  int _timerValue;

  @override
  void initState() {
    super.initState();
    _timerValue = int.parse(widget.duration) * 60;
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      startTimer();
    });
  }

  Timer _timer;

  void startTimer() async {
    var width = MediaQuery.of(context).size.width;
    _timer = Timer.periodic(
        Duration(seconds: 1),
        (Timer timer) => setState(() {
              print(_timerValue);
              if (_timerValue < 1) {
                _timer.cancel();
                showDialog<void>(
                    context: context,
                    builder: (context) {
                      return Dialog(
                        child: Container(
                            height: width * .63,
                            width: width / 2.4,
                            padding: EdgeInsets.symmetric(
                                horizontal: width * .05, vertical: width * .05),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Waktu Habis!',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: width / 28,
                                        fontWeight: FontWeight.w700)),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * .01),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      color: SiswamediaTheme.green,
                                    ),
                                    height: width / 10,
                                    width: width / 3,
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute<void>(
                                                builder: (_) => ResultPage(
                                                      duration: _timerValue,
                                                      currentAnswer:
                                                          currentAnswer,
                                                      rightAnswer: correction,
                                                    )));
                                      },
                                      child: Center(
                                        child: Text('Lihat Nilai',
                                            style:
                                                TextStyle(color: Colors.white)),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )),
                      );
                    }).then((value) => Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                        builder: (_) => ResultPage(
                              duration: _timerValue,
                              currentAnswer: currentAnswer,
                              rightAnswer: correction,
                            ))));
                Navigator.pushReplacement(context,
                    MaterialPageRoute<void>(builder: (context) => Layout()));
              } else {
                _timerValue = _timerValue - 1;
                print(_timerValue);
              }
            }));
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  int count = 0;

  int myIndex = 0;
  Map<String, dynamic> correction = <String, dynamic>{};
  Map<String, dynamic> currentAnswer = <String, dynamic>{};
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    // var data = Provider.of<QuizProvider>(context);
    // var data = Data();
    print(currentAnswer);
    print(correction);
    return ChangeNotifierProvider(
      create: (_) => QuizState(),
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                _timer.cancel();
                Navigator.pop(context);
              }),
          centerTitle: true,
          actions: [
            Center(
              child: InkWell(
                onTap: () => showDialog<void>(
                    context: context,
                    builder: (context) {
                      var contentText =
                          'Apakah anda yakin untuk menyudahi pekerjaan soal, cek kembali dan pastikan tidak ada soal yang terlewati';
                      return Dialog(
                        child: Container(
                            height: width * .63,
                            width: width / 2.4,
                            padding: EdgeInsets.symmetric(
                                horizontal: width * .05, vertical: width * .05),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Sudahi Latihan Soal',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: width / 28,
                                        fontWeight: FontWeight.w700)),
                                Text(
                                  contentText,
                                  textAlign: TextAlign.center,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * .01),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6),
                                            color: SiswamediaTheme.green,
                                          ),
                                          height: width / 10,
                                          width: width / 4,
                                          child: InkWell(
                                            onTap: () => Navigator.pop(context),
                                            child: Center(
                                              child: Text('Tidak',
                                                  style: TextStyle(
                                                      color: Colors.white)),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: width / 10,
                                          width: width / 4,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6),
                                            color: Color(0xff4B4B4B),
                                          ),
                                          child: InkWell(
                                            onTap: () {
                                              _timer.cancel();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute<void>(
                                                      builder: (_) =>
                                                          ResultPage(
                                                            currentAnswer:
                                                                currentAnswer,
                                                            rightAnswer:
                                                                correction,
                                                            duration:
                                                                _timerValue,
                                                          )));
                                            },
                                            child: Center(
                                              child: Text('Ya',
                                                  style: TextStyle(
                                                      color: Colors.white)),
                                            ),
                                          ),
                                        ),
                                      ]),
                                )
                              ],
                            )),
                      );
                    }),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                  child: Text(
                    'Selesai',
                    style: TextStyle(
                        color: SiswamediaTheme.green, fontSize: width / 30),
                  ),
                ),
              ),
            )
          ],
          title: Text(
            'Kuis',
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: width / 25,
                color: Colors.black),
          ),
        ),
        key: _scaffoldKey,
        backgroundColor: SiswamediaTheme.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: width * .05),
            child: Column(
              children: [
                Consumer<QuizState>(
                  builder: (context, state, child) => SizedBox(
                    height: 50,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: data.item.length,
                      itemBuilder: (context, index) => Container(
                          height: 40,
                          width: 40,
                          margin: EdgeInsets.only(
                              left: index == 0 ? width * .05 : 6.0,
                              top: 6,
                              right: index == data.item.length - 1
                                  ? width * .05
                                  : 6,
                              bottom: 6),
                          decoration: BoxDecoration(
                              border: myIndex == index
                                  ? Border.all(color: SiswamediaTheme.green)
                                  : null,
                              borderRadius: BorderRadius.circular(12),
                              color:
                                  state.selected[(index + 1).toString()] != null
                                      ? SiswamediaTheme.green
                                      : Colors.white),
                          child: Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(12),
                            child: InkWell(
                              onTap: () {
                                setState(() => myIndex = index);
                              },
                              child: Container(
                                child: Center(
                                    child: Text('${index + 1}',
                                        style: TextStyle(
                                            color: state.selected[(index + 1)
                                                        .toString()] !=
                                                    null
                                                ? Colors.white
                                                : SiswamediaTheme.green,
                                            fontSize: 14))),
                              ),
                            ),
                          )),
                    ),
                  ),
                ),
                data.item[myIndex].attpertanyaan.contains('http')
                    ? Container(
                        child: Image.network(
                          data.item[myIndex].attpertanyaan,
                          height: 170,
                          width: 170,
                        ),
                      )
                    : Container(
                        height: 0,
                      ),
                Card(
                  margin: EdgeInsets.symmetric(
                      horizontal: width * .03, vertical: width * .02),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    width: width * .94,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: width * .03, horizontal: width * .05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Soal',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: width / 22,
                                  fontWeight: FontWeight.w600)),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: Text(data.item[myIndex].pertanyaan,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Consumer<QuizState>(
                  builder: (context, state, child) {
                    var sampleData = <String>[
                      data.item[myIndex].pilihana,
                      data.item[myIndex].pilihanb,
                      data.item[myIndex].pilihanc,
                    ];
                    if (data.item[myIndex].pilihand != '0') {
                      sampleData.add(data.item[myIndex].pilihand);
                    }
                    return Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: sampleData.map((opt) {
                          return Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 0, vertical: 7),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(.2),
                                    offset: Offset(0, 0),
                                    spreadRadius: 2,
                                    blurRadius: 2)
                              ],
                              borderRadius: BorderRadius.circular(22),
                              color: state.selected[(myIndex + 1).toString()] ==
                                      opt
                                  ? Color(0xffE8F5D6)
                                  : Colors.white,
                            ),
                            child: Material(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(22),
                              child: InkWell(
                                onTap: () {
                                  state.adding((myIndex + 1).toString(), opt);
                                  currentAnswer = state.selected;
                                  correction[(myIndex + 1).toString()] =
                                      data.item[myIndex].jawaban;
                                },
                                child: Container(
                                  padding: EdgeInsets.all(16),
                                  child: Row(
                                    children: [
                                      Text(QuizChoice
                                          .choice[sampleData.indexOf(opt)]),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(left: 16),
                                          child: Text(
                                            opt,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: RaisedButton(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                          onPressed: () {
                            setState(() {
                              myIndex--;
                            });
                          },
                          child: Text(
                            'back',
                            style: TextStyle(
                                color: SiswamediaTheme.green,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      Container(
                        height: width * .1,
                        padding: EdgeInsets.symmetric(
                            horizontal: width * .05, vertical: width * .01),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(width),
                        ),
                        child: Container(
                          height: 60,
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                    '${_timerValue ~/ 60}:${_timerValue % 60}',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black)),
                              ),
                              Positioned(
                                  right: width * .09,
                                  top: 0,
                                  bottom: 0,
                                  child: SizedBox(
                                    height: 40,
                                    width: 40,
                                    child:
                                        Icon(Icons.timer, color: Colors.white),
                                  ))
                            ],
                          ),
                        ),
                      ),
                      myIndex == data.item.length - 1
                          ? Container(
                              child: RaisedButton(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                onPressed: () {
                                  showDialog<void>(
                                      context: context,
                                      builder: (context) {
                                        var contentText =
                                            'Apakah anda yakin untuk menyudahi pekerjaan soal, cek kembali dan pastikan tidak ada soal yang terlewati';
                                        return StatefulBuilder(
                                            builder:
                                                (context, setState) => Dialog(
                                                      child: Container(
                                                          height: width * .63,
                                                          width: width / 2.4,
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      width *
                                                                          .05,
                                                                  vertical:
                                                                      width *
                                                                          .05),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceEvenly,
                                                            children: [
                                                              Text(
                                                                  'Sudahi Latihan Soal',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          width /
                                                                              28,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w700)),
                                                              Text(
                                                                contentText,
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets
                                                                    .symmetric(
                                                                        horizontal:
                                                                            width *
                                                                                .01),
                                                                child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceEvenly,
                                                                    children: [
                                                                      Container(
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.circular(6),
                                                                          color:
                                                                              SiswamediaTheme.green,
                                                                        ),
                                                                        height:
                                                                            width /
                                                                                10,
                                                                        width:
                                                                            width /
                                                                                4,
                                                                        child:
                                                                            InkWell(
                                                                          onTap: () =>
                                                                              Navigator.pop(context),
                                                                          child:
                                                                              Center(
                                                                            child:
                                                                                Text('Tidak', style: TextStyle(color: Colors.white)),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        height:
                                                                            width /
                                                                                10,
                                                                        width:
                                                                            width /
                                                                                4,
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.circular(6),
                                                                          color:
                                                                              Color(0xff4B4B4B),
                                                                        ),
                                                                        child:
                                                                            InkWell(
                                                                          onTap:
                                                                              () {
                                                                            _timer.cancel();
                                                                            Navigator.push(
                                                                                context,
                                                                                MaterialPageRoute<void>(
                                                                                    builder: (_) => ResultPage(
                                                                                          currentAnswer: currentAnswer,
                                                                                          rightAnswer: correction,
                                                                                          duration: _timerValue,
                                                                                        )));
                                                                          },
                                                                          child:
                                                                              Center(
                                                                            child:
                                                                                Text('Ya', style: TextStyle(color: Colors.white)),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ]),
                                                              )
                                                            ],
                                                          )),
                                                    ));
                                      });
                                },
                                child: Text(
                                  'Finish',
                                  style: TextStyle(
                                      color: SiswamediaTheme.green,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            )
                          : Container(
                              child: RaisedButton(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                onPressed: () {
                                  setState(() {
                                    myIndex++;
                                  });
                                },
                                child: Text(
                                  'next',
                                  style: TextStyle(
                                      color: SiswamediaTheme.green,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class QuizState with ChangeNotifier {
  bool _progress;
  Map<String, dynamic> _selected = <String, dynamic>{};
  final PageController controller = PageController();

  bool get progress => _progress;
  Map<String, dynamic> get selected => _selected;

  set progress(bool Value) {
    _progress = Value;
    notifyListeners();
  }

  set selected(Map<String, dynamic> Value) {
    _selected = Value;
    notifyListeners();
  }

  void adding(String key, String value) {
    _selected[key] = value;
    notifyListeners();
  }

  void remove(String oldValue) {
    _selected.remove(oldValue);
    notifyListeners();
  }

  void nextPage() async {
    await controller.nextPage(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }
}
