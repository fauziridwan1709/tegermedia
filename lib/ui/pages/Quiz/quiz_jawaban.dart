part of '_quiz.dart';

class QuizJawaban extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var data = <QuizItem>[];
    return Scaffold(
      backgroundColor: SiswamediaTheme.background,
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.clear,
              color: SiswamediaTheme.green,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          'Kunci Jawaban',
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: S.w / 25,
              color: Colors.black),
        ),
      ),
      body: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .03),
        child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) => Container(
                  width: S.w * .9,
                  margin: EdgeInsets.symmetric(vertical: S.w * .02),
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${index + 1}  ${data[index].pertanyaan}'),
                      Text(
                        '${data[index].jawaban}',
                        style: TextStyle(
                            color: SiswamediaTheme.green,
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                )),
      ),
    );
  }
}
