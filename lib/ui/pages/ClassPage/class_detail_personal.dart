part of '_class_page.dart';

class ClassDetailPersonal extends StatefulWidget {
  final int id;
  ClassDetailPersonal({@required this.id});
  @override
  _ClassDetailPersonalState createState() => _ClassDetailPersonalState();
}

class _ClassDetailPersonalState extends State<ClassDetailPersonal> {
  List<DataListTeachers> teacher = [];

  String teacherUserId = '';
  ModelDetailClassId data;
  bool isLoading = true;
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    super.initState();
    _scaffoldKey = GlobalKey<ScaffoldState>();
    _getTeachers();
  }

  void _getTeachers() async {
    var dataTeachers =
        await TeacherServices.getListTeachers(classId: widget.id);
    if (dataTeachers.statusCode == 200) {
      setState(() {
        teacher = dataTeachers.data;
      });
    } else {
      setState(() {
        teacher = [];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Detail Personal'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: FutureBuilder<ModelDetailCourse>(
            future: ClassServices.detailCourse(id: widget.id),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Padding(
                  padding: EdgeInsets.only(top: S.w / 3),
                  child: Center(child: CircularProgressIndicator()),
                );
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data.statusCode == 200) {
                  var data = snapshot.data.data;

                  return ListView(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 24),
                        child: Column(
                          children: [
                            Text(
                              data.name,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20),
                            ),
                            SizedBox(height: 16),
                            Text(
                              'Wali Kelas : ${data.namaWalikelas}',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12),
                            ),
                            Text(
                              'Kontak : ${data.nomorTelpWalikelas}',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12),
                            ),
                            SizedBox(height: 16),
                            data.role != 'WALIKELAS'
                                ? SizedBox()
                                : InkWell(
                                    onTap: () {
                                      dialogDev(context);
                                    },
                                    child: Container(
                                        width: 200,
                                        height: 35,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                                color: Color(0xFFD9D9D9)),
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Center(
                                            child: Text('Ganti Wali Kelas')))),
                            SizedBox(height: 16),
                            Divider(),
                            SizedBox(height: 16),
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                        builder: (BuildContext context) =>
                                            ClassListByRefrence(
                                                id: data.id,
                                                role: 'GURU,WALIKELAS')),
                                  );
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 15),
                                  decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: SiswamediaTheme.grey
                                                .withOpacity(0.2),
                                            offset: const Offset(0.2, 0.2),
                                            blurRadius: 3.0),
                                      ],
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(
                                          color: Color(0xFF00000029))),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Guru Kelas (${data.totalGuru})',
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold)),
                                      ]),
                                )),
                            SizedBox(height: 15),
                            InkWell(
                                onTap: () {
                                  Navigator.push<void>(
                                    context,
                                    MaterialPageRoute<dynamic>(
                                      builder: (BuildContext context) =>
                                          ClassListByRefrence(
                                              id: data.id,
                                              role: 'SISWA',
                                              route: 'INSTITUSI'),
                                    ),
                                  );
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 15),
                                  decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: SiswamediaTheme.grey
                                                .withOpacity(0.2),
                                            offset: const Offset(0.2, 0.2),
                                            blurRadius: 3.0),
                                      ],
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(
                                          color: Color(0xFF00000029))),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Siswa Kelas (${data.totalSiswa})',
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold)),
                                      ]),
                                )),
                            SizedBox(height: 160),
                            data.role == 'SISWA'
                                ? SizedBox()
                                : Container(
                                    width: S.w,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        data.role != 'WALIKELAS'
                                            ? SizedBox()
                                            : Row(
                                                children: [
                                                  SizedBox(
                                                      width: 60,
                                                      child: Text('Guru')),
                                                  Expanded(
                                                      child: Container(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 10,
                                                            vertical: 5),
                                                    decoration: BoxDecoration(
                                                        color:
                                                            Color(0xFFECECEC),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(3),
                                                        border: Border.all(
                                                            color: Color(
                                                                0xFFECECEC))),
                                                    child: Text(
                                                        data
                                                            .invitationTeacherCode,
                                                        style: TextStyle(
                                                            color:
                                                                SiswamediaTheme
                                                                    .green)),
                                                  )),
                                                  GestureDetector(
                                                      onTap: () {
                                                        Clipboard.setData(
                                                            ClipboardData(
                                                                text: data
                                                                    .invitationTeacherCode));
                                                        CustomFlushBar
                                                            .eventFlushBar(
                                                                'Kode berhasil dicopy!',
                                                                context);
                                                      },
                                                      child: Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 15,
                                                                vertical: 5),
                                                        decoration: BoxDecoration(
                                                            color:
                                                                SiswamediaTheme
                                                                    .green,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        3)),
                                                        child: Text('Salin',
                                                            style: TextStyle(
                                                                color:
                                                                    SiswamediaTheme
                                                                        .white)),
                                                      )),
                                                ],
                                              ),
                                        SizedBox(height: 10),
                                        Column(
                                          children: [
                                            Row(
                                              children: [
                                                SizedBox(
                                                    width: 60,
                                                    child: Text('Siswa')),
                                                Expanded(
                                                    child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 10,
                                                      vertical: 5),
                                                  decoration: BoxDecoration(
                                                      color: Color(0xFFECECEC),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              3),
                                                      border: Border.all(
                                                          color: Color(
                                                              0xFFECECEC))),
                                                  child: Text(
                                                      data
                                                          .invitationStudentCode,
                                                      style: TextStyle(
                                                          color: SiswamediaTheme
                                                              .green)),
                                                )),
                                                GestureDetector(
                                                    onTap: () {
                                                      Clipboard.setData(
                                                          ClipboardData(
                                                              text: data
                                                                  .invitationStudentCode));

                                                      CustomFlushBar
                                                          .eventFlushBar(
                                                              'Salin', context);
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 15,
                                                              vertical: 5),
                                                      decoration: BoxDecoration(
                                                          color: SiswamediaTheme
                                                              .green,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(3)),
                                                      child: Text('Salin',
                                                          style: TextStyle(
                                                              color:
                                                                  SiswamediaTheme
                                                                      .white)),
                                                    )),
                                              ],
                                            ),
                                            SizedBox(height: 10),
                                          ],
                                        )
                                      ],
                                    )),
                            data.role == 'SISWA'
                                ? SizedBox()
                                : Text(
                                    'Kode diatas bisa digunakan untuk\nmengundang guru dan siswa menjadi bagian\ndari kelas secara instan, tekan tombol hijau\nuntuk menyalin kode',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Color(0xFFB9B9B9)),
                                  )
                          ],
                        ),
                      ),
                    ],
                  );
                } else {
                  return Container(
                      child: Center(child: Text('Terjadi Kesalahan...')));
                }
              } else {
                return Container(child: Text('Terjadi Kesalahan...'));
              }
            }));
  }

  void dialogDev(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              // String role = someCapitalizedString(data.role);
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      height: 230,
                      width: S.w * .5,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Ubah Wali Kelas',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                          SizedBox(
                            height: 16,
                          ),
                          DropdownButtonFormField<String>(
                            value: teacherUserId,
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconSize: 24,
                            elevation: 16,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 20),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(23.0),
                              ),
                            ),
                            style: TextStyle(color: Colors.deepPurple),
                            onChanged: (String newValue) {
                              setState(() {
                                teacherUserId = newValue;
                              });
                            },
                            items: [
                              DropdownMenuItem(
                                child: Text('Pilih Guru'),
                                value: '',
                              ),
                              for (DataListTeachers value in teacher)
                                DropdownMenuItem(
                                  value: value.userId.toString(),
                                  child: Text(value.fullName),
                                )
                            ],
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          InkWell(
                              onTap: () async {
                                // ignore: omit_local_variable_types
                                ResultCreateClassModel result =
                                    await ClassServices.updateWaliClass(
                                        reqBody: <String, int>{
                                      'user_id': int.parse(teacherUserId)
                                    },
                                        id: widget.id);
                                if (result.statusCode == 200) {
                                  Navigator.pop(context);
                                } else {
                                  CustomFlushBar.errorFlushBar(
                                    result.message,
                                    context,
                                  );
                                }
                              },
                              child: Container(
                                  width: S.w,
                                  height: 45,
                                  decoration: BoxDecoration(
                                      color: SiswamediaTheme.green,
                                      borderRadius: BorderRadius.circular(23)),
                                  child: Center(
                                      child: Text('Ganti Wali Kelas',
                                          style: TextStyle(
                                              color: SiswamediaTheme.white)))))
                        ],
                      )));
            }));
  }
}
