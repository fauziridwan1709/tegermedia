part of '_class_page.dart';

class CreateClassPersonal extends StatefulWidget {
  @override
  _CreateClassPersonalState createState() => _CreateClassPersonalState();
}

class _CreateClassPersonalState extends State<CreateClassPersonal> {
  TextEditingController name = TextEditingController();
  bool isLoading = false;
  var scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Buat Kelas'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: ListView(
          children: [
            SizedBox(height: isPortrait ? 205 : 10),
            Column(
              children: [
                Text('Nama Kelas Yang ingin anda buat',
                    textAlign: TextAlign.center),
                SizedBox(height: 18),
                Container(
                  height: 60,
                  width: S.w,
                  child: TextField(
                    controller: name,
                    style: semiBlack,
                    decoration: InputDecoration(
                      hintText: 'Nama Kelas...',
                      hintStyle: descBlack.copyWith(
                          fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffD8D8D8)),
                        borderRadius: BorderRadius.circular(S.w),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: SiswamediaTheme.green),
                        borderRadius: BorderRadius.circular(S.w),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: isPortrait ? 213 : 15),
                InkWell(
                  onTap: () async {
                    if (name.text == '') {
                      CustomFlushBar.errorFlushBar(
                        'Nama Kelas tidak boleh kosong!',
                        context,
                      );
                    } else {
                      confirmation(context);
                    }
                  },
                  child: Container(
                      width: S.w,
                      height: 45,
                      decoration: BoxDecoration(
                          color: SiswamediaTheme.green,
                          border: Border.all(color: SiswamediaTheme.green),
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                          child: isLoading
                              ? Container(
                                  height: 30,
                                  width: 30,
                                  child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.grey[300]),
                                      backgroundColor: Colors.white))
                              : Text('Lanjutkan',
                                  style: TextStyle(
                                      color: SiswamediaTheme.white)))),
                ),
                SizedBox(
                  height: 15,
                ),
                InkWell(
                    onTap: () {
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) =>
                              InvitationClassPersonal(
                            role: 'GURU',
                            type: 'PERSONAL',
                          ),
                        ),
                      );
                    },
                    child: Container(
                        width: S.w,
                        height: 45,
                        decoration: BoxDecoration(
                            color: SiswamediaTheme.white,
                            border: Border.all(color: SiswamediaTheme.green),
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                            child: Text('Gabung Kelas Lain',
                                style:
                                    TextStyle(color: SiswamediaTheme.green))))),
                SizedBox(
                  height: 24,
                ),
              ],
            ),
          ],
        ));
  }

  void confirmation(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              // String role = someCapitalizedString(data.role);
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      height: S.w * .7,
                      width: S.w * .8,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Konfirmasi',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16)),
                            Text(
                              'Apakah Anda yakin ingin menambahkan mata pelajaran ini ?',
                              style: TextStyle(fontSize: 16),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 35,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: InkWell(
                                        onTap: () async {
                                          setState(() {
                                            isLoading = true;
                                          });
                                          var result =
                                              await ClassServices.createClass(
                                                  name.text);

                                          print(result.data.id);
                                          if (result.statusCode == 200) {
                                            var course =
                                                await ClassServices.getMeCourse(
                                                    cache: false);
                                            //todo set me course
                                            dialog(context, result.data.id);
                                            setState(() {
                                              isLoading = false;
                                            });
                                          } else {
                                            setState(() {
                                              isLoading = false;
                                            });
                                            Flushbar<void>(
                                              margin: const EdgeInsets.all(8),
                                              leftBarIndicatorColor:
                                                  Colors.white,
                                              duration:
                                                  Duration(milliseconds: 2000),
                                              flushbarPosition:
                                                  FlushbarPosition.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFFFF5c83),
                                              message: result.message,
                                            )..show(context);
                                          }
                                        },
                                        child: Container(
                                            height: 45,
                                            decoration: BoxDecoration(
                                                color:
                                                    SiswamediaTheme.nearlyBlack,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Center(
                                                child: isLoading
                                                    ? Container(
                                                        height: 30,
                                                        width: 30,
                                                        child: CircularProgressIndicator(
                                                            valueColor:
                                                                AlwaysStoppedAnimation<Color>(
                                                                    Colors.grey[
                                                                        300]),
                                                            backgroundColor:
                                                                Colors.white))
                                                    : Text('Ya',
                                                        style: TextStyle(
                                                            color: SiswamediaTheme
                                                                .white)))))),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                    child: InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        child: Container(
                                            height: 45,
                                            decoration: BoxDecoration(
                                                color: SiswamediaTheme.green,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Center(
                                                child: Text('Tidak',
                                                    style: TextStyle(
                                                        color: SiswamediaTheme
                                                            .white))))))
                              ],
                            )
                          ])));
            }));
  }

  void dialog(BuildContext context, int id) {
    // var S.w = MediaQuery.of(context).size.width;
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      height: 233,
                      width: 250,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: S.w - 48 * 2,
                            height: 100,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/icons/kelas/confirmation.png'))),
                          ),
                          SizedBox(height: 15),
                          Text('Pendaftaran Berhasil',
                              style: TextStyle(fontSize: 14)),
                          SizedBox(height: 15),
                          InkWell(
                            onTap: () {
                              Navigator.popUntil(
                                  context, (route) => route.isFirst);
                            },
                            child: Container(
                                height: 35,
                                width: 105,
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.green,
                                    borderRadius: BorderRadius.circular(4)),
                                child: Center(
                                    child: Text('Yeay',
                                        style:
                                            TextStyle(color: Colors.white)))),
                          ),
                        ],
                      )));
            }));
  }
}
