part of '_class_page.dart';

class ClassListByRefrence extends StatefulWidget {
  final int id;
  final String role;
  final String route;

  ClassListByRefrence({this.id, this.role, this.route = 'PERSONAL'});

  @override
  _ClassListByRefrenceState createState() => _ClassListByRefrenceState();
}

class _ClassListByRefrenceState extends State<ClassListByRefrence> {
  bool isShowCode = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(
              label: 'Detail ${someCapitalizedString(widget.role)}'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                // if (widget.route == "PERSONAL") {
                //   Navigator.pushReplacement(
                //     context,
                //     MaterialPageRoute<void>(
                //       builder: (context) => ClassEmpty(),
                //     ),
                //   );
                // } else {
                Navigator.pop(context);
                // }
              }),
        ),
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ListView(
            children: <Widget>[ItemListRefrence(widget.id, widget.role)],
          ),
        ));
  }
}

class ItemListRefrence extends StatelessWidget {
  final int id;
  final String role;

  ItemListRefrence(this.id, this.role);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: FutureBuilder<ClassByRefrence>(
            future: ClassServices.getRefrenceByCourse(
              id,
              role,
            ),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var count = snapshot.data.data.length;
                if (count > 0) {
                  return GridView.count(
                      physics: ScrollPhysics(),
                      crossAxisCount: 1,
                      shrinkWrap: true,
                      childAspectRatio: 5,
                      scrollDirection: Axis.vertical,
                      children: List<Widget>.generate(
                        snapshot.data.data.length,
                        (int index) {
                          var data = snapshot.data.data[index];

                          return Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12)),
                              child: Container(
                                  height: 50,
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Colors.white),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,

                                    // mainAxisAlignment:
                                    //     MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: 40,
                                            height: 40,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: DecorationImage(
                                                  image:
                                                      NetworkImage(data.image),
                                                  fit: BoxFit.cover),
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                data.fullName,
                                                style: semiBlack.copyWith(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Text(
                                                data.role,
                                                style: semiBlack.copyWith(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      InkWell(
                                        onTap: () {
                                          dialog(context, data);
                                        },
                                        child: Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.grey),
                                            ),
                                            child: Text('Kontak')),
                                      )
                                    ],
                                  )));
                        },
                      ));
                } else {
                  return Center(child: Text('Data Masih Kosong...'));
                }
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }

  void dialog(BuildContext context, DataClassByRefrence data) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              var role = someCapitalizedString(data.role);
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      height: S.w * .95,
                      width: S.w * .5,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Kelas $role',
                            style: semiBlack.copyWith(
                                fontSize: 20, fontWeight: FontWeight.w700),
                          ),
                          Container(
                              width: 80,
                              height: 80,
                              margin: EdgeInsets.symmetric(vertical: 20),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(data.image)),
                              )),
                          Text(data.fullName,
                              style: semiBlack.copyWith(
                                  fontSize: 20, fontWeight: FontWeight.w500)),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6)),
                                  child: InkWell(
                                    onTap: () async {
                                      var email =
                                          'mailto:${data.email}?subject=News&body=Hai%20Siswamedia';
                                      if (await canLaunch(email)) {
                                        await launch(email);
                                      } else {
                                        throw 'Could not launch $email';
                                      }
                                    },
                                    child: Container(
                                      height: 50,
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 10),
                                      child: Center(
                                        child: Row(
                                          children: [
                                            Icon(Icons.email),
                                            SizedBox(width: 5),
                                            Text('Email', style: semiBlack),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )),
                              Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6)),
                                child: InkWell(
                                  onTap: () async {
                                    var telp = 'tel:${data.noTelp}';
                                    if (await canLaunch(telp)) {
                                      await launch(telp);
                                    } else {
                                      throw 'Could not launch $telp';
                                    }
                                  },
                                  child: Container(
                                    height: 50,
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 10),
                                    child: Center(
                                      child: Row(
                                        children: [
                                          Icon(Icons.phone),
                                          SizedBox(width: 5),
                                          Text('Telepon', style: semiBlack),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ))
                        ],
                      )));
            }));
  }
}
