part of '_class_page.dart';

class ClassMenu extends StatelessWidget {
  final List<String> data = [
    'Jadwal',
    'Tugas',
    'Ujian',
    'Conference',
    'Kelasku'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: S.w,
      height: S.w * .7,
      margin: EdgeInsets.symmetric(horizontal: S.w * .03),
      child: GridView.count(
          physics: ScrollPhysics(),
          crossAxisCount: 3,
          shrinkWrap: true,
          childAspectRatio: 1.6,
          scrollDirection: Axis.vertical,
          children: data
              .map((e) => Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    margin: EdgeInsets.symmetric(
                        horizontal: S.w * .01, vertical: S.w * .02),
                    child: Container(
                        height: S.w / 3,
                        padding: EdgeInsets.symmetric(
                            horizontal: S.w * .02, vertical: S.w * .02),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Center(
                                child: Text(
                              e,
                            )),
                          ],
                        )),
                  ))
              .toList()),
    );
  }
}

class Materi extends StatelessWidget {
  final List<String> student = ["paud", "tk", "sd", "smp", "sma", "smk"];
  final List<String> pilihan = ["kelas10", "kelas11", "kelas12"];
  final List<String> kurikulum = ["kurtilas", "ktsp"];
  final List<String> jurusan = ["bahagia", "ketawa yuk", "haha hihi huhu"];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: pilihan.length,
        itemBuilder: (context, index) {
          return Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12)),
              margin: EdgeInsets.only(
                left: index == 0 ? (width * .04) : (width * .01),
                right:
                    index == pilihan.length - 1 ? (width * .04) : (width * .01),
                top: width * .03,
                bottom: width * .03,
              ),
              child: Container(
                  height: 120,
                  width: width - 100,
                  padding: EdgeInsets.symmetric(
                    horizontal: width * .04,
                    vertical: width * .04,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Materi Belajar Hari Ini",
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  )));
        });
  }
}

class ShareClass extends StatelessWidget {
  const ShareClass({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        margin: EdgeInsets.symmetric(
            horizontal: width * .04, vertical: width * .04),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Selamat Datang di kelasku",
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            RaisedButton(
              elevation: 0,
              color: SiswamediaTheme.green,
              onPressed: () {},
              child: const Text('Bagikan Kode',
                  style: TextStyle(fontSize: 20, color: Colors.white)),
            ),
          ],
        ));
  }
}
