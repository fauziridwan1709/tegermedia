part of '_class_page.dart';

class SelectEntitasInstansi extends StatelessWidget {
  final bool isPersonal;

  SelectEntitasInstansi({this.isPersonal = false});

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Pilih Entitas'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              alignment: Alignment.center,
              width: S.w,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: isPortrait ? S.w * .3 : 0),
                  Text('Dalam Institusi ini sebagai apakah\nanda ?',
                      textAlign: TextAlign.center,
                      style: semiBlack.copyWith(fontSize: S.w / 28)),
                  SizedBox(height: 30),
                  InkWell(
                      onTap: () => context.push<void>(isPersonal
                          ? CreateClassInstansi(isPersonal: isPersonal)
                          : SelectMethodJoinClass(role: 'GURU')),
                      child: Container(
                          width: S.w * .8,
                          height: S.w * .13,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: SiswamediaTheme.green, width: 1.5),
                              borderRadius: BorderRadius.circular(S.w)),
                          child: Center(
                              child: Text('Guru',
                                  style: boldWhite.copyWith(
                                      fontSize: S.w / 28,
                                      color: SiswamediaTheme.green))))),
                  SizedBox(height: 20),
                  InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (BuildContext context) =>
                                InvitationClassInstansi(
                              role: 'SISWA',
                            ),
                          ),
                        );
                      },
                      child: Container(
                          width: S.w * .8,
                          height: S.w * .13,
                          decoration: BoxDecoration(
                              color: Color(0xff4B4B4B),
                              borderRadius: BorderRadius.circular(S.w)),
                          child: Center(
                              child: Text('Siswa',
                                  style: boldWhite.copyWith(
                                      fontSize: S.w / 28,
                                      color: Colors.white))))),
                  SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) =>
                              InvitationClassInstansi(
                            role: 'ORTU',
                          ),
                        ),
                      );
                    },
                    child: Container(
                        width: S.w * .8,
                        height: 45,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: SiswamediaTheme.green, width: 1.5),
                            color: SiswamediaTheme.green,
                            borderRadius: BorderRadius.circular(23)),
                        child: Center(
                            child: Text('Orang Tua/Wali Murid',
                                style: boldWhite.copyWith(
                                    fontSize: S.w / 28, color: Colors.white)))),
                  ),
                  SizedBox(height: 213),
                ],
              ),
            ),
          ),
        ));
  }
}

class SelectEntitasPersonal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Entitas Personal'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        body: ListView(
          children: [
            SizedBox(height: isPortrait ? 205 : 10),
            Column(
              children: [
                Text('Dalam kelas ini sebagai apakah\nanda ?',
                    textAlign: TextAlign.center),
                SizedBox(height: 24),
                InkWell(
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute<void>(
                          builder: (context) => CreateClassPersonal(),
                        ),
                      );
                    },
                    child: Container(
                        width: S.w,
                        height: 45,
                        decoration: BoxDecoration(
                            border: Border.all(color: SiswamediaTheme.green),
                            borderRadius: BorderRadius.circular(23)),
                        child: Center(
                            child: Text('Guru',
                                style:
                                    TextStyle(color: SiswamediaTheme.green))))),
                SizedBox(height: 16),
                InkWell(
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute<void>(
                          builder: (context) => InvitationClassPersonal(
                              role: 'SISWA', type: 'PERSONAL'),
                        ),
                      );
                    },
                    child: Container(
                        width: S.w,
                        height: 45,
                        decoration: BoxDecoration(
                            border: Border.all(color: SiswamediaTheme.green),
                            borderRadius: BorderRadius.circular(23)),
                        child: Center(
                            child: Text('Siswa',
                                style:
                                    TextStyle(color: SiswamediaTheme.green))))),
                SizedBox(height: 213),
                // GestureDetector(
                //     onTap: () {
                //       Navigator.pushReplacement(
                //         context,
                //         MaterialPageRoute<void>(builder: (context) => ClassEmpty()),
                //       );
                //     },
                //     child: Center(
                //         child: Text('Batalkan',
                //             style: TextStyle(
                //                 fontSize: 16, color: SiswamediaTheme.green))))
              ],
            ),
          ],
        ));
  }
}

class SelectMethodJoinClass extends StatelessWidget {
  final String role;
  SelectMethodJoinClass({@required this.role});
  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Daftar Kelasku'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              width: S.w,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: isPortrait ? S.w * .3 : 10),
                  Text(
                    'Pilih sekolah Anda melalui pilihan\ncara dibawah ini ',
                    textAlign: TextAlign.center,
                    style: semiBlack.copyWith(fontSize: S.w / 28),
                  ),
                  SizedBox(height: 30),
                  InkWell(
                      onTap: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute<void>(
                              builder: (context) =>
                                  InvitationClassInstansi(role: role)),
                        );
                      },
                      child: Container(
                          width: S.w * .8,
                          height: S.w * .13,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: SiswamediaTheme.green, width: 1.5),
                              borderRadius: BorderRadius.circular(S.w)),
                          child: Center(
                              child: Text('Gunakan Kode Kelas',
                                  style: boldWhite.copyWith(
                                      fontSize: S.w / 28,
                                      color: SiswamediaTheme.green))))),
                  SizedBox(height: 16),
                  InkWell(
                      onTap: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute<void>(
                            builder: (context) => JoinSchoolIsReady(),
                          ),
                        );
                      },
                      child: Container(
                          width: S.w * .8,
                          height: S.w * .13,
                          decoration: BoxDecoration(
                              color: Color(0xff4B4B4B),
                              borderRadius: BorderRadius.circular(S.w)),
                          child: Center(
                              child: Text('Cari sekolah / Institusi terdaftar',
                                  style: boldWhite.copyWith(
                                      fontSize: S.w / 28))))),
                  SizedBox(height: 16),
                  Text('Sekolah / Institusi Anda belum terdaftar ?',
                      style: semiBlack.copyWith(fontSize: S.w / 28)),
                  SizedBox(height: 3),
                  InkWell(
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute<void>(
                          builder: (context) => CreateClassInstansi(),
                        ),
                      );
                    },
                    child: Text('Daftar',
                        style: TextStyle(
                            color: SiswamediaTheme.green,
                            fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(height: 213),
                  // InkWell(
                  //     onTap: () {
                  //       Navigator.pushReplacement(
                  //         context,
                  //         MaterialPageRoute<void>(
                  //           builder: (context) => ClassEmpty(),
                  //         ),
                  //       );
                  //     },
                  //     child: Center(
                  //         child: Text('Batalkan',
                  //             style: TextStyle(
                  //                 fontSize: 16, color: SiswamediaTheme.green))))
                ],
              ),
            ),
          ),
        ));
  }
}
