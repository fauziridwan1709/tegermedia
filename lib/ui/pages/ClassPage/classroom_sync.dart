part of '_class_page.dart';

class ClassroomSync extends StatefulWidget {
  final GlobalKey<RefreshIndicatorState> rKey;
  ClassroomSync({this.rKey});

  @override
  _ClassroomSyncState createState() => _ClassroomSyncState();
}

class _ClassroomSyncState extends State<ClassroomSync> {
  final authState = Injector.getAsReactive<AuthState>();
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;
  ReactiveModel<ClassroomState> _classroomState;
  // final browser = ChromeSafariBrowser(bFallback: InAppBrowser(),);
  var profileState = Injector.getAsReactive<ProfileState>();
  var classState = Injector.getAsReactive<ClassState>();

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    _classroomState = Injector.getAsReactive<ClassroomState>();
    Initializer(
        reactiveModel: _classroomState,
        cacheKey: TIME_EDIT_ABSENSI,
        rIndicator: _refreshIndicatorKey,
        state: _classroomState.state.classroomCourse != null)
      .initialize();
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await _classroomState.setState(
          (s) async => await s.getClassroom(
              Email(email: profileState.state.profile.email, type: 'gmail')),
          catchError: true, onError: (context, dynamic error) {
        print((error as SiswamediaException).message);
      });
      // completer.complete();
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: SiswamediaAppBar(
        context: context,
        title: 'Sync Classroom',
      ),
      body: StateBuilder<ClassroomState>(
        observe: () => _classroomState,
        builder: (_, snapshot) {
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: retrieveData,
            child: WhenRebuilder<ClassroomState>(
              observe: () => _classroomState,
              onIdle: () => WaitingView(),
              onWaiting: () => WaitingView(),
              onError: (dynamic error) => ViewErrorClassroom(error: error),
              onData: (data) {
                print(data.classroomCourse);
                if (data.classroomCourse == null ||
                    data.classroomCourse.courses == null) {
                  return ListView(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (MediaQuery.of(context).orientation ==
                          Orientation.portrait)
                        SizedBox(height: width * .25),
                      Image.asset(
                        'assets/dialog/sorry_no_student.png',
                        width: width * 1,
                        height: width * .5,
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Tidak ada classroom terdaftar atau sudah ada yang tersingkron',
                        style: semiBlack.copyWith(fontSize: 14),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 40),
                    ],
                  );
                }
                return ListView(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .03),
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonSinkron(onTap: () async {
                          var firstDataCheckedUnComplete = _classroomState
                              .state.mapCourses.values
                              .where((element) => validate(element));
                          if (firstDataCheckedUnComplete.isNotEmpty) {
                            return CustomFlushBar.errorFlushBar(
                              'Lengkapi data pada ${firstDataCheckedUnComplete.first.name}, klik tombol edit',
                              context,
                            );
                          }
                          SiswamediaTheme.infoLoading(
                              context: context,
                              info: 'Sedang mengimport kelas');
                          _classroomState.state.mapCourses
                              .forEach((key, value) async {
                            var data = value;
                            var respTeachers =
                                await ClassroomServices.getAllTeachers(
                                    courseId: key);
                            var respStudents =
                                await ClassroomServices.getAllStudents(
                                    courseId: key);
                            var teachers = <String>[];
                            var students = <String>[];
                            if (respStudents.students != null) {
                              respStudents.students.forEach((element) {
                                students.add(element.profile.emailAddress);
                              });
                            }

                            if (respTeachers.teachers != null) {
                              respTeachers.teachers.forEach((element) {
                                if (profileState.state.profile.email !=
                                    element.profile.emailAddress) {
                                  teachers.add(element.profile.emailAddress);
                                }
                              });
                            }
                            data.studentList = students;
                            data.teacherList = teachers;

                            await ClassroomCreateServices
                                    .createClassFromClassroom(data)
                                .timeout(GLOBAL_TIMEOUT)
                                .then((value) {
                              if (value.statusCode == 200) {
                                Navigator.pop(context);
                                Navigator.pop(context);
                                Navigator.pop(context);
                                widget.rKey.currentState.show();
                                CustomFlushBar.successFlushBar(
                                  'Berhasil membuat kelas',
                                  context,
                                );
                              } else {
                                Navigator.pop(context);
                                CustomFlushBar.errorFlushBar(
                                  '${value.message}',
                                  context,
                                );
                              }
                            });
                          });
                        }),
                        Container(
                          height: S.w * .15,
                          width: S.w * .4,
                          child: CheckboxListTile(
                            onChanged: (bool value) {
                              _classroomState.setState((s) => s.switchSelectAll(
                                  authState.state.currentState.schoolId));
                            },
                            value: _classroomState.state.mapCourses.length ==
                                _classroomState
                                    .state.classroomCourse.courses.length,
                            title: Text(
                              'Select All',
                              style: descBlack.copyWith(fontSize: S.w / 30),
                            ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: data.classroomCourse.courses
                          .where((element) =>
                              classState.state.classMap.values
                                  .where((data) =>
                                      data.googleClassRoomId == element.id)
                                  .length !=
                              1)
                          .map((course) => CardClassroom(
                                course: course,
                                authState: authState,
                              ))
                          .toList(),
                    )
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }
}

bool validate(GoogleClassroomClass value) {
  return (value.tingkat == null ||
      value.jurusan == null ||
      value.tahunAjaran == null ||
      value.semester == null);
}

class CardClassroom extends StatelessWidget {
  final Courses course;
  final ReactiveModel<AuthState> authState;

  CardClassroom({@required this.course, this.authState})
      : assert(course != null);

  @override
  Widget build(BuildContext context) {
    var classroomState = Injector.getAsReactive<ClassroomState>();
    var data = classroomState.state.mapCourses[course.id];
    return Container(
      width: S.w * .9,
      padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .03),
      margin: EdgeInsets.symmetric(vertical: S.w * .02),
      decoration: BoxDecoration(
          borderRadius: radius(10),
          color: Colors.white,
          boxShadow: [SiswamediaTheme.shadowContainer]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(children: [
                CustomText(
                  course.name,
                  Kind.heading3,
                  align: TextAlign.left,
                ),
                SizedBox(width: 10),
                if (data != null)
                  Icon(
                      (validate(data))
                          ? Icons.info
                          : Icons.check_circle_rounded,
                      color: SiswamediaTheme.green),
              ]),
              Container(
                  decoration: BoxDecoration(
                      color: SiswamediaTheme.green, borderRadius: radius(4)),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        classroomState.setState((s) => s.select(
                            course, authState.state.currentState.schoolId));
                        navigate(context, DetailClass(id: course.id));
                      },
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 14.0, vertical: 3),
                        child: Center(
                          child: Text('Edit',
                              style: semiWhite.copyWith(fontSize: S.w / 32)),
                        ),
                      ),
                    ),
                  )
                  // Material(
                  //   // borderRadius: radius(S.w),
                  //   color: Colors.transparent,
                  //   child: InkWell(
                  //     borderRadius: radius(S.w),
                  //     onTap: () {
                  //       classroomState.setState(
                  //           (s) => s.select(course, auth.currentState.schoolId));
                  //       navigate(context, DetailClass(id: course.id));
                  //     },
                  //     child: Padding(
                  //         padding: EdgeInsets.all(8),
                  //         child: Icon(
                  //           Icons.edit,
                  //           size: 20,
                  //           color: SiswamediaTheme.white,
                  //         )),
                  //   ),
                  // ),
                  ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Id: ${course.id}',
                  style: semiBlack.copyWith(fontSize: S.w / 32)),
              Row(
                children: [
                  Checkbox(
                    value:
                        classroomState.state.mapCourses.containsKey(course.id),
                    onChanged: (bool value) {
                      classroomState.setState((s) => s.switchSelect(
                          course, authState.state.currentState.schoolId));
                    },
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ButtonSinkron extends StatelessWidget {
  final VoidCallback onTap;
  ButtonSinkron({this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: SiswamediaTheme.green,
        borderRadius: radius(12),
      ),
      child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: S.w * .05, vertical: S.w * .03),
            child: Center(
                child: Row(
              children: [
                Icon(Icons.sync, color: SiswamediaTheme.white),
                SizedBox(width: 10),
                Text(
                  'Singkron',
                  style: semiWhite.copyWith(fontSize: S.w / 26),
                ),
              ],
            )),
          )),
    );
  }
}

class ButtonAuthorize extends StatelessWidget {
  final VoidCallback onTap;
  ButtonAuthorize({this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: SiswamediaTheme.blue,
        borderRadius: radius(200),
      ),
      child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: S.w * .05, vertical: S.w * .025),
            child: Center(
                child: Text(
              'Authorize',
              style: semiWhite.copyWith(fontSize: S.w / 26),
            )),
          )),
    );
  }
}

class DetailClass extends StatefulWidget {
  final String id;

  DetailClass({this.id});

  @override
  _DetailClassState createState() => _DetailClassState();
}

class _DetailClassState extends State<DetailClass>
    with SingleTickerProviderStateMixin {
  ReactiveModel<ClassroomState> _classroomState;
  TabController _tabController;
  GlobalKey<FormState> _formKey;
  var className = TextEditingController();
  var jurusan = TextEditingController();
  var pelajaran = TextEditingController();
  GlobalKey<RefreshIndicatorState> rIKey;
  GlobalKey<RefreshIndicatorState> rIKey2;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    _formKey = GlobalKey<FormState>();
    _classroomState = Injector.getAsReactive<ClassroomState>();
    className.text = _classroomState.state.mapCourses[widget.id].name;
    jurusan.text = _classroomState.state.mapCourses[widget.id].jurusan;
    pelajaran.text = _classroomState.state.mapCourses[widget.id].courseName;
    rIKey = GlobalKey<RefreshIndicatorState>();
    rIKey2 = GlobalKey<RefreshIndicatorState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SiswamediaAppBar(
        context: context,
        title: 'Detail classroom',
        actions: [
          InkWell(
              onTap: () {
                if (_formKey.currentState.validate()) {
                  //TODO
                }
              },
              child: Text('Simpan'))
        ],
        bottom: TabBar(
          controller: _tabController,
          unselectedLabelColor: Colors.grey,
          labelColor: Colors.black,
          labelPadding: EdgeInsets.all(8),
          indicatorWeight: 4,
          tabs: <Widget>[Text('Info Class'), Text('Student'), Text('Teachers')],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          InfoClass(
            formKey: _formKey,
            className: className,
            jurusan: jurusan,
            courseId: widget.id,
            pelajaran: pelajaran,
            data: _classroomState.state.mapCourses[widget.id],
          ),
          GoogleClassroomStudentsList(
              courseId: widget.id, refreshIndicatorKey: rIKey),
          GoogleClassroomTeachersList(
              courseId: widget.id, refreshIndicatorKey: rIKey2)
        ],
      ),
    );
  }
}

class GoogleClassroomTeachersList extends StatelessWidget {
  final String courseId;
  final GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  GoogleClassroomTeachersList({this.courseId, this.refreshIndicatorKey});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var classroomState = Injector.getAsReactive<ClassroomState>();
    classroomState.setState((s) => s.getTeachers(courseId));
    return StateBuilder<ClassroomState>(
        observe: () => classroomState,
        builder: (_, snapshot) {
          // refreshIndicatorKey.currentState.show();
          return RefreshIndicator(
              key: refreshIndicatorKey,
              onRefresh: () async =>
                  classroomState.setState((s) => s.getTeachers(courseId)),
              child: WhenRebuilder<ClassroomState>(
                observe: () => classroomState,
                onIdle: () => LoadingView(),
                onWaiting: () => LoadingView(),
                onError: (dynamic error) => ErrorView(error: error),
                onData: (data) {
                  if (data.classroomTeachers.teachers.isEmpty) {
                    return ListView(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (MediaQuery.of(context).orientation ==
                            Orientation.portrait)
                          SizedBox(height: width * .25),
                        Image.asset(
                          'assets/dialog/sorry_no_student.png',
                          width: width * 1,
                          height: width * .5,
                        ),
                        SizedBox(height: 20),
                        Text(
                          'Empty',
                          style: semiBlack.copyWith(fontSize: 14),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 40),
                      ],
                    );
                  }
                  return ListView(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .03),
                    //TODO
                    children: data.classroomTeachers.teachers
                        .map((e) => TeacherCard(teacher: e))
                        .toList(),
                  );
                },
              ));
        });
  }
}

class LoadingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(
            height: 25, width: 25, child: CircularProgressIndicator()));
  }
}

class InfoClass extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final TextEditingController pelajaran;
  final TextEditingController className;
  final TextEditingController jurusan;
  final String courseId;
  final GoogleClassroomClass data;

  InfoClass(
      {this.courseId,
      this.formKey,
      this.pelajaran,
      this.className,
      this.jurusan,
      this.data});

  @override
  _InfoClassState createState() => _InfoClassState();
}

class _InfoClassState extends State<InfoClass> {
  String _simpleValidator(String value) {
    if (value.isEmpty) {
      return 'Tidak boleh kosong';
    }
    return null;
  }

  var classroomState = Injector.getAsReactive<ClassroomState>();
  var listSemester = <String>['I', 'II'];
  var kelas = List.generate(12, (index) => index);

  String _tahunAjaran;
  String _semester;
  String _tingkatKelas;

  @override
  void initState() {
    _tahunAjaran = widget.data.tahunAjaran ?? '';
    _semester = widget.data.semester ?? '';
    _tingkatKelas = widget.data.tingkat ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Form(
            key: widget.formKey,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: S.w * .05),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 20),
                    TextFormField(
                      validator: _simpleValidator,
                      controller: widget.className,
                      // focusNode: jurusanNode,
                      style: semiBlack.copyWith(fontSize: S.w / 30),
                      decoration: fullRadiusField(context,
                          hint: 'Class Name', label: true),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    DropdownButtonFormField<String>(
                      value: _tahunAjaran,
                      icon: Icon(Icons.keyboard_arrow_down,
                          color: SiswamediaTheme.green),
                      iconSize: 24,
                      elevation: 16,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffD8D8D8)),
                          borderRadius: BorderRadius.circular(S.w),
                        ),
                        labelText: 'Tahun Ajaran',
                        labelStyle: hintText(context),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: SiswamediaTheme.green),
                          borderRadius: BorderRadius.circular(S.w),
                        ),
                      ),
                      style: descBlack.copyWith(
                          fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                      onChanged: (String newValue) {
                        setState(() => _tahunAjaran = newValue);

                        classroomState.setState((s) =>
                            s.changeTahunAjaran(widget.courseId, newValue));
                      },
                      items: [
                        DropdownMenuItem(
                          child: Text('Pilih Tahun Ajaran'),
                          value: '',
                        ),
                        for (String value in listTahunAjaran())
                          DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          )
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    DropdownButtonFormField<String>(
                      value: _semester,
                      icon: Icon(Icons.keyboard_arrow_down,
                          color: SiswamediaTheme.green),
                      iconSize: 24,
                      elevation: 16,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffD8D8D8)),
                          borderRadius: BorderRadius.circular(S.w),
                        ),
                        labelText: 'Semester',
                        labelStyle: hintText(context),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: SiswamediaTheme.green),
                          borderRadius: BorderRadius.circular(S.w),
                        ),
                      ),
                      style: descBlack.copyWith(
                          fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                      onChanged: (String newValue) {
                        setState(() => _semester = newValue);
                        classroomState.setState(
                            (s) => s.changeSemester(widget.courseId, newValue));
                      },
                      items: [
                        DropdownMenuItem(
                          child: Text('Pilih Semester'),
                          value: '',
                        ),
                        for (String value in listSemester)
                          DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          )
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    DropdownButtonFormField<String>(
                      value: _tingkatKelas,
                      icon: Icon(Icons.keyboard_arrow_down,
                          color: SiswamediaTheme.green),
                      iconSize: 24,
                      elevation: 16,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffD8D8D8)),
                          borderRadius: BorderRadius.circular(S.w),
                        ),
                        labelText: 'Tingkat kelas',
                        labelStyle: hintText(context),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: SiswamediaTheme.green),
                          borderRadius: BorderRadius.circular(S.w),
                        ),
                      ),
                      style: descBlack.copyWith(
                          fontSize: S.w / 30, color: Color(0xff4B4B4B)),
                      onChanged: (String newValue) {
                        setState(() => _tingkatKelas = newValue);
                        classroomState.setState(
                            (s) => s.changeTingkat(widget.courseId, newValue));
                      },
                      items: [
                        DropdownMenuItem(
                          child: Text('Pilih Tingkat Kelas'),
                          value: '',
                        ),
                        for (int value in kelas)
                          DropdownMenuItem(
                            value: (value + 1).toString(),
                            child: Text((value + 1).toString()),
                          )
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    TextFormField(
                      validator: _simpleValidator,
                      controller: widget.jurusan,
                      // focusNode: jurusanNode,
                      onChanged: (newValue) => classroomState.setState(
                          (s) => s.changeJurusan(widget.courseId, newValue)),
                      style: semiBlack.copyWith(fontSize: S.w / 30),
                      decoration: fullRadiusField(context,
                          hint: 'Jurusan', label: true),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    TextFormField(
                      validator: _simpleValidator,
                      controller: widget.pelajaran,
                      // focusNode: jurusanNode,
                      onChanged: (newValue) => classroomState.setState(
                          (s) => s.changePelajaran(widget.courseId, newValue)),
                      style: semiBlack.copyWith(fontSize: S.w / 30),
                      decoration: fullRadiusField(context,
                          hint: 'Mata Pelajaran', label: true),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                  ]),
            )),
      ],
    );
  }
}

class GoogleClassroomStudentsList extends StatelessWidget {
  final String courseId;
  final GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  GoogleClassroomStudentsList({this.courseId, this.refreshIndicatorKey});
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var classroomState = Injector.getAsReactive<ClassroomState>();
    classroomState.setState((s) => s.getStudents(courseId));
    return StateBuilder<ClassroomState>(
        observe: () => classroomState,
        builder: (_, snapshot) {
          // refreshIndicatorKey.currentState.show();
          return RefreshIndicator(
              key: refreshIndicatorKey,
              onRefresh: () async =>
                  classroomState.setState((s) => s.getStudents(courseId)),
              child: WhenRebuilder<ClassroomState>(
                observe: () => classroomState,
                onIdle: () => LoadingView(),
                onWaiting: () => LoadingView(),
                onError: (dynamic error) => ErrorView(error: error),
                onData: (data) {
                  if (data.classroomStudents == null ||
                      data.classroomStudents.students == null) {
                    return ListView(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (MediaQuery.of(context).orientation ==
                            Orientation.portrait)
                          SizedBox(height: width * .25),
                        Image.asset(
                          'assets/dialog/sorry_no_student.png',
                          width: width * 1,
                          height: width * .5,
                        ),
                        SizedBox(height: 20),
                        Text(
                          'Empty',
                          style: semiBlack.copyWith(fontSize: 14),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 40),
                      ],
                    );
                  }
                  return ListView(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .03),
                    //TODO
                    children: data.classroomStudents.students
                        .map((e) => StudentCard(student: e))
                        .toList(),
                  );
                },
              ));
        });
  }
}

class TeacherCard extends StatelessWidget {
  final GoogleClassroomTeachers teacher;

  TeacherCard({this.teacher});
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: Colors.transparent,
        backgroundImage: NetworkImage(teacher.profile.photoUrl),
      ),
      title: Text(
        teacher.profile.emailAddress,
        style: semiBlack.copyWith(fontSize: S.w / 28),
      ),
    );
  }
}

class StudentCard extends StatelessWidget {
  final GoogleClassroomStudents student;

  StudentCard({this.student});
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: Colors.transparent,
        backgroundImage: NetworkImage(student.profile.photoUrl),
      ),
      title: Text(
        student.profile.emailAddress,
        style: semiBlack.copyWith(fontSize: S.w / 28),
      ),
    );
  }
}
