part of '_class_page.dart';

class ListClassOnlyme extends StatefulWidget {
  final int id;
  final List<String> schoolRole;
  final bool stateRouteRegister;
  ListClassOnlyme({@required this.id, this.schoolRole, this.stateRouteRegister = false});
  @override
  _ListClassOnlymeState createState() => _ListClassOnlymeState();
}

class _ListClassOnlymeState extends BaseStateReBuilder<ListClassOnlyme, ClassState> {
  @override
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final authState = Injector.getAsReactive<AuthState>();
  final classState = Injector.getAsReactive<ClassState>();
  final dialogKey = GlobalKey<ScaffoldState>();

  bool _visible;

  int _count = 0;

  @override
  void initState() {
    super.initState();
    _visible = true;
    _getDetailSchool();
    _getCountWaitingApproval();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _getDetailSchool() async {
    var detailSchool = await ClassServices.detailSchool(id: authState.state.currentState.schoolId);
    if (detailSchool.statusCode == 200) {
      if (authState.state.currentState.schoolRole != null &&
          authState.state.currentState.schoolRole.isNotEmpty) {
        var data = UserCurrentState(
          schoolId: detailSchool.data.id,
          schoolRole: detailSchool.data.roles,
          schoolName: detailSchool.data.name,
          type: authState.state.currentState.type,
          className: authState.state.currentState.className,
          classRole: authState.state.currentState.classRole,
          classId: authState.state.currentState.classId,
          isAdmin: authState.state.currentState.isAdmin,
        );
        await authState.setState((s) => s.changeCurrentState(state: data), silent: true);
      }
    }
  }

  void _getCountWaitingApproval() async {
    var result =
        await SchoolServices.countWaitingApproval(schoolID: authState.state.currentState.schoolId);

    if (result.statusCode == 200) {
      setState(() {
        _count = result.data.count;
      });
    } else {
      setState(() {
        _count = 0;
      });
    }
  }

  @override
  Future<void> retrieveData() async {
    var data = ClassBySchoolModel(param: GetClassModel(schoold: widget.id, me: true));
    if (classState.isWaiting) {
      setState(() => _visible = false);
    }
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await classState.setState((s) async => await s.retrieveData(data: data),
          onData: (context, data) => setState(
                () => _visible = false,
              ),
          onRebuildState: (_) => setState(() => _visible = true),
          catchError: true,
          onError: (context, dynamic error) {});
      // completer.complete();
    });
  }

  @override
  Widget build(BuildContext context) {
    var role = widget.schoolRole;
    var id = widget.id;
    return Injector(
        inject: [
          Inject(() => ClassroomCredentialState()),
        ],
        builder: (_) {
          return WillPopScope(
              onWillPop: () {
                Navigator.of(context).pop();
                return;
              },
              child: Scaffold(
                  key: dialogKey,
                  appBar: AppBar(
                    // backgroundColor: Colors.white,
                    shadowColor: Colors.transparent,
                    centerTitle: true,
                    title: Text(
                      'Daftar Kelas',
                      style: boldBlack.copyWith(fontSize: S.w / 22, color: Colors.white),
                    ),
                    flexibleSpace: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [SiswamediaTheme.materialGreen[100], SiswamediaTheme.green],
                          begin: Alignment.topCenter,
                          end: Alignment(0, 1.5),
                        ),
                      ),
                    ),
                    leading: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                  ),
                  body: StateBuilder<ClassState>(
                    observe: () => classState,
                    tag: ['tago'],
                    builder: (_, snapshot) {
                      return RefreshIndicator(
                          key: refreshIndicatorKey,
                          onRefresh: () async {
                            await retrieveData();
                          },
                          child: WhenRebuilder<ClassState>(
                            observe: () => snapshot,
                            onWaiting: () => WaitingView(),
                            onError: (dynamic error) => ErrorView(error: error),
                            onIdle: () => WaitingView(),
                            onData: (data) {
                              // if (data.classMap.isEmpty) {
                              //   return AnimatedOpacity(
                              //     opacity: _visible ? 1.0 : 0.0,
                              //     duration: Duration(milliseconds: 300),
                              //     child: ListView(children: [
                              //       Container(
                              //         margin: EdgeInsets.only(top: 16),
                              //         child: ClassNotFound(
                              //             label:
                              //                 '${authState.state.currentState.isAdmin ? '\nSekolah ini belum memiliki kelas, Silahkan tambah kelas kemudian Undang Guru dan Siswa' : 'Anda belum memiliki kelas'}'),
                              //       ),
                              //       role.contains('ORTU')
                              //           ? SizedBox()
                              //           : ButtonWidthCondition(
                              //               role: role,
                              //               authState: authState,
                              //               id: id,
                              //               rKey: refreshIndicatorKey,
                              //               count: _count,
                              //               refreshCount: () =>
                              //                   _getCountWaitingApproval())
                              //     ]),
                              //   );
                              // }
                              return AnimatedOpacity(
                                opacity: _visible ? 1.0 : 0.0,
                                duration: Duration(milliseconds: 450),
                                child: ListView(
                                  children: [
                                    Column(children: [
                                      data.classMap.isEmpty
                                          ? Container(
                                              margin: EdgeInsets.only(top: 16),
                                              child: ClassNotFound(
                                                  label:
                                                      '${authState.state.currentState.isAdmin ? '\n${authState.state.currentState.type == 'PERSONAL' ? 'Media' : 'Sekolah'} ini belum memiliki kelas, Silahkan tambah kelas ${authState.state.currentState.type == 'PERSONAL' ? 'kemudian Undang Siswa' : 'kemudian Undang Guru dan Siswa'}' : 'Anda belum memiliki kelas'}'),
                                            )
                                          : Container(
                                              margin: EdgeInsets.only(top: 16),
                                              child: ListClass(
                                                data: classState.state.classMap.values.toList(),
                                              ),
                                            ),
                                      // role.contains('ORTU')
                                      //     ? SizedBox()
                                      //     :
                                      ButtonWidthCondition(
                                          role: role,
                                          authState: authState,
                                          id: id,
                                          dialogKey: dialogKey,
                                          rKey: refreshIndicatorKey,
                                          count: _count,
                                          refreshCount: () => _getCountWaitingApproval())
                                    ]),
                                    SizedBox(height: 40),
                                  ],
                                ),
                              );
                            },
                          )
                          // child: LayoutBuilder(builder: (context, _) {
                          //   print('aduh ${classState.hasError}');
                          //   if (snapshot.isWaiting) {
                          //     return WaitingView();
                          //   } else if (snapshot.hasError) {
                          //     return ErrorView(
                          //         error: (snapshot.error is SiswamediaException));
                          //   } else if (snapshot.state.classModel == null ||
                          //       snapshot.state.classModel.data.isEmpty) {
                          //     if (completer.isCompleted) {
                          //       return ListView(children: [
                          //         Container(
                          //           margin: EdgeInsets.only(top: 16),
                          //           child: DataNotFound(
                          //               label: 'Anda belum memiliki kelas'),
                          //         ),
                          //         role.contains('ORTU')
                          //             ? SizedBox()
                          //             : ButtonWidthCondition(
                          //                 role: role,
                          //                 auth: auth,
                          //                 id: id,
                          //                 count: _count,
                          //                 refreshCount: () =>
                          //                     _getCountWaitingApproval())
                          //       ]);
                          //     }
                          //     return WaitingView();
                          //   }
                          //   return ListView(
                          //     children: [
                          //       Column(children: [
                          //         Container(
                          //           margin: EdgeInsets.only(top: 16),
                          //           child: ListClass(
                          //             data: classState.state.classModel.data,
                          //           ),
                          //         ),
                          //         role.contains('ORTU')
                          //             ? SizedBox()
                          //             : ButtonWidthCondition(
                          //                 role: role,
                          //                 auth: auth,
                          //                 id: id,
                          //                 count: _count,
                          //                 refreshCount: () =>
                          //                     _getCountWaitingApproval())
                          //       ]),
                          //       SizedBox(height: 40),
                          //     ],
                          //   );
                          // }),
                          );
                    },
                  )));
        });
  }
}

class ButtonWidthCondition extends StatelessWidget {
  const ButtonWidthCondition({
    Key key,
    @required this.role,
    @required this.authState,
    @required this.id,
    this.refreshCount,
    this.count,
    this.rKey,
    this.dialogKey,
  }) : super(key: key);

  final List<String> role;
  final ReactiveModel<AuthState> authState;
  final int id;
  final int count;
  final Function refreshCount;
  final GlobalKey<RefreshIndicatorState> rKey;
  final GlobalKey<ScaffoldState> dialogKey;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
          margin: EdgeInsets.only(left: 25, right: 25, bottom: 10, top: 10), child: Divider()),
      if (authState.state.currentState.isAdmin && authState.state.currentState.type != 'PERSONAL')
        ButtonAction(
            label: 'Verifikasi Guru',
            subLabel: 'Menunggu Persetujuan : $count',
            onTap: () {
              Navigator.push<dynamic>(
                context,
                MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) => ListTeachersWaitingApproval(
                          schoolId: id,
                          refreshCount: () => refreshCount(),
                        )),
              );
            },
            icon: 'assets/icons/kelas/add.png'),
      if (authState.state.currentState.isAdmin)
        ButtonAction(
          label: 'Tambah Kelas',
          icon: 'assets/icons/kelas/add.png',
          onTap: () {
            //TODO uncomment dont delete this
            showDialog<void>(
                context: dialogKey.currentContext,
                builder: (context) {
                  return Dialog(
                      shape: RoundedRectangleBorder(borderRadius: radius(12)),
                      child: Padding(
                        padding: EdgeInsets.all(14),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CustomText(
                              'Ingin menambah kelas dari google classroom?',
                              Kind.heading3,
                              align: TextAlign.center,
                            ),
                            SizedBox(height: 20),
                            ButtonDialogTambah(
                                icon: Icons.sync,
                                onTap: () => popAndNavigate(context, ClassroomSync(rKey: rKey)),
                                label: 'Sync Google Classroom'),
                            SizedBox(height: 14),
                            ButtonDialogTambah(
                                icon: Icons.add,
                                onTap: () =>
                                    popAndNavigate(context, CreateClassBySchool(schoolId: id)),
                                label: 'Tambah Kelas Baru'),
                            SizedBox(height: 14),
                          ],
                        ),
                      ));
                });
            // navigate(context, CreateClassBySchool(schoolId: id));
          },
        ),
      if (authState.state.currentState.type != 'PERSONAL')
        ButtonAction(
          label: 'Join Kelas',
          icon: 'assets/icons/kelas/join.png',
          onTap: () {
            showDialog<void>(
                context: context,
                builder: (context) {
                  return Dialog(
                    shape: RoundedRectangleBorder(borderRadius: radius(12)),
                    child: Container(
                      width: S.w * .8,
                      padding: EdgeInsets.all(S.w * .025),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CustomText('Pilih Role', Kind.heading1),
                          CustomText('Role yang dipilih untuk join kelas baru', Kind.descBlack),
                          SizedBox(height: S.w * .05),
                          Row(
                            children: [
                              Expanded(
                                child: RaisedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    Navigator.push<dynamic>(
                                      context,
                                      MaterialPageRoute<dynamic>(
                                          builder: (BuildContext context) =>
                                              InvitationClassInstansi(
                                                role: 'SISWA',
                                                navigateFromProfile: false,
                                              )),
                                    );
                                  },
                                  color: SiswamediaTheme.green,
                                  child: Center(
                                      child: Text(
                                    'SISWA',
                                    style: semiWhite.copyWith(fontSize: S.w / 30),
                                  )),
                                  shape: RoundedRectangleBorder(borderRadius: radius(200)),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: RaisedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    Navigator.push<dynamic>(
                                      context,
                                      MaterialPageRoute<dynamic>(
                                          builder: (BuildContext context) =>
                                              InvitationClassInstansi(
                                                role: 'GURU',
                                                navigateFromProfile: false,
                                              )),
                                    );
                                  },
                                  color: SiswamediaTheme.lightBlack,
                                  child: Center(
                                      child: Text(
                                    'GURU',
                                    style: semiWhite.copyWith(fontSize: S.w / 30),
                                  )),
                                  shape: RoundedRectangleBorder(borderRadius: radius(200)),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                });
          },
        ),
    ]);
  }
}

class ListClass extends StatelessWidget {
  final String tipe;
  final List<ClassBySchoolData> data;
  final VoidCallback onTap;

  ListClass({this.data, this.tipe, this.onTap});

  final className = TextEditingController();
  final classDescription = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var authState = Injector.getAsReactive<AuthState>();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        children: [
          if (authState.state.currentState.isAdmin) AdminLabel(),
          Image.asset('assets/images/asset_kelas/kelas.png', height: S.w * .6, width: S.w * .7),
          Divider(color: Colors.grey),
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 15),
            child: Text(
              'Daftar Kelas saya',
              style: descBlack.copyWith(color: Colors.black, fontSize: S.w / 26),
              textAlign: TextAlign.center,
            ),
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: data
                  .map((e) => Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child: InkWell(
                            onTap: tipe == 'change'
                                ? () async {
                                    var schoolRole = authState.state.currentState.schoolRole;
                                    vUtils.setLog(schoolRole.isEmpty);
                                    SiswamediaTheme.infoLoading(
                                        context: context, info: 'Sedang mengganti kelas');
                                    var data = UserCurrentState(
                                        className: e.name,
                                        classId: e.id,
                                        classRole: e.role,
                                        isAdmin: authState.state.currentState.isAdmin,
                                        type: authState.state.currentState.type,
                                        schoolName: authState.state.currentState.schoolName,
                                        schoolRole: schoolRole.isEmpty ? [e.role] : schoolRole,
                                        schoolId: authState.state.currentState.schoolId);
                                    var result = await ClassServices.setCurrentState(
                                        data: json.encode(data.toJson()));
                                    authState.state.changeCurrentState(state: data);

                                    var materi = MateriDetail(
                                        jenjang: e.tingkat.contains('SMA')
                                            ? e.tingkat
                                            : generateJenjang(e.tingkat),
                                        kurikulum: '2013',
                                        kelas: 'kelas ' + e.tingkat,
                                        pelajaran: null,
                                        pengguna: e.role == 'SISWA' ? 'SISWA' : 'GURU');
                                    if (e.role == 'ORTU') {
                                      //todo penting
                                      await authState
                                          .setState((s) => s.setListAnak(e.studentIdList));
                                    }

                                    if (result.statusCode != 200) {
                                      Navigator.pop(context);
                                      CustomFlushBar.errorFlushBar(
                                        result.message,
                                        context,
                                      );
                                    } else {
                                      //todo remove listStudent
                                      await GlobalState.materi().state.setMateri(materi);

                                      //todo penting
                                      await authState.setState((s) => s.setCurrentClass(
                                          className: e.name, classId: e.id, classRole: e.role));
                                      GlobalState.materi().notify();
                                      GlobalState.auth().notify();
                                      Navigator.pop(context);
                                      Utils.log(e.role, info: 'class role');
                                      CustomFlushBar.successFlushBar(
                                        'Mengganti ke kelas ${e.name}',
                                        context,
                                      );
                                      Cleaner()..cleanState();
                                    }
                                  }
                                : () {
                                    if (!e.isApprove && !authState.state.currentState.isAdmin) {
                                      dialogDev(context,
                                          title: 'Konfirmasi',
                                          desc:
                                              'Mohon Maaf anda belum di verifikasi oleh admin sekolah!');
                                    } else {
                                      authState.setState((s) => s.setSelectedClassRole(e.role));
                                      GlobalState.detailClass()
                                          .setState((s) => s.setId(e.id), silent: true);
                                      navigate(context, DetailClassInstansi(id: e.id),
                                          settings: RouteSettings(name: '/detail-class'));
                                    }
                                  },
                            child: buildContainerItem(context, e)),
                      ))
                  .toList()),
        ],
      ),
    );
  }

  Stack buildContainerItem(BuildContext context, ClassBySchoolData meClass) {
    var authState = Injector.getAsReactive<AuthState>();
    return Stack(children: [
      StateBuilder<AuthState>(
          observe: () => authState,
          builder: (context, _) {
            return Container(
              width: S.w,
              padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .035),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                      color: authState.state.currentState.classId == meClass.id
                          ? SiswamediaTheme.green
                          : Color(0xffCFCFCF))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Icon(
                        CustomIcons.kelas,
                        color: authState.state.currentState.classId == meClass.id
                            ? SiswamediaTheme.green
                            : Colors.black87,
                      ),
                      SizedBox(width: 15),
                      Expanded(
                        child: Text(meClass.name,
                            style: TextStyle(
                                fontSize: S.w / 26,
                                fontWeight: FontWeight.w800,
                                color: authState.state.currentState.classId == meClass.id
                                    ? SiswamediaTheme.green
                                    : Colors.black87)),
                      ),
                      SizedBox(width: 20),
                      if (meClass.role.isNotEmpty)
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 6, vertical: 5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(color: Color(0xffCFCFCF), width: 1),
                                color: SiswamediaTheme.materialGreen[100].withOpacity(.2)),
                            child: Text('${meClass.role}', style: TextStyle(fontSize: S.w / 36))),
                    ]),
                  ),
                  SizedBox(width: 10),
                  if ((meClass.role == 'WALIKELAS' || authState.state.currentState.isAdmin) &&
                      tipe != 'change')
                    InkWell(
                        onTap: () {
                          className.text = meClass.name;
                          classDescription.text = meClass.description;
                          print('description');
                          print(meClass.description);
                          dialog(context, meClass);
                        },
                        child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            decoration: BoxDecoration(
                                color: SiswamediaTheme.green,
                                borderRadius: BorderRadius.circular(100)),
                            child: Icon(Icons.edit, color: Colors.white)))
                ],
              ),
            );
          }),
      Positioned(
          top: 0,
          right: 0,
          child: meClass.isApprove || authState.state.currentState.isAdmin
              ? SizedBox()
              : Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width * 0.4,
                  decoration: BoxDecoration(
                      color: Colors.red[700],
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30.0), topRight: Radius.circular(10))),
                  child: Center(
                      child: Text('Belum Verifikasi', style: TextStyle(color: Colors.white))),
                )),
    ]);
  }

  void dialog(BuildContext context, ClassBySchoolData detailClass) {
    var classState = Injector.getAsReactive<ClassState>();
    showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (_) =>
            StatefulBuilder(builder: (BuildContext _, void Function(void Function()) setState) {
              return Center(
                child: SingleChildScrollView(
                  child: Dialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                      child: Container(
                          width: S.w,
                          padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .05),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText('Kelasku', Kind.heading1),
                              SizedBox(
                                height: 10,
                              ),
                              CustomText('Ubah atau hapus kelas', Kind.descBlack,
                                  align: TextAlign.center),
                              SizedBox(
                                height: 10,
                              ),

                              // DropdownButtonFormField<String>(
                              //   value: teacherUserId,
                              //   icon: Icon(Icons.keyboard_arrow_down),
                              //   iconSize: 24,
                              //   elevation: 16,
                              //   decoration: fullRadiusField(context,
                              //       hint: 'Pilih Guru'),
                              //   style: normalText(context),
                              //   onChanged: (String newValue) {
                              //     setState(() {});
                              //   },
                              //   items: [
                              //     DropdownMenuItem(
                              //       child: Text('Pilih Guru',
                              //           style: hintText(context)),
                              //       value: '',
                              //     ),
                              //     // for (DataListTeachers value in teacher)
                              //     //   DropdownMenuItem(
                              //     //     value: value.userId.toString(),
                              //     //     child: Text(
                              //     //       value.fullName,
                              //     //       style: normalText(context),
                              //     //     ),
                              //     //   )
                              //   ],
                              // ),
                              SizedBox(
                                height: 16,
                              ),
                              TextField(
                                controller: className,
                                style: normalText(context),
                                decoration: fullRadiusField(context, hint: 'Nama Kelas'),
                              ),
                              SizedBox(height: 16),
                              TextField(
                                controller: classDescription,
                                style: normalText(context),
                                maxLines: 4,
                                minLines: 3,
                                decoration: InputDecoration(
                                  hintText: 'Deskripsi kelas',
                                  hintStyle: descBlack.copyWith(
                                      fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: SiswamediaTheme.green),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                ),
                              ),
                              SizedBox(height: 16),
                              InkWell(
                                  onTap: () async {
                                    print('ontap description');
                                    print(classDescription.text);
                                    detailClass.name = className.text;
                                    detailClass.description = classDescription.text;
                                    var data =
                                        ClassBySchoolModel(cudModel: CUDClassModel(detailClass));
                                    await GlobalState.classes().state.updateData(context, data);
                                    GlobalState.detailClass()
                                        .state
                                        .detailClassMap
                                        .remove(detailClass.id);
                                    await GlobalState.classes().notify();
                                    // setState(
                                    //     (s) => s.updateData(context, data));
                                  },
                                  child: Container(
                                      width: S.w,
                                      height: 45,
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius: BorderRadius.circular(23)),
                                      child: Center(
                                          child: Text('Simpan',
                                              style: TextStyle(
                                                  fontSize: S.w / 30,
                                                  fontWeight: FontWeight.w600,
                                                  color: SiswamediaTheme.white))))),
                              SizedBox(height: 10),
                              InkWell(
                                  onTap: () async {
                                    await GlobalState.classes()
                                        .state
                                        .deleteData(context, detailClass.id);

                                    await GlobalState.classes().notify();
                                  },
                                  child: Container(
                                      width: S.w,
                                      height: 45,
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.nearlyBlack,
                                          borderRadius: BorderRadius.circular(23)),
                                      child: Center(
                                          child: Text('Hapus',
                                              style: TextStyle(
                                                  fontSize: S.w / 30,
                                                  fontWeight: FontWeight.w600,
                                                  color: SiswamediaTheme.white)))))
                            ],
                          ))),
                ),
              );
            }));
  }
}
