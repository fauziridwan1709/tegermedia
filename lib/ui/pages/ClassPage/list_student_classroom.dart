part of '_class_page.dart';

class ListStudentClassroom extends StatefulWidget {
  final int id;

  ListStudentClassroom({this.id});

  @override
  _ListStudentClassroomState createState() => _ListStudentClassroomState();
}

class _ListStudentClassroomState extends State<ListStudentClassroom> {
  final authState = Injector.getAsReactive<AuthState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var showBottom = 20.0;
  var isShowCode = false;
  var isShowBottom = true;
  var isScrollingDown = false;
  final ScrollController scrollController = ScrollController();

  var searchController = TextEditingController();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  var studentState = Injector.getAsReactive<StudentState>();

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(TIME_LIST_CLASS)) {
        var date = value.getString(TIME_LIST_CLASS);
        if (isDifferenceLessThanFiveMin(date) &&
            studentState.state.studentModel != null) {
        } else {
          await value.setString(
              TIME_LIST_CLASS, DateTime.now().toIso8601String());
          await refreshIndicatorKey.currentState.show();
        }
      } else {
        await value.setString(
            TIME_LIST_CLASS, DateTime.now().toIso8601String());
        await refreshIndicatorKey.currentState.show();
      }
    });

    scrollController.addListener(() {
      if (scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (!isScrollingDown) {
          setState(() {
            isScrollingDown = true;
            isShowBottom = false;
          });
        }
      }

      if (scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if (isScrollingDown) {
          setState(() {
            isScrollingDown = false;
            isShowBottom = true;
          });
        }
      }
    });
  }

  @override
  void dispose() {
    scrollController.removeListener(() {});
    scrollController.dispose();
    super.dispose();
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await studentState.setState((s) async => await s.retrieveData(widget.id),
          catchError: true, onError: (context, dynamic error) {
        print((error as SiswamediaException).message);
        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Color(0xfff7f7f7),
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Daftar Siswa'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(5),
            child: Divider(
              color: Colors.grey,
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: S.w * .02),
              child: StateBuilder<StudentState>(
                  observe: () => studentState,
                  builder: (context, snapshot) {
                    return RefreshIndicator(
                        key: refreshIndicatorKey,
                        onRefresh: () async {
                          await retrieveData();
                        },
                        child: LayoutBuilder(builder: (_, __) {
                          if (snapshot.isWaiting) {
                            return WaitingView();
                          } else if (!snapshot.hasData) {
                            return WaitingView();
                          } else if (snapshot.hasError) {
                            return ErrorView(
                                error: (snapshot.error is SiswamediaException));
                          }
                          return ListView(
                            controller: scrollController,
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(S.w)),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey.withOpacity(.2),
                                          offset: Offset(0, 0),
                                          spreadRadius: 2,
                                          blurRadius: 4)
                                    ]),
                                child: TextField(
                                  controller: searchController,
                                  onChanged: (val) {
                                    setState(() {});
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    prefixIcon: Icon(Icons.search),
                                    fillColor: Colors.transparent,
                                    hintText: 'Cari Murid Kesayangan kamu..',
                                    hintStyle: TextStyle(fontSize: S.w / 28),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                              GridView.count(
                                  physics: ScrollPhysics(),
                                  crossAxisCount: gridCountStudents(
                                      MediaQuery.of(context).size.aspectRatio),
                                  shrinkWrap: true,
                                  childAspectRatio: .65,
                                  padding: EdgeInsets.all(10),
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                  scrollDirection: Axis.vertical,
                                  children: snapshot.state.studentModel.data
                                      .where((element) => element.fullName
                                          .toLowerCase()
                                          .contains(searchController.text
                                              .toLowerCase()))
                                      .map((e) => CardDaftarSiswa(
                                          data: e, studentState: studentState))
                                      .toList()),
                            ],
                          );
                        }));
                  }),
            ),
            // AnimatedPositioned(
            //     duration: Duration(milliseconds: 400),
            //     bottom: isShowBottom ? showBottom : -2.5 * showBottom,
            //     child: SizedBox(
            //         width: MediaQuery.of(context).size.width,
            //         child: Center(
            //             child: Container(
            //                 width: 120,
            //                 height: 45,
            //                 decoration: BoxDecoration(
            //                     color: SiswamediaTheme.green,
            //                     borderRadius: radius(S.w)),
            //                 child: Row(
            //                     mainAxisAlignment: MainAxisAlignment.center,
            //                     children: [
            //                       Icon(Icons.filter_list_sharp,
            //                           color: Colors.white),
            //                       SizedBox(width: 10),
            //                       CustomText('Filter', Kind.heading4)
            //                     ])))))
          ],
        ));
  }
}

class ListStudent extends StatelessWidget {
  final int id;
  final bool isPortrait;
  ListStudent({this.id, this.isPortrait});

  @override
  Widget build(BuildContext context) {
    var studentState = Injector.getAsReactive<StudentState>();
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: StateBuilder<StudentState>(
            observe: () => studentState,
            builder: (context, snapshot) {
              if (snapshot.isWaiting) {
                return WaitingView();
              } else if (!snapshot.hasData) {
                return WaitingView();
              } else if (snapshot.hasError) {
                return ErrorView(
                    error: (snapshot.error is SiswamediaException));
              }
              return GridView.count(
                  physics: ScrollPhysics(),
                  crossAxisCount: isPortrait ? 2 : 4,
                  shrinkWrap: true,
                  childAspectRatio: .7,
                  padding: EdgeInsets.all(10),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  scrollDirection: Axis.vertical,
                  children: snapshot.state.studentModel.data
                      .map((e) =>
                          CardDaftarSiswa(data: e, studentState: studentState))
                      .toList());
            }));
  }
}

class CardDaftarSiswa extends StatelessWidget {
  final StudentClassroomModel data;
  final ReactiveModel<StudentState> studentState;

  CardDaftarSiswa({@required this.data, @required this.studentState})
      : assert(data != null),
        assert(studentState != null);

  @override
  Widget build(BuildContext context) {
    final authState = GlobalState.auth();
    // var auth = context.watch<UserProvider>();
    return Container(
        padding: EdgeInsets.all(S.w * .05),
        decoration: SiswamediaTheme.whiteRadiusShadow,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(2000),
                child: Image.network(data.image,
                    height: S.w * .15, width: S.w * .15)),
            SizedBox(height: 10),
            CustomText(data.fullName, Kind.headingMax2Line,
                align: TextAlign.center),
            CustomText(
              '${data.role}',
              Kind.descBlack,
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: radius(200)),
                    onPressed: () {
                      ContactDialog(data: data)..show(context);
                    },
                    color: SiswamediaTheme.green,
                    child: Center(
                      child: Text(
                        'Kontak',
                        style: semiWhite.copyWith(fontSize: S.w / 30),
                      ),
                    ))
              ],
            ),
            SizedBox(height: 10),
            if (authState.state.selectedClassRole.isGuruOrWaliKelas ||
                authState.state.currentState.isAdmin)
              InkWell(
                onTap: () {
                  delete(context, data);
                },
                child: Icon(
                  Icons.remove_circle_outline_outlined,
                ),
              )
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     RaisedButton(
            //         shape: RoundedRectangleBorder(borderRadius: radius(200)),
            //         onPressed: () {
            //           ContactDialog(data: data)..show(context);
            //         },
            //         color: SiswamediaTheme.green,
            //         child: Center(
            //           child: Text(
            //             'Kontak',
            //             style: semiWhite.copyWith(fontSize: S.w / 30),
            //           ),
            //         ))
            //   ],
            // ),
          ],
        ));
  }

  void delete(BuildContext context, StudentClassroomModel data) {
    showDialog<void>(
        context: context,
        builder: (context2) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
              width: S.w,
              padding: EdgeInsets.all(S.w * .05),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text('Keluarkan member ini?',
                    style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                SizedBox(height: 15),
                Text('Apakah Anda yakin untuk mengeluarkan member ini ?',
                    textAlign: TextAlign.center,
                    style: descBlack.copyWith(fontSize: S.w / 34)),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .01),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: S.w / 10,
                          width: S.w / 3.5,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: SiswamediaTheme.green,
                          ),
                          child: InkWell(
                            onTap: () async {
                              // ignore: unawaited_futures
                              showDialog<void>(
                                  context: context,
                                  builder: (_) => Center(
                                      child: CircularProgressIndicator()));
                              await ClassServices.kickMemberClass(
                                      classId: data.classId,
                                      userId: data.userId)
                                  .then((value) async {
                                if (value.statusCode == StatusCodeConst.success ||
                                    value.statusCode == 201) {
                                  Navigator.pop(context);
                                  Navigator.pop(context);

                                  await studentState.setState(
                                      (s) => s.retrieveData(data.classId));
                                  CustomFlushBar.successFlushBar(
                                    'Berhasil mengeluarkan member',
                                    context,
                                  );
                                } else {
                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                  CustomFlushBar.errorFlushBar(
                                    'Terjadi kesalahan ketika mengeluarkan member',
                                    context,
                                  );
                                }
                              });

                              //Navigator.push(context,
                              //  MaterialPageRoute<void>(builder: (_) => null));
                            },
                            child: Center(
                              child: Text('Ya',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: SiswamediaTheme.lightBlack,
                          ),
                          height: S.w / 10,
                          width: S.w / 3.5,
                          child: InkWell(
                            onTap: () => Navigator.pop(context),
                            child: Center(
                              child: Text('Tidak',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                      ]),
                )
              ]),
            )));
  }
}

class ContactDialog {
  StudentClassroomModel data;
  ContactDialog({this.data});

  final authState = GlobalState.auth();
  void show(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(borderRadius: radius(12)),
            child: Container(
              width: S.w,
              padding: EdgeInsets.all(S.w * .05),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                CustomText('Kontak', Kind.heading1),
                SizedBox(height: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                        width: 60,
                        child: CustomText('Telepon', Kind.descBlack)),
                    Row(
                      children: [
                        Expanded(
                            child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          decoration: BoxDecoration(
                              color: Color(0xFFECECEC),
                              borderRadius: BorderRadius.circular(3),
                              border: Border.all(color: Color(0xFFECECEC))),
                          child: Text(
                              (data.noTelp == '' ? 'Tidak ada' : data.noTelp)
                                  .toString(),
                              style: TextStyle(color: SiswamediaTheme.green)),
                        )),
                        GestureDetector(
                            onTap: () {
                              if (data.noTelp != '') {
                                Navigator.pop(context);
                                ShareService.share(context, data.noTelp, 'Tindakan');
                              } else {
                                CustomFlushBar.errorFlushBar(
                                  'Tidak ada nomor telepon',
                                  context,
                                );
                              }
                            },
                            child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 5),
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.green,
                                    borderRadius: BorderRadius.circular(3)),
                                child: Icon(
                                  Icons.share,
                                  color: Colors.white,
                                  size: S.w * .05,
                                ))),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 5),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                        width: 60, child: CustomText('Email', Kind.descBlack)),
                    Row(
                      children: [
                        Expanded(
                            child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          decoration: BoxDecoration(
                              color: Color(0xFFECECEC),
                              borderRadius: BorderRadius.circular(3),
                              border: Border.all(color: Color(0xFFECECEC))),
                          child: Text(data.email,
                              style: TextStyle(color: SiswamediaTheme.green)),
                        )),
                        GestureDetector(
                            onTap: () {
                              if (data.email != '') {
                                Navigator.pop(context);
                                ShareService.share(context, data.email, 'Tindakan');
                              } else {
                                CustomFlushBar.errorFlushBar(
                                  'Tidak ada email',
                                  context,
                                );
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              decoration: BoxDecoration(
                                  color: SiswamediaTheme.green,
                                  borderRadius: BorderRadius.circular(3)),
                              child: Icon(Icons.share,
                                  color: Colors.white, size: S.w * .05),
                            )),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Card(
                        elevation: 4,
                        shadowColor: Colors.grey.withOpacity(.2),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        child: InkWell(
                          onTap: () async {
                            if (data.email != '') {
                              var email =
                                  'mailto:${data.email}?subject=News&body=Hai%20Siswamedia';
                              if (await canLaunch(email)) {
                                await launch(email);
                              } else {
                                throw 'Could not launch $email';
                              }
                            } else {
                              CustomFlushBar.errorFlushBar(
                                'Tidak ada email',
                                context,
                              );
                            }
                          },
                          child: Container(
                            height: 50,
                            padding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            child: Center(
                              child: Row(
                                children: [
                                  Icon(Icons.email),
                                  SizedBox(width: 5),
                                  CustomText('Kirim Email', Kind.descBlack),
                                ],
                              ),
                            ),
                          ),
                        )),
                    Card(
                      elevation: 4,
                      shadowColor: Colors.grey.withOpacity(.2),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: InkWell(
                        onTap: () async {
                          if (data.noTelp != '') {
                            var telp = 'tel:${data.noTelp}';
                            if (await canLaunch(telp)) {
                              await launch(telp);
                            } else {
                              throw 'Could not launch $telp';
                            }
                          } else {
                            CustomFlushBar.errorFlushBar(
                              'Tidak ada no telepon',
                              context,
                            );
                          }
                        },
                        child: Container(
                          height: 50,
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          child: Center(
                            child: Row(
                              children: [
                                Icon(Icons.phone),
                                SizedBox(width: 5),
                                CustomText('Telepon', Kind.descBlack),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )),
                SizedBox(height: 15),
                authState.state.selectedClassRole.isWaliKelas
                    ? data.codeIsUsed
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText('Wali Murid: ', Kind.descBlack),
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: CustomText(
                                    data.parentName ?? '', Kind.descGreen),
                              ),
                            ],
                          )
                        : Column(
                            children: [
                              CustomText('Kode Ortu', Kind.descBlack),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.pop(context);
                                      Clipboard.setData(
                                          ClipboardData(text: data.parentCode));
                                      CustomFlushBar.eventFlushBar(
                                          'Kode ortu copied', context);
                                    },
                                    child: Container(
                                        height: S.w / 12,
                                        width: S.w / 12,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 5, vertical: 5),
                                        decoration: BoxDecoration(
                                            color: SiswamediaTheme.lightBlack,
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                        child: Icon(
                                          Icons.attach_file,
                                          size: S.w / 24,
                                          color: Colors.white,
                                        )),
                                  ),
                                  Container(
                                    height: S.w / 12,
                                    width: S.w * .4,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    decoration: BoxDecoration(
                                        color: Color(0xFFECECEC),
                                        borderRadius: BorderRadius.circular(4),
                                        border: Border.all(
                                            color: Color(0xFFECECEC))),
                                    child: Center(
                                      child: Text(data.parentCode,
                                          style: TextStyle(
                                              fontSize: S.w / 32,
                                              color:
                                                  SiswamediaTheme.lightBlack)),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                    : SizedBox(),
                if (data.codeIsUsed)
                  Column(
                    children: [
                      SizedBox(height: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              width: 60,
                              child: CustomText('Telepon', Kind.descBlack)),
                          Row(
                            children: [
                              Expanded(
                                  child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5),
                                decoration: BoxDecoration(
                                    color: Color(0xFFECECEC),
                                    borderRadius: BorderRadius.circular(3),
                                    border:
                                        Border.all(color: Color(0xFFECECEC))),
                                child: Text(
                                    (data.parentTelephone == ''
                                            ? 'Tidak ada'
                                            : data.parentTelephone)
                                        .toString(),
                                    style: TextStyle(
                                        color: SiswamediaTheme.green)),
                              )),
                              GestureDetector(
                                  onTap: () {
                                    if (data.parentTelephone != '') {
                                      Navigator.pop(context);
                                      ShareService.share(context, data.parentTelephone,
                                          'Tindakan');
                                    } else {
                                      CustomFlushBar.errorFlushBar(
                                        'Tidak ada nomor telepon',
                                        context,
                                      );
                                    }
                                  },
                                  child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 5, vertical: 5),
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius:
                                              BorderRadius.circular(3)),
                                      child: Icon(
                                        Icons.share,
                                        color: Colors.white,
                                        size: S.w * .05,
                                      ))),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              width: 60,
                              child: CustomText('Email', Kind.descBlack)),
                          Row(
                            children: [
                              Expanded(
                                  child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5),
                                decoration: BoxDecoration(
                                    color: Color(0xFFECECEC),
                                    borderRadius: BorderRadius.circular(3),
                                    border:
                                        Border.all(color: Color(0xFFECECEC))),
                                child: Text(data.parentEmail,
                                    style: TextStyle(
                                        color: SiswamediaTheme.green)),
                              )),
                              GestureDetector(
                                  onTap: () {
                                    if (data.parentEmail != '') {
                                      Navigator.pop(context);
                                      ShareService.share(context, data.parentEmail,
                                          'Tindakan');
                                    } else {
                                      CustomFlushBar.errorFlushBar(
                                        'Tidak ada email',
                                        context,
                                      );
                                    }
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 5),
                                    decoration: BoxDecoration(
                                        color: SiswamediaTheme.green,
                                        borderRadius: BorderRadius.circular(3)),
                                    child: Icon(Icons.share,
                                        color: Colors.white, size: S.w * .05),
                                  )),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                    ],
                  )
              ]),
            ))
        // child
        );
  }
}

int gridCountStudents(double ratio) {
  if (ratio > 1.9) {
    return 7;
  } else if (ratio > 1.5) {
    return 5;
  } else if (ratio > .9) {
    return 3;
  } else {
    return 2;
  }
}
