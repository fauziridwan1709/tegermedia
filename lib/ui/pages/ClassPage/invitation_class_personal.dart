part of '_class_page.dart';

class InvitationClassPersonal extends StatefulWidget {
  final String type;
  final String role;
  InvitationClassPersonal({@required this.role, this.type = 'PERSONAL'});

  @override
  _InvitationClassPersonalState createState() =>
      _InvitationClassPersonalState();
}

class _InvitationClassPersonalState extends State<InvitationClassPersonal> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController name = TextEditingController();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Join Course'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
        body: ListView(
          children: [
            SizedBox(height: isPortrait ? 205 : 10),
            Column(
              children: [
                Text(
                    'Gabung Mata Pelajaran dengan menggunakan\nkode kelas yang sudah Anda\ndapatkan, dan salin ke kotak dibawah init',
                    textAlign: TextAlign.center),
                SizedBox(height: 18),
                Container(
                  height: 60,
                  width: S.w,
                  child: TextField(
                    controller: name,
                    style: semiBlack,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(45)),
                        hintText: 'Kode Kelas...'),
                  ),
                ),
                SizedBox(height: isPortrait ? 213 : 15),
                GestureDetector(
                  onTap: () async {
                    if (name.text == '') {
                      CustomFlushBar.errorFlushBar(
                        'Kode Kelas tidak boleh kosong!',
                        context,
                      );
                    } else {
                      setState(() {
                        isLoading = true;
                      });

                      var result = await ClassServices.joinCourse(reqData: {
                        'invitation_code': name.text,
                        'type': widget.type,
                        'role': widget.role
                      });

                      if (result == 200) {
                        setState(() {
                          isLoading = false;
                        });

                        var course =
                            await ClassServices.getMeCourse(cache: false);

                        dialog(context);
                        //todo course
                        // await provider.getMeSchool();
                        // provider.setMeCourse(course.data);
                      } else {
                        setState(() {
                          isLoading = false;
                        });
                        CustomFlushBar.errorFlushBar(
                          'Terjadi Kesalahan',
                          context,
                        );
                      }
                    }
                  },
                  child: Container(
                      width: S.w,
                      height: 45,
                      decoration: BoxDecoration(
                          color: SiswamediaTheme.green,
                          border: Border.all(color: SiswamediaTheme.green),
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                          child: isLoading
                              ? Container(
                                  height: 30,
                                  width: 30,
                                  child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.grey[300]),
                                      backgroundColor: Colors.white))
                              : Text('Lanjutkan',
                                  style: TextStyle(
                                      color: SiswamediaTheme.white)))),
                ),
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 24,
                ),
              ],
            ),
          ],
        ));
  }

  void dialog(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      height: 233,
                      width: 250,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: S.w - 48 * 2,
                            height: 100,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/icons/kelas/confirmation.png'))),
                          ),
                          SizedBox(height: 15),
                          Text('Pendaftaran Berhasil',
                              style: TextStyle(fontSize: 14)),
                          SizedBox(height: 15),
                          InkWell(
                            onTap: () {
                              Navigator.popUntil(
                                  context, (route) => route.isFirst);
                            },
                            child: Container(
                                height: 35,
                                width: 105,
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.green,
                                    borderRadius: BorderRadius.circular(4)),
                                child: Center(
                                    child: Text('Yeay',
                                        style:
                                            TextStyle(color: Colors.white)))),
                          ),
                        ],
                      )));
            }));
  }
}
