part of '_class_page.dart';

class ProgressView extends StatefulWidget {
  final Function onTap;
  ProgressView({this.onTap});
  @override
  _ProgressViewState createState() => _ProgressViewState();
}

class _ProgressViewState extends State<ProgressView> {
  DetailMeAssesmentsStatitistic data;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = true;
    });
    getMeAssesments();
  }

  void getMeAssesments() async {
    var result = await AssignmentServices.getAssessmentsStatistic();

    setState(() {
      isLoading = true;
    });
    if (result.statusCode == 200) {
      setState(() {
        data = result.data;
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return isLoading
        ? Padding(
            padding: EdgeInsets.only(top: 90, bottom: 90),
            child: Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green),
            )),
          )
        : !data.isAssessments
            ? Column(children: [
                SizedBox(height: 25),
                Container(
                  height: 150,
                  width: 179.67,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/icons/kelas/belum-ada-tugas.png'))),
                ),
                Text('Belum Ada Tugas',
                    style: TextStyle(color: SiswamediaTheme.green)),
                SizedBox(height: 25)
              ])
            : Container(
                margin: EdgeInsets.symmetric(horizontal: width * .05),
                decoration: BoxDecoration(
                  color: SiswamediaTheme.white,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      bottomLeft: Radius.circular(8.0),
                      bottomRight: Radius.circular(8.0),
                      topRight: Radius.circular(68.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: SiswamediaTheme.grey.withOpacity(0.2),
                        offset: const Offset(1.1, 1.1),
                        blurRadius: 10.0),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 16, left: 16, right: 16, bottom: 16),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 4, bottom: 3),
                                                child: Text(
                                                  'Tugas',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontFamily: SiswamediaTheme
                                                        .fontName,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 32,
                                                    color:
                                                        SiswamediaTheme.green,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 4, top: 2, bottom: 14),
                                            child: Text(
                                              '${data.totalSudahDikerjakan}/${data.totalAssessment} Tugas sudah dikerjakan',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                fontFamily:
                                                    SiswamediaTheme.fontName,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14,
                                                letterSpacing: 0.0,
                                                color: SiswamediaTheme.darkText,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 4,
                                            right: 4,
                                            top: 8,
                                            bottom: 16),
                                        child: Container(
                                          height: 2,
                                          decoration: BoxDecoration(
                                            color: SiswamediaTheme.background,
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(4.0)),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 16),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 4),
                                                  child: Icon(
                                                    Icons.access_time,
                                                    color: SiswamediaTheme.grey
                                                        .withOpacity(0.5),
                                                    size: 16,
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 4.0),
                                                  child: Text(
                                                    'Terakhir dikumpulkan',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          SiswamediaTheme
                                                              .fontName,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 14,
                                                      letterSpacing: 0.0,
                                                      color: SiswamediaTheme
                                                          .grey
                                                          .withOpacity(0.5),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.only(top: 4),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Flexible(
                                                    child: Text(
                                                      formattedDate(
                                                          data.updatedAt),
                                                      textAlign:
                                                          TextAlign.start,
                                                      style: TextStyle(
                                                        fontFamily:
                                                            SiswamediaTheme
                                                                .fontName,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 12,
                                                        letterSpacing: 0.0,
                                                        color: SiswamediaTheme
                                                            .green,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 16, right: 8, top: 16),
                                  child: Container(
                                    width: 60,
                                    height: 160,
                                    decoration: BoxDecoration(
                                      color: Color(0xffE8EDFE),
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(80.0),
                                          bottomLeft: Radius.circular(80.0),
                                          bottomRight: Radius.circular(80.0),
                                          topRight: Radius.circular(80.0)),
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: SiswamediaTheme.grey
                                                .withOpacity(0.4),
                                            offset: const Offset(2, 2),
                                            blurRadius: 4),
                                      ],
                                    ),
                                    child: WaveView(
                                      percentageValue:
                                          data.totalSudahDikerjakan /
                                              data.totalAssessment *
                                              100,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            GestureDetector(
                              onTap: () {
                                if (widget.onTap != null) {
                                  widget.onTap();
                                }
                              },
                              child: Container(
                                // height: 30,
                                padding: EdgeInsets.symmetric(vertical: 10),
                                margin: EdgeInsets.only(top: 30, bottom: 16),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: SiswamediaTheme.green,
                                ),
                                child: Center(
                                  child: Text('Selengkapnya', style: semiWhite),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
  }
}
