part of '_class_page.dart';

class TutorialOverlay extends ModalRoute<void> {
  TutorialOverlay({this.offset});

  final Offset offset;

  @override
  Duration get transitionDuration => Duration(milliseconds: 500);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.6);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
  ) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    print(offset.dy);
    print(offset.dx);
    return Container(
      width: S.w,
      height: S.h,
      child: Stack(
        children: [
          Row(
            children: [
              SizedBox(width: S.w * .25),
              // FDottedLine(
              //   color: Colors.white,
              //   height: S.h,
              // ),
              SizedBox(width: S.w * .05),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(right: S.w * .06),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(height: 5),
                      Text(
                          'Tekan Tombol tersebut untuk memilih atau mengganti kelas',
                          textAlign: TextAlign.right,
                          style: boldBlack.copyWith(
                              fontSize: S.w / 26, color: Colors.white)),
                      SizedBox(height: 20),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text('Ok, Mengerti',
                              textAlign: TextAlign.right,
                              style: boldBlack.copyWith(
                                  fontSize: S.w / 22, color: Colors.green)),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
          Positioned(
            child: ButtonSmall(
              onTap: () {
                Navigator.pop(context);
                navigate(
                    context,
                    ChangeClass(
                        id: Injector.getAsReactive<AuthState>()
                            .state
                            .currentState
                            .schoolId));
              },
              icon: 'assets/icons/home/board_icon.png',
            ),
            left: offset.dx,
            top: offset.dy - MediaQuery.of(context).padding.top,
          ),
        ],
      ),
    );
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // You can add your own animations for the overlay content
    return FadeTransition(
      opacity: animation,
      child: child,
      // ScaleTransition(
      //   scale: animation,
      //   child: child,
      // ),
    );
  }
}
