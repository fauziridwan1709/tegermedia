part of '_class_page.dart';

class ChangeClass extends StatefulWidget {
  final int id;
  final List<String> role;
  final bool stateRouteRegister;
  ChangeClass({@required this.id, this.role, this.stateRouteRegister = false});
  @override
  _ChangeClassState createState() => _ChangeClassState();
}

class _ChangeClassState extends State<ChangeClass> {
  final classState = Injector.getAsReactive<ClassState>();
  final authState = Injector.getAsReactive<AuthState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  var completer = Completer<Null>();

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(TIME_LIST_CLASS)) {
        var date = value.getString(TIME_LIST_CLASS);
        if (isDifferenceLessThanFiveMin(date) &&
            classState.state.classMap.isNotEmpty) {
        } else {
          await value.setString(
              TIME_LIST_CLASS, DateTime.now().toIso8601String());
          await refreshIndicatorKey.currentState.show();
        }
      } else {
        await value.setString(
            TIME_LIST_CLASS, DateTime.now().toIso8601String());
        await refreshIndicatorKey.currentState.show();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await classState.setState(
          (s) async => await s.retrieveData(
              data: ClassBySchoolModel(
                  param: GetClassModel(schoold: widget.id, me: true))),
          filterTags: <dynamic>['tago'],
          catchError: true, onError: (context, dynamic error) {
        print((error as SiswamediaException).message);
        classState.notify(<dynamic>['tago']);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop();
        return;
      },
      child: Scaffold(
        // backgroundColor: Colors.grey[100],
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Ganti kelas',
            style: boldBlack.copyWith(fontSize: S.w / 22, color: Colors.white),
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xffB5D548), Colors.green],
                begin: Alignment.topCenter,
                end: Alignment(0, 1.5),
              ),
            ),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
        body: StateBuilder(
          observe: () => classState,
          tag: ['tago'],
          builder: _buildListClass,
        ),
      ),
    );
  }

  Widget _buildListClass(
      BuildContext context, ReactiveModel<ClassState> snapshot) {
    return RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: () async {
          await retrieveData();
        },
        child: WhenRebuilder<ClassState>(
          observe: () => snapshot,
          onWaiting: () => WaitingView(),
          onError: (dynamic error) =>
              ErrorView(error: (error is SiswamediaException)),
          onIdle: () => WaitingView(),
          onData: (data) {
            //todo for radit
            if (data.classMap == null || data.classMap.isEmpty) {
              return ListView(children: [
                Container(
                    margin: EdgeInsets.only(top: 16),
                    child: ClassNotFound(
                        label:
                            '${authState.state.currentState.isAdmin ? '\n${authState.state.currentState.type == 'PERSONAL' ? 'Media' : 'Sekolah'} ini belum memiliki kelas, Silahkan tambah kelas ${authState.state.currentState.type == 'PERSONAL' ? 'kemudian Undang Siswa' : 'kemudian Undang Guru dan Siswa'}' : 'Anda belum memiliki kelas'}')),
              ]);
            }
            return ListView(
              children: [
                Column(children: [
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: ListClass(
                      tipe: 'change',
                      data: classState.state.classMap.values.toList(),
                    ),
                  ),
                ]),
                SizedBox(height: 40),
              ],
            );
          },
        )
        // child: LayoutBuilder(builder: (context, _) {
        //   print('aduh ${classState.hasError}');
        //   if (snapshot.isWaiting) {
        //     return WaitingView();
        //   } else if (snapshot.hasError) {
        //     return ErrorView(
        //         error: (snapshot.error is SiswamediaException));
        //   } else if (snapshot.state.classModel == null ||
        //       snapshot.state.classModel.data.isEmpty) {
        //     if (completer.isCompleted) {
        //       return ListView(children: [
        //         Container(
        //           margin: EdgeInsets.only(top: 16),
        //           child: DataNotFound(
        //               label: 'Anda belum memiliki kelas'),
        //         ),
        //         role.contains('ORTU')
        //             ? SizedBox()
        //             : ButtonWidthCondition(
        //                 role: role,
        //                 provider: provider,
        //                 id: id,
        //                 count: _count,
        //                 refreshCount: () =>
        //                     _getCountWaitingApproval())
        //       ]);
        //     }
        //     return WaitingView();
        //   }
        //   return ListView(
        //     children: [
        //       Column(children: [
        //         Container(
        //           margin: EdgeInsets.only(top: 16),
        //           child: ListClass(
        //             data: classState.state.classModel.data,
        //           ),
        //         ),
        //         role.contains('ORTU')
        //             ? SizedBox()
        //             : ButtonWidthCondition(
        //                 role: role,
        //                 provider: provider,
        //                 id: id,
        //                 count: _count,
        //                 refreshCount: () =>
        //                     _getCountWaitingApproval())
        //       ]),
        //       SizedBox(height: 40),
        //     ],
        //   );
        // }),
        );
  }
}
