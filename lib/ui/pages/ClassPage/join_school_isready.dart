part of '_class_page.dart';

class JoinSchoolIsReady extends StatefulWidget {
  @override
  _JoinSchoolIsReadyState createState() => _JoinSchoolIsReadyState();
}

class _JoinSchoolIsReadyState extends State<JoinSchoolIsReady> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;

  TextEditingController name = TextEditingController();
  TextEditingController invitationCode = TextEditingController();
  TextEditingController address = TextEditingController();

  String provinsiID = '';
  String kotaID = '';
  String schoolID = '';
  String gradeName = '';

  ModelListWilayah provinsi = ModelListWilayah(data: [], statusCode: null);
  ModelListWilayah kabupaten = ModelListWilayah(data: [], statusCode: null);
  List<DataSchoolModel> schools = [];

  List<String> grade = ['TK', 'SD', 'SMP', 'SMA', 'SMK', 'UMUM'];

  @override
  void initState() {
    super.initState();

    getProvinsi();
    if (mounted) {
      provinsiID = null;
      kotaID = null;
      schoolID = null;
      gradeName = null;
    }
  }

  @override
  void dispose() {
    provinsiID = null;
    kotaID = null;
    schoolID = null;
    gradeName = null;
    name.dispose();
    invitationCode.dispose();
    address.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Cari Sekolah'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        backgroundColor: Colors.white,
        body: Center(
          child: Container(
              width: S.w,
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: isPortrait ? S.w * .3 : 10),
                      Container(
                        width: S.w * .76,
                        child: Text(
                          'Silahkan cari Sekolah yang sesuai dengan \nLokasi Anda,',
                          textAlign: TextAlign.center,
                          style: semiBlack.copyWith(fontSize: S.w / 28),
                        ),
                      ),
                      SizedBox(height: 24),
                      DropdownButtonFormField<String>(
                        value: provinsiID,
                        icon: Icon(Icons.keyboard_arrow_down,
                            color: SiswamediaTheme.green),
                        iconSize: 24,
                        elevation: 16,
                        isExpanded: true,
                        style: descBlack.copyWith(
                            fontSize: S.w / 30, color: Colors.black),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 25),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(S.w),
                              borderSide: BorderSide(color: Color(0xffD8D8D8)),
                            ),
                            hintStyle: descBlack.copyWith(fontSize: S.w / 32)),
                        hint: Text('Pilih Provinsi',
                            style: descBlack.copyWith(
                                fontSize: S.w / 30, color: Color(0xff4B4B4B))),
                        onChanged: (String newValue) {
                          setState(() {
                            kotaID = null;
                            provinsiID = newValue.toString();
                            kabupaten =
                                ModelListWilayah(statusCode: null, data: []);
                          });
                          getKota(newValue.toString());
                        },
                        items: [
                          DropdownMenuItem(
                            value: '0',
                            child: Text('Pilih Provinsi'),
                          ),
                          for (DataWilayah value in provinsi.data)
                            DropdownMenuItem(
                              value: value.id.toString(),
                              child: Text('${value.nama}'),
                            )
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      DropdownButtonFormField<String>(
                        value: kotaID,
                        icon: Icon(Icons.keyboard_arrow_down,
                            color: SiswamediaTheme.green),
                        iconSize: 24,
                        elevation: 16,
                        isExpanded: true,
                        style: descBlack.copyWith(
                            fontSize: S.w / 30, color: Colors.black),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 25),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(23.0),
                              borderSide: BorderSide(color: Color(0xffD8D8D8)),
                            )),
                        hint: Text('Pilih Kota',
                            style: descBlack.copyWith(
                                fontSize: S.w / 30, color: Color(0xff4B4B4B))),
                        onChanged: (String newValue) {
                          setState(() {
                            kotaID = newValue.toString();
                          });
                        },
                        items: [
                          DropdownMenuItem(
                            value: '0',
                            child: Text('Pilih Kota'),
                          ),
                          for (DataWilayah value in kabupaten.data)
                            DropdownMenuItem(
                              value: value.id.toString(),
                              child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6,
                                  child: Text(
                                    '${value.nama}',
                                  )),
                            )
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      DropdownButtonFormField<String>(
                        value: gradeName,
                        icon: Icon(Icons.keyboard_arrow_down,
                            color: SiswamediaTheme.green),
                        iconSize: 24,
                        elevation: 16,
                        isExpanded: true,
                        style: descBlack.copyWith(
                            fontSize: S.w / 30, color: Colors.black),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 25),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(23.0),
                            borderSide: BorderSide(color: Color(0xffD8D8D8)),
                          ),
                        ),
                        hint: Text('Pilih Jenjang Sekolah',
                            style: descBlack.copyWith(
                                fontSize: S.w / 30, color: Color(0xff4B4B4B))),
                        onChanged: (String newValue) {
                          print(newValue);
                          setState(() {
                            gradeName = newValue;
                            schoolID = null;
                          });
                          getSchools(int.parse(provinsiID), int.parse(kotaID),
                                  newValue)
                              .then((value) {
                            if (schools.isEmpty) {
                              CustomFlushBar.errorFlushBar(
                                'Tidak menemukan Sekolah di wilayah tersebut',
                                context,
                              );
                            }
                          });
                          if (schools.isEmpty) {}
                        },
                        items: [
                          DropdownMenuItem(
                            value: '0',
                            child: Text('Pilih Jenjang Sekolah'),
                          ),
                          for (String value in grade)
                            DropdownMenuItem(
                              value: value.toString(),
                              child: Text(value),
                            )
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      DropdownButtonFormField<String>(
                        value: schoolID,
                        icon: Icon(Icons.keyboard_arrow_down,
                            color: SiswamediaTheme.green),
                        iconSize: 24,
                        elevation: 16,
                        isExpanded: true,
                        style: descBlack.copyWith(
                            fontSize: S.w / 30, color: Colors.black),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 25),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffD8D8D8)),
                            borderRadius: BorderRadius.circular(23.0),
                          ),
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            schoolID = newValue.toString();
                          });
                          if (kotaID == null || provinsiID == null) {
                            CustomFlushBar.errorFlushBar(
                              'Pilih Wilayah Terlebih dahulu ',
                              context,
                            );
                          }
                        },
                        hint: Text('Pilih Sekolah',
                            style: descBlack.copyWith(
                                fontSize: S.w / 30, color: Color(0xff4B4B4B))),
                        items: [
                          DropdownMenuItem(
                            value: '0',
                            child: Text('Pilih Sekolah'),
                          ),
                          for (DataSchoolModel value in schools)
                            DropdownMenuItem(
                              value: value.id.toString(),
                              child: Text('${value.name}'),
                            )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Tidak menemukannya?',
                              style: semiBlack.copyWith(fontSize: S.w / 28)),
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (_) => CreateClassInstansi()));
                            },
                            child: Text(
                              '- Daftar',
                              style: TextStyle(
                                  color: SiswamediaTheme.green,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      GestureDetector(
                        onTap: () async {
                          if (provinsiID == null || provinsiID == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Provinsi wajib dipilih!',
                              context,
                            );
                          }

                          if (kotaID == null || kotaID == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Kota wajib dipilih!',
                              context,
                            );
                          }

                          if (schoolID == null || schoolID == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Sekolah wajib dipilih!',
                              context,
                            );
                          }

                          if (gradeName == null || gradeName == '') {
                            return CustomFlushBar.errorFlushBar(
                              'Jenjang wajib dipilih!',
                              context,
                            );
                          }

                          setState(() {
                            isLoading = true;
                          });
                          // ignore: unawaited_futures
                          showDialog<void>(
                              context: context,
                              builder: (context) => Center(
                                      child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        SiswamediaTheme.green),
                                  )));
                          var result = await SchoolServices.createClass(
                              schoolID: int.parse(schoolID));
                          print(result.data);
                          print(result.message);
                          print(result.statusCode);
                          if (result.statusCode == 201 ||
                              result.statusCode == 200) {
                            Navigator.pop(context);
                            dialog(context);
                            setState(() {
                              isLoading = false;
                            });
                          } else {
                            setState(() {
                              isLoading = false;
                            });
                            Navigator.pop(context);
                            CustomFlushBar.errorFlushBar(
                              result.message,
                              context,
                            );
                          }
                        },
                        child: Container(
                          height: S.w * .13,
                          margin: EdgeInsets.only(top: 10, bottom: 10),
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                              child: Text('Lanjutkan',
                                  style:
                                      TextStyle(color: SiswamediaTheme.white))),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: SiswamediaTheme.green,
                              border: Border.all(color: SiswamediaTheme.green)),
                        ),
                      ),
                      SizedBox(height: 10),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute<void>(
                                builder: (context) => ProfilePage()),
                          );
                        },
                        child: Text('Batalkan',
                            style: TextStyle(color: SiswamediaTheme.green)),
                      )
                    ],
                  ),
                ),
              )),
        ));
  }

  void getProvinsi() async {
    dynamic wilayah = await WilayahServices.getWilayah();
    setState(() {
      provinsi = wilayah;
      kabupaten = ModelListWilayah(statusCode: null, data: []);
    });
  }

  void getKota(String id) async {
    dynamic wilayah = await WilayahServices.getKabupaten(id);
    var data = ModelListWilayah.fromJson(wilayah);

    if (data.statusCode == 200) {
      setState(() {
        kabupaten = data;
      });
    } else {
      setState(() {
        kabupaten = ModelListWilayah(statusCode: null, data: []);
      });
    }
  }

  Future<void> getSchools(int provinsiID, int kotaID, String grade) async {
    dynamic wilayah =
        await SchoolServices.getSchools(provinsiID, kotaID, grade);
    var data = SchoolModel.fromJson(wilayah as Map<String, dynamic>);
    setState(() {
      schools = [];
    });
    if (data.statusCode == 200) {
      setState(() {
        schools = data.data;
      });
    } else {
      setState(() {
        schools = [];
      });
    }
  }

  void dialog(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      height: 310,
                      width: 250,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: 17),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: S.w - 48 * 2,
                            height: 100,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/icons/kelas/confirmation.png'))),
                          ),
                          SizedBox(height: 15),
                          Text('Yey Join Sekolah Berhasil',
                              style: boldBlack.copyWith(fontSize: S.w / 24)),
                          Text(
                            'Sekolah yang diikuti akan ditambahkan ke sekolah bila sudah diverifikasi',
                            textAlign: TextAlign.center,
                            style: descBlack.copyWith(fontSize: S.w / 32),
                          ),
                          SizedBox(height: 15),
                          // Text(
                          //   name.text,
                          //   style: TextStyle(
                          //       fontSize: 20, fontWeight: FontWeight.bold),
                          //   textAlign: TextAlign.center,
                          // ),
                          // Text(address.text,
                          //     style: TextStyle(fontSize: 14),
                          //     textAlign: TextAlign.center),
                          // Text('Jl. Kehirupan 5 Bandung Selatan, 113920',
                          //     style: TextStyle(fontSize: 10),
                          //     textAlign: TextAlign.center),
                          // SizedBox(height: 16),
                          // Text('Belum Terverifikasi',
                          //     style: TextStyle(fontSize: 16, color: Colors.red),
                          //     textAlign: TextAlign.center),
                          // Text(
                          //   'Kelas yang dibuat akan ditambahkan ke sekolah bila sudah diverifikasi',
                          //   textAlign: TextAlign.center,
                          //   style: TextStyle(fontSize: 11, color: Colors.grey),
                          // ),
                          // SizedBox(height: 16),
                          // Text('Kontak Admin', textAlign: TextAlign.center),
                          // Text('Kontak Admin',
                          //     style: TextStyle(
                          //         fontSize: 16, fontWeight: FontWeight.bold),
                          //     textAlign: TextAlign.center),
                          SizedBox(height: 16),
                          InkWell(
                            onTap: () {
                              Navigator.popUntil(
                                  context, (route) => route.isFirst);
                              CustomFlushBar.eventFlushBar(
                                  'Tunggu diverifikasi ya!', context);
                            },
                            child: Container(
                                height: 35,
                                width: 105,
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.green,
                                    borderRadius: BorderRadius.circular(4)),
                                child: Center(
                                    child: Text('Ok',
                                        style:
                                            TextStyle(color: Colors.white)))),
                          ),
                        ],
                      )));
            }));
  }
}
