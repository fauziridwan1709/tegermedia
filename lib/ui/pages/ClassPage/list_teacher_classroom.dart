part of '_class_page.dart';

class ListTeacherClassroom extends StatefulWidget {
  final int id;

  ListTeacherClassroom({this.id});

  @override
  _ListTeacherClassroomState createState() => _ListTeacherClassroomState();
}

class _ListTeacherClassroomState extends State<ListTeacherClassroom> {
  final authState = Injector.getAsReactive<AuthState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var isShowCode = false;
  var searchController = TextEditingController();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  var teacherState = Injector.getAsReactive<TeacherState>();
  var isShowBottom = true;
  var isScrollingDown = false;
  final ScrollController scrollController = ScrollController();
  var showBottom = 20.0;

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(TIME_LIST_CLASS)) {
        var date = value.getString(TIME_LIST_CLASS);
        if (isDifferenceLessThanFiveMin(date) &&
            teacherState.state.teacherModel != null) {
        } else {
          await value.setString(
              TIME_LIST_CLASS, DateTime.now().toIso8601String());
          await refreshIndicatorKey.currentState.show();
        }
      } else {
        await value.setString(
            TIME_LIST_CLASS, DateTime.now().toIso8601String());
        await refreshIndicatorKey.currentState.show();
      }
    });

    scrollController.addListener(() {
      if (scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (!isScrollingDown) {
          setState(() {
            isScrollingDown = true;
            isShowBottom = false;
          });
        }
      }

      if (scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if (isScrollingDown) {
          setState(() {
            isScrollingDown = false;
            isShowBottom = true;
          });
        }
      }
    });
  }

  @override
  void dispose() {
    scrollController.removeListener(() {});
    scrollController.dispose();
    super.dispose();
  }

  Future<void> retrieveData() async {
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await teacherState.setState((s) async => await s.retrieveData(widget.id),
          catchError: true, onError: (context, dynamic error) {
        print((error as SiswamediaException).message);
        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return WillPopScope(
      onWillPop: () async {
        // await teacherState.setState((s) => s.clearData());
        return true;
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.white,
            shadowColor: Colors.transparent,
            centerTitle: true,
            title: TitleAppbar(label: 'Daftar Guru'),
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: SiswamediaTheme.green,
                ),
                onPressed: () {
                  Navigator.pop(context);
                  // teacherState.setState((s) => s.clearData());
                }),
          ),
          body: Stack(
            children: [
              Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: S.w * .02),
                child: StateBuilder<TeacherState>(
                    observe: () => teacherState,
                    builder: (context, snapshot) {
                      return RefreshIndicator(
                          key: refreshIndicatorKey,
                          onRefresh: () async {
                            await retrieveData();
                          },
                          child: WhenRebuilder<TeacherState>(
                            observe: () => teacherState,
                            onWaiting: () => WaitingView(),
                            onIdle: () => WaitingView(),
                            onError: (dynamic error) => ErrorView(
                                error: (snapshot.error is SiswamediaException)),
                            onData: (data) {
                              var teachers = data.teacherModel.data
                                  .where((element) => element.fullName
                                      .toLowerCase()
                                      .contains(
                                          searchController.text.toLowerCase()))
                                  .toList();
                              return ListView(
                                controller: scrollController,
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    decoration: BoxDecoration(
                                        color: SiswamediaTheme.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(S.w)),
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(.2),
                                              offset: Offset(0, 0),
                                              spreadRadius: 2,
                                              blurRadius: 4)
                                        ]),
                                    child: TextField(
                                      controller: searchController,
                                      onChanged: (val) {
                                        setState(() {});
                                      },
                                      decoration: InputDecoration(
                                        filled: true,
                                        prefixIcon: Icon(Icons.search),
                                        fillColor: Colors.transparent,
                                        hintText: 'Cari Guru Kesayangan kamu..',
                                        hintStyle:
                                            TextStyle(fontSize: S.w / 28),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  ListView.builder(
                                      physics: ScrollPhysics(),
                                      shrinkWrap: true,
                                      padding: EdgeInsets.all(10),
                                      scrollDirection: Axis.vertical,
                                      itemCount: teachers.length,
                                      itemBuilder: (_, index) =>
                                          _buildListGuru(index, teachers)),
                                ],
                              );
                            },
                          ));
                    }),
              ),
              // AnimatedPositioned(
              //     duration: Duration(milliseconds: 400),
              //     bottom: isShowBottom ? showBottom : -2.5 * showBottom,
              //     child: SizedBox(
              //         width: MediaQuery.of(context).size.width,
              //         child: Center(
              //             child: Container(
              //                 width: 120,
              //                 height: 45,
              //                 decoration: BoxDecoration(
              //                     color: SiswamediaTheme.green,
              //                     borderRadius: radius(S.w)),
              //                 child: Row(
              //                     mainAxisAlignment: MainAxisAlignment.center,
              //                     children: [
              //                       Icon(Icons.filter_list_sharp,
              //                           color: Colors.white),
              //                       SizedBox(width: 10),
              //                       CustomText('Filter', Kind.heading4)
              //                     ])))))
            ],
          )),
    );
  }

  Widget _buildListGuru(int index, List<TeacherClassroomModel> teachers) {
    return CardDaftarGuru(data: teachers[index], teacherState: teacherState);
  }
}

class ListTeacher extends StatelessWidget {
  final int id;
  final Orientation orientation;
  ListTeacher({this.id, this.orientation});

  @override
  Widget build(BuildContext context) {
    // var classProvider = context.watch<ClassProvider>();
    final teacherState = GlobalState.teachers();
    var isPortrait = orientation == Orientation.portrait;
    return Container(
        margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 24),
        child: FutureBuilder<TeachersClassroomModel>(
            future: ClassServices.getTeacherClassroom(id: id),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var count = snapshot.data.data.length;
                if (count > 0) {
                  return GridView.count(
                      padding: EdgeInsets.all(10),
                      physics: ScrollPhysics(),
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 5,
                      crossAxisCount: isPortrait ? 2 : 4,
                      shrinkWrap: true,
                      childAspectRatio: .7,
                      scrollDirection: Axis.vertical,
                      children: teacherState.state.teacherModel.data
                          .map((e) => CardDaftarGuru(data: e))
                          .toList());
                } else {
                  return Center(child: Text('Data Masih Kosong...'));
                }
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }

  void dialog(BuildContext context, TeacherClassroomModel data) {
    showDialog<void>(
        context: context,
        builder: (context) => StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              var role = someCapitalizedString(data.role);
              return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Container(
                      height: S.w * .95,
                      width: S.w * .5,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Kelas $role',
                            style: semiBlack.copyWith(
                                fontSize: 20, fontWeight: FontWeight.w700),
                          ),
                          Container(
                              width: 80,
                              height: 80,
                              margin: EdgeInsets.symmetric(vertical: 20),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(data.image)),
                              )),
                          Text(data.fullName,
                              style: semiBlack.copyWith(
                                  fontSize: 20, fontWeight: FontWeight.w500)),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6)),
                                  child: InkWell(
                                    onTap: () async {
                                      if (data.email != '') {
                                        var email =
                                            'mailto:${data.email}?subject=News&body=Hai%20Siswamedia';
                                        if (await canLaunch(email)) {
                                          await launch(email);
                                        } else {
                                          throw 'Could not launch $email';
                                        }
                                      } else {
                                        CustomFlushBar.errorFlushBar(
                                            'Tidak ada email', context);
                                      }
                                    },
                                    child: Container(
                                      height: 50,
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 10),
                                      child: Center(
                                        child: Row(
                                          children: [
                                            Icon(Icons.email),
                                            SizedBox(width: 5),
                                            Text('Email', style: semiBlack),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )),
                              Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6)),
                                child: InkWell(
                                  onTap: () async {
                                    if (data.noTelp != '') {
                                      var telp = 'tel:${data.noTelp}';
                                      if (await canLaunch(telp)) {
                                        await launch(telp);
                                      } else {
                                        throw 'Could not launch $telp';
                                      }
                                    } else {
                                      CustomFlushBar.errorFlushBar(
                                          'Tidak ada no telepon', context);
                                    }
                                  },
                                  child: Container(
                                    height: 50,
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 10),
                                    child: Center(
                                      child: Row(
                                        children: [
                                          Icon(Icons.phone),
                                          SizedBox(width: 5),
                                          Text('Telepon', style: semiBlack),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ))
                        ],
                      )));
            }));
  }
}
