part of '_forum.dart';

class DetailForum extends StatefulWidget {
  DetailForum({this.id, this.amountComment, this.isClass});
  final int id;
  final int amountComment;
  final bool isClass;

  @override
  _DetailForumState createState() => _DetailForumState();
}

class _DetailForumState extends State<DetailForum> with SingleTickerProviderStateMixin {
  final authState = Injector.getAsReactive<AuthState>();
  final forumState = Injector.getAsReactive<ForumState>();
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();

  var scaffoldKey = GlobalKey<ScaffoldState>();
  var fNodeChat = FocusNode();
  var status = <int, bool>{};
  var parentId = 0;
  var length = 0;
  var detailTime = ['Hari ini', 'Kemarin', 'Hari lalu'];
  var start = DateTime.now();
  var isEdit = false;
  var error = false;
  var commentReply = <int, String>{};
  var commentModelEdit = <int, int>{};
  var profile = Injector.getAsReactive<ProfileState>().state.profile;

  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey;
  AnimationController _animationController;
  TextEditingController _textController;

  double _progressValue = 0;
  int _progressPercentValue = 0;

  @override
  void initState() {
    super.initState();
    _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    _animationController = AnimationController(duration: Duration(milliseconds: 400), vsync: this);
    _textController = TextEditingController();
    Initializer(
      rIndicator: _refreshIndicatorKey,
      state: false,
      cacheKey: 'time_detail_forum',
      reactiveModel: forumState,
    ).initialize();
  }

  Future<void> retrieveData() async {
    await forumState.setState((s) => s.fetchChosenForum(widget.id));
    forumState.notify();
  }

  @override
  Widget build(BuildContext context) {
    // var forum = context.watch<ForumProvider>();
    var data = forumState.state.chosenForum;
    var date = DateTime.parse(data.postingDate).toLocal();
    return WillPopScope(
      onWillPop: () {
        if (fNodeChat.hasFocus) {
          fNodeChat.unfocus();
          return Future.value(false);
        }
        return Future.value(true);
      },
      child: Scaffold(
        key: scaffoldKey,
        appBar: isEdit ? editAppBar(data: data.comments) : appbar(data: data, date: date),
        backgroundColor: Colors.white,
        body: StateBuilder<ForumState>(
            observe: () => forumState,
            builder: (context, _) {
              // print(snapshot.state.chosenForum.comments);
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: retrieveData,
                child: WhenRebuilder<ForumState>(
                    observe: () => forumState,
                    onIdle: () => WaitingView(),
                    onWaiting: () => WaitingView(),
                    onError: (dynamic error) => ErrorView(error: error),
                    onData: (snapshot) {
                      var data = snapshot.chosenForum;
                      List<ForumAttachments> files;
                      List<ForumAttachments> images;
                      if (data.forumAttachments != null) {
                        files = data.forumAttachments
                            .where((element) => (!element.fileType.contains('png') &&
                                !element.fileType.contains('jpg') &&
                                !element.fileType.contains('jpeg')))
                            .toList();
                        images = data.forumAttachments
                            .where((element) => (element.fileType.contains('png') ||
                                element.fileType.contains('jpg') ||
                                element.fileType.contains('jpeg')))
                            .toList();
                      }

                      return Container(
                        height: MediaQuery.of(context).size.height,
                        child: Stack(
                          children: [
                            SingleChildScrollView(
                              child: Column(
                                children: <Widget>[
                                  ForumTitle(title: data.judulForum),
                                  ForumDescription(desc: data.deskripsi),
                                  SizedBox(height: 15),
                                  if (data.forumAttachments != null && files.isNotEmpty)
                                    _buildListAttachmentFile(files),
                                  SizedBox(height: 10),
                                  if (data.forumAttachments != null && images.isNotEmpty)
                                    _buildListAttachmentImage(images),
                                  Divider(color: Colors.grey),
                                  TextCountComment(
                                      comments: data.comments, initial: widget.amountComment),
                                  Divider(color: Colors.grey),
                                  if (data.comments != null && data.comments.isNotEmpty)
                                    _buildListComment(data),
                                  SizedBox(
                                    height: 120,
                                  ),
                                ],
                              ),
                            ),
                            CommentBar(
                              upload: () async {
                                await uploadFile(_setUploadProgress, context);
                              },
                              onTapClear: () {
                                setState(() {
                                  commentReply.clear();
                                });
                              },
                              isReply: commentReply.isNotEmpty,
                              replyName: commentReply.isNotEmpty ? commentReply.values.first : ' ',
                              value: _progressValue,
                              commentController: _textController,
                              fNodeComment: fNodeChat,
                              onTap: () async => await _giveComment(),
                            )
                          ],
                        ),
                      );
                    }),
              );
            }),
      ),
    );
  }

  Widget _buildListComment(ForumDetailItem data) {
    print('data---');
    print(data.comments);
    return Column(
      children: (data.comments.where((element) => element.parentCommentId == 0)).map((e) {
        var subComment = data.comments.where((element) => e.iD == element.parentCommentId);
        return CommentList(
          data: data,
          comments: e,
          onTap: (comments) {
            if (isEdit) {
              _configComment(comments);
            }
          },
          onLongPress: (comments) => _configComment(comments),
          onTapReply: () => _onTapReply(e),
          onTapSubComment: () => _onTapSubComment(e),
          status: status[e.iD],
          isSelected: commentModelEdit.containsKey(e.iD),
          commentModelEdit: commentModelEdit,
        );
      }).toList(),
    );
  }

  Widget _buildListAttachmentFile(List<ForumAttachments> files) {
    return Column(
        children: files
            .map((e) => AttachmentFile(
                  filename: e.fileName,
                  onTap: () async {
                    await launch(e.fileLink);
                  },
                ))
            .toList());
  }

  Widget _buildListAttachmentImage(List<ForumAttachments> images) {
    return Container(
      height: S.w * .5,
      child: ListView(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        children: images.map((e) => AttachmentImage(link: e.fileLink.substring(31, 64))).toList(),
      ),
    );
  }

  AppBar editAppBar({List<Comments> data}) {
    return AppBar(
        brightness: Brightness.dark,
        shadowColor: Colors.grey.shade50,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () => setState(() {
                  isEdit = false;
                  commentModelEdit = <int, int>{};
                })),
        centerTitle: false,
        backgroundColor: SiswamediaTheme.green,
        actions: [
          DeleteIconButton(onTap: () async {
            fNodeChat.unfocus();
            SiswamediaTheme.infoLoading(context: context, info: 'Sedang Menghapus');
            await deleteBatchComment(data: data).then((value) async {
              await forumState.state
                  .fetchChosenForum(widget.id)
                  .then((_) => Navigator.pop(context));
              forumState.notify();
              setState(() {
                isEdit = false;
                commentModelEdit = <int, int>{};
              });
              // await forumState.setState(
              //   (s) => s.fetchChosenForum(widget.id).then((value) {
              //     Navigator.pop(context);
              //   }),
              // );
            });
          })
        ],
        title: Text(
          '${commentModelEdit.length} Dipilih',
          style: semiWhite.copyWith(fontSize: S.w / 24),
        ));
  }

  AppBar appbar({ForumDetailItem data, DateTime date}) {
    return AppBar(
        brightness: Brightness.light,
        shadowColor: Colors.grey.shade50,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            padding: EdgeInsets.all(0),
            icon: Icon(Icons.more_horiz),
            onPressed: () {
              fNodeChat.unfocus();
              //todo penting
              // more(context, widget.id);
              Navigator.pop(context);
            },
            color: Colors.black,
          )
        ],
        title: Row(children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Image(
              height: 40,
              width: 40,
              fit: BoxFit.cover,
              image: NetworkImage(data.image ?? ''),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  text: TextSpan(
                      text: '${data.fullName}',
                      style: secondHeadingBlack.copyWith(fontSize: S.w / 26)),
                ),
                Text(
                    '${DateTime.now().difference(date).inDays == 0 && DateTime.now().day - date.day == 0 ? "Hari ini" : start.yearMonthDay.difference(date.yearMonthDay).inDays < 2 ? "Kemarin" : start.difference(date).inDays.toString() + " Hari yang lalu"} ${date.hour < 10 ? "0" : ""}${date.hour}.${date.minute < 10 ? "0" : ""}${date.minute}',
                    style: descBlack.copyWith(fontSize: S.w / 34)),
              ],
            ),
          )
        ]));
  }

  Widget _buildReply() {
    return Container();
  }

  void _onTapSubComment(Comments comments) {
    setState(() {
      status[comments.iD] = true;
    });
    // if (profile.userId == comments.userId) {
    //   if (!commentModelEdit.containsKey(comments.iD)) {
    //     commentModelEdit[comments.iD] = comments.userId;
    //     if (commentModelEdit.isNotEmpty) {
    //       setState(() {
    //         isEdit = true;
    //       });
    //     }
    //   } else {
    //     commentModelEdit.remove(comments.iD);
    //     if (commentModelEdit.isEmpty) {
    //       setState(() {
    //         isEdit = false;
    //       });
    //     } else {
    //       setState(() {});
    //     }
    //   }
    // } else {
    //   CustomFlushBar.errorFlushBar(
    //     'Tidak bisa mengedit  komentar orang lain',
    //     context,
    //   );
    // }
  }

  void _onTapReply(Comments comments) {
    setState(() {
      commentReply.clear();
      commentReply[comments.iD] = comments.fullName;
      // _textController.text = '@${comments.iD} ';
    });
  }

  void _configComment(Comments comments) {
    //todo userId
    if (profile.id == comments.userId) {
      if (!commentModelEdit.containsKey(comments.iD)) {
        commentModelEdit[comments.iD] = comments.userId;
        if (commentModelEdit.isNotEmpty) {
          setState(() {
            isEdit = true;
          });
        }
      } else {
        commentModelEdit.remove(comments.iD);
        if (commentModelEdit.isEmpty) {
          setState(() {
            isEdit = false;
          });
        } else {
          setState(() {});
        }
      }
    } else {
      CustomFlushBar.errorFlushBar(
        'Tidak bisa mengedit  komentar orang lain',
        context,
      );
    }
  }

  Future<void> _giveComment({String image}) async {
    if (_textController.text.isNotEmpty || image != null) {
      try {
        var id = commentReply.keys.first;
        var parentId = id;
        print('commented');
        var comment = image ?? _textController.text;
        var data = ForumCommentModel(
          commentDate: DateTime.now().toUtc().toIso8601String(),
          comments: comment,
          parentCommentId: parentId,
        );
        setState(() => commentReply.clear());
        await ForumServices.comment(id: widget.id, reqBody: data);
        await forumState.state.fetchChosenForum(widget.id);
        forumState.notify();
      } catch (e) {
        print(e);
        var data = ForumCommentModel(
          commentDate: DateTime.now().toUtc().toIso8601String(),
          comments: image ?? _textController.text,
          parentCommentId: 0,
        );
        await ForumServices.comment(id: widget.id, reqBody: data);
        await forumState.state.fetchChosenForum(widget.id);
        forumState.notify();
      }
      _textController.clear();
    }
  }

  Future<void> deleteBatchComment({List<Comments> data}) async {
    commentModelEdit.forEach((key, value) async {
      data.forEach((element) async {
        if (element.parentCommentId == key) {
          await deleteComment(element.iD);
        }
      });
      await deleteComment(key);
    });
  }

  static HttpClient getHttpClient() {
    var httpClient = HttpClient()
      ..connectionTimeout = const Duration(seconds: 10)
      ..badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    return httpClient;
  }

  Future<void> uploadFile(OnUploadProgressCallback onUploadProgress, BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    var pick = await FilePicker.platform.pickFiles(
        allowMultiple: false, type: FileType.custom, allowedExtensions: ['jpg', 'png', 'jpeg']);
    if (pick != null) {
      try {
        // ignore: unawaited_futures
        showDialog<void>(
            context: context,
            builder: (_) {
              return Center(child: CircularProgressIndicator());
            });
        var file = pick.files.first;

        var client2 = http.Client();
        if (file != null) {
          var totalByteLength = File(file.path).lengthSync();
          var type = lookupMimeType(file.path);
          SiswamediaGoogleSignIn.getInstance().scopes.addAll([
            'https://www.googleapis.com/auth/drive.file',
          ]);
          await SiswamediaGoogleSignIn.getInstance().signInSilently();
          var headers = await SiswamediaGoogleSignIn.getInstance().currentUser.authHeaders;
          var requestHeaders = <String, String>{
            'X-Upload-Content-Type': type,
            'Content-Type': 'application/json',
            'X-Upload-Content-Length': '$totalByteLength',
          };
          headers.forEach((key, value) {
            requestHeaders[key] = value;
          });
          var resp =
              await client2.post('$baseGoogleApiUrl/upload/drive/v3/files?uploadType=resumable',
                  body: json.encode(<String, int>{
                    'Content-Length': totalByteLength,
                  }),
                  headers: requestHeaders);

          final client = HttpClient();
          final response = await client.putUrl(Uri.parse(resp.headers['location']));

          var byteCount = 0;

          response.contentLength = totalByteLength;
          print('content lenght ${response.contentLength}');
          response.headers.set(HttpHeaders.contentTypeHeader, type);
          response.headers.set(HttpHeaders.contentLengthHeader, totalByteLength);
          response.headers.set('Content-Range', 'bytes 0-${totalByteLength - 1}/$totalByteLength');

          headers.forEach((key, value) {
            response.headers.add(key, value);
          });

          final stream = File(file.path).openRead();

          Navigator.pop(context);
          var stream2 = stream.transform<List<int>>(
            StreamTransformer.fromHandlers(
              handleData: (data, sink) {
                byteCount += data.length;
                print(byteCount);
                sink.add(data);
                if (onUploadProgress != null) {
                  print('uploading');
                  onUploadProgress(byteCount, totalByteLength, context);
                  // CALL STATUS CALLBACK;
                }
              },
              handleError: (error, stack, sink) {},
              handleDone: (sink) {
                sink.close();
              },
            ),
          );

          print('masuk');
          // var streamUpload = msStream.transform<List<int>>(
          //   StreamTransformer.fromHandlers(
          //     handleData: (data, sink) {
          //       sink.add(data);
          //
          //       byteCount += data.length;
          //
          //       if (onUploadProgress != null) {
          //         print('uploading');
          //         onUploadProgress(byteCount, totalByteLength, context);
          //         // CALL STATUS CALLBACK;
          //       }
          //     },
          //     handleError: (error, stack, sink) {
          //       print('error sink');
          //       throw error;
          //     },
          //     handleDone: (sink) {
          //       print('done');
          //       sink.close();
          //       // UPLOAD DONE;
          //     },
          //   ),
          // );

          await response.addStream(stream2);

          final httpResponse = await response.close();
          var statusCode = httpResponse.statusCode;

          print(statusCode);
          if (statusCode ~/ 100 != 2) {
            print(statusCode);
            throw SiswamediaException(
                'Error uploading file, Status code: ${httpResponse.statusCode}');
          } else {
            return await readResponseAsString(httpResponse, file.name, headers);
          }
          // return response;
        } else {
          progressUploadState.state.progress = 0;
          // return null;
        }
      } on LargeFileException catch (error) {
        progressUploadState.state.progress = 0;
        Navigator.pop(context);
        CustomFlushBar.errorFlushBar(
          error.cause,
          context,
        );
      } on ErrorUploadException catch (error) {
        progressUploadState.state.progress = 0;
        Navigator.pop(context);
        CustomFlushBar.errorFlushBar(
          error.cause,
          context,
        );
      } catch (e) {
        progressUploadState.state.progress = 0;
        Navigator.pop(context);
        CustomFlushBar.errorFlushBar(
          error is SiswamediaException ? (e as SiswamediaException).message : e.toString(),
          context,
        );
        print(e);
      }
    }
    // try {
  }

  Future<String> readResponseAsString(
      HttpClientResponse response, String filename, Map<String, String> headers) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
    }, onDone: () async {
      completer.complete(contents.toString());
      var result = await completer.future;
      var data = json.decode(result) as Map<String, dynamic>;
      var client = http.Client();
      print(data['id']);
      var resp = await client.patch('$baseGoogleApiUrl/drive/v3/files/${data['id']}?fields=*',
          body: json.encode({'name': filename}), headers: headers);
      print(resp.body);
      await client
          .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
              body: json.encode({'role': 'reader', 'type': 'anyone'}), headers: headers)
          .then((value) {
        print(value.body);
        _giveComment(image: 'https://drive.google.com/uc?export=view&id=${data['id']}');
        CustomFlushBar.successFlushBar('Berhasil Mengupload', context);
        setState(() => _progressValue = 0);
        progressUploadState.setState((s) => s.setProgress(0), silent: true);
      });
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes), silent: true);
    progressUploadState.setState((s) => s.setContent(totalBytes), silent: true);
    var __progressValue = Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }
}

class CommentModelEdit {
  int id;
  int userId;

  CommentModelEdit({this.id, this.userId});
}

Future<GeneralResultAPI> deleteComment(int id) async {
  var url = '$baseUrl/$version/forum/comment/$id';

  //TODO exception Handler [TimeoutException, SocketException]
  try {
    var client = http.Client();

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);

    await client.delete(url, headers: {
      'Authorization': token,
    }).timeout(Duration(seconds: 10),
        onTimeout: () => throw TimeoutException('Timeout, try Again'));

    return GeneralResultAPI(statusCode: StatusCodeConst.success, message: 'Berhasil');
  } on TimeoutException catch (e) {
    print(e.message);
    return GeneralResultAPI(statusCode: StatusCodeConst.timeout, message: 'Timeout');
  } on SocketException {
    print('Tidak ada Internet');
    return GeneralResultAPI(statusCode: StatusCodeConst.noInternet, message: 'No Internet');
  } catch (e) {
    return GeneralResultAPI(statusCode: StatusCodeConst.general, message: 'Try again');
  }
}
