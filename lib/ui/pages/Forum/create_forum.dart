part of '_forum.dart';

class CreateForum extends StatefulWidget {
  final bool isClass;

  CreateForum({this.isClass = false});

  @override
  _CreateForumState createState() => _CreateForumState();
}

class _CreateForumState extends State<CreateForum> {
  final authState = Injector.getAsReactive<AuthState>();
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();
  final forumState = Injector.getAsReactive<ForumState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController controllerTitle = TextEditingController();
  TextEditingController controllerDesc = TextEditingController();

  FocusNode fNodeTitle = FocusNode();
  FocusNode fNodeDesc = FocusNode();

  List<MainAttachments> attachments = [];

  bool imageAdded = false;
  bool fileAdded = false;
  bool loading = false;

  List<Map<FileData, Uint8List>> image = <Map<FileData, Uint8List>>[];
  List<Map<FileData, Uint8List>> files = <Map<FileData, Uint8List>>[];

  double _progressValue = 0;
  int _progressPercentValue = 0;

  void setAttachments(DataFile file) {
    attachments.add(MainAttachments(file: file.file, fileName: file.fileName));
    setState(() {
      attachments = attachments;
    });
  }

  void removeAttachments(int index) {
    attachments.removeAt(index);
    setState(() {
      attachments = attachments;
    });
  }

  Future<void> uploadImage(
      BuildContext context, Map<FileData, Uint8List> data) async {
    await uploadFile(_setUploadProgress, context, data, 0);
  }

  Future<void> uploadFileForum(
      BuildContext context, Map<FileData, Uint8List> data) async {
    await uploadFile(_setUploadProgress, context, data, 1);
  }

  // Future<void> getAll() async {
  //   for (var data in attachments) {
  //     listFile.add(ForumCreateAttachments(
  //       fileName: data.fileName,
  //       fileLink: data.file,
  //       // fileName: data.keys.first.name,
  //     ));
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    // var forum = Provider.of<ForumProvider>(context);
    var progress = progressUploadState.state;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white, statusBarBrightness: Brightness.light));
    var auth = authState.state;
    return WillPopScope(
      onWillPop: () {
        if (fNodeTitle.hasFocus || fNodeDesc.hasFocus) {
          fNodeDesc.unfocus();
          fNodeTitle.unfocus();
          return Future.value(true);
        }
        progressUploadState.setState((s) => s.setProgress(0), silent: true);
        return Future.value(true);
      },
      child: Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          brightness: Brightness.light,
          shadowColor: Colors.grey.shade50,
          leading: IconButton(
              icon: Icon(
                Icons.clear,
                color: SiswamediaTheme.green,
              ),
              onPressed: () {
                progressUploadState.setState((s) => s.setProgress(0));
                Navigator.pop(context);
              }),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Buat Forum',
              style: TextStyle(color: Colors.black, fontSize: S.w / 22)),
        ),
        body: GestureDetector(
          onTap: () {
            if (fNodeDesc.hasFocus || fNodeTitle.hasFocus) {
              fNodeTitle.unfocus();
              fNodeDesc.unfocus();
            }
          },
          child: Stack(
            children: [
              ListView(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(vertical: S.w * .03),
                          child: TextField(
                              focusNode: fNodeTitle,
                              controller: controllerTitle,
                              onTap: () {
                                if (fNodeDesc.hasFocus) {
                                  fNodeDesc.unfocus();
                                }
                                fNodeTitle.requestFocus();
                              },
                              decoration: InputDecoration(
                                  hintText: 'Judul Forum',
                                  hintStyle: descBlack.copyWith(
                                      fontSize: S.w / 26,
                                      color: Colors.grey.withOpacity(.5)),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: SiswamediaTheme.green)))),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: S.w * .03),
                          child: Text(
                            'Deskripsi Forum',
                            style: TextStyle(
                              fontFamily: 'OpenSans',
                              fontSize: S.w / 26,
                              color: const Color(0xff4b4b4b),
                              fontWeight: FontWeight.w600,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: S.w * .03),
                          child: Container(
                            constraints: BoxConstraints(
                              maxHeight: S.w,
                              minHeight: S.w * .3,
                            ),
                            child: TextField(
                              focusNode: fNodeDesc,
                              minLines: 3,
                              controller: controllerDesc,
                              maxLines: null,
                              onChanged: (String txt) {},
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black54,
                              ),
                              cursorColor: Colors.blue,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          color: SiswamediaTheme.border)),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          color: SiswamediaTheme.border)),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          color: SiswamediaTheme.green)),
                                  hintText: 'Caption...'),
                            ),
                          ),
                        ),
                        ImageForumBuilder(
                          image: image,
                          isAdded: imageAdded,
                          onRemove: (val) {
                            if (image.length == 1) {
                              setState(() {
                                imageAdded = false;
                                image.remove(val);
                                var data = attachments.firstWhere((element) =>
                                    element.fileName == val.keys.first.name);
                                attachments.remove(data);
                              });
                            } else {
                              setState(() {
                                image.remove(val);
                                var data = attachments.firstWhere((element) =>
                                    element.fileName == val.keys.first.name);
                                attachments.remove(data);
                              });
                            }
                          },
                          onTap: () async {
                            SiswamediaTheme.loading(context);
                            var a = await getFile(
                                isImage: true,
                                context: context,
                                extension: ['jpeg', 'jpg', 'png']);

                            if (a != null) {
                              await uploadImage(context, a);
                            } else {
                              pop(context);
                            }
                          },
                        ),
                        FileForumBuilder(
                          files: files,
                          isAdded: fileAdded,
                          onRemove: (val) {
                            if (files.length == 1) {
                              setState(() {
                                fileAdded = false;
                                files.remove(val);
                                var data = attachments.firstWhere((element) =>
                                    element.fileName == val.keys.first.name);
                                attachments.remove(data);
                              });
                            } else {
                              setState(() {
                                files.remove(val);
                                var data = attachments.firstWhere((element) =>
                                    element.fileName == val.keys.first.name);
                                attachments.remove(data);
                              });
                            }
                          },
                          onTap: () async {
                            SiswamediaTheme.loading(context);
                            var a = await getFile(
                                context: context,
                                extension: ['pdf', 'xlsx', 'ppt', 'pptx']);
                            if (a != null) {
                              await uploadFileForum(context, a).then((value) {
                                print('toolkit');
                              });
                            } else {
                              pop(context);
                            }
                          },
                        ),
                        SizedBox(height: 10),
                        progress.progress == 0
                            ? SizedBox()
                            : Container(
                                width: S.w,
                                margin: EdgeInsets.symmetric(
                                    // horizontal: width * 0.05,
                                    vertical: 3),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey[300]),
                                    borderRadius: BorderRadius.circular(5),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: SiswamediaTheme.grey
                                              .withOpacity(0.2),
                                          offset: const Offset(0.2, 0.2),
                                          blurRadius: 3.0),
                                    ],
                                    color: Colors.white),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          'Uploading... $_progressPercentValue%'),
                                      SizedBox(height: 5),
                                      LinearPercentIndicator(
                                        width: S.w - 35 * 2,
                                        lineHeight: 8.0,
                                        percent: _progressValue,
                                        backgroundColor: Colors.grey[300],
                                        progressColor: SiswamediaTheme.green,
                                      ),
                                      SizedBox(height: 5),
                                      Container(
                                        width: S.w * .8,
                                        child: Text(
                                          '${Format.formatByte(progress.progress)}/${Format.formatByte(progress.contentLength)}',
                                          textAlign: TextAlign.right,
                                        ),
                                      ),
                                    ])),
                        SizedBox(height: S.w * .2),
                        loading
                            ? Center(child: CircularProgressIndicator())
                            : Padding(
                                padding: EdgeInsets.only(
                                    bottom: S.w * .04,
                                    left: S.w * .04,
                                    right: S.w * .04),
                                child: CustomButton(
                                  title: 'Tambahkan Forum',
                                  onPressed: () async {
                                    if (progress.progress != 0) {
                                      return CustomFlushBar.errorFlushBar(
                                        'Ada file yang sedang di upload',
                                        context,
                                      );
                                    } else if (controllerTitle.text == '') {
                                      await popCreate(context,
                                          'Pastikan judul forum tidak kosong');
                                    } else if (controllerDesc.text == '') {
                                      await popCreate(context,
                                          'Pastikan deskripsi forum tidak kosong');
                                    } else {
                                      try {
                                        setState(() {
                                          loading = true;
                                        });
                                        var listFile =
                                            <ForumCreateAttachments>[];
                                        for (var data in attachments) {
                                          listFile.add(ForumCreateAttachments(
                                            fileName: data.fileName,
                                            fileLink: data.file,
                                            fileType:
                                                data.fileName.split('.').last,
                                            // fileName: data.keys.first.name,
                                          ));
                                        }
                                        print('---listfile---');
                                        print(listFile);

                                        var ok = ForumCreateModel(
                                            judulForum: controllerTitle.text,
                                            deskripsi: controllerDesc.text,
                                            postingDate: DateTime.now()
                                                .toUtc()
                                                .toIso8601String(),
                                            schoolId:
                                                auth.currentState.schoolId,
                                            forumAttachments: listFile,
                                            classId: widget.isClass
                                                ? authState
                                                    .state.currentState.classId
                                                : 0);

                                        await ForumServices.createForum(
                                                reqBody: ok)
                                            .then((value) async {
                                          if (value
                                              .statusCode.isSuccessOrCreated) {
                                            await progressUploadState.setState(
                                                (s) => s.setProgress(0),
                                                silent: true);
                                            await forumState.state.fetchItems(
                                                authState.state.currentState
                                                    .schoolId,
                                                widget.isClass);
                                            await Navigator.pop(context);
                                            setState(() {
                                              loading = false;
                                            });
                                            await forumState.notify();
                                            return CustomFlushBar
                                                .successFlushBar(
                                              'Berhasil Membuat Forum',
                                              context,
                                            );
                                          } else {
                                            await popCreate(context,
                                                'Terjadi Kesalahan saat membuat forum');
                                            setState(() {
                                              loading = false;
                                            });
                                          }
                                        });
                                      } on UploadImageFailed {
                                        CustomFlushBar.errorFlushBar(
                                          'Gagal upload gambar',
                                          context,
                                        );
                                      } on UploadFileFailed {
                                        CustomFlushBar.errorFlushBar(
                                          'Gagal upload file',
                                          context,
                                        );
                                      } on LargeFileException {
                                        CustomFlushBar.errorFlushBar(
                                          'File lebih besar dari 3mb',
                                          context,
                                        );
                                      } on ErrorUploadException {
                                        CustomFlushBar.errorFlushBar(
                                          'Gagal upload',
                                          context,
                                        );
                                      }
                                    }
                                  },
                                  height: S.w * .12,
                                  width: MediaQuery.of(context).size.width * .9,
                                ),
                              ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  static HttpClient getHttpClient() {
    var httpClient = HttpClient()
      ..connectionTimeout = const Duration(seconds: 10)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);

    return httpClient;
  }

  Future<String> readResponseAsString(HttpClientResponse response,
      String filename, Map<String, String> headers) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    // var progress = Provider.of<ProgressProvider>(context, listen: false);
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
    }, onDone: () async {
      print('isi konten');
      print(contents.toString());
      completer.complete(contents.toString());
      var result = await completer.future;
      var data = json.decode(result) as Map<String, dynamic>;
      var client = http.Client();
      print(data);

      print(data['id']);
      await client
          .patch('$baseGoogleApiUrl/drive/v3/files/${data['id']}',
              body: json.encode({'name': filename}), headers: headers)
          .then((value) async {
        await client
            .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
                body: json.encode({'role': 'reader', 'type': 'anyone'}),
                headers: headers)
            .then((value) {
          print(value.body);
          CustomFlushBar.successFlushBar(
              'Berhasil Mengupload Lampiran', context);
          setAttachments(
              DataFile(fileName: filename, file: data['webContentLink']));
          progressUploadState.setState((s) => s.setProgress(0));
        });
      });
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    // var progress = Provider.of<ProgressProvider>(context, listen: false);
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes), silent: true);
    progressUploadState.setState((s) => s.setContent(totalBytes), silent: true);
    // progress.setProgress(sentBytes);
    // progress.setContent(totalBytes);
    var __progressValue =
        Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }

  Future<void> uploadFile(OnUploadProgressCallback onUploadProgress,
      BuildContext context, Map<FileData, Uint8List> fileData, int type) async {
    var prefs = await SharedPreferences.getInstance();
    var path = fileData.keys.first.path;
    var token = prefs.getString('auth_siswamedia_api');
    // var provider = Provider.of<ProgressProvider>(context, listen: false);
    try {
      if (path != null) {
        final stream = await File(path).openRead();
        var _googleSignIn = GoogleSignIn(scopes: [
          'https://www.googleapis.com/auth/drive.file',
        ]);
        await _googleSignIn.signInSilently();

        final httpClient = getHttpClient();
        //3145727
        if (File(path).lengthSync() > 3145727) {
          throw Exception('Data lebih dari 3Mb');
        }

        final client = HttpClient();
        final response = await client.postUrl(Uri.parse(
            '$baseGoogleApiUrl/upload/drive/v3/files?uploadType=media&fields=*'));

        var byteCount = 0;
        // var type = lookupMimeType(path);
        response.contentLength = File(path).lengthSync();
        var totalByteLength = response.contentLength;
        var headers = await _googleSignIn.currentUser.authHeaders;
        headers.forEach((key, value) {
          response.headers.add(key, value);
        });
        pop(context);
        var stream2 = stream.transform<List<int>>(
          StreamTransformer.fromHandlers(
            handleData: (data, sink) {
              byteCount += data.length;
              print(byteCount);
              sink.add(data);
              if (onUploadProgress != null) {
                print('uploading');
                onUploadProgress(byteCount, totalByteLength, context);
                // CALL STATUS CALLBACK;
              }
            },
            handleError: (error, stack, sink) {
              print(error);
            },
            handleDone: (sink) {
              sink.close();
            },
          ),
        );

        print('masuk');
        await response.addStream(stream2);

        final httpResponse = await response.close();
        print('keluar');

//
        var statusCode = httpResponse.statusCode;
        print(statusCode);
        // final completer = Completer<String>();
        // final contents = StringBuffer();
        // httpResponse.transform(utf8.decoder).listen((data) {
        //   contents.write(data);
        // }, onDone: () => completer.complete(contents.toString()));
        // print(await completer.future);

        if (statusCode ~/ 100 != 2) {
          print(statusCode);
          throw Exception(
              'Error uploading file, Status code: ${httpResponse.statusCode}');
        } else {
          var filename = File(path).path.split('/').last;
          var completer = Completer<String>();
          var contents = StringBuffer();
          // var progress = Provider.of<ProgressProvider>(context, listen: false);
          headers['content-type'] = 'application/json';
          httpResponse.transform(utf8.decoder).listen((String data) {
            contents.write(data);
          }, onDone: () async {
            var result = contents.toString();
            var data = json.decode(result) as Map<String, dynamic>;
            var client = http.Client();
            print(data);
            print(data['id']);
            await client
                .patch('$baseGoogleApiUrl/drive/v3/files/${data['id']}',
                    body: json.encode({'name': filename}), headers: headers)
                .then((_) async {
              await client
                  .post(
                      '$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
                      body: json.encode({'role': 'reader', 'type': 'anyone'}),
                      headers: headers)
                  .then((value) {
                print(value.body);
                progressUploadState.setState((s) => s.setProgress(0),
                    silent: true);

                CustomFlushBar.successFlushBar(
                  'Berhasil Mengupload Lampiran',
                  context,
                );
                setState(() {
                  if (type == 1) {
                    files.add(fileData);
                    fileAdded = true;
                  } else {
                    image.add(fileData);
                    imageAdded = true;
                  }
                });
                setAttachments(
                    DataFile(fileName: filename, file: data['webContentLink']));
                completer.complete(contents.toString());
              });
            });
            // print(await completer.future);
          });
          // return await readResponseAsString(
          //     httpResponse, File(path).path.split('/').last, headers);
        }
        // return response;
      } else {
        await progressUploadState.setState((s) => s.setProgress(0));
        // return null;
      }
    } on LargeFileException catch (error) {
      await progressUploadState.setState((s) => s.setProgress(0));
      pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
      throw LargeFileException();
    } on ErrorUploadException catch (error) {
      await progressUploadState.setState((s) => s.setProgress(0));
      pop(context);
      throw ErrorUploadException();
    } catch (e) {
      print(e);
      print(e.message);
      await progressUploadState.setState((s) => s.setProgress(0));
      pop(context);
      if (type == 0) {
        throw UploadImageFailed();
      } else {
        throw UploadFileFailed();
      }
    }
  }

  Future<String> readResponse(HttpClientResponse response) {
    final completer = Completer<String>();
    final contents = StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }
}

class ImageForumBuilder extends StatelessWidget {
  ImageForumBuilder({this.isAdded, this.onTap, this.onRemove, this.image});
  final bool isAdded;
  final Function onTap;
  final Function(Map<FileData, Uint8List>) onRemove;
  final List<Map<FileData, Uint8List>> image;

  @override
  Widget build(BuildContext context) {
    if (isAdded) {
      return Container(
        width: double.infinity,
        height: S.w * .18,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            Row(
                children: image
                    .map((e) => Container(
                        width: S.w * .16,
                        height: S.w * .16,
                        margin:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: Stack(children: [
                          AspectRatio(
                            aspectRatio: 1,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.file(File(e.keys.first.path),
                                  fit: BoxFit.cover),
                            ),
                          ),
                          Positioned(
                              top: 5,
                              right: 5,
                              child: InkWell(
                                  onTap: () => onRemove(e),
                                  child: Icon(Icons.clear))),
                        ])))
                    .toList()),
            Container(
                width: S.w * .16,
                height: S.w * .16,
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(color: SiswamediaTheme.green, width: 2),
                ),
                child: InkWell(
                    onTap: onTap,
                    child: Center(
                        child: Icon(Icons.add, color: SiswamediaTheme.green)))),
          ]),
        ),
      );
    }
    return Container(
      margin: EdgeInsets.symmetric(vertical: S.w * .03),
      width: S.w * .5,
      height: S.w * .12,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: const Color(0xffffffff),
        boxShadow: [
          BoxShadow(
            color: const Color(0x29000000),
            offset: Offset(0, 0),
            blurRadius: 2,
          ),
        ],
      ),
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(Icons.camera_alt, color: SiswamediaTheme.green),
              SizedBox(width: 15),
              Text(
                'Unggah Foto',
                style: TextStyle(
                  fontFamily: 'Open Sans',
                  fontSize: 12,
                  color: const Color(0xff4b4b4b),
                ),
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FileForumBuilder extends StatelessWidget {
  FileForumBuilder({this.isAdded, this.onTap, this.files, this.onRemove});
  final bool isAdded;
  final Function onTap;
  final Function(Map<FileData, Uint8List>) onRemove;
  final List<Map<FileData, Uint8List>> files;

  @override
  Widget build(BuildContext context) {
    if (isAdded) {
      return Container(
        width: double.infinity,
        child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                Column(
                  children: files
                      .map((e) => Container(
                          margin: EdgeInsets.symmetric(vertical: S.w * .03),
                          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                          width: S.w * .9,
                          height: S.w * .12,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: const Color(0xffffffff),
                            boxShadow: [
                              BoxShadow(
                                color: const Color(0x29000000),
                                offset: Offset(0, 0),
                                blurRadius: 2,
                              ),
                            ],
                          ),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Text(
                                    e.keys.first.name,
                                    overflow: TextOverflow.ellipsis,
                                    style: semiBlack.copyWith(
                                        fontSize: S.w / 32,
                                        color: SiswamediaTheme.green),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    onRemove(e);
                                  },
                                  child: Text(
                                    'Hapus',
                                    style: semiBlack.copyWith(
                                        fontSize: S.w / 32, color: Colors.red),
                                  ),
                                ),
                              ])))
                      .toList(),
                ),
                ListTile(
                  leading: Container(
                      width: S.w * .1,
                      height: S.w * .1,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border:
                            Border.all(color: SiswamediaTheme.green, width: 2),
                      ),
                      child: InkWell(
                          onTap: onTap,
                          child: Center(
                              child: Icon(Icons.add,
                                  color: SiswamediaTheme.green)))),
                  title: Text(
                    'Tambahkan Lampiran',
                    style: semiBlack.copyWith(
                        fontSize: S.w / 32, color: Colors.grey.withOpacity(.4)),
                  ),
                )
              ],
            )),
      );
    }
    return Container(
      margin: EdgeInsets.symmetric(vertical: S.w * .03),
      width: S.w * .5,
      height: S.w * .12,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: const Color(0xffffffff),
        boxShadow: [
          BoxShadow(
            color: const Color(0x29000000),
            offset: Offset(0, 0),
            blurRadius: 2,
          ),
        ],
      ),
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(Icons.attach_file, color: SiswamediaTheme.green),
              SizedBox(width: 15),
              Text(
                'Unggah Lampiran',
                style: TextStyle(
                  fontFamily: 'Open Sans',
                  fontSize: 12,
                  color: const Color(0xff4b4b4b),
                ),
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FileData {
  final String name;
  final String type;
  final String path;

  FileData({this.name, this.type, this.path});
}

class Mantap {
  final String filename;
  final Map<String, String> headers;
  final HttpClientResponse response;
  Mantap({this.filename, this.headers, this.response});
}
