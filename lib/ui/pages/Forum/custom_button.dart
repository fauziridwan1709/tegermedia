part of '_forum.dart';

class CustomButton extends StatelessWidget {
  CustomButton({this.title, this.onPressed, this.width, this.height});
  final String title;
  final double width;
  final double height;
  final Function onPressed;
  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: SiswamediaTheme.green,
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 0),
                spreadRadius: 2,
                blurRadius: 2,
                color: Colors.grey.withOpacity(.2))
          ]),
      child: Material(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(12),
          child: InkWell(
              onTap: onPressed,
              child: Center(
                  child: Text(title,
                      style:
                          secondHeadingWhite.copyWith(fontSize: S.w / 28))))),
    );
  }
}
