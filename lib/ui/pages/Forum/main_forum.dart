part of '_forum.dart';

class MainForum extends StatefulWidget {
  final bool isClass;
  MainForum({this.isClass = false});

  @override
  _MainForumState createState() => _MainForumState();
}

class _MainForumState extends State<MainForum> {
  final forumState = Injector.getAsReactive<ForumState>();
  final authState = Injector.getAsReactive<AuthState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  var title = TextEditingController();
  var desc = TextEditingController();
  var completer = Completer<Null>();
  var pref = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();
    Initializer(
      reactiveModel: forumState,
      rIndicator: refreshIndicatorKey,
      cacheKey: TIME_FORUMS + '${widget.isClass}',
      state: widget.isClass ? forumState.state.items2 != null : forumState.state.items != null,
    ).initialize();
  }

  Future<void> retrieveData() async {
    await forumState
        .setState((s) => s.fetchItems(authState.state.currentState.schoolId, widget.isClass));
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    // var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shadowColor: Colors.grey.shade50,
        brightness: Brightness.light,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('Forum', style: semiBlack.copyWith(color: Colors.black, fontSize: S.w / 22)),
      ),
      floatingActionButton: (widget.isClass
              ? authState.state.currentState.classRole.isGuruOrWaliKelas
              : authState.state.currentState.isAdmin)
          ? FloatingActionButton(
              elevation: 2.0,
              child: Icon(Icons.add),
              backgroundColor: SiswamediaTheme.green,
              onPressed: () => navigate(context, CreateForum(isClass: widget.isClass)))
          : SizedBox(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Stack(
        children: [
          StateBuilder<ForumState>(
              observe: () => forumState,
              builder: (context, snapshot) {
                return RefreshIndicator(
                    key: refreshIndicatorKey,
                    onRefresh: retrieveData,
                    child: WhenRebuilder<ForumState>(
                        observe: () => forumState,
                        onIdle: () => WaitingView(),
                        onWaiting: () => WaitingView(),
                        onError: (dynamic error) => ErrorView(error: error),
                        onData: (data) {
                          var items = widget.isClass ? data.items2 : data.items;
                          if (items.data.isEmpty) {
                            return ListView(
                              children: [
                                if (isPortrait)
                                  SizedBox(
                                    height: S.w * .3,
                                  ),
                                Image.asset('assets/images/no_forum.png',
                                    height: S.w / 2, width: S.w / 2),
                                Text(
                                  'Belum ada Forum',
                                  style: TextStyle(
                                    fontFamily: 'Open Sans',
                                    fontSize: S.w / 30,
                                    color: SiswamediaTheme.green,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            );
                          }
                          return ListView(children: [
                            ForumBuilder(
                                titleController: title,
                                descController: desc,
                                ratio: MediaQuery.of(context).size.aspectRatio,
                                orientation: orientation,
                                isClass: widget.isClass,
                                forumState: forumState),
                          ]);
                        }));
              })
        ],
      ),
    );
  }
}

class ForumBuilder extends StatelessWidget {
  ForumBuilder(
      {this.ratio,
      this.forumState,
      this.orientation,
      this.titleController,
      this.isClass,
      this.descController});
  final TextEditingController titleController;
  final TextEditingController descController;
  final double ratio;
  final Orientation orientation;
  final ReactiveModel<ForumState> forumState;
  final bool isClass;
  final String desc =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis ultricies urna. Donec pulvinar, magna vitae consequat feugiat, lectus elit maximus mauris, at consectetur arcu felis tempor enim. Vestibulum eu orci eu elit vestibulum facilisis. Integer mollis ornare magna, sed euismod mi. Praesent viverra posuere ligula eget dignissim.';

  final auth = GlobalState.auth().state;
  @override
  Widget build(BuildContext context) {
    var isPortrait = orientation == Orientation.portrait;
    var items = isClass ? forumState.state.items2 : forumState.state.items;
    return ListView(
      // crossAxisCount: gridCountHome(ratio),
      shrinkWrap: true,
      physics: ScrollPhysics(),
      // childAspectRatio: 2.4,
      children: items.data.map((e) {
        var date = DateTime.parse(e.postingDate).toLocal();
        var start = DateTime.now();
        return Container(
          margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .03),
          padding: EdgeInsets.symmetric(vertical: S.w * .03),
          decoration: SiswamediaTheme.whiteRadiusShadow,
          child: InkWell(
            onTap: () {
              forumState.setState((s) => s.choseForum(ForumDetailItem(
                  iD: e.iD,
                  judulForum: e.judulForum,
                  deskripsi: e.deskripsi,
                  image: e.image,
                  fullName: e.fullName,
                  postingDate: e.postingDate)));
              navigate(context, DetailForum(id: e.iD, amountComment: 0, isClass: isClass));
            },
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Row(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.network(
                            e.image,
                            fit: BoxFit.cover,
                            scale: 0.0000000000000000000000000001,
                            height: 40,
                            width: 40,
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: S.w * .5,
                              child: RichText(
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(
                                      text: '${e.judulForum}',
                                      style: secondHeadingBlack.copyWith(fontSize: S.w / 26))),
                            ),
                            Container(
                              width: S.w * .5,
                              child: Text(
                                  'Oleh ${e.fullName} ${DateTime.now().difference(date).inDays == 0 && DateTime.now().day - date.day == 0 ? "Hari ini" : start.yearMonthDay.difference(date.yearMonthDay).inDays < 2 ? "Kemarin" : start.difference(date).inDays.toString() + " Hari yang lalu"} ${date.hour < 10 ? "0" : ""}${date.hour}.${date.minute < 10 ? "0" : ""}${date.minute}',
                                  style: descBlack.copyWith(fontSize: S.w / 34)),
                            ),
                          ],
                        ),
                      ],
                    ),
                    if (auth.currentState.isAdmin)
                      IconButton(
                        icon: Icon(Icons.more_horiz),
                        onPressed: () {
                          more(context, e.iD, judul: e.judulForum, deskripsi: e.deskripsi);
                        },
                        color: Colors.black,
                      )
                  ]),
                ),
                Divider(color: Colors.grey),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .005),
                  child: Text(
                    e.deskripsi,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: descBlack.copyWith(fontSize: S.w / 32),
                  ),
                ),
                // AspectRatio(
                //   aspectRatio: 16 / 9,
                //   child: Container(
                //     width: S.w * .8,
                //     margin: EdgeInsets.symmetric(
                //         horizontal: S.w * .05, vertical: S.w * .03),
                //     decoration: BoxDecoration(
                //         borderRadius: BorderRadius.circular(12),
                //         image: DecorationImage(
                //             fit: BoxFit.cover,
                //             image: AssetImage('assets/images/dummy.png'))),
                //   ),
                // ),

                // Divider(color: Colors.grey),
                // Column(
                //   children: e.co,
                // )
                // ListTile(
                //   // leading: ClipRRect(
                //   //   borderRadius: BorderRadius.circular(12),
                //   //   child: Image.asset('assets/images/avatarred.png',
                //   //       height: 40, width: 40),
                //   // ),
                //   title: Column(
                //     crossAxisAlignment: CrossAxisAlignment.start,
                //     children: [
                //       Container(
                //         padding: EdgeInsets.symmetric(
                //             vertical: 8, horizontal: 12),
                //         margin: EdgeInsets.all(5),
                //         decoration: BoxDecoration(
                //           color: Color(0xfff8f8f8),
                //           borderRadius: BorderRadius.circular(8),
                //         ),
                //         child: Column(
                //           crossAxisAlignment: CrossAxisAlignment.start,
                //           children: [
                //             Text('Dede Suherman',
                //                 style: secondHeadingBlack.copyWith(
                //                     fontSize: S.w / 28)),
                //             Text('Asyik banget kayaknya ikut yang beginian',
                //                 style: descBlack.copyWith(
                //                     color: Colors.black87,
                //                     fontSize: S.w / 32)),
                //           ],
                //         ),
                //       ),
                //       Row(
                //         mainAxisAlignment: MainAxisAlignment.start,
                //         children: [
                //           Container(
                //             margin: EdgeInsets.only(right: 10, left: 10),
                //             child: Text('Hari ini 11.41',
                //                 style: descBlack.copyWith(
                //                     fontSize: S.w / 34,
                //                     color: Colors.black38)),
                //           ),
                //           InkWell(
                //             onTap: () {},
                //             child: Text('Balas',
                //                 style: secondHeadingBlack.copyWith(
                //                     fontSize: S.w / 34)),
                //           )
                //         ],
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        );
      }).toList(),
    );
  }

  void more(BuildContext context, int id, {String judul, String deskripsi}) {
    // var S.w = MediaQuery.of(context).size.width;
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        context: context,
        builder: (context2) => Container(
              // height: S.w / 4,
              padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(12.0), topRight: const Radius.circular(12.0))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                //mainAxisAlignment:
                //  MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 5,
                    width: 25,
                    decoration: BoxDecoration(borderRadius: radius(200), color: Colors.grey[300]),
                  ),
                  SizedBox(height: 10),
                  ListTile(
                    leading: Icon(Icons.edit),
                    title: Text(
                      'Ubah Forum',
                      style: secondHeadingBlack.copyWith(fontSize: S.w / 28),
                    ),
                    onTap: () async {
                      titleController.text = judul;
                      descController.text = deskripsi;
                      await update(
                        context,
                        id,
                      );
                    },
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.delete),
                    title:
                        Text('Hapus Forum', style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                    onTap: () async {
                      await delete(
                        context,
                        id,
                      );
                    },
                  ),
                ],
              ),
            ));
  }

  void update(
    BuildContext context,
    int id,
  ) {
    Navigator.pop(context);

    showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (_) =>
            StatefulBuilder(builder: (BuildContext _, void Function(void Function()) setState) {
              // String role = someCapitalizedString(data.role);
              return Center(
                child: SingleChildScrollView(
                  child: Dialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                      child: Container(
                          width: S.w,
                          padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .05),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText('Forum', Kind.heading1),
                              SizedBox(
                                height: 10,
                              ),
                              CustomText(
                                  'Ubah forum dengan mengganti data - data berikut', Kind.descBlack,
                                  align: TextAlign.center),
                              SizedBox(
                                height: 26,
                              ),
                              TextField(
                                controller: titleController,
                                style: normalText(context),
                                decoration: fullRadiusField(context, hint: 'Judul Forum'),
                              ),
                              SizedBox(height: 16),
                              TextField(
                                controller: descController,
                                style: normalText(context),
                                maxLines: 4,
                                minLines: 3,
                                decoration: InputDecoration(
                                  hintText: 'Deskripsi Forum',
                                  hintStyle: descBlack.copyWith(
                                      fontSize: S.w / 30, color: Color(0xffB9B9B9)),
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Color(0xffD8D8D8)),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: SiswamediaTheme.green),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                ),
                              ),
                              SizedBox(height: 16),
                              InkWell(
                                  onTap: () async {
                                    // print(className.text);
                                    Navigator.pop(context);
                                    SiswamediaTheme.infoLoading(
                                        context: context, info: 'Mengedit forum');
                                    await ForumServices.updateForum(
                                            judulForum: titleController.text,
                                            deskripsi: descController.text,
                                            id: id)
                                        .then((value) async {
                                      if (value.statusCode == StatusCodeConst.success) {
                                        Navigator.pop(context);
                                        await forumState.setState((s) => s.fetchItems(id, isClass));
                                        CustomFlushBar.successFlushBar(
                                          value.message,
                                          context,
                                        );
                                      } else {
                                        Navigator.pop(context);
                                        CustomFlushBar.errorFlushBar(
                                          value.message,
                                          context,
                                        );
                                      }
                                    });
                                  },
                                  child: Container(
                                      width: S.w,
                                      height: 45,
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius: BorderRadius.circular(23)),
                                      child: Center(
                                          child: Text('Simpan',
                                              style: TextStyle(
                                                  fontSize: S.w / 30,
                                                  fontWeight: FontWeight.w600,
                                                  color: SiswamediaTheme.white))))),
                              SizedBox(height: 10),
                            ],
                          ))),
                ),
              );
            }));
  }

  Future<void> delete(BuildContext context, int id) {
    Navigator.pop(context);
    return showDialog<void>(
        context: context,
        builder: (context2) => Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
              padding: EdgeInsets.all(S.w * .05),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text('Hapus Forum ?', style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                SizedBox(height: 15),
                Text('Apakah Anda yakin untuk menghapus forum ini ?',
                    style: descBlack.copyWith(fontSize: S.w / 34)),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .01),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                    Container(
                      height: S.w / 10,
                      width: S.w / 3.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: SiswamediaTheme.green,
                      ),
                      child: InkWell(
                        onTap: () async {
                          // ignore: unawaited_futures
                          showDialog<void>(
                              context: context,
                              builder: (context2) => Center(child: CircularProgressIndicator()));
                          await ForumServices.deleteForum(id: id).then((value) async {
                            print(value.statusCode);
                            Navigator.pop(context);
                            if (value.statusCode == 200) {
                              print(value.statusCode);
                              Navigator.pop(context);
                              print('masuk');
                              await forumState.state
                                  .fetchItems(auth.currentState.schoolId, isClass);
                              await forumState.notify();
                              return CustomFlushBar.successFlushBar(
                                value.message,
                                context,
                              );
                            } else {
                              Navigator.pop(context);
                              return CustomFlushBar.errorFlushBar(
                                value.message,
                                context,
                              );
                            }
                          });

                          //Navigator.push(context,
                          //  MaterialPageRoute<void>(builder: (_) => null));
                        },
                        child: Center(
                          child: Text('Ya', style: TextStyle(color: Colors.white)),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: SiswamediaTheme.lightBlack,
                      ),
                      height: S.w / 10,
                      width: S.w / 3.5,
                      child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Center(
                          child: Text('Tidak', style: TextStyle(color: Colors.white)),
                        ),
                      ),
                    ),
                  ]),
                ),
                SizedBox(height: 10),
              ]),
            )));
  }
}
