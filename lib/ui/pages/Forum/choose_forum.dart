part of '_forum.dart';

class ChooseForum extends StatefulWidget {
  @override
  _ChooseForumState createState() => _ChooseForumState();
}

class _ChooseForumState extends State<ChooseForum> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SiswamediaAppBar(
        context: context,
        title: 'Pilih forum',
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: S.w * .2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/png/social.png',
              width: S.w * .6,
              height: S.w * .5,
            ),
            CustomText('Pilih Forum', Kind.heading3),
            CustomText('Pilihlah salah satu forum dibawah ini', Kind.descBlack),
            SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                color: SiswamediaTheme.green,
                borderRadius: radius(200),
              ),
              child: InkWell(
                  onTap: () => navigate(context, MainForum()),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .025),
                    child: Center(
                        child: Text(
                      'Sekolah',
                      style: semiWhite.copyWith(fontSize: S.w / 26),
                    )),
                  )),
            ),
            SizedBox(height: 10),
            Container(
              decoration: BoxDecoration(
                color: SiswamediaTheme.nearBlack,
                borderRadius: radius(200),
              ),
              child: InkWell(
                  onTap: () => navigate(context, MainForum(isClass: true)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .025),
                    child: Center(
                        child: Text(
                      'Kelas',
                      style: semiWhite.copyWith(fontSize: S.w / 26),
                    )),
                  )),
            ),
            HeightSpace(20),
          ],
        ),
      ),
    );
  }
}
