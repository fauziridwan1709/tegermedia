part of '_pendaftaran.dart';

class FormRegistrationNext extends StatefulWidget {
  @override
  _FormRegistrationNextState createState() => _FormRegistrationNextState();
}

class _FormRegistrationNextState extends State<FormRegistrationNext> {
  TextEditingController alamatController = TextEditingController();
  TextEditingController kodePosController = TextEditingController();
  TextEditingController nomorHandphoneController = TextEditingController();
  //TextEditingController genderController = TextEditingController();
  TextEditingController namaAyahController = TextEditingController();
  TextEditingController namaIbuController = TextEditingController();
  TextEditingController keteranganController = TextEditingController();

  List<String> gender = ['Pria', 'Wanita'];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Formulir Pendaftaran',
            style: secondHeadingBlack.copyWith(fontSize: width / 22),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: SiswamediaTheme.green,
            onPressed: () => Navigator.pop(context),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: width * .05, vertical: width * .05),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              'Alamat Sekarang',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: alamatController,
              decoration: InputDecoration(
                  hintText: '    Isi sesuai tempat tinggal sekarang '),
            ),
            SizedBox(
              height: width * .07,
            ),
            /*Text(
              'Jenis Kelamin',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: namaController,
              decoration: InputDecoration(
                  hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
                Text(
                  'Jenis Kelamin',
                  style: descBlack.copyWith(fontSize: width / 28),
                ),
                TextField(
                  controller: namaController,
                  decoration: InputDecoration(
                      hintText: '    Isi sesuai dengan akte kelahiran'),
                ),
                Text(
                  'Jenis Kelamin',
                  style: descBlack.copyWith(fontSize: width / 28),
                ),
                TextField(
                  controller: namaController,
                  decoration: InputDecoration(
                      hintText: '    Isi sesuai dengan akte kelahiran'),
                ),
                Text(
                  'Jenis Kelamin',
                  style: descBlack.copyWith(fontSize: width / 28),
                ),
                TextField(
                  controller: namaController,
                  decoration: InputDecoration(
                      hintText: '    Isi sesuai dengan akte kelahiran'),
                ),*/
            Text(
              'Kode Pos',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: kodePosController,
              decoration: InputDecoration(hintText: ''),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'No HP Ayah/Ibu',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: nomorHandphoneController,
              decoration: InputDecoration(hintText: ''),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Nama Ayah',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: namaAyahController,
              decoration:
                  InputDecoration(hintText: '    Isi lengkap dengan gelar'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Nama Ibu',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: namaIbuController,
              decoration:
                  InputDecoration(hintText: '    Isi lengkap dengan gelar'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Keterangan',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: keteranganController,
              decoration:
                  InputDecoration(hintText: '    Contoh: nama pengantar'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: width * .2, vertical: width * .05),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width),
                color: SiswamediaTheme.green,
              ),
              child: InkWell(
                  onTap: () {
                    Navigator.popUntil(context, ModalRoute.withName('/Page1'));
                    //Navigator.push(
                    //context, MaterialPageRoute<void>(builder: (_) => null));
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                    child: Center(
                      child: Text('Simpan',
                          style: secondHeadingWhite.copyWith(
                              fontSize: width / 30)),
                    ),
                  )),
            ),
          ]),
        )));
  }
}
