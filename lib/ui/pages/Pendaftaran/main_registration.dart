part of '_pendaftaran.dart';

class MainRegistration extends StatefulWidget {
  @override
  _MainRegistrationState createState() => _MainRegistrationState();
}

class _MainRegistrationState extends State<MainRegistration> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: TitleAppbar(label: 'Pendaftaran'),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
        ),
        body: ListView(
          children: [
            Container(
                height: width / 1.8,
                margin: EdgeInsets.symmetric(
                    horizontal: width * .05, vertical: width * .05),
                padding: EdgeInsets.symmetric(
                    horizontal: width * .05, vertical: width * .05),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 0),
                          spreadRadius: 2,
                          blurRadius: 2,
                          color: Colors.grey.withOpacity(.2))
                    ]),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Nama Lengkap',
                          style: secondHeadingBlack.copyWith(
                              fontSize: width / 25)),
                      Padding(
                        padding: EdgeInsets.only(left: width * .05),
                        child: Text('Muhamad Ridwan',
                            style: descBlack.copyWith(
                                fontSize: width / 26, color: Colors.black87)),
                      ),
                      Text('Nomor Induk Siswa Nasional (NISN)',
                          style: secondHeadingBlack.copyWith(
                              fontSize: width / 25)),
                      Padding(
                        padding: EdgeInsets.only(left: width * .05),
                        child: Text('###############',
                            style: descBlack.copyWith(
                                fontSize: width / 26, color: Colors.black87)),
                      ),
                      SizedBox(height: 1),
                      Center(
                        child: Container(
                            width: width / 1.7,
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(width),
                            ),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (_) =>
                                                FormRegistration()));
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 12),
                                    child: Center(
                                        child: Text(
                                            'Lengkapi Formulir Pendaftaran',
                                            style: descWhite.copyWith(
                                                fontSize: width / 30))),
                                  )),
                            )),
                      ),
                    ])),
            Column(
              children: [
                Container(
                    margin: EdgeInsets.only(
                        left: width * .05,
                        right: width * .05,
                        bottom: width * .03),
                    height: 120,
                    decoration: BoxDecoration(
                      color: Color(0xff3E7428),
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                  builder: (_) => SearchSchool())),
                          child: Stack(children: [
                            Positioned(
                                left: 15,
                                child: Container(
                                    height: 120,
                                    child: Center(
                                      child: Text(
                                          'Cari \nSekolah Impianmu \nDi Sini',
                                          style: secondHeadingWhite.copyWith(
                                              fontSize: width / 24)),
                                    ))),
                            Positioned(
                              right: -5,
                              bottom: -5,
                              child: Image.asset(
                                'assets/search.png',
                                height: width / 3.5,
                                width: width / 3.5,
                              ),
                            )
                          ]),
                        ),
                      ),
                    )),
                Titles(
                  title: 'Sekolah yang Saya Daftarkan',
                  subtitle: 'Lihat semua',
                  callback: () => Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                          builder: (_) => ListRegistered())),
                  size: width / 24,
                ),
                FutureBuilder<SchoolModel>(
                    future: ClassServices.getMeSchool(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Column(
                            children: snapshot.data.data
                                .sublist(0, 1)
                                .map((e) => Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: width * .05,
                                        vertical: width * .05),
                                    height: 150,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(16),
                                      boxShadow: [
                                        BoxShadow(
                                            offset: Offset(0, 0),
                                            spreadRadius: 2,
                                            blurRadius: 2,
                                            color: Colors.grey.withOpacity(.2))
                                      ],
                                    ),
                                    child: InkWell(
                                      onTap: () {},
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: width * .05,
                                            vertical: width * .05),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: width * .6,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(e.name,
                                                      style:
                                                          headingBlack.copyWith(
                                                              fontSize:
                                                                  width / 24)),
                                                  Text('Status Pendaftaran:',
                                                      style: descBlack.copyWith(
                                                          fontSize:
                                                              width / 30)),
                                                  Text(
                                                      '${e.isApprove ? e.isVerified ? 'Berhasil' : 'Berhasil, Menunggu Pembayaran' : 'Menunggu'}',
                                                      style: secondHeadingBlack
                                                          .copyWith(
                                                              fontSize:
                                                                  width / 30)),
                                                  Text('Status Seleksi:',
                                                      style: descBlack.copyWith(
                                                          fontSize:
                                                              width / 30)),
                                                  Text(
                                                      '${e.isVerified ? 'Berhasil' : 'Menunggu Hasil Seleksi'}',
                                                      style: secondHeadingBlack
                                                          .copyWith(
                                                              fontSize:
                                                                  width / 30)),
                                                ],
                                              ),
                                            ),
                                            if (!e.isVerified)
                                              Container(
                                                decoration: BoxDecoration(
                                                  color: SiswamediaTheme.green,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width),
                                                ),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 8,
                                                            horizontal: 20),
                                                    child: Text('Bayar',
                                                        style: secondHeadingWhite
                                                            .copyWith(
                                                                fontSize:
                                                                    width /
                                                                        30)),
                                                  ),
                                                ),
                                              ),
                                          ],
                                        ),
                                      ),
                                    )))
                                .toList());
                      } else if (ConnectionState.waiting ==
                          snapshot.connectionState) {
                        return Center(
                            child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    SiswamediaTheme.green)));
                      }
                      return Container();
                    }),
              ],
            ),
          ],
        ));
  }
}
