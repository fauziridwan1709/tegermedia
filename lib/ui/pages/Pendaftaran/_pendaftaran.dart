import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'form_registration.dart';
part 'form_registration_next.dart';
part 'list_registered.dart';
part 'main_registration.dart';
part 'search_school.dart';
