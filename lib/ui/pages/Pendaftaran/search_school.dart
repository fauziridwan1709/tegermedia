part of '_pendaftaran.dart';

class SearchSchool extends StatefulWidget {
  @override
  _SearchSchoolState createState() => _SearchSchoolState();
}

class _SearchSchoolState extends State<SearchSchool> {
  TextEditingController searchController = TextEditingController();
  String _choose1;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(width * .18),
        child: Center(
          child: AppBar(
            backgroundColor: Colors.white,
            shadowColor: Color(0xfffdfdfd),
            centerTitle: true,
            title: Column(children: [
              Text(
                'Daftar Sekolah',
                style: secondHeadingBlack.copyWith(fontSize: width / 26),
              ),
              Text(
                'Jakarta timur',
                style: descBlack.copyWith(fontSize: width / 32),
              ),
            ]),
            leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: SiswamediaTheme.green,
                ),
                onPressed: () => Navigator.pop(context)),
            actions: [
              IconButton(
                icon: Icon(Icons.format_list_bulleted),
                color: SiswamediaTheme.green,
                onPressed: () => showDialog<void>(
                    context: context,
                    builder: (context) => Dialog(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(width * .025),
                          ),
                          child: Container(
                            height: width / 1.6,
                            width: width / 2,
                            padding: EdgeInsets.symmetric(
                                horizontal: width * .05, vertical: width * .05),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(width * .1),
                            ),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                        'Pilih daerah sekolah yang\nAnda inginkan',
                                        textAlign: TextAlign.center,
                                        style: descBlack.copyWith(
                                            fontSize: width / 28)),
                                    SizedBox(height: 1),
                                    Container(
                                      width: width * .6,
                                      height: width * .1,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width * .05),
                                      decoration:
                                          SiswamediaTheme.dropDownDecoration,
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                            icon: Icon(
                                              Icons.arrow_drop_down,
                                              color: SiswamediaTheme.green,
                                            ),
                                            hint: Text('Provinsi'),
                                            value: _choose1,
                                            onChanged: (String o) {
                                              setState(() {
                                                _choose1 = o;
                                              });
                                            },
                                            items: [
                                              'Jakarta Barat',
                                              'Jakarta Pusat',
                                              'Jakarta Timur'
                                            ]
                                                .map((e) => DropdownMenuItem(
                                                      value: e,
                                                      child: Text(e),
                                                    ))
                                                .toList()),
                                      ),
                                    ),
                                    SizedBox(height: 1),
                                    Column(children: [
                                      ButtonTerapkan(
                                        title: 'Terapkan',
                                        callback: () => Navigator.pop(context),
                                      ),
                                      FlatButton(
                                        onPressed: () => Navigator.pop(context),
                                        child: Text('Batal'),
                                      ),
                                    ])
                                  ]),
                            ),
                          ),
                        )),
              )
            ],
          ),
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: width * .05,
              vertical: width * .03,
            ),
            child: CustomSearchBar(
              controller: searchController,
            ),
          ),
          FutureBuilder<dynamic>(
            future: SchoolServices.getSchools(2, 2, 'SMA'),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Container();
              }
              return Container();
            },
          )
        ],
      ),
    );
  }
}
