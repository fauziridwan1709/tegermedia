part of '_pendaftaran.dart';

class ListRegistered extends StatefulWidget {
  @override
  _ListRegisteredState createState() => _ListRegisteredState();
}

class _ListRegisteredState extends State<ListRegistered> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: TitleAppbar(label: 'Sekolah yang Saya Daftarkan'),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () => Navigator.pop(context)),
      ),
      body: FutureBuilder<SchoolModel>(
          future: ClassServices.getMeSchool(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                  children: snapshot.data.data
                      .sublist(0, 1)
                      .map((e) => Container(
                          margin: EdgeInsets.symmetric(vertical: width * .05),
                          height: 150,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(0, 0),
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  color: Colors.grey.withOpacity(.2))
                            ],
                          ),
                          child: InkWell(
                            onTap: () {},
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * .05,
                                  vertical: width * .05),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: width * .7,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(e.name,
                                            style: headingBlack.copyWith(
                                                fontSize: width / 24)),
                                        Text('Status Pendaftaran:',
                                            style: descBlack.copyWith(
                                                fontSize: width / 30)),
                                        Text(
                                            '${e.isApprove ? e.isVerified ? 'Berhasil' : 'Berhasil, Menunggu Pembayaran' : 'Menunggu'}',
                                            style: secondHeadingBlack.copyWith(
                                                fontSize: width / 30)),
                                        Text('Status Seleksi:',
                                            style: descBlack.copyWith(
                                                fontSize: width / 30)),
                                        Text(
                                            '${e.isVerified ? 'Berhasil' : 'Menunggu Hasil Seleksi'}',
                                            style: secondHeadingBlack.copyWith(
                                                fontSize: width / 30)),
                                      ],
                                    ),
                                  ),
                                  if (!e.isVerified)
                                    Container(
                                      decoration: BoxDecoration(
                                        color: SiswamediaTheme.green,
                                        borderRadius:
                                            BorderRadius.circular(width),
                                      ),
                                      child: InkWell(
                                        onTap: () {},
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 8, horizontal: 20),
                                          child: Text('Bayar',
                                              style:
                                                  secondHeadingWhite.copyWith(
                                                      fontSize: width / 30)),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          )))
                      .toList());
            } else if (ConnectionState.waiting == snapshot.connectionState) {
              return Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation(SiswamediaTheme.green)));
            }
            return Container();
          }),
    );
  }
}
