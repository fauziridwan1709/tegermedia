part of '_pendaftaran.dart';

class FormRegistration extends StatefulWidget {
  @override
  _FormRegistrationState createState() => _FormRegistrationState();
}

class _FormRegistrationState extends State<FormRegistration> {
  TextEditingController namaController = TextEditingController();
  TextEditingController tempatLahirController = TextEditingController();
  TextEditingController tanggalLahirController = TextEditingController();
  //TextEditingController genderController = TextEditingController();
  TextEditingController agamaController = TextEditingController();
  TextEditingController nisnController = TextEditingController();
  TextEditingController sekolahAsalController = TextEditingController();

  List<String> gender = ['Pria', 'Wanita'];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Formulir Pendaftaran',
            style: secondHeadingBlack.copyWith(fontSize: width / 22),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: SiswamediaTheme.green,
            onPressed: () => Navigator.pop(context),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: width * .05, vertical: width * .05),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              'Nama Lengkap',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: namaController,
              decoration: InputDecoration(
                  hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Tempat Lahir',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: tempatLahirController,
              decoration: InputDecoration(
                  hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Tanggal Lahir',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: tanggalLahirController,
              decoration: InputDecoration(
                  hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Jenis Kelamin',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: namaController,
              decoration: InputDecoration(
                  hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Agama',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: agamaController,
              decoration: InputDecoration(
                  hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Nomor Induk Siswa Nasional (NISN)',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: nisnController,
              //decoration:
              //InputDecoration(hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Text(
              'Nama Sekolah Asal',
              style: descBlack.copyWith(fontSize: width / 28),
            ),
            TextField(
              controller: sekolahAsalController,
              //decoration:
              //InputDecoration(hintText: '    Isi sesuai dengan akte kelahiran'),
            ),
            SizedBox(
              height: width * .07,
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: width * .2, vertical: width * .05),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width),
                color: SiswamediaTheme.green,
              ),
              child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                            builder: (_) => FormRegistrationNext()));
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                    child: Center(
                      child: Text('Selanjutnya',
                          style: secondHeadingWhite.copyWith(
                              fontSize: width / 30)),
                    ),
                  )),
            ),
          ]),
        )));
  }
}
