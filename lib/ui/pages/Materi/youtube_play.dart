part of '_materi.dart';

class JIJI extends StatefulWidget {
  JIJI({this.url});
  final String url;
  @override
  _JIJIState createState() => _JIJIState();
}

class _JIJIState extends State<JIJI> {
  List<Video> listing = [];

  @override
  void initState() {
    super.initState();
    _initList();
    print(listing);
  }

  Future<void> _initList() async {
    var channel = await APIService.instance
        .fetchVideosFromPlaylist(playlistId: widget.url.substring(49, 83));
    setState(() {
      listing = channel;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light));
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          VideoScreen(url: widget.url),
          SizedBox(
            height: 10,
          ),
          Column(
            children: listing
                .map((e) => Container(
                      child: Column(
                        children: [
                          Material(
                            color: Colors.transparent,
                            child: InkWell(
                                onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                        builder: (_) => JIJI(
                                            url:
                                                'https://www.youtube.com/watch?v=${e.id}'))),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: S.w * .05,
                                      vertical: S.w * .03),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                          height: 70,
                                          width: 120,
                                          child: Image.network(
                                            e.thumbnailUrl,
                                            fit: BoxFit.cover,
                                          )),
                                      SizedBox(
                                        width: 12,
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              e.title,
                                              style: TextStyle(fontSize: 12),
                                            ),
                                            Text('Kurikulum 2013',
                                                style: TextStyle(fontSize: 12))
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: S.w * .05),
                            child: Divider(
                              color: Colors.grey.withOpacity(.6),
                            ),
                          ),
                        ],
                      ),
                    ))
                .toList(),
          ),
        ],
      ),
    ));
  }
}
