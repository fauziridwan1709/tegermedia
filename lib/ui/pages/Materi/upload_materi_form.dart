part of '_materi.dart';

class UploadMateriForm extends StatefulWidget {
  UploadMateriForm({this.type});
  final String type;

  @override
  _UploadMateriFormState createState() => _UploadMateriFormState();
}

class _UploadMateriFormState extends State<UploadMateriForm> {
  TextEditingController title = TextEditingController();
  TextEditingController url = TextEditingController();
  MateriDocItemModel item = MateriDocItemModel();
  String chooseClass;
  String chooseCurriculum;
  String chooseCourse;
  String chooseDocumentType;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Unggah Materi Baru',
          style: semiBlack.copyWith(fontSize: width / 22),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.green,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      backgroundColor: Color(0xfff9f9f9),
      body: Stack(
        children: [
          ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 100.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: width - 16 * 2,
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 10,
                                color: Colors.grey[300],
                                spreadRadius: 1,
                                offset: Offset(.5, .5))
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 18,
                          ),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              height: 40,
                              child: TextField(
                                controller: title,
                                decoration: InputDecoration(
                                    hintText: 'Judul Materi',
                                    hintStyle: TextStyle(fontSize: width / 30),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 2.0, horizontal: 10),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                            color: Colors.grey[300])),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                            color: Colors.grey[300]))),
                              )),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              height: 40,
                              child: DropdownButtonFormField<String>(
                                  value: chooseClass,
                                  icon: Icon(Icons.keyboard_arrow_down,
                                      size: 30, color: Colors.grey[300]),
                                  iconSize: 24,
                                  elevation: 16,
                                  hint: Text('Pilih Kelas/Materi Umum'),
                                  decoration: InputDecoration(
                                    hintStyle: TextStyle(fontSize: width / 30),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 2.0, horizontal: 10),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                            color: Colors.grey[300])),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                            color: Colors.grey[300])),
                                  ),
                                  style: TextStyle(color: Colors.black),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      chooseClass = newValue;
                                    });
                                  },
                                  items: [
                                    '1',
                                    '2',
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8',
                                    '9',
                                    '10',
                                    '11 IPA',
                                    '11 IPS',
                                    '12 IPA',
                                    '12 IPS'
                                  ]
                                      .map(
                                        (e) => DropdownMenuItem(
                                          value: e,
                                          child: Text('kelas $e'),
                                        ),
                                      )
                                      .toList())),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              height: 40,
                              child: DropdownButtonFormField<String>(
                                value: chooseCurriculum,
                                icon: Icon(Icons.keyboard_arrow_down,
                                    size: 30, color: Colors.grey[300]),
                                iconSize: 24,
                                elevation: 16,
                                hint: Text('Pilih Kurikulum'),
                                decoration: InputDecoration(
                                  hintStyle: TextStyle(fontSize: width / 30),
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 2.0, horizontal: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide:
                                          BorderSide(color: Colors.grey[300])),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide:
                                          BorderSide(color: Colors.grey[300])),
                                ),
                                style: TextStyle(color: Colors.black),
                                onChanged: (String newValue) {
                                  setState(() {
                                    chooseCurriculum = newValue;
                                  });
                                },
                                items: ['2013', '2006']
                                    .map(
                                      (e) => DropdownMenuItem(
                                        value: e.toString(),
                                        child: Text('$e'),
                                      ),
                                    )
                                    .toList(),
                              )),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              height: 40,
                              child: DropdownButtonFormField<String>(
                                value: chooseCourse,
                                icon: Icon(Icons.keyboard_arrow_down,
                                    size: 30, color: Colors.grey[300]),
                                iconSize: 24,
                                elevation: 16,
                                hint: Text('Pilih Mata Pelajaran'),
                                decoration: InputDecoration(
                                  hintStyle: TextStyle(fontSize: width / 30),
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 2.0, horizontal: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide:
                                          BorderSide(color: Colors.grey[300])),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide:
                                          BorderSide(color: Colors.grey[300])),
                                ),
                                style: TextStyle(color: Colors.black),
                                onChanged: (String newValue) {
                                  setState(() {
                                    chooseCourse = newValue;
                                  });
                                },
                                items: ['Matematika', 'Fisika', 'Kimia']
                                    .map(
                                      (e) => DropdownMenuItem(
                                        value: e.toString(),
                                        child: Text('$e'),
                                      ),
                                    )
                                    .toList(),
                              )),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              height: 40,
                              child: DropdownButtonFormField<String>(
                                value: chooseDocumentType,
                                icon: Icon(Icons.keyboard_arrow_down,
                                    size: 30, color: Colors.grey[300]),
                                iconSize: 24,
                                elevation: 16,
                                hint: Text('Pilih Tipe Dokumen'),
                                decoration: InputDecoration(
                                  hintStyle: TextStyle(fontSize: width / 30),
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 2.0, horizontal: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide:
                                          BorderSide(color: Colors.grey[300])),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide:
                                          BorderSide(color: Colors.grey[300])),
                                ),
                                style: TextStyle(color: Colors.black),
                                onChanged: (String newValue) {
                                  setState(() {
                                    chooseDocumentType = newValue;
                                  });
                                },
                                items: ['Pdf', 'Ppt', 'xlsx']
                                    .map(
                                      (e) => DropdownMenuItem(
                                        value: e.toString(),
                                        child: Text('$e'),
                                      ),
                                    )
                                    .toList(),
                              )),
                        ],
                      ),
                    ),
                    Container(
                      // height: 40,
                      width: width - 16 * 2,
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 10,
                                color: Colors.grey[300],
                                spreadRadius: 1,
                                offset: Offset(.5, .5))
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Pilih file untuk di unggah',
                              style: descBlack.copyWith(
                                  fontSize: width / 26,
                                  fontWeight: FontWeight.w600)),
                          SizedBox(
                            height: 18,
                          ),
                          Container(
                            height: 60,
                            decoration: BoxDecoration(
                              border: Border.all(color: Color(0xffd9d9d9)),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Center(
                              child: Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: SiswamediaTheme.green),
                                    shape: BoxShape.circle),
                                child: Center(
                                    child: Icon(Icons.add,
                                        color: SiswamediaTheme.green)),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 18,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Divider(
                                  color: Colors.grey,
                                  height: 1,
                                ),
                              ),
                              SizedBox(width: 20),
                              Text('Atau'),
                              SizedBox(width: 20),
                              Expanded(
                                child: Divider(
                                  color: Colors.grey,
                                  height: 1,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 10),
                          Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              height: 40,
                              child: TextField(
                                controller: url,
                                decoration: InputDecoration(
                                    hintText:
                                        'Masukkan link dokumen Google Drive',
                                    hintStyle: TextStyle(fontSize: width / 30),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 2.0, horizontal: 10),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                            color: Colors.grey[300])),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                            color: Colors.grey[300]))),
                              )),
                          SizedBox(height: 10),
                          Text('Pilih Bloom tag untuk materi Anda',
                              style: descBlack.copyWith(
                                  fontSize: width / 26,
                                  fontWeight: FontWeight.w600)),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: GridView.count(
                              physics: ScrollPhysics(),
                              crossAxisCount: 2,
                              shrinkWrap: true,
                              childAspectRatio: 3.5,
                              scrollDirection: Axis.vertical,
                              children: values.keys
                                  .map((e) => Container(
                                        height: 30,
                                        child: CheckboxListTile(
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          contentPadding: EdgeInsets.all(0),
                                          title: Text(
                                            e,
                                            style:
                                                TextStyle(fontSize: width / 32),
                                          ),
                                          value: values[e],
                                          onChanged: (bool value) {
                                            setState(() {
                                              values[e] = value;
                                            });
                                          },
                                        ),
                                      ))
                                  .toList(),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            left: 0,
            bottom: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: width * .02),
              color: Colors.white,
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  height: width * .15,
                  child: SizedBox(
                    height: 20,
                    child: RaisedButton(
                      elevation: 0,
                      // padding: EdgeInsets.symmetric(vertical: 10),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(width)),

                      color: SiswamediaTheme.green,
                      onPressed: () async {},
                      child: Text(
                        'Unggah',
                        style: descWhite.copyWith(
                            fontSize: 16, color: Colors.white),
                      ),
                    ),
                  )),
            ),
          ),
        ],
      ),
    );
  }

  Map<String, bool> values = {
    'Pengetahuan': true,
    'Penerapan': false,
    'Penilaian': false,
    'Pemahaman': false,
    'Analisis': false,
    'Kreasi': false,
  };
}
