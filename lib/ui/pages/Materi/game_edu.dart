part of '_materi.dart';

class GameEdu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: S.w * .3,
      child: ListView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.only(right: S.w * .04, left: S.w * .04),
          children: gameData.keys
              .map((e) => Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    margin:
                        EdgeInsets.only(right: S.w * .025, left: S.w * .025),
                    child: Container(
                      width: S.w * .4,
                      height: S.w * .3,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Stack(children: [
                        InkWell(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                  builder: (_) => GameWebView(
                                        title: e,
                                        url: gameData[e].first,
                                      ))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: S.w * .3,
                                width: S.w * .4,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(12),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          gameData[e].last.toString(),),
                                      fit: BoxFit.cover),
                                ),
                                child: Center(
                                    child: Container(
                                        width: S.w / 4.5,
                                        height: S.w / 9,
                                        decoration: BoxDecoration(
                                            color: Colors.white.withOpacity(.8),
                                            borderRadius: radius(8),
                                            border: Border.all(
                                                width: 2,
                                                color: SiswamediaTheme.green
                                                    .withOpacity(.3))),
                                      child: Row(
                                        children: [
                                          Icon(Icons.play_arrow,
                                              color: SiswamediaTheme.green),
                                          Text('   Main',
                                              style: descBlack.copyWith(
                                                  fontSize: S.w / 30))
                                        ],
                                      ),
                                    ),),
                              ),
                              // Container(
                              //   padding: EdgeInsets.symmetric(
                              //       horizontal: S.w * .03),
                              //   height: S.w * .12,
                              //   child: Column(
                              //     crossAxisAlignment:
                              //         CrossAxisAlignment.start,
                              //     mainAxisAlignment: MainAxisAlignment.center,
                              //     children: [
                              //       Flexible(
                              //         child: RichText(
                              //           maxLines: 1,
                              //           overflow: TextOverflow.ellipsis,
                              //           strutStyle:
                              //               StrutStyle(fontSize: S.w / 30),
                              //           text: TextSpan(
                              //             text: e,
                              //             style: TextStyle(
                              //               fontWeight: FontWeight.w700,
                              //               fontSize: S.w / 30,
                              //               letterSpacing: 0.18,
                              //               color: Colors.black,
                              //             ),
                              //           ),
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                        Positioned(
                          bottom: 15,
                          left: 0,
                          child: Container(
                            height: S.w / 18,
                            // width: S.w / 9,
                            padding: EdgeInsets.symmetric(horizontal: 4),
                            child: Center(
                                child: Text(e,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: S.w / 36,
                                        fontWeight: FontWeight.w700))),
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(4),
                                  bottomRight: Radius.circular(4)),
                            ),
                          ),
                        )
                      ]),
                    ),
                  ))
              .toList()),
    );
  }
}

const gameData = <String, List<String>>{
  'Menata Bola': [
    'https://belajar.kemdikbud.go.id/edugame/game/Menata%20Bola/index.html',
    'https://belajar.kemdikbud.go.id/EduGame/images/menatabola.png'
  ],
  'Mengumpulkan Telur': [
    'https://belajar.kemdikbud.go.id/edugame/game/Mengumpulkan Telur/index.html',
    'https://belajar.kemdikbud.go.id/EduGame/images/mengumpulkantelurayam.png'
  ],
  'Petualangan Antariksa': [
    'https://belajar.kemdikbud.go.id/edugame/game/Petualangan%20Antariksa%20Vol%201/index.html',
    'https://belajar.kemdikbud.go.id/EduGame/images/petualanganantariksa1.png'
  ],
  'Memanen Apel': [
    'https://belajar.kemdikbud.go.id/edugame/game/Memanen%20Apel/index.html',
    'https://belajar.kemdikbud.go.id/EduGame/images/memanenapel.png'
  ],
  'Memperbaiki Pagar': [
    'https://belajar.kemdikbud.go.id/edugame/game/Memperbaiki%20Pagar/index.html',
    'https://belajar.kemdikbud.go.id/EduGame/images/memperbaikipagar.png'
  ],
  'Menangkap Balon': [
    'https://belajar.kemdikbud.go.id/edugame/game/Menangkap%20Balon/index.html',
    'https://belajar.kemdikbud.go.id/EduGame/images/menangkapbalon.png'
  ]
};
