part of '_materi.dart';

class MateriUmumList extends StatefulWidget {
  MateriUmumList({this.topic, this.subTopic});
  final String subTopic;
  final String topic;
  @override
  _MateriUmumListState createState() => _MateriUmumListState();
}

class _MateriUmumListState extends State<MateriUmumList> {
  List<Widget> jamal;

  // _scrollListener() {
  //   var materi = Provider.of<MateriProvider>(context);
  //   if (_controller.offset > 120) {
  //     if (topBarOpacity != 1.0) {
  //       setState(() {
  //         topBarOpacity = 1.0;
  //       });
  //     }
  //   } else {
  //     if (topBarOpacity != 0.0) {
  //       setState(() {
  //         topBarOpacity = 0.0;
  //       });
  //     }
  //   }
  //   //materi.setOffset(_controller.offset);
  //   print(_controller.offset);
  // }

  @override
  void initState() {
    super.initState();
    jamal = [
      ListEbookUmum(
        topic: widget.topic,
        subTopic: widget.subTopic,
      ),
      ListPresentasiUmum(
        topic: widget.topic,
        subTopic: widget.subTopic,
      ),
      ListVideoUmum(
        topic: widget.topic,
        subTopic: widget.subTopic,
      ),
      ListQuizUmum(
        topic: widget.topic,
        subTopic: widget.subTopic,
      ),
      ListAppUmum(
        topic: widget.topic,
        subTopic: widget.subTopic,
      )
    ];
    //_controller.addListener(_scrollListener);
  }

  int currentIndex = 0;
  List<int> kodeAPI = [5, 10, 14];

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
      body: CustomScrollView(
        //controller: _controller,
        slivers: <Widget>[
          SliverAppBar(
            title: Text('Materi Umum', style: semiWhite.copyWith(fontSize: S.w / 22)),
            expandedHeight: S.w * .68,
            pinned: true,
            backgroundColor: Color(0xFF7BA939),
            centerTitle: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/materi_topnav_background.png'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.only(top: 90.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          width: S.w * .2,
                          height: S.w * .2,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage('assets/icons/materi/umum/Kewirausahaan.png'),
                                  fit: BoxFit.cover))),
                      Container(
                        decoration: BoxDecoration(color: Color(0xFF7BA939)),
                        child: Padding(
                          padding: EdgeInsets.only(top: S.w / 120),
                          child: Text(widget.topic, //Colors.black87
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Colors.white, // was lightText
                              )),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(color: Color(0xFF7BA939)),
                        child: Text(widget.subTopic,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Colors.white, // was lightText
                            )),
                      ),
                    ],
                  ),
                ),
              ),
              // Image.asset(
              //   'assets/materi_topnav_background.png', // <===   Add your own image to assets or use a .network image instead.
              //   fit: BoxFit.cover,
              // ),
            ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(S.w * .15),
              child: Container(
                width: S.w,
                padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .02),
                decoration: BoxDecoration(
                  color: SiswamediaTheme.background,
                  borderRadius: isPortrait ? radius(0) : radius(6),
                ),
                child: Wrap(
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.start,
                  runSpacing: 0.0,
                  spacing: S.w / 72,
                  children: TipePelajaran.tipeBelajarSma
                      .map((e) => ActionChip(
                          label: Text(
                            e,
                            style: TextStyle(fontSize: S.w / 30),
                          ),
                          labelStyle: TextStyle(
                            color: currentIndex == TipePelajaran.tipeBelajarSma.indexOf(e)
                                ? Colors.white
                                : Colors.black,
                          ),
                          backgroundColor: currentIndex == TipePelajaran.tipeBelajarSma.indexOf(e)
                              ? SiswamediaTheme.green
                              : Colors.white,
                          onPressed: () {
                            _selectyaaa(TipePelajaran.tipeBelajarSma.indexOf(e));
                          }))
                      .toList(),
                ),
              ),
            ),
          ),
          jamal.elementAt(currentIndex),
        ],
      ),
    );
  }

  void _selectyaaa(int i) {
    setState(() {
      currentIndex = i;
    });
    print(currentIndex);
  }
}

class ListEbookUmum extends StatelessWidget {
  ListEbookUmum({this.topic, this.subTopic});
  final String topic;
  final String subTopic;

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    return SliverList(
      delegate: SliverChildListDelegate([
        Container(
            child: FutureBuilder(
          future: MateriServices.getDocData(subTopic,
              'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=10&columns=false'),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<MateriDocItemModel> snapshotFiltered = snapshot.data.rows
                  .where((dynamic element) =>
                      AdditionalMateriSettings.filterUmum(topic, subTopic, element))
                  .toList();
              if (snapshotFiltered.isEmpty) {
                return AdditionalMateriSettings.noData(context);
              } else {
                return Column(
                  children: snapshotFiltered
                      .map((e) => Padding(
                            padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                            child: DocumentUmumChildren(item: e),
                          ))
                      .toList(),
                );
              }
            } else {
              return Padding(
                padding: EdgeInsets.only(top: S.w / 3),
                child: Center(child: CircularProgressIndicator()),
              );
            }
          },
        ))
      ]),
    );
  }
}

class ListPresentasiUmum extends StatelessWidget {
  ListPresentasiUmum({this.topic, this.subTopic});
  final String topic;
  final String subTopic;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SliverList(
      delegate: SliverChildListDelegate([
        Container(
            child: FutureBuilder(
          future: MateriServices.getDocData(subTopic,
              'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=14&columns=false'),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<MateriDocItemModel> snapshotFiltered = snapshot.data.rows
                  .where((dynamic element) =>
                      AdditionalMateriSettings.filterUmum(topic, subTopic, element))
                  .toList();
              if (snapshotFiltered.isEmpty) {
                return AdditionalMateriSettings.noData(context);
              } else {
                return Column(
                  children: snapshotFiltered
                      .map((e) => Padding(
                            padding: EdgeInsets.symmetric(horizontal: width * .05),
                            child: DocumentUmumChildren(item: e),
                          ))
                      .toList(),
                );
              }
            } else {
              return Padding(
                padding: EdgeInsets.only(top: width / 3),
                child: Center(child: CircularProgressIndicator()),
              );
            }
          },
        ))
      ]),
    );
  }
}

class ListVideoUmum extends StatelessWidget {
  ListVideoUmum({this.topic, this.subTopic});
  final String topic;
  final String subTopic;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SliverList(
      delegate: SliverChildListDelegate([
        Container(
          child: FutureBuilder<MateriVideoModel>(
            future: MateriServices.getVideoData(subTopic,
                'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=5&columns=false'),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<MateriVideoItemModel> snapshotFiltered = snapshot.data.rows
                    .where(
                        (element) => AdditionalMateriSettings.filterUmum(topic, subTopic, element))
                    .toList();
                if (snapshotFiltered.isEmpty) {
                  ///showing widget noData
                  return AdditionalMateriSettings.noData(context);
                }
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * .03, vertical: width * .05),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: snapshotFiltered
                        .map((e) => VideoUmumChildren(item: e))
                        .toList()
                        .sublist(0, AdditionalMateriSettings.lengthUmum(snapshot, topic, subTopic)),
                  ),
                );
              } else {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Padding(
                    padding: EdgeInsets.only(top: width / 3),
                    child: Center(
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green))),
                  );
                } else {
                  ///showing widget noData
                  return AdditionalMateriSettings.noData(context);
                }
              }
            },
          ),
        )
      ]),
    );
  }
}

class ListQuizUmum extends StatelessWidget {
  ListQuizUmum({this.topic, this.subTopic});
  final String topic;
  final String subTopic;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return SliverList(
      delegate: SliverChildListDelegate([
        Container(
          child: FutureBuilder<MateriQuizModel>(
              future: MateriQuizServices.getData(value: null),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<MateriQuizDetail> snapshotFiltered = snapshot.data.rows
                      .where(
                          (element) => element.kelas == topic && element.matapelajaran == subTopic)
                      .toList();
                  if (snapshotFiltered.isEmpty) {
                    return AdditionalMateriSettings.noData(context);
                  } else {
                    return Column(
                      children: snapshotFiltered
                          .map((e) => Padding(
                                padding: EdgeInsets.symmetric(horizontal: width * .05),
                                child: QuizChildren(item: e),
                              ))
                          .toList(),
                    );
                  }
                } else {
                  return Padding(
                    padding: EdgeInsets.only(top: width / 3),
                    child: Center(child: CircularProgressIndicator()),
                  );
                }
              }),
        )
      ]),
    );
  }
}

class ListAppUmum extends StatelessWidget {
  ListAppUmum({this.topic, this.subTopic});
  final String topic;
  final String subTopic;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    final materiState = GlobalState.materi();
    print(materiState.state.materi.kelas);
    print(materiState.state.materi.jenjang);
    return SliverList(
        delegate: SliverChildListDelegate([
      Container(
        child: FutureBuilder<StudentRecModel>(
            future: StudentRecServices.getData(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Padding(
                    padding: EdgeInsets.only(top: width / 3),
                    child: Center(
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green))));
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData) {
                  var snapshotFiltered =
                      snapshot.data.rows.where((element) => element.kelas == 'Umum').toList();
                  if (snapshotFiltered.isEmpty) {
                    ///showing widget noData
                    return AdditionalMateriSettings.noData(context);
                  }
                  return Container(
                    color: Colors.white,
                    child: Column(
                      children: snapshotFiltered
                          .map((e) => Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * .03, vertical: width * .025),
                                child: InkWell(
                                  onTap: () => LaunchServices.launchInBrowser(e.link),
                                  child: Container(
                                    height: width * .23,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width * .04, vertical: width * .02),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Colors.white,
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: width * .2,
                                          height: width * .2,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(12),
                                              image: DecorationImage(
                                                  image: NetworkImage(
                                                e.icon,
                                              ))),
                                        ),
                                        SizedBox(width: 15),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Flexible(
                                                child: RichText(
                                                  maxLines: 2,
                                                  overflow: TextOverflow.ellipsis,
                                                  strutStyle: StrutStyle(fontSize: width / 26),
                                                  text: TextSpan(
                                                    text: e.namaaplikasi,
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.w700,
                                                      fontSize: width / 28,
                                                      color: SiswamediaTheme.green,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(height: 10),
                                              Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: width * .015,
                                                      vertical: width * .007),
                                                  decoration: BoxDecoration(
                                                    color: SiswamediaTheme.green,
                                                    borderRadius: BorderRadius.circular(6),
                                                  ),
                                                  child: Text(
                                                    e.kategori,
                                                    style: TextStyle(
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: width / 34,
                                                      color: Colors.white,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ))
                          .toList(),
                    ),
                  );
                }
                return SizedBox();
              }
              return SizedBox();
            }),
      )
    ]));
  }
}

class DocumentUmumChildren extends StatelessWidget {
  DocumentUmumChildren({this.item});
  final MateriDocItemModel item;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    final materiState = GlobalState.materi();
    return InkWell(
      onTap: () async {
        await materiState.setState((s) => s.setJudul(item.judul));
        // PDFDocument document = await PDFDocument.fromURL(item.link);
        // await Navigator.push(
        //     context,
        //     MaterialPageRoute<void>(
        //         builder: (_) => PDFViewerFromUrl(doc: document)));
      },
      child: Container(
          height: width * .3,
          width: width * .9,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Flexible(
                    child: RichText(
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      strutStyle: StrutStyle(fontSize: width / 30),
                      text: TextSpan(
                        text: item.judul,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: width / 28,
                          letterSpacing: 0.18,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      showModalBottomSheet<void>(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          context: context,
                          builder: (context) => Container(
                                height: width / 1.8,
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * .05, vertical: width * .05),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                        topLeft: const Radius.circular(20.0),
                                        topRight: const Radius.circular(20.0))),
                                child: Column(
                                  //mainAxisAlignment:
                                  //  MainAxisAlignment.spaceEvenly,
                                  children: [
                                    // ListTile(
                                    //   leading: Icon(Icons.bookmark_border),
                                    //   title: Text(
                                    //     'Simpan Ke Materiku',
                                    //     style: secondHeadingBlack.copyWith(
                                    //         fontSize: width / 28),
                                    //   ),
                                    //   onTap: () {},
                                    // ),
                                    ListTile(
                                      leading: Icon(Icons.screen_share),
                                      title: Text('Share',
                                          style: secondHeadingBlack.copyWith(fontSize: width / 28)),
                                      onTap: () async {
                                        final RenderBox box = context.findRenderObject();
                                        await Share.share(item.link,
                                            subject: 'Share materi ke',
                                            sharePositionOrigin:
                                                box.localToGlobal(Offset.zero) & box.size);
                                      },
                                    ),
                                    // ListTile(
                                    //   leading: Icon(Icons.outlined_flag),
                                    //   title: Text('Laporkan',
                                    //       style:
                                    //           secondHeadingBlack.copyWith(
                                    //               fontSize: width / 28)),
                                    //   onTap: () =>
                                    //       FABAction.laporkan(context),
                                    // )
                                  ],
                                ),
                              ));
                    },
                    child: Icon(Icons.more_vert),
                  )
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Status ',
                            style: TextStyle(fontSize: width / 34, color: Colors.grey[500])),
                        TextSpan(
                            text: item.status,
                            style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w700,
                                color: Colors.black)),
                      ],
                    ),
                  ),
                ]),
                SizedBox(height: 10),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: [
                //     Row(children: [
                //       Row(children: [
                //         Icon(Icons.remove_red_eye, color: Color(0xffA4A4A4)),
                //         SizedBox(width: 10),
                //         Text(Format.format(
                //             (math.Random.secure().nextInt(3000) + 900)
                //                 .toString()))
                //       ]),
                //       SizedBox(width: 40),
                //       Row(children: [
                //         Icon(Icons.favorite, color: Color(0xffA4A4A4)),
                //         SizedBox(width: 10),
                //         Text(Format.format(
                //             math.Random.secure().nextInt(900).toString()))
                //       ])
                //     ]),
                //     InkWell(
                //       child: Icon(Icons.bookmark_border),
                //       onTap: () {},
                //     )
                //   ],
                // ),
                Divider(color: Colors.grey[300]),
              ])),
    );
  }
}

class VideoUmumChildren extends StatelessWidget {
  VideoUmumChildren({this.item});
  final MateriVideoItemModel item;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        height: width * .67,
        width: width * .94,
        padding: EdgeInsets.symmetric(horizontal: width * .03, vertical: width * .01),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: width * .87,
                height: width * .41,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.grey.withOpacity(.2),
                  image: DecorationImage(image: NetworkImage(item.thumb), fit: BoxFit.cover),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => Navigator.push(
                        context, MaterialPageRoute<void>(builder: (_) => JIJI(url: item.link))),
                    child: Center(
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          color: Colors.green.withOpacity(.3),
                          border: Border.all(color: Colors.white, width: 2),
                          shape: BoxShape.circle,
                        ),
                        child: Icon(Icons.play_arrow, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: RichText(
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  strutStyle: StrutStyle(fontSize: width / 26),
                  text: TextSpan(
                    text: item.judul,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: width / 26,
                      letterSpacing: 0.18,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Text(item.status,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: width / 34,
                    letterSpacing: 0.18,
                    color: Colors.black,
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  /*Container(
                    height: 20,
                    width: 20,
                    child: Icon(
                      Icons.remove_red_eye,
                      color: Colors.grey,
                      size: width / 22,
                    ),
                  ),
                  Text(format((videoRate.watch ?? 0)),
                      style: TextStyle(
                        fontSize: width / 34,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                      )),
                  SizedBox(width: 5),
                  Container(
                    height: 20,
                    width: 20,
                    child: Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size: width / 22,
                    ),
                  ),
                  Text(format((videoRate.likes ?? 0)),
                      style: TextStyle(
                        fontSize: width / 34,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                      )),
                  SizedBox(width: 5),*/
                  Container(
                    height: width / 22,
                    width: width / 22,
                    child: Image.asset('assets/icon gold.png'),
                  ),
                  SizedBox(width: 10),
                  Text('300 Gold',
                      style: TextStyle(
                        fontSize: width / 34,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff006076),
                      )),
                  SizedBox(width: 15),
                  Container(
                    height: width / 22,
                    width: width / 22,
                    child: Image.asset('assets/icon xp.png'),
                  ),
                  SizedBox(width: 10),
                  Text('400 EXP',
                      style: TextStyle(
                        fontSize: width / 34,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff006076),
                      )),
                  SizedBox(width: 5),
                  Expanded(
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.pin_drop, color: Colors.grey)),
                  ),
                ],
              ),
              /*FutureBuilder<VideoRate>(
                  future: getLikes(widget.item.link),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 20,
                            width: 20,
                            child: Icon(
                              Icons.remove_red_eye,
                              color: Colors.grey,
                            ),
                          ),
                          Text(format(videoRate.watch),
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Icon(
                              Icons.favorite,
                              color: Colors.red,
                            ),
                          ),
                          Text(format(videoRate.likes),
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Image.asset('assets/icon gold.png'),
                          ),
                          Text('300 Gold',
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff006076),
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Image.asset('assets/icon xp.png'),
                          ),
                          Text('400 EXP',
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff006076),
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Icon(Icons.pin_drop, color: Colors.grey),
                          ),
                        ],
                      );
                    }
                    return Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey[100],
                        enabled: true,
                        child: Container(
                          height: 20,
                          width: width * .9,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(6),
                          ),
                        ));
                  }),*/
              Divider(color: Colors.grey),
            ]));
  }
}
