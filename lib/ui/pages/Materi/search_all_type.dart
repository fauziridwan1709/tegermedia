part of '_materi.dart';

class ListVideoSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var data = Provider.of<MateriProvider>(context);
    final materiState = GlobalState.materi();
    var kodeApi =
        JenjangData.getApiKodePelajaran(materiState.state.materi.jenjang);
    return FutureBuilder<MateriVideoModel>(
      future: MateriServices.getVideoData(materiState.state.materi.kelas,
          'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=$kodeApi&columns=false'),
      builder: (context, snapshot) {
        if (materiState.state.search == '01=') {
          return SizedBox();
        } else if (snapshot.hasData) {
          if (snapshot.data.rows
              .where((element) =>
                  element.kelas ==
                      (TipePelajaran.umum
                              .contains(materiState.state.materi.kelas)
                          ? materiState.state.materi.kelas
                          : materiState.state.materi.kelas.substring(
                              materiState.state.materi.kelas.indexOf(' ') +
                                  1)) &&
                  (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                      ? true
                      : element.kurikulum.toString() ==
                          materiState.state.materi.kurikulum) &&
                  element.judul
                      .toLowerCase()
                      .contains(materiState.state.search))
              .toList()
              .isEmpty) {
            return Padding(
              padding: EdgeInsets.only(top: S.w / 4),
              child: Center(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/update.png',
                    height: S.w / 2.4,
                    width: S.w / 2.4,
                  ),
                  Text('Tunggu, akan segera kita tambahkan',
                      style: TextStyle(
                          color: SiswamediaTheme.green, fontSize: S.w / 30))
                ],
              )),
            );
          }
          return Container(
            padding: EdgeInsets.symmetric(
                horizontal: S.w * .03, vertical: S.w * .05),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12), topRight: Radius.circular(12)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: snapshot.data.rows
                  .where((element) =>
                      element.kelas ==
                          (TipePelajaran.umum
                                  .contains(materiState.state.materi.kelas)
                              ? materiState.state.materi.kelas
                              : materiState.state.materi.kelas.substring(
                                  materiState.state.materi.kelas
                                          .indexOf('' '') +
                                      1)) &&
                      (TipePelajaran.umum
                              .contains(materiState.state.materi.kelas)
                          ? true
                          : element.kurikulum.toString() ==
                              materiState.state.materi.kurikulum) &&
                      element.judul
                          .toLowerCase()
                          .contains(materiState.state.search))
                  .map((e) => VideoChildren(
                        item: e,
                        search: true,
                      ))
                  .toList(),
            ),
          );
        } else {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Padding(
              padding: EdgeInsets.only(top: S.w / 3),
              child: Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation(SiswamediaTheme.green))),
            );
          } else {
            return Padding(
              padding: EdgeInsets.only(top: S.w / 4),
              child: Center(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/update.png',
                    height: S.w / 2.4,
                    width: S.w / 2.4,
                  ),
                  Text('Tunggu, akan segera kita tambahkan',
                      style: TextStyle(
                          color: SiswamediaTheme.green, fontSize: S.w / 30))
                ],
              )),
            );
          }
        }
      },
    );
  }
}

class ListPresentasiSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    // var data = Provider.of<MateriProvider>(context);
    final materiState = GlobalState.materi();
    var kodeApi = JenjangData.getApiKodePres(
        materiState.state.materi.jenjang, materiState.state.materi.kelas);
    return FutureBuilder<MateriPresModel>(
      future: MateriServices.getPresData(materiState.state.materi.kelas,
          'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=$kodeApi&columns=false'),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Padding(
              padding: EdgeInsets.only(top: width / 3),
              child: Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation(SiswamediaTheme.green))));
        } else if (snapshot.connectionState == ConnectionState.done) {
          if (materiState.state.search == '01=') {
            return SizedBox();
          } else if (snapshot.data.rows
              .where((element) =>
                  element.kelas ==
                      (TipePelajaran.umum
                              .contains(materiState.state.materi.kelas)
                          ? materiState.state.materi.kelas
                          : materiState.state.materi.jenjang == 'sma'
                              ? materiState.state.materi.kelas.substring(
                                  materiState.state.materi.kelas
                                          .indexOf('' '') +
                                      1,
                                  materiState.state.materi.kelas
                                          .indexOf('' '') +
                                      3)
                              : materiState.state.materi.kelas.substring(
                                  materiState.state.materi.kelas
                                          .indexOf('' '') +
                                      1)) &&
                  (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                      ? true
                      : element.kurikulum.toString() ==
                          materiState.state.materi.kurikulum) &&
                  element.judul
                      .toLowerCase()
                      .contains(materiState.state.search))
              .toList()
              .isEmpty) {
            return Padding(
              padding: EdgeInsets.only(top: width / 4),
              child: Center(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/update.png',
                    height: width / 2.4,
                    width: width / 2.4,
                  ),
                  Text('Tunggu, akan segera kita tambahkan',
                      style: TextStyle(
                          color: SiswamediaTheme.green, fontSize: width / 30))
                ],
              )),
            );
          } else {
            return Container(
              padding: EdgeInsets.symmetric(
                  horizontal: width * .05, vertical: width * .05),
              color: Colors.white,
              child: Column(
                children: snapshot.data.rows
                    .where((element) =>
                        element.kelas ==
                            (TipePelajaran.umum
                                    .contains(materiState.state.materi.kelas)
                                ? materiState.state.materi.kelas
                                : materiState.state.materi.jenjang == 'sma'
                                    ? materiState.state.materi.kelas.substring(
                                        materiState.state.materi.kelas
                                                .indexOf('' '') +
                                            1,
                                        materiState.state.materi.kelas
                                                .indexOf('' '') +
                                            3)
                                    : materiState.state.materi.kelas.substring(
                                        materiState.state.materi.kelas
                                                .indexOf('' '') +
                                            1)) &&
                        (TipePelajaran.umum
                                .contains(materiState.state.materi.kelas)
                            ? true
                            : element.kurikulum.toString() ==
                                materiState.state.materi.kurikulum) &&
                        element.judul
                            .toLowerCase()
                            .contains(materiState.state.search))
                    .map((e) => PresentasiChildren(
                          item: e,
                          search: true,
                        ))
                    .toList(),
              ),
            );
          }
        }
        return SizedBox();
      },
    );
  }
}

class ListEbookSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var data = Provider.of<MateriProvider>(context);
    final materiState = GlobalState.materi();
    var kodeApi = JenjangData.getApiKodeDoc(
        materiState.state.materi.jenjang, materiState.state.materi.kelas);
    print(kodeApi);
    print(materiState.state.materi.kelas);
    return FutureBuilder<MateriDocModel>(
      future: MateriServices.getDocData(materiState.state.materi.kelas,
          'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=$kodeApi&columns=false'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          print(snapshot.data.rows);
          if (materiState.state.search == '01=') {
            return SizedBox();
          } else if (snapshot.data.rows
              .where((element) =>
                  element.kelas ==
                      (TipePelajaran.umum
                              .contains(materiState.state.materi.kelas)
                          ? materiState.state.materi.kelas
                          : materiState.state.materi.jenjang == 'sma'
                              ? materiState.state.materi.kelas.substring(
                                  materiState.state.materi.kelas.indexOf(' ') +
                                      1,
                                  materiState.state.materi.kelas.indexOf(' ') +
                                      3)
                              : materiState.state.materi.kelas.substring(
                                  materiState.state.materi.kelas.indexOf(' ') +
                                      1)) &&
                  (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                      ? true
                      : element.kurikulum ==
                          materiState.state.materi.kurikulum) &&
                  element.judul
                      .toLowerCase()
                      .contains(materiState.state.search))
              .toList()
              .isEmpty) {
            print('masuk sini dong');
            return Padding(
              padding: EdgeInsets.only(top: S.w / 4),
              child: Center(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/update.png',
                    height: S.w / 2.4,
                    width: S.w / 2.4,
                  ),
                  Text('Tunggu, akan segera kita tambahkan',
                      style: TextStyle(
                          color: SiswamediaTheme.green, fontSize: S.w / 30))
                ],
              )),
            );
          }
          return Container(
            color: Colors.white,
            padding: EdgeInsets.only(
                left: S.w * .05, right: S.w * .05, top: S.w * .05),
            child: Column(
              children: snapshot.data.rows
                  .where((element) =>
                      element.kelas ==
                          (TipePelajaran.umum
                                  .contains(materiState.state.materi.kelas)
                              ? materiState.state.materi.kelas
                              : materiState.state.materi.jenjang == 'sma'
                                  ? materiState.state.materi.kelas.substring(
                                      materiState.state.materi.kelas
                                              .indexOf('' '') +
                                          1,
                                      materiState.state.materi.kelas
                                              .indexOf('' '') +
                                          3)
                                  : materiState.state.materi.kelas.substring(
                                      materiState.state.materi.kelas
                                              .indexOf('' '') +
                                          1)) &&
                      (TipePelajaran.umum
                              .contains(materiState.state.materi.kelas)
                          ? true
                          : element.kurikulum ==
                              materiState.state.materi.kurikulum) &&
                      element.judul
                          .toLowerCase()
                          .contains(materiState.state.search))
                  .map((e) => EbookChildren(
                        item: e,
                        search: true,
                      ))
                  .toList(),
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Padding(
              padding: EdgeInsets.only(top: S.w / 3),
              child: Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation(SiswamediaTheme.green))));
        } else {
          return Padding(
            padding: EdgeInsets.only(top: S.w / 4),
            child: Center(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/update.png',
                  height: S.w / 2.4,
                  width: S.w / 2.4,
                ),
                Text('Tunggu, akan segera kita tambahkan',
                    style: TextStyle(
                        color: SiswamediaTheme.green, fontSize: S.w / 30))
              ],
            )),
          );
        }
      },
    );
  }
}
