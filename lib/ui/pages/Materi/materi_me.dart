part of '_materi.dart';

class MateriMe extends StatefulWidget {
  @override
  _MateriMeState createState() => _MateriMeState();
}

class _MateriMeState extends State<MateriMe> {
  List<Widget> listViews = List();
  List<Widget> jamal;
  List<String> me = ['Dokumen', 'Presentasi', 'Video', 'Unggahan'];

  @override
  void initState() {
    super.initState();
    jamal = [MyBookList(), MyPresentationList(), MyVideoList(), MyAppList()];
  }

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    final materiState = GlobalState.materi();
    return ScreenTypeLayout(
      mobile: Scaffold(
          floatingActionButton: FloatingActionButton(
            elevation: 0.0,
            backgroundColor: SiswamediaTheme.green,
            onPressed: () => FABAction.floatingAction(context),
            child: Icon(Icons.add),
          ),
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(S.w / 2.4),
              child: AppBar(
                backgroundColor: SiswamediaTheme.green,
                leading: Container(),
                shadowColor: Colors.transparent,
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(S.w / 3),
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: S.w * .06, right: S.w * .05, left: S.w * .05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: S.w / 10.5,
                              width: S.w / 10.5,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Center(
                                child: IconButton(
                                  padding: EdgeInsets.all(0),
                                  icon: Icon(Icons.arrow_back),
                                  onPressed: () => Navigator.pop(context, true),
                                ),
                              ),
                            ),

                            ///
                            Container(
                                width: S.w * .2,
                                height: S.w * .2,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/icons/materi/me${currentIndex + 1}.png'),
                                        fit: BoxFit.cover))),
                            SizedBox(
                              width: S.w / 9,
                            ),
                          ],
                        ),
                        Container(
                          decoration:
                              BoxDecoration(color: SiswamediaTheme.green),
                          child: Padding(
                            padding: EdgeInsets.only(top: S.w / 120),
                            child: Text('Materiku', //Colors.black87
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white, // was lightText
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: S.w * .15),
                  constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height -
                          AppBar().preferredSize.height -
                          S.w / 2.4),
                  color: Colors.white,
                  //margin: EdgeInsets.only(bottom: width * .15),
                  child: Column(
                    children: [
                      //TODO implement [API] here
                      jamal.elementAt(currentIndex),
                    ],
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                color: SiswamediaTheme.background,
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .02),
                child: Wrap(
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.start,
                  runSpacing: 0.0,
                  spacing: S.w / 72,
                  children: me
                      .map((e) => ActionChip(
                          label: Text(
                            e,
                            style: TextStyle(fontSize: S.w / 34),
                          ),
                          labelStyle: TextStyle(
                            color: currentIndex == me.indexOf(e)
                                ? Colors.white
                                : Colors.black,
                          ),
                          backgroundColor: currentIndex == me.indexOf(e)
                              ? SiswamediaTheme.green
                              : Colors.white,
                          onPressed: () {
                            _selectyaaa(me.indexOf(e));
                          }))
                      .toList(),
                ),
              ),
            ],
          )),
      desktop: Scaffold(
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(S.h * .2),
              child: AppBar(
                backgroundColor: Colors.white,
                leading: Container(),
                shadowColor: Colors.transparent,
                flexibleSpace: Image.asset(
                  'assets/materi_topnav_background.png',
                  fit: BoxFit.cover,
                ),
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(S.w / 3),
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: S.w * .06, right: S.w * .05, left: S.w * .05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: S.w / 10.5,
                              width: S.w / 10.5,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Center(
                                child: IconButton(
                                  padding: EdgeInsets.all(0),
                                  icon: Icon(Icons.arrow_back),
                                  onPressed: () => Navigator.pop(context, true),
                                ),
                              ),
                            ),
                            Container(
                                width: S.w * .2,
                                height: S.w * .2,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/icons/materi/${(TipePelajaran.umum.contains(materiState.state.materi.kelas) ? materiState.state.materi.kelas : materiState.state.materi.pelajaran)}.png'),
                                        fit: BoxFit.cover))),
                            SizedBox(
                              width: S.w / 9,
                            ),
                          ],
                        ),
                        Container(
                          decoration:
                              BoxDecoration(color: SiswamediaTheme.green),
                          child: Padding(
                            padding: EdgeInsets.only(top: S.w / 120),
                            child: Text('aduh',
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white, // was lightText
                                )),
                          ),
                        ),
                        Container(
                          decoration:
                              BoxDecoration(color: SiswamediaTheme.green),
                          child: Text(
                              '${(TipePelajaran.umum.contains(materiState.state.materi.kelas) ? 'Umum' : materiState.state.materi.kelas)} - ${materiState.state.materi.kurikulum}',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Colors.white, // was lightText
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: S.w * .15),
                  child: Column(
                    children: [
                      //TODO implement [API] here
                      jamal.elementAt(currentIndex),
                    ],
                  ),
                ),
              ),
              Container(
                width: S.w,
                color: SiswamediaTheme.background,
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .02),
                child: Wrap(
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.start,
                  runSpacing: 0.0,
                  spacing: S.w / 72,
                  children: TipePelajaran.tipeBelajarSma
                      .map((e) => ActionChip(
                          label: Text(
                            e,
                            style: TextStyle(fontSize: S.w / 30),
                          ),
                          labelStyle: TextStyle(
                            color: currentIndex ==
                                    TipePelajaran.tipeBelajarSma.indexOf(e)
                                ? Colors.white
                                : Colors.black,
                          ),
                          backgroundColor: currentIndex ==
                                  TipePelajaran.tipeBelajarSma.indexOf(e)
                              ? SiswamediaTheme.green
                              : Colors.grey.shade200,
                          onPressed: () {
                            _selectyaaa(
                                TipePelajaran.tipeBelajarSma.indexOf(e));
                          }))
                      .toList(),
                ),
              ),
            ],
          )),
    );
  }

  void _selectyaaa(int i) {
    setState(() {
      currentIndex = i;
    });
    print(currentIndex);
  }
}
