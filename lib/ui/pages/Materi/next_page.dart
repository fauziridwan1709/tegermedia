part of "_materi.dart";

class NextPage extends StatefulWidget {
  NextPage({Key key, this.jenjang, this.kurikulum, this.kelas, this.jurusan})
      : super(key: key);
  final String jenjang;
  final String kurikulum;
  final String kelas;
  final String jurusan;

  @override
  _NextPageState createState() => _NextPageState();
}

class _NextPageState extends State<NextPage> {
  List<Widget> listViews = <Widget>[];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(width * .1),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: ProfilePage(kelas: widget.kelas, jenjang: widget.jenjang),
          ),
        ),
      ),
      body: ListView.builder(
          itemCount: listViews.length,
          itemBuilder: (context, index) => listViews[index]),
    );
  }
}

class ProfilePage extends StatelessWidget {
  ProfilePage({this.kelas, this.jenjang});
  final String kelas;
  final String jenjang;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(children: [
          IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.blue,
              ),
              onPressed: () => Navigator.pop(context, true)),
          CircleAvatar(
            backgroundImage: AssetImage('assets/icons/materi/$jenjang.png'),
          ),
          SizedBox(width: 10),
          Text('$kelas'),
        ]),
        Row(
          children: [Text('cari materi'), Icon(Icons.search)],
        )
      ],
    );
  }
}

class Pelajaran extends StatelessWidget {
  Pelajaran({this.scaffoldKey, this.orientation});
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Orientation orientation;

  final materiState = GlobalState.materi();

  @override
  Widget build(BuildContext context) {
    var isPortrait = orientation == Orientation.portrait;
    if (materiState.state.materi.jenjang == null) {
      return SizedBox();
    }
    return Container(
        padding: EdgeInsets.only(
          left: S.w * .04,
          right: S.w * .04,
        ),
        color: Colors.white,
        // margin: EdgeInsets.symmetric(vertical: S.w * .015),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            Text(
              'Materi Pelajaran',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: S.w / 25,
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 15,
            ),
            (materiState.state.materi.jenjang == 'paud' ||
                        materiState.state.materi.jenjang == 'smk') ==
                    true
                ? Center(
                    child:
                        Text('mata pelajaran belum tersedia untuk jenjang ini'))
                : Container(
                    child: GridView.count(
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        crossAxisCount: isPortrait ? 4 : 8,
                        childAspectRatio: 1,
                        mainAxisSpacing: S.w / 60,
                        children: (materiState.state.materi.name == null
                                ? MataPelajaran.mataPelajaranTk
                                : MataPelajaran.getMataPelajaran(materiState
                                    .state.materi.name
                                    .substring(materiState.state.materi.name
                                            .indexOf(' ') +
                                        1)))
                            .map((e) => Container(
                                  height: S.w / 3.78,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: S.w / 8.3,
                                        width: S.w / 8.3,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.grey.withOpacity(.2),
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/icons/materi/$e.png'),
                                              fit: BoxFit.cover),
                                        ),
                                        child: Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                              onTap: () => callback(
                                                    context: context,
                                                    tipe: e,
                                                    pelajaran: e,
                                                  )),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Expanded(
                                        child: Text(
                                          e,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: S.w / 35,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ],
                                  ),
                                ))
                            .toList()),
                  ),
          ],
        ));
  }

  void callback(
      {String tipe, BuildContext context, String pelajaran, String kurikulum}) {
    switch (tipe) {
      case 'lainnya':
        print(2);
        showModalBottomSheet<void>(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            context: context,
            builder: (context) => Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .05),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(20.0),
                          topRight: const Radius.circular(20.0))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Materi Selengkapnya',
                              style: TextStyle(
                                  fontSize: S.w / 28,
                                  fontWeight: FontWeight.w600)),
                          IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: () => Navigator.pop(context, true),
                          )
                        ],
                      ),
                      SizedBox(height: 5),
                      Expanded(
                        //height: width * .8,
                        child: GridView.count(
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            crossAxisCount: 4,
                            mainAxisSpacing: S.w / 36,
                            children: (materiState.state.materi.name == null
                                    ? MataPelajaran.mataPelajaranTk
                                    : MataPelajaran.getMataPelajaranLain(
                                        materiState.state.materi.name.substring(
                                            materiState.state.materi.name
                                                    .indexOf(' ') +
                                                1)))
                                .map((e) => Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          height: S.w / 7.3,
                                          width: S.w / 7.3,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    'assets/icons/materi/$e.png'),
                                                fit: BoxFit.cover),
                                          ),
                                          child: Material(
                                            color: Colors.transparent,
                                            child: InkWell(onTap: () {
                                              Navigator.pop(context, true);
                                              materiState.setState(
                                                  (s) => s.setPelajaran(e));
                                              if (materiState
                                                      .state.materi.jenjang ==
                                                  null) {
                                                CustomFlushBar.errorFlushBar(
                                                    'Anda Belum memilih jenjang',
                                                    context);
                                              } else {
                                                materiState.setState((s) =>
                                                    s.setKelas(materiState
                                                        .state.materi.name));
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute<void>(
                                                        settings: RouteSettings(
                                                            name: "/materi"),
                                                        builder: (_) =>
                                                            MateriAll()));
                                              }
                                            }),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 6,
                                        ),
                                        SizedBox(
                                          height: S.w / 14.2,
                                          child: Text(
                                            e,
                                            style: TextStyle(
                                                fontSize: S.w / 35,
                                                fontWeight: FontWeight.w400),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ))
                                .toList()),
                      ),
                    ],
                  ),
                ));
        break;
      default:
        print(1);
        materiState.setState((s) => s.setPelajaran(pelajaran));
        materiState.setState((s) => s.setKelas(materiState.state.materi.name));
        if (materiState.state.materi.jenjang == null) {
          CustomFlushBar.errorFlushBar('anda belum memilih jenjang', context);
        } else {
          Navigator.push(
              context, MaterialPageRoute<void>(builder: (_) => MateriAll()));
        }
        break;
    }
  }
}
