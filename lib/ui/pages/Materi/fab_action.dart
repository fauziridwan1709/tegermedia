part of '_materi.dart';

class FABAction {
  static Future<dynamic> laporkan(BuildContext context) async {
    // var S.w = MediaQuery.of(context).size.width;
    Navigator.pop(context);
    return showDialog<void>(
        context: context,
        builder: (context) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  padding: EdgeInsets.all(S.w * .05),
                  child: Column(children: [
                    Text('Mengapa Ingin Melaporkan Materi ini?',
                        style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                    Composer(),
                    SizedBox(height: 15),
                    ButtonTerapkan(
                        callback: () => Navigator.pop(context),
                        title: 'Laporkan'),
                    FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text('Batal', style: descBlack))
                  ]),
                ))));
  }

  static Future<dynamic> share(BuildContext context) async {
    return showDialog<void>(context: context, builder: (context) => Dialog());
  }

  static Future<dynamic> floatingAction(BuildContext context) async {
    await showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        context: context,
        builder: (context) => Container(
              height: S.w / 1.8,
              padding: EdgeInsets.symmetric(
                  horizontal: S.w * .05, vertical: S.w * .05),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(20.0),
                      topRight: const Radius.circular(20.0))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Unggah Materi Baru',
                          style: descBlack.copyWith(
                              fontWeight: FontWeight.bold, fontSize: S.w / 28)),
                      IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () => Navigator.pop(context),
                      )
                    ],
                  ),
                  SizedBox(height: S.w * .05),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: fileType
                            .map(
                              (type) => InkWell(
                                onTap: () {
                                  dialogDev(context,
                                      title: 'Pengembangan',
                                      desc:
                                          'Kalau yang ini lagi tahap penyempurnaan ya! tunggu beberapa hari lagi');
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute<void>(
                                  //       builder: (_) =>
                                  //           UploadMateri(type: type)));
                                },
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/icons/materi/me${fileType.indexOf(type) + 1}.png',
                                      height: S.w * .13,
                                      width: S.w * .13,
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                      '$type',
                                      style: descBlack.copyWith(
                                          fontSize: S.w / 28),
                                    )
                                  ],
                                ),
                              ),
                            )
                            .toList()),
                  ),
                ],
              ),
            ));
  }

  static List<String> fileType = ['Dokumen', 'Presentasi', 'Video', 'Soal'];
}
