part of '_materi.dart';

class UploadMateri extends StatelessWidget {
  UploadMateri({this.type});
  final String type;

  @override
  Widget build(BuildContext context) {
    if (type == "Video") {
      return UploadVideoForm();
    }
    return UploadMateriForm();
  }
}
