part of '_materi.dart';

class MateriUmum extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return Container(
        // height: S.w * .43,
        width: double.infinity,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: S.w * .05),
              child: Text(
                'Materi umum',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: S.w / 25,
                    fontWeight: FontWeight.w600),
              ),
            ),
            SizedBox(height: 5),
            Container(
              height: isPortrait ? S.w * .36 : S.w * .33,
              width: double.infinity,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: topicList
                        .map((e) => Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: isPortrait ? S.w / 4.8 : S.w / 6,
                                  width: isPortrait ? S.w / 4.8 : S.w / 6,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: topicList.indexOf(e) == 0 ||
                                              topicList.indexOf(e) ==
                                                  topicList.length - 1
                                          ? S.w * .05
                                          : 10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(14),
                                    color: Colors.grey.withOpacity(.2),
                                    image: DecorationImage(
                                        image: AssetImage(
                                          'assets/images/asset_materi/$e.png',
                                        ),
                                        fit: BoxFit.cover),
                                  ),
                                  child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute<void>(
                                                settings: RouteSettings(
                                                    name: '/materi'),
                                                builder: (_) =>
                                                    MateriUmumDetail(
                                                        topic: e)));
                                      })),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  width: S.w / 5,
                                  child: Text(e,
                                      textAlign: TextAlign.center,
                                      style: descBlack.copyWith(
                                          fontWeight: FontWeight.normal,
                                          fontSize: isPortrait
                                              ? S.w / 36
                                              : S.w / 40)),
                                ),
                              ],
                            ))
                        .toList()),
              ),
            ),
          ],
        ));
  }
}
