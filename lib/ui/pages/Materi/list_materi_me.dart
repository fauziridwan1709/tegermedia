part of "_materi.dart";

class MyVideoList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AdditionalMateriSettings.noData(context);
  }
}

class MyPresentationList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var data = Provider.of<MateriProvider>(context);
    final materiState = GlobalState.materi();
    var kodeApi = JenjangData.getApiKodePres(
        materiState.state.materi.jenjang, materiState.state.materi.kelas);
    print(kodeApi);
    return AdditionalMateriSettings.noData(context);
  }
}

class MyBookList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var data = Provider.of<MateriProvider>(context);
    final materiState = GlobalState.materi();
    var kodeApi = JenjangData.getApiKodeDoc(
        materiState.state.materi.jenjang, materiState.state.materi.kelas);
    print(kodeApi);
    print(materiState.state.materi.kelas);
    return AdditionalMateriSettings.noData(context);
  }
}

class MyAppList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AdditionalMateriSettings.noData(context);
  }
}

class MeSettings {
  static var materiState = GlobalState.materi();
  static Widget noData(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.only(top: width / 4),
      child: Center(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/update.png',
            height: width / 2.4,
            width: width / 2.4,
          ),
          Text('Tunggu, akan segera kita tambahkan',
              style:
                  TextStyle(color: SiswamediaTheme.green, fontSize: width / 30))
        ],
      )),
    );
  }

  static int length(dynamic snapshot) {
    return snapshot.listData.rows
                .where((dynamic element) =>
                    AdditionalMateriSettings.filter(element))
                .length >
            20
        ? 20
        : snapshot.listData.rows
            .where(
                (dynamic element) => AdditionalMateriSettings.filter(element))
            .length;
  }

  static bool filter(dynamic element) {
    return element.kelas ==
            (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                ? materiState.state.materi.kelas
                : materiState.state.materi.jenjang == 'sma'
                    ? materiState.state.materi.kelas.substring(
                        materiState.state.materi.kelas.indexOf(' ') + 1,
                        materiState.state.materi.kelas.indexOf(' ') + 3)
                    : materiState.state.materi.kelas.substring(
                        materiState.state.materi.kelas.indexOf(' ') + 1)) &&
        (TipePelajaran.umum.contains(materiState.state.materi.kelas)
            ? true
            : element.matapelajaran == materiState.state.materi.pelajaran) &&
        (TipePelajaran.umum.contains(materiState.state.materi.kelas)
            ? true
            : element.kurikulum.toString() ==
                materiState.state.materi.kurikulum.toString());
  }
}
