part of '_materi.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String search = '';
  List<String> type = ['Ebook', 'Presentasi', 'Video'];
  List<Widget> jamal = [
    ListEbookSearch(),
    ListPresentasiSearch(),
    ListVideoSearch()
  ];
  int currentIndex = 0;
  TextEditingController controller = TextEditingController();
  FocusNode fNodeSearch = FocusNode();

  @override
  Widget build(BuildContext context) {
    final materiState = GlobalState.materi();
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          shadowColor: Colors.transparent,
          backgroundColor: Colors.white,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(S.w * .15),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 0),
              child: Column(children: [
                Hero(
                  tag: 'ad',
                  child: Container(
                    width: S.w * .9,
                    height: S.w * .1,
                    color: Colors.white,
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: SiswamediaTheme.white,
                          borderRadius: BorderRadius.all(Radius.circular(S.w)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(.2),
                                offset: Offset(0, 0),
                                spreadRadius: 2,
                                blurRadius: 2)
                          ]),
                      child: Center(
                        child: Material(
                          color: Colors.transparent,
                          child: TextField(
                            onChanged: (v) {
                              setState(() {
                                materiState.setState((s) => s.setSearch(v));
                              });
                            },
                            focusNode: fNodeSearch,
                            controller: controller,
                            style: TextStyle(fontSize: S.w / 28),
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.transparent,
                              hintText: "Cari materi...",
                              hintStyle: TextStyle(fontSize: S.w / 28),
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.search,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: S.w,
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .02),
                  child: Wrap(
                    alignment: WrapAlignment.start,
                    runAlignment: WrapAlignment.start,
                    runSpacing: 0.0,
                    spacing: S.w / 72,
                    children: type
                        .map((e) => ActionChip(
                            label: Text(
                              e,
                              style: TextStyle(fontSize: S.w / 30),
                            ),
                            labelStyle: TextStyle(
                              color: currentIndex == type.indexOf(e)
                                  ? Colors.white
                                  : Colors.black,
                            ),
                            backgroundColor: currentIndex == type.indexOf(e)
                                ? SiswamediaTheme.green
                                : Colors.grey.shade200,
                            onPressed: () {
                              _selectyaaa(type.indexOf(e));
                            }))
                        .toList(),
                  ),
                ),
              ]),
            ),
          ),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: jamal.elementAt(currentIndex),
          ),
        ));
  }

  void _selectyaaa(int i) {
    setState(() {
      currentIndex = i;
    });
    print(currentIndex);
  }
}
