part of '_materi.dart';

class VideoChildren extends StatefulWidget {
  VideoChildren({this.item, this.search});

  final MateriVideoItemModel item;
  final bool search;

  @override
  _VideoChildrenState createState() => _VideoChildrenState();
}

class _VideoChildrenState extends State<VideoChildren> {
  final materiState = GlobalState.materi();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: S.w * .67,
        width: S.w * .94,
        padding:
            EdgeInsets.symmetric(horizontal: S.w * .03, vertical: S.w * .01),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: S.w * .87,
                height: S.w * .41,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.grey.withOpacity(.2),
                  image: DecorationImage(
                      image: NetworkImage(widget.item.thumb),
                      fit: BoxFit.cover),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                            builder: (_) => JIJI(url: widget.item.link))),
                    child: Center(
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          color: Colors.green.withOpacity(.3),
                          border: Border.all(color: Colors.white, width: 2),
                          shape: BoxShape.circle,
                        ),
                        child: Icon(Icons.play_arrow, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: RichText(
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  strutStyle: StrutStyle(fontSize: S.w / 26),
                  text: TextSpan(
                    text: widget.item.judul,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: S.w / 26,
                      letterSpacing: 0.18,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Text(
                  'Kurikulum ${widget.item.kurikulum} ${widget.search ? widget.item.matapelajaran : ''}',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: S.w / 34,
                    letterSpacing: 0.18,
                    color: Colors.black,
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  /*Container(
                    height: 20,
                    width: 20,
                    child: Icon(
                      Icons.remove_red_eye,
                      color: Colors.grey,
                      size: width / 22,
                    ),
                  ),
                  Text(format((videoRate.watch ?? 0)),
                      style: TextStyle(
                        fontSize: width / 34,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                      )),
                  SizedBox(width: 5),
                  Container(
                    height: 20,
                    width: 20,
                    child: Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size: width / 22,
                    ),
                  ),
                  Text(format((videoRate.likes ?? 0)),
                      style: TextStyle(
                        fontSize: width / 34,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                      )),
                  SizedBox(width: 5),*/
                  Container(
                    height: S.w / 22,
                    width: S.w / 22,
                    child: Image.asset('assets/icon gold.png'),
                  ),
                  SizedBox(width: 10),
                  Text('300 Gold',
                      style: TextStyle(
                        fontSize: S.w / 34,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff006076),
                      )),
                  SizedBox(width: 15),
                  Container(
                    height: S.w / 22,
                    width: S.w / 22,
                    child: Image.asset('assets/icon xp.png'),
                  ),
                  SizedBox(width: 10),
                  Text('400 EXP',
                      style: TextStyle(
                        fontSize: S.w / 34,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff006076),
                      )),
                  SizedBox(width: 5),
                  Expanded(
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(Icons.pin_drop, color: Colors.grey)),
                  ),
                ],
              ),
              /*FutureBuilder<VideoRate>(
                  future: getLikes(widget.item.link),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 20,
                            width: 20,
                            child: Icon(
                              Icons.remove_red_eye,
                              color: Colors.grey,
                            ),
                          ),
                          Text(format(videoRate.watch),
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Icon(
                              Icons.favorite,
                              color: Colors.red,
                            ),
                          ),
                          Text(format(videoRate.likes),
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Image.asset('assets/icon gold.png'),
                          ),
                          Text('300 Gold',
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff006076),
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Image.asset('assets/icon xp.png'),
                          ),
                          Text('400 EXP',
                              style: TextStyle(
                                fontSize: width / 34,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff006076),
                              )),
                          SizedBox(width: 5),
                          Container(
                            height: 20,
                            width: 20,
                            child: Icon(Icons.pin_drop, color: Colors.grey),
                          ),
                        ],
                      );
                    }
                    return Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey[100],
                        enabled: true,
                        child: Container(
                          height: 20,
                          width: width * .9,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(6),
                          ),
                        ));
                  }),*/
              Divider(color: Colors.grey),
            ]));
  }

  String format(String value) {
    if (int.parse(value) < 1000) {
      return value;
    } else if (int.parse(value) < 1000000) {
      return '${int.parse(value) ~/ 1000}.${value.substring(value.length - 3, value.length - 2)}K';
    } else {
      return '${int.parse(value) ~/ 1000000}.${value.substring(value.length - 6, value.length - 5)}M';
    }
  }
}

class PresentasiChildren extends StatelessWidget {
  PresentasiChildren({this.item, this.search});

  final MateriPresItem item;
  final bool search;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    final materiState = GlobalState.materi();
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute<void>(
              builder: (_) => WebViewHome(
                    url: item.link,
                  ))),
      child: Container(
          height: width * .3,
          width: width * .9,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: RichText(
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          strutStyle: StrutStyle(fontSize: width / 30),
                          text: TextSpan(
                            text: item.judul,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: width / 28,
                              letterSpacing: 0.18,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          showModalBottomSheet<void>(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                              ),
                              context: context,
                              builder: (context) => Container(
                                    height: width / 1.8,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width * .05,
                                        vertical: width * .05),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            topLeft:
                                                const Radius.circular(20.0),
                                            topRight:
                                                const Radius.circular(20.0))),
                                    child: Column(
                                      //mainAxisAlignment:
                                      //  MainAxisAlignment.spaceEvenly,
                                      children: [
                                        // ListTile(
                                        //   leading: Icon(Icons.bookmark_border),
                                        //   title: Text(
                                        //     'Simpan Ke Materiku',
                                        //     style: secondHeadingBlack.copyWith(
                                        //         fontSize: width / 28),
                                        //   ),
                                        //   onTap: () {},
                                        // ),
                                        ListTile(
                                          leading: Icon(Icons.screen_share),
                                          title: Text('Share',
                                              style:
                                                  secondHeadingBlack.copyWith(
                                                      fontSize: width / 28)),
                                          onTap: () => ShareService.share(context, item.link,
                                              'Share materi ke'),
                                        ),
                                        // ListTile(
                                        //   leading: Icon(Icons.outlined_flag),
                                        //   title: Text('Laporkan',
                                        //       style:
                                        //           secondHeadingBlack.copyWith(
                                        //               fontSize: width / 28)),
                                        //   onTap: () =>
                                        //       FABAction.laporkan(context),
                                        // )
                                      ],
                                    ),
                                  ));
                        },
                        child: Icon(Icons.more_vert),
                      )
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          RichText(
                            text: TextSpan(
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    text: TipePelajaran.umum.contains(
                                            materiState.state.materi.kelas)
                                        ? 'Membahas '
                                        : 'Kurikulum ',
                                    style: TextStyle(
                                        fontSize: width / 34,
                                        color: Colors.grey[500])),
                                TextSpan(
                                    text: TipePelajaran.umum.contains(
                                            materiState.state.materi.kelas)
                                        ? '${item.matapelajaran}'
                                        : '${item.kurikulum} - ',
                                    style: TextStyle(
                                        fontSize: width / 34,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.black)),
                              ],
                            ),
                          ),
                          if (!TipePelajaran.umum
                              .contains(materiState.state.materi.kelas))
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * .015,
                                  vertical: width * .007),
                              decoration: BoxDecoration(
                                  color: SiswamediaTheme.green,
                                  borderRadius: BorderRadius.circular(4)),
                              child: Text(item.pengguna,
                                  style: TextStyle(
                                      fontSize: width / 37,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white)),
                            ),
                        ],
                      ),
                    ]),
                SizedBox(height: 10),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: [
                //     Row(children: [
                //       Row(children: [
                //         Icon(Icons.remove_red_eye, color: Color(0xffA4A4A4)),
                //         SizedBox(width: 10),
                //         Text(Format.format(
                //             (math.Random.secure().nextInt(3000) + 900)
                //                 .toString()))
                //       ]),
                //       SizedBox(width: 40),
                //       Row(children: [
                //         Icon(Icons.favorite, color: Color(0xffA4A4A4)),
                //         SizedBox(width: 10),
                //         Text(Format.format(
                //             math.Random.secure().nextInt(900).toString()))
                //       ])
                //     ]),
                //     InkWell(
                //       child: Icon(Icons.bookmark_border),
                //       onTap: () {},
                //     )
                //   ],
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    search ? Text(item.matapelajaran) : SizedBox(),
                    /*Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                          height: 25,
                          width: 25,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: SiswamediaTheme.green,
                          ),
                          child: Icon(
                            Icons.arrow_downward,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                    )*/
                  ],
                ),
                Divider(color: Colors.grey[300]),
              ])),
    );
  }
}

class EbookChildren extends StatelessWidget {
  EbookChildren({this.item, this.search});

  final MateriDocItemModel item;
  final bool search;

  @override
  Widget build(BuildContext context) {
    final materiState = GlobalState.materi();
    return InkWell(
      onTap: () async {
        await materiState.setState((s) => s.setJudul(item.judul));
        print(item.link);
        // final ChromeSafariBrowser browser =
        //     ChromeSafariBrowser(bFallback: InAppBrowser());
        // browser.open(url: item.link);
        // navigate(
        //     context,
        //     WebViewHome(
        //       url: item.link,
        //       title: item.judul,
        //     ));
        await navigate(
            context, CustomPdfViewer(title: item.judul, url: item.link));
      },
      child: Container(
          height: S.w * .3,
          width: S.w * .9,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: RichText(
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          strutStyle: StrutStyle(fontSize: S.w / 30),
                          text: TextSpan(
                            text: item.judul,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: S.w / 28,
                              letterSpacing: 0.18,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          showModalBottomSheet<void>(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                              ),
                              context: context,
                              builder: (context) => Container(
                                    height: S.w / 1.8,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: S.w * .05,
                                        vertical: S.w * .05),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            topLeft:
                                                const Radius.circular(20.0),
                                            topRight:
                                                const Radius.circular(20.0))),
                                    child: Column(
                                      //mainAxisAlignment:
                                      //  MainAxisAlignment.spaceEvenly,
                                      children: [
                                        // ListTile(
                                        //   leading: Icon(Icons.bookmark_border),
                                        //   title: Text(
                                        //     'Simpan Ke Materiku',
                                        //     style: secondHeadingBlack.copyWith(
                                        //         fontSize: width / 28),
                                        //   ),
                                        //   onTap: () {},
                                        // ),
                                        ListTile(
                                          leading: Icon(Icons.screen_share),
                                          title: Text('Share',
                                              style:
                                                  secondHeadingBlack.copyWith(
                                                      fontSize: S.w / 28)),
                                          onTap: () async {
                                            final RenderBox box =
                                                context.findRenderObject();
                                            await Share.share(item.link,
                                                subject: 'Share materi ke',
                                                sharePositionOrigin:
                                                    box.localToGlobal(
                                                            Offset.zero) &
                                                        box.size);
                                          },
                                        ),
                                        // ListTile(
                                        //   leading: Icon(Icons.outlined_flag),
                                        //   title: Text('Laporkan',
                                        //       style:
                                        //           secondHeadingBlack.copyWith(
                                        //               fontSize: width / 28)),
                                        //   onTap: () =>
                                        //       FABAction.laporkan(context),
                                        // )
                                      ],
                                    ),
                                  ));
                        },
                        child: Icon(Icons.more_vert),
                      )
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          RichText(
                            text: TextSpan(
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    text: TipePelajaran.umum.contains(
                                            materiState.state.materi.kelas)
                                        ? 'Membahas '
                                        : 'Kurikulum ',
                                    style: TextStyle(
                                        fontSize: S.w / 34,
                                        color: Colors.grey[500])),
                                TextSpan(
                                    text: TipePelajaran.umum.contains(
                                            materiState.state.materi.kelas)
                                        ? '${item.matapelajaran}'
                                        : '${item.kurikulum} - ',
                                    style: TextStyle(
                                        fontSize: S.w / 34,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.black)),
                              ],
                            ),
                          ),
                          if (!TipePelajaran.umum
                              .contains(materiState.state.materi.kelas))
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: S.w * .015, vertical: S.w * .007),
                              decoration: BoxDecoration(
                                  color: SiswamediaTheme.green,
                                  borderRadius: BorderRadius.circular(4)),
                              child: Text(item.pengguna,
                                  style: TextStyle(
                                      fontSize: S.w / 37,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white)),
                            ),
                        ],
                      ),
                    ]),
                SizedBox(height: 10),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: [
                //     Row(children: [
                //       Row(children: [
                //         Icon(Icons.remove_red_eye, color: Color(0xffA4A4A4)),
                //         SizedBox(width: 10),
                //         Text(Format.format(
                //             (math.Random.secure().nextInt(3000) + 900)
                //                 .toString()))
                //       ]),
                //       SizedBox(width: 40),
                //       Row(children: [
                //         Icon(Icons.favorite, color: Color(0xffA4A4A4)),
                //         SizedBox(width: 10),
                //         Text(Format.format(
                //             math.Random.secure().nextInt(900).toString()))
                //       ])
                //     ]),
                //     InkWell(
                //       child: Icon(Icons.bookmark_border),
                //       onTap: () {},
                //     )
                //   ],
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    search ? Text(item.matapelajaran) : SizedBox(),
                    /*Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                          height: 25,
                          width: 25,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: SiswamediaTheme.green,
                          ),
                          child: Icon(
                            Icons.arrow_downward,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                    )*/
                  ],
                ),
                Divider(color: Colors.grey[300]),
              ])),
    );
  }
}

class QuizChildren extends StatelessWidget {
  QuizChildren({this.item});

  final MateriQuizDetail item;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    // var quizProvider = Provider.of<QuizProvider>(context);
    final quizState = GlobalState.quiz();
    return Container(
      margin:
          EdgeInsets.symmetric(horizontal: width * .03, vertical: width * .025),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              offset: Offset(2, 2),
              spreadRadius: 2,
              color: Colors.grey.withOpacity(.2),
              blurRadius: 2)
        ],
        borderRadius: BorderRadius.circular(12),
      ),
      child: InkWell(
        onTap: () {
          quizState.setState((s) => s.setDetail(MateriQuizDetail(
              matapelajaran: item.matapelajaran,
              materi: item.materi,
              kelas: item.kelas,
              jumlahsoal: item.jumlahsoal,
              menit: item.menit)));
          navigate(context, QuizInfo());
        },
        child: Container(
          height: width * .38,
          padding: EdgeInsets.symmetric(
              horizontal: width * .04, vertical: width * .04),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(item.materi,
                                style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    color: Colors.black,
                                    fontSize: width / 25)),
                          ),
                          SizedBox(
                            width: width * .05,
                          ),
                          Container(
                            height: width / 16,
                            padding: EdgeInsets.all(width * .01),
                            decoration: BoxDecoration(
                              color: SiswamediaTheme.green,
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Center(
                              child: Text(
                                item.jumlahsoal + ' soal',
                                style: TextStyle(
                                    color: Colors.white, fontSize: width / 36),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Icon(Icons.notification_important,
                        color: Colors.grey.withOpacity(.6)),
                  ],
                ),
              ),
              Text(item.matapelajaran,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black54,
                      fontSize: width / 32)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                      width: width / 20,
                      child: Image.asset('assets/icon xp.png')),
                  Text('100 xp',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.black87,
                          fontSize: width / 34)),
                  SizedBox(
                      width: width / 20,
                      child: Image.asset('assets/icon gold.png')),
                  Text('100 Gold',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.black87,
                          fontSize: width / 34)),
                  Icon(
                    Icons.access_time,
                    color: SiswamediaTheme.green,
                  ),
                  Text('${item.menit} mins',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.black87,
                          fontSize: width / 34)),
                  Icon(
                    Icons.star,
                    color: SiswamediaTheme.green,
                  ),
                  Text('4.5',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: SiswamediaTheme.green,
                          fontSize: width / 34)),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                    decoration: BoxDecoration(
                      color: SiswamediaTheme.green,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Text(
                      'mulai',
                      style:
                          TextStyle(color: Colors.white, fontSize: width / 34),
                    ),
                  )
                ],
              ),
              Divider(color: Colors.grey),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(width: 1),
                  /*Text('Nilai : - ',
                                          style: TextStyle(
                                              fontSize: width / 28,
                                              fontWeight: FontWeight.w600)),*/
                  Text(
                    'Senin, 11 Agustus 2020',
                    style: TextStyle(
                        fontSize: width / 29, fontWeight: FontWeight.w600),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
