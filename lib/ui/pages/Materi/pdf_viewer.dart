part of '_materi.dart';

// class PDFViewerFromUrl extends StatelessWidget {
//   PDFViewerFromUrl({Key key, @required this.doc}) : super(key: key);
//   // Load from URL
//   // final PDFDocument doc;
//
//   @override
//   Widget build(BuildContext context) {
//     // var data = Provider.of<MateriProvider>(context);
//     final materiState = GlobalState.materi();
//     return Scaffold(
//       appBar: AppBar(
//         leading: IconButton(
//           icon: Icon(
//             Icons.arrow_back,
//             color: SiswamediaTheme.green,
//           ),
//           onPressed: () => Navigator.pop(context),
//         ),
//         shadowColor: Colors.transparent,
//         backgroundColor: Colors.white,
//         title: Text(
//           materiState.state.materi.judul,
//           style: TextStyle(color: Colors.black),
//         ),
//       ),
//       body: Center(child: PDFViewer(document: doc)),
//       // PDF(
//       //   enableSwipe: true,
//       //   pageFling: true,
//       //   pageSnap: true,
//       // ).fromUrl(
//       //   url,
//       //   placeholder: (value) => Center(
//       //     child: Column(
//       //         mainAxisAlignment: MainAxisAlignment.center,
//       //         crossAxisAlignment: CrossAxisAlignment.center,
//       //         children: [
//       //           CircularProgressIndicator(),
//       //           SizedBox(height: 10),
//       //           Text(value.round().toString() + '%'),
//       //         ]),
//       //   ),
//       //   errorWidget: (dynamic error) => Center(child: Text(error.toString())),
//       // ),
//     );
//   }
// }

// class PDFViewerFromAsset extends StatelessWidget {
//   PDFViewerFromAsset({Key key, @required this.pdfAssetPath}) : super(key: key);
//   final String pdfAssetPath;
//   final Completer<PDFViewController> _pdfViewController =
//       Completer<PDFViewController>();
//   final _pageCountController = StreamController<String>();
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('PDF From Asset'),
//         actions: <Widget>[
//           StreamBuilder<String>(
//               stream: _pageCountController.stream,
//               builder: (context, snapshot) {
//                 if (snapshot.hasData) {
//                   return Center(
//                     child: Container(
//                       padding: EdgeInsets.all(16),
//                       decoration: BoxDecoration(
//                         shape: BoxShape.circle,
//                         color: Colors.blue[900],
//                       ),
//                       child: Text(snapshot.data),
//                     ),
//                   );
//                 }
//                 return SizedBox();
//               }),
//         ],
//       ),
//       body: PDF(
//         enableSwipe: true,
//         swipeHorizontal: true,
//         autoSpacing: false,
//         pageFling: false,
//         onPageChanged: (current, total) =>
//             _pageCountController.add('${current + 1} - $total'),
//         onViewCreated: (pdfViewController) async {
//           _pdfViewController.complete(pdfViewController);
//           final currentPage = await pdfViewController.getCurrentPage();
//           final pageCount = await pdfViewController.getPageCount();
//           _pageCountController.add('${currentPage + 1} - $pageCount');
//         },
//       ).fromAsset(
//         pdfAssetPath,
//         errorWidget: (dynamic error) => Center(child: Text(error.toString())),
//       ),
//       floatingActionButton: FutureBuilder<PDFViewController>(
//         future: _pdfViewController.future,
//         builder: (context, AsyncSnapshot<PDFViewController> snapshot) {
//           if (snapshot.hasData && snapshot.data != null) {
//             return Row(
//               mainAxisSize: MainAxisSize.max,
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 FloatingActionButton(
//                   heroTag: '-',
//                   onPressed: () async {
//                     final pdfController = snapshot.data;
//                     final currentPage =
//                         await pdfController.getCurrentPage() - 1;
//                     if (currentPage >= 0) {
//                       await snapshot.data.setPage(currentPage);
//                     }
//                   },
//                   child: Text('-'),
//                 ),
//                 FloatingActionButton(
//                   heroTag: '+',
//                   onPressed: () async {
//                     final pdfController = snapshot.data;
//                     final currentPage =
//                         await pdfController.getCurrentPage() + 1;
//                     final numberOfPages = await pdfController.getPageCount();
//                     if (numberOfPages > currentPage) {
//                       await snapshot.data.setPage(currentPage);
//                     }
//                   },
//                   child: Text('+'),
//                 ),
//               ],
//             );
//           }
//           return SizedBox();
//         },
//       ),
//     );
//   }
// }
