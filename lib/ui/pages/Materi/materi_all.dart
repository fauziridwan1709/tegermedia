part of "_materi.dart";

class MateriAll extends StatefulWidget {
  @override
  _MateriAllState createState() => _MateriAllState();
}

class _MateriAllState extends State<MateriAll> {
  final materiState = GlobalState.materi();
  List<Widget> listViews = <Widget>[];
  List<Widget> jamal;

  @override
  void initState() {
    super.initState();
    jamal = [ListEbook(), ListPresentasi(), ListVideo(), ListApp()];
  }

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.red,
    ));

    return ScreenTypeLayout(
      mobile: Scaffold(
          // floatingActionButton: FloatingActionButton(
          //     elevation: 0.0,
          //     child: Icon(Icons.add),
          //     backgroundColor: SiswamediaTheme.green,
          //     onPressed: () {}),
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(S.w / 2.7),
              child: AppBar(
                brightness: Brightness.light,
                leading: Container(),
                backgroundColor: Colors.white,
                shadowColor: Colors.transparent,
                flexibleSpace: Image.asset(
                  'assets/materi_topnav_background.png',
                  fit: BoxFit.cover,
                ),
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(S.w / 3),
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: S.w * .06, right: S.w * .05, left: S.w * .05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: S.w / 10.5,
                              width: S.w / 10.5,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Center(
                                child: IconButton(
                                  padding: EdgeInsets.all(0),
                                  icon: Icon(Icons.arrow_back),
                                  onPressed: () => Navigator.pop(context, true),
                                ),
                              ),
                            ),
                            Container(
                                width: S.w * .2,
                                height: S.w * .2,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/icons/materi/${(TipePelajaran.umum.contains(materiState.state.materi.kelas) ? materiState.state.materi.kelas : materiState.state.materi.pelajaran)}.png'),
                                        fit: BoxFit.cover))),
                            SizedBox(
                              width: S.w / 9,
                            ),
                          ],
                        ),
                        Container(
                          decoration:
                              BoxDecoration(color: SiswamediaTheme.green),
                          child: Padding(
                            padding: EdgeInsets.only(top: S.w / 120),
                            child: Text(
                                (TipePelajaran.umum.contains(
                                        materiState.state.materi.kelas)
                                    ? materiState.state.materi.kelas.toString()
                                    : materiState.state.materi.pelajaran
                                        .toString()), //Colors.black87
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white, // was lightText
                                )),
                          ),
                        ),
                        Container(
                          decoration:
                              BoxDecoration(color: SiswamediaTheme.green),
                          child: Text(
                              "${(TipePelajaran.umum.contains(materiState.state.materi.kelas) ? "Umum" : materiState.state.materi.kelas)} - ${materiState.state.materi.kurikulum}",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Colors.white, // was lightText
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          body: RefreshIndicator(
            onRefresh: () {
              return Future.value(true);
            },
            child: Stack(
              children: [
                Center(
                  child: SingleChildScrollView(
                    child: Container(
                      constraints: BoxConstraints(
                          minHeight: MediaQuery.of(context).size.height -
                              AppBar().preferredSize.height -
                              S.w / 2.4),
                      padding: EdgeInsets.symmetric(vertical: S.w * .15),
                      color: Colors.white,
                      //margin: EdgeInsets.only(bottom: width * .15),
                      child: Column(
                        children: [
                          //TODO implement [API] here
                          jamal.elementAt(currentIndex),
                        ],
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: S.w,
                    color: SiswamediaTheme.background,
                    padding: EdgeInsets.symmetric(
                        horizontal: S.w * .05, vertical: S.w * .02),
                    child: Wrap(
                      alignment: WrapAlignment.start,
                      runAlignment: WrapAlignment.start,
                      runSpacing: 0.0,
                      spacing: S.w / 72,
                      children: TipePelajaran.tipeBelajarSma
                          .map((e) => ActionChip(
                              label: Text(
                                e,
                                style: TextStyle(fontSize: S.w / 30),
                              ),
                              labelStyle: TextStyle(
                                color: currentIndex ==
                                        TipePelajaran.tipeBelajarSma.indexOf(e)
                                    ? Colors.white
                                    : Colors.black,
                              ),
                              backgroundColor: currentIndex ==
                                      TipePelajaran.tipeBelajarSma.indexOf(e)
                                  ? SiswamediaTheme.green
                                  : Colors.white,
                              onPressed: () {
                                _selectyaaa(
                                    TipePelajaran.tipeBelajarSma.indexOf(e));
                              }))
                          .toList(),
                    ),
                  ),
                ),
              ],
            ),
          )),
      desktop: Scaffold(
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(S.h * .2),
              child: AppBar(
                backgroundColor: Colors.white,
                leading: Container(),
                shadowColor: Colors.transparent,
                flexibleSpace: Image.asset(
                  'assets/materi_topnav_background.png',
                  fit: BoxFit.cover,
                ),
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(S.w / 3),
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: S.w * .06, right: S.w * .05, left: S.w * .05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: S.w / 10.5,
                              width: S.w / 10.5,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Center(
                                child: IconButton(
                                  padding: EdgeInsets.all(0),
                                  icon: Icon(Icons.arrow_back),
                                  onPressed: () => Navigator.pop(context, true),
                                ),
                              ),
                            ),
                            Container(
                                width: S.w * .2,
                                height: S.w * .2,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/icons/materi/${(TipePelajaran.umum.contains(materiState.state.materi.kelas) ? materiState.state.materi.kelas : materiState.state.materi.pelajaran)}.png'),
                                        fit: BoxFit.cover))),
                            SizedBox(
                              width: S.w / 9,
                            ),
                          ],
                        ),
                        Container(
                          decoration:
                              BoxDecoration(color: SiswamediaTheme.green),
                          child: Padding(
                            padding: EdgeInsets.only(top: S.w / 120),
                            child: Text(
                                (TipePelajaran.umum.contains(
                                        materiState.state.materi.kelas)
                                    ? materiState.state.materi.kelas
                                    : materiState.state.materi
                                        .pelajaran), //Colors.black87
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white, // was lightText
                                )),
                          ),
                        ),
                        Container(
                          decoration:
                              BoxDecoration(color: SiswamediaTheme.green),
                          child: Text(
                              "${(TipePelajaran.umum.contains(materiState.state.materi.kelas) ? "Umum" : materiState.state.materi.kelas)} - ${materiState.state.materi.kurikulum}",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Colors.white, // was lightText
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: S.w * .15),
                  child: Column(
                    children: [
                      //TODO implement [API] here
                      jamal.elementAt(currentIndex),
                    ],
                  ),
                ),
              ),
              Container(
                width: S.w,
                color: SiswamediaTheme.background,
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .02),
                child: Wrap(
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.start,
                  runSpacing: 0.0,
                  spacing: S.w / 72,
                  children: TipePelajaran.tipeBelajarSma
                      .map((e) => ActionChip(
                          label: Text(
                            e,
                            style: TextStyle(fontSize: S.w / 30),
                          ),
                          labelStyle: TextStyle(
                            color: currentIndex ==
                                    TipePelajaran.tipeBelajarSma.indexOf(e)
                                ? Colors.white
                                : Colors.black,
                          ),
                          backgroundColor: currentIndex ==
                                  TipePelajaran.tipeBelajarSma.indexOf(e)
                              ? SiswamediaTheme.green
                              : Colors.grey.shade200,
                          onPressed: () {
                            _selectyaaa(
                                TipePelajaran.tipeBelajarSma.indexOf(e));
                          }))
                      .toList(),
                ),
              ),
            ],
          )),
    );
  }

  void _selectyaaa(int i) {
    setState(() {
      currentIndex = i;
    });
    print(currentIndex);
  }
}
