part of '_materi.dart';

const Duration duration = Duration(milliseconds: 200);

class CustomExpansionTile extends StatefulWidget {
  CustomExpansionTile({
    Key key,
    this.leading,
    this.title,
    this.subtitle,
    this.backgroundColor,
    this.headerColor,
    this.trailing,
    this.children = const <Widget>[],
    this.initiallyExpanded = true,
    this.isExpanded = false,
    this.controller,
  }) : super(key: key);

  final Widget leading;
  final Widget title;
  final Widget subtitle;
  final List<Widget> children;
  final Widget trailing;
  final Color backgroundColor;
  final Color headerColor;
  final bool initiallyExpanded;
  final bool isExpanded;
  final AnimationController controller;

  @override
  _CustomExpansionTileState createState() => _CustomExpansionTileState();
}

class _CustomExpansionTileState extends State<CustomExpansionTile>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeInTween = CurveTween(curve: Curves.easeInOut);

  final ColorTween _borderColorTween = ColorTween();
  final ColorTween _headerColorTween = ColorTween();
  final ColorTween _iconColorTween = ColorTween();
  final ColorTween _backgroundColorTween = ColorTween();

  Animation<double> _heightFactor;

  bool expanded;
  @override
  void initState() {
    super.initState();
    expanded = widget.isExpanded;
    _heightFactor = widget.controller.drive(_easeInTween);

    expanded = PageStorage.of(context)?.readState(context) as bool ?? widget.initiallyExpanded;
    if (widget.isExpanded) {
      setState(() => widget.controller.value = 1.0);
    }
  }

  Widget _buildChildren(BuildContext context, Widget child) {
    return ClipRect(
      child: Align(
        heightFactor: _heightFactor.value,
        child: child,
      ),
    );
  }

  @override
  void didChangeDependencies() {
    final theme = Theme.of(context);
    _borderColorTween.end = theme.dividerColor;
    _headerColorTween.end = widget.headerColor;
    _iconColorTween
      ..begin = theme.unselectedWidgetColor
      ..end = theme.accentColor;
    _backgroundColorTween.end = widget.backgroundColor;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final closed = !expanded && widget.controller.isDismissed;
    // print(widget.controller.view);
    return AnimatedBuilder(
      animation: widget.controller.view,
      builder: _buildChildren,
      child: closed ? null : Column(children: widget.children),
    );
  }
}

class JadwalTV extends StatelessWidget {
  JadwalTV({this.jenis});
  final String jenis;

  @override
  Widget build(BuildContext context) {
    var kodeApi = JenjangData.getApiKodeJadwal(jenis);
    print(kodeApi);
    var date = DateTime.now();
    return FutureBuilder<JadwalTayangModel>(
        future: MateriServices.getJadwalData(
            'https://api.siswamedia.com/g2j/?id=1SrNt_SO2tbt6aB9FyJcg2pjC5hZbZS9gc2KUADiSraY&sheet=$kodeApi&columns=false'),
        builder: (context, snapshot) {
          if (date.weekday == 6 || date.weekday == 7) {
            return Padding(
              padding: EdgeInsets.all(S.w * .1),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: S.w * .4,
                    height: S.w * .3,
                    child: Image.asset('assets/icons/materi/no_tayangan.png'),
                  ),
                  Text('tidak ada tayangan hari ini',
                      style: TextStyle(color: SiswamediaTheme.black)),
                ],
              ),
            );
          }
          if (snapshot.hasData) {
            return Padding(
              padding: EdgeInsets.only(left: S.w * .05, right: S.w * .05),
              child: Card(
                elevation: 4,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                shadowColor: Colors.grey.withOpacity(.3),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .04, vertical: S.w * .03),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: snapshot.data.rows
                          .where((element) => element.hari.toString() == date.weekday.toString())
                          .map((e) => Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Icon(Icons.live_tv, color: SiswamediaTheme.green),
                                  SizedBox(
                                    width: S.w * .3,
                                    child: Text(
                                      e.acara,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: S.w / 30, fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  SizedBox(width: S.w * .14, child: Divider()),
                                  Text(e.waktutayang,
                                      style: TextStyle(
                                          fontSize: S.w / 30, fontWeight: FontWeight.w600)),
                                ],
                              ))
                          .toList(),
                    ),
                  ),
                ),
              ),
            );
          }
          return Container(
              height: S.w / 2.7,
              child: Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green),
              )));
        });
  }
}

class Om extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    final materiState = Injector.getAsReactive<MateriState>();
    return FutureBuilder<MateriVideoModel>(
        future: MateriServices.getVidFavData(
            materiState.state.materi.jenjang, materiState.state.materi.kelas),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var json = snapshot.data.rows;
            var ind = math.Random.secure().nextInt(15);
            return Container(
              height: S.w * .45,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                padding: EdgeInsets.only(right: S.w * .05, left: S.w * .05),
                children: json
                    .map((e) => Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                          margin:
                              EdgeInsets.symmetric(horizontal: S.w * .025, vertical: S.w * .025),
                          child: Container(
                            width: isPortrait ? S.w * .55 : S.w * .4,
                            height: isPortrait ? S.w * .35 : S.w * .25,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Stack(children: [
                              InkWell(
                                onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute<void>(
                                        builder: (_) => VideoScreen(
                                              url: e.link,
                                            ))),
                                child: Padding(
                                  padding: EdgeInsets.only(bottom: S.w * .03),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: S.w * .25,
                                        width: S.w * .55,
                                        decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(12),
                                              topLeft: Radius.circular(12)),
                                          image: DecorationImage(
                                              image: NetworkImage(e.thumb.toString()),
                                              fit: BoxFit.cover),
                                        ),
                                        child: Center(
                                            child: Container(
                                                width: S.w / 9,
                                                height: S.w / 9,
                                                child: Icon(Icons.play_arrow,
                                                    color: SiswamediaTheme.green),
                                                decoration: BoxDecoration(
                                                    color: Colors.white.withOpacity(.7),
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        width: 2,
                                                        color: SiswamediaTheme.green
                                                            .withOpacity(.3))))),
                                      ),
                                      Container(
                                        padding: EdgeInsets.symmetric(horizontal: S.w * .03),
                                        height: S.w * .12,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Flexible(
                                              child: RichText(
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                strutStyle: StrutStyle(fontSize: S.w / 30),
                                                text: TextSpan(
                                                  text: e.judul,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: S.w / 30,
                                                    letterSpacing: 0.18,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: S.w * .006),
                                            // if (snapshot.data.rows[index].watch ==
                                            //         "0" ||
                                            //     snapshot.data.rows[index].likes == "0")
                                            Text('membahas ${e.matapelajaran}',
                                                style: descBlack.copyWith(fontSize: S.w / 34)),
                                            // FutureBuilder(
                                            //   stream: null,
                                            //   builder: (context, snapshot) {
                                            //     return Row(
                                            //         mainAxisAlignment:
                                            //             MainAxisAlignment.start,
                                            //         children: [
                                            //           Icon(
                                            //             Icons.remove_red_eye,
                                            //             size: width / 20,
                                            //             color: Colors.black87,
                                            //           ),
                                            //           SizedBox(
                                            //             width: width * .01,
                                            //           ),
                                            //           Text(
                                            //
                                            //               ///kelas format di jenjang.dart
                                            //               Format.format(Random.secure().nextInt(max)),
                                            //               style: TextStyle(
                                            //                   fontSize: width / 34)),
                                            //           SizedBox(
                                            //             width: width * .02,
                                            //           ),
                                            //           Icon(Icons.favorite,
                                            //               size: width / 20,
                                            //               color: Colors.red),
                                            //           SizedBox(
                                            //             width: width * .01,
                                            //           ),
                                            //           Text(
                                            //
                                            //               ///kelas format di jenjang.dart
                                            //               Format.format("90"),
                                            //               style: TextStyle(
                                            //                   fontSize: width / 34)),
                                            //         ]);
                                            //   }
                                            // ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 25,
                                left: 0,
                                child: Container(
                                  height: S.w / 18,
                                  width: S.w / 9,
                                  child: Center(
                                      child: Text('video',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: S.w / 36,
                                              fontWeight: FontWeight.w700))),
                                  decoration: BoxDecoration(
                                    color: SiswamediaTheme.green,
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(4),
                                        bottomRight: Radius.circular(4)),
                                  ),
                                ),
                              )
                            ]),
                          ),
                        ))
                    .toList()
                    .sublist(0, 3),
              ),
            );
          }
          return Center(child: CircularProgressIndicator());
        });
  }
}

class AplikasiLain extends StatelessWidget {
  final List<App> linkIcon = [
    App(
        link: 'https://play.google.com/store/apps/details?id=mam.reader.ipusnas',
        thumb:
            'https://lh3.googleusercontent.com/tCJEJQ2_AwxttaepbpgwPoQzBzZG-4MdjK2N8ABtFyU6U0US5WUfpcE6QkxEpxzhhA=s180-rw',
        title: 'iPusnas',
        kategori: 'Buku & Referensi'),
    // App(
    //     link: 'https://play.google.com/store/apps/details?id=fahim_edu.bse',
    //     thumb:
    //         'https://play-lh.googleusercontent.com/PfYm69_DllbBqT-tsE0PYvji_4_f7FA-85qW_JcdpMGsDJJbRpkHMDYmbelhMcVfVzw=s360',
    //     title: 'Buku Sekolah (BSE), KBBI, Kamus dan Soal UN',
    //     kategori: 'Buku & Referensi'),
    // App(
    //     link:
    //         'https://play.google.com/store/apps/details?id=com.rileksaja.ebook',
    //     thumb:
    //         'https://play-lh.googleusercontent.com/B3JPQxVdnBMdo-HXoDS5H6kzrC2yfhHzHXWkVRPr0pZNQXZVV_fSKgHUN6k8f1zQjg=s360',
    //     title: 'Gudang Ilmu - Baca ebook gratis',
    //     kategori: 'Buku & Referensi'),
    // App(
    //     link:
    //         'https://play.google.com/store/apps/details?id=com.oodles.download.free.ebooks.reader',
    //     thumb:
    //         'https://play-lh.googleusercontent.com/IWbZf2SwGnN7rAB8WeBpU5OAjADDSGetI39EEJAtuBnTtoMcu6PyfDzO9vSWo_njoA=s360',
    //     title: '50000 Free eBooks & Free AudioBooks',
    //     kategori: 'Buku & Referensi'),
    // App(
    //     link:
    //         'https://play.google.com/store/apps/details?id=com.spreadsong.freebooks',
    //     thumb:
    //         'https://play-lh.googleusercontent.com/Sm1DxxVQpsMGankADgsbl-5VQuLu9mpSiiMAYtC0bhGmP5kia5dnx6GqW1FHMtOTUU4=s360',
    //     title: 'Free Books – Novels, Fiction Books, & Audiobooks',
    //     kategori: 'Buku & Referensi'),
  ];

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    var orientation = MediaQuery.of(context).orientation;
    var isPortrait = orientation == Orientation.portrait;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: S.w * .03),
      child: Container(
        height: S.w / 2.2,
        child: ListView(
            physics: ScrollPhysics(),
            // crossAxisCount: isPortrait ? 2 : 4,
            shrinkWrap: true,
            // childAspectRatio: 1.39,
            scrollDirection: Axis.horizontal,
            children: linkIcon
                .map((e) => Card(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                      margin: EdgeInsets.symmetric(horizontal: S.w * .02, vertical: S.w * .05),
                      elevation: 4,
                      shadowColor: Colors.grey.withOpacity(.2),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => LaunchServices.launchInBrowser(e.link),
                          child: Container(
                              height: S.w / 2.9,
                              width: S.w / 2,
                              padding:
                                  EdgeInsets.symmetric(horizontal: S.w * .03, vertical: S.w * .03),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: RichText(
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                      strutStyle: StrutStyle(fontSize: S.w / 30),
                                      text: TextSpan(
                                        text: e.title,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: S.w / 28,
                                          letterSpacing: 0.18,
                                          color: SiswamediaTheme.green,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(
                                        width: S.w * .25,
                                        child: AutoSizeText(
                                          e.kategori,
                                          maxLines: 1,
                                          maxFontSize: 16,
                                          minFontSize: 8,
                                        ),
                                      ),
                                      Container(
                                          height: S.w / 9,
                                          width: S.w / 9,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            image: DecorationImage(
                                                image: NetworkImage(e.thumb), fit: BoxFit.cover),
                                          )),
                                    ],
                                  ),
                                ],
                              )),
                        ),
                      ),
                    ))
                .toList()),
      ),
    );
  }
}

class App {
  final String title;
  final String thumb;
  final String link;
  final String kategori;

  App({this.title, this.thumb, this.link, this.kategori});
}

class MateriFav extends StatefulWidget {
  @override
  _MateriFavState createState() => _MateriFavState();
}

class _MateriFavState extends State<MateriFav> {
  int currentIndex;
  List<Widget> jamal;

  @override
  void initState() {
    jamal = [
      ListEbook(),
      ListVideo(),
      ListVideo(),
      ListVideo(),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.value(true),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: Container());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  SizedBox(
                    width: S.w,
                    child: Wrap(
                      alignment: WrapAlignment.start,
                      runAlignment: WrapAlignment.start,
                      runSpacing: 0.0,
                      spacing: 5.0,
                      children: TipePelajaran.tipeBelajarSma
                          .map((e) => ActionChip(
                              label: Text(
                                e,
                                style: TextStyle(fontSize: 12),
                              ),
                              labelStyle: TextStyle(
                                color: currentIndex == TipePelajaran.tipeBelajarSma.indexOf(e)
                                    ? Colors.white
                                    : Colors.black,
                              ),
                              backgroundColor:
                                  currentIndex == TipePelajaran.tipeBelajarSma.indexOf(e)
                                      ? Colors.blue
                                      : Colors.grey.shade200,
                              onPressed: () {
                                _selectyaaa(TipePelajaran.tipeBelajarSma.indexOf(e));
                              }))
                          .toList(),
                    ),
                  ),
                  //TODO implement [API] here
                  jamal.elementAt(currentIndex),
                ],
              );
            } else {
              return Center(child: Text('belum ada data untuk kategori ini'));
            }
          }
          return Container();
        });
  }

  void _selectyaaa(int i) {
    setState(() {
      currentIndex = i;
    });
    print(currentIndex);
  }
}

class Format {
  static String format(String value) {
    if (int.parse(value) < 1000) {
      return value;
    } else if (int.parse(value) < 1000000) {
      return '${int.parse(value) ~/ 1000}.${value.substring(value.length - 3, value.length - 2)}K';
    } else {
      return '${int.parse(value) ~/ 1000000}.${value.substring(value.length - 6, value.length - 5)}M';
    }
  }

  static String formatByte(int value) {
    if (value < 1000000) {
      return '${value ~/ 1000} KB';
    } else if (value < 1000000000) {
      return '${value ~/ 1000000}.${int.parse(value.toString().substring(1)) ~/ 10000} MB';
    }
    return '';
  }
}
