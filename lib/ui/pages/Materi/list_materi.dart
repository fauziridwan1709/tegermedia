part of '_materi.dart';

class ListVideo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final materiState = Injector.getAsReactive<MateriState>();
    var kodeApi = JenjangData.getApiKodePelajaran(materiState.state.materi.jenjang);
    return FutureBuilder<MateriVideoModel>(
      future: MateriServices.getVideoData(materiState.state.materi.kelas,
          'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=$kodeApi&columns=false'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.rows
              .where((element) => AdditionalMateriSettings.filter(element))
              .toList()
              .isEmpty) {
            ///showing widget noData
            return AdditionalMateriSettings.noData(context);
          }
          return Container(
            padding: EdgeInsets.symmetric(horizontal: S.w * .03, vertical: S.w * .05),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius:
                  BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: snapshot.data.rows
                  .where((element) => AdditionalMateriSettings.filter(element))
                  .map((e) => VideoChildren(item: e, search: false))
                  .toList()
                  .sublist(0, AdditionalMateriSettings.length(snapshot)),
            ),
          );
        } else {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Padding(
              padding: EdgeInsets.only(top: S.w / 3),
              child: Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green))),
            );
          } else {
            ///showing widget noData
            return AdditionalMateriSettings.noData(context);
          }
        }
      },
    );
  }
}

class ListPresentasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final materiState = GlobalState.materi();
    // var data = Provider.of<MateriProvider>(context);
    var kodeApi = JenjangData.getApiKodePres(
        materiState.state.materi.jenjang, materiState.state.materi.kelas);
    print(kodeApi);
    return FutureBuilder<MateriPresModel>(
      future: MateriServices.getPresData(materiState.state.materi.kelas,
          'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=$kodeApi&columns=false'),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Padding(
              padding: EdgeInsets.only(top: S.w / 3),
              child: Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green))));
        } else if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data.rows
              .where((element) => AdditionalMateriSettings.filter(element))
              .toList()
              .isEmpty) {
            ///showing widget noData
            return AdditionalMateriSettings.noData(context);
          } else {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
              color: Colors.white,
              child: Column(
                children: snapshot.data.rows
                    .where((element) => AdditionalMateriSettings.filter(element))
                    .map((e) => PresentasiChildren(
                          item: e,
                          search: false,
                        ))
                    .toList()
                    .sublist(0, AdditionalMateriSettings.length(snapshot)),
              ),
            );
          }
        }
        return SizedBox();
      },
    );
  }
}

class ListEbook extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var data = Provider.of<MateriProvider>(context);
    final materiState = GlobalState.materi();
    var kodeApi =
        JenjangData.getApiKodeDoc(materiState.state.materi.jenjang, materiState.state.materi.kelas);
    return FutureBuilder<MateriDocModel>(
      future: MateriServices.getDocData(materiState.state.materi.kelas,
          'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=$kodeApi&columns=false'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          print(snapshot.data.rows);
          //data.setResources(bookList: snapshot.data.rows);
          if (snapshot.data.rows
              .where((element) => AdditionalMateriSettings.filter(element))
              .toList()
              .isEmpty) {
            ///showing widget noData
            return AdditionalMateriSettings.noData(context);
          }
          return Container(
            color: Colors.white,
            padding: EdgeInsets.only(left: S.w * .05, right: S.w * .05, top: S.w * .05),
            child: Column(
              children: snapshot.data.rows
                  .where((element) => AdditionalMateriSettings.filter(element))
                  .map((e) => EbookChildren(
                        item: e,
                        search: false,
                      ))
                  .toList()
                  .sublist(0, AdditionalMateriSettings.length(snapshot)),
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Padding(
              padding: EdgeInsets.only(top: S.w / 3),
              child: Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green))));
        } else {
          ///showing widget noData
          return AdditionalMateriSettings.noData(context);
        }
      },
    );
  }
}

class ListQuiz extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // var data = Provider.of<MateriProvider>(context);
    final materiState = GlobalState.materi();
    return FutureBuilder<MateriQuizModel>(
        future: MateriQuizServices.getData(value: null),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Padding(
                padding: EdgeInsets.only(top: S.w / 3),
                child: Center(
                    child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green))));
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data.rows
                .where((element) =>
                    element.kelas ==
                        (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                            ? materiState.state.materi.kelas
                            : materiState.state.materi.jenjang == 'sma'
                                ? materiState.state.materi.kelas
                                    .substring(materiState.state.materi.kelas.indexOf(' ') + 1)
                                : materiState.state.materi.kelas
                                    .substring(materiState.state.materi.kelas.indexOf(' ') + 1)) &&
                    (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                        ? true
                        : element.matapelajaran == materiState.state.materi.pelajaran))
                .isEmpty) {
              ///showing widget noData
              return AdditionalMateriSettings.noData(context);
            } else {
              return Container(
                color: Colors.white,
                child: Column(
                  children: snapshot.data.rows
                      .where((element) =>
                          element.kelas ==
                              (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                                  ? materiState.state.materi.kelas
                                  : materiState.state.materi.jenjang == 'sma'
                                      ? materiState.state.materi.kelas.substring(
                                          materiState.state.materi.kelas.indexOf(' ') + 1)
                                      : materiState.state.materi.kelas.substring(
                                          materiState.state.materi.kelas.indexOf(' ') + 1)) &&
                          (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                              ? true
                              : element.matapelajaran == materiState.state.materi.pelajaran))
                      .map((e) => QuizChildren(item: e))
                      .toList(),
                ),
              );
            }
          }
          return SizedBox();
        });
  }
}

class ListApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final materiState = GlobalState.materi();
    ;
    return FutureBuilder<StudentRecModel>(
        future: StudentRecServices.getData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Padding(
                padding: EdgeInsets.only(top: S.w / 3),
                child: Center(
                    child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green))));
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              if (snapshot.data.rows
                  .where((element) =>
                      element.kelas ==
                          (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                              ? 'Umum'
                              : materiState.state.materi.jenjang.toUpperCase()) &&
                      element.pelajaran ==
                          (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                              ? materiState.state.materi.kelas
                              : materiState.state.materi.pelajaran))
                  .isEmpty) {
                ///showing widget noData
                return AdditionalMateriSettings.noData(context);
              }
              return Container(
                color: Colors.white,
                child: Column(
                  children: snapshot.data.rows
                      .where((element) =>
                          element.kelas ==
                              (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                                  ? 'Umum'
                                  : materiState.state.materi.jenjang.toUpperCase()) &&
                          element.pelajaran ==
                              (TipePelajaran.umum.contains(materiState.state.materi.kelas)
                                  ? materiState.state.materi.kelas
                                  : materiState.state.materi.pelajaran))
                      .map((e) => Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: S.w * .03, vertical: S.w * .025),
                            child: InkWell(
                              onTap: () => LaunchServices.launchInBrowser(e.link),
                              child: Container(
                                height: S.w * .23,
                                padding: EdgeInsets.symmetric(
                                    horizontal: S.w * .04, vertical: S.w * .02),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  color: Colors.white,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: S.w * .2,
                                      height: S.w * .2,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(12),
                                          image: DecorationImage(image: NetworkImage(e.icon))),
                                    ),
                                    SizedBox(width: 15),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Flexible(
                                            child: RichText(
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              strutStyle: StrutStyle(fontSize: S.w / 26),
                                              text: TextSpan(
                                                text: e.namaaplikasi,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: S.w / 28,
                                                  color: SiswamediaTheme.green,
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 10),
                                          Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: S.w * .015, vertical: S.w * .007),
                                              decoration: BoxDecoration(
                                                color: SiswamediaTheme.green,
                                                borderRadius: BorderRadius.circular(6),
                                              ),
                                              child: Text(
                                                e.kategori,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: S.w / 34,
                                                  color: Colors.white,
                                                ),
                                              )),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ))
                      .toList(),
                ),
              );
            }
            return SizedBox();
          }
          return SizedBox();
        });
  }
}

class AdditionalMateriSettings {
  static final materiState = Injector.getAsReactive<MateriState>();
  static Widget noData(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: S.w / 4),
      child: Center(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/update.png',
            height: S.w / 2.4,
            width: S.w / 2.4,
          ),
          Text('Tunggu, akan segera kita tambahkan',
              style: TextStyle(color: SiswamediaTheme.black, fontSize: S.w / 30))
        ],
      )),
    );
  }

  static int length(dynamic snapshot) {
    return snapshot.data.rows
                .where((dynamic element) => AdditionalMateriSettings.filter(element))
                .length >
            20
        ? 20
        : snapshot.data.rows
            .where((dynamic element) => AdditionalMateriSettings.filter(element))
            .length;
  }

  static bool filter(dynamic element) {
    var data = materiState.state;
    return (data.materi.jenjang == 'sma'
            ? data.materi.kelas.contains(element.kelas)
            : false ||
                element.kelas ==
                    (data.materi.jenjang == 'sma'
                        ? data.materi.kelas.substring(data.materi.kelas.indexOf(' ') + 1)
                        : data.materi.kelas.substring(data.materi.kelas.indexOf(' ') + 1))) &&
        (element.matapelajaran == data.materi.pelajaran) &&
        (element.kurikulum.toString() == data.materi.kurikulum.toString());
  }

  static int lengthUmum(dynamic snapshot, String topic, String subTopic) {
    return snapshot.data.rows
                .where((dynamic element) =>
                    AdditionalMateriSettings.filterUmum(topic, subTopic, element))
                .length >
            20
        ? 20
        : snapshot.data.rows
            .where(
                (dynamic element) => AdditionalMateriSettings.filterUmum(topic, subTopic, element))
            .length;
  }

  static bool filterUmum(String topic, String subTopic, dynamic element) {
    return element.kelas == topic && element.matapelajaran == subTopic;
  }
}
