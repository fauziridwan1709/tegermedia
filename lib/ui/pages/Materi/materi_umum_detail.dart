part of '_materi.dart';

class MateriUmumDetail extends StatefulWidget {
  MateriUmumDetail({this.topic});
  final String topic;

  @override
  _MateriUmumDetailState createState() => _MateriUmumDetailState();
}

class _MateriUmumDetailState extends State<MateriUmumDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          shadowColor: Colors.grey.shade50,
          brightness: Brightness.light,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: SiswamediaTheme.green,
              ),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Pilih SubTopik',
              style:
                  semiBlack.copyWith(color: Colors.black, fontSize: S.w / 22)),
        ),
        body: ListView(
          children: subTopicList[topicList.indexOf(widget.topic)]
              .map((name) => Container(
                    //padding: EdgeInsets.symmetric(horizontal: S.w * .05),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                            bottom: BorderSide(color: Colors.grey[200]),
                            top: BorderSide(color: Colors.grey[200]))),
                    child: Material(
                      color: Colors.transparent,
                      child: ListTile(
                        // leading: Icon(Icons.library_books_rounded,
                        //     color: SiswamediaTheme.green),
                        title: Text(
                          name,
                          style: descBlack.copyWith(fontSize: S.w / 28),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                builder: (_) => MateriUmumList(
                                  topic: widget.topic,
                                  subTopic: name,
                                ),
                              ));
                        },
                        trailing: Icon(Icons.arrow_forward_ios_rounded),
                      ),
                    ),
                  ))
              .toList(),
        ));
  }
}
