part of '_base.dart';

abstract class BaseStateReBuilder<T extends StatefulWidget,
    K extends FutureState<K, dynamic>> extends State<T> with Diagnosticable {
  @protected
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  @protected
  @mustCallSuper
  @override
  void initState() {
    super.initState();
    Initializer(
      reactiveModel: Injector.getAsReactive<K>(),
      rIndicator: refreshIndicatorKey,
      state: Injector.getAsReactive<K>().state.getCondition(),
      cacheKey: Injector.getAsReactive<K>().state.cacheKey,
    ).initialize();
  }

  @protected
  @mustCallSuper
  Future<void> retrieveData();
}
