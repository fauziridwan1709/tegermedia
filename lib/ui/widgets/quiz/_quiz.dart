import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/provider/_provider.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/pages/HomePage/_home.dart';
import 'package:tegarmedia/ui/pages/Ujian/_ujian.dart';

import '../_widgets.dart';

part 'card_quiz.dart';
