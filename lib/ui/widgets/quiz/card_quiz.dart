part of '_quiz.dart';

class CardQuiz extends StatelessWidget {
  final Function(BuildContext context, QuizListDetail data) onDelete;
  final Function(int id) onTap;
  final QuizListDetail data;
  final ReactiveModel<AuthState> authState;
  final QuizProviderKelas penilaianProvider;

  CardQuiz({this.onTap, this.onDelete, this.data, this.authState, this.penilaianProvider});

  @override
  Widget build(BuildContext context) {
    var penilaianProvider = Provider.of<QuizProviderKelas>(context);
    var auth = authState.state;
    print('mantap');
    print(auth.currentState.classRole);
    return Container(
      margin: EdgeInsets.symmetric(vertical: S.w * .025),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              offset: Offset(2, 2),
              spreadRadius: 2,
              color: Colors.grey.withOpacity(.2),
              blurRadius: 2)
        ],
        borderRadius: BorderRadius.circular(12),
      ),
      child: InkWell(
        onTap: () async {
          /*penilaianProvider.getPenilaian(Penilaian(
                                        courseId: e.courseId,
                                        type: e.quizType,
                                      ));*/
          if (auth.currentState.classRole == '') {
            return CustomFlushBar.errorFlushBar('Pilih Kelas terlebih dahulu', context);
          }
          if ((auth.currentState.classRole == 'GURU' ||
                  auth.currentState.classRole == 'WALIKELAS') &&
              auth.currentState.classId == data.classId) {
            await penilaianProvider.setDetail(QuizListDetail(
                courseId: data.courseId,
                classId: data.classId,
                className: data.className,
                courseName: data.courseName,
                startDate: data.startDate,
                schoolId: data.schoolId,
                schoolName: data.schoolName,
                name: data.name,
                totalEssay: data.totalEssay,
                totalPg: data.totalPg,
                totalTime: data.totalTime,
                quizType: data.quizType,
                id: data.id,
                hasBeenDone: data.hasBeenDone,
                totalNilai: data.totalNilai,
                isHidden: data.isHidden,
                isRandom: data.isRandom));
            return navigate(context, QuizAssessment(id: data.id));
          }
          if (DateTime.now().difference(DateTime.parse(data.startDate)) > Duration(seconds: 0)) {
            await penilaianProvider.setDetail(QuizListDetail(
                courseId: data.courseId,
                classId: data.classId,
                className: data.className,
                courseName: data.courseName,
                startDate: data.startDate,
                schoolId: data.schoolId,
                schoolName: data.schoolName,
                name: data.name,
                totalEssay: data.totalEssay,
                totalPg: data.totalPg,
                totalTime: data.totalTime,
                quizType: data.quizType,
                id: data.id,
                hasBeenDone: data.hasBeenDone,
                totalNilai: data.totalNilai,
                isHidden: data.isHidden,
                isRandom: data.isRandom));
            await Navigator.push<void>(context, MaterialPageRoute(builder: (_) => QuizInfoKelas()));
          } else {
            CustomFlushBar.errorFlushBar('Ujian belum dimulai!', context);
          }
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: S.w * .04, vertical: S.w * .04),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: S.w * .6,
                        child: Text('${data.courseName}',
                            overflow: TextOverflow.ellipsis,
                            style: boldBlack.copyWith(fontSize: S.w / 26)),
                      ),
                    ],
                  ),
                  if (GlobalState.auth().state.currentState.classRole.isGuruOrWaliKelas)
                    InkWell(
                      onTap: () => onDelete(context, data),
                      child: Icon(Icons.more_vert),
                    ),
                ],
              ),
              Text(data.name.length > 22 ? (data.name).substring(23) : data.name,
                  style: TextStyle(
                      fontWeight: FontWeight.w400, color: Colors.black54, fontSize: S.w / 32)),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ///show xp and gold
                  // SizedBox(
                  //     width: width / 20,
                  //     child: Imagdata.asset(
                  //         'assets/icon xp.png')),
                  // Text('100 xp',
                  //     style: TextStyle(
                  //         fontWeight:
                  //             FontWeight.w600,
                  //         color: Colors.black87,
                  //         fontSize: width / 34)),
                  // SizedBox(
                  //     width: width / 20,
                  //     child: Image.asset(
                  //         'assets/icon gold.png')),
                  // Text('100 Gold',
                  //     style: TextStyle(
                  //         fontWeight:
                  //             FontWeight.w600,
                  //         color: Colors.black87,
                  //         fontSize: width / 34)),

                  Row(
                    children: [
                      Icon(
                        Icons.access_time,
                        color: SiswamediaTheme.green,
                      ),
                      SizedBox(width: 15),
                      Text('${data.totalTime} mins',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.black87,
                              fontSize: S.w / 34)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      if (data.hasBeenDone)
                        Center(child: Icon(Icons.check, color: SiswamediaTheme.green)),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: S.w * .02, vertical: S.w * .01),
                    decoration: BoxDecoration(
                      color: Color(0xff4B4B4B),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Center(
                      child: Text(
                        '${(data.totalPg > 0 && data.totalEssay > 0) ? data.totalPg + data.totalEssay : data.totalPg == -1 ? data.totalEssay : data.totalPg} soal',
                        style: TextStyle(color: Colors.white, fontSize: S.w / 36),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                          '${(data.totalPg > 0 && data.totalEssay > 0) ? 'PG & ESSAY' : data.totalPg == -1 ? 'ESSAY' : 'PG'}',
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Colors.black87,
                              fontSize: S.w / 34)),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                    decoration: BoxDecoration(
                      color: SiswamediaTheme.green,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Text(
                      'mulai',
                      style: TextStyle(color: Colors.white, fontSize: S.w / 34),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10),
              Divider(color: Colors.grey.withOpacity(.4)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (auth.currentState.schoolRole.contains('SISWA'))
                    Text(
                        'Nilai : ${data.hasBeenDone ? data.totalNilai.isNegative ? 'Bellum Dinilai' : data.isHidden ? 'Not Permitted' : formatNilai(data.totalNilai) : '-'}',
                        style: TextStyle(fontSize: S.w / 30, fontWeight: FontWeight.w600)),
                  if (!data.name.contains('Kuis Latihan Berhitung'))
                    Text(
                      '${DateTime.parse(data.startDate).toLocal().hour < 10 ? '0' : ''}${DateTime.parse(data.startDate).toLocal().hour}:${DateTime.parse(data.startDate).toLocal().minute < 10 ? '0' : ''}${DateTime.parse(data.startDate).toLocal().minute} - ${DateTime.parse(data.startDate).toLocal().day} ${QuizData.bulan[DateTime.parse(data.startDate).toLocal().month - 1]} ${DateTime.parse(data.startDate).toLocal().year}',
                      style: TextStyle(fontSize: S.w / 30, fontWeight: FontWeight.w600),
                    )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CardQuizSekolah extends StatelessWidget {
  final Function(BuildContext context, QuizListDetail data) onDelete;
  final Function(int id) onTap;
  final QuizListDetail data;
  final ReactiveModel<AuthState> authState;
  final QuizProviderKelas penilaianProvider;

  CardQuizSekolah({this.onTap, this.onDelete, this.data, this.authState, this.penilaianProvider});

  @override
  Widget build(BuildContext context) {
    var penilaianProvider = Provider.of<QuizProviderKelas>(context);
    var startDate = DateTime.parse(data.startDate);
    return Container(
      margin: EdgeInsets.symmetric(vertical: S.w * .025),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              offset: Offset(2, 2),
              spreadRadius: 2,
              color: Colors.grey.withOpacity(.2),
              blurRadius: 2)
        ],
        borderRadius: BorderRadius.circular(12),
      ),
      child: InkWell(
        onTap: () async {
          print('url ' + data.description);
          await context.push<void>(WebViewHome(
            url: data.description,
            title: 'Quiz',
          ));
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: S.w * .04, vertical: S.w * .04),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: S.w * .6,
                        child: Text('${data.courseName}',
                            overflow: TextOverflow.ellipsis,
                            style: boldBlack.copyWith(fontSize: S.w / 26)),
                      ),
                    ],
                  ),
                ],
              ),
              Text(data.name.length > 22 ? (data.name).substring(23) : data.name,
                  style: TextStyle(
                      fontWeight: FontWeight.w400, color: Colors.black54, fontSize: S.w / 32)),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.access_time,
                        color: SiswamediaTheme.green,
                      ),
                      SizedBox(width: 15),
                      Text('${data.totalTime} mins',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.black87,
                              fontSize: S.w / 34)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      if (data.hasBeenDone)
                        Center(child: Icon(Icons.check, color: SiswamediaTheme.green)),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: S.w * .02, vertical: S.w * .01),
                    decoration: BoxDecoration(
                      color: Color(0xff4B4B4B),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Center(
                      child: Text(
                        '${data.totalPg} soal',
                        style: TextStyle(color: Colors.white, fontSize: S.w / 36),
                      ),
                    ),
                  ),
                  if (data.status == 0)
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                      decoration: BoxDecoration(
                        color: SiswamediaTheme.green,
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Text(
                        'mulai',
                        style: TextStyle(color: Colors.white, fontSize: S.w / 34),
                      ),
                    )
                  else
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                      decoration: BoxDecoration(
                        color: SiswamediaTheme.transparent,
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Text(
                        'mulai',
                        style: TextStyle(color: Colors.transparent, fontSize: S.w / 34),
                      ),
                    )
                ],
              ),
              SizedBox(height: 10),
              Divider(color: Colors.grey.withOpacity(.4)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (GlobalState.auth().state.currentState.classRole.isSiswa)
                    Text('Nilai : ${data.status == 1 ? data.totalNilai : 'Belum ada Nilai'}',
                        style: TextStyle(fontSize: S.w / 30, fontWeight: FontWeight.w600)),
                  if (!data.name.contains('Kuis Latihan Berhitung'))
                    Text(
                      '${DateFormat('HH:mm - dd MMM yyy').format(startDate)}',
                      style: TextStyle(fontSize: S.w / 30, fontWeight: FontWeight.w600),
                    )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
