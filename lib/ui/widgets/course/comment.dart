part of '../_widgets.dart';

Widget comment() {
  return Stack(
    children: [
      Container(
        margin: EdgeInsets.only(bottom: 80),
        child: ListView.builder(
            itemCount: 10,
            shrinkWrap: true,
            itemBuilder: (c, i) => Container(
                  margin: EdgeInsets.only(top: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 9,
                      ),
                      photoProfile(),
                      SizedBox(
                        width: 9,
                      ),
                      Expanded(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                            Container(
                              width: double.infinity,
                              margin: EdgeInsets.only(right: 40),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: SiswamediaTheme.textGrey[300]),
                              padding: EdgeInsets.symmetric(
                                  vertical: 3, horizontal: 8),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  vText('Jajang',
                                      fontSize: 14,
                                      color: SiswamediaTheme.textGrey[500],
                                      fontWeight: FontWeight.w500),
                                  SizedBox(height: 1),
                                  vText(
                                      'Suatu kata yang memiliki bentuk yang berbeda bentuk yang berbeda Suatu kata yang memiliki bentuk yang berbeda bentuk yang berbeda Suatu kata yang memiliki bentuk yang berbeda bentuk yang berbeda Suatu kata yang memiliki bentuk yang berbeda bentuk yang berbeda Suatu kata yang memiliki bentuk yang berbeda bentuk yang berbeda Suatu kata yang memiliki bentuk yang berbeda bentuk yang berbeda Suatu kata yang memiliki bentuk yang berbeda bentuk yang berbeda',
                                      fontSize: 14,
                                      color: SiswamediaTheme.textGrey[500],
                                      maxLines: 6,
                                      overflow: TextOverflow.ellipsis)
                                ],
                              ),
                            ),
                            SizedBox(height: 5),
                            vText('Hari ini 10.02',
                                color: SiswamediaTheme.textGrey[400],
                                fontSize: 14)
                          ]))
                    ],
                  ),
                )),
      ),
      Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 80,
            width: double.infinity,
            alignment: Alignment.bottomCenter,
            color: Colors.white,
            padding: EdgeInsets.all(16),
            child: Row(
              children: [
                photoProfile(),
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: SiswamediaTheme.textGrey[300]),
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: TextFormField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Tulis Komentar Disini',
                            focusedBorder: InputBorder.none),
                      )),
                ),
                SizedBox(
                  width: 5,
                ),
                Icon(
                  Icons.photo_camera_outlined,
                  color: SiswamediaTheme.primary,
                ),
                SizedBox(
                  width: 5,
                ),
                Icon(
                  Icons.attach_file_outlined,
                  color: SiswamediaTheme.primary,
                ),
                SizedBox(
                  width: 5,
                ),
                Icon(
                  Icons.send,
                  color: SiswamediaTheme.primary,
                ),
              ],
            ),
          ))
    ],
  );
}
