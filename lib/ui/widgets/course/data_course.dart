part of '../_widgets.dart';

Widget dataCourse({String statusDate}) {
  return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 2,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
          color: Colors.white),
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          vText('Penalaran Kritis',
              color: SiswamediaTheme.textGrey[200],
              fontSize: 18,
              fontWeight: FontWeight.w500),
          Row(children: [
            IconButton(
              icon: Icon(Icons.chevron_left),
              onPressed: () {},
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                    decoration: BoxDecoration(
                        color: statusDate == 'Incoming'
                            ? SiswamediaTheme.textGrey[400]
                            : SiswamediaTheme.primary,
                        borderRadius: BorderRadius.circular(4)),
                    margin: EdgeInsets.only(right: 20),
                    child: vText(
                      statusDate,
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(child: vText('Mata Pelajaran')),
                      Expanded(
                          child: Row(
                        children: [
                          vText(':  '),
                          Expanded(
                            child: vText('Karakter',
                                overflow: TextOverflow.ellipsis,
                                color: SiswamediaTheme.textGrey[500],
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Expanded(child: vText('Guru')),
                      Expanded(
                          child: Row(
                        children: [
                          vText(':  '),
                          vText('Budi Susilo',
                              color: SiswamediaTheme.textGrey[500],
                              fontWeight: FontWeight.w500)
                        ],
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Expanded(child: vText('Kelas ')),
                      Expanded(
                          child: Row(
                        children: [
                          vText(':  '),
                          vText('Kelas 6A ',
                              color: SiswamediaTheme.textGrey[500],
                              fontWeight: FontWeight.w500)
                        ],
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Expanded(child: vText('Tanggal')),
                      Expanded(
                          child: Row(
                        children: [
                          vText(':  '),
                          vText('7 Oktober 2020',
                              color: SiswamediaTheme.textGrey[500],
                              fontWeight: FontWeight.w500)
                        ],
                      ))
                    ],
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(Icons.chevron_right),
              onPressed: () {},
            ),
          ]),
        ],
      ));
}
