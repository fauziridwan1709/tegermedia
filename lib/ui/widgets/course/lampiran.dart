part of '../_widgets.dart';

Widget lampiran() {
  return Column(
    children: [
      SizedBox(
        height: 8,
      ),
      Container(
        child: Row(
          children: [
            SizedBox(
              width: 8,
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: SiswamediaTheme.textGrey[100])),
              padding: EdgeInsets.all(12),
              child: Icon(Icons.camera_alt_outlined,
                  color: SiswamediaTheme.primary),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  vText('Meeting URL :', color: SiswamediaTheme.textGrey[500]),
                  vText('Googlemeet.com/room1',
                      color: SiswamediaTheme.textBlue, fontSize: 17),
                ],
              ),
            )
          ],
        ),
      ),
      SizedBox(
        height: 8,
      ),
      Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 8,
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: SiswamediaTheme.textGrey[100])),
              padding: EdgeInsets.all(12),
              child: Icon(Icons.attach_file_outlined,
                  color: SiswamediaTheme.primary),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  vText('View Link :', color: SiswamediaTheme.textGrey[500]),
                  vText('Youtube.com/course1',
                      color: SiswamediaTheme.textBlue, fontSize: 17),
                ],
              ),
            )
          ],
        ),
      ),
    ],
  );
}
