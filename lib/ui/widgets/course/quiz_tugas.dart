part of '../_widgets.dart';

Widget quiz() {
  return ListView.builder(
    itemBuilder: (c, i) => itemQuiz(),
    itemCount: 10,
    shrinkWrap: true,
  );
}

Widget itemQuiz() {
  return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      margin: EdgeInsets.only(right: 16, left: 16, top: 16),
      padding: EdgeInsets.all(8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              vText('Bahasa Indonesia',
                  color: SiswamediaTheme.nearBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
              SizedBox(width: 15),
              Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: SiswamediaTheme.textBlue,
                ),
                child: vText('Segera', color: Colors.white),
              ),
            ],
          ),
          SizedBox(
            height: 6,
          ),
          vText('Budi Susilo', color: SiswamediaTheme.nearBlack),
          SizedBox(
            height: 6,
          ),
          vText('SINONIM KATA', color: SiswamediaTheme.nearBlack),
          SizedBox(
            height: 6,
          ),
          Divider(
            height: 1,
            color: SiswamediaTheme.textGrey[200],
          ),
          SizedBox(
            height: 6,
          ),
          Row(
            children: [
              Expanded(
                child: vText('07/01/2020 - 13.00',
                    color: SiswamediaTheme.nearBlack),
              ),
              Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: SiswamediaTheme.primary,
                ),
                child: vText('Segera', color: Colors.white),
              ),
            ],
          )
        ],
      ));
}
