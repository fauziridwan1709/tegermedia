import 'package:flutter/material.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/utils/v_widget.dart';

class ClassicDropdown<T> extends StatelessWidget {
  final T valueSelected;
  final List<T> itemsDropdown;
  final ValueChanged<T> onChanged;
  final String Function(T item) titleValue;
  final String hint;

  ClassicDropdown(
      {@required this.valueSelected,
      @required this.itemsDropdown,
      @required this.titleValue,
      this.onChanged,
      this.hint});

  @override
  Widget build(BuildContext context) {
    return DropdownButton<T>(
      iconSize: 24,
      value: valueSelected,
      isExpanded: false,
      hint: Text(hint, style: TextStyle(color: Colors.black)),
      icon: Icon(
        Icons.keyboard_arrow_down,
        size: 24,
        color: SiswamediaTheme.primary,
      ),
      style: TextStyle(color: Colors.black, fontFamily: "latoNormal", fontSize: 16),
      underline: Container(
        height: 0,
        color: Colors.grey,
      ),
      onChanged: onChanged,
      items: itemsDropdown.map<DropdownMenuItem<T>>((T value) {
        return DropdownMenuItem<T>(
          value: value,
          child: vText(titleValue(value), fontSize: 14, overflow: TextOverflow.ellipsis),
        );
      }).toList(),
    );
  }
}
