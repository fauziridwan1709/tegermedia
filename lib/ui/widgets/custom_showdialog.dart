part of '_widgets.dart';

class CustomShowDialog {
  final String imageAsset;
  final String description;

  const CustomShowDialog({this.imageAsset, this.description});

  void showTheDialog(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
                width: S.w * .9,
                padding: EdgeInsets.symmetric(
                    vertical: S.w * .05, horizontal: S.w * .05),
                child: Column(
                  ///biar responsive dialognya [MainAxisSize.min]
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(imageAsset, height: S.w / 3, width: S.w / 3),
                    Text(description,
                        textAlign: TextAlign.center,
                        style: semiBlack.copyWith(fontSize: S.w / 26)),
                    SizedBox(height: S.w * .05),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: S.w * .15),
                        child: SpinKitSquareCircle(
                          color: Colors.black54,
                          size: 30,
                        )),
                  ],
                ))));
  }
}
