part of '_forum.dart';

class AttachmentImage extends StatelessWidget {
  final String link;
  // final Function(String link) onTap;

  AttachmentImage({@required this.link})
      : assert(link != null, 'link cannot be null');
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .03),
      child: Stack(
        children: [
          Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[200],
            enabled: true,
            child: Container(
              height: S.w * .4,
              width: S.w * .9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.grey[300]),
            ),
          ),
          InkWell(
            onTap: () => navigate(
                context,
                ImagePreview(
                    tag: 'preview',
                    link: 'https://drive.google.com/uc?export=view&id=$link')),
            child: Hero(
              tag: 'preview',
              child: Container(
                height: S.w * .4,
                width: S.w * .9,
                // decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(12),
                //     color: Colors.grey[300]),
                foregroundDecoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                          'https://drive.google.com/uc?export=view&id=$link',
                          scale: .1),
                    )),
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.more_vert),
            color: Colors.white,
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
