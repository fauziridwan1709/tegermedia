part of '_forum.dart';

class DeleteIconButton extends StatelessWidget {
  final VoidCallback onTap;
  DeleteIconButton({this.onTap});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      padding: EdgeInsets.all(0),
      icon: Icon(
        Icons.delete_outline,
        color: Colors.white,
      ),
      onPressed: onTap,
      color: Colors.black,
    );
  }
}
