part of '_forum.dart';

class CommentList extends StatelessWidget {
  final Function(Comments) onLongPress;
  final Function(Comments) onTap;
  final VoidCallback onTapReply;
  final VoidCallback onTapSubComment;
  final ForumDetailItem data;
  final Comments comments;
  final bool isSelected;
  final bool status;
  final Map<int, int> commentModelEdit;

  CommentList({
    this.onLongPress,
    this.onTap,
    this.onTapReply,
    this.data,
    this.comments,
    this.isSelected,
    this.onTapSubComment,
    this.status,
    this.commentModelEdit,
  });

  @override
  Widget build(BuildContext context) {
    var start = DateTime.now();
    return LayoutBuilder(
      builder: (_, __) {
        var date = DateTime.parse(comments.commentDate).toLocal();
        var subComment = data.comments
            .where((element) => comments.iD == element.parentCommentId);
        var count = subComment.length;
        return Column(
          children: [
            ListTile(
              selected: commentModelEdit.containsKey(comments.iD),
              selectedTileColor: Colors.grey[300],
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.network(
                  comments.image,
                  height: 40,
                  width: 40,
                  fit: BoxFit.cover,
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                    margin: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Color(0xfff8f8f8),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(comments.fullName,
                            style: secondHeadingBlack.copyWith(
                                fontSize: S.w / 28)),
                        if (comments.comments.contains('http'))
                          HeroImage(
                            tag: 'preview-comment',
                            link: comments.comments,
                            onTap: () => navigate(
                                context,
                                ImagePreview(
                                    tag: 'preview-comment',
                                    link: comments.comments)),
                          )
                        else
                          Text(comments.comments,
                              style: descBlack.copyWith(
                                  color: Colors.black87, fontSize: S.w / 32)),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10, left: 10),
                        child: Text(
                            "${DateTime.now().difference(date).inDays == 0 && DateTime.now().day - date.day == 0 ? "Hari ini" : start.yearMonthDay.difference(date.yearMonthDay).inDays < 2 ? "Kemarin" : start.difference(date).inDays.toString() + " Hari yang lalu"} ${date.hour < 10 ? "0" : ""}${date.hour}.${date.minute < 10 ? "0" : ""}${date.minute}",
                            style: descBlack.copyWith(
                                fontSize: S.w / 34, color: Colors.black38)),
                      ),
                      InkWell(
                        onTap: onTapReply,
                        child: Text('Balas',
                            style: secondHeadingBlack.copyWith(
                                fontSize: S.w / 34)),
                      )
                    ],
                  ),
                  if (subComment.isNotEmpty && status == null)
                    SizedBox(height: 5),
                  if (subComment.isNotEmpty && status == null)
                    InkWell(
                        onTap: onTapSubComment,
                        child: Material(
                          color: Colors.transparent,
                          child: Row(
                            children: [
                              Container(
                                  width: S.w * .15,
                                  child: Divider(
                                      color: Colors.grey.withOpacity(.5))),
                              SizedBox(width: 10),
                              CustomText(
                                  'Lihat $count balasan', Kind.descBlack32)
                            ],
                          ),
                        )),
                ],
              ),
              onTap: () => onTap(comments),
              onLongPress: () => onLongPress(comments),
            ),
            if (subComment.isNotEmpty && status == true)
              Column(
                children: subComment.map((e) {
                  var subDate = DateTime.parse(e.commentDate).toLocal();
                  return ListTile(
                    selected: commentModelEdit.containsKey(e.iD),
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Image.network(e.image,
                          height: 35, width: 35, fit: BoxFit.cover),
                    ),
                    selectedTileColor: Colors.grey[300],
                    title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              color: Color(0xfff8f8f8),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(e.fullName,
                                    style: secondHeadingBlack.copyWith(
                                        fontSize: S.w / 28)),
                                if (e.comments.contains('http'))
                                  HeroImage(
                                    tag: 'preview-sub-comment',
                                    link: e.comments,
                                    onTap: () => navigate(
                                        context,
                                        ImagePreview(
                                            tag: 'preview-sub-comment',
                                            link: e.comments)),
                                  )
                                else
                                  Text(e.comments,
                                      style: descBlack.copyWith(
                                          color: Colors.black87,
                                          fontSize: S.w / 32)),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 10, left: 10),
                                child: Text(
                                    "${DateTime.now().difference(subDate).inDays == 0 && DateTime.now().day - subDate.day == 0 ? "Hari ini" : start.yearMonthDay.difference(subDate.yearMonthDay).inDays < 2 ? "Kemarin" : start.difference(subDate).inDays.toString() + " Hari yang lalu"} ${subDate.hour < 10 ? "0" : ""}${subDate.hour}.${subDate.minute < 10 ? "0" : ""}${subDate.minute}",
                                    style: descBlack.copyWith(
                                        fontSize: S.w / 34,
                                        color: Colors.black38)),
                              ),
                            ],
                          ),
                        ]),
                    onTap: () => onTap(e),
                    onLongPress: () => onLongPress(e),
                    contentPadding: EdgeInsets.only(left: 40.0),
                  );
                }).toList(),
              )
          ],
        );
      },
    );
  }
}
