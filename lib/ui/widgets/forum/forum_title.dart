part of '_forum.dart';

class ForumTitle extends StatelessWidget {
  final String title;
  ForumTitle({this.title});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .03),
      child: CustomText(
        title,
        Kind.heading3,
        align: TextAlign.left,
      ),
    );
  }
}
