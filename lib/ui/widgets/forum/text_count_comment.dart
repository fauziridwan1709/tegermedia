part of '_forum.dart';

class TextCountComment extends StatelessWidget {
  final List<Comments> comments;
  final int initial;
  TextCountComment({this.comments, this.initial});

  @override
  Widget build(BuildContext context) {
    return Text(
        '${(comments != null && comments.isNotEmpty) ? comments.length : initial} Komentar');
  }
}
