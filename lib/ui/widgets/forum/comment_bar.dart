part of '_forum.dart';

class CommentBar extends StatelessWidget {
  final TextEditingController commentController;
  final FocusNode fNodeComment;
  final VoidCallback onTap;
  final bool isForum;
  final double value;
  final VoidCallback upload;
  final bool isReply;
  final String replyName;
  final VoidCallback onTapClear;

  CommentBar(
      {this.isForum = true,
      this.value = 0.0,
      this.upload,
      this.isReply,
      this.replyName,
      this.onTapClear,
      @required this.commentController,
      @required this.fNodeComment,
      @required this.onTap})
      : assert(commentController != null),
        assert(fNodeComment != null),
        assert(onTap != null);

  @override
  Widget build(BuildContext context) {
    var profile = Injector.getAsReactive<ProfileState>().state.profile;
    return Positioned(
      bottom: 0,
      child: Container(
        width: S.w,
        child: ListView(
          shrinkWrap: true,
          children: [
            if (isReply)
              CustomContainer(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                boxShadow: [SiswamediaTheme.shadowContainer],
                color: SiswamediaTheme.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SimpleText('Reply to ', style: SiswamediaTextStyle.title, fontSize: 14),
                        SimpleText('$replyName',
                            style: SiswamediaTextStyle.description, fontSize: 14),
                      ],
                    ),
                    IconButton(
                      icon: Icon(Icons.clear, color: SiswamediaTheme.green),
                      onPressed: onTapClear,
                    )
                  ],
                ),
              ),
            LinearPercentIndicator(
              percent: value,
              padding: EdgeInsets.all(0),
              backgroundColor: Colors.transparent,
              progressColor: SiswamediaTheme.green,
            ),
            Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: S.w * .04, vertical: S.w * .05),
              decoration: BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    offset: Offset(0, 0),
                    blurRadius: 2,
                    spreadRadius: 2,
                    color: Colors.grey.withOpacity(.2))
              ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (isForum)
                    RoundedAvatar(
                      image: profile.profileImagePresignedUrl ?? '',
                      size: 40,
                    ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      margin: const EdgeInsets.symmetric(horizontal: 15),
                      constraints: BoxConstraints(maxHeight: 100, minHeight: 40),
                      decoration: BoxDecoration(
                        color: Color(0xfff8f8f8),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: SingleChildScrollView(
                          child: TextField(
                            controller: commentController,
                            focusNode: fNodeComment,
                            maxLines: null,
                            // onChanged: (value) {
                            //   print(value.length);
                            // },
                            style: normalText(context),
                            decoration: InputDecoration(
                                hintText: 'Tulis Komentar disini',
                                hintStyle: hintText(context),
                                border: InputBorder.none,
                                suffixIcon: isForum
                                    ? InkWell(
                                        onTap: () {
                                          fNodeComment.unfocus();
                                          // getFileAny(context: context);
                                          upload();
                                        },
                                        child: Icon(Icons.attach_file,
                                            size: 22, color: SiswamediaTheme.green),
                                      )
                                    : SizedBox()),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: S.w * .1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // Material(
                        //   color: Colors.transparent,
                        //   child: InkWell(
                        //     onTap: () {
                        //       fNodeChat.unfocus();
                        //       getFile(
                        //           context: context,
                        //           extension: ['jpeg', 'jpg', 'png']);
                        //     },
                        //     child: Icon(Icons.camera_alt,
                        //         size: 26, color: SiswamediaTheme.green),
                        //   ),
                        // ),
                        // Transform.rotate(
                        //   angle: 5 * pi / 4,
                        //   child: Material(
                        //     color: Colors.transparent,
                        //     child: ,
                        //   ),
                        // ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(200),
                          child: Material(
                            color: SiswamediaTheme.green,
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: InkWell(
                                onTap: onTap,
                                child: Icon(Icons.send, size: 26, color: SiswamediaTheme.white),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
