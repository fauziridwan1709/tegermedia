part of '_forum.dart';

class ForumDescription extends StatelessWidget {
  final String desc;
  ForumDescription({this.desc});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .01),
      child: CustomText(
        desc,
        Kind.descBlack,
        align: TextAlign.left,
      ),
    );
  }
}
