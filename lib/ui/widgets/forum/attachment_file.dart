part of '_forum.dart';

class AttachmentFile extends StatelessWidget {
  final String filename;
  final VoidCallback onTap;
  AttachmentFile({this.filename, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .01),
      child: Wrap(
        children: [
          Text(filename + '  ',
              style: TextStyle(
                  fontSize: S.w / 30,
                  color: Colors.black,
                  fontWeight: FontWeight.w600)),
          InkWell(
            onTap: onTap,
            child: Text('Unduh',
                style: TextStyle(
                    fontSize: S.w / 30,
                    color: SiswamediaTheme.green,
                    fontWeight: FontWeight.w500)),
          ),
        ],
      ),
    );
  }
}
