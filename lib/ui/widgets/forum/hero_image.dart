part of '_forum.dart';

class HeroImage extends Hero {
  HeroImage({
    String tag,
    VoidCallback onTap,
    String link,
  }) : super(
            tag: 'preview-sub-comment',
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 200, maxHeight: 250),
              child: Material(
                  color: Colors.transparent,
                  child: InkWell(onTap: onTap, child: Image.network(link))),
            ));
}
