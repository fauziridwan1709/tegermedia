part of '_widgets.dart';

class RoundedButton extends StatelessWidget {
  const RoundedButton({this.color, this.text, this.onPressed});
  final Color color;
  final String text;
  final Function onPressed;
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        color: color,
        shape: RoundedRectangleBorder(
          borderRadius: radius(250),
        ),
        onPressed: () {
          onPressed();
        },
        child: CustomText(text, Kind.descWhite));
  }
}
