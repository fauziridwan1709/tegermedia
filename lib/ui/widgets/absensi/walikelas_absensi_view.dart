part of '_absensi.dart';

class AbsensiView extends StatelessWidget {
  final List<CoursesAbsenSummary> data;
  final UserCurrentState userState;
  final Animation animation;
  final AnimationController controller;
  final VoidCallback onTap;
  final List<ClassBySchoolData> listClass;
  final String value;
  final Function(String s) onChange;

  AbsensiView(
      {this.data,
      this.userState,
      this.onTap,
      this.listClass,
      this.controller,
      this.value,
      this.onChange,
      this.animation});

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Highlight(
        animation: animation,
        controller: controller,
        title: 'Absensi Saya',
        button: Container(
          height: S.w * .09,
          decoration: BoxDecoration(
            color: SiswamediaTheme.green,
            borderRadius: BorderRadius.circular(6),
          ),
          child: InkWell(
            onTap: onTap,
            child: Center(
                child: Text('Selengkapnya',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: S.w / 30,
                        fontWeight: FontWeight.normal))),
          ),
        ),
      ),
      InkWell(
        onTap: () => context.push<void>(KotakMasuk()),
        child: Container(
          margin: EdgeInsets.all(16),
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                    offset: Offset(2, 2),
                    spreadRadius: 2,
                    blurRadius: 2,
                    color: Colors.grey.withOpacity(.2))
              ]),
          child: Row(
            children: [
              Image.asset(
                'assets/images/mailbox.png',
                height: 24,
                width: 24,
              ),
              SizedBox(
                width: 20,
              ),
              vText('Kotak Masuk')
            ],
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: S.w * .05),
        child: Text('Absensi Kelas',
            style: TextStyle(
                color: Colors.black87,
                fontSize: S.w / 23,
                fontWeight: FontWeight.w600)),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: S.w * .05),
        child: Column(
          children: listClass
              .where((element) => element.id == userState.classId)
              .map((e) => Container(
                    width: S.w * .9,
                    height: S.w * .2,
                    margin: EdgeInsets.symmetric(vertical: S.w * .025),
                    padding: EdgeInsets.symmetric(
                        vertical: S.w * .025, horizontal: S.w * .05),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(2, 2),
                              spreadRadius: 2,
                              blurRadius: 2,
                              color: Colors.grey.withOpacity(.2))
                        ]),
                    child: InkWell(
                      onTap: () {
                        Navigator.push<void>(
                            context,
                            MaterialPageRoute(
                                builder: (_) => WaliKelasSiswa(
                                    value: value,
                                    onChange: onChange,
                                    title: 'Siswa ' + e.name)));
                      },
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              e.name,
                              style: TextStyle(
                                fontSize: S.w / 25,
                                fontWeight: FontWeight.w600,
                                color: Colors.black87,
                              ),
                            ),
                            SizedBox(height: 8),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'Sekolah : ',
                                      style: TextStyle(
                                          fontSize: S.w / 28,
                                          color: Colors.grey[600])),
                                  TextSpan(
                                      text: userState.schoolName,
                                      style: TextStyle(
                                          fontSize: S.w / 32,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black87)),
                                ],
                              ),
                            ),
                          ]),
                    ),
                  ))
              .toList(),
        ),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: S.w * .05),
        child: Text('Absensi Pelajaran Saya',
            style: TextStyle(
                color: Colors.black87,
                fontSize: S.w / 23,
                fontWeight: FontWeight.w600)),
      ),
      SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: S.w * .05),
          child: Column(
            children: data
                .map((CoursesAbsenSummary e) => InkWell(
                      onTap: () => navigate(context, EditAbsen(course: e)),
                      child: Container(
                        width: S.w * .9,
                        height: S.w * .2,
                        margin: EdgeInsets.symmetric(vertical: S.w * .025),
                        padding: EdgeInsets.symmetric(
                            vertical: S.w * .025, horizontal: S.w * .05),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(2, 2),
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  color: Colors.grey.withOpacity(.2))
                            ]),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                e.courseName.capitalizeFirstOfEach,
                                style: TextStyle(
                                  fontSize: S.w / 25,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black87,
                                ),
                              ),
                              SizedBox(height: 8),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.black,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: 'Sekolah : ',
                                        style: TextStyle(
                                            fontSize: S.w / 28,
                                            color: Colors.grey[600])),
                                    TextSpan(
                                        text: userState.schoolName,
                                        style: TextStyle(
                                            fontSize: S.w / 32,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black87)),
                                  ],
                                ),
                              ),
                            ]),
                      ),
                    ))
                .toList(),
          ),
        ),
      ),
    ]);
  }
}
