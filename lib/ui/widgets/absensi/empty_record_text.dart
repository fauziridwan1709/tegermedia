part of '_absensi.dart';

class EmptyRecordText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    return ListView(
      children: [
        SizedBox(
          height: media.size.height -
              (media.padding.top + AppBar().preferredSize.height),
          width: media.size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/png/absen_empty_record.png',
                width: 200,
              ),
              HeightSpace(20),
              SimpleText(
                'Empty Record',
                style: SiswamediaTextStyle.title,
                fontSize: 22,
              ),
              HeightSpace(10),
              Text(
                'Belum Ada Record, Mulailah Absen anda\ndifitur jadwal',
                textAlign: TextAlign.center,
              ),
              HeightSpace(80)
            ],
          ),
        )
      ],
    );
  }
}
