import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/ui/pages/Absensi/_absensi.dart';
import 'package:tegarmedia/utils/v_widget.dart';

part 'absensi_curve.dart';
part 'empty_record_text.dart';
part 'walikelas_absensi_view.dart';
