part of '_home.dart';

class CardNews extends StatelessWidget {
  final HomeItem rows;
  final bool isCached;

  CardNews(this.rows, this.isCached);

  @override
  Widget build(BuildContext context) {
    if (rows == null) {
      return Container(
          margin: EdgeInsets.symmetric(vertical: S.w * .02),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                    offset: Offset(2, 2),
                    spreadRadius: 4,
                    blurRadius: 4,
                    color: Colors.grey.withOpacity(.1))
              ]),
          child: Container(
            height: S.w * .3,
            width: S.w * .9,
            padding: EdgeInsets.symmetric(horizontal: S.w * .02),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                      height: S.w * .22,
                      width: S.w * .22,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey.withOpacity(.2),
                      )),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .02, vertical: S.w * .04),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Shimmer.fromColors(
                          baseColor: Colors.grey[300],
                          highlightColor: Colors.grey[100],
                          enabled: true,
                          child: Container(
                              width: S.w * .58,
                              height: 10,
                              decoration: BoxDecoration(
                                borderRadius: radius(100),
                                color: Colors.grey.withOpacity(.2),
                              )),
                        ),
                        SizedBox(height: 10),
                        Shimmer.fromColors(
                          baseColor: Colors.grey[300],
                          highlightColor: Colors.grey[100],
                          enabled: true,
                          child: Container(
                              width: S.w * .3,
                              height: 10,
                              decoration: BoxDecoration(
                                borderRadius: radius(100),
                                color: Colors.grey.withOpacity(.2),
                              )),
                        ),
                      ]),
                ),
              ],
            ),
          ));
    }
    return Container(
        margin: EdgeInsets.symmetric(vertical: S.w * .02),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                  offset: Offset(2, 2),
                  spreadRadius: 4,
                  blurRadius: 4,
                  color: Colors.grey.withOpacity(.1))
            ]),
        child: Container(
          width: S.w * .9,
          padding: EdgeInsets.symmetric(horizontal: S.w * .03),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CachedImage(
                rows.gambar ??
                    'https://www.creativefabrica.com/wp-content/uploads/2019/02/Loading-Icon-by-Kanggraphic-2-580x386.jpg',
                isCached: isCached,
                height: S.w * .16,
                width: S.w * .16,
                borderRadius: BorderRadius.circular(10),
                backgroundColor: Colors.grey.withOpacity(.2),
                fit: BoxFit.cover,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .03, vertical: S.w * .05),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: S.w * .58,
                          child: RichText(
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            strutStyle: StrutStyle(fontSize: S.w / 32),
                            text: TextSpan(
                                text: rows.judul,
                                style: semiBlack.copyWith(fontSize: S.w / 32)),
                          ),
                        ),
                        // CustomText('- ', Kind.descBlack),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomText(rows.tanggal, Kind.descBlack),
                            CustomText(rows.penerbit, Kind.descBlack),
                          ],
                        ),
                      ]),
                ),
              )
            ],
          ),
        ));
  }
  //   return Container(
  //       decoration: BoxDecoration(
  //         borderRadius: const BorderRadius.all(Radius.circular(16.0)),
  //         boxShadow: <BoxShadow>[
  //           BoxShadow(
  //             color: Colors.grey.withOpacity(0.6),
  //             offset: const Offset(4, 4),
  //             blurRadius: 16,
  //           ),
  //         ],
  //       ),
  //       child: ClipRRect(
  //         borderRadius: const BorderRadius.all(Radius.circular(16.0)),
  //         child: Stack(
  //           children: <Widget>[
  //             Column(
  //               children: <Widget>[
  //                 AspectRatio(
  //                     aspectRatio: 2,
  //                     child: Image.network(
  //                       rows.gambar == null
  //                           ? 'https://www.creativefabrica.com/wp-content/uploads/2019/02/Loading-Icon-by-Kanggraphic-2-580x386.jpg'
  //                           : rows.gambar,
  //                       fit: BoxFit.cover,
  //                     )),
  //                 Container(
  //                   child: Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     crossAxisAlignment: CrossAxisAlignment.start,
  //                     children: <Widget>[
  //                       Expanded(
  //                         child: Container(
  //                           child: Padding(
  //                             padding: const EdgeInsets.only(
  //                                 left: 16, top: 8, bottom: 8),
  //                             child: Column(
  //                               mainAxisAlignment: MainAxisAlignment.center,
  //                               crossAxisAlignment: CrossAxisAlignment.start,
  //                               children: <Widget>[
  //                                 Text(
  //                                   'Text',
  //                                   textAlign: TextAlign.left,
  //                                   style: TextStyle(
  //                                     fontWeight: FontWeight.w600,
  //                                     fontSize: 22,
  //                                   ),
  //                                 ),
  //                                 Row(
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.center,
  //                                   mainAxisAlignment: MainAxisAlignment.start,
  //                                   children: <Widget>[
  //                                     Text(
  //                                       'Text',
  //                                       style: TextStyle(
  //                                           fontSize: 14,
  //                                           color:
  //                                               Colors.grey.withOpacity(0.8)),
  //                                     ),
  //                                     const SizedBox(
  //                                       width: 4,
  //                                     ),
  //                                     Expanded(
  //                                       child: Text(
  //                                         ' km to city',
  //                                         overflow: TextOverflow.ellipsis,
  //                                         style: TextStyle(
  //                                             fontSize: 14,
  //                                             color:
  //                                                 Colors.grey.withOpacity(0.8)),
  //                                       ),
  //                                     ),
  //                                   ],
  //                                 ),
  //                                 Padding(
  //                                   padding: const EdgeInsets.only(top: 4),
  //                                   child: Row(
  //                                     children: <Widget>[
  //                                       Text(
  //                                         ' Reviews',
  //                                         style: TextStyle(
  //                                             fontSize: 14,
  //                                             color:
  //                                                 Colors.grey.withOpacity(0.8)),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ),
  //                       Padding(
  //                         padding: const EdgeInsets.only(right: 16, top: 8),
  //                         child: Column(
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           crossAxisAlignment: CrossAxisAlignment.end,
  //                           children: <Widget>[
  //                             Text(
  //                               '200000',
  //                               textAlign: TextAlign.left,
  //                               style: TextStyle(
  //                                 fontWeight: FontWeight.w600,
  //                                 fontSize: 22,
  //                               ),
  //                             ),
  //                             Text(
  //                               '/per night',
  //                               style: TextStyle(
  //                                   fontSize: 14,
  //                                   color: Colors.grey.withOpacity(0.8)),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ],
  //             ),
  //             Positioned(
  //               top: 8,
  //               right: 8,
  //               child: Material(
  //                 color: Colors.transparent,
  //                 child: InkWell(
  //                   borderRadius: const BorderRadius.all(
  //                     Radius.circular(32.0),
  //                   ),
  //                   onTap: () {},
  //                   child: Padding(
  //                     padding: const EdgeInsets.all(8.0),
  //                     child: Icon(
  //                       Icons.favorite_border,
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             )
  //           ],
  //         ),
  //       ));
  // }
}
