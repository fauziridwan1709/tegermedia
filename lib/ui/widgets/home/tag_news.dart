part of '_home.dart';

class TagNews extends StatelessWidget {
  final int currentIndex;
  final String title;
  final Function(int) onTap;
  final List<String> tags;

  TagNews({this.currentIndex, this.title, this.onTap, this.tags});
  @override
  Widget build(BuildContext context) {
    var tagNews = tags ?? <String>['Berita Sekolah', 'Artikel'];
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      margin: EdgeInsets.symmetric(horizontal: 4, vertical: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(200),
        border: Border.all(
          color: currentIndex == tagNews.indexOf(title) ? Colors.transparent : Color(0xffE6E6E6),
        ),
        color: currentIndex == tagNews.indexOf(title) ? SiswamediaTheme.green : Colors.transparent,
      ),
      child: InkWell(
        onTap: () => onTap(tagNews.indexOf(title)),
        child: Center(
            child: Text(title,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 10,
                    color: currentIndex == tagNews.indexOf(title) ? Colors.white : Colors.black))),
      ),
    );
  }
}
