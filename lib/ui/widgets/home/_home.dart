import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'berita_sekolah.dart';
part 'card_ads.dart';
part 'card_donasi.dart';
part 'card_materi.dart';
part 'card_news.dart';
part 'drag_target.dart';
part 'draggable_widget.dart';
part 'tag_news.dart';
