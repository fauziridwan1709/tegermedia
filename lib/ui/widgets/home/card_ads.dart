part of '_home.dart';

class CardAds extends StatelessWidget {
  final HomeItem ads;

  CardAds(this.ads);

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    if (ads == null) {
      return Container(
        height: S.w * 6,
        width: S.w * .7,
        margin:
            EdgeInsets.symmetric(horizontal: S.w * .03, vertical: S.w * .03),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [SiswamediaTheme.shadowContainer],
        ),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          enabled: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  height: S.w * .37,
                  width: S.w * 7,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(12),
                        topRight: Radius.circular(12)),
                    color: Colors.grey.withOpacity(.9),
                  )),
              Container(
                height: S.w * .16,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .02, vertical: S.w * .01),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: S.w * .7,
                          height: 10,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.grey.withOpacity(.9)),
                        ),
                        Container(
                          width: S.w * .3,
                          height: 10,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.grey.withOpacity(.9)),
                        ),
                      ]),
                ),
              )
            ],
          ),
        ),
      );
    }
    return Container(
      height: S.w * 6,
      width: S.w * .7,
      margin: EdgeInsets.symmetric(horizontal: S.w * .03, vertical: S.w * .03),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [SiswamediaTheme.shadowContainer],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CachedImage(
            ads.gambar ??
                'https://www.creativefabrica.com/wp-content/uploads/2019/02/Loading-Icon-by-Kanggraphic-2-580x386.jpg',
            height: S.w * .37,
            width: S.w * 7,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12), topRight: Radius.circular(12)),
            backgroundColor: Colors.grey.withOpacity(.2),
            fit: BoxFit.cover,
            isCached: ads.cached,
            alignment: Alignment.topCenter,
          ),
          // Container(
          //     height: S.w * .37,
          //     width: S.w * 7,
          //     decoration: BoxDecoration(
          //       borderRadius: BorderRadius.only(
          //           topLeft: Radius.circular(12),
          //           topRight: Radius.circular(12)),
          //       color: Colors.grey.withOpacity(.2),
          //       image: DecorationImage(
          //           image: NetworkImage(
          //             ads.gambar ??
          //                 'https://www.creativefabrica.com/wp-content/uploads/2019/02/Loading-Icon-by-Kanggraphic-2-580x386.jpg',
          //           ),
          //           fit: BoxFit.cover,
          //           alignment: Alignment.topCenter),
          //     )),
          Container(
            height: S.w * .16,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: S.w * .02, vertical: S.w * .02),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 1,
                      child: RichText(
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        strutStyle: StrutStyle(fontSize: S.w / 30),
                        text: TextSpan(
                          text: ads.judul,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: S.w / 28,
                            letterSpacing: 0.18,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    Text(ads.tanggal, style: TextStyle(fontSize: S.w / 30)),
                  ]),
            ),
          )
        ],
      ),
    );
  }
}
