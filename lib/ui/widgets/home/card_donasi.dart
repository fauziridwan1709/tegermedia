part of '_home.dart';

class CardDonasi extends StatelessWidget {
  final HomeItem donasi;
  final Function onTap;

  CardDonasi(this.donasi, {this.onTap});

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    if (donasi == null) {
      return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(S.w / 30),
        ),
        child: Container(
          height: S.w * 45,
          width: S.w * .45,
          child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            enabled: true,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: S.w * .26,
                  width: S.w * 45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(12),
                        topRight: Radius.circular(12)),
                    color: Colors.grey.withOpacity(.6),
                  ),
                  child: Center(
                      child: Container(
                    width: S.w / 9,
                    height: S.w / 9,
                    decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(.7),
                        shape: BoxShape.circle,
                        border: Border.all(
                            width: 2,
                            color: SiswamediaTheme.green.withOpacity(.9))),
                    child: Icon(Icons.play_arrow, color: SiswamediaTheme.green),
                  )),
                ),
                SizedBox(
                  height: S.w * .15,
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: S.w * .02, vertical: S.w * .02),
                      child: Column(children: [
                        Container(
                          width: S.w * .25,
                          height: 10,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.grey.withOpacity(.9)),
                        ),
                      ])),
                )
              ],
            ),
          ),
        ),
      );
    }
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(S.w / 30),
          side: BorderSide(color: Color(0xffCFCFCF))),
      elevation: 0,
      child: Container(
        height: S.w * 45,
        width: S.w * .45,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CachedImage(
              donasi.gambar ??
                  'https://www.creativefabrica.com/wp-content/uploads/2019/02/Loading-Icon-by-Kanggraphic-2-580x386.jpg',
              isCached: donasi.cached,
              height: S.w * .26,
              width: S.w * .45,
              fit: BoxFit.cover,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12), topRight: Radius.circular(12)),
              backgroundColor: Colors.grey.withOpacity(.2),
              child: Center(
                  child: Container(
                width: S.w / 9,
                height: S.w / 9,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(.7),
                    shape: BoxShape.circle,
                    border: Border.all(
                        width: 2,
                        color: SiswamediaTheme.green.withOpacity(.3))),
                child: Icon(Icons.play_arrow, color: SiswamediaTheme.green),
              )),
            ),
            SizedBox(
              height: S.w * .15,
              child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .02, vertical: S.w * .01),
                  child: Column(children: [
                    Flexible(
                      flex: 1,
                      child: RichText(
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        strutStyle: StrutStyle(fontSize: S.w / 30),
                        text: TextSpan(
                          text: donasi.judul,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: S.w / 28,
                            letterSpacing: 0.18,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ])),
            )
          ],
        ),
      ),
    );
  }
}
