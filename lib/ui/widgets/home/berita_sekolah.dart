part of '_home.dart';

class BeritaSekolah extends StatelessWidget {
  final Orientation orientation;
  final double ratio;
  final Function(HomeItem data) onTap;
  final Future<HomeModel> future;
  const BeritaSekolah({this.orientation, this.ratio, this.onTap, this.future});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return SizedBox(
        width: width * .9,
        child: FutureBuilder<HomeModel>(
            future: future,
            builder: (context, snapshot) {
              return LayoutBuilder(
                builder: (context, constrains) {
                  return GridView.count(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    padding: EdgeInsets.only(
                        left: width * 0.05, right: width * 0.05),
                    crossAxisCount: gridCountHome(ratio),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 3,
                    children: (snapshot.hasData
                            ? snapshot.data.rows.reversed
                            : [HomeItem(), HomeItem(), HomeItem(), HomeItem()])
                        .map(
                          (e) => InkWell(
                              onTap: () => onTap(e),
                              child: CardNews(
                                  snapshot.hasData ? e : null, e.cached)),
                        )
                        .toList()
                        .sublist(
                            0,
                            snapshot.hasData
                                ? snapshot.data.rows.reversed.length > 1
                                    ? 2
                                    : 1
                                : 2
                            // : snapshot.data.rows.reversed.length - 1
                            ),
                  );
                },
              );
            }));
  }
}
