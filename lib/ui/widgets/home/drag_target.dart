part of '_home.dart';

// class DragTargetWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return DragTarget(onWillAccept: (bool data) {
//       return true;
//     }, onAccept: (Map<String, dynamic> data) {
//       if (Provider.of<DragProvider>(context).itemsList.isNotEmpty) {
//         Provider.of<DragProvider>(context).removeLastItem();
//         Provider.of<DragProvider>(context).changeSuccessDrop(true);
//         Provider.of<DragProvider>(context).changeAcceptedData(data);
//       }
//     }, builder: (context, List<Map<String, dynamic>> cd, rd) {
//       if (Provider.of<DragProvider>(context).isSuccessDrop) {
//         return Padding(
//           padding: const EdgeInsets.all(25.0),
//           child: Stack(
//             children:
//                 buildTargetList(Provider.of<DragProvider>(context).items[2]),
//           ),
//         );
//       } else {
//         return Padding(
//           padding: const EdgeInsets.all(25.0),
//           child: Container(
//               height: 200.0,
//               width: 200.0,
//               color: Colors.grey[400],
//               child: Card(
//                 shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(5)),
//                 color: Colors.grey[400],
//                 child: Center(
//                   child: Icon(Icons.add, color: SiswamediaTheme.green),
//                 ),
//               )),
//         );
//       }
//     });
//   }
//
//   List<Widget> buildTargetList(Map<String, dynamic> cardItem) {
//     var targetList = <Widget>[];
//     targetList.add(Container(
//       height: 200.0,
//       width: 200.0,
//       child: Card(
//         shape:
//             RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
//         // color: cardItem.cardColor,
//         child: Center(
//             child: Text(
//           '${cardItem}',
//           style: TextStyle(fontSize: 25.0, color: Colors.white),
//         )),
//       ),
//     ));
//     return targetList;
//   }
// }
