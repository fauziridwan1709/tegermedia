part of '_home.dart';

class DraggableWidget extends StatelessWidget {
  DraggableWidget({
    Key key,
    @required this.items,
    @required this.i,
  }) : super(key: key);

  final int i;
  final List<Map<String, dynamic>> items;

  @override
  Widget build(BuildContext context) {
    return Draggable(
      data: items,
      childWhenDragging: Container(
        height: 200.0,
        width: 200.0,
        child: Card(
          elevation: 2.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          color:
              // i >= 1
              // ? Provider.of<DragProvider>(context).itemsList.elementAt(
              // (Provider.of<DragProvider>(context).itemsList.length - 1) - 1)
              // .cardColor :
              Colors.grey,
          child: Center(
            child: Text(
              // i >= 1
              // ? '${Provider.of<DragProvider>(context).itemsList.elementAt((Provider.of<DragProvider>(context).itemsList.length - 1) - 1)}'
              // :
              'no Data',
              style: TextStyle(fontSize: 25.0, color: Colors.white),
            ),
          ),
        ),
      ),
      feedback: Container(
        height: 200.0,
        width: 200.0,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          elevation: 2.0,
          // color: Provider.of<DragProvider>(context).itemsList[i],
          child: Center(
              child: Text(
            ''
            // '${Provider.of<DragProvider>(context).itemsList[i]}',
            ,
            style: TextStyle(fontSize: 25.0, color: Colors.white),
          )),
        ),
      ),
      child: Container(
        height: 200.0,
        width: 200.0,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          elevation: 2.0,
          // color: Provider.of<DragProvider>(context).itemsList[i].cardColor,
          child: Center(
              child: Text(
            ''
            // '${Provider.of<DragProvider>(context).itemsList[i]}',
            ,
            style: TextStyle(fontSize: 25.0, color: Colors.white),
          )),
        ),
      ),
    );
  }
}
