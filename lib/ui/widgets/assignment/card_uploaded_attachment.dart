part of '_assignment.dart';

class CardUploadedAttachment extends StatelessWidget {
  final MainAttachments attachment;
  final VoidCallback onTap;
  final VoidCallback onDelete;

  CardUploadedAttachment({this.attachment, this.onTap, this.onDelete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: Container(
            width: S.w,
            height: 42,
            margin: EdgeInsets.symmetric(
                // horizontal: width * 0.05,
                vertical: 3),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[300]),
                borderRadius: BorderRadius.circular(5),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: SiswamediaTheme.grey.withOpacity(0.2),
                      offset: const Offset(0.2, 0.2),
                      blurRadius: 3.0),
                ],
                color: Colors.white),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      attachment.fileName,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  GestureDetector(
                    onTap: onDelete,
                    child: Text(
                      'Hapus',
                      style: TextStyle(color: Colors.red),
                    ),
                  )
                  // Spacer(),
                ])));
  }
}
