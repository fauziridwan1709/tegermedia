part of '_assignment.dart';

class ButtonCreateTugas extends StatelessWidget {
  final String role;
  final VoidCallback onTap;

  ButtonCreateTugas({this.role, this.onTap});

  @override
  Widget build(BuildContext context) {
    if (role.isSiswaOrOrtu) {
      return SizedBox();
    } else {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 16),
        child: InkWell(
          onTap: onTap,
          child: Container(
            width: MediaQuery.of(context).size.width - 28 * 2,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xFFD9D9D9)),
                borderRadius: BorderRadius.circular(10)),
            child:
                Icon(Icons.add_circle, size: 35, color: SiswamediaTheme.green),
          ),
        ),
      );
    }
  }
}
