part of '_assignment.dart';

class FilterHomeSort extends StatelessWidget {
  final Function(String val) onChanged;
  final String value;
  final String selectedValue;

  FilterHomeSort({this.onChanged, this.value, this.selectedValue});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: SiswamediaTheme.green.withOpacity(.2),
        onTap: () => onChanged(value),
        child: Row(
          children: [
            Radio<String>(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                value: value,
                groupValue: selectedValue,
                onChanged: onChanged),
            Text(
              value,
              style: mediumBlack.copyWith(fontSize: S.w / 30),
            ),
          ],
        ),
      ),
    );
  }
}
