part of '_assignment.dart';

class FilterHomeStatus extends StatelessWidget {
  final Function(bool val) onChanged;
  final bool value;
  final String title;

  FilterHomeStatus({this.onChanged, this.value, this.title});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: SiswamediaTheme.green.withOpacity(.2),
        onTap: () => onChanged(value),
        child: Row(
          children: [
            Checkbox(
              value: value,
              onChanged: onChanged,
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            ),
            Text(
              title,
              style: mediumBlack.copyWith(fontSize: S.w / 30),
            ),
          ],
        ),
      ),
    );
  }
}
