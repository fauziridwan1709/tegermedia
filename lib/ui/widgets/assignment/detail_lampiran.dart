part of '_assignment.dart';

class TugasDetailLampiran extends StatelessWidget {
  TugasDetailLampiran({@required this.onView, @required this.attachments})
      : assert(onView != null),
        assert(attachments != null);
  final List<MainAttachments> attachments;
  final Function(MainAttachments attachment) onView;

  @override
  Widget build(BuildContext context) {
    if (attachments.isEmpty) {
      return Text('Tidak ada lampiran..',
          style: descBlack.copyWith(
            fontSize: S.w / 32,
          ));
    }
    // var listTypeImage = <String>['png', 'jpg', 'jpeg'];
    // var listImage = attachments.where(
    //     (element) => listTypeImage.contains(element.fileName.extensionType));
    return Column(
      children: attachments
          .map((attachment) => GestureDetector(onTap: () async {
                onView(attachment);
              }, child: LayoutBuilder(
                builder: (context, _) {
                  //todo
                  // var type = attachment.fileName.extensionType;

                  // if (listTypeImage.contains(type)) {}
                  return Container(
                      margin: EdgeInsets.only(bottom: 2),
                      alignment: Alignment.centerLeft,
                      height: 40,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                color: SiswamediaTheme.green,
                                borderRadius: BorderRadius.circular(50)),
                            child: Icon(
                              Icons.file_download,
                              size: 18,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: 5),
                          Expanded(
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2 +
                                            80,
                                    child: Text(
                                      attachment.fileName,
                                      overflow: TextOverflow.ellipsis,
                                    ))),
                          )
                        ],
                      ));
                },
              )))
          .toList(),
    );
  }
}
