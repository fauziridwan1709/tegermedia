part of '_assignment.dart';

class MultiSelectClass extends StatelessWidget {
  final VoidCallback onTap;
  final List<Widget> widget;
  final double radiusContainer;

  MultiSelectClass({this.onTap, this.widget, this.radiusContainer = 6});

  List<Widget> defaultWidget = [
    Text('Tambahkan Kelas', style: mediumBlack.copyWith(fontSize: S.w / 30)),
    Icon(
      Icons.arrow_forward_ios_outlined,
      color: SiswamediaTheme.green,
      size: 20,
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: SiswamediaTheme.white,
        border: Border.all(color: SiswamediaTheme.border),
        borderRadius: radius(radiusContainer),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 12),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: widget ?? defaultWidget),
          ),
        ),
      ),
    );
  }
}
