part of '../_widgets.dart';

class RoundedAvatar extends StatelessWidget {
  RoundedAvatar({this.image, this.size});
  final String image;
  final double size;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(12),
      child: Image(
        image: NetworkImage(image),
        height: size,
        width: size,
        fit: BoxFit.cover,
      ),
    );
  }
}
