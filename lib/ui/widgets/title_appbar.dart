part of '_widgets.dart';

class TitleAppbar extends StatelessWidget {
  final String label;

  TitleAppbar({@required this.label});

  @override
  Widget build(BuildContext context) {
    return Text(label,
        style: TextStyle(
            color: Colors.black,
            fontSize: S.w / 24,
            fontWeight: FontWeight.w600));
  }
}
