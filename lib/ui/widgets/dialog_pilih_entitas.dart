part of '_widgets.dart';

class MyDialog extends StatefulWidget {
  const MyDialog({this.onValueChange, this.initialValue, this.onSubmit});

  final int initialValue;
  final void Function(int) onValueChange;
  final void Function() onSubmit;

  @override
  State createState() => MyDialogState();
}

class MyDialogState extends State<MyDialog> {
  int _selectedId;

  @override
  void initState() {
    super.initState();
    _selectedId = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Pilih Entitas'),
      content: Builder(builder: (context) {
        return Container(
          width: 50,
          height: 120,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 0,
                          groupValue: _selectedId,
                          onChanged: (int value) {
                            setState(() {
                              _selectedId = value;
                            });
                            widget.onValueChange(value);
                          },
                        ),
                        Text('Guru  '),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 1,
                          groupValue: _selectedId,
                          onChanged: (int value) {
                            setState(() {
                              _selectedId = value;
                            });
                            widget.onValueChange(value);
                          },
                        ),
                        Text('Siswa'),
                      ],
                    ),
                  ]),
                ],
              ),
            ],
          ),
        );
      }),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pop(context);
            widget.onSubmit();
          },
          child: Text('Pilih'),
        ),
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Kembali'),
        ),
      ],
    );
  }
}
