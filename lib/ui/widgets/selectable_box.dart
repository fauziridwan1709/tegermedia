part of '_widgets.dart';

class SelectableBox extends StatelessWidget {
  final bool isSelected;
  final bool isEnabled;
  final Function onTap;
  final double height;
  final double width;
  final String text;
  final TextStyle textStyle;

  SelectableBox(this.text,
      {this.isEnabled = true,
      this.isSelected = false,
      this.height = 60,
      this.onTap,
      this.width = 144,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          if (onTap != null) {
            onTap();
          }
        },
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: (!isEnabled)
                  ? Color(0xFFE4E4E4)
                  : isSelected
                      ? SiswamediaTheme.green
                      : Colors.transparent,
              border: Border.all(
                  color: (!isEnabled)
                      ? Color(0xFFE4E4E4)
                      : isSelected
                          ? Colors.transparent
                          : Color(0xFFE4E4E4))),
          child: Center(
            child: Text(
              text ?? 'None',
              style: (textStyle ?? blackTextFont)
                  .copyWith(fontSize: 16, fontWeight: FontWeight.w400),
            ),
          ),
        ));
  }
}
