part of '_class.dart';

class ClassNotFound extends StatelessWidget {
  final String label;
  final String type;
  const ClassNotFound({Key key, this.label, this.type = 'detail'})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SizedBox(height: 50),
      Container(
          height: 170,
          width: 215,
          margin: EdgeInsets.only(bottom: 8),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/icons/kelas/kelas_empty.png')))),
      SizedBox(height: 10),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: S.w * .1),
        child: Center(
            child: Text(
          '$label',
          style: semiBlack.copyWith(fontSize: S.w / 30),
          textAlign: TextAlign.center,
        )),
      )
    ]);
  }
}
