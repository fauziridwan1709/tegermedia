part of '_class.dart';

class ViewErrorClassroom extends StatelessWidget {
  final dynamic error;
  ViewErrorClassroom({this.error});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return ListView(
      children: [
        if (MediaQuery.of(context).orientation == Orientation.portrait)
          SizedBox(height: width * .25),
        Image.asset(
          error is SocketException
              ? 'assets/404.png'
              : 'assets/dialog/sorry_no_student.png',
          width: width * 1,
          height: width * .5,
        ),
        SizedBox(height: 20),
        Text(
          error is SocketException
              ? error.message
              : (error as SiswamediaException).message,
          style: semiBlack.copyWith(fontSize: 14),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: S.w * .1)
      ],
    );
  }
}
