part of '_class.dart';

class ErrorView extends StatelessWidget {
  final dynamic error;
  ErrorView({this.error}) : assert(error != null);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    print(error);
    return ListView(
      children: [
        if (MediaQuery.of(context).orientation == Orientation.portrait)
          SizedBox(height: width * .3),
        Image.asset(
          (error is SiswamediaException)
              ? 'assets/dialog/sorry_no_student.png'
              : 'assets/404.png',
          width: width * 1,
          height: width * .65,
        ),
        SizedBox(height: 20),
        Text(
          '${(error is SiswamediaException) ? error.message : (error is TimeoutException) ? (error as TimeoutException).message : (error as SocketException).message}',
          style: semiBlack.copyWith(fontSize: 14),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40),
      ],
    );
  }
}
