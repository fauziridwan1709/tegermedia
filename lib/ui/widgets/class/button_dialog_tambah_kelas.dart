part of '_class.dart';

class ButtonDialogTambah extends StatelessWidget {
  final VoidCallback onTap;
  final String label;
  final IconData icon;

  ButtonDialogTambah({this.onTap, this.label, this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: SiswamediaTheme.green,
        borderRadius: radius(12),
      ),
      child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: S.w * .05, vertical: S.w * .03),
            child: Center(
                child: Row(
              children: [
                Icon(icon, color: SiswamediaTheme.white),
                SizedBox(width: 10),
                Text(
                  label,
                  style: semiWhite.copyWith(fontSize: S.w / 26),
                ),
              ],
            )),
          )),
    );
  }
}
