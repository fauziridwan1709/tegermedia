part of '_widgets.dart';

class TugasCard extends StatelessWidget {
  final double widthApp;
  final double heightApp;
  final String namaMapel;
  final String keteranganTugas;
  final String deadlinePengerjaan;
  final String statusTugas;
  final int index;
  final dynamic nilaiMataPelajaran;
  final Function ubahStatus;
  final Function ubahNilai;

  const TugasCard({
    Key key,
    this.widthApp,
    this.heightApp,
    this.namaMapel,
    this.keteranganTugas,
    this.deadlinePengerjaan,
    this.statusTugas,
    this.index,
    this.nilaiMataPelajaran,
    this.ubahStatus,
    this.ubahNilai,
  }) : super(key: key);

  static List<String> logoMapelUrutan = [
    'Biologi',
    'Fisika',
    'Matematika',
    'Sejarah'
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthApp * 0.95,
      padding: EdgeInsets.only(bottom: 5, left: 10, right: 10),
      child: Stack(
        children: <Widget>[
          Container(
            width: widthApp * 0.95,
            height: heightApp * 0.25,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Card(
                    elevation: 0,
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Image.asset(
                            'assets/png/1.png',
                            fit: BoxFit.fill,
                            height: widthApp * 0.2,
                            width: widthApp * 0.2,
                          ),
                          Row(
                            children: <Widget>[
                              // SizedBox(
                              //   width: widthApp * 0.125,
                              // ),
                              //Informasi Tugas
                              Container(
                                width: widthApp * 0.45,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    FittedBox(
                                      child: Text(
                                        namaMapel,
                                        style: GoogleFonts.openSans(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    AutoSizeText(
                                      keteranganTugas,
                                      maxFontSize: 16,
                                      maxLines: 3,
                                      style: GoogleFonts.openSans(
                                          fontWeight: FontWeight.w700),
                                    ),
                                    FittedBox(
                                      child: Text(
                                        deadlinePengerjaan,
                                        style: GoogleFonts.openSans(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          //status Tugas
                          Container(
                            width: widthApp * 0.15,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                FittedBox(
                                  child: Text(statusTugas,
                                      style: GoogleFonts.openSans(
                                        color: statusTugas == 'Open'
                                            ? Colors.greenAccent
                                            : Colors.redAccent,
                                        fontWeight: FontWeight.bold,
                                      )),
                                ),
                                statusTugas == 'Open'
                                    ? IconButton(
                                        onPressed: () async {
                                          var files = await FilePicker.platform
                                              .pickFiles(allowMultiple: false);
                                          var file = files.files.first;
                                          if (file.size > 10) {
                                            ubahStatus(index);
                                            ubahNilai(index, 'Grading');
                                          }
                                          // Navigator.push(
                                          //   context,
                                          //   MaterialPageRoute<void>(
                                          //     builder: (context) => OnGoingQuiz(
                                          //       ubahGrading: ubahNilai,
                                          //       indexMapel: index,
                                          //       waktuPengerjaan: lamaPengerjaan,
                                          //     ),
                                          //   ),
                                          // );
                                        },
                                        icon: Icon(Icons.attachment),
                                      )
                                    : //Widget untuk Nilai
                                    Stack(
                                        children: <Widget>[
                                          Container(
                                            height: widthApp * 0.15,
                                            width: widthApp * 0.15,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: nilaiMataPelajaran ==
                                                        'Grading'
                                                    ? Colors.greenAccent
                                                    : nilaiMataPelajaran < 65 !=
                                                            null
                                                        ? Colors.redAccent
                                                        : Colors.blue,
                                                width: 2,
                                                style: BorderStyle.solid,
                                              ),
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(10),
                                                  topRight: Radius.circular(10),
                                                  bottomLeft:
                                                      Radius.circular(10),
                                                  bottomRight:
                                                      Radius.circular(10)),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: nilaiMataPelajaran ==
                                                          'Grading'
                                                      ? Colors.green[100]
                                                          .withOpacity(0.5)
                                                      : nilaiMataPelajaran <
                                                                  65 !=
                                                              null
                                                          ? Colors.red[100]
                                                              .withOpacity(0.5)
                                                          : Colors.blue[100]
                                                              .withOpacity(0.5),
                                                  spreadRadius: 5,
                                                  blurRadius: 7,
                                                  offset: Offset(0, 3),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: widthApp * 0.15,
                                            width: widthApp * 0.15,
                                            child: Center(
                                              child: FittedBox(
                                                child: Text(
                                                  nilaiMataPelajaran.toString(),
                                                  style: nilaiMataPelajaran ==
                                                          'Grading'
                                                      ? GoogleFonts.openSans(
                                                          letterSpacing: 0,
                                                          fontSize: 12,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold)
                                                      : GoogleFonts.pacifico(
                                                          fontSize: 24,
                                                          color:
                                                              nilaiMataPelajaran <
                                                                          65 !=
                                                                      null
                                                                  ? Colors.red
                                                                  : Colors
                                                                      .black,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Avatar Mapel
          // Positioned(
          //   top: (heightApp * 0.25 - widthApp * 0.2) / 2,
          //   child: ClipRRect(
          //     borderRadius: BorderRadius.circular(widthApp * 0.1),
          //     child: Image.asset(
          //       'assets/png/1.png',
          //       fit: BoxFit.fill,
          //       height: widthApp * 0.2,
          //       width: widthApp * 0.2,
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
