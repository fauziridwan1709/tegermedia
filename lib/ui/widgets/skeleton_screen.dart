part of '_widgets.dart';

class SkeletonScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Container(
      height: 110,
      width: width,
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 16 / 2),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[200]),
          color: Colors.white,
          borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SkeletonLine(height: 15, width: width / 3),
            SkeletonLine(height: 10, width: width / 4),
            SizedBox(
              height: 20,
            ),
            SkeletonLine(height: 15, width: width / 2),
          ]),
    );
  }
}

class SkeletonLine extends StatelessWidget {
  final double height;
  final double width;
  final double borderRadius;

  SkeletonLine({this.height = 15, this.width = 50, this.borderRadius = 10});

  @override
  Widget build(BuildContext context) {
    return Shimmer(
      gradient: LinearGradient(stops: [
        0.4,
        0.5,
        0.6
      ], colors: [
        Colors.grey[100],
        Colors.grey[200],
        Colors.grey[100],
      ]),
      child: Container(
          height: height,
          width: width,
          margin: EdgeInsets.only(bottom: 5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(borderRadius),
              color: Colors.grey[100])),
    );
  }
}
