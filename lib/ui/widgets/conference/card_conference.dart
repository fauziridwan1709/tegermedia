part of '_conference.dart';

class CardConference extends StatelessWidget {
  CardConference(
      {this.context,
      this.data,
      this.dateStart,
      this.dateStartEnd,
      this.availability,
      this.width,
      this.onTapOption,
      this.onTap,
      this.onTapShare});

  final BuildContext context;
  final ConferenceListModelData data;
  final DateTime dateStart;
  final DateTime dateStartEnd;
  final Availability availability;
  final double width;
  final Function(BuildContext context, int id, String link) onTapOption;
  final Function(Availability value, String url) onTap;
  final VoidCallback onTapShare;

  @override
  Widget build(BuildContext context) {
    final authState = Injector.getAsReactive<AuthState>();
    final classState = Injector.getAsReactive<ClassState>();
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: width * .03,
        vertical: width * .025,
      ),
      decoration: SiswamediaTheme.whiteRadiusShadow,
      child: InkWell(
        onTap: () => onTap(availability, data.url),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: width * .04,
            vertical: width * .04,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: width * .7,
                        child: RichText(
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          text: TextSpan(
                              text: data.title,
                              style: boldBlack.copyWith(
                                  color: SiswamediaTheme.green,
                                  fontSize: width / 22)),
                        ),
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: () => onTapOption(context, data.id,
                        'Bergabung ke konferensi google meet siswamedia di\n${data.url}'),
                    child: Icon(Icons.more_vert,
                        color: Colors.grey.withOpacity(.6)),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Text(data.description,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.black54,
                      fontSize: width / 30)),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: width * .02, vertical: width * .01),
                    child: Center(
                      child: Text(
                        '${data.duration} mins',
                        style: TextStyle(
                            color: Colors.white, fontSize: width / 36),
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: Color(0xff4B4B4B),
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  SizedBox(width: width * .1),
                  Row(
                    children: [
                      Icon(
                        CustomIcons.kelas,
                        color: SiswamediaTheme.green,
                      ),
                      SizedBox(width: 15),
                      Text(
                          'Kelas ${authState.state.currentState.type == 'PERSONAL' ? authState.state.currentState.schoolName : classState.state.classMap.values.where((element) => element.id == data.classId).isEmpty ? '' : classState.state.classMap.values.where((element) => element.id == data.classId).first.name}',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.black87,
                              fontSize: width / 34)),
                    ],
                  ),
                  WidthSpace(10),
                  // SimpleContainer(
                  //   padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
                  //   radius: BorderRadius.circular(5),
                  //   color: SiswamediaTheme.blue,
                  //   onTap: () async {
                  //     await share(context, data.url, 'share link ke');
                  //   },
                  //   child: Row(
                  //     children: <Widget>[
                  //       Icon(
                  //         Icons.share,
                  //         color: SiswamediaTheme.white,
                  //         size: 18,
                  //       ),
                  //       WidthSpace(5),
                  //       SimpleText(
                  //         'Bagikan',
                  //         style: SiswamediaTextStyle.subtitle,
                  //         fontSize: 12,
                  //         color: SiswamediaTheme.white,
                  //       )
                  //     ],
                  //   ),
                  // )
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      '${dateStart.day} ${bulanFull[dateStart.month - 1]} ${dateStart.year}',
                      style: boldBlack.copyWith(
                          color: Colors.black87, fontSize: width / 24)),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 14, vertical: 5),
                    child: Text(
                      availability.status,
                      style:
                          TextStyle(color: Colors.white, fontSize: width / 34),
                    ),
                    decoration: BoxDecoration(
                      color: availability.isAvailable
                          ? SiswamediaTheme.green
                          : availability.status == 'Berakhir'
                              ? Colors.red
                              : Colors.grey,
                      borderRadius: BorderRadius.circular(6),
                    ),
                  )
                ],
              ),
              Divider(color: Colors.grey),
              Text(
                  'Ruangan Tersedia:  ${dateStart.hour}.${dateStart.minute < 10 ? '0' + dateStart.minute.toString() : dateStart.minute} - ${dateStartEnd.hour}.${dateStartEnd.minute < 10 ? '0' + dateStartEnd.minute.toString() : dateStartEnd.minute}',
                  style: semiBlack.copyWith(
                      fontSize: width / 28, color: Color(0xff707070))),
            ],
          ),
        ),
      ),
    );
  }
}
