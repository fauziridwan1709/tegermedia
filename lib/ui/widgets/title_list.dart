part of '_widgets.dart';

class TitleList extends StatelessWidget {
  final String label;
  final TextStyle style;

  TitleList({@required this.label, this.style});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * .9,
      margin:
          EdgeInsets.symmetric(horizontal: width * .03, vertical: width * .02),
      child: Text(label, style: style),
    );
  }
}
