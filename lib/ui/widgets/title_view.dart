part of '_widgets.dart';

class Titles extends StatelessWidget {
  Titles({this.title, this.callback, this.size, this.subtitle = ''});
  final String title;
  final String subtitle;
  final double size;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    return Container(
      width: S.w * .9,
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * .055, vertical: S.w * .02),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title,
            style: TextStyle(
              color: Colors.black,
              fontSize: size ?? S.w / 25,
              fontWeight: FontWeight.w600,
            ),
          ),
          InkWell(
            onTap: callback,
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                subtitle,
                style:
                    TextStyle(color: SiswamediaTheme.black, fontSize: (size ?? S.w / 25) * 3 / 4),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
