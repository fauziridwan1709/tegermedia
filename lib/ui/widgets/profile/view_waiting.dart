part of '_profile.dart';

class WaitingProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(children: [
      SkeletonLine(
        height: 45,
        width: 45,
        borderRadius: 16,
      ),
      SizedBox(
        width: 16,
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SkeletonLine(
            height: 10,
            width: 100,
            borderRadius: 10,
          ),
          SkeletonLine(
            height: 10,
            width: 80,
            borderRadius: 10,
          ),
        ],
      )
    ]);
  }
}
