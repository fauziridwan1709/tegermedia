part of '_profile.dart';

class ErrorProfileView extends StatelessWidget {
  final dynamic error;
  final Animation<Color> drawerTween;
  ErrorProfileView({this.error, this.drawerTween});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Row(children: [
          InkWell(
              onTap: () {
                navigate(context, ProfilePage());
              },
              child: Hero(
                tag: 'image',
                child: Material(
                  color: Colors.transparent,
                  child: Container(
                      height: AppBar().preferredSize.height * .8,
                      width: AppBar().preferredSize.height * .8,
                      margin: EdgeInsets.only(right: 16),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          image: DecorationImage(
                              image:
                                  AssetImage('assets/profile-default.png')))),
                ),
              )),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                    Injector.getAsReactive<ProfileState>().state.userName ??
                        'Empty',
                    style: descBlack.copyWith(
                        fontWeight: FontWeight.normal,
                        color: drawerTween.value,
                        fontSize: S.w / 26)),
                SizedBox(height: 5),
                Container(
                  width: MediaQuery.of(context).size.width * .55,
                  child: Wrap(
                    children: [
                      Container(
                          margin: EdgeInsets.symmetric(vertical: 3),
                          padding:
                              EdgeInsets.symmetric(horizontal: 17, vertical: 3),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              color: Colors.grey[300].withOpacity(.5)),
                          child: Text(
                              '${error is SocketException ? 'No Internet' : (error as SiswamediaException).message}',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 9)))
                    ],
                  ),
                ),
              ])
        ]),
        Spacer(),
        InkWell(
            onTap: () async {
              await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                      builder: (context) => NotificationPage()));
            },
            child: Center(
                child: Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(shape: BoxShape.circle),
                    child: Stack(children: [
                      Image.asset(
                        'assets/icons/home/notification.png',
                        height: 28,
                        width: 28,
                        // color: drawerTween.value,
                      ),
                      Positioned(right: 0, top: 0, child: SizedBox())
                    ])))),
        SizedBox(width: 5),
        // InkWell(
        //     onTap: () async {
        //       await Navigator.push(context,
        //           MaterialPageRoute<void>(builder: (context) => SwitchRole()));
        //     },
        //     child: Center(
        //         child: Container(
        //             height: 30,
        //             width: 30,
        //             decoration: BoxDecoration(shape: BoxShape.circle),
        //             child: Stack(children: [
        //               Image.asset(
        //                 'assets/icons/profile/change_role.png',
        //                 height: 28,
        //                 width: 28,
        //                 // color: drawerTween.value,
        //               ),
        //             ]))))
      ],
    );
  }
}
