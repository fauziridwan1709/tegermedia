import 'dart:io';

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/aw/widgets/avatar/_avatar.dart';
import 'package:tegarmedia/aw/widgets/custom/_custom.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/states/profile/_profile.dart';
import 'package:tegarmedia/ui/pages/Profile/_profile.dart';
import 'package:tegarmedia/ui/pages/_pages.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'animated_appbar.dart';
part 'view_error.dart';
part 'view_waiting.dart';
