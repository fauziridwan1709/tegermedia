part of '_profile.dart';

class AnimatedAppBar extends StatelessWidget {
  final AnimationController colorAnimationController;
  final Animation<Color> colorTween,
      homeTween,
      workOutTween,
      iconTween,
      drawerTween,
      bottomBorderTween;
  final VoidCallback onLogin;
  final VoidCallback onLoginApple;
  final bool isShowAppbar;
  final String type;
  final bool isWeb;

  AnimatedAppBar({
    @required this.type,
    @required this.colorAnimationController,
    @required this.colorTween,
    @required this.homeTween,
    @required this.iconTween,
    @required this.drawerTween,
    @required this.workOutTween,
    @required this.bottomBorderTween,
    this.isShowAppbar,
    this.onLogin,
    this.onLoginApple,
    this.isWeb,
  });

  final authState = Injector.getAsReactive<AuthState>();
  final profileState = Injector.getAsReactive<ProfileState>();

  @override
  Widget build(BuildContext context) {
    // var orientation = MediaQuery.of(context).orientation;
    // var isPortrait = orientation == Orientation.portrait;
    // var auth = authState.state;
    var notificationState = Injector.getAsReactive<NotificationState>();
    return Container(
      // duration: Duration(milliseconds: 500),
      // height: isShowAppbar
      //     ? (isPortrait
      //         ? AppBar().preferredSize.height +
      //             2 * MediaQuery.of(context).padding.top
      //         : AppBar().preferredSize.height +
      //             MediaQuery.of(context).padding.top)
      //     : 0.0,
      height: AppBar().preferredSize.height + 2 * MediaQuery.of(context).padding.top,
      child: AnimatedBuilder(
        animation: colorAnimationController,
        builder: (context, child) => Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: MediaQuery.of(context).padding.top),
          decoration: BoxDecoration(
              color: colorTween.value,
              border: Border(bottom: BorderSide(color: bottomBorderTween.value))),
          child: StateBuilder<AuthState>(
            // observe: () => authState,
            observeMany: [
              () => authState,
              () => profileState,
            ],
            builder: (_, state) {
              return WhenRebuilder<AuthState>(
                // observe: () => authState,
                observeMany: [
                  () => authState,
                  () => profileState,
                ],
                onWaiting: () => WaitingProfileView(),
                onError: (dynamic error) =>
                    ErrorProfileView(error: error, drawerTween: drawerTween),
                onIdle: () => Row(
                  children: [
                    Spacer(),
                    SizedBox(
                      // height: S.w * .1,
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(S.w),
                            // color: Color(0xff3971A9),
                            color: SiswamediaTheme.green,
                          ),
                          child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  onTap: () {
                                    if (Platform.isAndroid) {
                                      print('login android');
                                      return onLogin();
                                    } else if (Platform.isIOS) {
                                      showModalBottomSheet<void>(
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(20),
                                          ),
                                          context: context,
                                          isScrollControlled: true,
                                          builder: (_) => StatefulBuilder(
                                                builder: (BuildContext _,
                                                        void Function(void Function()) setState) =>
                                                    Container(
                                                  width: S.w,
                                                  padding: EdgeInsets.only(
                                                      left: MediaQuery.of(context).size.width * .05,
                                                      right:
                                                          MediaQuery.of(context).size.width * .05,
                                                      top: S.w * .05),
                                                  height: MediaQuery.of(context).size.height * .9,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      image: DecorationImage(
                                                          image: AssetImage('assets/images/bg.png'),
                                                          fit: BoxFit.cover),
                                                      borderRadius: BorderRadius.only(
                                                          topLeft: const Radius.circular(20.0),
                                                          topRight: const Radius.circular(20.0))),
                                                  child: Column(
                                                    children: [
                                                      Align(
                                                        alignment: Alignment.centerLeft,
                                                        child: IconButton(
                                                            onPressed: () => Navigator.pop(context),
                                                            icon: Icon(Icons.clear),
                                                            iconSize: 26,
                                                            color: SiswamediaTheme.white),
                                                      ),
                                                      SizedBox(
                                                          height:
                                                              MediaQuery.of(context).size.height *
                                                                  .2),
                                                      CustomContainer(
                                                        width: S.w * .7,
                                                        color: SiswamediaTheme.white,
                                                        radius: radius(12),
                                                        padding: EdgeInsets.symmetric(
                                                            vertical: 20, horizontal: 12),
                                                        child: Column(
                                                          mainAxisSize: MainAxisSize.min,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment.center,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment.center,
                                                          children: [
                                                            CustomText('Silahkan Login Dengan',
                                                                Kind.heading3),
                                                            SizedBox(height: 20),
                                                            InkWell(
                                                              onTap: () {
                                                                Navigator.pop(context);
                                                                onLogin();
                                                              },
                                                              child: CustomContainer(
                                                                color: SiswamediaTheme.white,
                                                                border: Border.all(
                                                                    color: SiswamediaTheme.border),
                                                                margin: EdgeInsets.symmetric(
                                                                    horizontal: 10),
                                                                padding: EdgeInsets.symmetric(
                                                                    horizontal: 14, vertical: 6),
                                                                radius: radius(S.w),
                                                                child: Row(
                                                                  children: [
                                                                    Image.asset(
                                                                      'assets/google.png',
                                                                      height: S.w / 14,
                                                                      width: S.w / 14,
                                                                    ),
                                                                    SizedBox(width: 10),
                                                                    CustomText(
                                                                        'Masuk dengan Google',
                                                                        Kind.descBlack)
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(height: 15),
                                                            InkWell(
                                                              onTap: () async {
                                                                onLoginApple();
                                                                await authState.setState((auth) => auth
                                                                    .initializeState()
                                                                    .then((value) async =>
                                                                        await Injector
                                                                                .getAsReactive<
                                                                                    ProfileState>()
                                                                            .setState((s) =>
                                                                                s.retrieveData())));
                                                                await SharedPreferences
                                                                        .getInstance()
                                                                    .then((value) =>
                                                                        value.setBool(FLAG, true));
                                                              },
                                                              child: CustomContainer(
                                                                color: SiswamediaTheme.nearBlack,
                                                                // border: Border.all(
                                                                //     color: SiswamediaTheme
                                                                //         .border),
                                                                margin: EdgeInsets.symmetric(
                                                                    horizontal: 10),
                                                                padding: EdgeInsets.symmetric(
                                                                    horizontal: 14, vertical: 6),
                                                                radius: radius(S.w),
                                                                child: Row(
                                                                  children: [
                                                                    Image.asset(
                                                                      'assets/apple.png',
                                                                      height: S.w / 14,
                                                                      width: S.w / 14,
                                                                    ),
                                                                    SizedBox(width: 10),
                                                                    CustomText('Masuk dengan Apple',
                                                                        Kind.descWhite)
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(height: 20),
                                                            // AppleSignInButton(),
                                                          ],
                                                        ),
                                                      )
                                                      //todo
                                                    ],
                                                  ),
                                                ),
                                              ));
                                      // showDialog<void>(
                                      //     context: context,
                                      //     builder: (context) {
                                      //       return Dialog(
                                      //           shape: RoundedRectangleBorder(
                                      //               borderRadius: radius(12)),
                                      //           child: Padding(
                                      //               padding:
                                      //                   EdgeInsets.symmetric(
                                      //                       vertical: S.w * .1,
                                      //                       horizontal:
                                      //                           S.w * .05),
                                      //               child: Column(
                                      //                 mainAxisSize:
                                      //                     MainAxisSize.min,
                                      //                 crossAxisAlignment:
                                      //                     CrossAxisAlignment
                                      //                         .center,
                                      //                 children: [
                                      //                   Container(
                                      //                       decoration: BoxDecoration(
                                      //                           borderRadius:
                                      //                               radius(6),
                                      //                           color:
                                      //                               SiswamediaTheme
                                      //                                   .blue),
                                      //                       child: Material(
                                      //                         color: Colors
                                      //                             .transparent,
                                      //                         child: InkWell(
                                      //                           onTap: () {
                                      //                             Navigator.pop(
                                      //                                 context);
                                      //                             onLogin();
                                      //                           },
                                      //                           child: Padding(
                                      //                             padding: EdgeInsets
                                      //                                 .symmetric(
                                      //                                     vertical:
                                      //                                         8),
                                      //                             child: Row(
                                      //                               mainAxisAlignment:
                                      //                                   MainAxisAlignment
                                      //                                       .center,
                                      //                               children: [
                                      //                                 Container(
                                      //                                   height:
                                      //                                       S.w /
                                      //                                           14,
                                      //                                   width: S.w /
                                      //                                       14,
                                      //                                   decoration: BoxDecoration(
                                      //                                       shape:
                                      //                                           BoxShape.circle,
                                      //                                       color: Colors.white,
                                      //                                       image: DecorationImage(image: AssetImage('assets/google.png'))),
                                      //                                 ),
                                      //                                 SizedBox(
                                      //                                     width:
                                      //                                         10),
                                      //                                 Text(
                                      //                                   'Sign in with Google',
                                      //                                   style: descWhite.copyWith(
                                      //                                       fontSize:
                                      //                                           S.w / 22),
                                      //                                 ),
                                      //                               ],
                                      //                             ),
                                      //                           ),
                                      //                         ),
                                      //                       )),
                                      //                   SizedBox(height: 25),
                                      //                   SignInWithAppleButton(
                                      //                     onPressed: () async {
                                      //                       final credential =
                                      //                           await SignInWithApple
                                      //                               .getAppleIDCredential(
                                      //                         scopes: [
                                      //                           AppleIDAuthorizationScopes
                                      //                               .email,
                                      //                           AppleIDAuthorizationScopes
                                      //                               .fullName,
                                      //                         ],
                                      //                         webAuthenticationOptions:
                                      //                             WebAuthenticationOptions(
                                      //                           // TODO: Set the `clientId` and `redirectUri` arguments to the values you entered in the Apple Developer portal during the setup
                                      //                           clientId:
                                      //                               'com.aboutyou.dart_packages.sign_in_with_apple.example',
                                      //                           redirectUri:
                                      //                               Uri.parse(
                                      //                             'https://flutter-sign-in-with-apple-example.glitch.me/callbacks/sign_in_with_apple',
                                      //                           ),
                                      //                         ),
                                      //                         // TODO: Remove these if you have no need for them
                                      //                         nonce:
                                      //                             'example-nonce',
                                      //                         state:
                                      //                             'example-state',
                                      //                       );
                                      //
                                      //                       print(credential);
                                      //
                                      //                       // This is the endpoint that will convert an authorization code obtained
                                      //                       // via Sign in with Apple into a session in your system
                                      //                       final signInWithAppleEndpoint =
                                      //                           Uri(
                                      //                         scheme: 'https',
                                      //                         host:
                                      //                             'flutter-sign-in-with-apple-example.glitch.me',
                                      //                         path:
                                      //                             '/sign_in_with_apple',
                                      //                         queryParameters: <
                                      //                             String,
                                      //                             String>{
                                      //                           'code': credential
                                      //                               .authorizationCode,
                                      //                           'firstName':
                                      //                               credential
                                      //                                   .givenName,
                                      //                           'lastName':
                                      //                               credential
                                      //                                   .familyName,
                                      //                           'useBundleId': Platform
                                      //                                       .isIOS ||
                                      //                                   Platform
                                      //                                       .isMacOS
                                      //                               ? 'true'
                                      //                               : 'false',
                                      //                           if (credential
                                      //                                   .state !=
                                      //                               null)
                                      //                             'state':
                                      //                                 credential
                                      //                                     .state,
                                      //                         },
                                      //                       );
                                      //
                                      //                       final session =
                                      //                           await http
                                      //                                   .Client()
                                      //                               .post(
                                      //                         signInWithAppleEndpoint,
                                      //                       );
                                      //
                                      //                       // If we got this far, a session based on the Apple ID credential has been created in your system,
                                      //                       // and you can now set this as the app's session
                                      //                       print(session);
                                      //                     },
                                      //                   ),
                                      //                 ],
                                      //               )));
                                      //     });
                                    }
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 14, vertical: 5),
                                    child: Row(children: [
                                      if (Platform.isAndroid)
                                        // if (Platform.isAndroid)
                                        Container(
                                          width: S.w / 14,
                                          height: S.w / 14,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.white,
                                              image: DecorationImage(
                                                  image: AssetImage('assets/google.png'))),
                                        )
                                      else
                                        Icon(Icons.portrait_rounded, color: SiswamediaTheme.white),
                                      SizedBox(width: 10),
                                      Text(
                                        '${Platform.isAndroid ? 'Masuk/Daftar Dengan Google' : 'Masuk/Daftar'}',
                                        style: descWhite.copyWith(fontSize: S.w / 32),
                                      ),
                                      SizedBox(width: 10),
                                    ]),
                                  )))),
                    ),
                  ],
                ),
                onData: (data) {
                  Logger().d('--- FLAG ONDATA ---');
                  if (!data.isLogin) {
                    return Row(
                      children: [
                        Spacer(),
                        SizedBox(
                          // height: S.w * .1,
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(S.w),
                                color: Color(0xff3971A9),
                              ),
                              child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                      onTap: () {
                                        if (Platform.isAndroid) {
                                          print('login android');
                                          return onLogin();
                                        } else if (Platform.isIOS) {
                                          showModalBottomSheet<void>(
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(20),
                                              ),
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (_) => StatefulBuilder(
                                                    builder: (BuildContext _,
                                                            void Function(void Function())
                                                                setState) =>
                                                        Container(
                                                      width: S.w,
                                                      padding: EdgeInsets.only(
                                                          left: MediaQuery.of(context).size.width *
                                                              .05,
                                                          right: MediaQuery.of(context).size.width *
                                                              .05,
                                                          top: S.w * .05),
                                                      height:
                                                          MediaQuery.of(context).size.height * .9,
                                                      decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          image: DecorationImage(
                                                              image: AssetImage(
                                                                  'assets/images/bg.png'),
                                                              fit: BoxFit.cover),
                                                          borderRadius: BorderRadius.only(
                                                              topLeft: const Radius.circular(20.0),
                                                              topRight:
                                                                  const Radius.circular(20.0))),
                                                      child: Column(
                                                        children: [
                                                          Align(
                                                            alignment: Alignment.centerLeft,
                                                            child: IconButton(
                                                                onPressed: () =>
                                                                    Navigator.pop(context),
                                                                icon: Icon(Icons.clear),
                                                                iconSize: 26,
                                                                color: SiswamediaTheme.white),
                                                          ),
                                                          SizedBox(
                                                              height: MediaQuery.of(context)
                                                                      .size
                                                                      .height *
                                                                  .2),
                                                          CustomContainer(
                                                            width: S.w * .7,
                                                            color: SiswamediaTheme.white,
                                                            radius: radius(12),
                                                            padding: EdgeInsets.symmetric(
                                                                vertical: 20, horizontal: 12),
                                                            child: Column(
                                                              mainAxisSize: MainAxisSize.min,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment.center,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment.center,
                                                              children: [
                                                                CustomText('Silahkan Login Dengan',
                                                                    Kind.heading3),
                                                                SizedBox(height: 20),
                                                                InkWell(
                                                                  onTap: () {
                                                                    Navigator.pop(context);
                                                                    onLogin();
                                                                  },
                                                                  child: CustomContainer(
                                                                    color: SiswamediaTheme.white,
                                                                    border: Border.all(
                                                                        color:
                                                                            SiswamediaTheme.border),
                                                                    margin: EdgeInsets.symmetric(
                                                                        horizontal: 10),
                                                                    padding: EdgeInsets.symmetric(
                                                                        horizontal: 14,
                                                                        vertical: 6),
                                                                    radius: radius(S.w),
                                                                    child: Row(
                                                                      children: [
                                                                        Image.asset(
                                                                          'assets/google.png',
                                                                          height: S.w / 14,
                                                                          width: S.w / 14,
                                                                        ),
                                                                        SizedBox(width: 10),
                                                                        CustomText(
                                                                            'Masuk dengan Google',
                                                                            Kind.descBlack)
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(height: 15),
                                                                InkWell(
                                                                  onTap: () async {
                                                                    //todo
                                                                    // final credential =
                                                                    await SignInWithApple
                                                                        .getAppleIDCredential(
                                                                      scopes: [
                                                                        AppleIDAuthorizationScopes
                                                                            .email,
                                                                        AppleIDAuthorizationScopes
                                                                            .fullName,
                                                                      ],
                                                                    );
                                                                  },
                                                                  child: CustomContainer(
                                                                    color:
                                                                        SiswamediaTheme.nearBlack,
                                                                    // border: Border.all(
                                                                    //     color: SiswamediaTheme
                                                                    //         .border),
                                                                    margin: EdgeInsets.symmetric(
                                                                        horizontal: 10),
                                                                    padding: EdgeInsets.symmetric(
                                                                        horizontal: 14,
                                                                        vertical: 6),
                                                                    radius: radius(S.w),
                                                                    child: Row(
                                                                      children: [
                                                                        Image.asset(
                                                                          'assets/apple.png',
                                                                          height: S.w / 14,
                                                                          width: S.w / 14,
                                                                        ),
                                                                        SizedBox(width: 10),
                                                                        CustomText(
                                                                            'Masuk dengan Apple',
                                                                            Kind.descWhite)
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(height: 20),
                                                                // AppleSignInButton(),
                                                              ],
                                                            ),
                                                          )
                                                          //todo
                                                        ],
                                                      ),
                                                    ),
                                                  ));
                                          // showDialog<void>(
                                          //     context: context,
                                          //     builder: (context) {
                                          //       return Dialog(
                                          //           shape: RoundedRectangleBorder(
                                          //               borderRadius: radius(12)),
                                          //           child: Padding(
                                          //               padding:
                                          //                   EdgeInsets.symmetric(
                                          //                       vertical: S.w * .1,
                                          //                       horizontal:
                                          //                           S.w * .05),
                                          //               child: Column(
                                          //                 mainAxisSize:
                                          //                     MainAxisSize.min,
                                          //                 crossAxisAlignment:
                                          //                     CrossAxisAlignment
                                          //                         .center,
                                          //                 children: [
                                          //                   Container(
                                          //                       decoration: BoxDecoration(
                                          //                           borderRadius:
                                          //                               radius(6),
                                          //                           color:
                                          //                               SiswamediaTheme
                                          //                                   .blue),
                                          //                       child: Material(
                                          //                         color: Colors
                                          //                             .transparent,
                                          //                         child: InkWell(
                                          //                           onTap: () {
                                          //                             Navigator.pop(
                                          //                                 context);
                                          //                             onLogin();
                                          //                           },
                                          //                           child: Padding(
                                          //                             padding: EdgeInsets
                                          //                                 .symmetric(
                                          //                                     vertical:
                                          //                                         8),
                                          //                             child: Row(
                                          //                               mainAxisAlignment:
                                          //                                   MainAxisAlignment
                                          //                                       .center,
                                          //                               children: [
                                          //                                 Container(
                                          //                                   height:
                                          //                                       S.w /
                                          //                                           14,
                                          //                                   width: S.w /
                                          //                                       14,
                                          //                                   decoration: BoxDecoration(
                                          //                                       shape:
                                          //                                           BoxShape.circle,
                                          //                                       color: Colors.white,
                                          //                                       image: DecorationImage(image: AssetImage('assets/google.png'))),
                                          //                                 ),
                                          //                                 SizedBox(
                                          //                                     width:
                                          //                                         10),
                                          //                                 Text(
                                          //                                   'Sign in with Google',
                                          //                                   style: descWhite.copyWith(
                                          //                                       fontSize:
                                          //                                           S.w / 22),
                                          //                                 ),
                                          //                               ],
                                          //                             ),
                                          //                           ),
                                          //                         ),
                                          //                       )),
                                          //                   SizedBox(height: 25),
                                          //                   SignInWithAppleButton(
                                          //                     onPressed: () async {
                                          //                       final credential =
                                          //                           await SignInWithApple
                                          //                               .getAppleIDCredential(
                                          //                         scopes: [
                                          //                           AppleIDAuthorizationScopes
                                          //                               .email,
                                          //                           AppleIDAuthorizationScopes
                                          //                               .fullName,
                                          //                         ],
                                          //                         webAuthenticationOptions:
                                          //                             WebAuthenticationOptions(
                                          //                           // TODO: Set the `clientId` and `redirectUri` arguments to the values you entered in the Apple Developer portal during the setup
                                          //                           clientId:
                                          //                               'com.aboutyou.dart_packages.sign_in_with_apple.example',
                                          //                           redirectUri:
                                          //                               Uri.parse(
                                          //                             'https://flutter-sign-in-with-apple-example.glitch.me/callbacks/sign_in_with_apple',
                                          //                           ),
                                          //                         ),
                                          //                         // TODO: Remove these if you have no need for them
                                          //                         nonce:
                                          //                             'example-nonce',
                                          //                         state:
                                          //                             'example-state',
                                          //                       );
                                          //
                                          //                       print(credential);
                                          //
                                          //                       // This is the endpoint that will convert an authorization code obtained
                                          //                       // via Sign in with Apple into a session in your system
                                          //                       final signInWithAppleEndpoint =
                                          //                           Uri(
                                          //                         scheme: 'https',
                                          //                         host:
                                          //                             'flutter-sign-in-with-apple-example.glitch.me',
                                          //                         path:
                                          //                             '/sign_in_with_apple',
                                          //                         queryParameters: <
                                          //                             String,
                                          //                             String>{
                                          //                           'code': credential
                                          //                               .authorizationCode,
                                          //                           'firstName':
                                          //                               credential
                                          //                                   .givenName,
                                          //                           'lastName':
                                          //                               credential
                                          //                                   .familyName,
                                          //                           'useBundleId': Platform
                                          //                                       .isIOS ||
                                          //                                   Platform
                                          //                                       .isMacOS
                                          //                               ? 'true'
                                          //                               : 'false',
                                          //                           if (credential
                                          //                                   .state !=
                                          //                               null)
                                          //                             'state':
                                          //                                 credential
                                          //                                     .state,
                                          //                         },
                                          //                       );
                                          //
                                          //                       final session =
                                          //                           await http
                                          //                                   .Client()
                                          //                               .post(
                                          //                         signInWithAppleEndpoint,
                                          //                       );
                                          //
                                          //                       // If we got this far, a session based on the Apple ID credential has been created in your system,
                                          //                       // and you can now set this as the app's session
                                          //                       print(session);
                                          //                     },
                                          //                   ),
                                          //                 ],
                                          //               )));
                                          //     });
                                        }
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 14, vertical: 5),
                                        child: Row(children: [
                                          if (Platform.isAndroid)
                                            // if (Platform.isAndroid)
                                            Container(
                                              width: S.w / 14,
                                              height: S.w / 14,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: Colors.white,
                                                  image: DecorationImage(
                                                      image: AssetImage('assets/google.png'))),
                                            )
                                          else
                                            Icon(Icons.portrait_rounded,
                                                color: SiswamediaTheme.white),
                                          SizedBox(width: 10),
                                          Text(
                                            '${Platform.isAndroid ? 'Masuk/Daftar Dengan Google' : 'Masuk/Daftar'}',
                                            style: descWhite.copyWith(fontSize: S.w / 32),
                                          ),
                                          SizedBox(width: 10),
                                        ]),
                                      )))),
                        ),
                      ],
                    );
                  }
                  return Row(
                    children: [
                      Row(children: [
                        InkWell(
                          onTap: () {
                            print('ke profile');
                            navigate(context, ProfilePage());
                          },
                          child: AvatarPP(
                            tag: 'image',
                            height: AppBar().preferredSize.height * .8,
                            width: AppBar().preferredSize.height * .8,
                          ),
                        ),
                        WidthSpace(16),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(profileState.state.profile.name,
                                  style: descBlack.copyWith(
                                      fontWeight: FontWeight.normal,
                                      color: drawerTween.value,
                                      fontSize: S.w / 26)),
                              SizedBox(height: 5),
                              Container(
                                width: MediaQuery.of(context).size.width * .55,
                                child: Wrap(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.symmetric(vertical: 3),
                                      padding: EdgeInsets.symmetric(horizontal: 17, vertical: 3),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(200),
                                          color: data.currentState.schoolName == null
                                              ? Colors.grey[300].withOpacity(.5)
                                              : workOutTween.value),
                                      child: data.currentState.schoolName == null
                                          ? Text(
                                              'Belum pilih/punya sekolah',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              softWrap: false,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500, fontSize: 9),
                                            )
                                          : Text(
                                              data.currentState.schoolName ?? '',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 9,
                                                  color: homeTween.value),
                                            ),
                                    ),
                                    SizedBox(width: 5),
                                    Container(
                                      margin: EdgeInsets.symmetric(vertical: 3),
                                      padding: EdgeInsets.symmetric(horizontal: 17, vertical: 3),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(200),
                                          color: data.currentState.schoolRole.isEmpty
                                              ? Colors.grey[300].withOpacity(.5)
                                              : workOutTween.value),
                                      child: data.currentState.schoolRole.isEmpty
                                          ? Text('Belum ada role',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500, fontSize: 9))
                                          : Text(
                                              data.currentState.schoolRole.join(', '),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 9,
                                                  color: homeTween.value),
                                            ),
                                    )
                                  ],
                                ),
                              ),
                            ])
                      ]),
                      Spacer(),
                      InkWell(
                          onTap: () async => await navigate(context, NotificationPage()),
                          child: Center(
                              child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(shape: BoxShape.circle),
                                  child: Stack(children: [
                                    Image.asset(
                                      'assets/images/asset_home/notif.png',
                                      height: 28,
                                      width: 28,
                                      // color: drawerTween.value,
                                    ),
                                    if ((notificationState.state.count ?? 0) > 1)
                                      Positioned(
                                          right: 0,
                                          top: 0,
                                          child: Container(
                                              width: 15,
                                              height: 15,
                                              decoration: BoxDecoration(
                                                  color: Colors.red, shape: BoxShape.circle),
                                              child: Padding(
                                                  padding: EdgeInsets.all(1),
                                                  child: Center(
                                                      child: Text(
                                                          '${notificationState.state.count}',
                                                          style: TextStyle(
                                                              fontSize: 8, color: Colors.white))))))
                                  ])))),
                      SizedBox(width: 5),
                      //TODO
                      // InkWell(
                      //     onTap: () async {
                      //       await Navigator.push(
                      //           context,
                      //           MaterialPageRoute<void>(
                      //               builder: (context) =>
                      //                   SwitchRole()));
                      //     },
                      //     child: Center(
                      //         child: Container(
                      //             height: 30,
                      //             width: 30,
                      //             decoration: BoxDecoration(
                      //                 shape: BoxShape.circle),
                      //             child: Stack(children: [
                      //               Image.asset(
                      //                 'assets/icons/profile/change_role.png',
                      //                 height: 28,
                      //                 width: 28,
                      //                 // color: drawerTween.value,
                      //               ),
                      //             ]))))
                    ],
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
