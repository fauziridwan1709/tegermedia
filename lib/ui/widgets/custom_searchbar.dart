part of '_widgets.dart';

class CustomSearchBar extends StatelessWidget {
  CustomSearchBar(
      {Key key,
      @required this.controller,
      this.callback,
      this.doaData,
      this.booleanCallback,
      this.fNode})
      : super(key: key);

  final TextEditingController controller;
  final Function(String) callback;
  final VoidCallback booleanCallback;
  final List<dynamic> doaData;
  final FocusNode fNode;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: SiswamediaTheme.white,
            borderRadius: BorderRadius.all(Radius.circular(200)),
            boxShadow: [
              BoxShadow(
                  color: SiswamediaTheme.background,
                  offset: Offset(0, 0),
                  spreadRadius: 3,
                  blurRadius: 3)
            ]),
        child: TextField(
          onTap: booleanCallback,
          controller: controller,
          onChanged: callback,
          focusNode: fNode,
          style: TextStyle(fontSize: 16),
          decoration: InputDecoration(
            hintText: 'Cari...',
            border: InputBorder.none,
            prefixIcon: Icon(
              Icons.search,
            ),
          ),
        ),
      ),
    );
  }
}
