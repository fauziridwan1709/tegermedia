part of '_widgets.dart';

class ButtonTerapkan extends StatelessWidget {
  ButtonTerapkan({Key key, this.title = 'Terapkan', this.callback})
      : super(key: key);
  final VoidCallback callback;
  final String title;

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    return Container(
      height: S.w * .1,
      width: S.w * .3,
      decoration: BoxDecoration(
        color: SiswamediaTheme.green,
        borderRadius: BorderRadius.circular(S.w),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: callback,
          child: Center(
            child: Text(
              title,
              style: TextStyle(color: Colors.white, fontSize: S.w / 25.7),
            ),
          ),
        ),
      ),
    );
  }
}
