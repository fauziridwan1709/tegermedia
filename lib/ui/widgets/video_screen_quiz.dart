part of '_widgets.dart';

class VideoScreenQuiz extends StatefulWidget {
  final String url;

  VideoScreenQuiz({this.url});

  @override
  _VideoScreenQuizState createState() => _VideoScreenQuizState();
}

class _VideoScreenQuizState extends State<VideoScreenQuiz> {
  YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    String videoId;
    videoId = YoutubePlayer.convertUrlToId(widget.url);
    print(videoId);

    _controller = YoutubePlayerController(
      initialVideoId: videoId,
      flags: YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
        return Future.value(true);
      },
      child: YoutubePlayer(
        controller: _controller,
        showVideoProgressIndicator: true,
        onReady: () {
          print('Player is ready.');
        },
      ),
    );
  }
}
