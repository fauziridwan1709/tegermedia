part of '_core.dart';

Future<void> popCreate(BuildContext context, String info) {
  return showDialog<void>(
      context: context,
      builder: (context) => Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          child: AspectRatio(
              aspectRatio: 1.8,
              child: Container(
                padding: EdgeInsets.all(S.w * .05),
                child: Column(children: [
                  Text('Lengkapi Data',
                      style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                  SizedBox(height: 15),
                  Text(info, style: descBlack.copyWith(fontSize: S.w / 34)),
                  SizedBox(height: 20),
                  Container(
                    height: S.w / 10,
                    width: S.w / 3.5,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Color(0xff4B4B4B),
                    ),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        //Navigator.push(context,
                        //  MaterialPageRoute<void>(builder: (_) => null));
                      },
                      child: Center(
                        child: Text('Close',
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ),
                ]),
              ))));
}
