part of '_core.dart';

//todo change with fadeInImage
class CachedImage extends Container {
  CachedImage(
    String image, {
    double height,
    double width,
    Color backgroundColor,
    BorderRadius borderRadius,
    EdgeInsetsGeometry margin,
    Widget child,
    BoxFit fit,
    Alignment alignment,
    bool isCached,
  })  : assert(image != null),
        super(
          // image: isCached ? FileImage(File(image)) : NetworkImage(image),
          height: height,
          width: width,
          // placeholder: ,
          margin: margin,
          child: child,
          alignment: alignment,
          decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: borderRadius,
              image: DecorationImage(
                  image: isCached
                      ? FileImage(File(image))
                      : NetworkImage(image, scale: .1),
                  fit: fit)),
        );
}
