part of '_core.dart';

class AppBarUI extends StatefulWidget {
  final VoidCallback onLogin;
  final Function onTapNotif;
  final String title;
  final String tag;
  final bool isPayment;

  AppBarUI({@required this.onLogin, this.onTapNotif, this.title, this.tag, this.isPayment = false});

  @override
  _AppBarUIState createState() => _AppBarUIState();
}

class _AppBarUIState extends State<AppBarUI> {
  final notificationState = GlobalState.notification();
  final authState = GlobalState.auth();

  @override
  void initState() {
    super.initState();
    getDataCount();
  }

  void getDataCount() async {
    if (authState.state.isLogin) {
      await notificationState.setState((s) => s.getCount());
    }
  }

  var profileState = Injector.getAsReactive<ProfileState>();

  @override
  Widget build(BuildContext context) {
    var auth = authState.state;
    return Container(
      height: AppBar().preferredSize.height * 1.5,
      padding: EdgeInsets.symmetric(horizontal: 20),
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      decoration: BoxDecoration(
          color: Colors.white, border: Border(bottom: BorderSide(color: Colors.grey[300]))),
      child: StateBuilder<AuthState>(
        observeMany: [
          () => authState,
          () => profileState,
        ],
        builder: (_, snapshot) {
          return WhenRebuilder<AuthState>(
              observeMany: [
                () => authState,
                () => profileState,
              ],
              onIdle: _waitingView,
              onWaiting: _waitingView,
              onError: _errorView,
              onData: (data) {
                if (!data.isLogin) {
                  return Row(
                    children: [
                      Spacer(),
                      SizedBox(
                        width: S.w * .6,
                        child: AspectRatio(
                          aspectRatio: 6.5,
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(S.w),
                                color: Color(0xff3971A9),
                              ),
                              child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                      onTap: widget.onLogin,
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                        child: Row(children: [
                                          Container(
                                            width: S.w / 14,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white,
                                                image: DecorationImage(
                                                    image: AssetImage('assets/google.png'))),
                                          ),
                                          SizedBox(width: 10),
                                          Text(
                                            'Masuk/Daftar Dengan Google',
                                            style: descWhite.copyWith(fontSize: S.w / 32),
                                          ),
                                        ]),
                                      )))),
                        ),
                      ),
                    ],
                  );
                }
                return Row(
                  children: [
                    Row(children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                builder: (_) => ProfilePage(),
                              ),
                            );
                          },
                          child: AvatarPP(
                            tag: 'image',
                            height: AppBar().preferredSize.height * .8,
                            width: AppBar().preferredSize.height * .8,
                          )),
                      WidthSpace(16),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Hero(
                              tag: 'fullname',
                              child: Material(
                                color: Colors.transparent,
                                child: Text(profileState.state.profile.name,
                                    style: descBlack.copyWith(
                                        fontWeight: FontWeight.normal,
                                        color: Colors.black,
                                        fontSize: S.w / 26)),
                              ),
                            ),
                            SizedBox(height: 6),
                            Row(children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 17, vertical: 3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: authState.state.currentState.schoolName == null
                                        ? Colors.grey[300]
                                        : SiswamediaTheme.green),
                                child: auth.currentState.schoolName == null
                                    ? Text(
                                        'Belum pilih/punya sekolah',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: false,
                                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 9),
                                      )
                                    : Text(
                                        auth.currentState.schoolName,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 9,
                                            color: Colors.white),
                                      ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                padding: EdgeInsets.symmetric(horizontal: 17, vertical: 3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: auth.currentState.schoolRole.isEmpty
                                        ? Colors.grey[300]
                                        : SiswamediaTheme.green),
                                child: auth.currentState.schoolRole.isEmpty
                                    ? Text('No role',
                                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 9))
                                    : Text(
                                        auth.currentState.schoolRole.join(', '),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 9,
                                            color: Colors.white),
                                      ),
                              )
                            ]),
                          ])
                    ]),
                    Spacer(),
                    if (widget.isPayment) ...[
                      InkWell(
                          onTap: () async => navigate(context, PaymentHistoryPage()),
                          child: Center(
                              child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(shape: BoxShape.circle),
                                  child: Stack(children: [
                                    SVGImage(
                                      'payment_history.svg',
                                      height: 24,
                                      width: 24,
                                      color: SiswamediaTheme.green,
                                    ),
                                    // _buildNotification(),
                                  ]))))
                    ] else ...[
                      InkWell(
                          onTap: () async => navigate(context, NotificationPage()),
                          child: Center(
                              child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(shape: BoxShape.circle),
                                  child: Stack(children: [
                                    Icon(
                                      Icons.notifications_none,
                                      color: Colors.black,
                                      size: 28,
                                    ),
                                    _buildNotification(),
                                  ]))))
                    ]
                  ],
                );
              });
        },
      ),
    );
  }

  Widget _buildNotification() {
    if ((notificationState.state.count ?? 0) > 1) {
      return Positioned(
          right: 0,
          top: 0,
          child: Container(
              width: 15,
              height: 15,
              decoration: BoxDecoration(color: Colors.red, shape: BoxShape.circle),
              child: Padding(
                  padding: EdgeInsets.all(1),
                  child: Center(
                      child: Text('${notificationState.state.count}',
                          style: TextStyle(fontSize: 8, color: Colors.white))))));
    }
    return SizedBox();
  }

  Widget _waitingView() {
    return Row(children: [
      SkeletonLine(
        height: 45,
        width: 45,
        borderRadius: 16,
      ),
      SizedBox(
        width: 16,
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SkeletonLine(
            height: 10,
            width: 100,
            borderRadius: 10,
          ),
          SkeletonLine(
            height: 10,
            width: 80,
            borderRadius: 10,
          ),
        ],
      )
    ]);
  }

  Widget _errorView(dynamic error) {
    return Row(children: [
      GestureDetector(
          onTap: () => navigate(context, ProfilePage()),
          child: Hero(
            tag: 'image',
            child: Material(
              color: Colors.transparent,
              child: Container(
                  width: AppBar().preferredSize.height * .8,
                  height: AppBar().preferredSize.height * .8,
                  margin: EdgeInsets.only(right: 16),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      image: DecorationImage(
                          image: AssetImage(
                        'assets/profile-default.png',
                      )))),
            ),
          )),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Hero(
              tag: 'fullname',
              child: Material(
                color: Colors.transparent,
                child: Text(profileState.state.userName,
                    style: descBlack.copyWith(fontWeight: FontWeight.normal, fontSize: S.w / 24)),
              ),
            ),
            SizedBox(height: 6),
            Row(children: [
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 17, vertical: 3),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5), color: Colors.grey[300]),
                  child: Text(
                    '${error is SocketException ? 'No Internet' : (error as SiswamediaException).message}',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 9),
                  )),
            ]),
          ])
    ]);
  }
}

class SiswamediaAppBar extends AppBar {
  SiswamediaAppBar({
    Key key,
    String title,
    BuildContext context,
    List<Widget> actions,
    IconData icon = Icons.arrow_back,
    Function additionalFunction,
    Function onBackFunction,
    PreferredSizeWidget bottom,
  }) : super(
            key: key,
            title: Text(
              title,
              style: semiBlack.copyWith(fontSize: 22),
            ),
            leading: IconButton(
                icon: Icon(icon),
                color: SiswamediaTheme.green,
                onPressed: () {
                  if (onBackFunction == null) {
                    if (additionalFunction != null) additionalFunction();
                    Navigator.pop(context);
                  } else {
                    onBackFunction();
                  }
                }),
            actions: actions,
            brightness: Brightness.light,
            centerTitle: true,
            backgroundColor: Colors.white,
            shadowColor: Colors.grey.withOpacity(.2),
            bottom: bottom);
}
