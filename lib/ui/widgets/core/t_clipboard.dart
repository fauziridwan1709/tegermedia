part of '_core.dart';

enum ClipboardButtonCopyPosition { leading, trailing }

class TextClipboard extends StatelessWidget {
  final String text;

  ///using nullsafety
  final Widget contentButton;
  final Function(String text) onCopy;
  TextClipboard(this.text, {this.onCopy, this.contentButton});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          fit: FlexFit.tight,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
                color: Color(0xFFEFEFEF),
                borderRadius: BorderRadius.circular(3),
                border: Border.all(color: Color(0xFFECECEC))),
            child: AutoSizeText(text,
                maxLines: 1, style: TextStyle(color: SiswamediaTheme.green)),
          ),
        ),
        InkWell(
            onTap: () => onCopy(text),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              decoration: BoxDecoration(
                  color: SiswamediaTheme.green,
                  borderRadius: BorderRadius.circular(3)),
              child: contentButton ??
                  Text('salin', style: descWhite.copyWith(fontSize: S.w / 29)),
            )),
      ],
    );
  }
}
