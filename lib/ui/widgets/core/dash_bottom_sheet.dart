part of '_core.dart';

///indicator scrollable bottom sheet
class DashTopBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 5,
        width: 50,
        margin: EdgeInsets.only(bottom: 15),
        decoration: BoxDecoration(
          color: SiswamediaTheme.lightBlack,
          borderRadius: radius(200),
        ));
  }
}
