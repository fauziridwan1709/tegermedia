part of '_core.dart';

class ProgressDialog {
  BuildContext context;
  String message;
  ReactiveModel<ProgressMessageState> observer;

  ProgressDialog({this.message, this.context, this.observer});

  void infoLoading() {
    showDialog<void>(
        barrierDismissible: false,
        context: context,
        builder: (context) => StateBuilder<ProgressMessageState>(
            observe: () => observer,
            builder: (_, snapshot) {
              return WillPopScope(
                onWillPop: () async => false,
                child: Dialog(
                    shape: RoundedRectangleBorder(borderRadius: radius(16)),
                    child: Container(
                      width: S.w * .9,
                      padding: EdgeInsets.symmetric(
                          vertical: S.w * .05, horizontal: S.w * .05),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Image.asset(
                            'assets/kelas/Jadwal.png',
                            height: S.w * .4,
                            width: S.w * .4,
                          ),
                          CustomText(
                              observer.state.message ?? '', Kind.heading3),
                          SizedBox(height: 20),
                          Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: S.w * .15),
                              child: SpinKitSquareCircle(
                                color: SiswamediaTheme.green,
                                size: 30,
                              )),
                        ],
                      ),
                    )),
              );
            }));
  }

  ///message and progress
  void update(String m) {
    message = m;
  }

  String getMessage() {
    return this.message;
  }
}
