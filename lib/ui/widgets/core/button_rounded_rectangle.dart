part of '_core.dart';

class RoundedRectangleButton extends StatelessWidget {
  final VoidCallback onTap;
  final Color backgroundColor;
  final Color fontColor;
  final String title;

  RoundedRectangleButton(
      {this.onTap, this.backgroundColor, this.fontColor, this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: S.w * .9,
      decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: radius(12),
          boxShadow: [SiswamediaTheme.shadowContainer]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          child: Center(
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                title,
                style: semiBlack.copyWith(fontSize: S.w / 26, color: fontColor),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
