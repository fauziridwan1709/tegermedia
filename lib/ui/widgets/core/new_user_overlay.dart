part of '_core.dart';

class NewUserOverlay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
            child: Center(
                child: RaisedButton(
                    onPressed: () {
                      ActionOverlay.noSchoolOverlay(
                        context,
                      );
                    },
                    child: CustomText('here', Kind.descBlack)))));
  }
}

class ActionOverlay {
  static Duration normalDuration = Duration(milliseconds: 750);
  static var data = <String>['SISWA', 'GURU', 'ORTU'];
  static var errorMessage = '';
  static void noSchoolOverlay(
    BuildContext context,
  ) {
    var controller = TextEditingController();
    showGeneralDialog<Widget>(
        pageBuilder: (context, anim1, anim2) {
          return;
        },
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.4),
        barrierLabel: '',
        context: context,
        transitionDuration: normalDuration,
        transitionBuilder: (context, anim1, anim2, child) {
          final curvedValue = Curves.easeOutBack.transform(anim1.value);
          return Transform.scale(
            scale: curvedValue,
            child: Dialog(
              // title: Text('Wybierz miasto'),
              shape: RoundedRectangleBorder(
                borderRadius: radius(12),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: S.w * .05, vertical: S.w * .045),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    CustomText(
                      'Gabung Tempat Belajar Sekarang',
                      Kind.heading1,
                      align: TextAlign.center,
                    ),
                    SizedBox(height: S.w * .025),
                    CustomText(
                        'Isi code yang sudah di berikan, jika kamu ingin bergabung menjadi guru atau siswa',
                        Kind.descBlack32,
                        align: TextAlign.center),
                    SizedBox(height: S.w * .025),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: S.w * .4,
                          height: S.w * .1,
                          child: TextField(
                            controller: controller,
                            style: normalText(context),
                            decoration: InputDecoration(
                              fillColor: Color(0xfff6f6f6),
                              hintText: 'kode',
                              hintStyle: hintText(context),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  borderSide: BorderSide(
                                      color: SiswamediaTheme.border)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  borderSide:
                                      BorderSide(color: SiswamediaTheme.green)),
                            ),
                          ),
                        ),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: radius(8),
                              color: SiswamediaTheme.green,
                            ),
                            height: S.w * .1,
                            child: Material(
                                color: Colors.transparent,
                                child: InkWell(
                                    onTap: () {
                                      //TODO stuff here

                                      Navigator.pop(context);
                                      chooseRole(
                                        context,
                                        controller.text,
                                      );
                                      // classOrSchool(context, controller.text);
                                    },
                                    child: Center(
                                        child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: S.w * .05),
                                      child: CustomText('Join', Kind.descWhite),
                                    ))))),
                      ],
                    ),
                    SizedBox(height: S.w * .025),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                              width: S.w * .2,
                              child: Divider(color: SiswamediaTheme.border)),
                          CustomText('atau', Kind.descBlack32),
                          Container(
                              width: S.w * .2,
                              child: Divider(color: SiswamediaTheme.border)),
                        ]),
                    SizedBox(height: S.w * .04),
                    CustomText('Bikin Baru Sekarang', Kind.heading3,
                        align: TextAlign.center),
                    Row(children: [
                      Expanded(
                        child: RoundedButton(
                          color: SiswamediaTheme.lightBlack,
                          text: 'Sekolah / Instansi resmi',
                          onPressed: () {
                            Navigator.pop(context);
                            navigate(context, CreateClassInstansi());
                          },
                        ),
                      )
                    ]),
                    // Row(children: [
                    //   Expanded(
                    //     child: RoundedButton(
                    //       color: SiswamediaTheme.blue,
                    //       text: 'Kelas Personal',
                    //       onPressed: () {
                    //         Navigator.pop(context);
                    //       },
                    //     ),
                    //   )
                    // ]),
                    FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: CustomText('Nanti aja deh', Kind.descBlack)),
                  ],
                ),
              ),
            ),
          );
        });
  }

  static void wrongCode(
    BuildContext context,
  ) {
    showGeneralDialog<void>(
      pageBuilder: (context, anim1, anim2) {
        return;
      },
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.4),
      barrierLabel: '',
      context: context,
      transitionDuration: normalDuration,
      transitionBuilder: (context, anim1, anim2, child) {
        final curvedValue = Curves.easeOutBack.transform(anim1.value);
        return Transform.scale(
          scale: curvedValue,
          child: Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: radius(12),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: S.w * .05, vertical: S.w * .05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset('assets/icons/home/overlay_wrong_code.png',
                      height: S.w * .4, width: S.w * .4),
                  SizedBox(height: S.w * .025),
                  CustomText(
                    'Ooops, kode salah',
                    Kind.heading1,
                    align: TextAlign.center,
                  ),
                  SizedBox(height: S.w * .025),
                  CustomText(
                      'Kode yang kamu masukkan salah atau belum terdaftar, coba kamu periksa lagi mungkin ada huruf kode yang salah',
                      Kind.descBlack32,
                      align: TextAlign.center),
                  SizedBox(height: S.w * .025),
                  RoundedButton(
                    color: SiswamediaTheme.blue,
                    text: 'Coba Lagi',
                    onPressed: () {
                      Navigator.pop(context);
                      noSchoolOverlay(
                        context,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  static void successCode(BuildContext context, String role) {
    showGeneralDialog<void>(
      pageBuilder: (context, anim1, anim2) {
        return;
      },
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.4),
      barrierLabel: '',
      context: context,
      transitionDuration: normalDuration,
      transitionBuilder: (context, anim1, anim2, child) {
        final curvedValue = Curves.easeOutBack.transform(anim1.value);
        return Transform.scale(
          scale: curvedValue,
          child: Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: radius(12),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: S.w * .05, vertical: S.w * .05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset('assets/icons/home/overlay_success_join.png',
                      height: S.w * .4, width: S.w * .4),
                  SizedBox(height: S.w * .025),
                  SizedBox(height: S.w * .025),
                  CustomText(
                      'Berhasil !! kamu sudah bergabung sebagai $role, yuk lihat kelas baru Kamu mungkin ada informasi penting disana.',
                      Kind.descBlack32,
                      align: TextAlign.center),
                  SizedBox(height: S.w * .025),
                  RoundedButton(
                    color: SiswamediaTheme.blue,
                    text: 'Lihat list sekolah',
                    onPressed: () {
                      // await context
                      //     .read<ListProvider>()
                      //     .getMeSchool()
                      //     .then((value) {});
                      Navigator.pop(context);
                      navigate(context, SwitchRole());
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  // static void classOrSchool(BuildContext context, String code) {
  //   var width = MediaQuery.of(context).size.width;
  //   showGeneralDialog<Widget>(
  //       pageBuilder: (context, anim1, anim2) {
  //         return;
  //       },
  //       barrierDismissible: true,
  //       barrierColor: Colors.black.withOpacity(0.4),
  //       barrierLabel: '',
  //       context: context,
  //       transitionDuration: normalDuration,
  //       transitionBuilder: (context, anim1, anim2, child) {
  //         final curvedValue = Curves.easeOutBack.transform(anim1.value);
  //         var currentIndex = 0;
  //         return Transform.scale(
  //           scale: curvedValue,
  //           child: StatefulBuilder(builder: (BuildContext context,
  //               void Function(void Function()) setState) {
  //             return Dialog(
  //               shape: RoundedRectangleBorder(
  //                 borderRadius: radius(12),
  //               ),
  //               child: Padding(
  //                 padding: EdgeInsets.symmetric(
  //                     horizontal: width * .05, vertical: width * .05),
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.center,
  //                   mainAxisSize: MainAxisSize.min,
  //                   children: <Widget>[
  //                     CustomText(
  //                       'Join ke',
  //                       Kind.heading1,
  //                       align: TextAlign.center,
  //                     ),
  //                     SizedBox(height: width * .025),
  //                     CustomText(
  //                       'Jika anda ORTU klik tombol kelas',
  //                       Kind.descBlack,
  //                       align: TextAlign.center,
  //                     ),
  //                     SizedBox(height: width * .025),
  //                     Row(
  //                       children: [
  //                         Expanded(
  //                           child: RoundedButton(
  //                             color: SiswamediaTheme.nearBlack,
  //                             text: 'Sekolah / Instansi Resmi',
  //                             onPressed: () {
  //                               Navigator.pop(context);
  //                               chooseRole(context, code, 'SEKOLAH');
  //                             },
  //                           ),
  //                         )
  //                       ],
  //                     ),
  //                     Row(children: [
  //                       Expanded(
  //                         child: RoundedButton(
  //                           color: SiswamediaTheme.blue,
  //                           text: 'Kelas',
  //                           onPressed: () {
  //                             Navigator.pop(context);
  //                             chooseRole(context, code, 'KELAS');
  //                           },
  //                         ),
  //                       ),
  //                     ]),
  //                   ],
  //                 ),
  //               ),
  //             );
  //           }),
  //         );
  //       });
  // }

  static void chooseRole(BuildContext context, String code, {String type}) {
    type = 'KELAS';
    showGeneralDialog<Widget>(
        pageBuilder: (context, anim1, anim2) {
          return;
        },
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.4),
        barrierLabel: '',
        context: context,
        transitionDuration: normalDuration,
        transitionBuilder: (context, anim1, anim2, child) {
          final curvedValue = Curves.easeOutBack.transform(anim1.value);
          return Transform.scale(
            scale: curvedValue,
            child: StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              return Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: radius(12),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .05),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(children: []),
                      CustomText('Pilih Role', Kind.heading1),
                      SizedBox(height: S.w * .025),
                      Column(
                        children:
                            (type == 'SEKOLAH' ? data.sublist(0, 2) : data)
                                .map((e) => Row(
                                      children: [
                                        Expanded(
                                            child: RoundedButton(
                                          color: data.indexOf(e) == 0
                                              ? SiswamediaTheme.blue
                                              : data.indexOf(e) == 1
                                                  ? SiswamediaTheme.lightBlack
                                                  : SiswamediaTheme.green,
                                          text: e,
                                          onPressed: () {
                                            Navigator.pop(context);
                                            confirmationJoin(
                                              context,
                                              code,
                                              e,
                                              type,
                                            );
                                          },
                                        )),
                                      ],
                                    ))
                                .toList(),
                      ),
                      // Row(children: [
                      //   Icon(Icons.info, size: width / 40, color: Colors.red),
                      //   Text(errorMessage,
                      //       style: TextStyle(
                      //           color: Colors.red, fontSize: width / 40)),
                      // ])
                    ],
                  ),
                ),
              );
            }),
          );
        });
  }

  static void confirmationJoin(
    BuildContext context,
    String code,
    String role,
    String type,
  ) {
    showGeneralDialog<Widget>(
        pageBuilder: (context, anim1, anim2) {
          return;
        },
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.4),
        barrierLabel: '',
        context: context,
        transitionDuration: normalDuration,
        transitionBuilder: (context, anim1, anim2, child) {
          final curvedValue = Curves.easeOutBack.transform(anim1.value);
          return Transform.scale(
            scale: curvedValue,
            child: StatefulBuilder(builder: (BuildContext context,
                void Function(void Function()) setState) {
              return Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: radius(12),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: S.w * .05, vertical: S.w * .05),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      CustomText(
                        'Konfirmasi',
                        Kind.heading1,
                        align: TextAlign.center,
                      ),
                      SizedBox(height: S.w * .025),
                      CustomText(
                          'Apakah Anda yakin untuk join', Kind.descHeading,
                          align: TextAlign.center,
                          subText: 'dengan role',
                          span: type,
                          span2: role),
                      SizedBox(height: S.w * .025),
                      Row(
                        children: [
                          Expanded(
                            child: RoundedButton(
                              color: SiswamediaTheme.blue,
                              text: 'Yakin',
                              onPressed: () {
                                var reqMap = <String, dynamic>{
                                  'invitation_code': code,
                                  'role': role,
                                };
                                if (type == 'SEKOLAH') {
                                  SchoolServices.joinSchool(reqData: reqMap)
                                      .then((value) {
                                    if (value == 200) {
                                      Navigator.pop(context);
                                    } else {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      CustomFlushBar.errorFlushBar(
                                          'Gagal Bergabung', context);
                                    }
                                  });
                                } else {
                                  ClassServices.joinClass(reqData: reqMap)
                                      .then((value) {
                                    if (value.statusCode == 200 ||
                                        value.statusCode == 201) {
                                      Navigator.pop(context);
                                      successCode(context, role);
                                    } else {
                                      Navigator.pop(context);
                                      wrongCode(
                                        context,
                                      );
                                    }
                                  });
                                }
                              },
                            ),
                          )
                        ],
                      ),
                      FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: CustomText('Batal', Kind.descBlack)),
                    ],
                  ),
                ),
              );
            }),
          );
        });
  }
}
