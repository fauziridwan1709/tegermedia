part of '_core.dart';

class CustomDialog extends StatelessWidget {
  final String title;
  final String description;
  final String image;
  final List<String> actions;
  final Function onTap;

  CustomDialog({
    @required this.title,
    this.description,
    this.image,
    this.actions,
    this.onTap,
  }) : assert(
          title != null,
        );

  @override
  Widget build(BuildContext context) {
    // 'assets/icons/profile/role.png'
    // var S.w = MediaQuery.of(context).size.width;
    return Center(
      child: SingleChildScrollView(
        child: Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
            child: Container(
                width: S.w * .7,
                padding: EdgeInsets.symmetric(
                    vertical: S.w * .05, horizontal: S.w * .05),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 10),
                    Container(
                      height: S.w * .35,
                      width: S.w * .45,
                      margin: EdgeInsets.only(bottom: 12),
                      decoration: BoxDecoration(
                          image: DecorationImage(image: AssetImage(image))),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    CustomText(title, Kind.heading1),
                    SizedBox(
                      height: 15,
                    ),
                    CustomText(
                      description,
                      Kind.descBlack,
                      align: TextAlign.center,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    InkWell(
                      onTap: () {
                        onTap();
                      },
                      child: Container(
                        width: S.w * .6,
                        padding: EdgeInsets.symmetric(
                            horizontal: S.w * .1, vertical: 14),
                        decoration: BoxDecoration(
                          color: SiswamediaTheme.green,
                          borderRadius: radius(S.w),
                        ),
                        child: Center(
                          child: Text(
                            actions.first,
                            style: boldWhite.copyWith(fontSize: S.w / 30),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    if (actions.length > 1)
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: S.w * .6,
                          padding: EdgeInsets.symmetric(
                              horizontal: S.w * .1, vertical: 14),
                          decoration: BoxDecoration(
                            color: SiswamediaTheme.lightBlack,
                            borderRadius: radius(S.w),
                          ),
                          child: Center(
                            child: Text(
                              actions.last,
                              style: boldWhite.copyWith(fontSize: S.w / 30),
                            ),
                          ),
                        ),
                      ),
                    SizedBox(height: 10),
                  ],
                ))),
      ),
    );
  }
}
