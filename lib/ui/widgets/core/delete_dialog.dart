part of '_core.dart';

class DeleteDialog {
  final String title;
  final String desc;
  final VoidCallback onAgree;
  final BuildContext context;

  DeleteDialog(this.context,
      {@required String title, @required String description, this.onAgree})
      : assert(title != null),
        assert(description != null),
        title = title,
        desc = description;

  void show() {
    showDialog<void>(
        context: context,
        builder: (context2) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
              padding: EdgeInsets.all(S.w * .05),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text(title,
                    style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
                SizedBox(height: 15),
                Text(desc, style: descBlack.copyWith(fontSize: S.w / 34)),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: S.w * .01),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: S.w / 10,
                          width: S.w / 3.5,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: SiswamediaTheme.green,
                          ),
                          child: InkWell(
                            onTap: onAgree,
                            child: Center(
                              child: Text('Ya',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: SiswamediaTheme.lightBlack,
                          ),
                          height: S.w / 10,
                          width: S.w / 3.5,
                          child: InkWell(
                            onTap: () => Navigator.pop(context),
                            child: Center(
                              child: Text('Tidak',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ),
                      ]),
                ),
                SizedBox(height: 10),
              ]),
            )));
  }
}
