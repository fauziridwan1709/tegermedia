part of '_core.dart';

class Error extends StatelessWidget {
  final bool error;
  Error({this.error});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (MediaQuery.of(context).orientation == Orientation.portrait)
          SizedBox(height: width * .25),
        Image.asset(
          'assets/dialog/sorry_no_student.png',
          width: width * 1,
          height: width * .5,
        ),
        SizedBox(height: 20),
        Text(
          '${error}',
          style: semiBlack.copyWith(fontSize: 14),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40),
      ],
    );
  }
}
