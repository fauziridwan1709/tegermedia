part of '_core.dart';

class AdminLabel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 6, vertical: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            border: Border.all(color: SiswamediaTheme.border, width: 1),
            color: SiswamediaTheme.materialGreen[100].withOpacity(.2)),
        child: Text('Admin', style: TextStyle(fontSize: 16)));
  }
}
