part of '_core.dart';

class ButtonAction extends StatelessWidget {
  final Function onTap;
  final String label;
  final String icon;
  final String subLabel;

  ButtonAction(
      {@required this.onTap,
        @required this.label,
        @required this.icon,
        this.subLabel});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: Container(
          margin: EdgeInsets.only(left: 25, right: 25, bottom: 10),
          width: S.w,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 13),
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: SiswamediaTheme.grey.withOpacity(0.2),
                    offset: const Offset(0, 0),
                    blurRadius: 1.0),
              ],
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Color(0xFF00000029))),
          child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(height: 45, width: 65, child: Image.asset(icon)),
            SizedBox(width: 25),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(label,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
              subLabel == null
                  ? SizedBox()
                  : Text(subLabel,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.normal,
                      color: Colors.red))
            ]),
          ]),
        ));
  }
}