part of '../_widgets.dart';

class CustomFlushBar {
  static void errorFlushBar(String message, BuildContext context) {
    //todo for web
    // final bar = SnackBar(
    //     margin: EdgeInsets.only(left: S.w * .05, right: S.w * .05, bottom: 10),
    //     content: Row(children: [
    //       Icon(Icons.info_outline, color: Colors.white),
    //       SizedBox(width: 10),
    //       Text(message)
    //     ]),
    //     behavior: SnackBarBehavior.floating,
    //     backgroundColor: Color(0xFFFF6c83),
    //     shape: RoundedRectangleBorder(
    //       borderRadius: radius(10),
    //     ),
    //     duration: Duration(milliseconds: 1800));
    // ScaffoldMessenger.of(context).showSnackBar(bar);
    // key.currentState.showSnackBar(bar);
    Flushbar<void>(
      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      borderRadius: 10,
      icon: Icon(Icons.info_outline, color: Colors.white),
      duration: Duration(milliseconds: 1800),
      flushbarPosition: FlushbarPosition.BOTTOM,
      backgroundColor: Color(0xFFFF6c83),
      message: message,
      isDismissible: true,
    ).show(context);
  }

  static void errorButtonFlushBar(String message, BuildContext context,
      {Function onPressed}) {
    Flushbar<void>(
      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      borderRadius: 10,
      icon: Icon(Icons.info_outline, color: Colors.white),
      duration: Duration(milliseconds: 2500),
      flushbarPosition: FlushbarPosition.BOTTOM,
      backgroundColor: Color(0xFFFF6c83),
      message: message,
      mainButton: Padding(
        padding: EdgeInsets.only(right: 8.0),
        child: RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: radius(6)),
          onPressed: () {
            onPressed();
          },
          color: Colors.white,
          child: Text('Buat'),
        ),
      ),
    ).show(context);
  }

  static void errorInfoFlushBar(String message, BuildContext context) {
    Flushbar<void>(
      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      borderRadius: 10,
      title: 'Sorry!',
      icon: Icon(Icons.info_outline, color: Colors.white),
      duration: Duration(milliseconds: 1800),
      flushbarPosition: FlushbarPosition.BOTTOM,
      backgroundColor: Colors.red.shade500,
      message: message,
    ).show(context);
  }

  static void actionErrorFlushBar(
      String message, String sub, VoidCallback onTap, BuildContext context) {
    Flushbar<void>(
      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      borderRadius: 10,
      title: 'Sorry!',
      icon: Icon(Icons.info_outline, color: Colors.white),
      duration: Duration(milliseconds: 1800),
      flushbarPosition: FlushbarPosition.BOTTOM,
      mainButton: TextButton(
        onPressed: onTap,
        child: Text(sub, style: boldBlack.copyWith(fontSize: S.w / 26)),
      ),
      backgroundColor: Colors.red.shade500,
      message: message,
    )
      ..show(context)
      ..dismiss();
  }

  static void successFlushBar(String message, BuildContext context) {
    // final bar = SnackBar(
    //     margin: EdgeInsets.only(left: S.w * .05, right: S.w * .05, bottom: 10),
    //     content: Row(children: [
    //       Icon(Icons.check, color: Colors.white),
    //       SizedBox(width: 10),
    //       Text(message)
    //     ]),
    //     behavior: SnackBarBehavior.floating,
    //     backgroundColor: SiswamediaTheme.green,
    //     shape: RoundedRectangleBorder(
    //       borderRadius: radius(10),
    //     ),
    //     duration: Duration(milliseconds: 1800));
    // ScaffoldMessenger.of(context).showSnackBar(bar);
    // key.currentState.showSnackBar(bar);
    Flushbar<void>(
      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      borderRadius: 10,
      icon: Icon(Icons.check, color: Colors.white),
      duration: Duration(milliseconds: 1800),
      flushbarPosition: FlushbarPosition.BOTTOM,
      backgroundColor: SiswamediaTheme.green,
      message: message,
      isDismissible: true,
    ).show(context);
  }

  static void notificationFlushBar(
      String message, String title, BuildContext context) {
    Flushbar<void>(
      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      leftBarIndicatorColor: Colors.white,
      duration: Duration(milliseconds: 3000),
      flushbarPosition: FlushbarPosition.BOTTOM,
      backgroundColor: SiswamediaTheme.green,
      message: message,
      title: title,
    ).show(context);
  }

  static void eventFlushBar(String message, BuildContext context) {
    Flushbar<void>(
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
            borderRadius: 10,
            duration: Duration(milliseconds: 1800),
            flushbarPosition: FlushbarPosition.BOTTOM,
            backgroundColor: Color(0xff4B4B4B),
            message: message,
            icon: Icon(Icons.settings, color: Colors.white))
        .show(context);
  }

  static void fullEventFlushBar(String message, BuildContext context) {
    Flushbar<void>(
      margin: EdgeInsets.only(left: 0, right: 0, bottom: 0),
      borderRadius: 0,
      duration: Duration(milliseconds: 3000),
      flushbarPosition: FlushbarPosition.BOTTOM,
      backgroundColor: Color(0xff4B4B4B),
      message: message,
      // icon: Icon(Icons.settings, color: Colors.white),
    ).show(context);
  }

  static void noConnection(BuildContext context) {
    // final bar = SnackBar(
    //     margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
    //     content: Row(children: [
    //       Icon(Icons.info_outline, color: Colors.white),
    //       SizedBox(width: 10),
    //       Text('Anda tidak terhubung ke internet')
    //     ]),
    //     behavior: SnackBarBehavior.floating,
    //     backgroundColor: Color(0xFFFF6c83),
    //     shape: RoundedRectangleBorder(
    //       borderRadius: radius(10),
    //     ),
    //     duration: Duration(milliseconds: 1800));
  }
}
