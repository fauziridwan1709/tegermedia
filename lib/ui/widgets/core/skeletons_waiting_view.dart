part of '_core.dart';

class SkeletonWaitingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: List.generate(5, (index) => SkeletonScreen()),
    );
  }
}
