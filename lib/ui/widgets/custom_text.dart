part of '_widgets.dart';

enum Kind {
  heading1,
  heading2,
  heading3,
  heading4,
  headingMax2Line,
  descBlack,
  descWhite,
  descGreen,
  descBlack32,
  descWhite32,
  descGreen32,
  descHeading,
}

class CustomText extends StatelessWidget {
  final String text;
  final String subText;
  final String span;
  final String span2;
  final Kind style;
  final TextAlign align;

  CustomText(this.text, this.style,
      {this.align = TextAlign.left, this.span, this.span2, this.subText});

  @override
  Widget build(BuildContext context) {
    // var S.w = MediaQuery.of(context).size.width;
    switch (style) {
      case Kind.heading1:
        {
          return Text(
            text,
            style: boldBlack.copyWith(fontSize: S.w / 22),
            textAlign: align,
          );
        }
        break;

      case Kind.heading2:
        {
          return Text(text,
              style: semiBlack.copyWith(fontSize: S.w / 22), textAlign: align);
        }
        break;
      case Kind.heading3:
        {
          return Text(text,
              style: semiBlack.copyWith(fontSize: S.w / 26), textAlign: align);
        }
        break;
      case Kind.headingMax2Line:
        {
          return Text(text,
              maxLines: 2,
              style: semiBlack.copyWith(fontSize: S.w / 26),
              textAlign: align);
        }
        break;
      case Kind.heading4:
        {
          return Text(text,
              style: semiWhite.copyWith(fontSize: S.w / 26), textAlign: align);
        }
        break;
      case Kind.descHeading:
        {
          return RichText(
            textAlign: align,
            text: TextSpan(children: [
              TextSpan(
                text: '$text ',
                style: descBlack.copyWith(fontSize: S.w / 30),
              ),
              TextSpan(
                text: '$span ',
                style: semiBlack.copyWith(fontSize: S.w / 30),
              ),
              TextSpan(
                text: '$subText ',
                style: descBlack.copyWith(fontSize: S.w / 30),
              ),
              TextSpan(
                text: span2,
                style: semiBlack.copyWith(fontSize: S.w / 30),
              ),
            ]),
          );
        }
      case Kind.descBlack:
        {
          return Text(text,
              style: descBlack.copyWith(fontSize: S.w / 30), textAlign: align);
        }
        break;

      case Kind.descBlack32:
        {
          return Text(text,
              style: descBlack.copyWith(fontSize: S.w / 32), textAlign: align);
        }
        break;

      case Kind.descWhite:
        {
          return Text(text,
              style: descWhite.copyWith(fontSize: S.w / 30), textAlign: align);
        }
        break;

      case Kind.descWhite32:
        {
          return Text(text,
              style: descWhite.copyWith(fontSize: S.w / 32), textAlign: align);
        }
        break;

      case Kind.descGreen:
        {
          return Text(text,
              style: descBlack.copyWith(
                  fontSize: S.w / 30, color: SiswamediaTheme.green),
              textAlign: align);
        }
        break;

      case Kind.descGreen32:
        {
          return Text(text,
              style: descBlack.copyWith(
                  fontSize: S.w / 32, color: SiswamediaTheme.green),
              textAlign: align);
        }
        break;

      default:
        {
          return Text(text,
              style: boldBlack.copyWith(fontSize: S.w / 22), textAlign: align);
        }
        break;
    }
  }
}
