part of '_class_page.dart';

class CardMenu extends StatelessWidget {
  const CardMenu({
    Key key,
    @required this.context,
    @required this.data,
    this.keyPilihKelas,
    this.scrollController,
    this.onTap,
  }) : super(key: key);

  final BuildContext context;
  final Map<String, dynamic> data;
  final GlobalKey keyPilihKelas;
  final ScrollController scrollController;
  final Function(Map<String, dynamic> data) onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: SiswamediaTheme.whiteRadiusShadow,
      margin: EdgeInsets.symmetric(horizontal: S.w * .02, vertical: S.w * .03),
      child: InkWell(
        onTap: () => onTap(data),
        child: MenuClass(width: S.w, icon: data['icon'], name: data['name']),
      ),
    );
  }
}

class MenuClass extends StatelessWidget {
  const MenuClass({
    Key key,
    @required this.width,
    @required this.name,
    @required this.icon,
  }) : super(key: key);

  final double width;
  final String icon;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(
            horizontal: width * .02, vertical: width * .02),
        height: 180,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Center(
                child: Container(
              width: 115,
              height: 80,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage(icon))),
            )),
            Center(
              child: Text(name),
            )
          ],
        ));
  }
}
