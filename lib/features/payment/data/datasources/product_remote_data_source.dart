part of '_datasources.dart';

abstract class ProductRemoteDataSource {
  Future<Parsed<Payload<List<ProductConfigModel>>>> getConfigProduct(QueryProduct query);
  Future<Parsed<Payload<OrderProductDetail>>> orderInquiry(OrderProductRequestModel model);
  Future<Parsed<Payload<String>>> createOrder(OrderProductRequestModel model);
}

class ProductRemoteDataSourceImpl implements ProductRemoteDataSource {
  final Client client;
  ProductRemoteDataSourceImpl({this.client});

  @override
  Future<Parsed<Payload<List<ProductConfigModel>>>> getConfigProduct(QueryProduct query) async {
    String url = '${Endpoints.coreProductList}?$query';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader(), client: client);
    return resp.parse(
      Payload.fromJson(
        resp.dataPayloadAsMap,
        (dynamic data) {
          List<ProductConfigModel> productsList = [];
          for (dynamic product in data) {
            productsList.add(ProductConfigModel.fromJson(product));
          }
          return productsList;
        },
      ),
    );
  }

  @override
  Future<Parsed<Payload<OrderProductDetail>>> orderInquiry(OrderProductRequestModel model) async {
    String url = '${Endpoints.coreOrderInquiry}';
    var resp =
        await postIt(url, model.toJson(), headers: await Pref.getIdentityHeader(), client: client);
    return resp.parse(
      Payload.fromJson(
        resp.dataPayloadAsMap,
        (dynamic data) {
          Logger().d(data);
          return OrderProductDetail.fromJson(data);
        },
      ),
    );
  }

  @override
  Future<Parsed<Payload<String>>> createOrder(OrderProductRequestModel model) async {
    String url = '${Endpoints.coreOrderCreate}';
    var resp =
        await postIt(url, model.toJson(), headers: await Pref.getIdentityHeader(), client: client);
    return resp.parse(
      Payload.fromJson(
        resp.dataPayloadAsMap,
        (dynamic data) {
          return '';
        },
      ),
    );
  }
}
