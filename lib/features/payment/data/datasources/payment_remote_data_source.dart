part of '_datasources.dart';

abstract class PaymentRemoteDataSource {
  Future<Parsed<List<PaymentMethodModel>>> getListPaymentMethod(QueryConfigPaymentWithUrl query);
  Future<Parsed<RegisterInvoiceResponse>> registerInvoice(RegisterInvoiceModel model);
  Future<Parsed<NicepayPaymentModel>> confirmPPob(Map<String, dynamic> dataPayment);
  Future<Parsed<String>> confirmPayment(String paymentId);
}

class PaymentRemoteDataSourceImpl implements PaymentRemoteDataSource {
  @override
  Future<Parsed<List<PaymentMethodModel>>> getListPaymentMethod(
      QueryConfigPaymentWithUrl query) async {
    var resp = await getIt(query.toString(), headers: await Pref.getIdentityHeader());
    return resp.parse(ListPaymentMethodModel.fromJson(resp.bodyAsMap).list);
  }

  @override
  Future<Parsed<RegisterInvoiceResponse>> registerInvoice(RegisterInvoiceModel model) async {
    String url = '${Endpoints.registerInvoice}';
    var resp = await postIt(url, model.toJson(), headers: await Pref.getIdentityHeader());
    return resp.parse(RegisterInvoiceResponseModel.fromJson(resp.dataPayloadAsMap));
  }

  @override
  Future<Parsed<NicepayPaymentModel>> confirmPPob(Map<String, dynamic> dataPayment) async {
    String url = '${Endpoints.orderPaymentMethodsPPob}';
    var resp = await postIt(url, dataPayment, headers: await Pref.getIdentityHeader());
    return resp.parse(NicepayPaymentModel.fromJson(resp.dataPayloadAsMap));
  }

  @override
  Future<Parsed<String>> confirmPayment(String paymentId) async {
    String url = '${Endpoints.courses}';
    var resp = await postIt(url, <String, String>{'paymentID': paymentId});
    return resp.parse('');
  }
}
