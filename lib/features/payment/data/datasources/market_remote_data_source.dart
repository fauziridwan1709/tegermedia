part of '_datasources.dart';

abstract class MarketRemoteDataSource {
  Future<Parsed<List<SppBill>>> getAllBill(QueryBill query);
  Future<Parsed<VoucherAvailability>> checkVoucherAvailability(String code, String type);
  Future<Parsed<RegisterInvoice>> registerInvoice(RegisterInvoiceModel model);
  Future<Parsed<String>> confirmPayment(String paymentId);
  Future<Parsed<String>> cancelTransaction(String trxId);
}

class MarketRemoteDataSourceImpl implements MarketRemoteDataSource {
  ///return list of Bill in market service
  ///see [Endpoints] for list of market endpoints
  @override
  Future<Parsed<List<SppBill>>> getAllBill(QueryBill query) async {
    var url = '${Endpoints.getAllSppBill}?$query';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    return resp.parse(ListBillModel.fromJson(jsonDecode(resp.body)).list);
  }

  ///return list of availability voucher in market service
  ///ex: Spp
  @override
  Future<Parsed<VoucherAvailability>> checkVoucherAvailability(String code, String type) async {
    var param = 'code=$code&type=$type';
    var url = '${Endpoints.voucherAvailability}?$param';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    return resp.parse(VoucherAvailabilityModel.fromJson(resp.dataPayloadAsMap));
  }

  ///invoice registration or market service
  @override
  Future<Parsed<RegisterInvoice>> registerInvoice(RegisterInvoiceModel model) async {
    var url = '${Endpoints.registerInvoice}';
    var resp = await postIt(url, model.toJson());
    return resp.parse(RegisterInvoiceModel.fromJson(resp.dataPayloadAsMap));
  }

  @override
  Future<Parsed<String>> confirmPayment(String paymentId) async {
    String url = '${Endpoints.confirmPayment}';
    var resp = await postIt(url, <String, String>{'paymentID': paymentId});
    return resp.parse('');
  }

  ///cancel transaction of origin market in transactionHistoryPendingPage
  @override
  Future<Parsed<String>> cancelTransaction(String trxId) async {
    String url = '${Endpoints.marketPayment}';
    var resp = await postIt(url, <String, String>{'paymentID': trxId});
    return resp.parse('');
  }
}
