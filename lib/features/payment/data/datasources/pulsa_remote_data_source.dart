part of '_datasources.dart';

abstract class PulsaRemoteDataSource {
  Future<Parsed<Payload<List<PacketPulses>>>> getAllBill(QueryPulsa query);

  Future<Parsed<String>> createOrder(Map<String, dynamic> query);
  Future<Parsed<OrderProductDetail>> orderInquiry(Map<String, dynamic> query);

  Future<Parsed<OrderPaymentModels>> orderPayment(String orderId);
}

class PulsesRemoteDataSourceImpl implements PulsaRemoteDataSource {
  @override
  Future<Parsed<Payload<List<PacketPulses>>>> getAllBill(QueryPulsa query) async {
    String url = '${Endpoints.ppobDev}${Endpoints.dataPacketPulses}?$query';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    return resp.parse(
      Payload.fromJson(resp.dataPayloadAsMap, (dynamic data) {
        List<PacketPulses> packetPulses = [];
        data.forEach((dynamic v) {
          var site = PacketPulses.fromJson(v);
          packetPulses.add(site);
        });
        return packetPulses;
      }),
    );
  }

  @override
  Future<Parsed<String>> createOrder(Map<String, dynamic> query) async {
    String url = '${Endpoints.ppobDev}${Endpoints.orderId}';
    var resp = await postIt(url, query, headers: await Pref.getIdentityHeaderSignature());
    String orderID = resp.dataPayloadAsMap['orderID'];
    var data = resp.parse(orderID);
    return data;
  }

  @override
  Future<Parsed<OrderPaymentModels>> orderPayment(String orderId) async {
    String url = '${Endpoints.ppobDev}${Endpoints.orderPayment}/$orderId';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    return resp.parse(OrderPaymentModels.fromJson(resp.dataPayloadAsMap));
  }

  @override
  Future<Parsed<OrderProductDetail>> orderInquiry(Map<String, dynamic> query) async {
    String url = '${Endpoints.ppobDev}${Endpoints.orderInquiry}';
    var resp = await postIt(url, query, headers: await Pref.getIdentityHeader());
    vUtils.setLog('url : ' + url);
    OrderProductDetail productDetail;
    if (resp.dataPayloadAsMap != null) {
      productDetail = OrderProductDetail.fromJson(resp.dataPayloadAsMap['productDetail']);
      productDetail.totalAdmin = resp.dataPayloadAsMap['adminFee'];
    }
    var data = resp.parse(productDetail);
    return data;
  }
}
