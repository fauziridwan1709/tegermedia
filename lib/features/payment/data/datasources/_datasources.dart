import 'dart:convert';

import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/features/payment/data/models/_models.dart';
import 'package:tegarmedia/features/payment/data/models/payload_model.dart';
import 'package:tegarmedia/features/payment/data/models/payment/nicepayment_model.dart';
import 'package:tegarmedia/features/payment/data/models/payment/order_payment_models.dart';
import 'package:tegarmedia/features/payment/domain/entities/_entities.dart';
import 'package:tegarmedia/features/payment/domain/entities/query_pulsa.dart';
import 'package:tegarmedia/features/payment/domain/entities/transaction_pulses.dart';
import 'package:tegarmedia/utils/v_utils.dart';

part 'market_remote_data_source.dart';
part 'payment_remote_data_source.dart';
part 'product_remote_data_source.dart';
part 'pulsa_remote_data_source.dart';
part 'transaction_remote_data_source.dart';
