part of '_datasources.dart';

abstract class TransactionRemoteDataSource {
  Future<Parsed<List<TransactionModel>>> getTransactionHistory(String status);
  Future<Parsed<List<TransactionModel>>> getTransactionSppHistory(QueryTransactionSpp query);

  Future<Parsed<TransactionModel>> getTransactionDetail(String transactionId);
  Future<Parsed<Payload<List<TransactionPulses>>>> getTransactionPulses(
      String limit, String number);
  Future<Parsed<Payload<TransactionPulses>>> getDetailTransactionPulses(String transactionId);
  Future<Parsed<Payload<List<TransactionPulses>>>> getTransactionPendingPulses(
      String limit, String number);
  Future<Parsed<Payload<List<TransactionPulses>>>> getTransactionProductHistory(
      QueryTransactionProduct query);
}

class TransactionRemoteDataSourceImpl implements TransactionRemoteDataSource {
  TransactionRemoteDataSourceImpl({this.client});
  final Client client;

  @override
  Future<Parsed<TransactionModel>> getTransactionDetail(String transactionId) async {
    var url = '${Endpoints.transaction}/$transactionId';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader(), client: client);
    return resp.parse(TransactionModel.fromJson(resp.dataPayloadAsMap));
  }

  @override
  Future<Parsed<List<TransactionModel>>> getTransactionHistory(String status) async {
    var url = '${Endpoints.transaction}/history?status=${status.toUpperCase()}';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader(), client: client);
    var list = status == 'init'
        ? await getInitTransactionHistory()
        : ListTransactionModel.fromJson(resp.bodyAsMap).list;
    Logger().d(list);
    return resp.parse(list);
  }

  Future<List<TransactionModel>> getInitTransactionHistory() async {
    var url = '${Endpoints.transaction}/history?status=PENDING';
    var url2 = '${Endpoints.transaction}/history?status=SUCCESS';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    var resp2 = await getIt(url2, headers: await Pref.getIdentityHeader());
    List<TransactionModel> listModel = <TransactionModel>[];
    listModel.addAll(ListTransactionModel.fromJson(resp.bodyAsMap).list);
    listModel.addAll(ListTransactionModel.fromJson(resp2.bodyAsMap).list);
    return listModel;
  }

  @override
  Future<Parsed<Payload<List<TransactionPulses>>>> getTransactionPulses(
      String limit, String number) async {
    var url =
        '${Endpoints.historyPulses}?pageLimit=10&pageNumber=$number&ayopopStatus=SUCCESS,FAILED';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader(), client: client);

    return resp.parse(Payload.fromJson(resp.dataPayloadAsMap, (dynamic data) {
      List<TransactionPulses> packetPulses = [];
      if (data != null) {
        data.forEach((dynamic v) {
          var site = TransactionPulses.fromJson(v);
          packetPulses.add(site);
        });
      }
      return packetPulses;
    }));
  }

  @override
  Future<Parsed<Payload<List<TransactionPulses>>>> getTransactionPendingPulses(
      String limit, String number) async {
    var url =
        '${Endpoints.historyPulses}?pageLimit=10&pageNumber=$number&ayopopStatus=CREATED,PROCESS';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader(), client: client);
    List<TransactionPulses> listModel = <TransactionPulses>[];
    return resp.parse(Payload.fromJson(resp.dataPayloadAsMap, (dynamic data) {
      if (resp.dataPayloadAsMap['totalData'] != 0) {
        data.forEach((dynamic v) {
          var site = TransactionPulses.fromJson(v);
          Logger().d(site.toJson());
          listModel.add(site);
        });
      }
      return listModel;
    }));
  }

  @override
  Future<Parsed<Payload<TransactionPulses>>> getDetailTransactionPulses(
      String transactionId) async {
    // TODO: implement getDetailTransactionPulses

    var url = '${Endpoints.historyPulsesDetail}/$transactionId';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader(), client: client);
    List<TransactionPulses> listModel = <TransactionPulses>[];
    return resp.parse(Payload.fromJson(resp.dataPayloadAsMap, (dynamic data) {
      return TransactionPulses.fromJson(data);
    }));
  }

  @override
  Future<Parsed<List<TransactionModel>>> getTransactionSppHistory(QueryTransactionSpp query) async {
    String url = '${Endpoints.transaction}/history?$query';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    List<TransactionModel> listModel = <TransactionModel>[];
    listModel.addAll(ListTransactionModel.fromJson(resp.bodyAsMap).list);
    return resp.parse(listModel);
  }

  @override
  Future<Parsed<Payload<List<TransactionPulses>>>> getTransactionProductHistory(
      QueryTransactionProduct query) async {
    var url = '${Endpoints.historyPulses}?$query';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader(), client: client);
    List<TransactionPulses> listModel = <TransactionPulses>[];
    return resp.parse(Payload.fromJson(resp.dataPayloadAsMap, (dynamic data) {
      data.forEach((dynamic v) {
        var site = TransactionPulses.fromJson(v);
        Logger().d(site.toJson());
        listModel.add(site);
      });
      return listModel;
    }));
  }
}
