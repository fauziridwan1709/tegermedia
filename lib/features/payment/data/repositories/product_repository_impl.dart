part of '_repositories.dart';

class ProductRepositoryImpl implements ProductRepository {
  final ProductRemoteDataSource remoteDataSource;

  const ProductRepositoryImpl({@required this.remoteDataSource});

  @override
  Future<Decide<Failure, Parsed<Payload<List<ProductConfigModel>>>>> getConfigByCategory(
      QueryProduct query) async {
    return await apiCall(remoteDataSource.getConfigProduct(query));
  }

  @override
  Future<Decide<Failure, Parsed<Payload<OrderProductDetail>>>> orderInquiry(
      OrderProductRequestModel model) async {
    return await apiCall(remoteDataSource.orderInquiry(model));
  }

  @override
  Future<Decide<Failure, Parsed<Payload<String>>>> createOrder(
      OrderProductRequestModel model) async {
    return await apiCall(remoteDataSource.createOrder(model));
  }
}
