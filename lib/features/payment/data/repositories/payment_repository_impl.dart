part of '_repositories.dart';

class PaymentRepositoryImpl implements PaymentRepository {
  PaymentRemoteDataSource remoteDataSource = PaymentRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<PaymentMethodModel>>>> getListPaymentMethod(
      QueryConfigPaymentWithUrl query) async {
    return await apiCall(remoteDataSource.getListPaymentMethod(query));
  }

  @override
  Future<Decide<Failure, Parsed<RegisterInvoiceResponse>>> registerInvoice(
      RegisterInvoice model) async {
    return await apiCall(remoteDataSource.registerInvoice(model));
  }

  @override
  Future<Decide<Failure, Parsed<String>>> confirmPayment(String paymentId) async {
    return await apiCall(remoteDataSource.confirmPayment(paymentId));
  }

  @override
  Future<Decide<Failure, Parsed<NicepayPaymentModel>>> confirmPPob(
      Map<String, dynamic> dataPayment) async {
    return await apiCall(remoteDataSource.confirmPPob(dataPayment));
  }
}
