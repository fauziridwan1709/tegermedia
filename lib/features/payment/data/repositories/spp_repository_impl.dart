part of '_repositories.dart';

class MarketRepositoryImpl implements MarketRepository {
  MarketRemoteDataSource remoteDataSource = MarketRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<SppBill>>>> getAllBill(QueryBill query) async {
    return await apiCall(remoteDataSource.getAllBill(query));
  }

  @override
  Future<Decide<Failure, Parsed<VoucherAvailability>>> checkVoucherAvailability(
      String code, String type) async {
    return await apiCall(remoteDataSource.checkVoucherAvailability(code, type));
  }

  @override
  Future<Decide<Failure, Parsed<RegisterInvoice>>> registerInvoice(RegisterInvoice model) async {
    return await apiCall(remoteDataSource.registerInvoice(model));
  }

  @override
  Future<Decide<Failure, Parsed<String>>> confirmPayment(String paymentId) async {
    return await apiCall(remoteDataSource.confirmPayment(paymentId));
  }

  @override
  Future<Decide<Failure, Parsed<String>>> cancelTransaction(String trxId) async {
    return await apiCall(remoteDataSource.cancelTransaction(trxId));
  }
}
