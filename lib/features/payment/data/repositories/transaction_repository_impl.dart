part of '_repositories.dart';

class TransactionRepositoryImpl implements TransactionRepository {
  const TransactionRepositoryImpl({this.remoteDataSource});

  final TransactionRemoteDataSource remoteDataSource;

  @override
  Future<Decide<Failure, Parsed<Transaction>>> getTransactionDetail(String transactionId) async {
    return await apiCall(remoteDataSource.getTransactionDetail(transactionId));
  }

  @override
  Future<Decide<Failure, Parsed<List<Transaction>>>> getTransactionHistory(String status) async {
    return await apiCall(remoteDataSource.getTransactionHistory(status));
  }

  @override
  Future<Decide<Failure, Parsed<Payload<List<TransactionPulses>>>>> getTransactionPendingPulses(
      String limit, String number) async {
    // TODO: implement getTransactionPendingPulses
    return await apiCall(remoteDataSource.getTransactionPendingPulses(limit, number));
  }

  @override
  Future<Decide<Failure, Parsed<Payload<List<TransactionPulses>>>>> getTransactionPulses(
      String limit, String number) async {
    // TODO: implement getTransactionPulses
    return await apiCall(remoteDataSource.getTransactionPulses(limit, number));
  }

  @override
  Future<Decide<Failure, Parsed<List<Transaction>>>> getTransactionSppHistory(
      QueryTransactionSpp query) async {
    return await apiCall(remoteDataSource.getTransactionSppHistory(query));
  }

  @override
  Future<Decide<Failure, Parsed<Payload<List<TransactionPulses>>>>> getTransactionProductHistory(
      QueryTransactionProduct query) async {
    return await apiCall(remoteDataSource.getTransactionProductHistory(query));
  }
}
