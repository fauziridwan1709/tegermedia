part of '_repositories.dart';

class PulsaRepositoryImpl implements PulsaRepository {
  PulsaRemoteDataSource remoteDataSource = PulsesRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<Payload<List<PacketPulses>>>>> getAllAvailablePulsa(
      QueryPulsa query) async {
    return await apiCall(remoteDataSource.getAllBill(query));
  }

  @override
  Future<Decide<Failure, Parsed<String>>> createOrder(Map<String, dynamic> query) async {
    return await apiCall(remoteDataSource.createOrder(query));
  }

  @override
  Future<Decide<Failure, Parsed<OrderPaymentModels>>> orderPayment(String query) async {
    return await apiCall(remoteDataSource.orderPayment(query));
  }

  @override
  Future<Decide<Failure, Parsed<OrderProductDetail>>> orderInquery(
      Map<String, dynamic> query) async {
    return await apiCall(remoteDataSource.orderInquiry(query));
  }
}
