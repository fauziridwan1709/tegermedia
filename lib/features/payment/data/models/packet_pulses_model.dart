part of '_models.dart';

class PacketPulses {
  String createdAt;
  String updatedAt;
  String id;
  ProductsModel products;
  String logo;
  int margin;
  bool isActive;

  PacketPulses(
      {this.createdAt,
      this.updatedAt,
      this.id,
      this.products,
      this.logo,
      this.margin,
      this.isActive});

  PacketPulses.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    id = json['id'];
    products = json['products'] != null ? ProductsModel.fromJson(json['products']) : null;
    logo = json['logo'];
    margin = json['margin'];
    isActive = json['isActive'];
  }
}
