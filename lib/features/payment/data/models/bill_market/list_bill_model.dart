part of '../_models.dart';

class ListBillModel extends Models {
  List<BillModel> list;

  ListBillModel({this.list});

  ListBillModel.fromJson(Map<String, dynamic> json) {
    if (json['payload'] != null) {
      list = <BillModel>[];
      json['payload'].forEach((dynamic v) {
        list.add(BillModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
