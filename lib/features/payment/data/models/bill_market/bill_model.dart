part of '../_models.dart';

class BillModel extends SppBill {
  BillModel(
      {int id,
      String userId,
      String schoolId,
      String type,
      int month,
      int year,
      String dueDate,
      int discount,
      int total,
      String disburmentAccountName,
      String disburmentAccountNumb,
      String disburmentAccountType,
      String disburmentBankName,
      String status,
      String createAt,
      String updatedAt})
      : super(
            id: id,
            userId: userId,
            schoolId: schoolId,
            type: type,
            month: month,
            year: year,
            dueDate: dueDate,
            discount: discount,
            total: total,
            disburmentAccountName: disburmentAccountName,
            disburmentAccountNumb: disburmentAccountNumb,
            disburmentAccountType: disburmentAccountType,
            disburmentBankName: disburmentBankName,
            status: status,
            createAt: createAt,
            updatedAt: updatedAt);

  BillModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    schoolId = json['school_id'];
    type = json['type'];
    month = json['month'];
    year = json['year'];
    dueDate = json['due_date'];
    discount = json['discount'];
    total = json['total'];
    disburmentAccountName = json['disburment_account_name'];
    disburmentAccountNumb = json['disburment_account_numb'];
    disburmentAccountType = json['disburment_account_type'];
    disburmentBankName = json['disburment_bank_name'];
    status = json['status'];
    createAt = json['create_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['school_id'] = schoolId;
    data['type'] = type;
    data['month'] = month;
    data['year'] = year;
    data['due_date'] = dueDate;
    data['discount'] = discount;
    data['total'] = total;
    data['disburment_account_name'] = disburmentAccountName;
    data['disburment_account_numb'] = disburmentAccountNumb;
    data['disburment_account_type'] = disburmentAccountType;
    data['disburment_bank_name'] = disburmentBankName;
    data['status'] = status;
    data['create_at'] = createAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
