part of '../_models.dart';

class TransactionItemModel extends TransactionItem {
  TransactionItemModel({
    String name,
    String unit,
    int qty,
    int unitPrice,
    String type,
  }) : super(
          name: name,
          unit: unit,
          qty: qty,
          unitPrice: unitPrice,
          type: type,
        );

  TransactionItemModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    unit = json['unit'];
    qty = json['qty'];
    unitPrice = json['unitPrice'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['unit'] = unit;
    data['qty'] = qty;
    data['unitPrice'] = unitPrice;
    data['type'] = type;
    return data;
  }
}
