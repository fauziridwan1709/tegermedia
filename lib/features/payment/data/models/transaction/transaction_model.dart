part of '../_models.dart';

class TransactionModel extends Transaction {
  TransactionModel({
    String iD,
    int userID,
    String invoiceID,
    String invoiceNumber,
    String productType,
    int productID,
    String trxID,
    String paymentID,
    String paymentStatus,
    String paymentState,
    int invoiceAmount,
    int adminFee,
    int totalAmount,
    String bank,
    String bankName,
    String paymentMethod,
    String paymentProvider,
    String userPayNumber,
    String checkoutURL,
    String token,
    int duration,
    String expiredAt,
    String transactionDate,
    String createdAt,
    String updatedAt,
    List<TransactionItemModel> items,
  }) : super(
          iD: iD,
          userID: userID,
          invoiceID: invoiceID,
          invoiceNumber: invoiceNumber,
          productType: productType,
          productID: productID,
          trxID: trxID,
          paymentID: paymentID,
          paymentStatus: paymentStatus,
          paymentState: paymentState,
          invoiceAmount: invoiceAmount,
          adminFee: adminFee,
          totalAmount: totalAmount,
          bank: bank,
          bankName: bankName,
          paymentMethod: paymentMethod,
          paymentProvider: paymentProvider,
          userPayNumber: userPayNumber,
          checkoutURL: checkoutURL,
          token: token,
          duration: duration,
          expiredAt: expiredAt,
          transactionDate: transactionDate,
          createdAt: createdAt,
          updatedAt: updatedAt,
          items: items,
        );

  TransactionModel.fromJson(Map<String, dynamic> json) {
    iD = json['ID'].toString();
    userID = json['UserID'];
    invoiceID = json['InvoiceID'];
    invoiceNumber = json['InvoiceNumber'];
    productType = json['ProductType'];
    productID = json['ProductID'];
    trxID = json['TrxID'];
    paymentID = json['PaymentID'];
    paymentStatus = json['PaymentStatus'];
    paymentState = json['PaymentState'];
    invoiceAmount = json['InvoiceAmount'];
    adminFee = json['AdminFee'];
    totalAmount = json['TotalAmount'];
    bank = json['Bank'];
    bankName = json['BankName'];
    paymentMethod = json['PaymentMethod'];
    paymentProvider = json['PaymentProvider'];
    userPayNumber = json['UserPayNumber'];
    checkoutURL = json['CheckoutURL'];
    token = json['Token'];
    duration = json['Duration'];
    expiredAt = json['ExpiredAt'];
    transactionDate = json['TransactionDate'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    if (json['item_details'] != null) {
      items = <TransactionItemModel>[];
      json['item_details'].forEach((dynamic v) {
        items.add(TransactionItemModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['UserID'] = userID;
    data['InvoiceID'] = invoiceID;
    data['InvoiceNumber'] = invoiceNumber;
    data['ProductType'] = productType;
    data['ProductID'] = productID;
    data['TrxID'] = trxID;
    data['PaymentID'] = paymentID;
    data['PaymentStatus'] = paymentStatus;
    data['PaymentState'] = paymentState;
    data['InvoiceAmount'] = invoiceAmount;
    data['AdminFee'] = adminFee;
    data['TotalAmount'] = totalAmount;
    data['Bank'] = bank;
    data['BankName'] = bankName;
    data['PaymentMethod'] = paymentMethod;
    data['PaymentProvider'] = paymentProvider;
    data['UserPayNumber'] = userPayNumber;
    data['CheckoutURL'] = checkoutURL;
    data['Token'] = token;
    data['Duration'] = duration;
    data['ExpiredAt'] = expiredAt;
    data['TransactionDate'] = transactionDate;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    if (items != null) {
      data['item_details'] = items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
