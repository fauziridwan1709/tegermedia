part of '../_models.dart';

class ListTransactionModel extends Models {
  List<TransactionModel> list;

  ListTransactionModel.fromJson(Map<String, dynamic> json) {
    if (json['payload'] != null) {
      list = <TransactionModel>[];
      json['payload'].forEach((dynamic v) {
        list.add(TransactionModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }
}
