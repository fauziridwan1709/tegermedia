class OrderPaymentModels {
  String id;
  String invoiceNumber;
  int margin;
  int bill;
  int total;
  String ayopopStatus;
  String failedReason;
  String logo;
  Product product;
  String invDownloadLink;
  String paymentDate;
  String createdAt;

  OrderPaymentModels(
      {this.id,
      this.invoiceNumber,
      this.margin,
      this.bill,
      this.total,
      this.ayopopStatus,
      this.failedReason,
      this.logo,
      this.product,
      this.invDownloadLink,
      this.paymentDate,
      this.createdAt});

  OrderPaymentModels.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    invoiceNumber = json['invoiceNumber'];
    margin = json['margin'];
    bill = json['bill'];
    total = json['total'];
    ayopopStatus = json['ayopopStatus'];
    failedReason = json['failedReason'];
    logo = json['logo'];
    product = json['product'] != null ? Product.fromJson(json['product']) : null;
    invDownloadLink = json['InvDownloadLink'];
    paymentDate = json['PaymentDate'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['invoiceNumber'] = this.invoiceNumber;
    data['margin'] = this.margin;
    data['bill'] = this.bill;
    data['total'] = this.total;
    data['ayopopStatus'] = this.ayopopStatus;
    data['failedReason'] = this.failedReason;
    data['logo'] = this.logo;
    if (this.product != null) {
      data['product'] = this.product.toJson();
    }
    data['InvDownloadLink'] = this.invDownloadLink;
    data['PaymentDate'] = this.paymentDate;
    data['createdAt'] = this.createdAt;
    return data;
  }
}

class Product {
  int inquiryId;
  String refNumber;
  String accountNumber;
  String productCode;
  String customerName;
  String productName;
  String category;
  int amount;
  int totalAdmin;
  String validity;

  Product(
      {this.inquiryId,
      this.refNumber,
      this.accountNumber,
      this.productCode,
      this.customerName,
      this.productName,
      this.category,
      this.amount,
      this.totalAdmin,
      this.validity});

  Product.fromJson(Map<String, dynamic> json) {
    inquiryId = json['inquiryId'];
    refNumber = json['refNumber'];
    accountNumber = json['accountNumber'];
    productCode = json['productCode'];
    customerName = json['customerName'];
    productName = json['productName'];
    category = json['category'];
    amount = json['amount'];
    totalAdmin = json['totalAdmin'];
    validity = json['validity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['inquiryId'] = this.inquiryId;
    data['refNumber'] = this.refNumber;
    data['accountNumber'] = this.accountNumber;
    data['productCode'] = this.productCode;
    data['customerName'] = this.customerName;
    data['productName'] = this.productName;
    data['category'] = this.category;
    data['amount'] = this.amount;
    data['totalAdmin'] = this.totalAdmin;
    data['validity'] = this.validity;
    return data;
  }
}
