part of '../_models.dart';

class RegisterInvoiceResponseModel extends RegisterInvoiceResponse {
  RegisterInvoiceResponseModel(
      {String invoiceID,
      String invoiceNumber,
      int invoiceAmount,
      String paymentState,
      String paymentID,
      String trxID,
      int totalAmount,
      int adminFee,
      int duration,
      String bank,
      String bankName,
      String paymentMethod,
      String paymentProvider,
      String transactionDate,
      String expiredAt,
      String userPayNumber,
      String checkoutURL,
      String token})
      : super(
          invoiceID: invoiceID,
          invoiceNumber: invoiceNumber,
          invoiceAmount: invoiceAmount,
          paymentState: paymentState,
          paymentID: paymentID,
          trxID: trxID,
          totalAmount: totalAmount,
          adminFee: adminFee,
          duration: duration,
          bank: bank,
          bankName: bankName,
          paymentMethod: paymentMethod,
          paymentProvider: paymentProvider,
          transactionDate: transactionDate,
          expiredAt: expiredAt,
          userPayNumber: userPayNumber,
          checkoutURL: checkoutURL,
          token: token,
        );

  RegisterInvoiceResponseModel.fromJson(Map<String, dynamic> json) {
    invoiceID = json['invoiceID'];
    invoiceNumber = json['invoiceNumber'];
    invoiceAmount = json['invoiceAmount'];
    paymentState = json['paymentState'];
    paymentID = json['paymentID'];
    trxID = json['trxID'];
    totalAmount = json['totalAmount'];
    adminFee = json['adminFee'];
    duration = json['duration'];
    bank = json['bank'];
    bankName = json['bankName'];
    paymentMethod = json['paymentMethod'];
    paymentProvider = json['paymentProvider'];
    transactionDate = json['transactionDate'];
    expiredAt = json['expiredAt'];
    userPayNumber = json['userPayNumber'];
    checkoutURL = json['checkoutURL'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['invoiceID'] = invoiceID;
    data['invoiceNumber'] = invoiceNumber;
    data['invoiceAmount'] = invoiceAmount;
    data['paymentState'] = paymentState;
    data['paymentID'] = paymentID;
    data['trxID'] = trxID;
    data['totalAmount'] = totalAmount;
    data['adminFee'] = adminFee;
    data['duration'] = duration;
    data['bank'] = bank;
    data['bankName'] = bankName;
    data['paymentMethod'] = paymentMethod;
    data['paymentProvider'] = paymentProvider;
    data['transactionDate'] = transactionDate;
    data['expiredAt'] = expiredAt;
    data['userPayNumber'] = userPayNumber;
    data['checkoutURL'] = checkoutURL;
    data['token'] = token;
    return data;
  }

  T map<T>({T Function() onLoading, T Function(RegisterInvoiceResponse) onSuccess}) {
    if (this == null) {
      return onLoading();
    }
    return onSuccess(this);
  }
}
