part of '../_models.dart';

class PaymentVaRequestModel {
  String origin;
  String paymentMethod;
  String transactionId;
  String bankCd;
  PaymentVaRequestModel({
    this.origin,
    this.paymentMethod,
    this.transactionId,
    this.bankCd,
  });

  PaymentVaRequestModel.fromJson(Map<String, dynamic> json) {
    origin = json['origin'];
    paymentMethod = json['payment_method'];
    transactionId = json['transaction_id'];
    bankCd = json['bankCd'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['origin'] = origin;
    data['payment_method'] = paymentMethod;
    data['transaction_id'] = transactionId;
    data['bankCd'] = bankCd;
    return data;
  }
}
