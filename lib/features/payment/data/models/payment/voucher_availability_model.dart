part of '../_models.dart';

class VoucherAvailabilityModel extends VoucherAvailability {
  VoucherAvailabilityModel({
    int id,
    String code,
    int minSpend,
    String discountType,
    int discountValue,
    int quota,
    String startAt,
    String endAt,
  }) : super(
          id: id,
          code: code,
          minSpend: minSpend,
          discountType: discountType,
          discountValue: discountValue,
          quota: quota,
          startAt: startAt,
          endAt: endAt,
        );

  VoucherAvailabilityModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    minSpend = json['min_spend'];
    discountType = json['discount_type'];
    discountValue = json['discount_value'];
    quota = json['quota'];
    startAt = json['start_at'];
    endAt = json['end_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['min_spend'] = minSpend;
    data['discount_type'] = discountType;
    data['discount_value'] = discountValue;
    data['quota'] = quota;
    data['start_at'] = startAt;
    data['end_at'] = endAt;
    return data;
  }
}
