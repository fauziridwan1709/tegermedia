class NicepayPaymentModel {
  dynamic adminFee;
  String bank;
  String bankName;
  String checkoutURL;
  int duration;
  String expiredAt;
  dynamic invoiceAmount;
  String invoiceID;
  String invoiceNumber;
  NicepayPayment nicepayPayment;
  String paymentID;
  String paymentMethod;
  String paymentProvider;
  String paymentState;
  String token;
  dynamic totalAmount;
  String transactionDate;
  String trxID;
  String userPayNumber;

  NicepayPaymentModel(
      {this.adminFee,
      this.bank,
      this.bankName,
      this.checkoutURL,
      this.duration,
      this.expiredAt,
      this.invoiceAmount,
      this.invoiceID,
      this.invoiceNumber,
      this.nicepayPayment,
      this.paymentID,
      this.paymentMethod,
      this.paymentProvider,
      this.paymentState,
      this.token,
      this.totalAmount,
      this.transactionDate,
      this.trxID,
      this.userPayNumber});

  NicepayPaymentModel.fromJson(Map<String, dynamic> json) {
    adminFee = json['adminFee'];
    bank = json['bank'];
    bankName = json['bankName'];
    checkoutURL = json['checkoutURL'];
    duration = json['duration'];
    expiredAt = json['expiredAt'];
    invoiceAmount = json['invoiceAmount'];
    invoiceID = json['invoiceID'];
    invoiceNumber = json['invoiceNumber'];
    nicepayPayment =
        json['nicepayPayment'] != null ? NicepayPayment.fromJson(json['nicepayPayment']) : null;
    paymentID = json['paymentID'];
    paymentMethod = json['paymentMethod'];
    paymentProvider = json['paymentProvider'];
    paymentState = json['paymentState'];
    token = json['token'];
    totalAmount = json['totalAmount'];
    transactionDate = json['transactionDate'];
    trxID = json['trxID'];
    userPayNumber = json['userPayNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['adminFee'] = adminFee;
    data['bank'] = bank;
    data['bankName'] = bankName;
    data['checkoutURL'] = checkoutURL;
    data['duration'] = duration;
    data['expiredAt'] = expiredAt;
    data['invoiceAmount'] = invoiceAmount;
    data['invoiceID'] = invoiceID;
    data['invoiceNumber'] = invoiceNumber;
    if (nicepayPayment != null) {
      data['nicepayPayment'] = nicepayPayment.toJson();
    }
    data['paymentID'] = paymentID;
    data['paymentMethod'] = paymentMethod;
    data['paymentProvider'] = paymentProvider;
    data['paymentState'] = paymentState;
    data['token'] = token;
    data['totalAmount'] = totalAmount;
    data['transactionDate'] = transactionDate;
    data['trxID'] = trxID;
    data['userPayNumber'] = userPayNumber;
    return data;
  }
}

class NicepayPayment {
  String callBackUrl;
  String cardCvv;
  String cardExpYymm;
  String cardHolderNm;
  String cardNo;
  String clickPayNo;
  String clickPayToken;
  String dataField3;
  String merchantToken;
  String mitraCd;
  String preauthToken;
  String recurringToken;
  String tXid;
  String timeStamp;
  String urlNicepayPayment;

  NicepayPayment(
      {this.callBackUrl,
      this.cardCvv,
      this.cardExpYymm,
      this.cardHolderNm,
      this.cardNo,
      this.clickPayNo,
      this.clickPayToken,
      this.dataField3,
      this.merchantToken,
      this.mitraCd,
      this.preauthToken,
      this.recurringToken,
      this.tXid,
      this.timeStamp,
      this.urlNicepayPayment});

  NicepayPayment.fromJson(Map<String, dynamic> json) {
    callBackUrl = json['callBackUrl'];
    cardCvv = json['cardCvv'];
    cardExpYymm = json['cardExpYymm'];
    cardHolderNm = json['cardHolderNm'];
    cardNo = json['cardNo'];
    clickPayNo = json['clickPayNo'];
    clickPayToken = json['clickPayToken'];
    dataField3 = json['dataField3'];
    merchantToken = json['merchantToken'];
    mitraCd = json['mitraCd'];
    preauthToken = json['preauthToken'];
    recurringToken = json['recurringToken'];
    tXid = json['tXid'];
    timeStamp = json['timeStamp'];
    urlNicepayPayment = json['urlNicepayPayment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['callBackUrl'] = callBackUrl;
    data['cardCvv'] = cardCvv;
    data['cardExpYymm'] = cardExpYymm;
    data['cardHolderNm'] = cardHolderNm;
    data['cardNo'] = cardNo;
    data['clickPayNo'] = clickPayNo;
    data['clickPayToken'] = clickPayToken;
    data['dataField3'] = dataField3;
    data['merchantToken'] = merchantToken;
    data['mitraCd'] = mitraCd;
    data['preauthToken'] = preauthToken;
    data['recurringToken'] = recurringToken;
    data['tXid'] = tXid;
    data['timeStamp'] = timeStamp;
    data['urlNicepayPayment'] = urlNicepayPayment;
    return data;
  }

  String toStringData() {
    String data =
        'callBackUrl=$callBackUrl&tXid=$tXid&timeStamp=$timeStamp&merchantToken=$merchantToken&cardHolderNm=$cardHolderNm&cardNo=$cardNo&cardExpYymm=$cardExpYymm&cardCvv=$cardCvv';
    return data;
  }
}
