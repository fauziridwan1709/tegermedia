part of '../_models.dart';

class ListPaymentMethodModel {
  List<PaymentMethodModel> list;

  ListPaymentMethodModel({this.list});

  ListPaymentMethodModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <PaymentMethodModel>[];
      json['data'].forEach((dynamic v) {
        list.add(PaymentMethodModel.fromJson(v as Map<String, dynamic>));
      });
    } else if (json['payload'] != null) {
      list = <PaymentMethodModel>[];
      json['payload'].forEach((dynamic v) {
        list.add(PaymentMethodModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    if (list != null) {
      data['payload'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PaymentMethodModel {
  String paymentId;
  String origin;
  String paymentProvider;
  String paymentMethod;
  String paymentMethodCode;
  int paymentTimeMinute;
  bool isPercentage;
  dynamic paymentValue;
  bool isMaxValue;
  int maxValue;
  List<BankModel> bank;

  PaymentMethodModel({
    this.bank,
    this.createdAt,
    this.createdBy,
    this.isActive,
    this.isMaxValue,
    this.isPercentage,
    this.maxValue,
    this.origin,
    this.paymentMethod,
    this.paymentMethodCode,
    this.paymentProvider,
    this.paymentTimeMinute,
    this.paymentValue,
    this.paymentId,
    this.updatedAt,
    this.updatedBy,
  });

  PaymentMethodModel.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    createdBy = json['created_by'];
    isActive = json['isActive'];
    isMaxValue = json['isMaxValue'];
    isPercentage = json['isPercentage'];
    maxValue = json['maxValue'];
    origin = json['origin'];
    paymentMethod = json['paymentMethod'];
    paymentMethodCode = json['paymentMethodCode'];
    paymentProvider = json['paymentProvider'];
    paymentTimeMinute = json['paymentTimeMinute'];
    paymentValue = json['paymentValue'];
    paymentId = json['payment_id'];
    updatedAt = json['updated_at'];
    updatedBy = json['updated_by'];
    paymentId = json['payment_id'];
    origin = json['origin'];
    paymentProvider = json['paymentProvider'];
    paymentMethod = json['paymentMethod'];
    paymentMethodCode = json['paymentMethodCode'];
    paymentTimeMinute = json['paymentTimeMinute'];
    isPercentage = json['isPercentage'];
    isMaxValue = json['isMaxValue'];
    maxValue = json['maxValue'];
    if (json['bank'] != null) {
      bank = <BankModel>[];
      json['bank'].forEach((dynamic v) {
        bank.add(BankModel.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['mitra'] != null) {
      bank = <BankModel>[];
      json['mitra'].forEach((dynamic v) {
        bank.add(BankModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['payment_id'] = paymentId;
    data['origin'] = origin;
    data['paymentProvider'] = paymentProvider;
    data['paymentMethod'] = paymentMethod;
    data['paymentMethodCode'] = paymentMethodCode;
    data['paymentTimeMinute'] = paymentTimeMinute;
    data['isPercentage'] = isPercentage;
    data['paymentValue'] = paymentValue;
    data['isMaxValue'] = isMaxValue;
    data['maxValue'] = maxValue;
    if (bank != null) {
      data['bank'] = bank.map((v) => v.toJson()).toList();
    }
    if (bank != null) {
      data['bank'] = bank.map((v) => v.toJson()).toList();
    }
    data['created_at'] = createdAt;
    data['created_by'] = createdBy;
    data['isActive'] = this.isActive;
    data['isMaxValue'] = this.isMaxValue;
    data['isPercentage'] = this.isPercentage;
    data['maxValue'] = this.maxValue;
    data['origin'] = this.origin;
    data['paymentMethod'] = this.paymentMethod;
    data['paymentMethodCode'] = this.paymentMethodCode;
    data['paymentProvider'] = this.paymentProvider;
    data['paymentTimeMinute'] = this.paymentTimeMinute;
    data['paymentValue'] = this.paymentValue;
    data['payment_id'] = this.paymentId;
    data['updated_at'] = this.updatedAt;
    data['updated_by'] = this.updatedBy;
    return data;
  }

  String createdAt;
  String createdBy;
  bool isActive;
  String updatedAt;
  String updatedBy;
}

class Bank {
  String bankCode;
  String bankName;

  Bank({this.bankCode, this.bankName});

  Bank.fromJson(Map<String, dynamic> json) {
    bankCode = json['bankCode'];
    bankName = json['bankName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bankCode'] = this.bankCode;
    data['bankName'] = this.bankName;
    return data;
  }
}

class Mitra {
  String mitraCode;
  String mitraName;

  Mitra({this.mitraCode, this.mitraName});

  Mitra.fromJson(Map<String, dynamic> json) {
    mitraCode = json['mitraCode'];
    mitraName = json['mitraName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mitraCode'] = this.mitraCode;
    data['mitraName'] = this.mitraName;
    return data;
  }
}

class BankModel {
  String bankName;
  String bankCode;

  BankModel({this.bankName, this.bankCode});

  BankModel.fromJson(Map<String, dynamic> json) {
    bankName = json['bankName'] ?? json['mitraName'];
    bankCode = json['bankCode'] ?? json['mitraCode'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['bankName'] = bankName;
    data['bankCode'] = bankCode;
    return data;
  }
}
