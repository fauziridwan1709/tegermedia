part of '../_models.dart';

class RegisterInvoiceModel extends RegisterInvoice {
  RegisterInvoiceModel({
    String origin,
    String paymentMethod,
    String paymentMethodCode,
    String paymentProvider,
    String bankCode,
    String mitraCode,
    String voucher,
    String voucherType,
    String phoneNumber,
    List<String> productId,
    String userId,
    String productType,
  }) : super(
          origin: origin,
          paymentMethod: paymentMethod,
          paymentMethodCode: paymentMethodCode,
          paymentProvider: paymentProvider,
          bankCode: bankCode,
          mitraCode: mitraCode,
          voucher: voucher,
          voucherType: voucherType,
          phoneNumber: phoneNumber,
          productId: productId,
          userId: userId,
          productType: productType,
        );

  RegisterInvoiceModel.fromJson(Map<String, dynamic> json) {
    origin = json['origin'];
    paymentMethod = json['paymentMethod'];
    paymentMethodCode = json['paymentMethodCode'];
    paymentProvider = json['paymentProvider'];
    bankCode = json['bankCode'];
    mitraCode = json['mitraCode'];
    voucher = json['voucher'];
    voucherType = json['voucher_type'];
    phoneNumber = json['phone_number'];
    productId = json['product_id'].cast<String>();
    userId = json['user_id'];
    productType = json['product_type'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['origin'] = origin;
    data['paymentMethod'] = paymentMethod;
    data['paymentMethodCode'] = paymentMethodCode;
    data['paymentProvider'] = paymentProvider;
    data['bankCode'] = bankCode;
    data['mitraCode'] = mitraCode;
    data['voucher'] = voucher;
    data['voucher_type'] = voucherType;
    data['phone_number'] = phoneNumber;
    data['product_id'] = productId;
    data['user_id'] = userId;
    data['product_type'] = productType;
    return data;
  }
}
