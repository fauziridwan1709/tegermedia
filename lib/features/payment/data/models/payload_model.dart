typedef Parser<T> = T Function(dynamic json);

class Payload<T> {
  int totalData;
  String orderId;
  T data;

  Payload({this.totalData, this.data});

  Payload.fromJson(Map<String, dynamic> json, Parser<T> parser, {T nullSafe}) {
    totalData = json['totalData'];
    if (json['orderID'] != null) {
      orderId = json['orderID'];
    }
    data = (json['data'] ?? json['productDetail']) != null
        ? parser((json['data'] ?? json['productDetail']))
        : nullSafe;
  }
}
