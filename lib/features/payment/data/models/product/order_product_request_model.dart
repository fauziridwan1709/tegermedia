part of '../_models.dart';

class OrderProductRequestModel {
  String productID;
  String accountNumber;
  String productCode;

  OrderProductRequestModel({this.productID, this.accountNumber, this.productCode});

  OrderProductRequestModel.fromJson(Map<String, dynamic> json) {
    productID = json['productID'];
    accountNumber = json['accountNumber'];
    productCode = json['productCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['productID'] = productID;
    data['accountNumber'] = accountNumber;
    data['productCode'] = productCode;
    return data;
  }
}
