part of '../_models.dart';

class OrderProductDetail {
  dynamic inquiryId;
  dynamic refNumber;
  dynamic accountNumber;
  dynamic productCode;
  dynamic customerName;
  dynamic productName;
  dynamic category;
  dynamic amount;
  dynamic totalAdmin;
  dynamic validity;
  List<CustomerDetail> customerDetail;
  List<BillDetails> billDetails;
  List<ProductDetails> productDetails;

  OrderProductDetail(
      {this.inquiryId,
      this.refNumber,
      this.accountNumber,
      this.productCode,
      this.customerName,
      this.productName,
      this.category,
      this.amount,
      this.totalAdmin,
      this.validity,
      this.customerDetail,
      this.billDetails,
      this.productDetails});

  OrderProductDetail.fromJson(Map<String, dynamic> json) {
    Logger().d(json);
    inquiryId = json['inquiryId'].toString();
    refNumber = json['refNumber'].toString();
    accountNumber = json['accountNumber'].toString();
    productCode = json['productCode'].toString();
    customerName = json['customerName'].toString();
    productName = json['productName'].toString();
    category = json['category'].toString();
    amount = json['amount'];
    totalAdmin = json['totalAdmin'];
    validity = json['validity'].toString();
    if (json['customerDetail'] != null) {
      customerDetail = <CustomerDetail>[];
      json['customerDetail'].forEach((dynamic v) {
        customerDetail.add(CustomerDetail.fromJson(v));
      });
    }
    if (json['billDetails'] != null) {
      billDetails = <BillDetails>[];
      json['billDetails'].forEach((dynamic v) {
        billDetails.add(BillDetails.fromJson(v));
      });
    }
    if (json['productDetails'] != null) {
      productDetails = <ProductDetails>[];
      json['productDetails'].forEach((dynamic v) {
        productDetails.add(ProductDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['inquiryId'] = inquiryId;
    data['refNumber'] = refNumber;
    data['accountNumber'] = accountNumber;
    data['productCode'] = productCode;
    data['customerName'] = customerName;
    data['productName'] = productName;
    data['category'] = category;
    data['amount'] = amount;
    data['totalAdmin'] = totalAdmin;
    data['validity'] = validity;
    if (customerDetail != null) {
      data['customerDetail'] = customerDetail.map((v) => v.toJson()).toList();
    }
    if (billDetails != null) {
      data['billDetails'] = billDetails.map((v) => v.toJson()).toList();
    }
    if (productDetails != null) {
      data['productDetails'] = productDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CustomerDetail {
  String key;
  String value;

  CustomerDetail({this.key, this.value});

  CustomerDetail.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['key'] = key;
    data['value'] = value;
    return data;
  }
}

class BillDetails {
  String billId;
  List<BillInfo> billInfo;
  String key;
  String value;

  BillDetails({this.billId, this.billInfo, this.key, this.value});

  BillDetails.fromJson(Map<String, dynamic> json) {
    billId = json['billId'];
    if (json['billInfo'] != null) {
      billInfo = <BillInfo>[];
      json['billInfo'].forEach((dynamic v) {
        billInfo.add(BillInfo.fromJson(v as Map<String, dynamic>));
      });
    }
    key = json['key'].toString();
    value = json['value'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['billId'] = billId;
    if (billInfo != null) {
      data['billInfo'] = billInfo.map((v) => v.toJson()).toList();
    }
    data['key'] = key;
    data['value'] = value;
    return data;
  }
}

class BillInfo {
  String key;
  String value;

  BillInfo({this.key, this.value});

  BillInfo.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['key'] = key;
    data['value'] = value;
    return data;
  }
}

class ProductDetails {
  String key;
  String value;

  ProductDetails({this.key, this.value});

  ProductDetails.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['key'] = key;
    data['value'] = value;
    return data;
  }
}
