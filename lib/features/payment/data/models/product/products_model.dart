part of '../_models.dart';

class ProductsModel extends Products {
  ProductsModel({
    String name,
    String code,
    String logo,
    int amount,
    String biller,
    String category,
    bool active,
    String type,
    String description,
  }) : super(
          name: name,
          code: code,
          logo: logo,
          amount: amount,
          biller: biller,
          category: category,
          active: active,
          type: type,
          description: description,
        );

  ProductsModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    code = json['code'];
    logo = json['logo'];
    amount = json['amount'] ?? 0;
    biller = json['biller'];
    category = json['category'];
    active = json['active'];
    type = json['type'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['code'] = this.code;
    data['logo'] = this.logo;
    data['amount'] = this.amount;
    data['biller'] = this.biller;
    data['category'] = this.category;
    data['active'] = this.active;
    data['type'] = this.type;
    data['description'] = this.description;
    return data;
  }
}
