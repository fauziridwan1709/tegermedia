part of '../_models.dart';

class ProductConfigModel extends ProductConfig {
  ProductConfigModel({
    String createdAt,
    String updatedAt,
    String id,
    ProductsModel products,
    String logo,
    int margin,
    bool isActive,
  }) : super(
          createdAt: createdAt,
          updatedAt: updatedAt,
          id: id,
          products: products,
          logo: logo,
          margin: margin,
          isActive: isActive,
        );

  ProductConfigModel.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    id = json['id'];
    products = json['products'] != null ? ProductsModel.fromJson(json['products']) : null;
    logo = json['logo'];
    margin = json['margin'];
    isActive = json['isActive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['id'] = id;
    if (products != null) {
      data['products'] = products.toJson();
    }
    data['logo'] = logo;
    data['margin'] = margin;
    data['isActive'] = isActive;
    return data;
  }
}
