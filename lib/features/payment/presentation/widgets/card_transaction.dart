part of '_widgets.dart';

class CardTransaction extends StatelessWidget {
  const CardTransaction({
    Key key,
    this.model,
    this.productType,
    this.category,
    this.onTap,
  }) : super(key: key);

  final Transaction model;
  final Function(StatusTransaction status) onTap;
  final String productType;
  final String category;

  @override
  Widget build(BuildContext context) {
    bool isExpired = false;
    if (model.expiredAt != '') {
      isExpired = DateTime.parse(model.expiredAt).toLocal().isBefore(DateTime.now().toLocal());
    }
    print('isexpired');
    print(isExpired);
    var status = StatusTransaction.fromStatus(model.paymentStatus, productType, isExpired);
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          color: SiswamediaTheme.white,
          borderRadius: BorderRadius.circular(6),
          boxShadow: [SiswamediaTheme.shadowContainer]),
      child: Column(
        children: <Widget>[
          HeightSpace(10),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  LocalizationService.formatDate('dd MMM yyyy', model.transactionDate),
                  style: boldBlack.copyWith(fontSize: 12),
                ),
                WidthSpace(10),
                Text(status.status,
                    style: SiswamediaTheme.countDown.copyWith(
                      color: status.color,
                      fontSize: 12,
                    )),
              ],
            ),
          ),
          Container(height: 1, color: SiswamediaTheme.lightBorder),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
            child: Content(
              'Batas akhir pembayaran',
              titleFontSize: 12,
              titleStyle: boldBlack.copyWith(fontSize: 12),
              child: Row(
                children: <Widget>[
                  if (model.paymentStatus == 'PENDING' ||
                      model.paymentStatus == 'CREATED' ||
                      model.paymentStatus == 'PROCESS') ...[
                    CountdownTimer(
                      endTime: DateTime.parse(model.expiredAt == ''
                              ? DateTime.now().toIso8601String()
                              : model.expiredAt)
                          .millisecondsSinceEpoch,
                      widgetBuilder: (_, time) {
                        if (time == null) {
                          // Navigator.popUntil(context, (route) => route.isFirst);
                          return Text(
                            'Berakhir',
                            style: SiswamediaTheme.countDown.copyWith(
                              color: Color(0xFF4B4B4B),
                              fontSize: 12,
                            ),
                          );
                        }
                        List<Widget> list = [];
                        if (time.days != null) {
                          list.add(Row(
                            children: <Widget>[
                              Text(time.days.toString()),
                            ],
                          ));
                        }
                        if (time.hours != null) {
                          list.add(Row(
                            children: <Widget>[
                              Text(
                                '${(time.hours < 10) ? '0' : ''}',
                                style: SiswamediaTheme.countDown,
                              ),
                              Text(time.hours.toString(), style: SiswamediaTheme.countDown),
                            ],
                          ));
                        }
                        if (time.min != null) {
                          list.add(Row(
                            children: <Widget>[
                              // Icon(Icons.sentiment_very_dissatisfied),

                              Text(
                                '${time.hours != null ? ' : ' : ''}${(time.min < 10) ? '0' : ''}',
                                style: SiswamediaTheme.countDown,
                              ),

                              Text(time.min.toString(), style: SiswamediaTheme.countDown),
                            ],
                          ));
                        }
                        if (time.sec != null) {
                          list.add(Row(
                            children: <Widget>[
                              if (time.min == null)
                                Text(
                                  '00',
                                  style: SiswamediaTheme.countDown,
                                ),
                              Text(' : ${(time.sec < 10) ? '0' : ''}',
                                  style: SiswamediaTheme.countDown),
                              Text(time.sec.toString(), style: SiswamediaTheme.countDown),
                            ],
                          ));
                        }
                        list.add(WidthSpace(10));
                        list.add(Text(
                          '( terhitung 24 jam dari "bayar sekarang" )',
                          style: GoogleFonts.openSans(
                              fontWeight: FontWeight.w500, fontSize: 10, color: Colors.black87),
                        ));
                        return Row(
                          children: list,
                        );
                      },
                    ),
                  ] else ...[
                    Text(
                      'SELESAI',
                      style: SiswamediaTheme.countDown.copyWith(
                        color: SiswamediaTheme.green,
                        fontSize: 12,
                      ),
                    ),
                  ]

                  // Text('( Terhitung 24 jam dari "bayar sekarang" )',
                  //     style: GoogleFonts.openSans(
                  //         fontWeight: FontWeight.w500, fontSize: 10, color: Colors.black87))
                ],
              ),
            ),
          ),
          Container(height: 1, color: SiswamediaTheme.lightBorder),
          HeightSpace(5),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/png/bank/${model.bank.toLowerCase()}.png',
                      height: 16,
                      width: 50,
                    ),
                    WidthSpace(20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        buildText('total tagihan',
                            fontSize: 10, color: Color(0xFF4B4B4B), fontWeight: FontWeight.normal),
                        HeightSpace(5),
                        buildText(
                            'RP. ${model.totalAmount == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(model.totalAmount)}',
                            fontSize: 16,
                            color: SiswamediaTheme.green,
                            fontWeight: FontWeight.bold),
                      ],
                    ),
                  ],
                ),
                AutoLayoutButton(
                  onTap: () {
                    onTap(status);
                  },
                  textColor: SiswamediaTheme.white,
                  backGroundColor: SiswamediaTheme.green,
                  text: 'Lihat',
                  radius: BorderRadius.circular(6),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                )
              ],
            ),
          ),
          HeightSpace(20),
        ],
      ),
    );
  }
}
