part of '_widgets.dart';

class PayBar extends StatelessWidget {
  final double price;
  final VoidCallback onPay;

  PayBar({this.price, this.onPay});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(color: SiswamediaTheme.white, boxShadow: [
          BoxShadow(color: SiswamediaTheme.grey.withOpacity(.1), spreadRadius: 1, blurRadius: 6)
        ]),
        child: Content(
          'Total Pembayaran',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Rp. ${price == 0.0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(price)}',
                style: GoogleFonts.openSans(
                    color: SiswamediaTheme.green, fontSize: 16, fontWeight: FontWeight.bold),
              ),
              InkWell(
                onTap: onPay,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 18, vertical: 6),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      gradient: onPay == null
                          ? LinearGradient(colors: [SiswamediaTheme.grey, SiswamediaTheme.grey])
                          : LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment(2, 0),
                              colors: [
                                  SiswamediaTheme.materialGreen[400],
                                  SiswamediaTheme.materialGreen[100]
                                ])),
                  child: Center(
                    child: Text(
                      'Bayar',
                      style: GoogleFonts.openSans(
                          color: SiswamediaTheme.white, fontWeight: FontWeight.w600, fontSize: 12),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
