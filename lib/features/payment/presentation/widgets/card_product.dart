part of '_widgets.dart';

class CardProduct extends StatelessWidget {
  final ProductConfig model;
  final Color textColor;
  final double fontSize;

  CardProduct({
    Key key,
    this.model,
    this.textColor,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              boxShadow: [SiswamediaTheme.shadowContainer],
              color: SiswamediaTheme.white),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width - 40,
                      padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12), topRight: Radius.circular(12)),
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment(1.5, 1),
                              colors: [
                                SiswamediaTheme.materialGreen[400],
                                SiswamediaTheme.materialGreen[100]
                              ])),
                      child: Content(
                        'Spp ' + model.products.name ?? '',
                        textColor: textColor ?? SiswamediaTheme.black,
                        titleFontSize: fontSize,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Text(
                                model.products.biller ?? '',
                                style:
                                    GoogleFonts.openSans(color: textColor ?? SiswamediaTheme.black),
                              ),
                            ),
                            AutoLayoutButton(
                              text: 'Bayar',
                              textColor: SiswamediaTheme.green,
                              backGroundColor: SiswamediaTheme.white,
                              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
                              radius: BorderRadius.circular(4),
                              onTap: () async {
                                var result = await showDialog<String>(
                                    context: context,
                                    builder: (_) {
                                      var controller = TextEditingController();
                                      return Dialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(12)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(20),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text('Account number',
                                                  style: FontTheme.openSans14w600Black),
                                              HeightSpace(10),
                                              TextField(
                                                controller: controller,
                                                decoration: InputDecoration(
                                                  fillColor: SiswamediaTheme.grey.shade200,
                                                  filled: true,
                                                  prefixIcon: Icon(Icons.phone),
                                                  enabledBorder: OutlineInputBorder(
                                                      borderRadius: BorderRadius.circular(8),
                                                      borderSide: BorderSide(
                                                          color: SiswamediaTheme.transparent)),
                                                  focusedBorder: OutlineInputBorder(
                                                      borderRadius: BorderRadius.circular(8),
                                                      borderSide: BorderSide(
                                                          color: SiswamediaTheme.transparent)),
                                                ),
                                              ),
                                              HeightSpace(15),
                                              Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Center(
                                                    child: AutoLayoutButton(
                                                      text: 'Confirm',
                                                      radius: BorderRadius.circular(6),
                                                      textColor: SiswamediaTheme.white,
                                                      backGroundColor: SiswamediaTheme.green,
                                                      padding: EdgeInsets.symmetric(
                                                          horizontal: 16, vertical: 7),
                                                      onTap: () => Navigator.pop<String>(
                                                          context, controller.text),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    });
                                if (result != null) {
                                  await context.push<void>(
                                      OrderInquirySpp(
                                        model: OrderProductRequestModel(
                                            productCode: model.products.code,
                                            productID: model.id,
                                            accountNumber: result),
                                      ),
                                      name: 'orderInquiry');
                                }
                              },
                            )
                          ],
                        ),
                      )),
                ],
              ),
              HeightSpace(10),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(child: Text(model.products.code ?? '')),
                    Text(
                      DateFormat('dd/MM/yyy').format(DateTime.parse(model.createdAt).toLocal()),
                      style: GoogleFonts.openSans(fontSize: 12, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              HeightSpace(15),
            ],
          ),
        ),
      ],
    );
  }
}
