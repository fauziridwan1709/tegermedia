part of '_widgets.dart';

class CardSubTotal extends StatelessWidget {
  final List<SppBill> data;
  final List<SppBill> selectedData;
  final Function(SppBill model) onChange;

  CardSubTotal({this.data, this.selectedData, this.onChange});

  @override
  Widget build(BuildContext context) {
    var totalPrice = selectedData.fold<double>(
        0, (double previousValue, SppBill element) => previousValue + element.total);
    return Container(
      color: SiswamediaTheme.white,
      child: ListView.separated(
          physics: ScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            if (index == data.length) {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Content(
                  'Subtotal',
                  titleFontSize: 12,
                  titleStyle: boldBlack,
                  child: Text(
                      'RP. ${totalPrice == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(totalPrice)}',
                      style: GoogleFonts.openSans(
                          fontWeight: FontWeight.bold, fontSize: 16, color: Colors.black)),
                ),
              );
            }
            var model = data[index];
            return CheckboxListTile(
              value: selectedData.contains(model),
              dense: true,
              contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              title: Text(
                'SPP Semester${model.userId.isEmpty ? '' : ' - ' + model.userId} - ${model.schoolId} - ${bulanFull[model.month - 1]}',
                style: GoogleFonts.openSans(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              subtitle: Padding(
                padding: EdgeInsets.only(top: 5.0),
                child: Text(
                  'RP. ${NumberFormat.currency(customPattern: '#,###').format(model.total)}',
                  style: GoogleFonts.openSans(
                      fontSize: 16, fontWeight: FontWeight.bold, color: SiswamediaTheme.green),
                ),
              ),
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (bool value) {
                onChange(model);
              },
            );
          },
          separatorBuilder: (context, _) {
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Divider(color: Colors.grey.withOpacity(.4)),
            );
          },
          itemCount: data.length + 1),
    );
  }
}
