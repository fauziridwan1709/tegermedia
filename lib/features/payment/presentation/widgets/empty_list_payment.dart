part of '_widgets.dart';

Widget EmptyListHistory() {
  return ListView(children: [
    HeightSpace(100),
    Image.asset(
      'assets/images/png/payment_empty.png',
      height: 180,
      width: 180,
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Kosong!', style: SiswamediaTheme.boldBlack),
      ],
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Tidak ada history',
            style: SiswamediaTheme.descBlack.copyWith(fontSize: 12, color: Colors.grey)),
      ],
    ),
  ]);
}
