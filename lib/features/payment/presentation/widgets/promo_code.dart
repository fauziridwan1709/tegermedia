part of '_widgets.dart';

class PromoCode extends StatelessWidget {
  final VoidCallback onSubmit;
  final TextEditingController controller;
  final FocusNode fNode;

  const PromoCode({this.onSubmit, this.controller, this.fNode});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
            color: SiswamediaTheme.white,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Content(
              'Kode promo',
              titleFontSize: 12,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width * .55,
                      padding: EdgeInsets.only(bottom: 20),
                      child: TextField(
                        focusNode: fNode,
                        controller: controller,
                        decoration: InputDecoration(
                          hintStyle: GoogleFonts.openSans(
                              fontSize: 12,
                              color: Color(0xFFDBDBDB),
                              fontWeight: FontWeight.w500),
                          hintText: 'Masukkan kode promo',
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: SiswamediaTheme.grey.withOpacity(.4))),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: SiswamediaTheme.grey.withOpacity(.4))),
                        ),
                      )),
                  WidthSpace(10),
                  AutoLayoutButton(
                    onTap: onSubmit,
                    radius: BorderRadius.circular(6),
                    textColor: SiswamediaTheme.white,
                    text: 'Terapkan',
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                    backGroundColor: SiswamediaTheme.green,
                  )
                ],
              ),
            )),
      ],
    );
  }
}
