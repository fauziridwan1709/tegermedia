part of '_widgets.dart';

class CardBill extends StatelessWidget {
  final SppBill model;
  final Color textColor;
  final double fontSize;
  final bool isAdded;
  final VoidCallback onTap;

  CardBill(
      {Key key,
      @required this.onTap,
      this.model,
      this.textColor,
      this.fontSize,
      this.isAdded = false})
      : assert(onTap != null, 'onTap cannot be null'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              boxShadow: [SiswamediaTheme.shadowContainer],
              color: SiswamediaTheme.white),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: [
                  Container(
                      width: MediaQuery.of(context).size.width - 40,
                      padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(12), topRight: Radius.circular(12)),
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment(1.5, 1),
                              colors: [
                                SiswamediaTheme.materialGreen[400],
                                SiswamediaTheme.materialGreen[100]
                              ])),
                      child: Content(
                        'Rp. ' + NumberFormat.currency(customPattern: '#,###').format(model.total),
                        textColor: textColor ?? SiswamediaTheme.black,
                        titleFontSize: fontSize,
                        child: Text(
                          'Tagihan SPP ${model.schoolId}',
                          style: GoogleFonts.openSans(color: textColor ?? SiswamediaTheme.black),
                        ),
                      )),
                ],
              ),
              HeightSpace(10),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      model.schoolId,
                      style: GoogleFonts.openSans(
                          fontSize: 12, color: SiswamediaTheme.green, fontWeight: FontWeight.w600),
                    ),
                    Text(
                      'Jatuh Tempo',
                      style: GoogleFonts.openSans(
                          fontSize: 12, color: SiswamediaTheme.green, fontWeight: FontWeight.w600),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(model.userId),
                    Text(
                      DateFormat('dd/MM/yyy').format(DateTime.parse(model.dueDate).toLocal()),
                      style: GoogleFonts.openSans(fontSize: 12, fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              HeightSpace(15),
            ],
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: AutoLayoutButton(
                key: key,
                text: isAdded ? 'Batalkan' : 'Tambahkan ke pembayaran',
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                textColor: isAdded ? SiswamediaTheme.white : SiswamediaTheme.green,
                textAlign: TextAlign.center,
                onTap: onTap,
                border: Border.all(color: SiswamediaTheme.green),
                radius: BorderRadius.circular(6),
                backGroundColor: isAdded ? SiswamediaTheme.green : SiswamediaTheme.white,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
