part of '_widgets.dart';

class PhoneNumberField extends StatelessWidget {
  const PhoneNumberField({Key key, this.title, this.hint, this.controller, this.onChange})
      : super(key: key);

  final String title;
  final String hint;
  final TextEditingController controller;
  final Function(String) onChange;
  @override
  Widget build(BuildContext context) {
    return Content(
      'Masukkan nomor HP E-Wallet anda',
      titleStyle: FontTheme.openSans12w700Black,
      height: 15,
      child: TextField(
        style: FontTheme.rubik16w500black1,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.phone),
          labelText: 'Masukkan Nomor Ponsel',
          labelStyle: FontTheme.rubik12w400black4,
          border: OutlineInputBorder(
            borderSide: BorderSide(color: SiswamediaTheme.grey600),
            borderRadius: BorderRadius.circular(4),
          ),
        ),
        controller: controller,
        keyboardType: TextInputType.phone,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        onChanged: onChange,
      ),
    );
  }
}
