part of '_widgets.dart';

class SummaryCard extends StatelessWidget {
  final List<SppBill> selectedBills;
  final VoucherAvailability voucher;
  final Function(double totalPrice) onTap;

  const SummaryCard({this.selectedBills, this.voucher, this.onTap});

  @override
  Widget build(BuildContext context) {
    var totalPrice = selectedBills.fold<double>(
        0, (double previousValue, SppBill element) => previousValue + element.total);
    var voucherValue = voucher == null ? 0 : voucher.discountValue;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      color: SiswamediaTheme.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          buildText('Ringkasan Belanja',
              fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
          ...List.generate(
            selectedBills.length,
            (index) {
              var model = selectedBills[index];
              return Column(
                children: <Widget>[
                  HeightSpace(10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: buildText(
                            'SPP${model.userId.isEmpty ? '' : ' ' + model.userId} ${model.schoolId} - ${bulanFull[model.month - 1]}',
                            fontSize: 12,
                            color: Color(0xFF989898),
                            fontWeight: FontWeight.w500),
                      ),
                      WidthSpace(15),
                      buildText(
                          'Rp. ${NumberFormat.currency(customPattern: '#,###').format(model.total)}',
                          fontSize: 12,
                          color: Color(0xFF4B4B4B),
                          fontWeight: FontWeight.bold),
                    ],
                  ),
                ],
              );
            },
          ),
          HeightSpace(10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              buildText('Potongan kode promo',
                  fontSize: 12, color: Color(0xFF989898), fontWeight: FontWeight.w500),
              buildText(
                  '- Rp. ${voucher != null ? NumberFormat.currency(customPattern: '#,###').format(voucher.discountValue) : '0'}',
                  fontSize: 12,
                  color: Color(0xFF4B4B4B),
                  fontWeight: FontWeight.bold),
            ],
          ),
          HeightSpace(10),
          buildText('Total Tagihan',
              fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
          HeightSpace(10),
          buildText(
              'RP. ${totalPrice == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(totalPrice - voucherValue)}',
              fontSize: 16,
              color: SiswamediaTheme.green,
              fontWeight: FontWeight.bold),
          HeightSpace(20),
          Row(
            children: [
              Expanded(
                child: AutoLayoutButton(
                  onTap: selectedBills.isEmpty ? null : () => onTap(totalPrice - voucherValue),
                  padding: EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                  text: 'Pilih Pembayaran',
                  textAlign: TextAlign.center,
                  textColor: SiswamediaTheme.white,
                  backGroundColor: SiswamediaTheme.green,
                  radius: BorderRadius.circular(6),
                ),
              ),
            ],
          ),
          HeightSpace(20),
        ],
      ),
    );
  }
}

Widget buildText(String text, {double fontSize, Color color, FontWeight fontWeight}) {
  return Text(text,
      style: GoogleFonts.openSans(fontSize: fontSize, color: color, fontWeight: fontWeight));
}
