part of '_pages.dart';

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends BaseStateful<PaymentPage> {
  final spp = Injector.getAsReactive<SppState>();
  final auth = GlobalState.auth();
  final scrollController = ScrollController();
  int _sliderIndex;
  var imageList = <String>['best-deal.png'];
  var sppList = <SppBill>[];
  // var paymentMenu = <String>['Pulsa'];
  var paymentMenu = <String>['Spp', 'Pulsa'];
  // var paymentMenu = <String>['Spp', 'Acara', 'Kegiatan', 'Perlengkapan', 'Pulsa'];

  @override
  void init() {
    _sliderIndex = 0;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      S().init(context);
      // retrieveData();
    });
  }

  @override
  void dispose() {
    scrollController.removeListener(() {});
    scrollController.dispose();
    super.dispose();
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(1),
      child: Container(),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    var size = sizeInfo.screenSize;
    return SafeArea(
      child: StateBuilder<AuthState>(
          observe: () => auth,
          builder: (context, snapshot) {
            if (auth.state.isLogin) {
              return Stack(
                children: [
                  CustomScrollView(
                    controller: scrollController,
                    slivers: <Widget>[
                      SliverAppBar(
                        toolbarHeight: AppBar().preferredSize.height * 1.5,
                        floating: true,
                        titleSpacing: 0,
                        shadowColor: Colors.transparent,
                        title: AppBarUI(
                          onLogin: () async {
                            await auth.state.login(context);
                          },
                          onTapNotif: () => dialogDev(context),
                          isPayment: true,
                          tag: 'paymentPP',
                        ),
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate([
                          HeightSpace(10),
                          CarouselSlider(
                            items: imageSliders(),
                            options: CarouselOptions(
                                autoPlay: false,
                                enlargeCenterPage: false,
                                aspectRatio: 2.4,
                                viewportFraction: .9,
                                enableInfiniteScroll: false,
                                onPageChanged: (index, reason) {
                                  setState(() {
                                    print(index);
                                    _sliderIndex = index;
                                  });
                                }),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: imageList.map((url) {
                              var index = imageList.indexOf(url);
                              return Container(
                                width: 8.0,
                                height: 8.0,
                                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _sliderIndex == index
                                      ? Color.fromRGBO(0, 0, 0, 0.9)
                                      : Color.fromRGBO(0, 0, 0, 0.4),
                                ),
                              );
                            }).toList(),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: List.generate(paymentMenu.length, (index) {
                                return InkWell(
                                  onTap: () {
                                    if (index == 0) {
                                      context.push<void>(PaymentSppPage());
                                    } else if (index == 1) {
                                      _dialogBuilder(context);
                                    }
                                  },
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                          height: 50,
                                          width: 50,
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.symmetric(horizontal: 5),
                                          decoration: BoxDecoration(
                                            color: SiswamediaTheme.white,
                                            boxShadow: [SiswamediaTheme.shadowContainer],
                                            borderRadius: BorderRadius.circular(8),
                                          ),
                                          child: Image.asset(
                                              'assets/images/png/payment_${paymentMenu[index]}.png')),
                                      HeightSpace(5),
                                      SizedBox(
                                        width: 50,
                                        child: AutoSizeText(
                                          paymentMenu[index],
                                          minFontSize: 6,
                                          maxFontSize: 10,
                                          textAlign: TextAlign.center,
                                          style: semiBlack.copyWith(fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              }),
                            ),
                          ),
                          InkWell(
                            onTap: () => context.push<void>(ListSchoolPage()),
                            child: Container(
                              height: 150,
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    bottom: 0,
                                    child: Container(
                                      height: 100,
                                      width: size.width - 40,
                                      decoration: BoxDecoration(
                                        color: SiswamediaTheme.green,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    left: 0,
                                    child:
                                        Image.asset('assets/icons/pantau/pantau.png', height: 140),
                                  ),
                                  Positioned(
                                    right: 0,
                                    top: 80,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 20.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Text('Pendaftaran siswa baru',
                                              style: semiWhite.copyWith(fontSize: 12)),
                                          Text('Kllik disini!',
                                              style: semiWhite.copyWith(fontSize: 12)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          HeightSpace(100),
                        ]),
                      )
                    ],
                  ),
                  // PayBar(
                  //   price: snapshot.state.totalPrice,
                  //   onPay: () => context.push<void>(PaymentSummary(bills: sppList)),
                  // )
                ],
              );
            }
            return sectionNotLogin(context: context, login: () => auth.state.login(context));
          }),
    );
  }

  void _dialogBuilder(BuildContext context) async {
    bool resultDialog = await showDialog<bool>(
        context: context,
        builder: (_) {
          return QuizDialog(
            image: 'assets/images/png/payment_info.png',
            title: 'Pilih jenis Tagihan',
            description: '',
            actions: ['Pasca Bayar', 'Pra Bayar'],
            type: DialogType.textImage,
            onTapFirstAction: () async {
              Navigator.pop(context, true);
            },
            onTapSecondAction: () {
              Navigator.pop(context, false);
            },
          );
        });
    if (resultDialog) {
      return context.push<void>(PrePaidPulsesPage());
    } else {
      return context.push<void>(PulsaPage());
    }
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  List<Widget> imageSliders() {
    return imageList
        .map((item) => Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Image.asset('assets/images/png/$item', fit: BoxFit.cover),
                    ],
                  )),
            ))
        .toList();
  }

  @override
  Future<bool> onBackPressed() async {
    // TODO: implement onBackPressed
    return true;
  }

  Future<void> onAddToPayment(int value, bool isAdded) async {
    await spp.setState((s) {
      isAdded ? s.addPrice(value) : s.removePrice(value);
    });
  }
}
