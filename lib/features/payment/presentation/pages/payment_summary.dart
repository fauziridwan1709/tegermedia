part of '_pages.dart';

class PaymentSummary extends StatefulWidget {
  final List<SppBill> bills;
  final String origin;
  final String productType;

  const PaymentSummary({this.bills, this.origin, this.productType});

  @override
  _PaymentSummaryState createState() => _PaymentSummaryState();
}

class _PaymentSummaryState extends BaseState<PaymentSummary, SppState> {
  final spp = Injector.getAsReactive<SppState>();
  TextEditingController _promoController;
  var fNode = FocusNode();
  List<SppBill> _bills = [];
  var selectedBill = <SppBill>[];

  @override
  void init() {
    _promoController = TextEditingController();
    if (widget.bills != null) {
      selectedBill.addAll(widget.bills);
      _bills = widget.bills;
    }
  }

  @override
  void dispose() {
    spp.setState((s) => s.refreshVoucher());
    super.dispose();
  }

  @override
  Future<void> retrieveData() async {}

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute(backgroundColor: Color(0xFFF8F8F8));
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_rounded),
        color: SiswamediaTheme.green,
        onPressed: () async => Navigator.pop(context),
      ),
      title: Text(
        'Pembayaran',
        style: GoogleFonts.openSans(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
            width: MediaQuery.of(context).size.width, height: 1, color: Colors.grey.shade300),
      ),
    );
  }

  @override
  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<SppState> snapshot, SizingInformation sizeInfo) {
    var totalPrice = selectedBill.fold<double>(
        0, (double previousValue, SppBill element) => previousValue + element.total);
    return ListView(
      shrinkWrap: true,
      children: [
        Container(height: 10, color: SiswamediaTheme.white),
        CardSubTotal(
            data: _bills,
            selectedData: selectedBill,
            onChange: (model) {
              setState(() => selectedBill.contains(model)
                  ? selectedBill.remove(model)
                  : selectedBill.add(model));
            }),
        Container(),
        HeightSpace(20),
        PromoCode(
          fNode: fNode,
          onSubmit: () async {
            // ignore: unawaited_futures
            showDialog<void>(
                context: context, barrierDismissible: false, builder: (_) => LoadingView());
            if (fNode.hasFocus) {
              fNode.unfocus();
            }
            await snapshot.setState((s) => s.checkVoucherAvailability(_promoController.text),
                onError: (_, dynamic __) {
              context.pop();
              CustomFlushBar.errorFlushBar('Unexpected error', context);
            }, onData: (_, data) {
              context.pop();
              print('minspend ${data.voucher.minSpend}');
              print('total $totalPrice');
              if (data.voucher.minSpend > totalPrice) {
                // snapshot.setState((s) => s.refreshVoucher());
                CustomFlushBar.errorFlushBar(
                    'minimum spend untuk voucher ini adalah ${data.voucher.minSpend}', context);
                snapshot.setState((s) => s.refreshVoucher());
              } else {
                CustomFlushBar.successFlushBar('Code bisa digunakan', context);
              }
              // if (fNode.hasFocus) {
              //   fNode.unfocus();
              // }
            });
          },
          controller: _promoController,
        ),
        HeightSpace(20),
        Align(
            alignment: Alignment.bottomCenter,
            child: SummaryCard(
                onTap: (total) => context.push<void>(PaymentMethodPage(
                    origin: widget.origin,
                    productType: widget.productType,
                    bills: selectedBill,
                    totalPrice: total,
                    voucher: snapshot.state.voucher)),
                selectedBills: selectedBill,
                voucher: snapshot.state.voucher)),
      ],
    );
  }

  @override
  Widget buildWideLayout(
      BuildContext context, ReactiveModel<SppState> snapshot, SizingInformation sizeInfo) {
    // TODO: implement buildWideLayout
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }
}
