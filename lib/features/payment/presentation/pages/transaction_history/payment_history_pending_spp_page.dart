part of '../_pages.dart';

class PaymentHistoryPendingSppPage extends StatefulWidget {
  @override
  _PaymentHistoryPendingSppPageState createState() => _PaymentHistoryPendingSppPageState();
}

class _PaymentHistoryPendingSppPageState
    extends BasePaginationState<PaymentHistoryPendingSppPage, TransactionSppPendingState> {
  @override
  void init() {
    // TODO: implement init
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Future<void> retrieveData() async {
    await thisState.setState((s) => s.retrieveData('PENDING'));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return EmptyAppBar();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, ReactiveModel<TransactionSppPendingState> snapshot,
      SizingInformation sizeInfo) {
    return Container(
      child: RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: retrieveData,
        child: WhenRebuilder<TransactionSppPendingState>(
          observe: () => snapshot,
          onIdle: () => WaitingView(),
          onWaiting: () => WaitingView(),
          onError: (dynamic error) => ErrorView(error: error),
          onData: (data) {
            return EmptyView(
                title: 'Empty',
                description: 'Tidak ditemukan Transaksi spp yang belum selesai',
                isEmpty: (data.listPending ?? []).isEmpty,
                child: ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  shrinkWrap: true,
                  controller: scrollController,
                  itemCount:
                      data.hasReachedMax ? data.listPending.length : data.listPending.length + 1,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  itemBuilder: (_, index) {
                    if (data.listPending.length == index) {
                      return LoadingView();
                    }
                    var e = data.listPending[index];
                    return CardTransaction(
                      model: e,
                      onTap: (status) => context.push<void>(PaymentHistoryDetailPage(
                        model: e,
                        status: status,
                      )),
                      productType: ProductTypeConst.market,
                    );
                  },
                ));
          },
        ),
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, ReactiveModel<TransactionSppPendingState> snapshot,
      SizingInformation sizeInfo) {
    return buildNarrowLayout(context, snapshot, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  void onScroll() {
    completer.complete();
    thisState.state.retrieveMoreData().then((value) {
      completer = Completer<Null>();
      thisState.notify();
    });
  }

  @override
  bool scrollCondition() {
    return !thisState.state.hasReachedMax;
  }
}
