part of '../_pages.dart';

//PaymentHistoryPendingPage
class PaymentHistoryPendingPage extends StatefulWidget {
  @override
  _PaymentHistoryPendingPageState createState() => _PaymentHistoryPendingPageState();
}

class _PaymentHistoryPendingPageState extends BaseStateful<PaymentHistoryPendingPage> {
  int _selected;
  List<Widget> _children;

  @override
  void init() {
    _children = [
      PaymentHistoryPendingSppPage(),
      PaymentHistoryPendingPulsaPage(),
      PaymentHistoryPendingPostpaidPage(),
      PaymentHistoryPendingEdukasiPage(),
    ];
    _selected = 0;
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(0),
      child: Container(),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return Column(
      children: [
        CustomChipBar(
          currentIndex: _selected,
          onTap: (index) {
            setState(() => _selected = index);
          },
          chips: [
            CustomChip(
              label: 'Spp',
            ),
            CustomChip(
              label: 'Pulsa',
            ),
            CustomChip(
              label: 'Pasca bayar',
            ),
            CustomChip(
              label: 'Edukasi',
            ),
          ],
        ),
        SizedBox(
          width: 12,
        ),
        Expanded(
          child: _children.elementAt(_selected),
        ),
      ],
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    // TODO: implement onBackPressed
    return true;
  }
}
