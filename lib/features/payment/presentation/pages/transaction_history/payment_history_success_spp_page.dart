part of '../_pages.dart';

class PaymentHistorySuccessSppPage extends StatefulWidget {
  @override
  _PaymentHistorySuccessSppPageState createState() => _PaymentHistorySuccessSppPageState();
}

class _PaymentHistorySuccessSppPageState
    extends BasePaginationState<PaymentHistorySuccessSppPage, TransactionSppSuccessState> {
  @override
  void init() {}

  @override
  Future<void> retrieveData() async {
    await thisState.setState((s) => s.retrieveData('success'));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return EmptyAppBar();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, ReactiveModel<TransactionSppSuccessState> snapshot,
      SizingInformation sizeInfo) {
    return Container(
      child: RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: retrieveData,
        child: WhenRebuilder<TransactionSppSuccessState>(
          observe: () => snapshot,
          onIdle: () => WaitingView(),
          onWaiting: () => WaitingView(),
          onError: (dynamic error) => ErrorView(error: error),
          onData: (data) {
            return EmptyView(
                title: 'Empty',
                description: 'Tidak ditemukan Transaksi spp yang selesai',
                isEmpty: (data.listSuccess ?? []).isEmpty,
                child: ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  shrinkWrap: true,
                  controller: scrollController,
                  itemCount:
                      data.hasReachedMax ? data.listSuccess.length : data.listSuccess.length + 1,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  itemBuilder: (_, index) {
                    if (data.listSuccess.length == index) {
                      return LoadingView();
                    }
                    var e = data.listSuccess[index];
                    return CardTransaction(
                      model: e,
                      onTap: (status) => context.push<void>(PaymentHistoryDetailPage(model: e, status: status)),
                      productType: ProductTypeConst.market,
                    );
                  },
                ));
          },
        ),
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, ReactiveModel<TransactionSppSuccessState> snapshot,
      SizingInformation sizeInfo) {
    return buildNarrowLayout(context, snapshot, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  void onScroll() {
    completer.complete();
    thisState.state.retrieveMoreData().then((value) {
      completer = Completer<Null>();
      thisState.notify();
    });
  }

  @override
  bool scrollCondition() {
    return !thisState.state.hasReachedMax;
  }
}
