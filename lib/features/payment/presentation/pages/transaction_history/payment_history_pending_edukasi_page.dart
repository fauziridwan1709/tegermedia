part of '../_pages.dart';

class PaymentHistoryPendingEdukasiPage extends StatefulWidget {
  @override
  _PaymentHistoryPendingEdukasiPageState createState() => _PaymentHistoryPendingEdukasiPageState();
}

class _PaymentHistoryPendingEdukasiPageState
    extends BasePaginationState<PaymentHistoryPendingEdukasiPage, TransactionEdukasiPendingState> {
  @override
  void init() {
    // TODO: implement init
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Future<void> retrieveData() async {
    await thisState.setState((s) => s.retrieveData('PENDING'));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return EmptyAppBar();
  }

  @override
  Widget buildNarrowLayout(BuildContext context,
      ReactiveModel<TransactionEdukasiPendingState> snapshot, SizingInformation sizeInfo) {
    return Container(
      child: RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: retrieveData,
        child: WhenRebuilder<TransactionEdukasiPendingState>(
          observe: () => snapshot,
          onIdle: () => WaitingView(),
          onWaiting: () => WaitingView(),
          onError: (dynamic error) => ErrorView(error: error),
          onData: (data) {
            return EmptyView(
                title: 'Empty',
                description: 'Tidak ditemukan Transaksi edukasi yang belum selesai',
                isEmpty: (data.listHistory ?? []).isEmpty,
                child: ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  shrinkWrap: true,
                  controller: scrollController,
                  itemCount:
                      data.hasReachedMax ? data.listHistory.length : data.listHistory.length + 1,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  itemBuilder: (_, index) {
                    if (data.listHistory.length == index) {
                      return LoadingView();
                    }
                    var e = data.listHistory[index];
                    var model = Transaction(
                      iD: e.id,
                      paymentStatus: e.ayopopStatus,
                      bank: e.payment.bankCd,
                      expiredAt: e.payment.expiredAt,
                      transactionDate:
                          e.payment.transactionDate ?? DateTime.now().toIso8601String(),
                      paymentProvider: e.payment.paymentProvider,
                      totalAmount: e.total,
                      trxID: e.product.inquiryId.toString(),
                    );
                    return CardTransaction(
                      model: model,
                      onTap: (status) => context
                          .push<void>(PaymentHistoryDetailPage(model: model, status: status)),
                      productType: ProductTypeConst.ppob,
                    );
                  },
                ));
          },
        ),
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context,
      ReactiveModel<TransactionEdukasiPendingState> snapshot, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, snapshot, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  void onScroll() {
    completer.complete();
    thisState.state.retrieveMoreData().then((value) {
      completer = Completer<Null>();
      thisState.notify();
    });
  }

  @override
  bool scrollCondition() {
    return !thisState.state.hasReachedMax;
  }
}
