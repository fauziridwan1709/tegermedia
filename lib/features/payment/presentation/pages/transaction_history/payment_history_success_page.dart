part of '../_pages.dart';

class PaymentHistorySuccessPage extends StatefulWidget {
  @override
  _PaymentHistorySuccessPageState createState() => _PaymentHistorySuccessPageState();
}

class _PaymentHistorySuccessPageState extends BaseStateful<PaymentHistorySuccessPage> {
  int _selected;
  List<Widget> _children;

  @override
  void init() {
    _children = [
      PaymentHistorySuccessSppPage(),
      PaymentHistorySuccessPulsaPage(),
      PaymentHistorySuccessPostpaidPage(),
      PaymentHistorySuccessEdukasiPage(),
    ];
    _selected = 0;
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(0),
      child: Container(),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return Column(
      children: [
        CustomChipBar(
          currentIndex: _selected,
          onTap: (index) {
            setState(() => _selected = index);
          },
          chips: [
            CustomChip(
              label: 'Spp',
            ),
            CustomChip(
              label: 'Pulsa',
            ),
            CustomChip(
              label: 'Pasca Bayar',
            ),
            CustomChip(
              label: 'Edukasi',
            ),
          ],
        ),
        SizedBox(
          width: 12,
        ),
        Expanded(
          child: _children.elementAt(_selected),
        ),
      ],
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    // TODO: implement onBackPressed
    return true;
  }
}
