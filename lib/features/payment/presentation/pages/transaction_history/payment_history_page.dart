part of '../_pages.dart';

class PaymentHistoryPage extends StatefulWidget {
  @override
  _PaymentHistoryPageState createState() => _PaymentHistoryPageState();
}

class _PaymentHistoryPageState extends BaseStateful<PaymentHistoryPage>
    with SingleTickerProviderStateMixin {
  TabController _controller;

  @override
  void init() {
    _controller = TabController(initialIndex: 0, length: 2, vsync: this);
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(
        'Transaction History',
        style: GoogleFonts.openSans(
            fontWeight: FontWeight.bold, fontSize: 18, color: SiswamediaTheme.black),
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      bottom: TabBar(
        controller: _controller,
        unselectedLabelColor: Colors.grey,
        labelColor: Colors.black,
        labelStyle: TextStyle(fontWeight: FontWeight.bold),
        indicatorWeight: 4,
        tabs: [
          Tab(text: 'Pending'),
          Tab(text: 'Riwayat'),
        ],
      ),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return TabBarView(controller: _controller, children: <Widget>[
      PaymentHistoryPendingPage(),
      PaymentHistorySuccessPage(),
    ]);
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }
}
