part of '../_pages.dart';

class PaymentHistoryDetailPage extends StatefulWidget {
  final Transaction model;
  final bool isPulses;
  final StatusTransaction status;

  const PaymentHistoryDetailPage({this.model, this.isPulses = false, this.status});
  @override
  _PaymentHistoryDetailPageState createState() => _PaymentHistoryDetailPageState();
}

class _PaymentHistoryDetailPageState extends BaseStateful<PaymentHistoryDetailPage> {
  final marketState = GlobalState.market();

  String get expireAt => widget.model.expiredAt;

  @override
  void init() {
    // TODO: implement init
  }

  @override
  ScaffoldAttribute buildAttribute() {
    // TODO: implement buildAttribute
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Detail Payment',
            style: GoogleFonts.openSans(
                fontWeight: FontWeight.bold, fontSize: 18, color: SiswamediaTheme.black),
          ),
          Text(
            'Order ID : ${widget.model.trxID}',
            style: GoogleFonts.openSans(
                fontWeight: FontWeight.normal, fontSize: 12, color: SiswamediaTheme.black),
          ),
        ],
      ),
      centerTitle: false,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 1,
          color: Colors.grey.shade300,
        ),
      ),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    bool isExpired = false;
    String expireTm = expireAt.isNotEmpty ? expireAt : DateTime.now().toLocal().toIso8601String();
    if (widget.model.expiredAt != '') {
      isExpired =
          DateTime.parse(widget.model.expiredAt).toLocal().isBefore(DateTime.now().toLocal());
    }
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      children: [
        if (!isExpired)
          Container(
            margin: const EdgeInsets.all(12),
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7.5),
            color: Color(0xFFFFF8F8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Selesaikan pembayaran dalam', style: FontTheme.openSans12w500Black),
                WidthSpace(25),
                CustomCountdown(
                  expirationTime: expireTm,
                  onExpiredWidget: Text('Berakhir'),
                ),
              ],
            ),
          ),
        HeightSpace(10),
        Content(
          'Tanggal transaksi',
          titleStyle: boldBlack,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            decoration: BoxDecoration(
              border: Border.all(color: SiswamediaTheme.lightBorder),
              borderRadius: BorderRadius.circular(6),
            ),
            child: Row(
              children: [
                Text(
                  DateFormat('dd MMM yyy').format(
                    DateTime.parse(widget.model.transactionDate).toLocal(),
                  ),
                  style: boldBlack.copyWith(fontSize: 12),
                ),
              ],
            ),
          ),
        ),
        Content(
          'Pembayaran',
          titleStyle: boldBlack,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            decoration: BoxDecoration(
              border: Border.all(color: SiswamediaTheme.lightBorder),
              borderRadius: BorderRadius.circular(6),
            ),
            child: _mapPembayaranWidget(),
          ),
        ),
        Content(
          'Status transaksi',
          titleStyle: boldBlack,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            decoration: BoxDecoration(
              border: Border.all(color: SiswamediaTheme.lightBorder),
              borderRadius: BorderRadius.circular(6),
            ),
            child: Row(
              children: <Widget>[
                SVGImage(
                  widget.status.asset,
                  color: widget.status.color,
                ),
                WidthSpace(10),
                Text(
                  widget.status.status,
                  style: FontTheme.rubik16w500black1.copyWith(
                    fontSize: 12,
                    color: widget.status.color,
                  ),
                ),
              ],
            ),
          ),
        ),
        HeightSpace(10),
        if (widget.model.paymentStatus == 'PENDING' && !isExpired) ...[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
            child: _buildText('Nomor Virtual Akun',
                fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
          ),
          HeightSpace(10),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _buildText('${widget.model.userPayNumber}',
                    fontSize: 16, color: SiswamediaTheme.blue, fontWeight: FontWeight.bold),
                AutoLayoutButton(
                  radius: BorderRadius.circular(6),
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  backGroundColor: SiswamediaTheme.green,
                  onTap: () {
                    Clipboard.setData(ClipboardData(text: widget.model.userPayNumber));
                    CustomFlushBar.fullEventFlushBar('Kode berhasil dicopy!', context);
                  },
                  text: 'Salin',
                  textColor: SiswamediaTheme.white,
                )
              ],
            ),
          ),
          Divider(color: SiswamediaTheme.grey),
        ],
        if (widget.model.items != null) ...[
          HeightSpace(10),
          Text(
            'Item',
            style: boldBlack,
          ),
          HeightSpace(5),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            decoration: BoxDecoration(
              border: Border.all(color: SiswamediaTheme.lightBorder),
              borderRadius: BorderRadius.circular(6),
            ),
            child: Column(
              children: [
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: widget.model.items.length + 1,
                  itemBuilder: (context, index) {
                    var e = index == widget.model.items.length
                        ? TransactionItemModel(name: 'Admin', unitPrice: widget.model.adminFee)
                        : widget.model.items[index];
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 2),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text((index + 1).toString() + '. ',
                                    style: FontTheme.openSans12w500Black),
                                Expanded(child: Text(e.name, style: FontTheme.openSans12w500Black)),
                              ],
                            ),
                          ),
                          WidthSpace(10),
                          _buildText(
                              'RP. ${e.unitPrice == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(e.unitPrice)}',
                              fontSize: 12,
                              color: SiswamediaTheme.green,
                              fontWeight: FontWeight.bold)
                        ],
                      ),
                    );
                  },
                ),
                if (widget.isPulses)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text((2).toString() + '. ', style: FontTheme.openSans12w500Black),
                          Text('Harga Voucher', style: FontTheme.openSans12w500Black),
                        ],
                      ),
                      _buildText(
                          'RP. ${widget.model.invoiceAmount == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(widget.model.invoiceAmount)}',
                          fontSize: 12,
                          color: SiswamediaTheme.green,
                          fontWeight: FontWeight.bold)
                    ],
                  ),
              ],
            ),
          ),
          HeightSpace(20),
          AutoLayoutButton(
            text: 'Cancel Transaction',
            onTap: () {
              marketState.setState((s) => s.cancelTransaction(widget.model.paymentID, context));
            },
            backGroundColor: SiswamediaTheme.red,
            textColor: SiswamediaTheme.white,
            textAlign: TextAlign.center,
            radius: BorderRadius.circular(6),
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            fontSize: 14,
          )
        ]
      ],
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  Widget _buildText(String text, {double fontSize, Color color, FontWeight fontWeight}) {
    return Text(text,
        style: GoogleFonts.openSans(fontSize: fontSize, color: color, fontWeight: fontWeight));
  }

  Widget _mapPembayaranWidget() {
    if (widget.model.paymentMethod != 'va') {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.asset(
                'assets/images/png/bank/${widget.model.bank.toLowerCase()}.png',
                height: 16,
                width: 50,
              ),
              WidthSpace(20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildText('Total Tagihan',
                      fontSize: 14, color: Color(0xFF5B5B5B), fontWeight: FontWeight.bold),
                  HeightSpace(5),
                  _buildText(
                      'RP. ${widget.model.totalAmount == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(widget.model.totalAmount)}',
                      fontSize: 14,
                      color: SiswamediaTheme.green,
                      fontWeight: FontWeight.bold),
                ],
              ),
            ],
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Nomor Rekening Virtual Account ${widget.model.bankName}',
              style: FontTheme.rubik12w400black4),
          HeightSpace(10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/png/bank/${widget.model.bank.toLowerCase()}.png',
                      height: 30,
                      width: 50,
                    ),
                    WidthSpace(20),
                    Expanded(
                        child: Text(widget.model.userPayNumber ?? '',
                            style: FontTheme.rubik16w500black1)),
                  ],
                ),
              ),
              InkWell(
                  onTap: () {
                    Clipboard.setData(ClipboardData(text: widget.model.userPayNumber ?? ''));
                    CustomFlushBar.fullEventFlushBar('Berhasil disalin', context);
                  },
                  child: Text('Salin', style: FontTheme.rubik12w400blue1)),
            ],
          ),
          HeightSpace(30),
          Text('Jumlah yang harus dibayar', style: FontTheme.rubik12w400black4),
          HeightSpace(10),
          Text(
              'RP. ${widget.model.totalAmount == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(widget.model.totalAmount)}',
              style: FontTheme.rubik16w500black1),
        ],
      );
    }
  }
}
