part of '_pages.dart';

class ConfirmPayment extends StatefulWidget {
  final RegisterInvoiceModel invoiceModel;
  final String productType;

  const ConfirmPayment({this.invoiceModel, this.productType = 'market'});

  @override
  _ConfirmPaymentState createState() => _ConfirmPaymentState();
}

class _ConfirmPaymentState extends BaseStateful<ConfirmPayment> {
  final spp = Injector.getAsReactive<SppState>();
  final repo = PaymentRepositoryImpl();
  RegisterInvoiceResponseModel _invoice;
  bool _error;
  String _errType = '';
  String _errMsg = '';

  Future<void> retrieveData() async {
    var resp = await repo.registerInvoice(widget.invoiceModel);
    resp.fold<void>(
        (failure) => setState(() {
              _error = true;
              _errMsg = failure.translate();
            }),
        (result) => result.statusCode.translate<void>(
              ifSuccess: () => setState(() => _invoice = result.data),
              ifElse: () {
                setState(() {
                  _error = true;
                  _errMsg = result.message;
                  _errType = result.statusCode.toFailure('').translate();
                });
              },
            ));
  }

  @override
  void dispose() {
    spp.setState((s) => s.resetContext());
    super.dispose();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_rounded),
        color: SiswamediaTheme.green,
        onPressed: onPop,
      ),
      title: Text(
        'Metode Pembayaran',
        style: GoogleFonts.openSans(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
            width: MediaQuery.of(context).size.width, height: 1, color: Colors.grey.shade300),
      ),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  Future<bool> onPop() async {
    bool resultDialog = await showDialog<bool>(
        context: context,
        builder: (_) {
          return QuizDialog(
            image: 'assets/images/png/payment_info.png',
            title: 'Hati-hati lupa?',
            description: 'Tekan ya jika Anda telah menyalin kode pembayaran',
            actions: ['Ya', 'Tidak'],
            type: DialogType.textImage,
            onTapFirstAction: () async {
              Navigator.pop(context, true);
            },
            onTapSecondAction: () {
              Navigator.pop(context, false);
            },
          );
        });
    if (resultDialog) {
      Navigator.popUntil(context, (route) => route.isFirst);
    }
    return resultDialog;
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return WillPopScope(
      onWillPop: onPop,
      child: RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: retrieveData,
        child: _error
            ? ListView(
                children: [
                  HeightSpace(100),
                  Image.asset(
                    'assets/images/png/payment_empty.png',
                    height: 180,
                    width: 180,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(_errType, style: SiswamediaTheme.boldBlack.copyWith(fontSize: 16)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 40,
                          ),
                          child: Text(_errMsg,
                              textAlign: TextAlign.center,
                              style: SiswamediaTheme.descBlack
                                  .copyWith(fontSize: 12, color: Colors.grey)),
                        ),
                      ),
                    ],
                  ),
                ],
              )
            : _invoice == null
                ? LoadingView()
                : ListView(
                    shrinkWrap: true,
                    // padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        color: SiswamediaTheme.white,
                        child: Content(
                          'Batas Akhir Pembayaran',
                          child: CountdownTimer(
                            endTime: DateTime.parse(_invoice.expiredAt).millisecondsSinceEpoch,
                            widgetBuilder: (_, time) {
                              if (time == null) {
                                Navigator.popUntil(context, (route) => route.isFirst);
                                return Text('Times up');
                              }
                              List<Widget> list = [];
                              if (time.days != null) {
                                list.add(Row(
                                  children: <Widget>[
                                    Icon(Icons.sentiment_dissatisfied),
                                    Text(time.days.toString()),
                                  ],
                                ));
                              }
                              if (time.hours != null) {
                                list.add(Row(
                                  children: <Widget>[
                                    Icon(Icons.sentiment_satisfied),
                                    Text(time.hours.toString()),
                                  ],
                                ));
                              }
                              if (time.min != null) {
                                list.add(Row(
                                  children: <Widget>[
                                    // Icon(Icons.sentiment_very_dissatisfied),
                                    Text(
                                      '${(time.min < 10) ? '0' : ''}',
                                      style: SiswamediaTheme.countDown,
                                    ),
                                    Text(time.min.toString(), style: SiswamediaTheme.countDown),
                                  ],
                                ));
                              }
                              if (time.sec != null) {
                                list.add(Row(
                                  children: <Widget>[
                                    if (time.min == null)
                                      Text(
                                        '00',
                                        style: SiswamediaTheme.countDown,
                                      ),
                                    Text(' : ${(time.sec < 10) ? '0' : ''}',
                                        style: SiswamediaTheme.countDown),
                                    Text(time.sec.toString(), style: SiswamediaTheme.countDown),
                                  ],
                                ));
                              }
                              list.add(WidthSpace(10));
                              list.add(Text(
                                '( terhitung 24 jam dari "bayar sekarang" )',
                                style: GoogleFonts.openSans(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10,
                                    color: Colors.black87),
                              ));
                              return Row(
                                children: list,
                              );
                            },
                          ),
                        ),
                      ),
                      HeightSpace(15),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        color: SiswamediaTheme.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                _buildText('Total Tagihan',
                                    fontSize: 14,
                                    color: Color(0xFF4B4B4B),
                                    fontWeight: FontWeight.bold),
                                _buildText(
                                    'RP. ${_invoice.totalAmount == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(_invoice.totalAmount)}',
                                    fontSize: 16,
                                    color: SiswamediaTheme.green,
                                    fontWeight: FontWeight.bold),
                              ],
                            ),
                            HeightSpace(10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                _buildText('Admin fee',
                                    fontSize: 12,
                                    color: SiswamediaTheme.black,
                                    fontWeight: FontWeight.w600),
                                _buildText(
                                    'RP. ${_invoice.adminFee == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(_invoice.adminFee)}',
                                    fontSize: 12,
                                    color: SiswamediaTheme.green,
                                    fontWeight: FontWeight.bold),
                              ],
                            ),
                          ],
                        ),
                      ),
                      HeightSpace(15),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        color: SiswamediaTheme.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            _buildText('Nomor Virtual Akun',
                                fontSize: 12,
                                color: Color(0xFF4B4B4B),
                                fontWeight: FontWeight.bold),
                            HeightSpace(10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                _buildText('${_invoice.userPayNumber}',
                                    fontSize: 16,
                                    color: SiswamediaTheme.green,
                                    fontWeight: FontWeight.bold),
                                TextButton(
                                  onPressed: () {
                                    Clipboard.setData(ClipboardData(text: _invoice.userPayNumber));
                                    CustomFlushBar.fullEventFlushBar(
                                        'Kode berhasil dicopy!', context);
                                  },
                                  child: Text('Salin',
                                      style: SiswamediaTheme.countDown
                                          .copyWith(color: SiswamediaTheme.green)),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      HeightSpace(15),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        color: SiswamediaTheme.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            // _buildText('Cara Pembayaran',
                            //     fontSize: 12,
                            //     color: Color(0xFF4B4B4B),
                            //     fontWeight: FontWeight.bold),
                            // HeightSpace(10),
                            // _buildText(
                            //     'RP. ${_invoice.totalAmount == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(_invoice.totalAmount)}',
                            //     fontSize: 16,
                            //     color: SiswamediaTheme.green,
                            //     fontWeight: FontWeight.bold),
                          ],
                        ),
                      ),
                    ],
                  ),
        // child: _invoice.map<Widget>(
        //   onLoading: () => Loading(),
        //   onSuccess: (data) {

        //   },
        // ),
      ),
    );
  }

  Widget _buildText(String text, {double fontSize, Color color, FontWeight fontWeight}) {
    return Text(text,
        style: GoogleFonts.openSans(fontSize: fontSize, color: color, fontWeight: fontWeight));
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return Container();
  }

  @override
  void init() {
    _error = false;
    retrieveData();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      spp.setState((s) => s.pageContext = context);
    });
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }
}
