part of '../_pages.dart';

class SppBillPage extends StatefulWidget {
  @override
  _SppBillPageState createState() => _SppBillPageState();
}

class _SppBillPageState extends BasePaginationState<SppBillPage, SppState> {
  final auth = GlobalState.auth();

  var sppList = <SppBill>[];
  bool isInputted = false;

  @override
  void init() {}

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Future<void> retrieveData() async {
    if (auth.state.isLogin) {
      sppList.clear();
      await thisState.setState((s) => s.retrieveData(0));
    }
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return EmptyAppBar();
  }

  @override
  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<SppState> snapshot, SizingInformation sizeInfo) {
    var size = sizeInfo.screenSize;
    return Container(
      height: size.height,
      child: Stack(
        children: [
          RefreshIndicator(
            onRefresh: retrieveData,
            key: refreshIndicatorKey,
            child: WhenRebuilder<SppState>(
              observe: () => thisState,
              onIdle: () => WaitingView(),
              onWaiting: () => WaitingView(),
              onError: (dynamic error) => ErrorView(error: error),
              onData: (data) {
                return EmptyView(
                  isEmpty: data.bills != null && data.bills.isEmpty,
                  title: 'Kosong!',
                  description: 'Tidak ada tagihan berjalan',
                  child: _buildBills(data.bills),
                );
              },
            ),
          ),
          PayBar(
            price: snapshot.state.totalPrice,
            onPay: sppList.isEmpty
                ? null
                : () => context.push<void>(PaymentSummary(
                      bills: sppList,
                      origin: 'spp',
                      productType: 'market',
                    )),
          ),
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(
      BuildContext context, ReactiveModel<SppState> snapshot, SizingInformation sizeInfo) {
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    // TODO: implement onBackPressed
    return true;
  }

  Future<void> onAddToPayment(int value, bool isAdded) async {
    await thisState.setState((s) {
      isAdded ? s.addPrice(value) : s.removePrice(value);
    });
  }

  Widget _buildBills(List<SppBill> bills) {
    bool hasReachedMax = thisState.state.hasReachedMax;
    return ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        padding: EdgeInsets.only(bottom: 100),
        shrinkWrap: true,
        itemCount: hasReachedMax ? bills.length : bills.length + 1,
        itemBuilder: (_, index) {
          if (!hasReachedMax && index == bills.length) {
            return Center(child: CircularProgressIndicator());
          }
          SppBill model = bills[index];
          return CardBill(
            model: model,
            fontSize: 20,
            textColor: SiswamediaTheme.white,
            isAdded: sppList.contains(model),
            onTap: () {
              setState(() => sppList.contains(model) ? sppList.remove(model) : sppList.add(model));
              onAddToPayment(model.total, sppList.contains(model));
              print(sppList);
            },
          );
        });
  }

  @override
  void onScroll() {
    completer.complete();
    thisState.state.retrieveMoreData().then((value) {
      completer = Completer<Null>();
      thisState.notify();
    });
  }

  @override
  bool scrollCondition() {
    return !thisState.state.hasReachedMax;
  }
}
