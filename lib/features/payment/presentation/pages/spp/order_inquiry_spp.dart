part of '../_pages.dart';

class OrderInquirySpp extends StatefulWidget {
  const OrderInquirySpp({this.model});

  final OrderProductRequestModel model;

  @override
  _OrderInquirySppState createState() => _OrderInquirySppState();
}

class _OrderInquirySppState extends BaseStateful<OrderInquirySpp> {
  final sppPpobState = Injector.getAsReactive<SppPpobState>();

  @override
  void init() {}

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(
        'Order Inquiry',
        style: GoogleFonts.openSans(
            fontWeight: FontWeight.bold, fontSize: 18, color: SiswamediaTheme.black),
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
            width: MediaQuery.of(context).size.width, height: 1, color: Colors.grey.shade300),
      ),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return FutureBuilder<Payload<OrderProductDetail>>(
      future: sppPpobState.state.orderInquiry(widget.model),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(child: Text(snapshot.error.toString()));
        }

        if (snapshot.hasData) {
          var model = snapshot.data.data;
          return Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    buildText('Customer',
                        fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
                    HeightSpace(10),
                    buildText(model.customerName ?? '',
                        fontSize: 16, color: SiswamediaTheme.green, fontWeight: FontWeight.bold),
                    HeightSpace(10),
                    Divider(
                      color: SiswamediaTheme.grey,
                    ),
                    HeightSpace(10),
                    buildText('Total Tagihan',
                        fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
                    HeightSpace(10),
                    buildText(
                        'RP. ${model.amount == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(model.amount)}',
                        fontSize: 16,
                        color: SiswamediaTheme.green,
                        fontWeight: FontWeight.bold),
                    HeightSpace(10),
                    Divider(
                      color: SiswamediaTheme.grey,
                    ),
                    HeightSpace(10),
                    buildText('Keterangan',
                        fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
                    HeightSpace(10),
                    ...List.generate(model.productDetails.length, (index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 2.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            buildText(model.productDetails[index].key ?? '',
                                fontSize: 12,
                                color: Color(0xFF4B4B4B),
                                fontWeight: FontWeight.w500),
                            buildText(model.productDetails[index].value ?? '',
                                fontSize: 12,
                                color: Color(0xFF4B4B4B),
                                fontWeight: FontWeight.bold),
                          ],
                        ),
                      );
                    }),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: AutoLayoutButton(
                            text: 'Create Order',
                            textColor: SiswamediaTheme.white,
                            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 12),
                            textAlign: TextAlign.center,
                            radius: BorderRadius.circular(6),
                            backGroundColor: SiswamediaTheme.green,
                            onTap: () async {
                              SiswamediaTheme.loading(context);
                              await sppPpobState.state
                                  .createOrder(widget.model, context, model.amount / 1.0);
                            },
                          ),
                        ),
                      ],
                    ),
                    HeightSpace(20)
                  ],
                ),
              ],
            ),
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }
}
