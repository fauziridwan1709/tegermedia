part of '../_pages.dart';

class PaymentSppPage extends StatefulWidget {
  @override
  _PaymentSppPageState createState() => _PaymentSppPageState();
}

class _PaymentSppPageState extends BaseStateful<PaymentSppPage> {
  List<Widget> _children;
  int _currentIndex;
  var tags = <String>['Tagihan Berjalan', 'Sekolah lainnya'];

  @override
  void init() {
    _currentIndex = 0;
    _children = [SppBillPage(), SppOtherSchoolPage()];
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(
        'Spp',
        style: GoogleFonts.openSans(
            fontWeight: FontWeight.bold, fontSize: 18, color: SiswamediaTheme.black),
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
            width: MediaQuery.of(context).size.width, height: 1, color: Colors.grey.shade300),
      ),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    var size = sizeInfo.screenSize;
    return SafeArea(
      child: Column(
        children: <Widget>[
          HeightSpace(10),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * .05,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: _buildTag(),
            ),
          ),
          Expanded(child: _children.elementAt(_currentIndex)),
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  List<Widget> _buildTag() {
    return tags
        .map((e) => TagNews(
              currentIndex: _currentIndex,
              title: e,
              tags: tags,
              onTap: (val) async {
                setState(() => _currentIndex = val);
              },
            ))
        .toList();
  }
}
