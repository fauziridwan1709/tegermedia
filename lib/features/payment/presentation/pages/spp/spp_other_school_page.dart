part of '../_pages.dart';

///still dummy
class SppOtherSchoolPage extends StatefulWidget {
  @override
  _SppOtherSchoolPageState createState() => _SppOtherSchoolPageState();
}

class _SppOtherSchoolPageState extends BasePaginationState<SppOtherSchoolPage, SppPpobState> {
  final auth = GlobalState.auth();
  var imageList = <String>['best-deal.png'];
  var sppList = <SppBill>[];
  bool isInputted = false;

  @override
  void init() {
    // retrieveData();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Future<void> retrieveData() async {
    if (auth.state.isLogin) {
      QueryProduct query = QueryProduct(category: 'Edukasi');
      await thisState.setState((s) => s.retrieveData(query));
    }
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return EmptyAppBar();
  }

  @override
  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<SppPpobState> snapshot, SizingInformation sizeInfo) {
    var size = sizeInfo.screenSize;
    return Container(
      height: size.height,
      child: Stack(
        children: [
          RefreshIndicator(
            onRefresh: retrieveData,
            key: refreshIndicatorKey,
            child: WhenRebuilder<SppPpobState>(
              observe: () => thisState,
              onIdle: () => WaitingView(),
              onWaiting: () => WaitingView(),
              onError: (dynamic error) => ErrorView(error: error),
              onData: (data) {
                return EmptyView(
                  isEmpty: data.payload.data.isEmpty,
                  title: 'Kosong!',
                  description: 'Product tidak ditemukan',
                  child: ListView.builder(
                      controller: scrollController,
                      itemCount: data.hasReachedMax
                          ? data.payload.data.length
                          : data.payload.data.length + 1,
                      itemBuilder: (context, index) {
                        if (index == data.payload.data.length) {
                          return LoadingView();
                        }
                        ProductConfig model = data.payload.data[index];
                        return CardProduct(
                          model: model,
                          textColor: SiswamediaTheme.white,
                        );
                      }),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(
      BuildContext context, ReactiveModel<SppPpobState> snapshot, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, snapshot, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  void onScroll() {
    completer.complete();
    thisState.state.retrieveMoreData().then((value) {
      completer = Completer<Null>();
      thisState.notify();
    });
  }

  @override
  bool scrollCondition() {
    return !thisState.state.hasReachedMax;
  }
}
