part of '_pages.dart';

enum PaymentMethod {
  cc,
  va,
  ewallet,
}

extension PaymentMethodExtension on PaymentMethod {
  String get text => getText(this);

  String getText(PaymentMethod method) {
    switch (method) {
      case PaymentMethod.cc:
        return 'Credit Card';
      case PaymentMethod.va:
        return 'Virtual Account';
      case PaymentMethod.ewallet:
        return 'E-Wallet';
      default:
        return '';
    }
  }
}

class ChoosePaymentMethod extends StatefulWidget {
  final double totalAmount;
  final bool ppob;
  final String origin;

  ChoosePaymentMethod({this.totalAmount, this.ppob = true, this.origin = 'market'});

  @override
  _ChoosePaymentMethodState createState() => _ChoosePaymentMethodState();
}

class _ChoosePaymentMethodState extends BaseStateful<ChoosePaymentMethod> {
  final repo = PaymentRepositoryImpl();
  PaymentMethodModel paymentMethod;

  Future<List<PaymentMethodModel>> retrieveData() async {
    var queryWithUrl = QueryConfigPaymentWithUrl(
      url: widget.ppob ? Endpoints.corePaymentConfig : Endpoints.paymentMethodsMarket,
      origin: widget.origin,
      amount: widget.totalAmount,
    );
    var resp = await repo.getListPaymentMethod(queryWithUrl);
    return resp.fold<List<PaymentMethodModel>>((failure) {
      vUtils.setLog(failure);
      throw failure;
    },
        (result) => result.statusCode.translate<List<PaymentMethodModel>>(
              ifSuccess: () {
                return result.data;
              },
              ifElse: () => throw result.statusCode.toFailure('message'),
            ));
  }

  @override
  void init() {
    // retrieveData();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.clear),
        color: SiswamediaTheme.green,
        onPressed: () async => Navigator.pop(context),
      ),
      title: Text(
        'Metode Pembayaran',
        style: GoogleFonts.openSans(
            fontSize: 16, color: SiswamediaTheme.black, fontWeight: FontWeight.bold),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 1,
            color: SiswamediaTheme.grey.shade300),
      ),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return FutureBuilder<List<PaymentMethodModel>>(
      future: retrieveData(),
      builder: (_, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return LoadingView();
        } else if (snapshot.hasData) {
          return ListView.separated(
              shrinkWrap: true,
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 6),
              itemBuilder: (_, index) {
                var isPercentage = snapshot.data[index].isPercentage;
                dynamic fee = snapshot.data[index].paymentValue;
                bool isEducation = widget.ppob && widget.origin == 'spp';
                if (isEducation && snapshot.data[index].paymentMethod == 'va') {
                  return Container();
                }
                return Content(
                  _parse(snapshot.data[index].paymentMethod).text,
                  titleFontSize: 16,
                  height: 12,
                  titleStyle: boldBlack,
                  child: ListView.builder(
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (_, methodIndex) {
                        var getFee =
                            !isPercentage ? format(fee) : format(fee / 100 * widget.totalAmount);
                        return itemMethod(snapshot, index, methodIndex, getFee);
                      },
                      itemCount: snapshot.data[index].bank.length),
                );
              },
              itemCount: snapshot.data.length,
              separatorBuilder: (_, index) {
                return Divider(color: SiswamediaTheme.grey.withOpacity(.2));
              });
        }
        return Container();
      },
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return Container();
  }

  PaymentMethod _parse(String value) {
    switch (value) {
      case 'cc':
        return PaymentMethod.cc;
      case 'va':
        return PaymentMethod.va;
      case 'ewallet':
        return PaymentMethod.ewallet;
      default:
        return null;
    }
  }

  String format(dynamic price) {
    return 'RP. ${price == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(price)}';
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  Widget itemMethod(
      AsyncSnapshot<List<PaymentMethodModel>> snapshot, int index, int methodIndex, dynamic fee) {
    var bank = snapshot.data[index].bank[methodIndex];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: OutlinedButton(
        // style:
        //     ButtonStyle(side: MaterialStateProperty.all(BorderSide(color: SiswamediaTheme.green))),
        onPressed: () => Navigator.pop(
            context, PaymentBank(model: snapshot.data[index], index: methodIndex, fee: fee)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(
                    'assets/images/png/bank/${bank.bankCode.toLowerCase()}.png',
                    height: 16,
                    width: 60,
                  ),
                  WidthSpace(20),
                  Text(
                    bank.bankName,
                    style: GoogleFonts.openSans(
                        fontWeight: FontWeight.w500, fontSize: 12, color: SiswamediaTheme.black),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    '$fee',
                    style: FontTheme.openSans12w500Black,
                  ),
                  WidthSpace(10),
                  Icon(Icons.arrow_forward_ios, size: 14),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
