part of '_pages.dart';

class PulsaPage extends StatefulWidget {
  @override
  _PulsaPageState createState() => _PulsaPageState();
}

class _PulsaPageState extends BaseState<PulsaPage, PulsaState>
    with SingleTickerProviderStateMixin, PrepaidValidator {
  final pulsa = Injector.getAsReactive<PulsaState>();

  String provider = '';
  String nameProvider;
  String number = '';

  TextEditingController numberController = TextEditingController();
  FocusNode _numberFocusNode;
  List<ProviderModels> providerModels = [];

  Widget get notFoundImage => Image.asset('assets/images/png/not_found.png');

  @override
  Future<void> init() async {
    _numberFocusNode = FocusNode();
    nameProvider = '';

    providerModels = VConst().providerModels;
    controller = TabController(vsync: this, length: 2);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      S().init(context);
    });
    controller.addListener(() {
      selectedPacket = 0;
      setState(() {});
    });

    numberController.addListener(() {
      if (numberController.text.length >= 4) {
        number = numberController.text;
        for (var data in providerModels) {
          for (var numberOfProvider in data.number) {
            if (numberController.text.length > 3 && !pulsa.state.isData) {
              setState(() => provider = data.name);
              pulsa.setState((s) => s.getItemPulsa('Pulsa', numberOfProvider));
              pulsa.setState((s) => s.getItemPacketData('Paket Data', numberOfProvider));
            } else if (numberController.text.substring(0, 4) == numberOfProvider &&
                numberController.text.length > 3) {
              provider = data.name;
              pulsa.setState((s) => s.getItemPulsa('Pulsa', numberOfProvider));
              pulsa.setState((s) => s.getItemPacketData('Paket Data', numberOfProvider));
            } else {
              vUtils.setLog('not got it');
            }
          }
        }
      } else if (numberController.text.length < 8) {
        number = '';
        provider = '';
        pulsa.setState((s) => s.removeList());

        vUtils.setLog(numberController.text.length.toString() + '-');
      }
    });
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    vUtils.setLog(token);
  }

  @override
  Future<void> retrieveData() async {}

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      title: vText('Pulsa & Paket Data'),
    );
  }

  @override
  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<PulsaState> snapshot, SizingInformation sizeInfo) {
    return _body();
  }

  @override
  Widget buildWideLayout(
      BuildContext context, ReactiveModel<PulsaState> snapshot, SizingInformation sizeInfo) {
    return _body();
  }

  TabController controller;

  void showBottom(PacketPulses pulses) {
    showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return _countPayment(pulses);
      },
      context: context,
    );
  }

  Widget _body() {
    return Column(
      children: [
        Container(
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                child: Text(provider, style: FontTheme.rubik12w500green),
              ),
              HeightSpace(10),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      child: StreamBuilder<String>(
                        stream: validatePhoneNumber,
                        builder: (context, snapshot) => TextFormField(
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: FontTheme.rubik16w500black1,
                          decoration: InputDecoration(
                              labelText: 'Masukan Nomor Ponsel',
                              labelStyle: FontTheme.rubik12w400black4,
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: SiswamediaTheme.green),
                                borderRadius: BorderRadius.circular(4),
                              ),
                              prefixIcon: Icon(Icons.phone),
                              suffixIcon: Icon(
                                snapshot.hasError || numberController.text.isEmpty
                                    ? Icons.clear
                                    : Icons.check_circle_rounded,
                                color: snapshot.hasError || numberController.text.isEmpty
                                    ? Colors.red
                                    : SiswamediaTheme.green,
                              ),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: SiswamediaTheme.grey600),
                                borderRadius: BorderRadius.circular(4),
                              )),
                          focusNode: _numberFocusNode,
                          controller: numberController,
                          onChanged: onPhoneNumberChanged,
                          keyboardType: TextInputType.phone,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: double.infinity,
                child: TabBar(
                  controller: controller,
                  tabs: <Tab>[
                    Tab(
                      child: vText('Pulsa', color: SiswamediaTheme.primary),
                    ),
                    Tab(
                      child: vText('Paket Data', color: SiswamediaTheme.primary),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: TabBarView(controller: controller, children: <Widget>[
            Container(child: _pulses()),
            Container(child: _paketData()),
          ]),
        ),
      ],
    );
  }

  List<bool> listValue = [false, false, false];

  Widget _paketData() {
    if (pulsa.state.payloadPacketData.isEmpty) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          notFoundImage,
          Text('Tidak menemukan produk', style: FontTheme.rubik12w400black4),
          HeightSpace(30),
        ],
      );
    }
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: ListView.builder(
        itemBuilder: (c, i) => InkWell(
          onTap: () {
            if (number.isEmpty) {
              CustomFlushBar.errorFlushBar(
                'Masukan nomor ponsel',
                context,
              );
            } else if (number.length > 7) {
              showBottom(pulsa.state.payloadPacketData[i]);
            } else {
              CustomFlushBar.errorFlushBar(
                'Minimal ada 8 digit angka',
                context,
              );
            }
            setState(() {});
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.only(bottom: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                vText(pulsa.state.payloadPacketData[i].products.name.toString(),
                    fontSize: 14,
                    color: selectedPacket == i + 1 ? Colors.white : SiswamediaTheme.black400,
                    fontWeight: FontWeight.w500),
                vText(pulsa.state.payloadPacketData[i].products.description,
                    fontSize: 12,
                    color: selectedPacket == i + 1
                        ? SiswamediaTheme.textGrey
                        : SiswamediaTheme.black400,
                    fontWeight: FontWeight.w400),
                SizedBox(
                  height: 23,
                ),
                vText(
                    'Rp . ${pulsa.state.payloadPacketData[i].products.amount + pulsa.state.payloadPacketData[i].margin}',
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: selectedPacket == i + 1 ? Colors.white : SiswamediaTheme.primary),
              ],
            ),
          ),
        ),
        itemCount: pulsa.state.payloadPacketData.length,
        shrinkWrap: true,
      ),
    );
  }

  int selected = 0;
  int selectedPacket = 0;

  Widget _pulses() {
    if (pulsa.state.count == 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          notFoundImage,
          Text('Tidak menemukan produk', style: FontTheme.rubik12w400black4),
          HeightSpace(30),
        ],
      );
    }
    return ListView(children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            SizedBox(
              height: 12,
            ),
            GridView.builder(
                itemCount: pulsa.state.count,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, childAspectRatio: 1.8),
                itemBuilder: (c, i) => _listPembayaran(i)),
          ],
        ),
      ),
      SizedBox(
        height: 12,
      ),
      // if (selected != 0) _countPayment()
    ]);
  }

  Widget _kodePromo() {
    return Column(
      children: [
        _title('Masukan Kode Promo'),
        SizedBox(
          height: 6,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                border: Border.all(color: Colors.grey)),
            child: TextFormField(
              decoration: InputDecoration(border: InputBorder.none, hintText: ' Promo 123'),
            ),
          ),
        ),
        SizedBox(
          height: 6,
        ),
      ],
    );
  }

  Widget _countPayment(PacketPulses pulses) {
    return Container(
      decoration: BoxDecoration(
          borderRadius:
              BorderRadius.only(topRight: Radius.circular(8), topLeft: Radius.circular(8)),
          color: Colors.white),
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 20,
          ),
          _title('Informasi Pelanggan'),
          SizedBox(
            height: 15,
          ),
          itemInformation(title: 'Nomor Ponsel', value: number.toString(), rp: false),
          SizedBox(
            height: 8,
          ),
          itemInformation(title: 'Amount ', value: pulses.products.amount.toString()),
          SizedBox(
            height: 8,
          ),
          Container(
            height: 20,
            width: S.w,
            color: SiswamediaTheme.greyDivider,
          ),
          SizedBox(
            height: 20,
          ),
          _title('Detail Pembayaran'),
          HeightSpace(15),
          itemInformation(title: 'Paket ', value: ' ${pulses.products.name}', rp: false),
          itemInformation(
              title: 'Harga Total ', value: ' ${pulses.products.amount + pulses.margin}'),
          itemInformation(title: 'Biaya Transaksi ', value: '0'),
          SizedBox(
            height: 28,
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _title('Total Pembayaran'),
                    SizedBox(
                      height: 4,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        children: [
                          vText('${pulses.products.amount + pulses.margin}',
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              money: true,
                              color: SiswamediaTheme.nearBlack),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    pulsa.setState((s) async => await s.pay(pulses, number, context));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: SiswamediaTheme.primary,
                    ),
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(vertical: 9),
                    child: vText('Lanjut', color: Colors.white, fontSize: 17),
                  ),
                ),
              ),
              SizedBox(
                width: 16,
              )
            ],
          ),
          SizedBox(
            height: 23,
          ),
        ],
      ),
    );
  }

  Widget itemInformation({String title, String value, bool rp = true}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          vText(
            title,
            fontSize: 14,
            color: SiswamediaTheme.textGrey,
          ),
          vText(
            value,
            fontSize: 14,
            color: SiswamediaTheme.textGrey,
            money: rp,
          ),
        ],
      ),
    );
  }

  Widget _title(String title, {double paddingHorizontal = 20.0}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddingHorizontal),
      child: vText(title, fontWeight: FontWeight.bold, color: Colors.black, fontSize: 18),
    );
  }

  List<String> title = [
    '15.000',
    '20.000',
    '25.000',
    '30.000',
    '50.000',
    '75.000',
    '100.000',
    '150.000',
  ];

  Widget _listPembayaran(int i) {
    var item = pulsa.state.payloadPulses[i];
    var isPostPaid = item.products.type == 'postpaid';
    return InkWell(
      onTap: () {
        if (number.isEmpty) {
          CustomFlushBar.errorFlushBar(
            'Masukan nomor ponsel',
            context,
          );
        } else if (number.length > 7) {
          showBottom(pulsa.state.payloadPulses[i]);
        } else {
          CustomFlushBar.errorFlushBar(
            'Minimal ada 8 digit angka',
            context,
          );
        }
        setState(() {});
      },
      child: Card(
          color: selected == i + 1 ? SiswamediaTheme.primary : Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  item.products.name,
                  style: GoogleFonts.openSans(
                      color: selected == i + 1 ? Colors.white : SiswamediaTheme.black400,
                      fontWeight: isPostPaid ? FontWeight.normal : FontWeight.w600,
                      fontSize: isPostPaid ? 14 : 20),
                  maxFontSize: 14,
                  maxLines: 2,
                ),
                if (isPostPaid) ...[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    decoration: BoxDecoration(
                      color: selected == i + 1 ? SiswamediaTheme.white : SiswamediaTheme.green,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Text(
                      'postpaid',
                      style: selected == i + 1
                          ? FontTheme.openSans10w500Black
                          : FontTheme.openSans10w500White,
                    ),
                  )
                ] else ...[
                  vText('Rp . ${item.products.amount + item.margin}',
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                      color: selected == i + 1 ? Colors.white : SiswamediaTheme.primary),
                ]
              ],
            ),
          )),
    );
  }

  @override
  Future<bool> onBackPressed() async {
    // TODO: implement onBackPressed
    return true;
  }
}
