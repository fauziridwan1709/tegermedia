import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:tegarmedia/aw/bases/_base.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/screen/_screen.dart';
import 'package:tegarmedia/features/payment/data/models/payment/nicepayment_model.dart';
import 'package:tegarmedia/ui/pages/Ujian/_ujian.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:tegarmedia/utils/v_widget.dart';

class WebViewPaymentPage extends StatefulWidget {
  final String url;
  final NicepayPayment data;

  const WebViewPaymentPage({this.url, this.data});

  @override
  _WebViewPaymentPageState createState() => _WebViewPaymentPageState();
}

class _WebViewPaymentPageState extends BaseStateful<WebViewPaymentPage> {
  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      title: vText('Pembayaran',
          fontWeight: FontWeight.bold, fontSize: 18, color: SiswamediaTheme.black),
      centerTitle: true,
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    print('data webview ===');
    print(widget.url);
    print(widget.data.toStringData());

    return InAppWebView(
      initialUrlRequest: URLRequest(
          url: Uri.parse(widget.data.urlNicepayPayment),
          method: 'POST',
          body: Uint8List.fromList(utf8.encode(widget.data.toStringData())),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}),
      onWebViewCreated: (controller) async {
        var result = await controller.getHtml();
        print(result.contains('statusCode'));
      },
      onLoadStop: (controller, url) async {
        print('url webview');
        print(url);
        var result = await controller.getHtml();
        print(result.contains('statusCode'));
        if (result.contains('statusCode')) {
          if (result.contains('200')) {
            Navigator.popUntil(context, (route) => route.isFirst);
            CustomFlushBar.successFlushBar('Berhasil melakukan pembayaran', context);
          }
        }
      },
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  void init() {
    // TODO: implement init
  }

  @override
  Future<bool> onBackPressed() async {
    var confirm = await showDialog<bool>(
        context: context,
        builder: (_) => QuizDialog(
              type: DialogType.onlyText,
              title: 'Confirm Exit?',
              description: 'apakah anda yakin untuk membatalkan pembayaran ini?',
              onTapFirstAction: () => Navigator.pop(context, true),
              onTapSecondAction: () => Navigator.pop(context, false),
              actions: ['Yes', 'No'],
            ));
    if (confirm) {
      Navigator.popUntil(context, (route) => route.isFirst);
    }
    return confirm;
  }
}
