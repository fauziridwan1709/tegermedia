part of '_pages.dart';

class PaymentMethodPage extends StatefulWidget {
  final List<SppBill> bills;
  final String origin;
  final String productType;
  final double totalPrice;
  final VoucherAvailability voucher;

  final String orderID;

  const PaymentMethodPage(
      {this.origin,
      this.productType = 'ppob',
      this.bills,
      this.totalPrice,
      this.voucher,
      this.orderID});

  @override
  _PaymentMethodPageState createState() => _PaymentMethodPageState();
}

class _PaymentMethodPageState extends BaseState<PaymentMethodPage, SppState> {
  final profile = GlobalState.profile();
  RegisterInvoiceModel _model;
  PaymentBank _paymentMethod;
  TextEditingController _numberTelp;
  TextEditingController _cardHolderName;
  TextEditingController _cardNumber;
  TextEditingController _cvv;
  TextEditingController _date;
  bool _isChangePaymentMethod = true;
  bool _isUseMyPhoneNumber;
  String _tmpPhoneNumber;

  String get myPhoneNumber => profile.state.profile.phone;

  @override
  void init() {
    _tmpPhoneNumber = '';
    _isUseMyPhoneNumber = profile.state.profile.phone.isNotEmpty;
    if (widget.origin != 'pulsa' && widget.productType == 'market') {
      _model = RegisterInvoiceModel(
        origin: widget.origin,
        voucher: widget.voucher != null ? widget.voucher.code : '',
        voucherType: widget.voucher != null ? 'outsource_tuition' : '',
        phoneNumber: profile.state.profile.phone,
        productId: widget.bills.fold([], (previousValue, element) {
          previousValue.add(element.id.toString());
          return previousValue;
        }),
        mitraCode: '',
        productType: widget.origin == 'spp' ? 'tuition' : 'ppdb',
      );
    } else {
      _model = RegisterInvoiceModel();
    }
    _cardHolderName = TextEditingController();
    _cardNumber = TextEditingController();
    _cvv = TextEditingController();
    _date = TextEditingController();
    _numberTelp = TextEditingController();
  }

  @override
  Future<void> retrieveData() async {}

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_rounded),
        color: SiswamediaTheme.green,
        onPressed: () async => Navigator.pop(context),
      ),
      title: Text(
        'Pembayaran',
        style: GoogleFonts.openSans(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
            width: MediaQuery.of(context).size.width, height: 1, color: Colors.grey.shade300),
      ),
    );
  }

  @override
  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<SppState> snapshot, SizingInformation sizeInfo) {
    String payMethodName = _paymentMethod != null ? _paymentMethod.model.paymentMethod : null;
    return Container(
      constraints: BoxConstraints(minHeight: sizeInfo.screenSize.height),
      child: ListView(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            color: SiswamediaTheme.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildText('Total Tagihan',
                    fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
                HeightSpace(10),
                _buildText(
                    'RP. ${widget.totalPrice == 0 ? '0' : NumberFormat.currency(customPattern: '#,###').format(widget.totalPrice)}',
                    fontSize: 16,
                    color: SiswamediaTheme.green,
                    fontWeight: FontWeight.bold),
              ],
            ),
          ),
          HeightSpace(15),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 35),
            color: SiswamediaTheme.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildText('Pilih Metode Pembayaran',
                    fontSize: 12, color: Color(0xFF4B4B4B), fontWeight: FontWeight.bold),
                HeightSpace(10),
                if (_paymentMethod == null) ...[
                  SelectorWidget(
                    label: 'Tap disini untuk memilih',
                    onTap: (context) async {
                      if (_isChangePaymentMethod) {
                        await _handlePaymentMethod();
                      }
                    },
                  )
                ] else ...[
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                    decoration: BoxDecoration(
                      border: Border.all(color: SiswamediaTheme.green),
                      borderRadius: BorderRadius.circular(4),
                      color: SiswamediaTheme.green.withOpacity(.3),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Image.asset(
                              'assets/images/png/bank/${_paymentMethod.model.bank[_paymentMethod.index].bankCode.toLowerCase()}.png',
                              height: 25,
                              width: 55,
                            ),
                            WidthSpace(15),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  _paymentMethod.model.bank[_paymentMethod.index].bankName,
                                  style: GoogleFonts.openSans(
                                      fontWeight: FontWeight.w600, fontSize: 12),
                                ),
                                HeightSpace(5),
                                Text(
                                  'Biaya admin: ${_paymentMethod.fee}',
                                  style: FontTheme.openSans12w500Black,
                                ),
                              ],
                            ),
                          ],
                        ),
                        AutoLayoutButton(
                          text: 'Ubah',
                          textColor: SiswamediaTheme.white,
                          backGroundColor: SiswamediaTheme.green,
                          padding: EdgeInsets.symmetric(horizontal: 14, vertical: 4),
                          radius: BorderRadius.circular(2),
                          onTap: _handlePaymentMethod,
                        )
                      ],
                    ),
                  )
                ],
              ],
            ),
          ),
          HeightSpace(15),
          if (_paymentMethod != null &&
              payMethodName != 'va' &&
              _paymentMethod.model.bank[_paymentMethod.index].bankCode != 'ESHP') ...[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              color: SiswamediaTheme.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Informasi Pembayar', style: FontTheme.openSans12w700Black),
                  HeightSpace(10),
                  _mapPaymentInformation(),
                ],
              ),
            ),
          ],
          Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: [
                  Expanded(
                    child: AutoLayoutButton(
                      backGroundColor: SiswamediaTheme.green,
                      radius: BorderRadius.circular(6),
                      onTap: !_numberTelp.text.isNotEmpty &&
                              widget.origin == 'pulsa' &&
                              checkBank('ESHP')
                          ? null
                          : () {
                              if (widget.origin == 'pulsa' &&
                                  _paymentMethod.model.paymentMethodCode == '05') {
                                _isChangePaymentMethod = false;
                                snapshot.state.continuePaymentPPOB(
                                    _paymentMethod.model,
                                    _numberTelp.text,
                                    widget.orderID,
                                    _paymentMethod.index,
                                    context);
                              } else if (widget.origin == 'spp' &&
                                  widget.productType == 'ppob' &&
                                  _paymentMethod.model.paymentMethodCode == '10') {
                                context.push<void>(ConfirmPayment(invoiceModel: _model),
                                    name: 'confirm_payment');
                              } else if (widget.origin == 'spp' &&
                                  widget.productType == 'ppob' &&
                                  _paymentMethod.model.paymentMethodCode == '01') {
                                snapshot.state.continuePaymentPPOBSpp(
                                    _paymentMethod.model,
                                    GlobalState.profile().state.profile.phone,
                                    widget.orderID,
                                    _paymentMethod.index,
                                    _cardNumber.text,
                                    _cardHolderName.text,
                                    _cvv.text,
                                    _date.text.substring(0, 2) + _date.text.substring(3),
                                    context);
                              } else {
                                context.push<void>(ConfirmPayment(invoiceModel: _model),
                                    name: 'confirm_payment');
                              }
                            },
                      text: 'Bayar Sekarang',
                      textAlign: TextAlign.center,
                      textColor: SiswamediaTheme.white,
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      padding: EdgeInsets.symmetric(horizontal: 60, vertical: 10),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(
      BuildContext context, ReactiveModel<SppState> snapshot, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, snapshot, sizeInfo);
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  Widget _buildText(String text, {double fontSize, Color color, FontWeight fontWeight}) {
    return Text(text,
        style: GoogleFonts.openSans(fontSize: fontSize, color: color, fontWeight: fontWeight));
  }

  Future<void> _handlePaymentMethod() async {
    var data = await context.push<PaymentBank>(ChoosePaymentMethod(
      totalAmount: widget.totalPrice,
      ppob: widget.orderID != null,
      origin: widget.origin,
    ));
    setState(() {
      _paymentMethod = data;
      _model.paymentMethodCode = data.model.paymentMethodCode;
      _model.paymentMethod = data.model.paymentMethod;
      _model.bankCode = data.model.bank[data.index].bankCode;
      _model.paymentProvider = data.model.paymentProvider;
      if (_isUseMyPhoneNumber) {
        _numberTelp.text = myPhoneNumber;
      }
    });
  }

  Widget _mapPaymentInformation() {
    Logger().d(_model.paymentMethod);
    if (_model.paymentMethod == 'va') {
      return Container();
    } else if (_model.paymentMethod == 'cc') {
      return Column(
        children: <Widget>[
          TextFormField(
            style: GoogleFonts.openSans(
                fontSize: 14, fontWeight: FontWeight.w600, color: SiswamediaTheme.black),
            decoration: InputDecoration(
              hintText: 'Cardholder Name',
              hintStyle: GoogleFonts.openSans(fontSize: 14, color: SiswamediaTheme.grey),
              border: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: SiswamediaTheme.grey.withOpacity(.5),
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: SiswamediaTheme.grey.withOpacity(.5),
                ),
              ),
              isDense: false,
              // contentPadding: EdgeInsets.symmetric(vertical: 25),
            ),
            controller: _cardHolderName,
            onChanged: (String v) {},
          ),
          HeightSpace(10),
          TextFormField(
            style: GoogleFonts.openSans(
                fontSize: 14, fontWeight: FontWeight.w600, color: SiswamediaTheme.black),
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.credit_card),
              hintText: 'Card Number',
              hintStyle: GoogleFonts.openSans(fontSize: 14, color: SiswamediaTheme.grey),
              border: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: SiswamediaTheme.grey.withOpacity(.5),
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: SiswamediaTheme.grey.withOpacity(.5),
                ),
              ),
            ),
            controller: _cardNumber,
            onChanged: (String v) {},
          ),
          HeightSpace(10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: TextFormField(
                  style: GoogleFonts.openSans(
                      fontSize: 14, fontWeight: FontWeight.w600, color: SiswamediaTheme.black),
                  maxLength: 5,
                  controller: _date,
                  inputFormatters: [
                    CardExpirationFormatter(),
                  ],
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: 'Date',
                    hintStyle: GoogleFonts.openSans(fontSize: 14, color: SiswamediaTheme.grey),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: SiswamediaTheme.grey.withOpacity(.5),
                      ),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: SiswamediaTheme.grey.withOpacity(.5),
                      ),
                    ),
                  ),
                  // controller: numberTelp,
                  onChanged: (String v) {},
                ),
              ),
              WidthSpace(10),
              Expanded(
                flex: 1,
                child: TextFormField(
                  style: GoogleFonts.openSans(
                      fontSize: 14, fontWeight: FontWeight.w600, color: SiswamediaTheme.black),
                  obscureText: true,
                  maxLength: 3,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: 'CVV',
                    hintStyle: GoogleFonts.openSans(fontSize: 14, color: SiswamediaTheme.grey),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: SiswamediaTheme.grey.withOpacity(.5),
                      ),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: SiswamediaTheme.grey.withOpacity(.5),
                      ),
                    ),
                  ),
                  controller: _cvv,
                  onChanged: (String v) {},
                ),
              ),
            ],
          ),
          HeightSpace(20),
        ],
      );
    } else if (_model.paymentMethod == 'ewallet') {
      return Column(
        children: [
          if (profile.state.profile.phone.isNotEmpty)
            CheckboxListTile(
              value: _isUseMyPhoneNumber,
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (bool value) {
                setState(() {
                  _isUseMyPhoneNumber = !_isUseMyPhoneNumber;
                  if (_isUseMyPhoneNumber) {
                    _numberTelp.text = myPhoneNumber;
                  } else {
                    _numberTelp.clear();
                    _numberTelp.text = _tmpPhoneNumber;
                  }
                });
              },
              title: Text('Use my phone number', style: FontTheme.rubik12w500black1),
            ),
          if (profile.state.profile.phone.isEmpty || !_isUseMyPhoneNumber)
            PhoneNumberField(
              controller: _numberTelp,
              onChange: (val) => setState(() => _tmpPhoneNumber = val),
            ),
        ],
      );
    }
    return Container();
  }

  bool checkBank(String code) {
    if (_paymentMethod != null) {
      return _paymentMethod.model.bank[_paymentMethod.index].bankCode != code;
    }
    return true;
  }
}

class CardExpirationFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final newValueString = newValue.text;
    String valueToReturn = '';

    for (int i = 0; i < newValueString.length; i++) {
      if (newValueString[i] != '/') valueToReturn += newValueString[i];
      var nonZeroIndex = i + 1;
      final contains = valueToReturn.contains(RegExp(r'\/'));
      if (nonZeroIndex % 2 == 0 && nonZeroIndex != newValueString.length && !(contains)) {
        valueToReturn += '/';
      }
    }
    if (newValueString.contains('-') ||
        newValueString.contains(',') ||
        newValueString.contains(' ') ||
        newValueString.contains('.')) {
      valueToReturn = valueToReturn.substring(0, valueToReturn.length - 1);
    }
    return newValue.copyWith(
      text: valueToReturn,
      selection: TextSelection.fromPosition(
        TextPosition(offset: valueToReturn.length),
      ),
    );
  }
}
