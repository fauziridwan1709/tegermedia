part of '_pages.dart';

class PrePaidPulsesPage extends StatefulWidget {
  @override
  _PrePaidPulsesPageState createState() => _PrePaidPulsesPageState();
}

class _PrePaidPulsesPageState extends BaseState<PrePaidPulsesPage, PrePaidPulsesState>
    with PrepaidValidator {
  final prepaid = Injector.getAsReactive<PrePaidPulsesState>();

  TextEditingController numberController = TextEditingController();

  final numberFocusNode = FocusNode();

  @override
  Widget buildAppBar(BuildContext context) {
    // TODO: implement buildAppBar
    return AppBar(
      elevation: 2,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      title: vText('Pasca Bayar'),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  int selected;

  @override
  Widget buildNarrowLayout(BuildContext context, ReactiveModel<PrePaidPulsesState> snapshot,
      SizingInformation sizeInfo) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    child: StreamBuilder<String>(
                      stream: validatePhoneNumber,
                      builder: (context, snapshot) => TextFormField(
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        style: FontTheme.rubik16w500black1,
                        decoration: InputDecoration(
                            labelText: 'Masukan Nomor Ponsel',
                            labelStyle: FontTheme.rubik12w400black4,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: SiswamediaTheme.green),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            prefixIcon: Icon(Icons.phone),
                            suffixIcon: Icon(
                              snapshot.hasError || numberController.text.isEmpty
                                  ? Icons.clear
                                  : Icons.check_circle_rounded,
                              color: snapshot.hasError || numberController.text.isEmpty
                                  ? Colors.red
                                  : SiswamediaTheme.green,
                            ),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: SiswamediaTheme.grey600),
                              borderRadius: BorderRadius.circular(4),
                            )),
                        focusNode: numberFocusNode,
                        controller: numberController,
                        onChanged: onPhoneNumberChanged,
                        keyboardType: TextInputType.phone,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 10,
            margin: EdgeInsets.only(top: 20, bottom: 20),
            color: SiswamediaTheme.newBackground,
            width: double.infinity,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: vText('Pilihan Provider', fontWeight: FontWeight.w500, fontSize: 14),
          ),
          Expanded(
            child: ListView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
              itemBuilder: (c, i) => InkWell(
                onTap: () {
                  selected = i;
                  setState(() {});
                },
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(
                          color: selected == i ? SiswamediaTheme.primary : SiswamediaTheme.grey600),
                      color:
                          selected == i ? SiswamediaTheme.primary.withOpacity(0.2) : Colors.white),
                  padding: EdgeInsets.all(12),
                  margin: EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: [
                      Image.network(
                        snapshot.state.prepaidPulses[i].logo +
                            snapshot.state.prepaidPulses[i].products.logo,
                        width: 76,
                        height: 24,
                        fit: BoxFit.fill,
                        cacheWidth: 76,
                        cacheHeight: 24,
                      ),
                      Expanded(
                        child: vText(snapshot.state.prepaidPulses[i].products.name,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ),
              ),
              itemCount: snapshot.state.prepaidPulses.length,
            ),
          ),
          StreamBuilder<String>(
              stream: validatePhoneNumber,
              builder: (context, snapshot) {
                return Row(
                  children: [
                    Expanded(
                      child: AutoLayoutButton(
                        backGroundColor: SiswamediaTheme.green,
                        onTap: !snapshot.hasData || selected == null
                            ? null
                            : () async {
                                if (numberFocusNode.hasFocus) {
                                  numberFocusNode.unfocus();
                                }
                                if (numberController.text.length >= 8 && selected != null) {
                                  bool isSucess = false;
                                  await prepaid.setState((s) async => isSucess = await s.pay(
                                      prepaid.state.prepaidPulses[selected],
                                      numberController.text,
                                      context));

                                  if (isSucess) {
                                    showBottom(prepaid.state.orderProductDetail);
                                  }
                                } else {
                                  CustomFlushBar.errorFlushBar(
                                    'Nomer harus lebih dari 8',
                                    context,
                                  );
                                }
                              },
                        textColor: SiswamediaTheme.white,
                        fontSize: 14,
                        radius: BorderRadius.circular(6),
                        margin: EdgeInsets.all(20),
                        textAlign: TextAlign.center,
                        padding: EdgeInsets.symmetric(vertical: 12),
                        text: 'Bayar Tagihan',
                      ),
                    ),
                  ],
                );
              })
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, ReactiveModel snapshot, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, snapshot, sizeInfo);
  }

  @override
  void init() {
    // TODO: implement init
    prepaid.setState((s) => s.getListPrepaid());
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  void showBottom(OrderProductDetail pulses) {
    showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return _countPayment(pulses);
      },
      isScrollControlled: true,
      context: context,
    );
  }

  Widget _countPayment(
    OrderProductDetail pulses,
  ) {
    return Container(
      decoration: BoxDecoration(
          borderRadius:
              BorderRadius.only(topRight: Radius.circular(18), topLeft: Radius.circular(18)),
          color: Colors.white),
      margin: EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 20,
          ),
          _title('Informasi Pelanggan'),
          SizedBox(
            height: 15,
          ),
          itemInformation(
              title: 'Nomor Ponsel', value: numberController.text.toString(), rp: false),
          for (var data in pulses.customerDetail)
            itemInformation(title: data.key, value: data.value, rp: false),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 12,
            width: S.w,
            color: SiswamediaTheme.greyDivider,
          ),
          SizedBox(
            height: 20,
          ),
          _title('Detail Pembayaran'),
          HeightSpace(15),
          itemInformation(title: 'Paket ', value: ' ${pulses.productName}', rp: false),
          for (var data in pulses.billDetails) itemInformation(title: data.key, value: data.value),
          SizedBox(
            height: 28,
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _title('Total Pembayaran'),
                    SizedBox(
                      height: 4,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        children: [
                          vText('${pulses.amount}',
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              money: true,
                              color: SiswamediaTheme.nearBlack),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    // VNav().payment(context,pulses.products.amount+pulses.margin,'608ce7d0f7232505cc3f6a63');

                    prepaid.setState((s) => s.createOrder(
                        prepaid.state.prepaidPulses[selected], numberController.text, context));
                    // pulsa.setState((s) => s.continuePay(  pulsa.state.orderId));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: SiswamediaTheme.primary,
                    ),
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(vertical: 9),
                    child: vText('Lanjut', color: Colors.white, fontSize: 17),
                  ),
                ),
              ),
              SizedBox(
                width: 16,
              )
            ],
          ),
          SizedBox(
            height: 23,
          ),
        ],
      ),
    );
  }

  Widget itemInformation({String title, String value, bool rp = true}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 1,
            child: vText(
              title,
              fontSize: 14,
              color: SiswamediaTheme.textGrey,
            ),
          ),
          Expanded(
            flex: 1,
            child: vText(
              value,
              fontSize: 14,
              color: SiswamediaTheme.textGrey,
              money: rp,
              align: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }

  Widget _title(String title, {double paddingHorizontal = 20.0}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddingHorizontal),
      child: vText(title, fontWeight: FontWeight.bold, color: Colors.black, fontSize: 18),
    );
  }

  @override
  Future<void> retrieveData() {
    // TODO: implement retrieveData
    throw UnimplementedError();
  }
}
