part of '_states.dart';

class PulsaState implements FutureState<PulsaState, int> {
  final PulsaRepository repo = PulsaRepositoryImpl();

  @override
  String cacheKey = 'pulsa';

  List<PacketPulses> payloadPulses = [];
  List<PacketPulses> payloadPacketData = [];
  String number;
  int count = 0;

  @override
  bool getCondition() {
    return true;
  }

  @override
  Future<void> retrieveData(int k) async {
    // TODO: implement retrieveData

    // var resp = await repo.getAllAvailablePulsa();
    // resp.fold((failure) => null, (result) => null);

    number = '0';
    throw UnimplementedError();
  }

  bool isData = false;

  void getItemPulsa(String category, String number) async {
    isData = true;
    var queryPulsa = QueryPulsa(category, true, number);
    var resp = await repo.getAllAvailablePulsa(queryPulsa);
    resp.fold((failure) {
      print(failure.toString());
      throw failure;
    },
        (result) => result.statusCode.translate(
            ifSuccess: () {
              payloadPulses = result.data.data;

              count = result.data.totalData;
            },
            ifElse: () => throw result.toFailure()));
  }

  void getItemPacketData(String category, String number) async {
    var queryPulsa = QueryPulsa(category, true, number);
    var resp = await repo.getAllAvailablePulsa(queryPulsa);
    resp.fold((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate(
            ifSuccess: () {
              payloadPacketData = result.data.data;
            },
            ifElse: () => throw result.toFailure()));
  }

  String orderId = '';

  Future<void> pay(PacketPulses pulses, String numberSelected, BuildContext context) async {
    // if (number != numberSelected) {
    number = numberSelected;
    SiswamediaTheme.loading(context);
    var queryPulses = <String, dynamic>{
      'productID': pulses.id,
      'accountNumber': number,
      'productCode': pulses.products.code,
      'month': 1,
    };

    var resp = await repo.createOrder(queryPulses);
    Navigator.pop(context);
    resp.fold((failure) {
      CustomFlushBar.errorFlushBar(failure.translate(), context);
      throw failure;
    },
        (result) => result.statusCode.translate(ifSuccess: () {
              orderId = result.data;

              VNav().payment(context, pulses.products.amount + pulses.margin, orderId, 'pulsa');
            }, ifElse: () {
              CustomFlushBar.errorFlushBar(result.message, context);
              throw result.toFailure();
            }));
    // } else {
    // VNav().payment(context, pulses.products.amount + pulses.margin, orderId, 'pulsa');
    // }
  }

  Future<void> continuePay(String number) async {
    var resp = await repo.orderPayment(number);
    resp.fold((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate(
            ifSuccess: () {
              vUtils.setLog('adnan' + result.data.id);
            },
            ifElse: () => throw result.toFailure()));
  }

  void removeList() {
    count = 0;
    orderId = '';
    isData = false;
    payloadPacketData.clear();
    payloadPulses.clear();
  }
}
