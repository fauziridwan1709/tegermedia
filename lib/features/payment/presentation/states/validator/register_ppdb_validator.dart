part of '../_states.dart';

const String _kEmailRule =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

class RegisterPPDBValidator {
  final _emailController = BehaviorSubject<String>();
  final _firstNameController = BehaviorSubject<String>();
  final _lastNameController = BehaviorSubject<String>();
  final _phoneNumberController = BehaviorSubject<String>();
  final _addressController = BehaviorSubject<String>();

  Stream<String> get validateEmail => _emailController.stream.transform(_validateEmail);

  Stream<String> get validateFirstName => _firstNameController.stream.transform(_validateName);

  Stream<String> get validateLastName => _lastNameController.stream.transform(_validateName);

  Stream<String> get validatePhoneNumber => _phoneNumberController.stream.transform(_validateName);

  Stream<String> get validateAddress => _addressController.stream.transform(_validateName);

  Function(String) get onEmailChanged => _emailController.sink.add;
  Function(String) get onFirstNameChanged => _firstNameController.sink.add;
  Function(String) get onLastNameChanged => _lastNameController.sink.add;
  Function(String) get onPhoneNumberChanged => _phoneNumberController.sink.add;
  Function(String) get onAddressChanged => _addressController.sink.add;

  final StreamTransformer<String, String> _validateName =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String inputString, EventSink<String> sink) async {
    try {
      if (inputString.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else {
        sink.add(inputString);
      }
    } catch (e) {
      sink.addError('Wrong input');
    }
  });

  final StreamTransformer<String, String> _validateEmail =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String email, EventSink<String> sink) {
    final RegExp emailExp = RegExp(_kEmailRule);

    if (!emailExp.hasMatch(email) || email.isEmpty) {
      sink.addError('invalid email address format');
    } else {
      sink.add(email);
    }
  });

  Stream<bool> get isValid => Rx.combineLatest5(
      validateEmail,
      validateFirstName,
      validateLastName,
      validatePhoneNumber,
      validateAddress,
      (dynamic a, dynamic b, dynamic c, dynamic d, dynamic e) => true);
}
