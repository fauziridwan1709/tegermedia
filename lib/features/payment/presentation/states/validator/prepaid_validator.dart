part of '../_states.dart';

class PrepaidValidator {
  final _phoneNumberController = BehaviorSubject<String>();

  Stream<String> get validatePhoneNumber => _phoneNumberController.stream.transform(_validateName);

  Function(String) get onPhoneNumberChanged => _phoneNumberController.sink.add;

  final StreamTransformer<String, String> _validateName =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String inputString, EventSink<String> sink) async {
    if (inputString.isEmpty) {
      sink.addError('Tidak boleh kosong');
    } else {
      var providerModels = VConst().providerModels;
      for (var data in providerModels) {
        if (inputString.length > 4 && data.number.contains(inputString.substring(0, 4))) {
          sink.add(data.name);
        }
      }
    }
  });

  Stream<bool> get isValid =>
      Rx.combineLatest2(validatePhoneNumber, validatePhoneNumber, (dynamic a, dynamic b) => true);
}
