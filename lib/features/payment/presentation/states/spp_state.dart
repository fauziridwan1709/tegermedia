part of '_states.dart';

class SppState implements FutureState<SppState, int> {
  final MarketRepository repo = MarketRepositoryImpl();
  final PaymentRepository repoPayment = PaymentRepositoryImpl();

  int page = 1;
  double totalPrice = 0;
  String _schoolCode;
  bool hasReachedMax = false;
  List<SppBill> bills;
  VoucherAvailability _voucherAvailability;
  BuildContext pageContext;

  VoucherAvailability get voucher => _voucherAvailability;
  String get getSchoolCode => _schoolCode;

  @override
  String cacheKey = 'SPP_KEY';

  @override
  bool getCondition() {
    return bills != null;
  }

  void resetContext() {
    pageContext = null;
  }

  @override
  Future<void> retrieveData(int k) async {
    initialState();
    QueryBill query = QueryBill(type: 'SPP', status: 'UNPAID');
    var resp = await repo.getAllBill(query);
    resp.fold<void>((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              bills = result.data;
              Logger().d(result.data);
              hasReachedMax = result.data.isEmpty || result.data.length < query.limit;
              Logger().d(hasReachedMax);
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveMoreData() async {
    QueryBill query = QueryBill(type: 'SPP', status: 'UNPAID', page: ++page);
    var resp = await repo.getAllBill(query);
    resp.fold<void>((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              bills.addAll(result.data);
              hasReachedMax = result.data.isEmpty || result.data.length < query.limit;
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  void inputSchoolCode(String val) {
    _schoolCode = val;
  }

  void continuePaymentPPOB(PaymentMethodModel payment, String numberTelp, String orderId, int index,
      BuildContext context) async {
    SiswamediaTheme.infoLoading(context: context, info: 'Menambahkan Pembayaran...');
    var body = <String, dynamic>{
      'dataPayment': {
        'bankCode': '',
        'mitraCode': payment.bank[index].bankCode,
        'paymentMethod': payment.paymentMethod,
        'paymentMethodCode': payment.paymentMethodCode,
        'paymentProvider': payment.paymentProvider,
        'userPayNumber': numberTelp
      },
      'orderID': orderId
    };
    var resp = await repoPayment.confirmPPob(body);
    resp.fold((failure) {
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        'Eror !',
        context,
      );
      throw failure;
    },
        (result) => result.statusCode.translate<void>(ifSuccess: () {
              Navigator.pop(context);
              VNav().webViewPayment(context, result.data.checkoutURL, result.data.nicepayPayment);
            }, ifElse: () {
              Navigator.pop(context);
              CustomFlushBar.errorFlushBar(
                result.message,
                context,
              );
              throw result.statusCode.toFailure(result.message);
            }));
  }

  void continuePaymentPPOBSpp(PaymentMethodModel payment, String numberTelp, String orderId,
      int index, String card, String name, String cvv, String exp, BuildContext context) async {
    SiswamediaTheme.infoLoading(context: context, info: 'Menambahkan Pembayaran...');
    var body = <String, dynamic>{
      'dataPayment': {
        'bankCode': payment.bank[index].bankCode,
        // 'mitraCode': payment.bank[index].bankCode,
        'paymentMethod': payment.paymentMethod,
        'paymentMethodCode': payment.paymentMethodCode,
        'paymentProvider': payment.paymentProvider,
        'userPayNumber': numberTelp
      },
      'orderID': orderId
    };
    var resp = await repoPayment.confirmPPob(body);
    resp.fold((failure) {
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        'Eror !',
        context,
      );
      throw failure;
    },
        (result) => result.statusCode.translate<void>(ifSuccess: () {
              Navigator.pop(context);
              result.data.nicepayPayment.cardHolderNm = name;
              result.data.nicepayPayment.cardNo = card;
              result.data.nicepayPayment.cardCvv = cvv;
              result.data.nicepayPayment.cardExpYymm = exp;
              NicepayPayment data = NicepayPayment(
                cardHolderNm: name,
                cardNo: card,
                cardCvv: cvv,
                cardExpYymm: exp,
                callBackUrl: result.data.nicepayPayment.callBackUrl,
                tXid: result.data.nicepayPayment.tXid,
                merchantToken: result.data.nicepayPayment.merchantToken,
                timeStamp: result.data.nicepayPayment.timeStamp,
                urlNicepayPayment: result.data.nicepayPayment.urlNicepayPayment,
              );
              print('url mantap');
              Logger().d(data.toStringData());
              print(data.cardNo);

              VNav().webViewPayment(
                  context,
                  'https://www.nicepay.co.id/nicepay/direct/v2/payment',
                  // + '&cardNo=$card&cardExpYymm=$exp&cardCvv=$cvv&cardHolderNm=$name',
                  data);
            }, ifElse: () {
              Navigator.pop(context);
              CustomFlushBar.errorFlushBar(
                result.message,
                context,
              );
              throw result.statusCode.toFailure(result.message);
            }));
  }

  Future<void> checkVoucherAvailability(String code) async {
    var resp = await repo.checkVoucherAvailability(code, 'outsource_tuition');
    resp.fold<void>((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () => _voucherAvailability = result.data,
            ifElse: () => throw result.statusCode.toFailure(result.message)));
  }

  void initialState() {
    page = 1;
    totalPrice = 0.0;
  }

  void addPrice(int value) {
    totalPrice += value;
  }

  void removePrice(int value) {
    totalPrice -= value;
  }

  void refreshVoucher() {
    _voucherAvailability = null;
  }
}
