part of '../_states.dart';

class SppMarketState implements FutureState<SppMarketState, int> {
  final MarketRepository repo = MarketRepositoryImpl();
  final PaymentRepository repoPayment = PaymentRepositoryImpl();

  int page = 1;
  double totalPrice = 0;
  String _schoolCode;
  bool hasReachedMax;
  List<SppBill> bills;
  VoucherAvailability _voucherAvailability;
  BuildContext pageContext;

  VoucherAvailability get voucher => _voucherAvailability;
  String get getSchoolCode => _schoolCode;

  @override
  String cacheKey = 'SPP_KEY';

  @override
  bool getCondition() {
    return false;
  }

  void resetContext() {
    pageContext = null;
  }

  @override
  Future<void> retrieveData(int k) async {
    initialState();
    QueryBill query = QueryBill(type: 'SPP', status: 'UNPAID');
    var resp = await repo.getAllBill(query);
    resp.fold<void>((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              bills = result.data;
              hasReachedMax = result.data.isEmpty || result.data.length < query.limit;
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveMoreData() async {
    QueryBill query = QueryBill(type: 'SPP', status: 'UNPAID', page: ++page);
    var resp = await repo.getAllBill(query);
    resp.fold<void>((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              bills.addAll(result.data);
              hasReachedMax = result.data.isEmpty || result.data.length < query.limit;
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  void inputSchoolCode(String val) {
    _schoolCode = val;
  }

  void continuePaymentPPOB(PaymentMethodModel payment, String numberTelp, String orderId, int index,
      BuildContext context) async {
    SiswamediaTheme.infoLoading(context: context, info: 'Menambahkan Pembayaran...');
    var body = <String, dynamic>{
      'dataPayment': {
        'bankCode': '',
        'mitraCode': payment.bank[index].bankCode,
        'paymentMethod': payment.paymentMethod,
        'paymentMethodCode': payment.paymentMethodCode,
        'paymentProvider': payment.paymentProvider,
        'userPayNumber': numberTelp
      },
      'orderID': orderId
    };
    var resp = await repoPayment.confirmPPob(body);
    resp.fold((failure) {
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        failure.translate(),
        context,
      );
      throw failure;
    },
        (result) => result.statusCode.translate<void>(ifSuccess: () {
              Navigator.pop(context);
              VNav().webViewPayment(context, result.data.checkoutURL, result.data.nicepayPayment);
            }, ifElse: () {
              Navigator.pop(context);
              CustomFlushBar.errorFlushBar(
                'Eror !',
                context,
              );
              throw result.statusCode.toFailure(result.message);
            }));
  }

  Future<void> checkVoucherAvailability(String code) async {
    var resp = await repo.checkVoucherAvailability(code, 'outsource_tuition');
    resp.fold<void>((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () => _voucherAvailability = result.data,
            ifElse: () => throw result.statusCode.toFailure(result.message)));
  }

  Future<void> cancelTransaction(String trxId, BuildContext context) async {
    var resp = await repo.cancelTransaction(trxId);
    resp.fold<void>(
      (failure) {
        CustomFlushBar.successFlushBar(failure.translate(), context);
      },
      (result) => result.statusCode.translate<void>(
          ifSuccess: () => CustomFlushBar.successFlushBar('Membatalkan transaksi', context),
          ifElse: () => CustomFlushBar.successFlushBar('Gagal Membatalkan transaksi', context)),
    );
  }

  void initialState() {
    page = 1;
    totalPrice = 0.0;
  }

  void addPrice(int value) {
    totalPrice += value;
  }

  void removePrice(int value) {
    totalPrice -= value;
  }

  void refreshVoucher() {
    _voucherAvailability = null;
  }
}
