part of '../_states.dart';

class SppPpobState implements FutureState<SppPpobState, QueryProduct> {
  SppPpobState({Client client}) {
    ProductRemoteDataSource dataSource = ProductRemoteDataSourceImpl(client: client);
    _repo = ProductRepositoryImpl(remoteDataSource: dataSource);
  }

  int page = 1;
  ProductRepository _repo;
  bool _reachedMax;
  Payload<List<ProductConfig>> payload;

  bool get hasReachedMax => _reachedMax;

  @override
  String cacheKey = CacheKeyConst.sppPpobState;

  @override
  bool getCondition() {
    return payload != null;
  }

  @override
  Future<void> retrieveData(QueryProduct query) async {
    initialState();
    var resp = await _repo.getConfigByCategory(query);
    resp.fold<void>(
        (failure) => throw failure,
        (result) => result.statusCode.translate(ifSuccess: () {
              _reachedMax = result.data.data.isEmpty || result.data.data.length < 10;
              if (result.data.totalData != 0) {
                payload = result.data;
              }
            }, ifElse: () {
              result.statusCode.toFailure(result.message);
            }));
  }

  Future<void> retrieveMoreData() async {
    QueryProduct query = QueryProduct(category: 'Edukasi', page: ++page);
    var resp = await _repo.getConfigByCategory(query);
    resp.fold<void>(
        (failure) => throw failure,
        (result) => result.statusCode.translate(ifSuccess: () {
              _reachedMax = result.data.data.isEmpty || result.data.data.length < 10;
              if (result.data.totalData != 0) {
                payload = result.data;
              }
            }, ifElse: () {
              result.statusCode.toFailure(result.message);
            }));
  }

  void initialState() {
    page = 1;
  }

  Future<Payload<OrderProductDetail>> orderInquiry(OrderProductRequestModel model) async {
    var resp = await _repo.orderInquiry(model);

    return resp.fold<Payload<OrderProductDetail>>(
        (failure) => throw failure,
        (result) => result.statusCode.translate<Payload<OrderProductDetail>>(ifElse: () {
              throw result.statusCode.toFailure(result.message);
            }, ifSuccess: () {
              print('result');
              Logger().d(result.data.data.toJson());
              return result.data;
            }));
  }

  Future<void> createOrder(
      OrderProductRequestModel model, BuildContext context, dynamic totalPrice) async {
    var resp = await _repo.createOrder(model);

    resp.fold<void>(
        (failure) => throw failure,
        (result) => result.statusCode.translate<void>(ifElse: () {
              Navigator.pop(context, ModalRoute.withName('orderInquiry'));
              CustomFlushBar.errorFlushBar(result.message, context);
            }, ifSuccess: () {
              Navigator.pop(context, ModalRoute.withName('orderInquiry'));
              context.pushReplacement<void>(PaymentMethodPage(
                origin: 'spp',
                orderID: result.data.orderId,
                totalPrice: totalPrice,
                productType: 'ppob',
              ));
            }));
  }
}
