part of '../_states.dart';

class TransactionPostpaidPendingState
    implements FutureState<TransactionPostpaidPendingState, String> {
  TransactionPostpaidPendingState({Client client}) {
    TransactionRemoteDataSource dataSource = TransactionRemoteDataSourceImpl(client: client);
    _repo = TransactionRepositoryImpl(remoteDataSource: dataSource);
  }

  int page = 1;
  TransactionRepository _repo;
  bool _reachedMax;
  List<TransactionPulses> _dataPending;

  List<TransactionPulses> get listHistory => _dataPending;
  bool get hasReachedMax => _reachedMax;

  @override
  String cacheKey = CacheKeyConst.postPaidPendingState;

  @override
  bool getCondition() {
    return _dataPending != null;
  }

  @override
  Future<void> retrieveData(String transactionStatus) async {
    initialState();
    List<String> status = ['CREATED', 'PROCESS'];
    var query = QueryTransactionProduct(category: 'Pascabayar', ayopopStatus: status, page: page);
    var resp = await _repo.getTransactionProductHistory(query);
    resp.fold((failure) {
      Logger().d('$failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.data.isEmpty || result.data.data.length < 10;
              _dataPending = result.data.data;
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveMoreData() async {
    List<String> status = ['CREATED', 'PROCESS'];
    var query = QueryTransactionProduct(category: 'Pulsa', ayopopStatus: status, page: ++page);
    var resp = await _repo.getTransactionProductHistory(query);
    resp.fold((failure) {
      Logger().d('$failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.data.isEmpty || result.data.data.length < 10;
              if (result.data.totalData != 0) {
                print('dapet' + result.data.totalData.toString());
                _dataPending.addAll(result.data.data);
              }
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  void initialState() {
    page = 1;
  }
}
