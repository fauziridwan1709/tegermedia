part of '../_states.dart';

class TransactionEdukasiSuccessState
    implements FutureState<TransactionEdukasiSuccessState, String> {
  TransactionEdukasiSuccessState({Client client}) {
    TransactionRemoteDataSource dataSource = TransactionRemoteDataSourceImpl(client: client);
    _repo = TransactionRepositoryImpl(remoteDataSource: dataSource);
  }

  int page = 1;
  TransactionRepository _repo;
  bool _reachedMax;
  List<TransactionPulses> _dataSuccess;

  List<TransactionPulses> get listHistory => _dataSuccess;
  bool get hasReachedMax => _reachedMax;

  @override
  String cacheKey = CacheKeyConst.pulsaSuccessState;

  @override
  bool getCondition() {
    return _dataSuccess != null;
  }

  @override
  Future<void> retrieveData(String transactionStatus) async {
    initialState();
    List<String> status = ['SUCCESS', 'FAILED'];
    var query = QueryTransactionProduct(category: 'Edukasi', ayopopStatus: status, page: page);
    var resp = await _repo.getTransactionProductHistory(query);
    resp.fold((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.data.isEmpty || result.data.data.length < 10;
              if (result.data.data != null) {
                _dataSuccess = result.data.data;
              }
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveMoreData() async {
    List<String> status = ['SUCCESS', 'FAILED'];
    var query = QueryTransactionProduct(category: 'Edukasi', ayopopStatus: status, page: ++page);
    var resp = await _repo.getTransactionProductHistory(query);
    resp.fold((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.data.isEmpty || result.data.data.length < 10;
              if (result.data.data != null) {
                _dataSuccess.addAll(result.data.data);
              }
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  void initialState() {
    page = 1;
  }
}
