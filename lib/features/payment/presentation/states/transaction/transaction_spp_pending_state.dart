part of '../_states.dart';

class TransactionSppPendingState implements FutureState<TransactionSppPendingState, String> {
  TransactionSppPendingState({Client client}) {
    TransactionRemoteDataSource dataSource = TransactionRemoteDataSourceImpl(client: client);
    _repo = TransactionRepositoryImpl(remoteDataSource: dataSource);
  }

  int page = 1;
  TransactionRepository _repo;
  bool _reachedMax;
  List<Transaction> _dataPending;

  List<Transaction> get listPending => _dataPending;
  bool get hasReachedMax => _reachedMax;

  @override
  String cacheKey = CacheKeyConst.sppPendingState;

  @override
  bool getCondition() {
    return _dataPending != null;
  }

  @override
  Future<void> retrieveData(String transactionStatus) async {
    initialState();
    QueryTransactionSpp query = QueryTransactionSpp(status: 'PENDING');
    var resp = await _repo.getTransactionSppHistory(query);
    resp.fold<void>((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.isEmpty || result.data.length < query.limit;
              _dataPending = result.data;
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveMoreData() async {
    QueryTransactionSpp query = QueryTransactionSpp(status: 'PENDING', page: ++page);
    var resp = await _repo.getTransactionSppHistory(query);
    resp.fold<void>((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.isEmpty || result.data.length < query.limit;
              _dataPending.addAll(result.data);
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  void initialState() {
    page = 1;
  }
}
