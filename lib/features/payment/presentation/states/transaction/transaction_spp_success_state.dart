part of '../_states.dart';

class TransactionSppSuccessState implements FutureState<TransactionSppSuccessState, String> {
  TransactionSppSuccessState({Client client}) {
    TransactionRemoteDataSource dataSource = TransactionRemoteDataSourceImpl(client: client);
    _repo = TransactionRepositoryImpl(remoteDataSource: dataSource);
  }

  int page = 1;
  TransactionRepository _repo;
  bool _reachedMax;
  List<Transaction> _dataSuccess;

  List<Transaction> get listSuccess => _dataSuccess;
  bool get hasReachedMax => _reachedMax;

  @override
  String cacheKey = CacheKeyConst.sppSuccessState;

  @override
  bool getCondition() {
    return _dataSuccess != null;
  }

  @override
  Future<void> retrieveData(String transactionStatus) async {
    initialState();
    QueryTransactionSpp query = QueryTransactionSpp(status: 'SUCCESS');
    var resp = await _repo.getTransactionSppHistory(query);
    resp.fold<void>((failure) {
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.isEmpty || result.data.length < query.limit;
              _dataSuccess = result.data;
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveMoreData() async {
    QueryTransactionSpp query = QueryTransactionSpp(status: 'SUCCESS', page: ++page);
    var resp = await _repo.getTransactionSppHistory(query);
    resp.fold<void>((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.isEmpty || result.data.length < query.limit;
              _dataSuccess.addAll(result.data);
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  void initialState() {
    page = 1;
  }
}
