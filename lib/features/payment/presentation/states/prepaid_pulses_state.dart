part of '_states.dart';

class PrePaidPulsesState implements FutureState<PrePaidPulsesState, int> {
  @override
  String cacheKey = 'PrePaidPulsesState';

  String number;
  String orderId;
  OrderProductDetail orderProductDetail;
  final PulsaRepository repo = PulsaRepositoryImpl();

  List<PacketPulses> prepaidPulses = [];

  @override
  bool getCondition() {
    return true;
  }

  @override
  Future<void> retrieveData(int k) {
    // TODO: implement retrieveData
    return getListPrepaid();
  }

  Future<void> createOrder(PacketPulses pulses, String numberSelected, BuildContext context) async {
    SiswamediaTheme.loading(context);
    var queryPulses = <String, dynamic>{
      'productID': pulses.id,
      'accountNumber': number,
      'productCode': pulses.products.code,
      'month': 1,
    };

    var resp = await repo.createOrder(queryPulses);
    Navigator.pop(context);
    resp.fold((failure) {
      CustomFlushBar.errorFlushBar('Error !', context);
      throw failure;
    },
        (result) => result.statusCode.translate(ifSuccess: () {
              orderId = result.data;

              VNav().payment(context, orderProductDetail.amount + orderProductDetail.totalAdmin,
                  orderId, 'pulsa');
            }, ifElse: () {
              CustomFlushBar.errorFlushBar('Error !', context);
              throw result.toFailure();
            }));
  }

  Future<bool> pay(PacketPulses pulses, String numberSelected, BuildContext context) async {
    bool isSuccess = false;
    number = numberSelected;
    SiswamediaTheme.loading(context);
    var query = <String, dynamic>{
      'productID': pulses.id,
      'accountNumber': number,
      'productCode': pulses.products.code,
      'month': 1,
    };

    vUtils.setLog(query.toString());
    var resp = await repo.orderInquery(query);
    Navigator.pop(context);
    resp.fold((failure) {
      number = '0';
      CustomFlushBar.errorFlushBar(failure.translate(), context);
      throw failure;
    },
        (result) => result.statusCode.translate(ifSuccess: () {
              isSuccess = true;
              orderProductDetail = result.data;
              vUtils.setLog(result.data.inquiryId.toString());
            }, ifElse: () {
              CustomFlushBar.errorFlushBar(result.message, context);
              throw result.toFailure();
            }));
    return isSuccess;
  }

  Future<void> getListPrepaid() async {
    number = '0';
    vUtils.setLog('pre paid');
    var queryPulsa = QueryPulsa('Pascabayar', true, '');
    var resp = await repo.getAllAvailablePulsa(queryPulsa);
    resp.fold((failure) {
      print(failure.toString());
      throw failure;
    },
        (result) => result.statusCode.translate(
            ifSuccess: () {
              prepaidPulses = result.data.data;
            },
            ifElse: () => throw result.toFailure()));
  }
}
