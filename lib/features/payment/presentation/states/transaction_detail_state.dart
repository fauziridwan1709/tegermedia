part of '_states.dart';

class TransactionDetailState implements FutureState<TransactionDetailState, String> {
  final TransactionRepository repo = TransactionRepositoryImpl();

  List<Transaction> _dataPending;
  List<Transaction> _dataSuccess;

  List<Transaction> get listTransactionHistory => _dataPending;
  List<Transaction> get listSuccessTransactionHistory => _dataSuccess;

  @override
  String cacheKey = 'SPP_KEY';

  @override
  bool getCondition() {
    return _dataPending != null && _dataSuccess != null;
  }

  @override
  Future<void> retrieveData(String transactionStatus) async {
    var resp = await repo.getTransactionHistory(transactionStatus);
    resp.fold<void>((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              var statusSuccess =
                  result.data.where((element) => element.paymentStatus == 'SUCCESS').toList();
              var statusPending =
                  result.data.where((element) => element.paymentStatus == 'PENDING').toList();
              if (transactionStatus == 'init') {
                _dataPending = statusPending;
                _dataSuccess = statusSuccess;
              } else if (transactionStatus == 'pending') {
                _dataPending = statusPending;
              } else {
                _dataSuccess = statusSuccess;
              }
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveSuccess(String transactionStatus) async {
    var resp = await repo.getTransactionHistory(transactionStatus);
    resp.fold<void>((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              var statusSuccess = _dataPending =
                  result.data.where((element) => element.paymentStatus == 'PENDING').toList();

              _dataSuccess =
                  result.data.where((element) => element.paymentStatus == 'SUCCESS').toList();
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }
}
