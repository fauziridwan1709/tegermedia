part of '_enums.dart';

enum MarketStatusEnum {
  unpaid,
  pending,
  success,
  selesai,
}

extension MarketStatusX on MarketStatusEnum {
  String get getText => getValue(this);

  String getValue(MarketStatusEnum enumValue) {
    switch (enumValue) {

      ///CREATED
      case MarketStatusEnum.unpaid:
        return 'CREATED';

      ///PROCESS
      case MarketStatusEnum.pending:
        return 'PENDING';

      ///FAILED
      case MarketStatusEnum.success:
        return 'SUCCESS';

      ///SELESAI
      default:
        return 'SELESAI';
    }
  }

  String getStatus() {
    switch (this) {

      ///CREATED
      case MarketStatusEnum.unpaid:
        return 'Menunggu Pembayaran';

      ///PROCESS
      case MarketStatusEnum.pending:
        return 'Menunggu Pembayaran';

      ///FAILED
      case MarketStatusEnum.success:
        return 'Sukses';

      ///SELESAI
      default:
        return 'Sukses';
    }
  }

  Color getColor() {
    switch (this) {

      ///CREATED
      case MarketStatusEnum.unpaid:
        return SiswamediaTheme.red;

      ///PROCESS
      case MarketStatusEnum.pending:
        return SiswamediaTheme.red;

      ///FAILED
      case MarketStatusEnum.success:
        return SiswamediaTheme.green;

      ///SELESAI
      default:
        return SiswamediaTheme.green;
    }
  }
}

MarketStatusEnum getMarketStatusEnum(String value) {
  switch (value) {

    ///CREATED
    case 'CREATED':
      return MarketStatusEnum.unpaid;

    ///PROCESS
    case 'PENDING':
      return MarketStatusEnum.pending;

    ///FAILED
    case 'SUCCESS':
      return MarketStatusEnum.success;

    ///SELESAI
    default:
      return MarketStatusEnum.selesai;
  }
}
