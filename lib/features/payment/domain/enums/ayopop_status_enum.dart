part of '_enums.dart';

enum AyopopStatusEnum {
  created,
  process,
  failed,
  selesai,
}

extension AyopopStatusX on AyopopStatusEnum {
  String get getText => getValue(this);

  String getValue(AyopopStatusEnum enumValue) {
    switch (enumValue) {

      ///CREATED
      case AyopopStatusEnum.created:
        return 'CREATED';

      ///PROCESS
      case AyopopStatusEnum.process:
        return 'PROCESS';

      ///FAILED
      case AyopopStatusEnum.failed:
        return 'FAILED';

      ///SELESAI
      default:
        return 'SELESAI';
    }
  }

  String getStatus() {
    switch (this) {

      ///CREATED
      case AyopopStatusEnum.created:
        return 'Menunggu Pembayaran';

      ///PROCESS
      case AyopopStatusEnum.process:
        return 'Diproses';

      ///FAILED
      case AyopopStatusEnum.failed:
        return 'Gagal';

      ///SELESAI
      default:
        return 'Sukses';
    }
  }

  Color getColor() {
    switch (this) {

      ///CREATED
      case AyopopStatusEnum.created:
        return SiswamediaTheme.red;

      ///PROCESS
      case AyopopStatusEnum.process:
        return SiswamediaTheme.blue;

      ///FAILED
      case AyopopStatusEnum.failed:
        return SiswamediaTheme.red;

      ///SELESAI
      default:
        return SiswamediaTheme.green;
    }
  }
}

AyopopStatusEnum getAyopopStatusEnum(String value) {
  switch (value) {

    ///CREATED
    case 'CREATED':
      return AyopopStatusEnum.created;

    ///PROCESS
    case 'PROCESS':
      return AyopopStatusEnum.process;

    ///FAILED
    case 'FAILED':
      return AyopopStatusEnum.failed;

    ///SELESAI
    default:
      return AyopopStatusEnum.selesai;
  }
}
