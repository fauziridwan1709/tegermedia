part of '_repositories.dart';

abstract class PulsaRepository {
  Future<Decide<Failure, Parsed<Payload>>> getAllAvailablePulsa(QueryPulsa query);
  Future<Decide<Failure, Parsed<String>>> createOrder(Map<String,dynamic>  query);
  Future<Decide<Failure, Parsed<OrderProductDetail>>> orderInquery(Map<String,dynamic>  query);
  Future<Decide<Failure, Parsed<OrderPaymentModels>>> orderPayment(String  query);
}
