part of '_repositories.dart';

abstract class TransactionRepository {
  Future<Decide<Failure, Parsed<List<Transaction>>>> getTransactionSppHistory(
      QueryTransactionSpp query);
  Future<Decide<Failure, Parsed<List<Transaction>>>> getTransactionHistory(String status);
  Future<Decide<Failure, Parsed<Transaction>>> getTransactionDetail(String transactionId);
  Future<Decide<Failure, Parsed<Payload<List<TransactionPulses>>>>> getTransactionPendingPulses(
      String limit, String number);
  Future<Decide<Failure, Parsed<Payload<List<TransactionPulses>>>>> getTransactionPulses(
      String limit, String number);
  Future<Decide<Failure, Parsed<Payload<List<TransactionPulses>>>>> getTransactionProductHistory(
      QueryTransactionProduct query);
}
