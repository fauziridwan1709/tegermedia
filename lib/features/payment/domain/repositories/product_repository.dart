part of '_repositories.dart';

abstract class ProductRepository {
  Future<Decide<Failure, Parsed<Payload<List<ProductConfigModel>>>>> getConfigByCategory(
      QueryProduct query);
  Future<Decide<Failure, Parsed<Payload<OrderProductDetail>>>> orderInquiry(
      OrderProductRequestModel model);
  Future<Decide<Failure, Parsed<Payload<String>>>> createOrder(OrderProductRequestModel model);
}
