part of '_repositories.dart';

abstract class MarketRepository {
  Future<Decide<Failure, Parsed<List<SppBill>>>> getAllBill(QueryBill query);
  Future<Decide<Failure, Parsed<String>>> confirmPayment(String paymentId);
  Future<Decide<Failure, Parsed<VoucherAvailability>>> checkVoucherAvailability(
      String code, String type);
  Future<Decide<Failure, Parsed<RegisterInvoice>>> registerInvoice(RegisterInvoice model);
  Future<Decide<Failure, Parsed<String>>> cancelTransaction(String trxId);
}
