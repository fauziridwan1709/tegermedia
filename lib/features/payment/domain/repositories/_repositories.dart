import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/payment/data/models/_models.dart';
import 'package:tegarmedia/features/payment/data/models/payload_model.dart';
import 'package:tegarmedia/features/payment/data/models/payment/nicepayment_model.dart';
import 'package:tegarmedia/features/payment/data/models/payment/order_payment_models.dart';
import 'package:tegarmedia/features/payment/domain/entities/_entities.dart';
import 'package:tegarmedia/features/payment/domain/entities/query_pulsa.dart';
import 'package:tegarmedia/features/payment/domain/entities/transaction_pulses.dart';

part 'payment_repository.dart';
part 'product_repository.dart';
part 'pulsa_repository.dart';
part 'spp_repository.dart';
part 'transaction_repository.dart';
