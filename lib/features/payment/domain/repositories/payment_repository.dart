part of '_repositories.dart';

abstract class PaymentRepository {
  Future<Decide<Failure, Parsed<List<PaymentMethodModel>>>> getListPaymentMethod(
      QueryConfigPaymentWithUrl query);
  Future<Decide<Failure, Parsed<String>>> confirmPayment(String paymentId);
  Future<Decide<Failure, Parsed<NicepayPaymentModel>>> confirmPPob(
      Map<String, dynamic> dataPayment);
  Future<Decide<Failure, Parsed<RegisterInvoiceResponse>>> registerInvoice(RegisterInvoice model);
}
