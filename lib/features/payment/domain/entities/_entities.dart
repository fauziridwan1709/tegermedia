import 'dart:ui';

import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/features/payment/data/models/_models.dart';
import 'package:tegarmedia/features/payment/domain/enums/_enums.dart';

part 'bills.dart';
part 'payment_bank.dart';
part 'product_config.dart';
part 'products.dart';
part 'query_bill.dart';
part 'query_config_payment.dart';
part 'query_product.dart';
part 'query_transaction_product.dart';
part 'query_transaction_spp.dart';
part 'register_invoice.dart';
part 'register_invoice_response.dart';
part 'spp_bill.dart';
// part 'transaction_pulses.dart';
part 'status_transaction.dart';
part 'transaction.dart';
part 'transaction_item.dart';
part 'voucher_availability.dart';
