class QueryPulsa {
  String category;
  bool productStatus;
  String search;

  QueryPulsa(this.category, this.productStatus, this.search);

  @override
  String toString() {
    return 'category=$category&productStatus=$productStatus&search=$search';
  }
}
