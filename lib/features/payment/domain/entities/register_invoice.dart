part of '_entities.dart';

class RegisterInvoice extends Models {
  String origin;
  String paymentMethod;
  String paymentMethodCode;
  String paymentProvider;
  String bankCode;
  String mitraCode;
  String voucher;
  String voucherType;
  String phoneNumber;
  List<String> productId;
  String userId;
  String productType;

  RegisterInvoice({
    this.origin,
    this.paymentMethod,
    this.paymentMethodCode,
    this.paymentProvider,
    this.bankCode,
    this.mitraCode,
    this.voucher,
    this.voucherType,
    this.phoneNumber,
    this.productId,
    this.userId,
    this.productType,
  });
}
