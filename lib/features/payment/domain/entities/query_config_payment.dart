part of '_entities.dart';

class QueryConfigPaymentWithUrl {
  String url;
  String origin;
  double amount;

  QueryConfigPaymentWithUrl({this.url, this.origin = 'market', this.amount});

  @override
  String toString() {
    return '$url?origin=$origin&amount=$amount';
  }
}