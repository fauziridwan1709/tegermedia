part of '_entities.dart';

class SppBill {
  int id;
  String userId;
  String schoolId;
  String type;
  int month;
  int year;
  String dueDate;
  int discount;
  int total;
  String disburmentAccountName;
  String disburmentAccountNumb;
  String disburmentAccountType;
  String disburmentBankName;
  String status;
  String createAt;
  String updatedAt;

  SppBill(
      {this.id,
      this.userId,
      this.schoolId,
      this.type,
      this.month,
      this.year,
      this.dueDate,
      this.discount,
      this.total,
      this.disburmentAccountName,
      this.disburmentAccountNumb,
      this.disburmentAccountType,
      this.disburmentBankName,
      this.status,
      this.createAt,
      this.updatedAt});

  SppBill.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    schoolId = json['school_id'];
    type = json['type'];
    month = json['month'];
    year = json['year'];
    dueDate = json['due_date'];
    discount = json['discount'];
    total = json['total'];
    disburmentAccountName = json['disburment_account_name'];
    disburmentAccountNumb = json['disburment_account_numb'];
    disburmentAccountType = json['disburment_account_type'];
    disburmentBankName = json['disburment_bank_name'];
    status = json['status'];
    createAt = json['create_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['school_id'] = schoolId;
    data['type'] = type;
    data['month'] = month;
    data['year'] = year;
    data['due_date'] = dueDate;
    data['discount'] = discount;
    data['total'] = total;
    data['disburment_account_name'] = disburmentAccountName;
    data['disburment_account_numb'] = disburmentAccountNumb;
    data['disburment_account_type'] = disburmentAccountType;
    data['disburment_bank_name'] = disburmentBankName;
    data['status'] = status;
    data['create_at'] = createAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
