part of '_entities.dart';

class Transaction {
  String iD;
  int userID;
  String invoiceID;
  String invoiceNumber;
  String productType;
  int productID;
  String trxID;
  String paymentID;
  String paymentStatus;
  String paymentState;
  int invoiceAmount;
  int adminFee;
  int totalAmount;
  String bank;
  String bankName;
  String paymentMethod;
  String paymentProvider;
  String userPayNumber;
  String checkoutURL;
  String token;
  int duration;
  String expiredAt;
  String transactionDate;
  String createdAt;
  String updatedAt;
  List<TransactionItemModel> items;

  Transaction({
    this.iD,
    this.userID,
    this.invoiceID,
    this.invoiceNumber,
    this.productType,
    this.productID,
    this.trxID,
    this.paymentID,
    this.paymentStatus,
    this.paymentState,
    this.invoiceAmount,
    this.adminFee,
    this.totalAmount,
    this.bank,
    this.bankName,
    this.paymentMethod,
    this.paymentProvider,
    this.userPayNumber,
    this.checkoutURL,
    this.token,
    this.duration,
    this.expiredAt,
    this.transactionDate,
    this.createdAt,
    this.updatedAt,
    this.items,
  });
}
