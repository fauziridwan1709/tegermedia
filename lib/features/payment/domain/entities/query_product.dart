part of '_entities.dart';

///status [paid, unpaid, pending]
class QueryProduct {
  int page;
  int limit;
  String category;
  bool productStatus;

  QueryProduct({
    this.page = 1,
    this.limit = Constants.limitPagination,
    this.category,
    this.productStatus = true,
  });

  @override
  String toString() {
    return 'category=$category&productStatus=$productStatus&page=$page&limit=$limit';
  }
}
