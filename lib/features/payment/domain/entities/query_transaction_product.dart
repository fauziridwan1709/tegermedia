part of '_entities.dart';

class QueryTransactionProduct {
  const QueryTransactionProduct(
      {this.ayopopStatus, this.category, this.page = 1, this.limit = Constants.limitPagination});

  final List<String> ayopopStatus;
  final String category;
  final int page;
  final int limit;

  @override
  String toString() {
    return 'ayopopStatus=${ayopopStatus.join(',').toUpperCase()}&category=$category&pageNumber=$page&pageLimit=$limit';
  }
}
