part of '_entities.dart';

class Products {
  String name;
  String code;
  String logo;
  int amount;
  String biller;
  String category;
  bool active;
  String type;
  String description;

  Products(
      {this.name,
      this.code,
      this.logo,
      this.amount,
      this.biller,
      this.category,
      this.active,
      this.type,
      this.description});
}
