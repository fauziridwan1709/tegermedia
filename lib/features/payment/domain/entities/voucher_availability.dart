part of '_entities.dart';

class VoucherAvailability {
  int id;
  String code;
  int minSpend;
  String discountType;
  int discountValue;
  int quota;
  String startAt;
  String endAt;

  VoucherAvailability(
      {this.id,
      this.code,
      this.minSpend,
      this.discountType,
      this.discountValue,
      this.quota,
      this.startAt,
      this.endAt});
}
