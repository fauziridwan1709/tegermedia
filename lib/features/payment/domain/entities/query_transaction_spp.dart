part of '_entities.dart';

class QueryTransactionSpp {
  const QueryTransactionSpp({this.status, this.page = 1, this.limit = Constants.limitPagination});

  final String status;
  final int page;
  final int limit;

  @override
  String toString() {
    return 'status=${status.toUpperCase()}&page=$page&limit=$limit';
  }
}
