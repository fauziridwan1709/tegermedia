part of '_entities.dart';

class TransactionItem {
  String name;
  String unit;
  int qty;
  int unitPrice;
  String type;

  TransactionItem({this.name, this.unit, this.qty, this.unitPrice, this.type});
}
