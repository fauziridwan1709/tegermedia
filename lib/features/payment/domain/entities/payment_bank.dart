part of '_entities.dart';

class PaymentBank {
  PaymentMethodModel model;
  int index;
  dynamic fee;

  PaymentBank({this.model, this.index, this.fee});
}
