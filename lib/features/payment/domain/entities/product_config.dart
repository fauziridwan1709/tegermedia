part of '_entities.dart';

class ProductConfig {
  String createdAt;
  String updatedAt;
  String id;
  ProductsModel products;
  String logo;
  int margin;
  bool isActive;

  ProductConfig(
      {this.createdAt,
      this.updatedAt,
      this.id,
      this.products,
      this.logo,
      this.margin,
      this.isActive});
}
