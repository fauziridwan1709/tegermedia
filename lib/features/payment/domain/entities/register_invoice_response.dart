part of '_entities.dart';

class RegisterInvoiceResponse {
  String invoiceID;
  String invoiceNumber;
  int invoiceAmount;
  String paymentState;
  String paymentID;
  String trxID;
  int totalAmount;
  int adminFee;
  int duration;
  String bank;
  String bankName;
  String paymentMethod;
  String paymentProvider;
  String transactionDate;
  String expiredAt;
  String userPayNumber;
  String checkoutURL;
  String token;

  RegisterInvoiceResponse(
      {this.invoiceID,
      this.invoiceNumber,
      this.invoiceAmount,
      this.paymentState,
      this.paymentID,
      this.trxID,
      this.totalAmount,
      this.adminFee,
      this.duration,
      this.bank,
      this.bankName,
      this.paymentMethod,
      this.paymentProvider,
      this.transactionDate,
      this.expiredAt,
      this.userPayNumber,
      this.checkoutURL,
      this.token});
}
