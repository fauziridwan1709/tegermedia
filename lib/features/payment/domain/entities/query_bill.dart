part of '_entities.dart';

///status [paid, unpaid, pending]
class QueryBill {
  int page;
  int limit;
  String type;
  String status;

  QueryBill({this.page = 1, this.limit = Constants.limitPagination, this.type, this.status});

  @override
  String toString() {
    return 'type=$type&status=$status&page=$page&limit=$limit';
  }
}
