part of '_entities.dart';

class StatusTransaction {
  String status;
  Color color;
  String asset;

  StatusTransaction.fromStatus(String value, String origin, bool isExpired) {
    if (origin == 'ppob') {
      AyopopStatusEnum stat = getAyopopStatusEnum(value);
      status = stat.getStatus();
      color = stat.getColor();
      if (isExpired && stat.getStatus() == 'Menunggu Pembayaran') {
        status = 'Kadaluarsa';
      }
      if (status == 'Menunggu Pembayaran') {
        color = SiswamediaTheme.blue;
      }
    } else {
      MarketStatusEnum stat = getMarketStatusEnum(value);
      status = stat.getStatus();
      color = stat.getColor();
      if (isExpired && status == 'Menunggu Pembayaran') {
        status = 'Kadaluarsa';
      }
      if (status == 'Menunggu Pembayaran') {
        color = SiswamediaTheme.blue;
      }
    }
    asset = _mapAsset(color);
  }

  String _mapAsset(Color color) {
    if (color == SiswamediaTheme.green) {
      return 'status_success.svg';
    } else if (color == SiswamediaTheme.blue) {
      return 'status_pending.svg';
    }
    return 'status_failed.svg';
  }
}
