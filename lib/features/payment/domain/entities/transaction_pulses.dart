import 'package:googleapis/manufacturers/v1.dart';

class TransactionPulses {
  String id;
  String invoiceNumber;
  dynamic margin;
  dynamic bill;
  dynamic total;
  String ayopopStatus;
  String failedReason;
  String logo;
  String ayopopTransactionDate;
  User user;
  Payment payment;
  Refund refund;
  Product product;
  String invDownloadLink;
  String paymentDate;
  String createdAt;
  String updatedAt;
  String updatedBy;

  TransactionPulses(
      {this.id,
      this.invoiceNumber,
      this.margin,
      this.bill,
      this.total,
      this.ayopopStatus,
      this.failedReason,
      this.logo,
      this.ayopopTransactionDate,
      this.user,
      this.payment,
      this.refund,
      this.product,
      this.invDownloadLink,
      this.paymentDate,
      this.createdAt,
      this.updatedAt,
      this.updatedBy});

  TransactionPulses.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? '';
    invoiceNumber = json['invoiceNumber'] ?? '';
    margin = json['margin'] ?? 0;
    bill = json['bill'] ?? 0;
    total = json['total'] ?? 0;
    ayopopStatus = json['ayopopStatus'] ?? '';
    failedReason = json['failedReason'] ?? '';
    logo = json['logo'] ?? '';
    ayopopTransactionDate = json['ayopopTransactionDate'] ?? '';
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    payment = json['payment'] != null ? Payment.fromJson(json['payment']) : null;
    refund = json['refund'] != null ? Refund.fromJson(json['refund']) : null;
    product = json['product'] != null ? Product.fromJson(json['product']) : null;
    invDownloadLink = json['InvDownloadLink'] ?? '';
    paymentDate = json['PaymentDate'] ?? '';
    createdAt = json['createdAt'] ?? '';
    updatedAt = json['updatedAt'] ?? '';
    updatedBy = json['updatedBy'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['invoiceNumber'] = this.invoiceNumber;
    data['margin'] = this.margin;
    data['bill'] = this.bill;
    data['total'] = this.total;
    data['ayopopStatus'] = this.ayopopStatus;
    data['failedReason'] = this.failedReason;
    data['logo'] = this.logo;
    data['ayopopTransactionDate'] = this.ayopopTransactionDate;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.payment != null) {
      data['payment'] = this.payment.toJson();
    }
    if (this.refund != null) {
      data['refund'] = this.refund.toJson();
    }
    if (this.product != null) {
      data['product'] = this.product.toJson();
    }
    data['InvDownloadLink'] = this.invDownloadLink;
    data['PaymentDate'] = this.paymentDate;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['updatedBy'] = this.updatedBy;
    return data;
  }
}

class User {
  dynamic iId;
  String role;
  String deviceId;

  User({this.iId, this.role, this.deviceId});

  User.fromJson(Map<String, dynamic> json) {
    iId = json['_id'] ?? 0;
    role = json['role'] ?? '';
    deviceId = json['device_id'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.iId;
    data['role'] = this.role;
    data['device_id'] = this.deviceId;
    return data;
  }
}

class Payment {
  String paymentID;
  String invoiceID;
  String paymentState;
  String trxID;
  dynamic totalAmount;
  dynamic adminFee;
  String bankName;
  String bankCd;
  String mitraCode;
  String paymentMethod;
  String paymentProvider;
  dynamic duration;
  String expiredAt;
  String transactionDate;

  Payment(
      {this.paymentID,
      this.invoiceID,
      this.paymentState,
      this.trxID,
      this.totalAmount,
      this.adminFee,
      this.bankName,
      this.bankCd,
      this.mitraCode,
      this.paymentMethod,
      this.paymentProvider,
      this.duration,
      this.expiredAt,
      this.transactionDate});

  Payment.fromJson(Map<String, dynamic> json) {
    paymentID = json['paymentID'] ?? '';
    invoiceID = json['invoiceID'] ?? '';
    paymentState = json['paymentState'] ?? '';
    trxID = json['trxID'] ?? '';
    totalAmount = json['totalAmount'] ?? 0;
    adminFee = json['adminFee'] ?? 0;
    bankName = json['bankName'];
    bankCd = json['bankCd'];
    mitraCode = json['mitraCd'] ?? json['mitraCd'] ?? '';
    paymentMethod = json['paymentMethod'] ?? '';
    paymentProvider = json['paymentProvider'] ?? '';
    duration = json['duration'] ?? 0;
    expiredAt = json['expiredAt'] ?? '';
    transactionDate = json['transactionDate'] ?? null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['paymentID'] = this.paymentID;
    data['invoiceID'] = this.invoiceID;
    data['paymentState'] = this.paymentState;
    data['trxID'] = this.trxID;
    data['totalAmount'] = this.totalAmount;
    data['adminFee'] = adminFee;
    data['mitraCode'] = mitraCode;
    data['paymentMethod'] = this.paymentMethod;
    data['paymentProvider'] = this.paymentProvider;
    data['duration'] = this.duration;
    data['expiredAt'] = this.expiredAt;
    data['transactionDate'] = transactionDate;
    return data;
  }
}

class Refund {
  String origin;
  String paymentId;
  String paymentProvider;
  RefundData refundData;

  Refund({this.origin, this.paymentId, this.paymentProvider, this.refundData});

  Refund.fromJson(Map<String, dynamic> json) {
    origin = json['origin'] ?? '';
    paymentId = json['payment_id'] ?? '';
    paymentProvider = json['paymentProvider'] ?? '';
    refundData = json['refundData'] != null ? new RefundData.fromJson(json['refundData']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['origin'] = this.origin;
    data['payment_id'] = this.paymentId;
    data['paymentProvider'] = this.paymentProvider;
    if (this.refundData != null) {
      data['refundData'] = this.refundData.toJson();
    }
    return data;
  }
}

class RefundData {
  String refundReason;
  String refundDestination;
  String auditorActionReason;
  bool isFullRefund;

  RefundData(
      {this.refundReason, this.refundDestination, this.auditorActionReason, this.isFullRefund});

  RefundData.fromJson(Map<String, dynamic> json) {
    refundReason = json['refundReason'] ?? '';
    refundDestination = json['refundDestination'] ?? '';
    auditorActionReason = json['auditorActionReason'] ?? '';
    isFullRefund = json['isFullRefund'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['refundReason'] = this.refundReason;
    data['refundDestination'] = this.refundDestination;
    data['auditorActionReason'] = this.auditorActionReason;
    data['isFullRefund'] = this.isFullRefund;
    return data;
  }
}

class Product {
  dynamic inquiryId;
  String refNumber;
  String accountNumber;
  String productCode;
  String customerName;
  String productName;
  String category;
  dynamic amount;
  dynamic totalAdmin;
  String validity;
  List<CustomerDetail> customerDetail;
  List<BillDetails> billDetails;
  List<ProductDetail> productDetails;

  Product(
      {this.inquiryId,
      this.refNumber,
      this.accountNumber,
      this.productCode,
      this.customerName,
      this.productName,
      this.category,
      this.amount,
      this.totalAdmin,
      this.validity,
      this.customerDetail,
      this.billDetails,
      this.productDetails});

  Product.fromJson(Map<String, dynamic> json) {
    inquiryId = json['inquiryId'] ?? 0;
    refNumber = json['refNumber'] ?? '';
    accountNumber = json['accountNumber'] ?? '';
    productCode = json['productCode'] ?? '';
    customerName = json['customerName'] ?? '';
    productName = json['productName'] ?? '';
    category = json['category'] ?? '';
    amount = json['amount'] ?? 0;
    totalAdmin = json['totalAdmin'] ?? 0;
    validity = json['validity'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['inquiryId'] = this.inquiryId;
    data['refNumber'] = this.refNumber;
    data['accountNumber'] = this.accountNumber;
    data['productCode'] = this.productCode;
    data['customerName'] = this.customerName;
    data['productName'] = this.productName;
    data['category'] = this.category;
    data['amount'] = this.amount;
    data['totalAdmin'] = this.totalAdmin;
    data['validity'] = this.validity;
    return data;
  }
}

class CustomerDetail {
  String key;
  String value;

  CustomerDetail({this.key, this.value});

  CustomerDetail.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['value'] = this.value;
    return data;
  }
}

class BillDetails {
  String billId;
  String key;
  String value;

  BillDetails({this.billId, this.key, this.value});

  BillDetails.fromJson(Map<String, dynamic> json) {
    billId = json['billId'];
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['billId'] = this.billId;
    data['key'] = this.key;
    data['value'] = this.value;
    return data;
  }
}
