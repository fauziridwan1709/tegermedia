part of '_datasources.dart';

abstract class SchoolAdmissionRemoteDataSource {
  Future<Parsed<List<SchoolAdmission>>> getSchoolAdmissions();
  Future<Parsed<List<AdmissionPurchase>>> getAdmissionPurchases(String status);
}

class SchoolAdmissionRemoteDataSourceImpl implements SchoolAdmissionRemoteDataSource {
  @override
  Future<Parsed<List<AdmissionPurchase>>> getAdmissionPurchases(String status) async {
    var url = '${Endpoints.admission}/purchases?status=${status.toUpperCase()}';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    var list = <AdmissionPurchase>[];
    for (var data in resp.bodyAsMap['payload']) {
      list.add(AdmissionPurchaseModel.fromJson(data));
    }
    return resp.parse(list);
  }

  @override
  Future<Parsed<List<SchoolAdmission>>> getSchoolAdmissions() async {
    var url = '${Endpoints.admission}/schools';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    var list = <SchoolAdmission>[];
    for (var data in resp.bodyAsMap['payload']) {
      list.add(SchoolAdmissionModel.fromJson(data));
    }
    return resp.parse(list);
  }
}
