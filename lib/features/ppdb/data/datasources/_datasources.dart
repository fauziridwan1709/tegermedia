import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/features/ppdb/data/models/_models.dart';
import 'package:tegarmedia/features/ppdb/domain/entities/_entities.dart';

part 'data_source.dart';
part 'school_admission_remote_data_source.dart';
