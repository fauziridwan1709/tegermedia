part of '_models.dart';

class AdmissionModel extends Admission {
  AdmissionModel({
    int id,
    int schoolId,
    String startDate,
    String endDate,
    String name,
    int price,
    int quota,
  }) : super(
          id: id,
          schoolId: schoolId,
          startDate: startDate,
          endDate: endDate,
          name: name,
          price: price,
          quota: quota,
        );

  AdmissionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    name = json['name'];
    price = json['price'];
    quota = json['quota'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['name'] = name;
    data['price'] = price;
    data['quota'] = quota;
    return data;
  }
}
