part of '_models.dart';

class SchoolAdmissionModel extends SchoolAdmission {
  SchoolAdmissionModel({
    int id,
    String invitationCode,
    String name,
    String provinsiId,
    String kotaId,
    String type,
    String grade,
    String address,
    String headmasterName,
    String phone,
    String email,
    String adminEmail,
    bool isVerified,
    String websiteUrl,
    String adminName,
    String postalCode,
    bool personal,
    String description,
    int logoImageId,
    String logoUrl,
    List<AdmissionModel> admissions,
  }) : super(
          id: id,
          invitationCode: invitationCode,
          name: name,
          provinsiId: provinsiId,
          kotaId: kotaId,
          type: type,
          grade: grade,
          address: address,
          headmasterName: headmasterName,
          phone: phone,
          email: email,
          adminEmail: adminEmail,
          isVerified: isVerified,
          websiteUrl: websiteUrl,
          adminName: adminName,
          postalCode: postalCode,
          personal: personal,
          description: description,
          logoImageId: logoImageId,
          logoUrl: logoUrl,
          admissions: admissions,
        );

  SchoolAdmissionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    invitationCode = json['invitation_code'];
    name = json['name'];
    provinsiId = json['provinsi_id'];
    kotaId = json['kota_id'];
    type = json['type'];
    grade = json['grade'];
    address = json['address'];
    headmasterName = json['headmaster_name'];
    phone = json['phone'];
    email = json['email'];
    adminEmail = json['admin_email'];
    isVerified = json['is_verified'];
    websiteUrl = json['website_url'];
    adminName = json['admin_name'];
    postalCode = json['postal_code'];
    personal = json['personal'];
    description = json['description'];
    logoImageId = json['logo_image_id'];
    logoUrl = json['logo_url'];
    if (json['admissions'] != null) {
      admissions = <AdmissionModel>[];
      json['admissions'].forEach((dynamic v) {
        admissions.add(AdmissionModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['invitation_code'] = invitationCode;
    data['name'] = name;
    data['provinsi_id'] = provinsiId;
    data['kota_id'] = kotaId;
    data['type'] = type;
    data['grade'] = grade;
    data['address'] = address;
    data['headmaster_name'] = headmasterName;
    data['phone'] = phone;
    data['email'] = email;
    data['admin_email'] = adminEmail;
    data['is_verified'] = isVerified;
    data['website_url'] = websiteUrl;
    data['admin_name'] = adminName;
    data['postal_code'] = postalCode;
    data['personal'] = personal;
    data['description'] = description;
    data['logo_image_id'] = logoImageId;
    data['logo_url'] = logoUrl;
    if (admissions != null) {
      data['admissions'] = admissions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
