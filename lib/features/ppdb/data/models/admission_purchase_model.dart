part of '_models.dart';

class AdmissionPurchaseModel extends AdmissionPurchase {
  AdmissionPurchaseModel({
    int id,
    int admissionId,
    int userId,
    String studentEmail,
    String studentFirstName,
    String studentLastName,
    String studentPhone,
    String studentAddress,
    String guardianEmail,
    String guardianName,
    String guardianPhone,
    String relationToGuardian,
    String paymentStatus,
    String status,
    String createdAt,
    String updatedAt,
  }) : super(
          id: id,
          admissionId: admissionId,
          userId: userId,
          studentEmail: studentEmail,
          studentFirstName: studentFirstName,
          studentLastName: studentLastName,
          studentPhone: studentPhone,
          studentAddress: studentAddress,
          guardianEmail: guardianEmail,
          guardianName: guardianName,
          guardianPhone: guardianPhone,
          relationToGuardian: relationToGuardian,
          paymentStatus: paymentStatus,
          status: status,
          createdAt: createdAt,
          updatedAt: updatedAt,
        );

  AdmissionPurchaseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    admissionId = json['admission_id'];
    userId = json['user_id'];
    studentEmail = json['student_email'];
    studentFirstName = json['student_first_name'];
    studentLastName = json['student_last_name'];
    studentPhone = json['student_phone'];
    studentAddress = json['student_address'];
    guardianEmail = json['guardian_email'];
    guardianName = json['guardian_name'];
    guardianPhone = json['guardian_phone'];
    relationToGuardian = json['relation_to_guardian'];
    paymentStatus = json['payment_status'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['admission_id'] = admissionId;
    data['user_id'] = userId;
    data['student_email'] = studentEmail;
    data['student_first_name'] = studentFirstName;
    data['student_last_name'] = studentLastName;
    data['student_phone'] = studentPhone;
    data['student_address'] = studentAddress;
    data['guardian_email'] = guardianEmail;
    data['guardian_name'] = guardianName;
    data['guardian_phone'] = guardianPhone;
    data['relation_to_guardian'] = relationToGuardian;
    data['payment_status'] = paymentStatus;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
