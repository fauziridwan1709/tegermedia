import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/ppdb/data/datasources/_datasources.dart';
import 'package:tegarmedia/features/ppdb/domain/entities/_entities.dart';
import 'package:tegarmedia/features/ppdb/domain/repositories/_repositories.dart';

part 'school_admission_repository_impl.dart';
