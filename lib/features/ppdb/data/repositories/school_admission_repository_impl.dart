part of '_repositories.dart';

class SchoolAdmissionRepositoryImpl implements SchoolAdmissionRepository {
  final SchoolAdmissionRemoteDataSource remoteDataSource = SchoolAdmissionRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<AdmissionPurchase>>>> getAdmissionPurchases(
      String status) async {
    return await apiCall(remoteDataSource.getAdmissionPurchases(status));
  }

  @override
  Future<Decide<Failure, Parsed<List<SchoolAdmission>>>> getSchoolAdmissions() async {
    return await apiCall(remoteDataSource.getSchoolAdmissions());
  }
}
