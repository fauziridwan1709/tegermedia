part of '_entities.dart';

class Admission {
  int id;
  int schoolId;
  String startDate;
  String endDate;
  String name;
  int price;
  int quota;

  Admission(
      {this.id, this.schoolId, this.startDate, this.endDate, this.name, this.price, this.quota});
}
