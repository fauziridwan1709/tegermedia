part of '_entities.dart';

class AdmissionPurchase {
  int id;
  int admissionId;
  int userId;
  String studentEmail;
  String studentFirstName;
  String studentLastName;
  String studentPhone;
  String studentAddress;
  String guardianEmail;
  String guardianName;
  String guardianPhone;
  String relationToGuardian;
  String paymentStatus;
  String status;
  String createdAt;
  String updatedAt;

  AdmissionPurchase(
      {this.id,
      this.admissionId,
      this.userId,
      this.studentEmail,
      this.studentFirstName,
      this.studentLastName,
      this.studentPhone,
      this.studentAddress,
      this.guardianEmail,
      this.guardianName,
      this.guardianPhone,
      this.relationToGuardian,
      this.paymentStatus,
      this.status,
      this.createdAt,
      this.updatedAt});
}
