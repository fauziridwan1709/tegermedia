part of '_entities.dart';

class SchoolAdmission {
  int id;
  String invitationCode;
  String name;
  String provinsiId;
  String kotaId;
  String type;
  String grade;
  String address;
  String headmasterName;
  String phone;
  String email;
  String adminEmail;
  bool isVerified;
  String websiteUrl;
  String adminName;
  String postalCode;
  bool personal;
  String description;
  int logoImageId;
  String logoUrl;
  List<AdmissionModel> admissions;

  SchoolAdmission(
      {this.id,
      this.invitationCode,
      this.name,
      this.provinsiId,
      this.kotaId,
      this.type,
      this.grade,
      this.address,
      this.headmasterName,
      this.phone,
      this.email,
      this.adminEmail,
      this.isVerified,
      this.websiteUrl,
      this.adminName,
      this.postalCode,
      this.personal,
      this.description,
      this.logoImageId,
      this.logoUrl,
      this.admissions});
}
