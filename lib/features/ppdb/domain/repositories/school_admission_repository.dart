part of '_repositories.dart';

abstract class SchoolAdmissionRepository {
  Future<Decide<Failure, Parsed<List<SchoolAdmission>>>> getSchoolAdmissions();
  Future<Decide<Failure, Parsed<List<AdmissionPurchase>>>> getAdmissionPurchases(String status);
}