part of '_states.dart';

class AdmissionState implements FutureState<AdmissionState, BuildContext> {
  AdmissionState();

  final SchoolAdmissionRepository repoAdmission = SchoolAdmissionRepositoryImpl();

  List<SchoolAdmission> _schoolAdmission;

  List<SchoolAdmission> get getSchoolAdmission => _schoolAdmission;

  @override
  String cacheKey = 'TIME_ADMISSION_STATE';

  @override
  bool getCondition() {
    return false;
  }

  @override
  Future<void> retrieveData(BuildContext context) async {
    var resp = await repoAdmission.getSchoolAdmissions();
    resp.fold<void>((failure) => throw failure, (result) {
      result.statusCode.translate<void>(ifSuccess: () {
        _schoolAdmission = result.data;
      }, ifElse: () {
        throw result.statusCode.toFailure(result.message);
      });
    });
  }
}
