import 'package:flutter/material.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/features/ppdb/data/repositories/_repositories.dart';
import 'package:tegarmedia/features/ppdb/domain/entities/_entities.dart';
import 'package:tegarmedia/features/ppdb/domain/repositories/_repositories.dart';

part 'admission_state.dart';
