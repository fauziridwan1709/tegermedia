part of '_pages.dart';

class DetailSchoolPage extends StatefulWidget {
  final String name;
  final String imageUrl;
  final String title;
  final String Description;

  const DetailSchoolPage({Key key, this.name, this.imageUrl, this.title, this.Description})
      : super(key: key);

  @override
  _DetailSchoolPageState createState() => _DetailSchoolPageState();
}

class _DetailSchoolPageState extends BaseState<DetailSchoolPage, PaymentSchoolState> {
  @override
  Widget buildAppBar(BuildContext context) {
    // TODO: implement buildAppBar
    return AppBar(
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      backgroundColor: Colors.white,
      title: vText('List Sekolah'),
      centerTitle: true,
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    // TODO: implement buildAttribute
    return ScaffoldAttribute();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, ReactiveModel<PaymentSchoolState> snapshot,
      SizingInformation sizeInfo) {
    // TODO: implement buildNarrowLayout
    return Stack(
      children: [
        Column(
          children: [
            Image.network(
              widget.imageUrl,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              fit: BoxFit.fill,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 16,
                  ),
                  vText(widget.name,
                      color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
                  SizedBox(
                    height: 5,
                  ),
                  vText(widget.name,
                      color: SiswamediaTheme.lightBlack, fontSize: 12, fontWeight: FontWeight.bold),
                  SizedBox(
                    height: 10,
                  ),
                  vText(
                    widget.Description,
                    color: SiswamediaTheme.lightBlack,
                    fontSize: 12,
                  ),
                ],
              ),
            )
          ],
        ),
        Positioned(
          bottom: 24,
          left: 10,
          right: 10,
          child: InkWell(
            onTap: () => context.push<void>(RegisterStudentInformationPage()),
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 10),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: SiswamediaTheme.primary,
                borderRadius: BorderRadius.circular(10),
              ),
              child: vText('Daftar', color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, ReactiveModel<PaymentSchoolState> snapshot,
      SizingInformation sizeInfo) {
    // TODO: implement buildWideLayout
    throw UnimplementedError();
  }

  @override
  void init() {
    // TODO: implement init
  }

  @override
  Future<bool> onBackPressed() {
    // TODO: implement onBackPressed
    throw UnimplementedError();
  }

  @override
  Future<void> retrieveData() {
    // TODO: implement retrieveData
    throw UnimplementedError();
  }
}
