import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tegarmedia/aw/bases/_base.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/aw/widgets/button/_button.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/screen/_screen.dart';
import 'package:tegarmedia/features/payment/presentation/states/_states.dart';
import 'package:tegarmedia/features/ppdb/domain/entities/_entities.dart';
import 'package:tegarmedia/features/ppdb/presentation/states/_states.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/utils/v_widget.dart';

part 'detail_school_page.dart';
part 'list_school_page.dart';
part 'register_guardian_information_page.dart';
part 'register_student_information_page.dart';
