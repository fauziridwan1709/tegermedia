part of '_pages.dart';

class RegisterGuardianInformationPage extends StatefulWidget {
  @override
  _RegisterGuardianInformationPageState createState() => _RegisterGuardianInformationPageState();
}

class _RegisterGuardianInformationPageState extends BaseStateful<RegisterGuardianInformationPage>
    with RegisterPPDBValidator {
  TextEditingController _emailController;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  TextEditingController _phoneNumberController;
  TextEditingController _addressController;

  @override
  void init() {
    _emailController = TextEditingController();
    _firstNameController = TextEditingController();
    _lastNameController = TextEditingController();
    _phoneNumberController = TextEditingController();
    _addressController = TextEditingController();
  }

  Future<void> retrieveData() async {
    // TODO: implement retrieveData
  }

  @override
  ScaffoldAttribute buildAttribute() {
    // TODO: implement buildAttribute
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(
        'Register',
        style: GoogleFonts.openSans(
            fontWeight: FontWeight.bold, fontSize: 18, color: SiswamediaTheme.black),
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
            width: MediaQuery.of(context).size.width, height: 1, color: Colors.grey.shade300),
      ),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      children: [
        Content(
          'Student information',
          child: Container(
            width: 140,
            child: Divider(
              color: SiswamediaTheme.green,
              thickness: 2.5,
            ),
          ),
        ),
        _buildTextField(_emailController,
            hint: 'Email', stream: validateEmail, onChanged: onEmailChanged),
        _buildTextField(_firstNameController,
            hint: 'First Name', stream: validateFirstName, onChanged: onFirstNameChanged),
        _buildTextField(_lastNameController,
            hint: 'Last Name', stream: validateLastName, onChanged: onLastNameChanged),
        _buildTextField(_phoneNumberController,
            hint: 'Phone Number', stream: validatePhoneNumber, onChanged: onPhoneNumberChanged),
        _buildTextField(_addressController,
            hint: 'Address', stream: validateAddress, onChanged: onAddressChanged),
        HeightSpace(25),
        StreamBuilder<bool>(
            stream: isValid,
            builder: (context, snapshot) {
              return AutoLayoutButton(
                onTap: (snapshot.hasData ? !snapshot.data : true) ? null : () {},
                text: 'Next',
                textAlign: TextAlign.center,
                padding: EdgeInsets.symmetric(horizontal: 60, vertical: 12),
                radius: BorderRadius.circular(10),
                textColor: SiswamediaTheme.white,
                backGroundColor: SiswamediaTheme.green,
              );
            })
      ],
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    // TODO: implement buildWideLayout
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    // TODO: implement onBackPressed
    return true;
  }

  Padding _buildTextField(TextEditingController controller,
      {String hint, @required Stream stream, @required Function(String) onChanged}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 5),
      child: StreamBuilder<String>(
          stream: stream,
          builder: (context, snapshot) {
            return TextField(
              controller: controller,
              onChanged: onChanged,
              style: semiBlack.copyWith(color: Colors.black87, fontSize: 12),
              decoration: InputDecoration(
                  hintText: hint,
                  hintStyle: semiBlack.copyWith(color: Colors.black54, fontSize: 12),
                  errorText: snapshot.error,
                  border: UnderlineInputBorder(
                    borderSide: BorderSide(color: SiswamediaTheme.grey.shade400),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: SiswamediaTheme.grey.shade400),
                  )),
            );
          }),
    );
  }
}
