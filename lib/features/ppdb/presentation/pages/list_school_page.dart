part of '_pages.dart';

class ListSchoolPage extends StatefulWidget {
  @override
  _ListSchoolPageState createState() => _ListSchoolPageState();
}

class _ListSchoolPageState extends BaseState<ListSchoolPage, PaymentSchoolState> {
  final admission = Injector.getAsReactive<AdmissionState>();

  @override
  void init() {}

  @override
  Future<void> retrieveData() async {
    await admission.setState((s) => s.retrieveData(context));
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: SiswamediaTheme.primary,
        ),
      ),
      backgroundColor: Colors.white,
      title: vText('List Sekolah'),
      centerTitle: true,
      actions: [
        Icon(
          Icons.search,
          color: SiswamediaTheme.primary,
        )
      ],
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    // TODO: implement buildAttribute
    return ScaffoldAttribute();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, ReactiveModel<PaymentSchoolState> snapshot,
      SizingInformation sizeInfo) {
    return RefreshIndicator(
      key: refreshIndicatorKey,
      onRefresh: retrieveData,
      child: WhenRebuilder<AdmissionState>(
          observe: () => admission,
          onIdle: () => WaitingView(),
          onWaiting: () => WaitingView(),
          onError: (dynamic error) => ErrorView(error: error),
          onData: (data) {
            return ListView.builder(
              itemBuilder: (context, index) => _listSchool(data.getSchoolAdmission[index]),
              padding: EdgeInsets.all(17),
              itemCount: data.getSchoolAdmission.length,
            );
          }),
    );
  }

  Widget _listSchool(SchoolAdmission model) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.white,
          boxShadow: [SiswamediaTheme.shadowContainer]),
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            color: SiswamediaTheme.grey,
            child: Image.network(
              model.logoUrl,
              width: double.infinity,
              fit: BoxFit.fill,
              height: 130,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          vText(model.name, fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
          vText('${model.grade} ${model.type}',
              fontSize: 10, fontWeight: FontWeight.w600, color: SiswamediaTheme.lightBlack),
          SizedBox(
            height: 10,
          ),
          vText(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
              maxLines: 3,
              color: SiswamediaTheme.lightBlack,
              fontSize: 10),
          SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              VNav().detailSchool(
                  context,
                  '${model.grade} ${model.type}',
                  model.name,
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                  model.logoUrl);
            },
            child: Container(
                decoration: BoxDecoration(
                    color: SiswamediaTheme.primary, borderRadius: BorderRadius.circular(10)),
                width: double.infinity,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 9),
                child: vText('Lihat', color: Colors.white)),
          )
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, ReactiveModel<PaymentSchoolState> snapshot,
      SizingInformation sizeInfo) {
    // TODO: implement buildWideLayout
    return ListView.builder(
      itemBuilder: (c, i) => Container(),
      padding: EdgeInsets.all(17),
      itemCount: 3,
    );
  }

  @override
  Future<bool> onBackPressed() {
    // TODO: implement onBackPressed
    return Future.value(true);
  }
}
