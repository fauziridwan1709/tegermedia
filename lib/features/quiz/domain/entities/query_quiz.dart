part of '_entities.dart';

class QueryQuiz {
  int page;
  int limit;

  QueryQuiz({this.page, this.limit = Constants.limitPagination});

  @override
  String toString() {
    return 'limit=$limit&page=$page';
  }
}
