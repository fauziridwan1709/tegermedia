part of '_repositories.dart';

abstract class QuizMediaSekolahRepository {
  Future<Decide<Failure, Parsed<List<QuizListDetail>>>> getAllQuiz(QueryQuiz query);
}
