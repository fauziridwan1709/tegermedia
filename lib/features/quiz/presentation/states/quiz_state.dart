part of '_states.dart';

class QuizMediaState implements FutureState<QuizMediaState, QueryQuiz> {
  final QuizMediaSekolahRepository _repo = QuizMediaSekolahRepositoryImpl();
  int page = 1;
  List<QuizListDetail> _data;
  bool _reachedMax;

  List<QuizListDetail> get listData => _data;
  bool get hasReachedMax => _reachedMax;

  @override
  String cacheKey = 'cachingQuiz';

  @override
  bool getCondition() {
    return _data != null;
  }

  @override
  Future<void> retrieveData(QueryQuiz query) async {
    initialState();
    var resp = await _repo.getAllQuiz(query);
    resp.fold<void>((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.isEmpty || result.data.length < 10;
              if (result.data != null) {
                _data = result.data;
              }
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  Future<void> retrieveMoreData() async {
    var query = QueryQuiz(page: ++page);
    var resp = await _repo.getAllQuiz(query);
    resp.fold<void>((failure) {
      Logger().d('failure');
      throw failure;
    },
        (result) => result.statusCode.translate<void>(
            ifSuccess: () {
              _reachedMax = result.data.isEmpty || result.data.length < 10;
              if (result.data != null) {
                _data.addAll(result.data);
              }
            },
            ifElse: () => result.statusCode.toFailure(result.message)));
  }

  void initialState() {
    page = 1;
  }
}
