import 'package:logger/logger.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/features/quiz/data/repositories/_repositories.dart';
import 'package:tegarmedia/features/quiz/domain/entities/_entities.dart';
import 'package:tegarmedia/features/quiz/domain/repositories/_repositories.dart';
import 'package:tegarmedia/models/_models.dart';

part 'quiz_state.dart';
