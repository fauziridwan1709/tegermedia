part of '_datasources.dart';

abstract class QuizMediaSekolahRemoteDataSource {
  Future<Parsed<List<QuizListDetail>>> getAllQuiz(QueryQuiz query);
}

class QuizMediaSekolahRemoteDataSourceImpl implements QuizMediaSekolahRemoteDataSource {
  @override
  Future<Parsed<List<QuizListDetail>>> getAllQuiz(QueryQuiz query) async {
    String url = '${Endpoints.quizMobileListQuiz}?$query';
    var email = GlobalState.profile().state.profile.email;
    var token = await postIt(Endpoints.quizMobileLogin, <String, dynamic>{'email': email},
        headers: await Pref.getInfixHeader());
    var resp = await getIt(url,
        headers: <String, String>{'Authorization': 'Bearer ${token.bodyAsMap['data']}'});
    List<QuizListDetail> listData = <QuizListDetail>[];
    if (resp.dataBodyAsMap['data'] != null) {
      listData = <QuizListDetail>[];
      for (var v in resp.dataBodyAsMap['data']) {
        listData.add(QuizListDetail(
          name: v['title'] as String,
          startDate: v['date_start'] as String,
          endDate: v['date_end'] as String,
          description: v['link'] as String,
          courseName: v['subject_name'] as String,
          totalTime: v['time_limit'] as int,
          hasBeenDone: v['is_taked'] as bool,
          totalPg: v['total'] as int,
          status: v['status'] as int,
          totalNilai: getNilai(v),
        ));
      }
    }
    return resp.parse(listData);
  }

  double getNilai(Map<String, dynamic> v) {
    try {
      return (v['student_take']['own_mark'] / v['student_take']['total_mark']) * 100;
    } catch (e) {
      return null;
    }
  }
}
