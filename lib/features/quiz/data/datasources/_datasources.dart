import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/features/quiz/domain/entities/_entities.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/states/global_state.dart';

part 'quiz_media_sekolah_remote_data_source.dart';
