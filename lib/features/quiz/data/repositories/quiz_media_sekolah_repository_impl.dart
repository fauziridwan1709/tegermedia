part of '_repositories.dart';

class QuizMediaSekolahRepositoryImpl implements QuizMediaSekolahRepository {
  final QuizMediaSekolahRemoteDataSource remoteDataSource = QuizMediaSekolahRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<QuizListDetail>>>> getAllQuiz(QueryQuiz query) async {
    return await apiCall(remoteDataSource.getAllQuiz(query));
  }
}
