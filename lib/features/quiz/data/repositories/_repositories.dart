import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/quiz/data/datasources/_datasources.dart';
import 'package:tegarmedia/features/quiz/domain/entities/_entities.dart';
import 'package:tegarmedia/features/quiz/domain/repositories/_repositories.dart';
import 'package:tegarmedia/models/_models.dart';

part 'quiz_media_sekolah_repository_impl.dart';
