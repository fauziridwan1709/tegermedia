part of '_repositories.dart';

abstract class GoogleDriveRepository {
  Future<Decide<Failure, Parsed<DriveFolder>>> createGDriveFolder();
}
