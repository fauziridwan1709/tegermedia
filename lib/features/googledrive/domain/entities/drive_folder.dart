part of '_entities.dart';

class DriveFolder {
  int statusCode;
  String kind;
  String id;
  String name;
  String mimeType;

  DriveFolder({this.statusCode, this.kind, this.id, this.name, this.mimeType});
}
