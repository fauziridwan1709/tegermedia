part of '_datasources.dart';

abstract class GoogleDriveRemoteDataSource {
  Future<Parsed<DriveFolder>> createGDriveFolder();
}

class GoogleDriveRemoteDataSourceImpl implements GoogleDriveRemoteDataSource {
  @override
  Future<Parsed<DriveFolder>> createGDriveFolder() {
    // TODO: implement createGDriveFolder
    throw UnimplementedError();
  }
}
