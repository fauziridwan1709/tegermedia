import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/googledrive/data/datasources/_datasources.dart';
import 'package:tegarmedia/features/googledrive/domain/entities/_entities.dart';
import 'package:tegarmedia/features/googledrive/domain/repositories/_repositories.dart';

part 'google_drive_repository_impl.dart';
