part of '_repositories.dart';

class GoogleDriveRepositoryImpl implements GoogleDriveRepository {
  final remoteDataSource = GoogleDriveRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<DriveFolder>>> createGDriveFolder() async {
    return await apiCall(remoteDataSource.createGDriveFolder());
  }
}
