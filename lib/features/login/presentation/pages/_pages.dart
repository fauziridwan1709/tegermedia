import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tegarmedia/aw/bases/_base.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/aw/widgets/button/_button.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/screen/_screen.dart';
import 'package:tegarmedia/states/_states.dart';
