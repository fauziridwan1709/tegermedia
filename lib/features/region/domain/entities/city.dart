part of '_entities.dart';

class City {
  int id;
  int provinceId;
  String provinceName;
  int countryId;
  String code;
  String name;
  bool status;
  String createdAt;
  String updatedAt;
  String deletedAt;

  City(
      {this.id,
      this.provinceId,
      this.provinceName,
      this.countryId,
      this.code,
      this.name,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});
}
