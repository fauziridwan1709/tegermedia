part of '_entities.dart';

class QueryCity {
  int page;
  int limit;
  int provinceId;
  bool status;
  String search;
  String orderBy;
  String sort;

  QueryCity(
      {this.page = 1,
      this.limit = 36,
      this.provinceId,
      this.status = true,
      this.search,
      this.orderBy,
      this.sort = 'ASC'});

  @override
  String toString() {
    return 'page=$page&limit=$limit&province_id=$provinceId&status=$status&search=$search&order_by=$orderBy&sort=$sort';
  }
}
