part of '_entities.dart';

class Province {
  int id;
  int countryId;
  String countryName;
  String code;
  String name;
  bool status;
  String createdAt;
  String updatedAt;
  String deletedAt;

  Province(
      {this.id,
      this.countryId,
      this.countryName,
      this.code,
      this.name,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});
}
