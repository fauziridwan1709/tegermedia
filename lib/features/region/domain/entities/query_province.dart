part of '_entities.dart';

class QueryProvince {
  int page;
  int limit;
  int countryId;
  bool status;
  String search;
  String orderBy;
  String sort;

  QueryProvince(
      {this.page = 1,
      this.limit = 36,
      this.countryId = 1,
      this.status = true,
      this.search,
      this.orderBy,
      this.sort = 'ASC'});

  @override
  String toString() {
    return 'page=$page&limit=$limit&country_id=$countryId&status=$status&search=$search&order_by=$orderBy&sort=$sort';
  }
}
