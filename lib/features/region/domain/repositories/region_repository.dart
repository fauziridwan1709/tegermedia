part of '_repositories.dart';

abstract class RegionRepository {
  Future<Decide<Failure, Parsed<List<Province>>>> getProvinces(
      QueryProvince query);
  Future<Decide<Failure, Parsed<List<City>>>> getCities(QueryCity query);
}
