part of '_models.dart';

class ProvinceModel extends Province {
  ProvinceModel({
    int id,
    int countryId,
    String countryName,
    String code,
    String name,
    bool status,
    String createdAt,
    String updatedAt,
    String deletedAt,
  }) : super(
          id: id,
          countryId: countryId,
          countryName: countryName,
          code: code,
          name: name,
          status: status,
          createdAt: createdAt,
          updatedAt: updatedAt,
          deletedAt: deletedAt,
        );

  ProvinceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    countryId = json['country_id'];
    countryName = json['country_name'];
    code = json['code'];
    name = json['name'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['country_id'] = countryId;
    data['country_name'] = countryName;
    data['code'] = code;
    data['name'] = name;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}
