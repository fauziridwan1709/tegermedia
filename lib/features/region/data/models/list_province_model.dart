part of '_models.dart';

class ListProvinceModel {
  List<ProvinceModel> list;

  ListProvinceModel({this.list});

  ListProvinceModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <ProvinceModel>[];
      json['data'].forEach((dynamic v) {
        list.add(ProvinceModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
