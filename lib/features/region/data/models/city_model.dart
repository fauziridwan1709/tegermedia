part of '_models.dart';

class CityModel extends City {
  CityModel(
      {int id,
      int provinceId,
      String provinceName,
      int countryId,
      String code,
      String name,
      bool status,
      String createdAt,
      String updatedAt,
      String deletedAt})
      : super(
            id: id,
            provinceId: provinceId,
            provinceName: provinceName,
            countryId: countryId,
            code: code,
            name: name,
            status: status,
            createdAt: createdAt,
            updatedAt: updatedAt,
            deletedAt: deletedAt);

  CityModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    provinceId = json['province_id'];
    provinceName = json['province_name'];
    countryId = json['country_id'];
    code = json['code'];
    name = json['name'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['province_id'] = provinceId;
    data['province_name'] = provinceName;
    data['country_id'] = countryId;
    data['code'] = code;
    data['name'] = name;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}
