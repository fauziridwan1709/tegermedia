part of '_models.dart';

class ListCityModel {
  List<CityModel> list;

  ListCityModel({this.list});

  ListCityModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <CityModel>[];
      json['data'].forEach((dynamic v) {
        list.add(CityModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
