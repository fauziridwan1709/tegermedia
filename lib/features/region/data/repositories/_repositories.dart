import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/region/data/datasources/_datasources.dart';
import 'package:tegarmedia/features/region/domain/entities/_entities.dart';
import 'package:tegarmedia/features/region/domain/repositories/_repositories.dart';

part 'region_repository_impl.dart';
