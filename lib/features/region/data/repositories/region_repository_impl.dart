part of '_repositories.dart';

class RegionRepositoryImpl implements RegionRepository {
  final remoteDataSource = RegionRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<Province>>>> getProvinces(
      QueryProvince query) async {
    return await apiCall(remoteDataSource.getProvinces(query));
  }

  @override
  Future<Decide<Failure, Parsed<List<City>>>> getCities(QueryCity query) async {
    return await apiCall(remoteDataSource.getCities(query));
  }
}
