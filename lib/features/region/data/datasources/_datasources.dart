import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/region/data/models/_models.dart';
import 'package:tegarmedia/features/region/domain/entities/_entities.dart';

part 'region_remote_data_source.dart';
