part of '_datasources.dart';

abstract class RegionRemoteDataSource {
  Future<Parsed<List<Province>>> getProvinces(QueryProvince query);
  Future<Parsed<List<City>>> getCities(QueryCity query);
}

class RegionRemoteDataSourceImpl implements RegionRemoteDataSource {
  @override
  Future<Parsed<List<City>>> getCities(QueryCity query) async {
    String mantap = '';
    var url = '${Endpoints.region}/city?$query';
    var resp = await getIt(url);
    return resp.parse(ListCityModel.fromJson(resp.bodyAsMap).list);
  }

  @override
  Future<Parsed<List<Province>>> getProvinces(QueryProvince query) async {
    var url = '${Endpoints.region}/province?$query';
    var resp = await getIt(url);
    return resp.parse(ListProvinceModel.fromJson(resp.bodyAsMap).list);
  }
}
