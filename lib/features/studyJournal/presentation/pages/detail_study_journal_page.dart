part of '_pages.dart';

class DetailStudyJournalPage extends StatefulWidget {
  final CourseJourneyData data;

  const DetailStudyJournalPage({Key key, @required this.data}) : super(key: key);

  @override
  _DetailStudyJournalPageState createState() => _DetailStudyJournalPageState();
}

class _DetailStudyJournalPageState extends State<DetailStudyJournalPage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  var isExpanded = true;
  var isDescription = true;
  var isLinkMateri = true;
  var isTugas = true;
  var isExample = true;
  VideoPlayerController _controller;
  TextEditingController textEditingController = TextEditingController();
  final detailJournal = Injector.getAsReactive<DetailJournalState>();

  Future<void> test() async {
    var resp = await client.get(
        'https://drive.google.com/u/1/uc?export=download&confirm=W7Wj&id=19SvZSW161SbjCGwAuYqm4YCLe_1t5C6s');
    print('checksomething');
    print(resp.body);
  }

  @override
  void dispose() {
    detailJournal.refresh();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
    tabController.addListener(() {
      print(tabController.index.toString());
      setState(() {});
    });
    // Initializer(
    //   reactiveModel: detailJournal, cacheKey: '', state: null, rIndicator: null,
    //
    // ).initialize();
    detailJournal.setState((s) => s.getDetailJournal(widget.data.journalId), onData: (_, data) {
      if (data.detailJournal.videoUrl.isNotEmpty) {
        _controller = VideoPlayerController.network(
          detailJournal.state.detailJournal?.videoUrl ??
              'https://drive.google.com/uc?id=1f_sg8ybe1f6sSyD5pemwaI8iRCZRR27W&export=download',
          videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true),
        );
        _controller.addListener(() {
          setState(() {});
        });
        _controller.setLooping(false);
        _controller.initialize();
      }
    }, onError: (_, dynamic error) {
      print(error);
    });
  }

  QuizProviderKelas penilaianProvider;

  @override
  Widget build(BuildContext context) {
    penilaianProvider = Provider.of<QuizProviderKelas>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: vText(widget.data.name),
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_outlined,
            color: SiswamediaTheme.primary,
          ),
        ),
      ),
      body: StateBuilder<DetailJournalState>(
          observe: () => detailJournal,
          builder: (_, snapshot) {
            return WhenRebuilder<DetailJournalState>(
              observe: () => detailJournal,
              onWaiting: () => LoadingView(),
              onError: (dynamic error) => ErrorView(
                error: error,
              ),
              onIdle: () => WaitingView(),
              onData: (data) {
                print('dah ada datanya');
                return Column(
                  children: [
                    SizedBox(
                      height: 1,
                    ),
                    DefaultTabController(
                      length: 2,
                      initialIndex: 0,
                      child: TabBar(
                        controller: tabController,
                        indicatorColor: SiswamediaTheme.primary,
                        labelColor: SiswamediaTheme.primary,
                        unselectedLabelColor: Theme.of(context).dividerColor.withOpacity(0.54),
                        tabs: [
                          Tab(text: 'Pembahasan'),
                          Tab(text: 'Komentar'),
                        ],
                      ),
                    ),
                    Expanded(
                      child: TabBarView(
                        controller: tabController,
                        children: [itemData(), itemComment()],
                      ),
                    )
                  ],
                );
              },
            );
          }),
    );
  }

  Widget itemComment() {
    return StateBuilder<DetailJournalState>(
        observe: () => detailJournal,
        builder: (context, snapshot) {
          return WhenRebuilder<DetailJournalState>(
              observe: () => detailJournal,
              onWaiting: () => WaitingView(),
              onError: (dynamic error) => ErrorView(
                    error: error.toString(),
                  ),
              onIdle: () => WaitingView(),
              onData: (data) {
                print('dah ada datanya');
                return Column(
                  children: [
                    Expanded(
                      child: data.listComment.isEmpty
                          ? Center(
                              child: vText('Data Not Found'),
                            )
                          : ListView.builder(
                              itemCount: data.listComment.length,
                              padding: EdgeInsets.only(bottom: 10, left: 8, right: 8),
                              itemBuilder: (c, i) => Container(
                                    margin: EdgeInsets.only(left: 8, top: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                vText(data.listComment[i].senderName,
                                                    color: SiswamediaTheme.primary,
                                                    fontWeight: FontWeight.bold),
                                                SizedBox(
                                                  width: 8,
                                                ),
                                                vText(
                                                    formattedDate(data.listComment[i].sentDate)
                                                        .toString()
                                                        .toString(),
                                                    color: SiswamediaTheme.textGrey)
                                              ],
                                            ),
                                            Container(
                                                width: MediaQuery.of(context).size.width - 70,
                                                child: vText(data.listComment[i].comment,
                                                    maxLines: 5, overflow: TextOverflow.ellipsis)),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(top: BorderSide(color: Colors.grey, width: 1)),
                      ),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: TextFormField(
                              decoration: const InputDecoration(
                                  hintText: 'Tulis Pesan !',
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none),
                              maxLines: 5,
                              minLines: 1,
                              controller: textEditingController,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          InkWell(
                              onTap: () {
                                var message = textEditingController.text;
                                if (message != '') {
                                  detailJournal.setState((s) =>
                                      s.sendComment(message, widget.data.journalId, context));
                                }
                                textEditingController.clear();
                              },
                              child: Icon(
                                Icons.send_outlined,
                                color: SiswamediaTheme.primary,
                              )),
                          SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    )
                  ],
                );
              });
        });
  }

  Widget itemData() {
    return Container(
      color: Color(0xFFF9F9F9),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              color: SiswamediaTheme.white,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: title('Pembahasan Lengkap'),
            ),
            if (detailJournal.state.detailJournal.videoUrl != '')
              SizedBox(
                height: 12,
              ),
            if (detailJournal.state.detailJournal.videoUrl != '')
              Container(
                color: SiswamediaTheme.white,
                child: InkWell(
                  onTap: () {
                    isExpanded = openDetail(isExpanded);
                    setState(() {});
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                    child: Row(
                      children: [
                        title('Vidio Pembahasan'),
                        SizedBox(
                          width: 20,
                        ),
                        _iconEdit(),
                        Expanded(
                          child: Container(),
                        ),
                        _bottomTransform(!isExpanded)
                      ],
                    ),
                  ),
                ),
              ),
            if (detailJournal.state.detailJournal.videoUrl != '')
              _itemColapsing(!isExpanded, 200, _videoPlayer()),
            SizedBox(
              height: 12,
            ),
            ExpansionTile(
              backgroundColor: SiswamediaTheme.white,
              collapsedBackgroundColor: SiswamediaTheme.white,
              title: Row(
                children: <Widget>[
                  title('Deskripsi Pembahasan'),
                  WidthSpace(15),
                  _iconEdit(),
                ],
              ),
              expandedCrossAxisAlignment: CrossAxisAlignment.start,
              expandedAlignment: Alignment.centerLeft,
              childrenPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              children: [
                vText(detailJournal.state.detailJournal.guide,
                    align: TextAlign.left, color: Color(0xFF707070), fontSize: 14),
              ],
            ),
            SizedBox(
              height: 12,
            ),
            _divider(),
            ExpansionTile(
              backgroundColor: SiswamediaTheme.white,
              collapsedBackgroundColor: SiswamediaTheme.white,
              childrenPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
              title: title('Link Materi'),
              children: detailJournal.state.detailJournal.attachment.map((e) {
                if (e.link.contains('youtube.com') || e.link.contains('youtu.be')) {
                  return _youtubePlayer(e.link);
                } else {
                  return InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                            builder: (_) => WebViewHome(
                                  url: e.link,
                                  title: e.name,
                                ))),
                    child: Row(
                      children: [
                        Container(
                          child: vText(
                            e.name,
                            color: Color(0xff63A2E2),
                          ),
                        ),
                        Expanded(
                          child: Container(),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: SiswamediaTheme.primary,
                              borderRadius: BorderRadius.circular(6)),
                          child: vText('Buka', color: Colors.white),
                          padding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                        )
                      ],
                    ),
                  );
                }
              }).toList(),
            ),
            SizedBox(
              height: 12,
            ),
            _divider(),
            ExpansionTile(
              backgroundColor: SiswamediaTheme.white,
              collapsedBackgroundColor: SiswamediaTheme.white,
              initiallyExpanded: true,
              title: Row(
                children: <Widget>[
                  title('Tugas'),
                  WidthSpace(15),
                  _iconEdit(),
                ],
              ),
              expandedCrossAxisAlignment: CrossAxisAlignment.start,
              expandedAlignment: Alignment.centerLeft,
              childrenPadding: EdgeInsets.symmetric(horizontal: 20),
              children: [
                if (detailJournal.state.listAssessment == null) ...[
                  Center(child: CircularProgressIndicator())
                ] else ...[
                  ListView.builder(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (c, i) => Column(
                      children: [
                        Row(
                          children: [
                            vText(detailJournal.state.listAssessment[i].name,
                                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),
                            SizedBox(
                              width: 20,
                            ),
                            //todo
                            // _iconEdit()
                          ],
                        ),
                        HeightSpace(5),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: vText(detailJournal.state.listAssessment[i].description,
                                  align: TextAlign.left, fontSize: 12, color: Color(0xFFAAAAAA)),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                        InkWell(
                          onTap: () =>
                              (GlobalState.auth().state.currentState.classRole.isSiswaOrOrtu)
                                  ? navigate(
                                      context,
                                      AssignmentDetail(
                                        referenceID: detailJournal.state.listAssessment[i].id,
                                        tugasName: detailJournal.state.listAssessment[i].name,
                                        startDate: detailJournal.state.listAssessment[i].startDate,
                                        endDate: detailJournal.state.listAssessment[i].endDate,
                                        assessmentID:
                                            detailJournal.state.listAssessment[i].assessmentId,
                                      ))
                                  : navigate(
                                      context,
                                      AssignmentDetailGuru(
                                        assessmentID: detailJournal.state.listAssessment[i].id,
                                        startDate: detailJournal.state.listAssessment[i].startDate,
                                        endDate: detailJournal.state.listAssessment[i].endDate,
                                        tugasName: detailJournal.state.listAssessment[i].name,
                                        tugasDescription:
                                            detailJournal.state.listAssessment[i].description,
                                      ))
                          // MaterialPageRoute<void>(
                          //     builder: (_) => WebViewHome(
                          //           url: detailJourneyState
                          //               .state.listAssessment[i].id
                          //               .toString(),
                          //           title:  detailJourneyState.state.listAssessment[i].name,
                          //         ))
                          ,
                          child: Row(
                            children: [
                              Icon(
                                Icons.hourglass_top_outlined,
                                size: 16,
                                color: Colors.black54,
                              ),
                              WidthSpace(5),
                              Container(
                                child: vText(
                                  formattedDate(detailJournal.state.listAssessment[i].startDate),
                                  fontSize: 12,
                                  color: Color(0xFF4B4B4B),
                                ),
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              Container(
                                child: vText('Lihat', color: Colors.white, fontSize: 12),
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.blue,
                                    borderRadius: BorderRadius.circular(4)),
                                padding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                      ],
                    ),
                    itemCount: detailJournal.state.listAssessment.length,
                    shrinkWrap: true,
                  )
                ]
              ],
            ),
            SizedBox(
              height: 12,
            ),
            ExpansionTile(
              backgroundColor: SiswamediaTheme.white,
              collapsedBackgroundColor: SiswamediaTheme.white,
              initiallyExpanded: true,
              title: Row(
                children: <Widget>[
                  title('Ujian / Latihan Soal'),
                  WidthSpace(15),
                  _iconEdit(),
                ],
              ),
              expandedCrossAxisAlignment: CrossAxisAlignment.start,
              expandedAlignment: Alignment.centerLeft,
              childrenPadding: EdgeInsets.symmetric(horizontal: 20),
              children: [
                if (detailJournal.state.listQuiz == null) ...[
                  Center(
                      child: Container(
                          width: 25,
                          height: 25,
                          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          child: CircularProgressIndicator()))
                ] else ...[
                  ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (c, i) => Column(
                      children: [
                        Row(
                          children: [
                            vText(detailJournal.state.listQuiz[i].name,
                                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),
                            SizedBox(
                              width: 20,
                            ),
                            //todo
                            // _iconEdit()
                          ],
                        ),
                        HeightSpace(5),
                        Row(
                          children: <Widget>[
                            vText(
                                'Nilai: ${detailJournal.state.listQuiz[i].totalNilai < 0 ? 'Belum Dikerjakan' : detailJournal.state.listQuiz[i].totalNilai}',
                                fontSize: 12,
                                color: Color(0xFFAAAAAA)),
                          ],
                        ),
                        SizedBox(height: 5),
                        InkWell(
                          onTap: () async {
                            await penilaianProvider.setDetail(QuizListDetail(
                                courseId: detailJournal.state.listQuiz[i].courseId,
                                classId: detailJournal.state.listQuiz[i].classId,
                                className: detailJournal.state.listQuiz[i].className,
                                courseName: detailJournal.state.listQuiz[i].courseName,
                                startDate: detailJournal.state.listQuiz[i].startDate,
                                schoolId: detailJournal.state.listQuiz[i].schoolId,
                                schoolName: detailJournal.state.listQuiz[i].schoolName,
                                name: detailJournal.state.listQuiz[i].name,
                                totalEssay: detailJournal.state.listQuiz[i].totalEssay,
                                totalPg: detailJournal.state.listQuiz[i].totalPg,
                                totalTime: detailJournal.state.listQuiz[i].totalTime,
                                quizType: detailJournal.state.listQuiz[i].quizType,
                                id: detailJournal.state.listQuiz[i].id,
                                hasBeenDone: detailJournal.state.listQuiz[i].hasBeenDone,
                                totalNilai: detailJournal.state.listQuiz[i].totalNilai.toDouble(),
                                isHidden: detailJournal.state.listQuiz[i].isHidden,
                                isRandom: detailJournal.state.listQuiz[i].isRandom));
                            await Navigator.push<void>(
                                context, MaterialPageRoute(builder: (_) => QuizInfoKelas()));
                          },
                          child: Row(
                            children: [
                              Icon(
                                Icons.hourglass_top_outlined,
                                size: 16,
                                color: Colors.black54,
                              ),
                              WidthSpace(5),
                              Container(
                                child: vText(
                                  formattedDate(detailJournal.state.listQuiz[i].startDate),
                                  fontSize: 12,
                                  color: Color(0xFF4B4B4B),
                                ),
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    color: SiswamediaTheme.blue,
                                    borderRadius: BorderRadius.circular(4)),
                                padding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                                child: vText('Lihat', color: Colors.white, fontSize: 12),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                      ],
                    ),
                    itemCount: detailJournal.state.listQuiz.length,
                    shrinkWrap: true,
                  ),
                ]
              ],
            ),
            // InkWell(
            //   onTap: () {
            //     isExample = openDetail(isExample);
            //     setState(() {});
            //   },
            //   child: Padding(
            //     padding: EdgeInsets.symmetric(horizontal: 16),
            //     child: Row(
            //       children: [
            //         title('Ujian / Latihan Soal'),
            //         SizedBox(
            //           width: 20,
            //         ),
            //         Expanded(
            //           child: Container(),
            //         ),
            //         _bottomTransform(isExample)
            //       ],
            //     ),
            //   ),
            // ),
            SizedBox(
              height: 12,
            ),
            _divider(),
          ],
        ),
      ),
    );
  }

  Widget _videoPlayer() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
      ),
      margin: EdgeInsets.only(top: 12),
      child: AspectRatio(
        aspectRatio: _controller.value.aspectRatio,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            VideoPlayer(_controller),
            ClosedCaption(text: _controller.value.caption.text),
            _ControlsOverlay(controller: _controller),
            VideoProgressIndicator(_controller, allowScrubbing: true),
          ],
        ),
      ),
    );
  }

  Widget _youtubePlayer(String url) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
      ),
      margin: EdgeInsets.only(top: 12),
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: YoutubePlayer(
          videoId: youtube.YoutubePlayer.convertUrlToId(url),
          title: widget.data.name,
          isMoreInfo: false,
          description: '',
        ),
      ),
    );
  }

  Widget _bottomTransform(bool isExpand) {
    return Transform.rotate(
        angle: isExpand ? -89.55 : 89.55,
        child: Icon(Icons.chevron_left_outlined, size: 30, color: SiswamediaTheme.greyIcon));
  }

  Widget _itemColapsing(bool isExpand, double height, Widget child) {
    return AnimatedContainer(
        height: isExpand ? 0 : height,
        width: double.infinity,
        duration: Duration(milliseconds: (2000).round()),
        curve: Curves.fastLinearToSlowEaseIn,
        margin: EdgeInsets.only(left: 16, right: 16, top: 8),
        child: child);
  }

  Widget _iconEdit() {
    return Icon(
      Icons.edit,
      size: 20,
      color: SiswamediaTheme.greyIcon,
    );
  }

  Widget _divider() {
    return Container(
      height: 10,
      width: double.infinity,
      color: SiswamediaTheme.greyDivider,
    );
  }

  bool openDetail(bool isExpand) {
    return !isExpand;
  }

  Widget title(String title) {
    return vText(title, fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500);
  }
}
