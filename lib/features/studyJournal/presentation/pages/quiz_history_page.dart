part of '_pages.dart';

class QuizHistoryPage extends StatefulWidget {
  final Client client;

  QuizHistoryPage({this.client});

  @override
  _QuizHistoryPageState createState() => _QuizHistoryPageState();
}

class _QuizHistoryPageState extends BaseStateful<QuizHistoryPage> {
  @override
  void init() {}

  @override
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: SimpleText(
        'Ujian Yang Sudah Dibuat',
        style: SiswamediaTextStyle.title,
        fontSize: 18,
      ),
      leading: IconButton(
          key: Key('leading'),
          icon: Icon(Icons.arrow_back),
          color: SiswamediaTheme.green,
          onPressed: () => context.pop()),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    var auth = GlobalState.auth().state;
    return FutureBuilder<QuizListModel>(
        key: Key('future'),
        future: QuizServices.getQuiz(
            mockClient: widget.client,
            classId: auth.currentState.classId,
            isJournal: true,
            isOrtu: false),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.data.isNotEmpty) {
            return Container(
              key: Key('container'),
              margin: EdgeInsets.symmetric(vertical: 15),
              child: ListView(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                padding: EdgeInsets.symmetric(horizontal: 20),
                // crossAxisSpacing: 10,
                // mainAxisSpacing: 10,
                // crossAxisCount: 1,
                // childAspectRatio: 2.5,
                children: snapshot.data.data.map(_buildCardQuiz).toList(),
              ),
            );
          } else {
            if (ConnectionState.waiting == snapshot.connectionState) {
              return Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(SiswamediaTheme.green)));
            }
          }
          return Center(
            key: Key('empty quiz history'),
            child: Padding(
                padding: EdgeInsets.only(top: 150),
                child: Column(
                  children: [
                    Image.asset('assets/no_quiz.png', height: 160, width: 160),
                    Text('Belum ada quiz'),
                  ],
                )),
          );
        });
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  Widget _buildCardQuiz(QuizListDetail data) {
    // print('mantap');
    return CardQuizHistory(
      data: data,
      onTap: (id) {},
    );
  }
}
