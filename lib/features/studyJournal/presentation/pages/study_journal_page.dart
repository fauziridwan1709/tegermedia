part of '_pages.dart';

class StudyJournalPage extends StatefulWidget {
  @override
  _StudyJournalPageState createState() => _StudyJournalPageState();
}

class _StudyJournalPageState extends State<StudyJournalPage>
    with SingleTickerProviderStateMixin {
  final journeyState = GlobalState.journey();
  final authState = GlobalState.auth().state;
  final course = GlobalState.course();
  TabController tabController;

  Future<void> retrieveData() async {
    var dataCourse = CourseByClassModel(
        statusCode: 200,
        param: GetCourseModel(classId: authState.currentState.classId));

    await course.setState((s) => s.retrieveData(data: dataCourse),
        onData: (context, data) async {
      await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
        await SharedPreferences.getInstance().then((value) async {
          journeyState.state.courseIdSelected =
              data.courseMap.isNotEmpty ? data.courseMap.values?.first?.id : 0;
          await journeyState.setState((s) async => await s.retrieveData(0),
              catchError: true, onError: (context, dynamic error) {
            print((error as SiswamediaException).message);
          }, onData: (_, data) {
            // journeyState.setState((s) => s.setCount(0));
          });
        });
      });
    });
  }

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    tabController.addListener(() {
      print(tabController.index.toString());
      // setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFF9F9F9),
        appBar: AppBar(
          elevation: 0.0,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: SiswamediaTheme.primary,
            ),
          ),
          actions: [
            if (!GlobalState.auth()
                .state
                .currentState
                .classRole
                .isSiswaOrOrtu) ...[
              IconButton(
                icon: Icon(Icons.add_circle),
                color: SiswamediaTheme.green,
                onPressed: () => context.push<void>(CreateStudyJournalPage(
                    onRefresh: retrieveData,
                    classId: GlobalState.auth().state.currentState.classId)),
              )
            ],
            if (tabController.index == 1) ...[
              IconButton(
                icon: Icon(Icons.filter_list),
                color: SiswamediaTheme.green,
                onPressed: () => navigate(context, FilterJourneyScreen()),
              )
            ]
          ],
          centerTitle: true,
          title: vText('Jurnal Belajar'),
          bottom: TabBar(
            controller: tabController,
            indicatorColor: SiswamediaTheme.primary,
            labelColor: SiswamediaTheme.primary,
            labelStyle: GoogleFonts.sourceSansPro(
                fontSize: 14, fontWeight: FontWeight.w600),
            unselectedLabelColor:
                Theme.of(context).dividerColor.withOpacity(0.54),
            tabs: [Tab(text: 'Berlangsung'), Tab(text: 'Selanjutnya')],
          ),
        ),
        body: TabBarView(
          controller: tabController,
          children: [ItemCourse(today: true), ItemCourse(today: false)],
        ));
  }
}

class ItemCourse extends StatefulWidget {
  final bool today;

  const ItemCourse({Key key, this.today}) : super(key: key);

  @override
  _ItemCourseState createState() => _ItemCourseState();
}

class _ItemCourseState extends State<ItemCourse> {
  final journey = GlobalState.journey();
  final authState = GlobalState.auth().state;
  final course = GlobalState.course();

  var listGuru = <ListGuru>[];
  Completer<Null> _completer;

  InputDecoration get __inputDecoration => InputDecoration(
        hintText: 'Pilih mata pelajaran',
        hintStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.w600),
        fillColor: Colors.white,
        filled: true,
        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: SiswamediaTheme.transparent)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: SiswamediaTheme.transparent)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: SiswamediaTheme.transparent)),
      );

  DropdownMenuItem<int> __buildCourseDropdown(int index) {
    var isGuru = authState.currentState.classRole.isGuru;
    var dataCourse = course.state.courseMap.values
        .where((e) => isGuru ? e.role.isGuru : true)
        .elementAt(index);
    return DropdownMenuItem(
        value: dataCourse.id,
        child: Text(
          dataCourse.name,
          style: TextStyle(
              fontSize: 14, fontWeight: FontWeight.w600, color: Colors.black),
        ));
  }

  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  ScrollController _scrollController;
  bool isLoading = false;
  int i = 1;

  @override
  void initState() {
    super.initState();
    _completer = Completer<Null>();
    _scrollController = ScrollController();
    Initializer(
      rIndicator: refreshIndicatorKey,
      cacheKey: journey.state.cacheKey,
      reactiveModel: journey,
      state: widget.today
          ? journey.state.getCondition()
          : journey.state.getIncomingCondition(),
    ).initialize();
    _scrollController.addListener(_onScroll);
  }

  Future<void> _onScroll() async {
    if (_isBottom &&  !_completer.isCompleted) {
      _completer.complete();
      if (widget.today) {
        await journey.state.loadMore().then((_) {
          journey.notify();
          _completer = Completer<Null>();
        });
      } else {
        await journey.state.loadMoreIncoming().then((_) {
          journey.notify();
          _completer = Completer<Null>();
        });
      }
    }
  }

  bool get _isBottom {
    if (widget.today
        ? !journey.state.hasReachedMax
        : !journey.state.incomingHasReachedMax) {
      if (!_scrollController.hasClients) {
        return false;
      }
      final maxScroll = _scrollController.position.maxScrollExtent;
      final currentScroll = _scrollController.offset;
      return currentScroll >= (maxScroll * 0.9);
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    journey.state.page = 1;
    journey.state.pageIncoming = 1;
    super.dispose();
  }

  Future<void> retrieveData() async {
    var dataCourse = CourseByClassModel(
        statusCode: 200,
        param: GetCourseModel(classId: authState.currentState.classId));

    await course.setState((s) => s.retrieveData(data: dataCourse),
        onData: (context, data) async {
      var isGuru = authState.currentState.classRole.isGuru;
      if (journey.state.courseIdSelected == null && data.courseMap.isNotEmpty) {
        journey.state.courseIdSelected = data.courseMap.values
            .where((e) => isGuru ? e.role.isGuru : true)
            .first
            ?.id;
      }

      // if (widget.today) {
      await journey.setState((s) async => await s.retrieveData(0));
      // } else {
      await journey.setState((s) async => await s.retrieveDataIncoming());
      // }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          color: SiswamediaTheme.white,
          child: StateBuilder(
            observe: () => course,
            builder: (context, snapshot) {
              var isGuru = authState.currentState.classRole.isGuru;
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 185,
                    height: 35,
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.shade300,
                              blurRadius: 1,
                              spreadRadius: 1)
                        ]),
                    child: DropdownButtonFormField<int>(
                        value: journey.state.courseIdSelected ?? 0,
                        onChanged: (val) async {
                          i = 1;
                          journey.state.courseJourney.clear();
                          await journey.setState((s) => s.changeCourseId(val));
                          await journey.setState((s) => s.retrieveData(i));
                          var selectedCourse = course.state.courseMap.values
                              .firstWhere((element) => element.id == val);
                          listGuru = selectedCourse.listGuru;
                        },
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w600),
                        decoration: __inputDecoration,
                        hint: SimpleText(
                          'Pilih mata pelajaran',
                          style: SiswamediaTextStyle.description,
                          color: SiswamediaTheme.border,
                          fontSize: 7,
                        ),
                        items: List.generate(
                            course.state.courseMap.values
                                    .where((e) => isGuru ? e.role.isGuru : true)
                                    .length ??
                                0,
                            __buildCourseDropdown)),
                  ),
                  AutoLayoutButton(
                    onTap: () async {
                      await refreshIndicatorKey.currentState.show();
                    },
                    backGroundColor: SiswamediaTheme.green,
                    padding: EdgeInsets.symmetric(horizontal: 6, vertical: 4),
                    text: 'Tampilkan',
                    fontSize: 12,
                    textColor: SiswamediaTheme.white,
                    radius: BorderRadius.circular(4),
                  )
                ],
              );
            },
          ),
        ),
        Expanded(
          child: StateBuilder<JourneyState>(
              observe: () => journey,
              builder: (context, snapshot) {
                return RefreshIndicator(
                  key: refreshIndicatorKey,
                  onRefresh: () async {
                    await retrieveData();
                  },
                  child: WhenRebuilder<JourneyState>(
                    observe: () => journey,
                    onWaiting: () => WaitingView(),
                    onError: (dynamic error) => ErrorView(error: error),
                    onIdle: () => WaitingView(),
                    onData: (data) {
                      var listData = (widget.today
                          ? data.courseJourney
                          : data.incomingCourseJourney);
                      if (listData.isEmpty) {
                        return ListView(
                          physics: AlwaysScrollableScrollPhysics(),
                          controller: _scrollController,
                          shrinkWrap: true,
                          children: [
                            if (MediaQuery.of(context).orientation.index == 0)
                              SizedBox(
                                height: S.w * .3,
                              ),
                            Image.asset(
                                'assets/icons/course/data_not_found.png',
                                height: S.w / 3,
                                width: S.w / 3),
                            Center(
                              child: vText('Masih Kosong',
                                  fontWeight: FontWeight.bold,
                                  color: SiswamediaTheme.greybgButton),
                            )
                          ],
                        );
                      }
                      return ListView.builder(
                        physics: AlwaysScrollableScrollPhysics(),
                        padding:
                            EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                        controller: _scrollController,
                        shrinkWrap: true,
                        itemCount: (widget.today
                                ? data.hasReachedMax
                                : data.incomingHasReachedMax)
                            ? listData.length
                            : listData.length + 1,
                        itemBuilder: (_, index) {
                          if (index == listData.length) {
                            return Center(
                                child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(),
                            ));
                          }
                          return _itemList(listData[index]);
                        },
                      );
                    },
                  ),
                );
              }),
        ),
      ],
    );
  }

  Widget _itemList(CourseJourneyData data) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 2),
      child: InkWell(
        onTap: () {
          context.push<void>(DetailStudyJournalPage(data: data));
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            // border: Border.all(color: SiswamediaTheme.borderGrey, width: 1),
            boxShadow: [
              BoxShadow(
                  color: SiswamediaTheme.grey.withOpacity(0.15),
                  offset: const Offset(0.2, 0.2),
                  blurRadius: 8.0,
                  spreadRadius: 1),
            ],
          ),
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          margin: EdgeInsets.only(top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // if (authState.currentState.classRole == 'SISWA')
              //   vText(data.name,
              //       color: SiswamediaTheme.primary,
              //       fontWeight: FontWeight.bold,
              //       fontSize: 16),
              SizedBox(
                height: 6,
              ),
              Text(data.name,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      color: authState.currentState.classRole == 'SISWA'
                          ? SiswamediaTheme.green
                          : Colors.black)),
              HeightSpace(4),
              Row(
                children: [
                  Text(data.guide,
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFFAAAAAA))),
                  //todo delete study jurnal
                  // InkWell(
                  //   onTap: () async {
                  //     try {
                  //       var url =
                  //           '$baseUrl/$version/study-journal/${data.journalId}';
                  //       var ref = await SharedPreferences.getInstance();
                  //       var resp = await client.delete(url,
                  //           headers: await Pref.getSiswamediaHeader());
                  //       resp.statusCode.translate<void>(ifSuccess: () {
                  //         CustomFlushBar.successFlushBar('Berhasil', context);
                  //       }, ifElse: () {
                  //         Utils.logWithDotes(resp.body,
                  //             info: 'delete body data');
                  //       });
                  //     } catch (e) {
                  //       print(e);
                  //     }
                  //   },
                  //   child: Icon(Icons.restore_from_trash_outlined),
                  // )
                ],
              ),
              SizedBox(height: 8),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Text(authState.currentState.className,
                            style: GoogleFonts.openSans(
                                fontWeight: FontWeight.w600,
                                fontSize: 12,
                                color: Color(0xFF777777))),
                        WidthSpace(14),
                        Row(
                          children: [
                            Icon(
                              Icons.calendar_today_outlined,
                              size: 12,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Container(
                              child: Text(
                                  DateFormat('dd MMM yyyy')
                                      .format(DateTime.parse(data.startTime)
                                          .toLocal())
                                      .toString(),
                                  style: GoogleFonts.openSans(
                                      color: Color(0xFF4B4B4B), fontSize: 12),
                                  overflow: TextOverflow.ellipsis),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 14,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.access_time_outlined,
                              size: 12,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Container(
                              child: Text(
                                  DateFormat('HH:mm')
                                      .format(DateTime.parse(data.startTime)
                                          .toLocal())
                                      .toString(),
                                  style: GoogleFonts.openSans(
                                      color: Color(0xFF4B4B4B), fontSize: 12),
                                  overflow: TextOverflow.ellipsis),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(vertical: 4, horizontal: 24),
                  //   child: vText('Lihat', color: Colors.white, fontSize: 17),
                  //   decoration: BoxDecoration(
                  //       color: SiswamediaTheme.primary,
                  //       borderRadius: BorderRadius.circular(5)),
                  // )
                ],
              ),
              // if (DateTime.parse(data.startTime).day != DateTime.now().day &&
              //     DateTime.parse(data.startTime).month !=
              //         DateTime.now().month &&
              //     DateTime.parse(data.startTime).year != DateTime.now())
              //   vText("Sudah Selesai", color: SiswamediaTheme.primary)
            ],
          ),
        ),
      ),
    );
  }
}

class _ControlsOverlay extends StatelessWidget {
  const _ControlsOverlay({@required this.controller});

  static const _examplePlaybackRates = [
    0.25,
    0.5,
    1.0,
    1.5,
    2.0,
    3.0,
    5.0,
    10.0,
  ];

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
        Align(
          alignment: Alignment.topRight,
          child: PopupMenuButton<double>(
            initialValue: controller.value.playbackSpeed,
            tooltip: 'Playback speed',
            onSelected: (speed) {
              controller.setPlaybackSpeed(speed);
            },
            itemBuilder: (context) {
              return [
                for (final speed in _examplePlaybackRates)
                  PopupMenuItem(
                    value: speed,
                    child: Text('${speed}x'),
                  )
              ];
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                // Using less vertical padding as the text is also longer
                // horizontally, so it feels like it would need more spacing
                // horizontally (matching the aspect ratio of the video).
                vertical: 12,
                horizontal: 16,
              ),
              child: Text('${controller.value.playbackSpeed}x'),
            ),
          ),
        ),
      ],
    );
  }
}
