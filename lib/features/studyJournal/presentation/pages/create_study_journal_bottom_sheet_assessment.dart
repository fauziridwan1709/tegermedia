part of '_pages.dart';

enum JournalStepAssessment {
  init,
  myAssignment,
  otherAssignment,
  chooseDate,
}

class CreateStudyJournalBottomSheetAssessment extends StatefulWidget {
  final int classId;

  CreateStudyJournalBottomSheetAssessment({this.classId});

  @override
  _CreateStudyJournalBottomSheetAssessmentState createState() =>
      _CreateStudyJournalBottomSheetAssessmentState();
}

class _CreateStudyJournalBottomSheetAssessmentState
    extends State<CreateStudyJournalBottomSheetAssessment> {
  final auth = GlobalState.auth();
  final dummy = GlobalState.dummy();

  String _title;
  JournalStepAssessment _step;
  Map<int, String> _listAssessment;

  @override
  void initState() {
    super.initState();
    _title = 'Pilih Tugas';
    _step = JournalStepAssessment.init;
    _listAssessment = <int, String>{};
  }

  @override
  Widget build(BuildContext context) {
    return _buildMain(
      child: __mapEvent(_step),
    );
  }

  Widget _buildMain({Widget child}) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12), topRight: Radius.circular(12)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          AppBar(
            elevation: 0,
            centerTitle: true,
            title: SimpleText(
              _title,
              style: SiswamediaTextStyle.subtitle,
              fontSize: 18,
            ),
            leading: InkWell(
              onTap: () => context.pop(),
              child: Icon(Icons.clear, color: SiswamediaTheme.green),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            child: child,
          ),
        ],
      ),
    );
  }

  Widget __mapEvent(JournalStepAssessment _step) {
    switch (_step) {
      case JournalStepAssessment.myAssignment:
        return __buildMyAssignment();
        break;
      default:
        return __buildInit();
        break;
    }
  }

  Widget __buildInit() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        HeightSpace(10),
        SelectorWidget(
            color: Colors.black54,
            label: 'Dari List Tugas Saya',
            onTap: (_) {
              setState(() {
                _title = 'List Tugas Saya';
                _step = JournalStepAssessment.myAssignment;
              });
            }),
        HeightSpace(10),
        // SelectorWidget(
        //     color: Colors.black54,
        //     label: 'Dari List Tugas Guru Lain',
        //     onTap: (_) {}),
        HeightSpace(60),
      ],
    );
  }

  Widget __buildMyAssignment() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SimpleText(
          'Pilih Salah Satu',
          style: SiswamediaTextStyle.subtitle,
          fontSize: 12,
        ),
        HeightSpace(10),
        FutureBuilder<ModelAssignmentsNew>(
          future: AssignmentServices.getAssignment(
              param:
                  'school_id=${auth.state.currentState?.schoolId ?? 9}&class_id=${widget.classId}',
              role: 'GURU',
              page: 1,
              course: [dummy.state.createModel.courseId],
              limit: 100),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return SizedBox(
                  height: 160,
                  child: Center(child: CircularProgressIndicator()));
            }
            var data = snapshot.data.data.listData;
            return Column(
              children: [
                SizedBox(
                  height: 160,
                  child: data.length == 0
                      ? SimpleText('Tidak ada tugas tersedia',
                          textAlign: TextAlign.left, fontSize: 12)
                      : ListView(
                          children: List.generate(
                              data.length,
                              (index) => CheckboxListTile(
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (bool value) {
                                      setState(() {
                                        if (_listAssessment
                                            .containsKey(data[index].id)) {
                                          _listAssessment
                                              .remove(data[index].id);
                                        } else {
                                          _listAssessment.putIfAbsent(
                                              data[index].id,
                                              () => data[index].name);
                                        }
                                      });
                                    },
                                    value: _listAssessment
                                        .containsKey(data[index].id),
                                    contentPadding: EdgeInsets.all(0),
                                    title: SimpleText(
                                      data[index].name,
                                      fontSize: 12,
                                    ),
                                  )),
                        ),
                ),
                AutoLayoutButton(
                  backGroundColor: SiswamediaTheme.green,
                  textColor: SiswamediaTheme.white,
                  text: 'Pilih',
                  onTap: _listAssessment.isNotEmpty
                      ? () {
                          // setState(() {
                          //   _title = 'Tentukkan Waktu Pengumpulan';
                          //   _step = Step.chooseDate;
                          // });
                          dummy.setState(
                              (s) => s.changeAssignment(_listAssessment));
                          context.pop();
                        }
                      : null,
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 4),
                  radius: BorderRadius.circular(8),
                )
              ],
            );
          },
        ),
        HeightSpace(40)
      ],
    );
  }

  Widget __buildOtherAssignment() {
    return Container();
  }

  Widget __buildChooseDate() {
    return Container();
    // return Column(
    //   children: [
    //     InkWell(
    //         onTap: () async {
    //           var data = await showDatePicker(
    //               context: context,
    //               initialDate: DateTime.now().toLocal(),
    //               firstDate: DateTime.now().toLocal(),
    //               lastDate: DateTime.now().toLocal().add(Duration(days: 365)));
    //           dummy.setState((s) => s.changeDayMonthYear(data));
    //           Utils.logWithDotes(data.toUtc().toIso8601String(),
    //               info: 'mantap');
    //         },
    //         child: Column(
    //           children: [
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               children: [
    //                 SimpleText('Tanggal Mulai'),
    //                 SimpleText(dummyState.dayMonthYear,
    //                     style: SiswamediaTextStyle.subtitle),
    //               ],
    //             ),
    //             Divider()
    //           ],
    //         )),
    //     HeightSpace(12),
    //     InkWell(
    //         onTap: () async {
    //           var data = await showTimePicker(
    //             context: context,
    //             initialTime: TimeOfDay.now(),
    //           );
    //           dummy.setState((s) => s.changeHourMinute(data));
    //           Utils.logWithDotes(data, info: 'mantap');
    //         },
    //         child: Column(
    //           children: [
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               children: [
    //                 SimpleText('Waktu Mulai'),
    //                 SimpleText(
    //                   dummyState.hourMinute,
    //                   style: SiswamediaTextStyle.subtitle,
    //                 ),
    //               ],
    //             ),
    //             Divider()
    //           ],
    //         )),
    //     HeightSpace(30)
    //   ],
    // );
  }
}
