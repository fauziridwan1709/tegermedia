part of '_pages.dart';

class FilterJourneyScreen extends StatefulWidget {
  @override
  _FilterJourneyScreenState createState() => _FilterJourneyScreenState();
}

class _FilterJourneyScreenState extends State<FilterJourneyScreen> {
  var filterState = Injector.getAsReactive<FilterAssignmentState>();
  final journey = GlobalState.journey();
  final course = GlobalState.course();

  var listGuru = <ListGuru>[];

  InputDecoration get __inputDecoration => InputDecoration(
        hintStyle: TextStyle(fontSize: 12),
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: SiswamediaTheme.border)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: SiswamediaTheme.border)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
            borderSide: BorderSide(color: SiswamediaTheme.border)),
      );

  DropdownMenuItem<int> __buildCourseDropdown(int index) {
    var dataCourse = course.state.courseMap.values.elementAt(index);
    return DropdownMenuItem(
        value: dataCourse.id, child: SimpleText(dataCourse.name, fontSize: 12));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SiswamediaAppBar(
        context: context,
        title: 'Filter',
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            title('Pilih Kelas Dan Mata Pelajaran'),
            SizedBox(
              height: 20,
            ),
            StateBuilder(
              observe: () => course,
              builder: (context, snapshot) {
                return Content(
                  'Mata Pelajaran',
                  child: DropdownButtonFormField<int>(
                      value: journey.state.courseIdSelected,
                      onChanged: (val) {
                        journey.setState((s) => s.retrieveData(val));
                        var selectedCourse = course.state.courseMap.values
                            .firstWhere((element) => element.id == val);
                        listGuru = selectedCourse.listGuru;
                      },
                      decoration: __inputDecoration,
                      hint: SimpleText(
                        '- Pilih mata pelajaran -',
                        style: SiswamediaTextStyle.description,
                        color: SiswamediaTheme.border,
                        fontSize: 12,
                      ),
                      items: List.generate(course.state.courseMap.length,
                          __buildCourseDropdown)),
                );
              },
            ),
            StateBuilder(
              observe: () => course,
              builder: (context, snapshot) {
                return Content(
                  'Mata Pelajaran',
                  child: DropdownButtonFormField<int>(
                      value: journey.state.courseIdSelected,
                      onChanged: (val) {
                        journey.setState((s) => s.retrieveData(val));
                        var selectedCourse = course.state.courseMap.values
                            .firstWhere((element) => element.id == val);
                        listGuru = selectedCourse.listGuru;
                      },
                      decoration: __inputDecoration,
                      hint: SimpleText(
                        '- Pilih mata pelajaran -',
                        style: SiswamediaTextStyle.description,
                        color: SiswamediaTheme.border,
                        fontSize: 12,
                      ),
                      items: List.generate(course.state.courseMap.length,
                          __buildCourseDropdown)),
                );
              },
            ),
            SizedBox(
              height: 20,
            ),
            title('Urutkan Dari'),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget title(String title) {
    return vText(title,
        fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold);
  }
}
