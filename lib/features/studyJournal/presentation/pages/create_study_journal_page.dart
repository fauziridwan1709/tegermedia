part of '_pages.dart';

class CreateStudyJournalPage extends StatefulWidget {
  final int classId;
  final VoidCallback onRefresh;
  final FilePicker mockFilePicker;

  const CreateStudyJournalPage({this.onRefresh, this.classId, this.mockFilePicker});

  @override
  _CreateStudyJournalPageState createState() => _CreateStudyJournalPageState();
}

class _CreateStudyJournalPageState
    extends BaseState<CreateStudyJournalPage, CreateStudyJournalState>
    with SingleTickerProviderStateMixin {
  final auth = GlobalState.auth();
  final authState = GlobalState.auth().state;
  final dummy = GlobalState.dummy();
  final dummyState = GlobalState.dummy().state;
  final course = GlobalState.course();
  final teacher = GlobalState.teachers();
  final progressUploadState = Injector.getAsReactive<ProgressUploadState>();

  var listTitle = <String>['Awal', 'Topik', 'Tugas', 'Ujian'];
  var listGuru = <ListGuru>[];
  var listLink = <Attachment>[];
  var textEditingControllers = <String, TextEditingController>{};

  double _progressValue = 0;
  int _progressPercentValue = 0;

  @override
  final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  TabController _tabController;
  TextEditingController _titleController;
  TextEditingController _descController;

  InputDecoration __inputDecoration2(String hint) => InputDecoration(
        hintStyle: TextStyle(fontSize: 12),
        hintText: hint,
        fillColor: SiswamediaTheme.greyBg,
        filled: true,
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: SiswamediaTheme.transparent)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: SiswamediaTheme.transparent)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: BorderSide(color: SiswamediaTheme.transparent)),
      );

  @override
  void init() {
    _tabController = TabController(vsync: this, length: 4, initialIndex: 0);
    _titleController = TextEditingController();
    _descController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    dummy.refresh();
  }

  @override
  Future<void> retrieveData() async {
    var dataCourse =
        CourseByClassModel(statusCode: 200, param: GetCourseModel(classId: widget.classId));
    await course.setState((s) => s.retrieveData(data: dataCourse));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    var tabs = List.generate(
        4,
        (index) => GestureTab(
              text: listTitle.elementAt(index),
            ));

    var tabBar = TabBar(
        controller: _tabController,
        indicatorColor: SiswamediaTheme.green,
        indicatorWeight: 2,
        labelColor: SiswamediaTheme.green,
        unselectedLabelColor: SiswamediaTheme.border,
        isScrollable: false,
        tabs: tabs);

    return AppBar(
      leading: IconButton(
          key: Key('CreateStudyBackButton'),
          icon: Icon(Icons.arrow_back),
          color: SiswamediaTheme.green,
          onPressed: () => onBackPressed()),
      centerTitle: true,
      elevation: 1,
      title: SimpleText(
        'Buat Kursus',
        style: SiswamediaTextStyle.title,
        fontSize: 18,
      ),
      bottom: PreferredSize(
        preferredSize: tabBar.preferredSize,
        child: IgnorePointer(
          child: tabBar,
        ),
      ),
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, ReactiveModel<CreateStudyJournalState> snapshot,
      SizingInformation sizeInfo) {
    var size = sizeInfo.screenSize;
    return RefreshIndicator(
      key: refreshIndicatorKey,
      onRefresh: retrieveData,
      child: Stack(
        children: [
          TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: [
              _buildAwal(context, size),
              _buildTopik(context, size),
              _buildTugas(context, size),
              _buildUjian(context, size),
            ],
          ),
          BottomStack(
              height: 80,
              child: AutoLayoutButton(
                key: Key('AutoLayoutButtonLanjutkan'),
                backGroundColor: SiswamediaTheme.green,
                radius: BorderRadius.circular(6),
                onTap: handler(context),
                text: dummyState.step == 3 ? 'Selesai' : 'Lanjutkan',
                textColor: SiswamediaTheme.white,
                padding: EdgeInsets.symmetric(horizontal: 45, vertical: 10),
              ))
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, ReactiveModel<CreateStudyJournalState> snapshot,
      SizingInformation sizeInfo) {
    // TODO: implement buildWideLayout
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    if (_tabController.index != 0) {
      _tabController.animateTo(_tabController.index - 1);
      await dummy.setState((s) => s.prev());
      return false;
    } else {
      await dummy.refresh();
      return true;
    }
  }

  Widget _buildAwal(BuildContext context, Size size) {
    var courses = course.state.courseMap;
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      children: [
        Content(
          'Kelas',
          child: Row(
            children: [
              Expanded(
                child: CustomContainer(
                  radius: BorderRadius.circular(4),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                  color: SiswamediaTheme.border,
                  child: SimpleText(authState?.currentState?.className ?? 'kelas 10 IPA'),
                ),
              ),
            ],
          ),
        ),
        StateBuilder(
          observe: () => course,
          builder: (context, snapshot) {
            return Content(
              'Mata Pelajaran',
              child: DropdownButtonFormField<int>(
                  value: dummyState.createModel.courseId,
                  onChanged: (val) {
                    dummy.setState((s) => s.changeCourseId(val));
                    var selectedCourse =
                        course.state.courseMap.values.firstWhere((element) => element.id == val);
                    listGuru = selectedCourse.listGuru;
                  },
                  decoration: inputDecoration,
                  // onTap: () {
                  //   print('courses length');
                  //   print(courses.length);
                  //   if (courses.isEmpty)
                  //     CustomFlushBar.errorFlushBar(
                  //         'Belum ada Mata pelajaran', context);
                  // },
                  hint: SimpleText(
                    courses.isEmpty ? '- Tidak ada mata pelajaran -' : '- Pilih mata pelajaran -',
                    style: SiswamediaTextStyle.description,
                    color: SiswamediaTheme.border,
                    fontSize: 12,
                  ),
                  items: List.generate(course.state.courseMap.length, __buildCourseDropdown)),
            );
          },
        ),
        if (dummyState.createModel.courseId != null)
          Content(
            'Pilih Guru',
            child: listGuru.isEmpty
                ? SimpleText('Tidak ada guru')
                : DropdownButtonFormField<int>(
                    value: dummyState.createModel.teacherUserId,
                    onChanged: (val) {
                      dummy.setState((s) => s.changeTeacherId(val));
                    },
                    decoration: inputDecoration,
                    hint: SimpleText(
                      '- Pilih guru -',
                      style: SiswamediaTextStyle.description,
                      color: SiswamediaTheme.border,
                      fontSize: 12,
                    ),
                    items: List.generate(listGuru.length, __buildTeacherDropdown)),
          ),
        Content(
          'Waktu Pelaksanaan',
          child: Column(
            children: [
              InkWell(
                  key: Key('CreateStudyJournalShowDatePicker'),
                  onTap: () async {
                    var data = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now().toLocal(),
                        firstDate: DateTime.now().toLocal(),
                        lastDate: DateTime.now().toLocal().add(Duration(days: 365)));
                    await dummy.setState((s) => s.changeDayMonthYear(data));
                    Utils.logWithDotes(data.toUtc().toIso8601String(), info: 'mantap');
                  },
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SimpleText('Tanggal Mulai'),
                          SimpleText(dummyState.dayMonthYear, style: SiswamediaTextStyle.subtitle),
                        ],
                      ),
                      Divider()
                    ],
                  )),
              HeightSpace(12),
              InkWell(
                  onTap: () async {
                    var data = await showTimePicker(
                      context: context,
                      initialTime: TimeOfDay.now(),
                    );
                    await dummy.setState((s) => s.changeHourMinute(data));
                    Utils.logWithDotes(data, info: 'mantap');
                  },
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SimpleText('Waktu Mulai'),
                          SimpleText(
                            dummyState.hourMinute,
                            style: SiswamediaTextStyle.subtitle,
                          ),
                        ],
                      ),
                      Divider()
                    ],
                  )),
            ],
          ),
        ),
        HeightSpace(80),
      ],
    );
    // return Stack(
    //   children: [
    //
    //     Container(
    //       decoration:
    //           BoxDecoration(boxShadow: [SiswamediaTheme.shadowContainer]),
    //       child: SimpleContainer(),
    //     )
    //   ],
    // );
  }

  Widget _buildTopik(BuildContext context, Size size) {
    return ListView(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
          child: Column(
            children: <Widget>[
              Content(
                'Judul Topik',
                child: TextField(
                  style: TextStyle(fontSize: 12),
                  onChanged: (val) => dummy.setState((s) => s.changeTitle(val)),
                  controller: _titleController,
                  decoration: __inputDecoration2('title'),
                ),
              ),
              Content(
                'Deskripsi Pembahasan',
                child: TextField(
                    style: TextStyle(fontSize: 12),
                    controller: _descController,
                    onChanged: (val) => dummy.setState((s) => s.changeDescription(val)),
                    minLines: 2,
                    maxLines: 6,
                    decoration: __inputDecoration2('Deskripsi')),
              )
            ],
          ),
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          child: Content('Video Pembahasan',
              child: dummyState.createModel.videoUrl.isEmpty
                  ? AutoLayoutButton(
                      onTap: () {
                        uploadFile(_setUploadProgress, context);
                      },
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
                      radius: BorderRadius.circular(6),
                      fontSize: 12,
                      text: 'Upload File',
                      textColor: SiswamediaTheme.white,
                      backGroundColor: SiswamediaTheme.green,
                    )
                  : InkWell(
                      onTap: () async => await launch(dummyState.createModel.videoUrl),
                      child: SimpleText(
                        dummyState.createModel.videoUrl,
                        style: SiswamediaTextStyle.subtitle,
                        color: SiswamediaTheme.green,
                        fontSize: 12,
                      ),
                    )),
        ),
        if (progressUploadState.state.progress != 0)
          Container(
              width: S.w,
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 3),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey[300]),
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: SiswamediaTheme.grey.withOpacity(0.2),
                        offset: const Offset(0.2, 0.2),
                        blurRadius: 3.0),
                  ],
                  color: Colors.white),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text('Uploading... $_progressPercentValue%'),
                SizedBox(height: 5),
                LinearPercentIndicator(
                  width: S.w - 35 * 2,
                  lineHeight: 8.0,
                  percent: _progressValue,
                  backgroundColor: Colors.grey[300],
                  progressColor: SiswamediaTheme.green,
                ),
                SizedBox(height: 5),
                Container(
                  width: S.w * .8,
                  child: Text(
                    '${Format.formatByte(progressUploadState.state.progress)}/${Format.formatByte(progressUploadState.state.contentLength)}',
                    textAlign: TextAlign.right,
                  ),
                ),
              ])),
        Divider(),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          child: Content(
            'Link File Pendukung',
            child: Column(
              children: <Widget>[
                ...List.generate(
                    listLink.length,
                    (index) => CardLink(listLink[index],
                            mantap: textEditingControllers, index: index, onDelete: (index) {
                          setState(() {
                            listLink.removeAt(index);
                            dummy.setState((s) => s.removeListAttachment(index));
                            textEditingControllers.remove('name$index');
                            textEditingControllers.remove('desc$index');
                          });
                        }, onChangedName: (s, key) {
                          // textEditingControllers[key].text = s;
                        }, onChangedDesc: (s, key) {
                          // textEditingControllers[key].text = s;
                        })),
                CustomContainer(
                  onTap: () {
                    setState(() {
                      var attach = AttachmentModel(name: 'name', link: 'http');
                      listLink.add(attach);
                      dummy.setState((s) => s.addListAttachment(attach));
                      textEditingControllers.putIfAbsent(
                          'name${listLink.length - 1}', () => TextEditingController(text: ''));
                      textEditingControllers.putIfAbsent(
                          'desc${listLink.length - 1}', () => TextEditingController(text: ''));
                    });
                  },
                  border: Border.all(color: SiswamediaTheme.green, width: 1.5),
                  radius: BorderRadius.circular(6),
                  color: SiswamediaTheme.transparent,
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/png/plus.png',
                        height: 15,
                        width: 15,
                      ),
                      WidthSpace(10),
                      SimpleText('Tambahkan File / Link',
                          fontSize: 12,
                          style: SiswamediaTextStyle.subtitle,
                          color: SiswamediaTheme.green)
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        HeightSpace(80),
      ],
    );
  }

  Widget _buildTugas(BuildContext context, Size size) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      children: [
        CustomContainer(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
          boxShadow: [SiswamediaTheme.shadowContainer],
          radius: BorderRadius.circular(8),
          color: SiswamediaTheme.white,
          child: CheckboxListTile(
            controlAffinity: ListTileControlAffinity.leading,
            value: dummyState.skipAssignment,
            onChanged: (val) => dummy.setState((s) => s.changeSkipAssignment()),
            title: Content(
              'Lewati Pemberian tugas langsung',
              child: SimpleText(
                '( Kamu bisa membuat tugas kapanpun setelah membuat topik kursus )',
                fontSize: 12,
              ),
            ),
          ),
        ),
        if (!dummyState.skipAssignment) ...[
          Content('Buat Tugas',
              child: Column(
                children: [
                  SelectorWidget(
                    label: 'Tambahkan Tugas',
                    onTap: _$_buildBottomSheetTugas,
                  ),
                  HeightSpace(20),
                  ...dummyState.titleAssessments.keys
                      .map((e) =>
                          CardSelectedAssessment(dummyState.titleAssessments[e], onDelete: (id) {
                            dummy.setState((s) => s.deleteAssessment(id));
                          }, id: e))
                      .toList()
                ],
              ))
        ]
      ],
    );
  }

  Widget _buildUjian(BuildContext context, Size size) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      children: [
        CustomContainer(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
          boxShadow: [SiswamediaTheme.shadowContainer],
          radius: BorderRadius.circular(8),
          color: SiswamediaTheme.white,
          child: CheckboxListTile(
            controlAffinity: ListTileControlAffinity.leading,
            value: dummyState.skipQuiz,
            onChanged: (val) => dummy.setState((s) => s.changeSkipQuiz()),
            title: Content(
              'Lewati pemberian ujian langsung',
              child: SimpleText(
                '( Kamu bisa membuat ujian kapanpun setelah membuat topik kursus )',
                fontSize: 12,
              ),
            ),
          ),
        ),
        if (!dummyState.skipQuiz)
          Content('Buat Latihan Soal / Ujian',
              child: Column(
                children: [
                  SelectorWidget(
                    label: 'Tambahkan Ujian',
                    onTap: _$_buildBottomSheetUjian,
                  ),
                  HeightSpace(20),
                  ...dummyState.titleQuizzes.keys
                      .map(
                          (e) => CardSelectedQuiz(dummyState.titleQuizzes[e], onDelete: (id, type) {
                                dummy.setState((s) => s.deleteQuiz(id, type));
                              }, id: e))
                      .toList()
                ],
              ))
      ],
    );
  }

  DropdownMenuItem<int> __buildCourseDropdown(int index) {
    var dataCourse = course.state.courseMap.values.elementAt(index);
    return DropdownMenuItem(value: dataCourse.id, child: SimpleText(dataCourse.name, fontSize: 12));
  }

  DropdownMenuItem<int> __buildTeacherDropdown(int index) {
    var dataGuru = listGuru[index];
    return DropdownMenuItem(value: dataGuru.userId, child: SimpleText(dataGuru.nama, fontSize: 12));
  }

  void _$_buildBottomSheetTugas(BuildContext context) {
    showModalBottomSheet<void>(
        context: context,
        backgroundColor: SiswamediaTheme.white,
        shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12))),
        builder: (BuildContext context) {
          return CreateStudyJournalBottomSheetAssessment(classId: widget.classId);
        });
  }

  void _$_buildBottomSheetUjian(BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (context) => Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
              child: Container(
                width: 200,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SimpleText(
                      'Pilih Metode',
                      style: SiswamediaTextStyle.title,
                      fontSize: 16,
                    ),
                    HeightSpace(10),
                    SimpleText(
                      'Pilih salah satu cara Kamu untuk memberikan soal latihan di kursus ini.',
                      style: SiswamediaTextStyle.description,
                      textAlign: TextAlign.center,
                      fontSize: 12,
                    ),
                    HeightSpace(25),
                    AutoLayoutButton(
                      key: Key('AutoLayoutButton'),
                      textColor: SiswamediaTheme.white,
                      backGroundColor: SiswamediaTheme.green,
                      radius: BorderRadius.circular(4),
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                      onTap: () {
                        context.pop();
                        context.push<void>(QuizHistoryPage(), name: '/history');
                      },
                      text: 'Ujian Yang Sudah Dibuat',
                    ),
                    HeightSpace(10),
                    AutoLayoutButton(
                      textColor: SiswamediaTheme.white,
                      backGroundColor: Color(0xFF60ACDF),
                      radius: BorderRadius.circular(4),
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                      onTap: () {
                        context.pop();
                        context.push<void>(
                            BankSoal(type: 'journal', courseId: dummyState.createModel.courseId),
                            name: '/journal');
                      },
                      text: 'Pilih Soal dari Bank Soal',
                    ),
                    HeightSpace(20)
                  ],
                ),
              ),
            ));
  }

  VoidCallback handler(BuildContext context) {
    var data = dummyState;
    if (data.validateStepOne()) {
      return () {
        _tabController.animateTo(1);
        dummy.setState((s) => s.next());
      };
    } else if (data.validateStepTwo()) {
      return () {
        _tabController.animateTo(2);
        dummy.setState((s) => s.next());
      };
    } else if (data.validateStepThree()) {
      return () {
        _tabController.animateTo(3);
        dummy.setState((s) => s.next());
      };
    } else if (data.validateStepFour()) {
      return () async {
        var journey = GlobalState.journey();
        await dummy.setState((s) => s.createData(data.createModel, textEditingControllers, context),
            onData: (_, data) async {
          await journey.setState((s) => s.retrieveData(0));
        });
      };
    } else {
      return null;
    }
  }

  static HttpClient getHttpClient() {
    var httpClient = HttpClient()
      ..connectionTimeout = const Duration(seconds: 10)
      ..badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    return httpClient;
  }

  Future<void> uploadFile(OnUploadProgressCallback onUploadProgress, BuildContext context) async {
    try {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context,
          builder: (_) {
            return Center(child: CircularProgressIndicator());
          });
      var _picker = widget.mockFilePicker ?? FilePicker.platform;
      var pick = await _picker.pickFiles(
          allowMultiple: false, type: FileType.custom, allowedExtensions: ['mp4', 'pdf', 'doc']);
      print('aduh');
      var file = pick.files.first;
      print(file.name);
      print(file.path);

      var client2 = Client();
      //todo file!=null
      if (file != null) {
        var totalByteLength = File(file.path).lengthSync();
        var type = lookupMimeType(file.path);
        SiswamediaGoogleSignIn.getInstance().scopes.addAll([
          'https://www.googleapis.com/auth/drive.file',
        ]);
        await SiswamediaGoogleSignIn.getInstance().signInSilently();
        var headers = await SiswamediaGoogleSignIn.getInstance().currentUser.authHeaders;
        var requestHeaders = <String, String>{
          'X-Upload-Content-Type': type ?? 'audio/ogg',
          'Content-Type': 'application/json',
          'X-Upload-Content-Length': '$totalByteLength',
        };
        headers.forEach((key, value) {
          requestHeaders[key] = value;
        });
        var resp =
            await client2.post('$baseGoogleApiUrl/upload/drive/v3/files?uploadType=resumable',
                body: json.encode(<String, int>{
                  'Content-Length': totalByteLength,
                }),
                headers: requestHeaders);
        print('response');
        print(resp.statusCode);
        print(resp.headers);
        final httpClient = getHttpClient();
        //3145727
        print(file.size);
        // if (file.size > 1048576) {
        //   throw LargeFileException(cause: 'Data lebih dari 1GB');
        // }

        final client = HttpClient();
        final response = await client.putUrl(Uri.parse(resp.headers['location']));

        var byteCount = 0;

        //todo
        // response.contentLength = File(file.path).lengthSync();
        response.contentLength = totalByteLength;
        print('content lenght ${response.contentLength}');
        response.headers.set(HttpHeaders.contentTypeHeader, type);
        response.headers.set(HttpHeaders.contentLengthHeader, totalByteLength);
        response.headers.set('Content-Range', 'bytes 0-${totalByteLength - 1}/$totalByteLength');

        headers.forEach((key, value) {
          response.headers.add(key, value);
        });

        final stream = await File(file.path).openRead();

        Navigator.pop(context);
        var stream2 = stream.transform<List<int>>(
          StreamTransformer.fromHandlers(
            handleData: (data, sink) {
              byteCount += data.length;
              print(byteCount);
              sink.add(data);
              if (onUploadProgress != null) {
                print('uploading');
                onUploadProgress(byteCount, totalByteLength, context);
                // CALL STATUS CALLBACK;
              }
            },
            handleError: (error, stack, sink) {},
            handleDone: (sink) {
              sink.close();
            },
          ),
        );

        await response.addStream(stream2);

        final httpResponse = await response.close();
        var statusCode = httpResponse.statusCode;

        print(statusCode);
        if (statusCode ~/ 100 != 2) {
          print(statusCode);
          throw SiswamediaException(
              'Error uploading file, Status code: ${httpResponse.statusCode}');
        } else {
          return await readResponseAsString(httpResponse, file.name, headers);
        }
        // return response;
      } else {
        progressUploadState.state.progress = 0;
        // return null;
      }
    } on LargeFileException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } on ErrorUploadException catch (error) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        error.cause,
        context,
      );
    } catch (e) {
      progressUploadState.state.progress = 0;
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        e.toString(),
        context,
      );
      print(e);
    }
  }

  Future<String> readResponseAsString(
      HttpClientResponse response, String filename, Map<String, String> headers) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    headers['content-type'] = 'application/json';
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
      // var result = json.decode(data) as Map<String, dynamic>;
      // print(result);
      // var file = ModelUploadFile.fromJson(result);
      // setAttachments(file.data);
    }, onDone: () async {
      completer.complete(contents.toString());
      var result = await completer.future;
      var data = json.decode(result) as Map<String, dynamic>;
      var client = Client();
      print(data['id']);
      var resp = await client.patch('$baseGoogleApiUrl/drive/v3/files/${data['id']}?fields=*',
          body: json.encode({'name': filename}), headers: headers);
      print(resp.body);
      await client
          .post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
              body: json.encode({'role': 'reader', 'type': 'anyone'}), headers: headers)
          .then((value) {
        print(value.body);
        CustomFlushBar.successFlushBar('Berhasil Mengupload Lampiran', context);
        dummy.setState(
            (s) => s.setVideoUrl('https://drive.google.com/uc?export=view&id=${data['id']}'));
        progressUploadState.setState((s) => s.setProgress(0), silent: true);
      });
      // print(await completer.future);
    });
    return completer.future;
  }

  void _setUploadProgress(int sentBytes, int totalBytes, BuildContext context) {
    print(sentBytes);
    print(totalBytes);
    progressUploadState.setState((s) => s.setProgress(sentBytes), silent: true);
    progressUploadState.setState((s) => s.setContent(totalBytes), silent: true);
    var __progressValue = Utils.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

    __progressValue = double.parse(__progressValue.toStringAsFixed(2));

    if (__progressValue != _progressValue) {
      setState(() {
        _progressValue = __progressValue;
        _progressPercentValue = (_progressValue * 100.0).toInt();
      });
    }
  }
}
