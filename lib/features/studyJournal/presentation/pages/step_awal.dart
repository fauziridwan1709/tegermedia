part of '_pages.dart';

// class StepAwal extends StatefulWidget {
//   final HashMap<int, CourseByClassData> courses;
//
//   const StepAwal({this.courses});
//
//   @override
//   _StepAwalState createState() => _StepAwalState();
// }
//
// class _StepAwalState extends State<StepAwal> {
//   final auth = GlobalState.auth();
//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//       children: [
//         Content(
//           'Kelas',
//           child: Row(
//             children: [
//               Expanded(
//                 child: SimpleContainer(
//                   radius: BorderRadius.circular(4),
//                   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
//                   color: SiswamediaTheme.border,
//                   child: SimpleText(
//                       auth.state?.currentState?.className ?? 'kelas 10 IPA'),
//                 ),
//               ),
//             ],
//           ),
//         ),
//         StateBuilder(
//           observe: () => course,
//           builder: (context, snapshot) {
//             return Content(
//               'Mata Pelajaran',
//               child: DropdownButtonFormField<int>(
//                   value: dummyState.createModel.courseId,
//                   onChanged: (val) {
//                     dummy.setState((s) => s.changeCourseId(val));
//                     var selectedCourse = course.state.courseMap.values
//                         .firstWhere((element) => element.id == val);
//                     listGuru = selectedCourse.listGuru;
//                   },
//                   decoration: __inputDecoration,
//                   // onTap: () {
//                   //   print('courses length');
//                   //   print(courses.length);
//                   //   if (courses.isEmpty)
//                   //     CustomFlushBar.errorFlushBar(
//                   //         'Belum ada Mata pelajaran', context);
//                   // },
//                   hint: SimpleText(
//                     widget.courses.isEmpty
//                         ? '- Tidak ada mata pelajaran -'
//                         : '- Pilih mata pelajaran -',
//                     style: SiswamediaTextStyle.description,
//                     color: SiswamediaTheme.border,
//                     fontSize: 12,
//                   ),
//                   items: List.generate(
//                       course.state.courseMap.length, __buildCourseDropdown)),
//             );
//           },
//         ),
//         if (dummyState.createModel.courseId != null)
//           Content(
//             'Pilih Guru',
//             child: listGuru.isEmpty
//                 ? SimpleText('Tidak ada guru')
//                 : DropdownButtonFormField<int>(
//                     value: dummyState.createModel.teacherUserId,
//                     onChanged: (val) {
//                       dummy.setState((s) => s.changeTeacherId(val));
//                     },
//                     decoration: __inputDecoration,
//                     hint: SimpleText(
//                       '- Pilih guru -',
//                       style: SiswamediaTextStyle.description,
//                       color: SiswamediaTheme.border,
//                       fontSize: 12,
//                     ),
//                     items:
//                         List.generate(listGuru.length, __buildTeacherDropdown)),
//           ),
//         Content(
//           'Waktu Pelaksanaan',
//           child: Column(
//             children: [
//               InkWell(
//                   key: Key('CreateStudyJournalShowDatePicker'),
//                   onTap: () async {
//                     var data = await showDatePicker(
//                         context: context,
//                         initialDate: DateTime.now().toLocal(),
//                         firstDate: DateTime.now().toLocal(),
//                         lastDate:
//                             DateTime.now().toLocal().add(Duration(days: 365)));
//                     await dummy.setState((s) => s.changeDayMonthYear(data));
//                     Utils.logWithDotes(data.toUtc().toIso8601String(),
//                         info: 'mantap');
//                   },
//                   child: Column(
//                     children: [
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           SimpleText('Tanggal Mulai'),
//                           SimpleText(dummyState.dayMonthYear,
//                               style: SiswamediaTextStyle.subtitle),
//                         ],
//                       ),
//                       Divider()
//                     ],
//                   )),
//               HeightSpace(12),
//               InkWell(
//                   onTap: () async {
//                     var data = await showTimePicker(
//                       context: context,
//                       initialTime: TimeOfDay.now(),
//                     );
//                     await dummy.setState((s) => s.changeHourMinute(data));
//                     Utils.logWithDotes(data, info: 'mantap');
//                   },
//                   child: Column(
//                     children: [
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           SimpleText('Waktu Mulai'),
//                           SimpleText(
//                             dummyState.hourMinute,
//                             style: SiswamediaTextStyle.subtitle,
//                           ),
//                         ],
//                       ),
//                       Divider()
//                     ],
//                   )),
//             ],
//           ),
//         ),
//         HeightSpace(80),
//       ],
//     );
//   }
// }
