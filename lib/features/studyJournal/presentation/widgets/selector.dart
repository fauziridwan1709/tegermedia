part of '_widgets.dart';

class SelectorWidget extends StatelessWidget {
  final String label;
  final Function(BuildContext) onTap;
  final Color color;

  const SelectorWidget({this.label, this.onTap, this.color = SiswamediaTheme.green});

  @override
  Widget build(BuildContext context) {
    return CustomContainer(
      // margin: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
      border: Border.all(color: color, width: 1.5),
      radius: BorderRadius.circular(6),
      color: SiswamediaTheme.transparent,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      onTap: () => onTap(context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SimpleText(label ?? '', fontSize: 12, style: SiswamediaTextStyle.subtitle, color: color),
          Icon(Icons.arrow_forward_ios_outlined, size: 14, color: color)
        ],
      ),
    );
  }
}
