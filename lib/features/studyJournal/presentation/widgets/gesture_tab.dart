part of '_widgets.dart';

class GestureTab extends Container {
  GestureTab({VoidCallback onTap, String text})
      : super(child: GestureDetector(onTap: onTap, child: Tab(text: text)));
}
