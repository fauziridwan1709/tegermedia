part of '_widgets.dart';

class CardSelectedQuiz extends StatelessWidget {
  final Quizzes model;
  final int id;
  final Function(int, String) onDelete;

  const CardSelectedQuiz(
    this.model, {
    this.id,
    this.onDelete,
  });

  @override
  Widget build(BuildContext context) {
    var date = DateTime.parse(model.startTime).toLocal();
    var data =
        '${date.hour < 10 ? '0${date.hour}' : date.hour}.${date.minute < 10 ? '0${date.minute}' : date.minute}';
    return Column(
      children: [
        CustomContainer(
          margin: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
          // boxShadow: [SiswamediaTheme.shadowContainer],
          radius: BorderRadius.circular(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SimpleText(
                      model.name,
                      style: SiswamediaTextStyle.subtitle,
                      fontSize: 14,
                    ),
                    HeightSpace(5),
                    Row(
                      children: [
                        SimpleText(
                          '${dayMonthYear(model.startTime)} - ${dayMonthYear(model.endTime)}',
                          style: SiswamediaTextStyle.description,
                          fontSize: 10,
                        ),
                        WidthSpace(20),
                        SimpleText(
                          '${model.duration} menit',
                          style: SiswamediaTextStyle.description,
                          fontSize: 12,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              IconButton(
                icon: Icon(Icons.restore_from_trash_rounded,
                    color: SiswamediaTheme.red),
                onPressed: () {
                  onDelete(id, model.type);
                },
              )
            ],
          ),
        ),
        Divider()
      ],
    );
  }

  String dayMonthYear(String time) {
    var date = DateTime.parse(time).toLocal();
    return '${date.day} ${bulan[date.month - 1]} ${date.year}';
  }
}
