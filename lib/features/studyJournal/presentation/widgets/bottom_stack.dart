part of '_widgets.dart';

class BottomStack extends Positioned {
  BottomStack(
      {double height, Widget child, Color color = SiswamediaTheme.white})
      : super(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              height: height,
              decoration: BoxDecoration(
                  color: color, boxShadow: [SiswamediaTheme.shadowContainer]),
              child: Center(child: child),
            ));
}
