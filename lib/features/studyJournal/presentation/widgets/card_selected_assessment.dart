part of '_widgets.dart';

class CardSelectedAssessment extends StatelessWidget {
  final String title;
  final int id;
  final Function(int) onDelete;

  const CardSelectedAssessment(
    this.title, {
    this.id,
    this.onDelete,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomContainer(
          margin: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
          // boxShadow: [SiswamediaTheme.shadowContainer],
          radius: BorderRadius.circular(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: SimpleText(
                title,
                style: SiswamediaTextStyle.subtitle,
                fontSize: 12,
              )),
              IconButton(
                icon: Icon(Icons.restore_from_trash_rounded, color: SiswamediaTheme.red),
                onPressed: () {
                  onDelete(id);
                },
              )
            ],
          ),
        ),
        Divider()
      ],
    );
  }
}
