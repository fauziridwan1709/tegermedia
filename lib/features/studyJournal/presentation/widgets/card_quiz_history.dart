part of '_widgets.dart';

class CardQuizHistory extends StatelessWidget {
  final Function(int id) onTap;
  final QuizListDetail data;

  const CardQuizHistory({
    this.onTap,
    this.data,
  });

  @override
  Widget build(BuildContext context) {
    var isExpired = DateTime.parse(data.endDate)
        .toLocal()
        .isBefore(DateTime.now().toLocal());
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              offset: Offset(2, 2),
              spreadRadius: 2,
              blurRadius: 2,
              color: Colors.grey.withOpacity(.2),
            )
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('${data.courseName}',
              style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.w700,
                  fontSize: 16)),
          SimpleText('${data.name}',
              style: SiswamediaTextStyle.description, fontSize: 12),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: <Widget>[
                  SimpleText(
                      "${data.totalPg == -1 ? data.totalEssay : data.totalEssay == -1 ? data.totalPg : data.totalPg + data.totalEssay} soal",
                      style: SiswamediaTextStyle.subtitle,
                      fontSize: 12),
                  WidthSpace(20),
                  Icon(Icons.timer, size: 14),
                  SimpleText('${data.totalTime} menit',
                      style: SiswamediaTextStyle.subtitle, fontSize: 12),
                ],
              ),
              Container(
                height: 30,
                width: 60,
                decoration: BoxDecoration(
                  color: isExpired
                      ? SiswamediaTheme.green.withOpacity(.4)
                      : SiswamediaTheme.green,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: isExpired
                        ? null
                        : () {
                            GlobalState.dummy().setState((s) => s.changeQuiz({
                                  data.id: Quizzes(
                                      type: 'normal',
                                      name: data.name,
                                      startTime: data.startDate,
                                      endTime: data.endDate,
                                      duration: data.totalTime.toInt())
                                }));
                            context.pop();
                          },
                    child: Center(
                        child: Text("Pilih",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 14))),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
