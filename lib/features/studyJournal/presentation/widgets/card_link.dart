part of '_widgets.dart';

class CardLink extends StatelessWidget {
  final Attachment data;
  final int index;
  final Function(int) onDelete;
  final Function(String, String) onChangedName;
  final Function(String, String) onChangedDesc;
  final Map<String, TextEditingController> mantap;

  const CardLink(this.data,
      {this.index, this.onDelete, this.mantap, this.onChangedName, this.onChangedDesc});

  @override
  Widget build(BuildContext context) {
    return CustomContainer(
      margin: EdgeInsets.symmetric(horizontal: 4, vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      boxShadow: [SiswamediaTheme.shadowContainer],
      radius: BorderRadius.circular(8),
      color: SiswamediaTheme.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Content('Nama Link',
              height: 0,
              child: TextField(
                controller: mantap['name$index'],
                onChanged: (s) => onChangedName(s, 'name$index'),
                style: TextStyle(fontSize: 12),
                decoration: InputDecoration(
                    hintText: 'name...',
                    filled: true,
                    fillColor: SiswamediaTheme.greyBg,
                    contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    border: InputBorder.none),
              )),
          Content('Link',
              height: 0,
              child: TextField(
                controller: mantap['desc${index}'],
                onChanged: (s) => onChangedName(s, 'desc$index}'),
                style: TextStyle(fontSize: 12),
                decoration: InputDecoration(
                    hintText: 'link...',
                    filled: true,
                    fillColor: SiswamediaTheme.greyBg,
                    contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    border: InputBorder.none),
              )),
          AutoLayoutButton(
            text: 'Hapus',
            onTap: () {
              onDelete(index);
            },
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
            textColor: SiswamediaTheme.white,
            radius: BorderRadius.circular(4),
            fontSize: 12,
            backGroundColor: SiswamediaTheme.red,
          )
        ],
      ),
    );
  }
}
