part of '_states.dart';

class CreateStudyJournalState
    implements FutureState<CreateStudyJournalState, int> {
  final StudyJournalRepository repo = StudyJournalRepositoryImpl();

  int step = 0;
  bool skipAssignment = true;
  bool skipQuiz = true;
  Map<int, String> titleAssessments = {};
  Map<int, Quizzes> titleQuizzes = {};
  List<int> listAssignment = [];
  List<int> listQuiz = [];

  //todo cache studyJournal tiap get detail study journal masukkin kesini (panca)
  // HashMap<int, StudyJournal> detailClassMap = HashMap<int, StudyJournal>();
  CreateStudyJournal createModel = CreateStudyJournalModel(
      startTime: DateTime.now().toUtc().toIso8601String(),
      videoUrl: '',
      listAttachment: <AttachmentModel>[],
      listAssessmentId: <int>[],
      listQuizId: <int>[]);

  @override
  String cacheKey = 'mantap';

  @override
  bool getCondition() {
    return false;
  }

  @override
  Future<void> retrieveData(int k) async {}

  // Future<StudyJournal> getDetailStudyJournal(int journalId) async {
  //   //todo (panca)
  // }

  Future<void> createData(
      CreateStudyJournalModel data,
      Map<String, TextEditingController> listController,
      BuildContext context) async {
    SiswamediaTheme.infoLoading(
        context: context, info: 'Membuat journal kelas..');
    var attachmentModel = <AttachmentModel>[];
    for (var i = 0; i < listController.length / 2; i++) {
      var name = listController['name$i'];
      var url = listController['desc$i'];
      print(name);
      print(url);
      attachmentModel.add(AttachmentModel(name: name.text, link: url.text));
    }
    data.listAttachment = attachmentModel;
    var result = await repo.createStudyJournal(data);

    result.fold<void>((failure) {
      pop(context);
      CustomFlushBar.errorFlushBar(failure.translate(), context);
    }, (result) async {
      pop(context);
      pop(context);
      await GlobalState.journey().setState((s) async => await s.retrieveData(0),
          catchError: true, onError: (context, dynamic error) {
        print((error as SiswamediaException).message);
      }, onData: (_, data) {
        // journeyState.setState((s) => s.setCount(0));
      });
      result.statusCode.translate<void>(
        ifElse: () => CustomFlushBar.errorFlushBar(result.message, context),
        ifSuccess: () {
          CustomFlushBar.successFlushBar(result.message, context);
        },
      );
    });
  }

  void changeTeacherId(int teacherId) {
    Utils.log('change teacher id', info: 'changed');
    createModel.teacherUserId = teacherId;
  }

  void changeCourseId(int courseId) {
    Utils.log('change course id', info: 'changed');
    createModel.courseId = courseId;
  }

  void changeDayMonthYear(DateTime data) {
    Utils.log('change start time', info: 'changed');
    var as = data.yearMonthDay.toUtc().toIso8601String();
    createModel.startTime = as;
  }

  void changeHourMinute(TimeOfDay data) {
    Utils.log('change start time', info: 'changed');
    var as = Duration(hours: data.hour, minutes: data.minute);
    var aw = DateTime.parse(createModel.startTime)
        .toUtc()
        .yearMonthDay
        .add(as)
        .toUtc();
    createModel.startTime = aw.toIso8601String();
    Logger().d(createModel.startTime);
    print(validateStepOne());
    print(step);
  }

  void changeTitle(String val) {
    Utils.log('change title', info: 'changed');
    createModel.name = val;
  }

  void changeDescription(String val) {
    Utils.log('change description', info: 'changed');
    createModel.guide = val;
  }

  void setVideoUrl(String val) {
    Utils.log(val, info: 'setting video url');
    createModel.videoUrl = val;
  }

  void deleteVideoUrl() {
    Utils.log('deleted?', info: 'deleting video url');
    createModel.videoUrl = null;
  }

  void addListAttachment(Attachment val) {
    Utils.log('change description', info: 'changed');
    createModel.listAttachment.add(val);
  }

  void removeListAttachment(int index) {
    Utils.log('change description', info: 'changed');
    createModel.listAttachment.removeAt(index);
  }

  void changeSkipAssignment() {
    skipAssignment = !skipAssignment;
  }

  void changeSkipQuiz() {
    skipQuiz = !skipQuiz;
  }

  void changeAssignment(Map<int, String> assessments) {
    createModel.listAssessmentId.addAll(assessments.keys.toList());
    titleAssessments.addAll(assessments);
  }

  void changeQuiz(Map<int, Quizzes> quizzes) {
    createModel.listQuizId.addAll(quizzes.keys.toList());
    titleQuizzes.addAll(quizzes);
  }

  void deleteAssessment(int id) {
    createModel.listAssessmentId.remove(id);
    titleAssessments.remove(id);
  }

  Future<void> deleteQuiz(int id, String type) async {
    if (type == 'new') {
      print('newew');
      await QuizServices.deleteQuiz(id);
    }
    createModel.listQuizId.remove(id);
    titleQuizzes.remove(id);
  }

  void next() {
    if (step != 3) {
      step += 1;
    }
  }

  void prev() {
    if (step != 0) {
      step -= 1;
    }
  }

  bool validateStepOne() {
    return createModel.teacherUserId != null &&
        createModel.courseId != null &&
        createModel.startTime != null &&
        step == 0;
  }

  bool validateStepTwo() {
    return createModel.name != null && createModel.guide != null && step == 1;
  }

  bool validateStepThree() {
    return (skipAssignment ? skipAssignment : titleAssessments.isNotEmpty) &&
        step == 2;
  }

  bool validateStepFour() {
    return (skipQuiz ? skipQuiz : titleQuizzes.isNotEmpty) && step == 3;
  }

  String get dayMonthYear {
    var date = DateTime.parse(createModel.startTime).toLocal();
    return '${date.day} ${bulan[date.month - 1]} ${date.year}';
  }

  String get hourMinute {
    var date = DateTime.parse(createModel.startTime).toLocal();
    return '${date.hour < 10 ? '0${date.hour}' : date.hour}.${date.minute < 10 ? '0${date.minute}' : date.minute}';
  }
}

class Quizzes {
  String type;
  String name;
  String startTime;
  String endTime;
  int duration;

  Quizzes({this.type, this.name, this.startTime, this.endTime, this.duration});
}
