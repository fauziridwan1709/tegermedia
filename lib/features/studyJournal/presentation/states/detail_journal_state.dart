part of '_states.dart';

class DetailJournalState {
  final StudyJournalRepository repo = StudyJournalRepositoryImpl();
  StudyJournal detailJournal;
  List<CommentModel> listComment;
  List<DataAssignmentReference> listAssessment;
  List<ListQuiz> listQuiz;

  Future<void> getDetailJournal(int journalId) async {
    await getQuiz(journalId).then((value) => getLinkAssessment(journalId));
    var resp = await repo.getDetailStudyJournal(journalId);
    resp.fold<void>((failure) => throw failure, (result) async {
      result.statusCode.translate<void>(
        ifSuccess: () async {
          listComment = result.data.comment;
          detailJournal = result.data;

          print('---data---');
        },
        ifElse: () {
          throw result.statusCode.toFailure('Terjadi kesalahan');
        },
      );
    });
  }

  Future<void> sendComment(
      String message, int journalId, BuildContext context) async {
    var resp = await repo.sendComment(message, journalId);
    resp.fold<void>((failure) => throw failure, (result) {
      result.statusCode.translate<void>(ifSuccess: () {
        listComment = result.data;
      }, ifElse: () {
        CustomFlushBar.errorFlushBar(result.message, context);
      });
    });
  }

  Future<void> getLinkAssessment(int journalId) async {
    print('get data course');
    var auth = GlobalState.auth();
    try {
      var query =
          'class_id=${auth.state.currentState.classId}&journal_id=$journalId';
      var response = await GetJourneyServices.getAssessment(query);
      listAssessment = response.data;
      Utils.log(response.toString(), info: 'get link assessment');
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }

  Future<void> getQuiz(int journalId) async {
    print('get data course');
    var auth = GlobalState.auth();
    var classId = auth.state.currentState.classId;
    try {
      var query = 'class_id=$classId&journal_id=$journalId&limit=50&page=1';
      var response = await GetJourneyServices.getQuiz(query);

      listQuiz = response;
      print('get link');
      Utils.log(response.toString(), info: 'get link quiz');
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }
}
