import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:tegarmedia/core/_core.dart';

import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/features/studyJournal/data/models/_models.dart';
import 'package:tegarmedia/features/studyJournal/data/models/quiz.dart';
import 'package:tegarmedia/features/studyJournal/data/repositories/_repositories.dart';
import 'package:tegarmedia/features/studyJournal/domain/entities/_entities.dart';
import 'package:tegarmedia/features/studyJournal/domain/repositories/_repositories.dart';
import 'package:tegarmedia/models/assignment/_assignment.dart';
import 'package:tegarmedia/models/journey/courser_journey.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/services/journey/_journey.dart';
import 'package:tegarmedia/states/global_state.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:tegarmedia/models/journey/courser_journey.dart';

part 'create_study_journal_state.dart';
part 'detail_journal_state.dart';
