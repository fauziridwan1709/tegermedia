part of '_repositories.dart';

class StudyJournalRepositoryImpl implements StudyJournalRepository {
  final StudyJournalRemoteDataSource remoteDataSource =
      StudyJournalRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<ResultId>>> createStudyJournal(
      CreateStudyJournal model) async {
    return await apiCall(remoteDataSource.createStudyJournal(model));
  }

  @override
  Future<Decide<Failure, Parsed<StudyJournal>>> getDetailStudyJournal(
      int journalId) async {
    return await apiCall(remoteDataSource.getDetailStudyJournal(journalId));
  }

  @override
  Future<Decide<Failure, Parsed<List<Comment>>>> sendComment(
      String message, int journalId) async {
    return await apiCall(remoteDataSource.sendComment(message, journalId));
  }
}
