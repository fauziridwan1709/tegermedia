import 'package:tegarmedia/aw/entities/_entities.dart';
import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/features/studyJournal/data/models/_models.dart';
import 'package:tegarmedia/features/studyJournal/domain/entities/_entities.dart';

part 'study_journal_remote_data_source.dart';
