part of '_datasources.dart';

abstract class StudyJournalRemoteDataSource {
  Future<Parsed<ResultId>> createStudyJournal(CreateStudyJournalModel model);
  Future<Parsed<StudyJournal>> getDetailStudyJournal(int journalId);
  Future<Parsed<List<CommentModel>>> sendComment(String message, int journalId);
}

class StudyJournalRemoteDataSourceImpl implements StudyJournalRemoteDataSource {
  @override
  Future<Parsed<ResultId>> createStudyJournal(
      CreateStudyJournalModel model) async {
    var url = '${Endpoints.studyJournal}';
    var resp = await postIt(url, model.toJson(),
        headers: await Pref.getSiswamediaHeader());
    return resp.parse(ResultIdModel.fromJson(resp.bodyAsMap));
  }

  @override
  Future<Parsed<StudyJournal>> getDetailStudyJournal(int journalId) async {
    var url = '${Endpoints.studyJournal}/$journalId';
    var resp = await getIt(url, headers: await Pref.getSiswamediaHeader());
    return resp.parse(StudyJournalModel.fromJson(resp.dataBodyAsMap));
  }

  @override
  Future<Parsed<List<CommentModel>>> sendComment(
      String message, int journalId) async {
    var url = '${Endpoints.studyJournal}/$journalId/comment';
    var body = <String, dynamic>{
      'comment': '$message',
      'sent_time': DateTime.now().toIso8601String() + 'Z'
    };
    var resp =
        await postIt(url, body, headers: await Pref.getSiswamediaHeader());
    return resp.parse(ListCommentModel.fromJson(resp.bodyAsMap).list);
  }
}
