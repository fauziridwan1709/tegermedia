part of '_models.dart';

class ListCommentModel {
  List<CommentModel> list;

  ListCommentModel({this.list});

  ListCommentModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <CommentModel>[];
      json['data'].forEach((dynamic v) {
        list.add(CommentModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
