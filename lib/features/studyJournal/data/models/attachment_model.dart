part of '_models.dart';

class AttachmentModel extends Attachment {
  AttachmentModel({
    String name,
    String link,
  }) : super(name: name, link: link);

  AttachmentModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['link'] = link;
    return data;
  }
}
