class Quiz {
  int statusCode;
  String message;
  Data data;

  Quiz({this.statusCode, this.message, this.data});

  Quiz.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<ListQuiz> listData;
  int totalCount;

  Data({this.listData, this.totalCount});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['list_data'] != null) {
      listData = <ListQuiz>[];
      json['list_data'].forEach((dynamic v) {
        listData.add(ListQuiz.fromJson(v as Map<String, dynamic>));
      });
    }
    totalCount = json['total_count'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (listData != null) {
      data['list_data'] = listData.map((v) => v.toJson()).toList();
    }
    data['total_count'] = totalCount;
    return data;
  }
}

class ListQuiz {
  int id;
  int schoolId;
  String schoolName;
  int classId;
  String className;
  int courseId;
  String courseName;
  String quizType;
  String name;
  int totalTime;
  int totalPg;
  int totalEssay;
  dynamic totalNilai;
  String startDate;
  String endDate;
  Null students;
  Null quizAttachments;
  bool isHidden;
  bool isRandom;
  bool hasBeenDone;

  ListQuiz(
      {this.id,
      this.schoolId,
      this.schoolName,
      this.classId,
      this.className,
      this.courseId,
      this.courseName,
      this.quizType,
      this.name,
      this.totalTime,
      this.totalPg,
      this.totalEssay,
      this.totalNilai,
      this.startDate,
      this.endDate,
      this.students,
      this.quizAttachments,
      this.isHidden,
      this.isRandom,
      this.hasBeenDone});

  ListQuiz.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    courseName = json['course_name'];
    quizType = json['quiz_type'];
    name = json['name'];
    totalTime = json['total_time'];
    totalPg = json['total_pg'];
    totalEssay = json['total_essay'];
    totalNilai = json['total_nilai'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    students = json['students'];
    quizAttachments = json['quiz_attachments'];
    isHidden = json['is_hidden'];
    isRandom = json['is_random'];
    hasBeenDone = json['has_been_done'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['class_id'] = classId;
    data['class_name'] = className;
    data['course_id'] = courseId;
    data['course_name'] = courseName;
    data['quiz_type'] = quizType;
    data['name'] = name;
    data['total_time'] = totalTime;
    data['total_pg'] = totalPg;
    data['total_essay'] = totalEssay;
    data['total_nilai'] = totalNilai;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['students'] = students;
    data['quiz_attachments'] = quizAttachments;
    data['is_hidden'] = isHidden;
    data['is_random'] = isRandom;
    data['has_been_done'] = hasBeenDone;
    return data;
  }
}
