part of '_models.dart';

class CommentModel extends Comment {
  CommentModel.fromJson(Map<String, dynamic> json) {
    commentId = json['comment_id'];
    comment = json['comment'];
    sentDate = json['sent_date'];
    senderUserId = json['sender_user_id'];
    senderName = json['sender_name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['comment_id'] = commentId;
    data['comment'] = comment;
    data['sent_date'] = sentDate;
    data['sender_user_id'] = senderUserId;
    data['sender_name'] = senderName;
    return data;
  }
}
