import 'package:tegarmedia/features/studyJournal/domain/entities/_entities.dart';

part 'attachment_model.dart';
part 'comment_model.dart';
part 'create_study_journal_model.dart';
part 'list_comment_model.dart';
part 'study_journal_model.dart';
