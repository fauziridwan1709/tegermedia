part of '_models.dart';

class CreateStudyJournalModel extends CreateStudyJournal {
  CreateStudyJournalModel(
      {String name,
      String startTime,
      String guide,
      int courseId,
      int teacherUserId,
      String videoUrl,
      List<AttachmentModel> listAttachment,
      List<int> listAssessmentId,
      List<int> listQuizId})
      : super(
          name: name,
          startTime: startTime,
          guide: guide,
          courseId: courseId,
          teacherUserId: teacherUserId,
          videoUrl: videoUrl,
          listAttachment: listAttachment,
          listAssessmentId: listAssessmentId,
          listQuizId: listQuizId,
        );

  CreateStudyJournalModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    startTime = json['start_time'];
    guide = json['guide'];
    courseId = json['course_id'];
    teacherUserId = json['teacher_user_id'];
    videoUrl = json['video_url'];
    if (json['list_attachment'] != null) {
      listAttachment = <AttachmentModel>[];
      json['list_attachment'].forEach((dynamic v) {
        listAttachment.add(AttachmentModel.fromJson(v as Map<String, dynamic>));
      });
    }
    listAssessmentId = json['list_assessment_id'].cast<int>();
    listQuizId = json['list_quiz_id'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['start_time'] = startTime;
    data['guide'] = guide;
    data['course_id'] = courseId;
    data['teacher_user_id'] = teacherUserId;
    data['video_url'] = videoUrl;
    if (listAttachment != null) {
      data['list_attachment'] = listAttachment.map((v) => v.toJson()).toList();
    }
    data['list_assessment_id'] = listAssessmentId;
    data['list_quiz_id'] = listQuizId;
    return data;
  }
}
