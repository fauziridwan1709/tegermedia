part of '_models.dart';

class StudyJournalModel extends StudyJournal {
  StudyJournalModel(
      {String name,
      String startTime,
      int teacherUserId,
      String guide,
      List<CommentModel> comment,
      List<AttachmentModel> attachment,
      String videoUrl,
      int journalId})
      : super(
            name: name,
            startTime: startTime,
            teacherUserId: teacherUserId,
            guide: guide,
            comment: comment,
            attachment: attachment,
            videoUrl: videoUrl,
            journalId: journalId);

  StudyJournalModel.fromJson(Map<String, dynamic> json) {
    print(json);
    name = json['name'];
    startTime = json['start_time'];
    teacherUserId = json['teacher_user_id'];
    guide = json['guide'];
    if (json['comment'] != null) {
      comment = <CommentModel>[];
      json['comment'].forEach((dynamic v) {
        comment.add(CommentModel.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['attachment'] != null) {
      attachment = <AttachmentModel>[];
      json['attachment'].forEach((dynamic v) {
        attachment.add(AttachmentModel.fromJson(v as Map<String, dynamic>));
      });
    }
    videoUrl = json['video_url'] ?? '';
    journalId = json['journal_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['start_time'] = startTime;
    data['teacher_user_id'] = teacherUserId;
    data['guide'] = guide;
    if (comment != null) {
      data['comment'] = comment.map((v) => v.toJson()).toList();
    }
    if (attachment != null) {
      data['attachment'] = attachment.map((v) => v.toJson()).toList();
    }
    data['video_url'] = videoUrl;
    data['journal_id'] = journalId;
    return data;
  }
}
