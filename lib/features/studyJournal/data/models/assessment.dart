class Assessment {
  int statusCode;
  String message;
  DataAssessment data;

  Assessment({this.statusCode, this.message, this.data});

  Assessment.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? DataAssessment.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataAssessment {
  List<ListAssessment> listData;
  int totalCount;

  DataAssessment({this.listData, this.totalCount});

  DataAssessment.fromJson(Map<String, dynamic> json) {
    if (json['list_data'] != null) {
      listData = <ListAssessment>[];
      json['list_data'].forEach((dynamic v) {
        listData.add(ListAssessment.fromJson(v as Map<String, dynamic>));
      });
    }
    totalCount = json['total_count'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (listData != null) {
      data['list_data'] = listData.map((v) => v.toJson()).toList();
    }
    data['total_count'] = totalCount;
    return data;
  }
}

class ListAssessment {
  int id;
  int userId;
  String fullName;
  String imageUser;
  String email;
  String status;
  int assessmentId;
  bool newComment;
  int nilai;
  String name;
  String className;
  String description;
  String mainDescription;
  String createdAt;
  String updatedAt;
  String startDate;
  String endDate;
  String courseName;

  ListAssessment(
      {this.id,
      this.userId,
      this.fullName,
      this.imageUser,
      this.email,
      this.status,
      this.assessmentId,
      this.newComment,
      this.nilai,
      this.name,
      this.className,
      this.description,
      this.mainDescription,
      this.createdAt,
      this.updatedAt,
      this.startDate,
      this.endDate,
      this.courseName});

  ListAssessment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    fullName = json['full_name'];
    imageUser = json['image_user'];
    email = json['email'];
    status = json['status'];
    assessmentId = json['assessment_id'];
    newComment = json['new_comment'];
    nilai = json['nilai'];
    name = json['name'];
    className = json['class_name'];
    description = json['description'];
    mainDescription = json['main_description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    courseName = json['course_name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['full_name'] = fullName;
    data['image_user'] = imageUser;
    data['email'] = email;
    data['status'] = status;
    data['assessment_id'] = assessmentId;
    data['new_comment'] = newComment;
    data['nilai'] = nilai;
    data['name'] = name;
    data['class_name'] = className;
    data['description'] = description;
    data['main_description'] = mainDescription;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['course_name'] = courseName;
    return data;
  }
}
