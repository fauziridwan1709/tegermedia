part of '_entities.dart';

class StudyJournal extends Models {
  String name;
  String startTime;
  int teacherUserId;
  String guide;
  List<CommentModel> comment;
  List<AttachmentModel> attachment;
  String videoUrl;
  int journalId;

  StudyJournal(
      {this.name,
      this.startTime,
      this.teacherUserId,
      this.guide,
      this.comment,
      this.attachment,
      this.videoUrl,
      this.journalId});
}
