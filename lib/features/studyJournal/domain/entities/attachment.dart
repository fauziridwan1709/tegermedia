part of '_entities.dart';

class Attachment extends Models {
  String name;
  String link;

  Attachment({this.name, this.link});
}
