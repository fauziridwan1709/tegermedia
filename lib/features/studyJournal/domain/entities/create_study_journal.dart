part of '_entities.dart';

class CreateStudyJournal extends Models {
  String name;
  String startTime;
  String guide;
  int courseId;
  int teacherUserId;
  String videoUrl;
  List<AttachmentModel> listAttachment;
  List<int> listAssessmentId;
  List<int> listQuizId;

  CreateStudyJournal(
      {this.name,
      this.startTime,
      this.guide,
      this.courseId,
      this.teacherUserId,
      this.videoUrl,
      this.listAttachment,
      this.listAssessmentId,
      this.listQuizId});
}
