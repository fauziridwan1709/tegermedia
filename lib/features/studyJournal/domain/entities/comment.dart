part of '_entities.dart';

class Comment extends Models {
  int commentId;
  String comment;
  String sentDate;
  int senderUserId;
  String senderName;

  Comment(
      {this.commentId,
      this.comment,
      this.sentDate,
      this.senderUserId,
      this.senderName});
}
