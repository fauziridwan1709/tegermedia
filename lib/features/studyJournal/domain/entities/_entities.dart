import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/features/studyJournal/data/models/_models.dart';

part 'attachment.dart';
part 'comment.dart';
part 'create_study_journal.dart';
part 'study_journal.dart';
