import 'package:tegarmedia/aw/entities/_entities.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/studyJournal/domain/entities/_entities.dart';

part 'study_journal_repository.dart';
