part of '_repositories.dart';

abstract class StudyJournalRepository {
  Future<Decide<Failure, Parsed<ResultId>>> createStudyJournal(
      CreateStudyJournal model);
  Future<Decide<Failure, Parsed<StudyJournal>>> getDetailStudyJournal(
      int journalId);
  Future<Decide<Failure, Parsed<List<Comment>>>> sendComment(
      String message, int journalId);
}
