part of '_repositories.dart';

class TeacherRepositoryImpl implements TeacherRepository {
  final TeacherRemoteDataSource remoteDataSource =
      TeacherRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<TeacherModel>>>> getAllTeacher(
      int classId) async {
    return await apiCall(remoteDataSource.getAllTeacher(classId));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> kickTeacher(
      int classId, UserIdModel id) async {
    return await apiCall(remoteDataSource.kickTeacher(classId, id));
  }
}
