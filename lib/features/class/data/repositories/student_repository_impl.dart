part of '_repositories.dart';

class StudentRepositoryImpl implements StudentRepository {
  final StudentRemoteDataSource remoteDataSource =
      StudentRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<StudentModel>>>> getAllStudent(
      int classId) async {
    return await apiCall(remoteDataSource.getAllStudent(classId));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> kickStudent(
      int classId, UserIdModel id) async {
    return await apiCall(remoteDataSource.kickStudent(classId, id));
  }
}
