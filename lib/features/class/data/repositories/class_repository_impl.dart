part of '_repositories.dart';

class ClassRepositoryImpl implements ClassRepository {
  final ClassRemoteDataSource remoteDataSource = ClassRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> changeWaliKelas(
      int classId, UserId id) async {
    return await apiCall(remoteDataSource.changeWaliKelas(classId, id));
  }

  @override
  Future<Decide<Failure, Parsed<ResultId>>> createMyClass(
      MyClassModel model) async {
    return await apiCall(remoteDataSource.createMyClass(model));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> deleteMyClass(int id) async {
    return await apiCall(remoteDataSource.deleteMyClass(id));
  }

  @override
  Future<Decide<Failure, Parsed<List<MyClassModel>>>> getAllMyClass(
      int schoolId) async {
    return await apiCall(remoteDataSource.getAllMyClass(schoolId));
  }

  @override
  Future<Decide<Failure, Parsed<MyClassModel>>> getMyClass(int classId) async {
    return await apiCall(remoteDataSource.getMyClass(classId));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> updateMyClass(
      MyClassModel model) async {
    return await apiCall(remoteDataSource.updateMyClass(model));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> joinClass(
      InvitationClassModel model) async {
    return await apiCall(remoteDataSource.joinClass(model));
  }

  @override
  Future<Decide<Failure, Parsed<NumberWaitingApprovalModel>>>
      getCountWaitingApproval(int schoolId) async {
    return await apiCall(remoteDataSource.getCountWaitingApproval(schoolId));
  }
}
