import 'dart:async';

import 'package:tegarmedia/aw/entities/_entities.dart';
import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/class/data/datasources/_datasources.dart';
import 'package:tegarmedia/features/class/data/models/_models.dart';
import 'package:tegarmedia/features/class/domain/repositories/_repositories.dart';

part 'class_repository_impl.dart';
part 'student_repository_impl.dart';
part 'teacher_repository_impl.dart';
