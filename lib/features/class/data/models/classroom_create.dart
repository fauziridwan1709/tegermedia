part of '_models.dart';

class GoogleClassroomClass {
  String name;
  String courseName;
  String gcrId;
  int schoolId;
  String tahunAjaran;
  String semester;
  String jurusan;
  String tingkat;
  List<String> studentList;
  List<String> teacherList;

  GoogleClassroomClass(
      {this.name,
      this.courseName,
      this.gcrId,
      this.schoolId,
      this.tahunAjaran,
      this.semester,
      this.jurusan,
      this.tingkat,
      this.studentList,
      this.teacherList});

  GoogleClassroomClass.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    courseName = json['course_name'];
    gcrId = json['gcr_id'];
    schoolId = json['school_id'];
    tahunAjaran = json['tahun_ajaran'];
    semester = json['semester'];
    jurusan = json['jurusan'];
    tingkat = json['tingkat'];
    studentList = json['student_list'].cast<String>();
    teacherList = json['teacher_list'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['course_name'] = courseName;
    data['gcr_id'] = gcrId;
    data['school_id'] = schoolId;
    data['tahun_ajaran'] = tahunAjaran;
    data['semester'] = semester;
    data['jurusan'] = jurusan;
    data['tingkat'] = tingkat;
    data['student_list'] = studentList;
    data['teacher_list'] = teacherList;
    return data;
  }
}
