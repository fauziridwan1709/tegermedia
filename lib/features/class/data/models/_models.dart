import 'package:tegarmedia/aw/models/_models.dart';

part 'all_class.dart';
part 'class_invitation_model.dart';
part 'classroom.dart';
part 'classroom_core.dart';
part 'classroom_create.dart';
part 'classroom_students.dart';
part 'classroom_teachers.dart';
part 'list_my_class_model.dart';
part 'list_student_model.dart';
part 'list_teacher_model.dart';
part 'my_class_model.dart';
part 'number_waiting_approval_model.dart';
part 'student_id_model.dart';
part 'student_model.dart';
part 'teacher_model.dart';
