part of '_models.dart';

class ModelAllClass {
  int statusCode;
  String message;
  List<DetailAllClass> data;

  ModelAllClass({this.statusCode, this.message, this.data});

  ModelAllClass.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DetailAllClass>[];
      json['data'].forEach((Map<String, dynamic> v) {
        data.add(DetailAllClass.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DetailAllClass {
  int id;
  int schoolId;
  String schoolName;
  String name;
  String tahunAjaran;
  String semester;
  String jurusan;
  String tingkat;
  String invitationStudentCode;
  String invitationTeacherCode;
  String role;
  String namaWalikelas;
  String nomorTelpWalikelas;
  bool isSudahGabung;
  String createdAt;
  String updatedAt;

  DetailAllClass(
      {this.id,
      this.schoolId,
      this.schoolName,
      this.name,
      this.tahunAjaran,
      this.semester,
      this.jurusan,
      this.tingkat,
      this.invitationStudentCode,
      this.invitationTeacherCode,
      this.role,
      this.namaWalikelas,
      this.nomorTelpWalikelas,
      this.isSudahGabung,
      this.createdAt,
      this.updatedAt});

  DetailAllClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    name = json['name'];
    tahunAjaran = json['tahun_ajaran'];
    semester = json['semester'];
    jurusan = json['jurusan'];
    tingkat = json['tingkat'];
    invitationStudentCode = json['invitation_student_code'];
    invitationTeacherCode = json['invitation_teacher_code'];
    role = json['role'];
    namaWalikelas = json['nama_walikelas'];
    nomorTelpWalikelas = json['nomor_telp_walikelas'];
    isSudahGabung = json['is_sudah_gabung'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['name'] = name;
    data['tahun_ajaran'] = tahunAjaran;
    data['semester'] = semester;
    data['jurusan'] = jurusan;
    data['tingkat'] = tingkat;
    data['invitation_student_code'] = invitationStudentCode;
    data['invitation_teacher_code'] = invitationTeacherCode;
    data['role'] = role;
    data['nama_walikelas'] = namaWalikelas;
    data['nomor_telp_walikelas'] = nomorTelpWalikelas;
    data['is_sudah_gabung'] = isSudahGabung;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class TeachersClassroomModel {
  int statusCode;
  String message;
  List<TeacherClassroomModel> data;

  TeachersClassroomModel({this.statusCode, this.message, this.data});

  TeachersClassroomModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <TeacherClassroomModel>[];
      json['data'].forEach((dynamic v) {
        data.add(TeacherClassroomModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TeacherClassroomModel {
  int userId;
  int classId;
  String fullName;
  String role;
  String noTelp;
  String email;
  String image;
  String createdAt;
  String updatedAt;

  TeacherClassroomModel(
      {this.userId,
      this.classId,
      this.fullName,
      this.role,
      this.noTelp,
      this.email,
      this.image,
      this.createdAt,
      this.updatedAt});

  TeacherClassroomModel.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    classId = json['class_id'];
    fullName = json['full_name'];
    role = json['role'];
    noTelp = json['no_telp'];
    email = json['email'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = userId;
    data['class_id'] = classId;
    data['full_name'] = fullName;
    data['role'] = role;
    data['no_telp'] = noTelp;
    data['email'] = email;
    data['image'] = image;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class StudentsClassroomModel {
  int statusCode;
  String message;
  List<StudentClassroomModel> data;

  StudentsClassroomModel({this.statusCode, this.message, this.data});

  StudentsClassroomModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <StudentClassroomModel>[];
      json['data'].forEach((dynamic v) {
        data.add(StudentClassroomModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class StudentClassroomModel {
  int userId;
  int classId;
  String fullName;
  String role;
  String noTelp;
  String email;
  String image;
  String createdAt;
  String updatedAt;
  String parentCode;
  bool codeIsUsed;
  String parentName;
  String parentEmail;
  String parentTelephone;
  int parentId;

  StudentClassroomModel(
      {this.userId,
      this.classId,
      this.fullName,
      this.role,
      this.noTelp,
      this.email,
      this.image,
      this.createdAt,
      this.updatedAt,
      this.parentCode,
      this.codeIsUsed,
      this.parentName,
      this.parentEmail,
      this.parentTelephone,
      this.parentId});

  StudentClassroomModel.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    classId = json['class_id'];
    fullName = json['full_name'];
    role = json['role'];
    noTelp = json['no_telp'];
    email = json['email'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    parentCode = json['parent_code'];
    codeIsUsed = json['code_is_used'];
    parentName = json['parent_name'];
    parentEmail = json['parent_email'];
    parentTelephone = json['parent_telephone'];
    parentId = json['parent_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = userId;
    data['class_id'] = classId;
    data['full_name'] = fullName;
    data['role'] = role;
    data['no_telp'] = noTelp;
    data['email'] = email;
    data['image'] = image;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['parent_code'] = parentCode;
    data['code_is_used'] = codeIsUsed;
    data['parent_name'] = parentName;
    data['parent_email'] = parentEmail;
    data['parent_telephone'] = parentTelephone;
    data['parent_id'] = parentId;
    return data;
  }
}
