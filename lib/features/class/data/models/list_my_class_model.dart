part of '_models.dart';

class ListMyClassModel extends Models {
  List<MyClassModel> list;

  ListMyClassModel({this.list});

  ListMyClassModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <MyClassModel>[];
      json['data'].forEach((dynamic v) {
        list.add(MyClassModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
