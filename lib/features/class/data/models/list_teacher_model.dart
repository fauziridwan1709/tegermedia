part of '_models.dart';

class ListTeacherModel extends Models {
  List<TeacherModel> list;

  ListTeacherModel({this.list});

  ListTeacherModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <TeacherModel>[];
      json['data'].forEach((dynamic v) {
        list.add(TeacherModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
