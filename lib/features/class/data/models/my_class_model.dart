part of '_models.dart';

class MyClassModel {
  int id;
  int schoolId;
  String schoolName;
  String name;
  String tahunAjaran;
  String semester;
  String jurusan;
  String tingkat;
  String invitationStudentCode;
  String invitationTeacherCode;
  String role;
  String namaWalikelas;
  String nomorTelpWalikelas;
  bool isSudahGabung;
  int totalGuru;
  int totalSiswa;
  bool isApprove;
  String createdAt;
  String updatedAt;
  String description;
  int totalCourse;
  String googleClassRoomId;
  List<StudentIdListModel> studentIdListModel;

  MyClassModel(
      {this.id,
      this.schoolId,
      this.schoolName,
      this.name,
      this.tahunAjaran,
      this.semester,
      this.jurusan,
      this.tingkat,
      this.invitationStudentCode,
      this.invitationTeacherCode,
      this.role,
      this.namaWalikelas,
      this.nomorTelpWalikelas,
      this.isSudahGabung,
      this.totalGuru,
      this.totalSiswa,
      this.isApprove,
      this.createdAt,
      this.updatedAt,
      this.description,
      this.totalCourse,
      this.googleClassRoomId,
      this.studentIdListModel});

  MyClassModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    name = json['name'];
    tahunAjaran = json['tahun_ajaran'];
    semester = json['semester'];
    jurusan = json['jurusan'];
    tingkat = json['tingkat'];
    invitationStudentCode = json['invitation_student_code'];
    invitationTeacherCode = json['invitation_teacher_code'];
    role = json['role'];
    namaWalikelas = json['nama_walikelas'];
    nomorTelpWalikelas = json['nomor_telp_walikelas'];
    isSudahGabung = json['is_sudah_gabung'];
    totalGuru = json['total_guru'];
    totalSiswa = json['total_siswa'];
    isApprove = json['is_approve'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    description = json['description'];
    totalCourse = json['total_course'];
    googleClassRoomId = json['google_class_room_id'];
    if (json['student_id_list'] != null) {
      studentIdListModel = <StudentIdListModel>[];
      json['student_id_list'].forEach((dynamic v) {
        studentIdListModel
            .add(StudentIdListModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['name'] = name;
    data['tahun_ajaran'] = tahunAjaran;
    data['semester'] = semester;
    data['jurusan'] = jurusan;
    data['tingkat'] = tingkat;
    data['invitation_student_code'] = invitationStudentCode;
    data['invitation_teacher_code'] = invitationTeacherCode;
    data['role'] = role;
    data['nama_walikelas'] = namaWalikelas;
    data['nomor_telp_walikelas'] = nomorTelpWalikelas;
    data['is_sudah_gabung'] = isSudahGabung;
    data['total_guru'] = totalGuru;
    data['total_siswa'] = totalSiswa;
    data['is_approve'] = isApprove;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['description'] = description;
    data['total_course'] = totalCourse;
    data['google_class_room_id'] = googleClassRoomId;
    data['student_id_list'] = studentIdListModel;
    if (studentIdListModel != null) {
      data['student_id_list'] =
          studentIdListModel.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
