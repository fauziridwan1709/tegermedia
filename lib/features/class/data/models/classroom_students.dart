part of '_models.dart';

class ClassroomStudents {
  ClassroomStudentsData _data;
  Null _meta;

  ClassroomStudents({ClassroomStudentsData data, Null meta}) {
    _data = data;
    _meta = meta;
  }

  ClassroomStudentsData get getData => _data;
  set data(ClassroomStudentsData data) => _data = data;
  Null get getMeta => _meta;
  set meta(Null meta) => _meta = meta;

  ClassroomStudents.fromJson(Map<String, dynamic> json) {
    _data = json['data'] != null ? ClassroomStudentsData.fromJson(json['data']) : null;
    _meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    data['meta'] = _meta;
    return data;
  }
}

class ClassroomStudentsData {
  List<GoogleClassroomStudents> _students;

  ClassroomStudentsData({List<GoogleClassroomStudents> students}) {
    _students = students;
  }

  List<GoogleClassroomStudents> get getStudents => _students;
  set students(List<GoogleClassroomStudents> students) => _students = students;

  ClassroomStudentsData.fromJson(Map<String, dynamic> json) {
    if (json['students'] != null) {
      _students = <GoogleClassroomStudents>[];
      json['students'].forEach((dynamic v) {
        _students.add(GoogleClassroomStudents.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (_students != null) {
      data['students'] = _students.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GoogleClassroomStudents {
  String _courseId;
  Profile _profile;
  StudentWorkFolder _studentWorkFolder;
  String _userId;

  GoogleClassroomStudents(
      {String courseId, Profile profile, StudentWorkFolder studentWorkFolder, String userId}) {
    _courseId = courseId;
    _profile = profile;
    _studentWorkFolder = studentWorkFolder;
    _userId = userId;
  }

  String get getCourseId => _courseId;
  set courseId(String courseId) => _courseId = courseId;
  Profile get getProfile => _profile;
  set profile(Profile profile) => _profile = profile;
  StudentWorkFolder get getStudentWorkFolder => _studentWorkFolder;
  set studentWorkFolder(StudentWorkFolder studentWorkFolder) =>
      _studentWorkFolder = studentWorkFolder;
  String get getUserId => _userId;
  set userId(String userId) => _userId = userId;

  GoogleClassroomStudents.fromJson(Map<String, dynamic> json) {
    _courseId = json['courseId'];
    _profile = json['profile'] != null ? Profile.fromJson(json['profile']) : null;
    _studentWorkFolder = json['studentWorkFolder'] != null
        ? StudentWorkFolder.fromJson(json['studentWorkFolder'])
        : null;
    _userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['courseId'] = _courseId;
    if (_profile != null) {
      data['profile'] = _profile.toJson();
    }
    if (_studentWorkFolder != null) {
      data['studentWorkFolder'] = _studentWorkFolder.toJson();
    }
    data['userId'] = _userId;
    return data;
  }
}

class Profile {
  String _emailAddress;
  String _id;
  Name _name;
  List<Permissions> _permissions;
  String _photoUrl;

  Profile({String emailAddress, String id, Name name, String photoUrl}) {
    _emailAddress = emailAddress;
    _id = id;
    _name = name;
    _photoUrl = photoUrl;
  }

  String get getEmailAddress => _emailAddress;
  set emailAddress(String emailAddress) => _emailAddress = emailAddress;
  String get getId => _id;
  set id(String id) => _id = id;
  Name get getName => _name;
  set name(Name name) => _name = name;
  String get getPhotoUrl => _photoUrl;
  set photoUrl(String photoUrl) => _photoUrl = photoUrl;

  Profile.fromJson(Map<String, dynamic> json) {
    _emailAddress = json['emailAddress'];
    _id = json['id'];
    _name = json['name'] != null ? Name.fromJson(json['name']) : null;
    _photoUrl = json['photoUrl'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['emailAddress'] = _emailAddress;
    data['id'] = _id;
    if (_name != null) {
      data['name'] = _name.toJson();
    }
    data['photoUrl'] = _photoUrl;
    return data;
  }
}

class Name {
  String familyName;
  String fullName;
  String givenName;

  Name({this.familyName, this.fullName, this.givenName});

  Name.fromJson(Map<String, dynamic> json) {
    familyName = json['familyName'];
    fullName = json['fullName'];
    givenName = json['givenName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['familyName'] = familyName;
    data['fullName'] = fullName;
    data['givenName'] = givenName;
    return data;
  }
}

class StudentWorkFolder {
  String _alternateLink;
  String _id;
  String _title;

  StudentWorkFolder({String alternateLink, String id, String title}) {
    _alternateLink = alternateLink;
    _id = id;
    _title = title;
  }

  String get getAlternateLink => _alternateLink;
  set alternateLink(String alternateLink) => _alternateLink = alternateLink;
  String get getId => _id;
  set id(String id) => _id = id;
  String get getTitle => _title;
  set title(String title) => _title = title;

  StudentWorkFolder.fromJson(Map<String, dynamic> json) {
    _alternateLink = json['alternateLink'];
    _id = json['id'];
    _title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['alternateLink'] = _alternateLink;
    data['id'] = _id;
    data['title'] = _title;
    return data;
  }
}
