part of '_models.dart';

class StudentModel {
  int userId;
  int classId;
  String fullName;
  String role;
  String noTelp;
  String email;
  String image;
  String createdAt;
  String updatedAt;
  String parentCode;
  bool codeIsUsed;
  String parentName;
  String parentEmail;
  String parentTelephone;
  int parentId;
  StudentModel(
      {this.userId,
      this.classId,
      this.fullName,
      this.role,
      this.noTelp,
      this.email,
      this.image,
      this.createdAt,
      this.updatedAt,
      this.parentCode,
      this.codeIsUsed,
      this.parentName,
      this.parentEmail,
      this.parentTelephone,
      this.parentId});

  StudentModel.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    classId = json['class_id'];
    fullName = json['full_name'];
    role = json['role'];
    noTelp = json['no_telp'];
    email = json['email'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    parentCode = json['parent_code'];
    codeIsUsed = json['code_is_used'];
    parentName = json['parent_name'];
    parentEmail = json['parent_email'];
    parentTelephone = json['parent_telephone'];
    parentId = json['parent_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = userId;
    data['class_id'] = classId;
    data['full_name'] = fullName;
    data['role'] = role;
    data['no_telp'] = noTelp;
    data['email'] = email;
    data['image'] = image;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['parent_code'] = parentCode;
    data['code_is_used'] = codeIsUsed;
    data['parent_name'] = parentName;
    data['parent_email'] = parentEmail;
    data['parent_telephone'] = parentTelephone;
    data['parent_id'] = parentId;
    return data;
  }
}
