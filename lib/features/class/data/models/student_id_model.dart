part of '_models.dart';

class StudentIdListModel {
  int studentId;
  StudentIdListModel({this.studentId});

  StudentIdListModel.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['student_id'] = studentId;
    return data;
  }
}
