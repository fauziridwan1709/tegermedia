part of '_models.dart';

class TeacherModel {
  int userId;
  int classId;
  String fullName;
  String role;
  String noTelp;
  String email;
  String image;
  String createdAt;
  String updatedAt;

  TeacherModel(
      {this.userId,
      this.classId,
      this.fullName,
      this.role,
      this.noTelp,
      this.email,
      this.image,
      this.createdAt,
      this.updatedAt});

  TeacherModel.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    classId = json['class_id'];
    fullName = json['full_name'];
    role = json['role'];
    noTelp = json['no_telp'];
    email = json['email'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = userId;
    data['class_id'] = classId;
    data['full_name'] = fullName;
    data['role'] = role;
    data['no_telp'] = noTelp;
    data['email'] = email;
    data['image'] = image;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
