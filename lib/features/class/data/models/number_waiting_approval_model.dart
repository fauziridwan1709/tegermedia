part of '_models.dart';

class NumberWaitingApprovalModel {
  int count;

  NumberWaitingApprovalModel({this.count});

  NumberWaitingApprovalModel.fromJson(Map<String, dynamic> json) {
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['count'] = count;
    return data;
  }
}
