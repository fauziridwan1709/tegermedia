part of '_models.dart';

class InvitationClassModel {
  String invitationCode;
  String role;
  InvitationClassModel({this.invitationCode, this.role});

  InvitationClassModel.fromJson(Map<String, dynamic> json) {
    invitationCode = json['invitation_code'];
    role = json['role'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['invitation_code'] = invitationCode;
    data['role'] = role;
    return data;
  }
}
