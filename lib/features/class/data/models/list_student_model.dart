part of '_models.dart';

class ListStudentModel extends Models {
  List<StudentModel> list;

  ListStudentModel({this.list});

  ListStudentModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <StudentModel>[];
      json['data'].forEach((dynamic v) {
        list.add(StudentModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
