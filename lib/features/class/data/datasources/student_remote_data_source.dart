part of '_datasources.dart';

abstract class StudentRemoteDataSource {
  Future<Parsed<List<StudentModel>>> getAllStudent(int classId);
  Future<Parsed<ResultMessage>> kickStudent(int classId, UserIdModel id);
}

class StudentRemoteDataSourceImpl implements StudentRemoteDataSource {
  @override
  Future<Parsed<List<StudentModel>>> getAllStudent(int classId) async {
    var url = '$apiUrl/classes/$classId/students';
    var resp = await getIt(url);
    return resp.parse(ListStudentModel.fromJson(
            json.decode(resp.body) as Map<String, dynamic>)
        .list);
  }

  @override
  Future<Parsed<ResultMessage>> kickStudent(int classId, UserIdModel id) async {
    var url = '$apiUrl/member';
    var resp = await deleteIt(url);
    return resp.parse(ResultMessageModel());
  }
}
