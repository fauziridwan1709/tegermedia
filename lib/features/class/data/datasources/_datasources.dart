import 'dart:convert';

import 'package:tegarmedia/aw/entities/_entities.dart';
import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/features/class/data/models/_models.dart';
import 'package:tegarmedia/states/auth/_auth.dart';

part 'class_remote_data_source.dart';
part 'student_remote_data_source.dart';
part 'teacher_remote_data_source.dart';
