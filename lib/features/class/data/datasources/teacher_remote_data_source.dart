part of '_datasources.dart';

abstract class TeacherRemoteDataSource {
  Future<Parsed<List<TeacherModel>>> getAllTeacher(int classId);
  Future<Parsed<ResultMessage>> kickTeacher(int classId, UserIdModel id);
}

class TeacherRemoteDataSourceImpl implements TeacherRemoteDataSource {
  @override
  Future<Parsed<List<TeacherModel>>> getAllTeacher(int classId) async {
    var url = '$apiUrl/classes/$classId/teachers';
    var resp = await getIt(url);
    return resp.parse(ListTeacherModel.fromJson(
            json.decode(resp.body) as Map<String, dynamic>)
        .list);
  }

  @override
  Future<Parsed<ResultMessage>> kickTeacher(int classId, UserIdModel id) async {
    var url = '$apiUrl/member';
    var resp = await deleteIt(url);
    return resp.parse(ResultMessageModel());
  }
}
