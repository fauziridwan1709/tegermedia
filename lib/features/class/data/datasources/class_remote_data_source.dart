part of '_datasources.dart';

abstract class ClassRemoteDataSource {
  Future<Parsed<List<MyClassModel>>> getAllMyClass(int schoolId);
  Future<Parsed<MyClassModel>> getMyClass(int classId);
  Future<Parsed<ResultId>> createMyClass(MyClassModel model);
  Future<Parsed<ResultMessage>> updateMyClass(MyClassModel model);
  Future<Parsed<ResultMessage>> deleteMyClass(int id);
  Future<Parsed<ResultMessage>> changeWaliKelas(int classId, UserIdModel id);
  Future<Parsed<ResultMessage>> joinClass(InvitationClassModel model);
  Future<Parsed<NumberWaitingApprovalModel>> getCountWaitingApproval(
      int schoolId);
}

class ClassRemoteDataSourceImpl implements ClassRemoteDataSource {
  @override
  Future<Parsed<List<MyClassModel>>> getAllMyClass(int schoolId) async {
    var onlyMe = !GS.state<AuthState>().currentState.isAdmin;
    // var onlyMe = false;
    var url = '$apiUrl/schools/$schoolId/classes?onlyme=$onlyMe&limit=1000';
    var resp = await getIt(url);
    var data = resp.parse(ListMyClassModel.fromJson(resp.bodyAsMap).list);
    return data;
  }

  @override
  Future<Parsed<MyClassModel>> getMyClass(int classId) async {
    var url = '$apiUrl/classes';
    var resp = await getIt(url);
    return resp.parse(MyClassModel.fromJson(resp.bodyAsMap));
  }

  @override
  Future<Parsed<ResultId>> createMyClass(MyClassModel model) async {
    var url = '$apiUrl/classes';
    var resp = await postIt(url, model.toJson());
    return resp.parse(ResultIdModel.fromJson(resp.bodyAsMap));
  }

  @override
  Future<Parsed<ResultMessage>> deleteMyClass(int id) async {
    var url = '$apiUrl/classes/$id';
    var resp = await deleteIt(url);
    return resp.parse(ResultMessageModel());
  }

  @override
  Future<Parsed<ResultMessage>> updateMyClass(MyClassModel model) async {
    var url = '$apiUrl/classes/${model.id}';
    var resp = await putIt(url, model.toJson());
    return resp.parse(ResultMessageModel());
  }

  @override
  Future<Parsed<ResultMessage>> changeWaliKelas(
      int classId, UserIdModel id) async {
    var url = '$apiUrl/classes/$classId';
    var resp = await putIt(url, id.toJson());
    return resp.parse(ResultMessageModel());
  }

  @override
  Future<Parsed<ResultMessage>> joinClass(InvitationClassModel model) async {
    var url = '$apiUrl/join-classes';
    var resp = await postIt(url, model.toJson());
    return resp.parse(ResultMessageModel());
  }

  @override
  Future<Parsed<NumberWaitingApprovalModel>> getCountWaitingApproval(
      int schoolId) async {
    var url = '$apiUrl/schools/$schoolId/approval-count';
    var resp = await getIt(url);
    return resp.parse(NumberWaitingApprovalModel.fromJson(resp.bodyAsMap));
  }
}
