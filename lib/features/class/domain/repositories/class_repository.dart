part of '_repositories.dart';

abstract class ClassRepository {
  Future<Decide<Failure, Parsed<List<MyClassModel>>>> getAllMyClass(
      int schoolId);
  Future<Decide<Failure, Parsed<MyClassModel>>> getMyClass(int classId);
  Future<Decide<Failure, Parsed<ResultId>>> createMyClass(MyClassModel model);
  Future<Decide<Failure, Parsed<ResultMessage>>> updateMyClass(
      MyClassModel model);
  Future<Decide<Failure, Parsed<ResultMessage>>> deleteMyClass(int id);
  Future<Decide<Failure, Parsed<ResultMessage>>> changeWaliKelas(
      int classId, UserId id);
  Future<Decide<Failure, Parsed<ResultMessage>>> joinClass(
      InvitationClassModel model);
  Future<Decide<Failure, Parsed<NumberWaitingApprovalModel>>>
      getCountWaitingApproval(int schoolId);
}
