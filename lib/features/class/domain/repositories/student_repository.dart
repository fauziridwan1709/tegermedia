part of '_repositories.dart';

abstract class StudentRepository {
  Future<Decide<Failure, Parsed<List<StudentModel>>>> getAllStudent(
      int classId);
  Future<Decide<Failure, Parsed<ResultMessage>>> kickStudent(
      int classId, UserIdModel id);
}
