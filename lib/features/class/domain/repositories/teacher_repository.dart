part of '_repositories.dart';

abstract class TeacherRepository {
  Future<Decide<Failure, Parsed<List<TeacherModel>>>> getAllTeacher(
      int classId);
  Future<Decide<Failure, Parsed<ResultMessage>>> kickTeacher(
      int classId, UserIdModel id);
}
