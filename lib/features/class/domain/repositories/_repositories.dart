import 'package:tegarmedia/aw/entities/_entities.dart';
import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/class/data/models/_models.dart';

part 'class_repository.dart';
part 'student_repository.dart';
part 'teacher_repository.dart';
