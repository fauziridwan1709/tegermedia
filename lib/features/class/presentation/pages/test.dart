part of '_pages.dart';

class ChangeClass extends StatefulWidget {
  @override
  _ChangeClassState createState() => _ChangeClassState();
}

class _ChangeClassState extends BaseState<ChangeClass, ClassState> {
  @override
  void init() {
    //todo
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return Container();
  }

  @override
  Widget buildNarrowLayout(BuildContext context,
      ReactiveModel<ClassState> snapshot, SizingInformation sizeInfo) {
    return SizedBox();
  }

  @override
  Widget buildWideLayout(BuildContext context,
      ReactiveModel<ClassState> snapshot, SizingInformation sizeInfo) {
    return RefreshIndicator(
      onRefresh: () {
        var start = 2;
        return;
      },
      child: WhenRebuilder<ClassState>(
        onIdle: () => Container(),
        onError: (dynamic data) => Container(),
        onWaiting: () => Container(),
        onData: (data) => Container(),
      ),
    );
  }

  @override
  Future<void> retrieveData() async {
    await GS.setState<ClassState>((s) => s.retrieveData(4));
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  @override
  ScaffoldAttribute buildAttribute() {
    // TODO: implement buildAttribute
    return ScaffoldAttribute();
  }
}
