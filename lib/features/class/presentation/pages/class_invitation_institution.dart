part of '_pages.dart';
//
// class InvitationClassInstansi extends StatefulWidget {
//   final String role;
//   final bool navigateFromProfile;
//   InvitationClassInstansi(
//       {@required this.role, this.navigateFromProfile = true});
//
//   @override
//   _InvitationClassInstansiState createState() =>
//       _InvitationClassInstansiState();
// }
//
// class _InvitationClassInstansiState extends State<InvitationClassInstansi> {
//   final authState = Injector.getAsReactive<AuthState>();
//   var scaffoldKey = GlobalKey<ScaffoldState>();
//   var name = TextEditingController();
//   var fNode = FocusNode();
//   bool isLoading = false;
//
//   var descOrtu =
//       'Gabung kelas dengan menggunakan\nkode wali murid yang sudah Anda\ndapatkan, dan salin ke kotak dibawah ini';
//   var descSiswa =
//       'Gabung kelas dengan menggunakan\nkode kelas yang sudah Anda\ndapatkan, dan salin ke kotak dibawah ini';
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var orientation = MediaQuery.of(context).orientation;
//     var isPortrait = orientation == Orientation.portrait;
//     return Scaffold(
//         key: scaffoldKey,
//         appBar: AppBar(
//           backgroundColor: Colors.white,
//           shadowColor: Colors.transparent,
//           centerTitle: true,
//           title: TitleAppbar(label: 'Join Kelasku'),
//           leading: IconButton(
//               icon: Icon(
//                 Icons.arrow_back,
//                 color: SiswamediaTheme.green,
//               ),
//               onPressed: () {
//                 if (fNode.hasFocus) {
//                   return fNode.unfocus();
//                 } else {
//                   return Navigator.pop(context);
//                 }
//
//                 // if (!widget.navigateFromProfile) {
//                 //   Navigator.pop(context);
//                 // } else {
//                 //   if (widget.role == 'GURU') {
//                 //     Navigator.pushReplacement(
//                 //       context,
//                 //       MaterialPageRoute<void>(
//                 //         builder: (context) =>
//                 //             SelectMethodJoinClass(role: 'GURU'),
//                 //       ),
//                 //     );
//                 //   } else {
//                 //     Navigator.pushReplacement(
//                 //       context,
//                 //       MaterialPageRoute<void>(
//                 //         builder: (context) =>
//                 //             SelectMethodJoinClass(role: 'SISWA'),
//                 //       ),
//                 //     );
//                 //   }
//                 // }
//               }),
//         ),
//         body: Center(
//           child: Container(
//             width: S.w,
//             child: Padding(
//               padding: EdgeInsets.symmetric(horizontal: S.w * .1),
//               child: SingleChildScrollView(
//                 child: Column(
//                   children: [
//                     SizedBox(height: isPortrait ? S.w * .3 : 10.0),
//                     Text(widget.role == 'ORTU' ? descOrtu : descSiswa,
//                         textAlign: TextAlign.center),
//                     SizedBox(height: 18),
//                     Container(
//                       height: 60,
//                       width: S.w,
//                       child: TextField(
//                         focusNode: fNode,
//                         controller: name,
//                         style: semiBlack,
//                         decoration: InputDecoration(
//                           hintText: widget.role == 'ORTU'
//                               ? 'Masukkan kode wali murid.. '
//                               : 'Masukkan kode kelas.. ',
//                           hintStyle: descBlack.copyWith(
//                               fontSize: S.w / 30, color: Color(0xffB9B9B9)),
//                           contentPadding: EdgeInsets.symmetric(
//                               vertical: 5.0, horizontal: 25),
//                           enabledBorder: OutlineInputBorder(
//                             borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                             borderRadius: BorderRadius.circular(S.w),
//                           ),
//                           focusedBorder: OutlineInputBorder(
//                             borderSide:
//                                 BorderSide(color: SiswamediaTheme.green),
//                             borderRadius: BorderRadius.circular(S.w),
//                           ),
//                         ),
//                       ),
//                     ),
//                     SizedBox(height: 30),
//                     if (!isLoading)
//                       Column(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         children: [
//                           SContainer(
//                               onTap: validate,
//                               width: S.w * .7,
//                               height: S.w * .125,
//                               color: SiswamediaTheme.green,
//                               border: Border.all(color: SiswamediaTheme.green),
//                               radius: BorderRadius.circular(10),
//                               child: Center(
//                                   child: Text('Lanjutkan',
//                                       style: TextStyle(
//                                           color: SiswamediaTheme.white)))),
//                           SizedBox(
//                             height: 15,
//                           ),
//                           SContainer(
//                               width: S.w * .7,
//                               height: S.w * .125,
//                               color: Color(0xFF4B4B4B),
//                               radius: radius(10),
//                               onTap: () => pop(context),
//                               child: InkWell(
//                                 onTap: () {
//                                   Navigator.pop(context);
//                                 },
//                                 child: Center(
//                                     child: Text('Batal',
//                                         style: TextStyle(
//                                             color: SiswamediaTheme.white))),
//                               )),
//                         ],
//                       ),
//                     if (isLoading)
//                       Container(
//                           height: 30,
//                           width: 30,
//                           child: CircularProgressIndicator(
//                               valueColor: AlwaysStoppedAnimation<Color>(
//                                   Colors.grey[300]),
//                               backgroundColor: Colors.white)),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ));
//   }
//
//   Future<void> validate() async {
//     if (name.text == '') {
//       CustomFlushBar.errorFlushBar(
//         'Kode Kelas tidak boleh kosong!',
//         context,
//       );
//     } else {
//       setState(() => isLoading = true);
//       var result = await GS
//           .state<ClassState>()
//           .joinClass(InvitationClassModel(code: name.text, role: widget.role));
//       result.fold((failure) {
//         pop(context);
//         setState(() => isLoading = false);
//         CustomFlushBar.errorFlushBar(failure.translate(), context);
//       }, (result) {
//         result.statusCode.translate(() async {
//           setState(() => isLoading = false);
//           pop(context);
//           await GS.school().setState((s) => s.retrieveData());
//           CustomFlushBar.successFlushBar(result.message, context);
//           if (widget.navigateFromProfile) {
//             pop(context);
//           } else {
//             var schoolId = GS.state<AuthState>().currentState.schoolId;
//             await GS.classes().setState((s) => s.retrieveData(schoolId));
//           }
//         }, () {
//           setState(() => isLoading = false);
//           CustomFlushBar.errorFlushBar(result.message, context);
//         });
//       });
//     }
//   }
//
//   void dialog(BuildContext context, int id) {
//     showDialog<void>(
//         context: context,
//         builder: (context) => StatefulBuilder(builder: (BuildContext context,
//                 void Function(void Function()) setState) {
//               return Dialog(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(12)),
//                   child: Container(
//                       height: 233,
//                       width: 250,
//                       padding: EdgeInsets.symmetric(
//                           vertical: S.w * .05, horizontal: S.w * .05),
//                       child: Column(
//                         // crossAxisAlignment: CrossAxisAlignment.center,
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Container(
//                             width: S.w - 48 * 2,
//                             height: 100,
//                             decoration: BoxDecoration(
//                                 image: DecorationImage(
//                                     image: AssetImage(
//                                         'assets/icons/kelas/confirmation.png'))),
//                           ),
//                           SizedBox(height: 15),
//                           Text('Pendaftaran Berhasil',
//                               style: TextStyle(fontSize: 14)),
//                           SizedBox(height: 15),
//                           InkWell(
//                             onTap: () {
//                               Navigator.pushReplacement(
//                                 context,
//                                 MaterialPageRoute<void>(
//                                   builder: (context) => ListClassOnlyme(id: id),
//                                 ),
//                               );
//                             },
//                             child: Container(
//                                 height: 35,
//                                 width: 105,
//                                 decoration: BoxDecoration(
//                                     color: SiswamediaTheme.green,
//                                     borderRadius: BorderRadius.circular(4)),
//                                 child: Center(
//                                     child: Text('Yeay',
//                                         style:
//                                             TextStyle(color: Colors.white)))),
//                           ),
//                         ],
//                       )));
//             }));
//   }
// }
