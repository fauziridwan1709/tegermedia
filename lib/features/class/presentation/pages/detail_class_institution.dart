part of '_pages.dart';
//
// class DetailClassInstansi extends StatefulWidget {
//   final int id;
//
//   DetailClassInstansi({@required this.id});
//
//   @override
//   _DetailClassInstansiState createState() => _DetailClassInstansiState();
// }
//
// class _DetailClassInstansiState extends State<DetailClassInstansi>
// // BaseStateReBuilder<DetailClassInstansi, DetailClassState>
// {
//   final authState = GlobalState.auth();
//   final detailClass = GlobalState.detailClass();
//   var scaffoldKey = GlobalKey<ScaffoldState>();
//   var prefs = SharedPreferences.getInstance();
//   List<DataListTeachers> teacher = [];
//
//   // @override
//   var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
//   String teacherUserId = '';
//   ModelDetailClassId data;
//   bool isLoading = true;
//   bool _visible;
//   var infoShare =
//       'Hi kabar baik, kelas kita sudah bergabung dengan Siswamedia, sekarang belajar jadi gampang, unduh siswamedia di Google Play Store atau melalui link ini : \nhttp://bit.ly/aplikasimerdekabelajar\n';
//
//   @override
//   void initState() {
//     super.initState();
//     _getTeachers();
//     _visible = true;
//     if (!detailClass.state.detailClassMap.containsKey(widget.id)) {
//       detailClass.resetToIsWaiting();
//     }
//     Initializer(
//       rIndicator: refreshIndicatorKey,
//       reactiveModel: detailClass,
//       state: detailClass.state.detailClassMap.containsKey(widget.id),
//       cacheKey: '$TIME_DETAIL_CLASS${widget.id}',
//     )..initialize();
//   }
//
//   void _getTeachers() async {
//     var dataTeachers =
//         await TeacherServices.getListTeachers(classId: widget.id);
//
//     if (dataTeachers.statusCode == 200) {
//       setState(() {
//         teacher = dataTeachers.data;
//       });
//     } else {
//       setState(() {
//         teacher = [];
//       });
//     }
//   }
//
//   Future<void> retrieveData() async {
//     await detailClass.setState((s) => s.getDetailClass(widget.id),
//         onData: (context, data) => setState(
//               () => _visible = false,
//             ),
//         onRebuildState: (_) => setState(() => _visible = true));
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         key: scaffoldKey,
//         appBar: AppBar(
//           backgroundColor: Colors.white,
//           shadowColor: Colors.transparent,
//           centerTitle: true,
//           title: TitleAppbar(label: 'Detail Kelas'),
//           leading: IconButton(
//               icon: Icon(
//                 Icons.arrow_back,
//                 color: SiswamediaTheme.green,
//               ),
//               onPressed: () async {
//                 Navigator.pop(context);
//                 //todo penting
//                 // await classProvider.resetTeachers();
//               }),
//         ),
//         body: StateBuilder<DetailClassState>(
//             observe: () => detailClass,
//             initState: null,
//             builder: (context, _) {
//               return RefreshIndicator(
//                 onRefresh: retrieveData,
//                 key: refreshIndicatorKey,
//                 child: WhenRebuilder<DetailClassState>(
//                     observe: () => detailClass,
//                     onIdle: () => WaitingView(),
//                     onWaiting: () => WaitingView(),
//                     onError: (dynamic error) => ErrorView(error: error),
//                     onData: (data) {
//                       // return detail(context);
//                       return AnimatedOpacity(
//                           opacity: _visible ? 1.0 : 0.0,
//                           duration: Duration(milliseconds: 450),
//                           child: detail(context));
//                     }),
//               );
//             }));
//   }
//
//   Widget detail(BuildContext context) {
//     var isAdmin = authState.state.currentState.isAdmin;
//     var role = authState.state.selectedClassRole.isWaliKelas || isAdmin;
//     var data = detailClass.state.detailClassMap[widget.id];
//     vUtils.setLog(data.id);
//     print(role);
//     return Injector(
//         inject: [Inject(() => StudentState()), Inject(() => TeacherState())],
//         builder: (_) {
//           return ListView(
//             children: [
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
//                 child: Column(
//                   children: [
//                     CustomText(
//                         authState.state.currentState.schoolName, Kind.heading1),
//                     Image.asset('assets/icons/kelas/kelasku.png',
//                         height: S.w * .6, width: S.w * .7),
//                     SizedBox(height: 12),
//                     Text(
//                       data.name,
//                       textAlign: TextAlign.center,
//                       style: semiBlack.copyWith(fontSize: S.w / 26),
//                     ),
//                     SizedBox(height: 12),
//                     Text(
//                       '${data.jurusan} / ${data.tahunAjaran}',
//                       textAlign: TextAlign.center,
//                       style: TextStyle(fontSize: 12),
//                     ),
//                     Text(
//                       'Wali Kelas : ${data.namaWalikelas}',
//                       textAlign: TextAlign.center,
//                       style: TextStyle(fontSize: 12),
//                     ),
//                     Text(
//                       'Kontak : ${data.nomorTelpWalikelas.isEmpty ? 'Tidak Ada' : data.nomorTelpWalikelas} ',
//                       textAlign: TextAlign.center,
//                       style: TextStyle(fontSize: 12),
//                     ),
//                     SizedBox(height: 16),
//                     if (role)
//                       Column(children: [
//                         InkWell(
//                             onTap: () {
//                               dialogDev(context);
//                             },
//                             child: Container(
//                                 width: 200,
//                                 height: 40,
//                                 decoration: BoxDecoration(
//                                     color: SiswamediaTheme.lightBlue,
//                                     borderRadius: BorderRadius.circular(10)),
//                                 child: Center(
//                                     child: Text(
//                                   'Ganti Wali Kelas',
//                                   style: semiWhite.copyWith(fontSize: S.w / 28),
//                                 )))),
//                         SizedBox(height: 16),
//                       ]),
//                     Divider(),
//                     SizedBox(height: 16),
//                     Row(
//                       children: [
//                         CustomText('Deskripsi', Kind.heading3),
//                       ],
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Row(
//                       children: [
//                         CustomText(
//                           (data.description != null &&
//                                   data.description.isNotEmpty)
//                               ? (data.description ?? '')
//                               : 'Tidak ada deskripsi kelas',
//                           Kind.descBlack,
//                           align: TextAlign.left,
//                         ),
//                       ],
//                     ),
//                     SizedBox(height: 16),
//                     Divider(),
//                     SizedBox(height: 16),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: listMenuDetailClass
//                           .map((e) => CardMenuUserClass(
//                                 title: getName(e),
//                                 count: getTotalCount(e, data),
//                                 image: 'assets/icons/kelas/$e.png',
//                                 onTap: () {
//                                   if (listMenuDetailClass.indexOf(e) == 0) {
//                                     navigate(
//                                         context,
//                                         ListCourseByClass(
//                                             schoolId: data.schoolId,
//                                             id: data.id,
//                                             role: authState
//                                                 .state.selectedClassRole,
//                                             classId: data.id));
//                                   } else if (listMenuDetailClass.indexOf(e) ==
//                                       1) {
//                                     navigate(context,
//                                         ListStudentClassroom(id: data.id));
//                                   } else if (listMenuDetailClass.indexOf(e) ==
//                                       2) {
//                                     navigate(context,
//                                         ListTeacherClassroom(id: data.id));
//                                   } else {
//                                     ///TODO
//                                   }
//                                 },
//                               ))
//                           .toList(),
//                     ),
//                     SizedBox(height: 16),
//                     Divider(),
//                     SizedBox(height: 20),
//                     if (!authState.state.selectedClassRole.isSiswaOrOrtu)
//                       Column(
//                         children: [
//                           Container(
//                               width: S.w,
//                               child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.center,
//                                 mainAxisSize: MainAxisSize.min,
//                                 children: [
//                                   if (role)
//                                     Row(
//                                       mainAxisSize: MainAxisSize.max,
//                                       children: [
//                                         SizedBox(
//                                             width: 60, child: Text('Guru')),
//                                         Expanded(
//                                             child: TextClipboard(
//                                                 data.invitationTeacherCode,
//                                                 onCopy: (text) {
//                                           Clipboard.setData(
//                                               ClipboardData(text: text));
//                                           CustomFlushBar.eventFlushBar(
//                                               'Kode berhasil dicopy!', context);
//                                         })),
//                                         IconButton(
//                                           icon: Icon(Icons.share),
//                                           onPressed: () => share(
//                                               context,
//                                               infoShare +
//                                                   'Lalu sebagai guru masukkan code kelas untuk guru kita :\n' +
//                                                   data.invitationTeacherCode,
//                                               'Kode Undang Guru'),
//                                         )
//                                       ],
//                                     ),
//                                   SizedBox(height: 10),
//                                   Row(
//                                     children: [
//                                       SizedBox(width: 60, child: Text('Siswa')),
//                                       Expanded(
//                                           child: TextClipboard(
//                                               data.invitationStudentCode,
//                                               onCopy: (text) {
//                                         Clipboard.setData(
//                                             ClipboardData(text: text));
//                                         CustomFlushBar.eventFlushBar(
//                                             'Kode berhasil dicopy!', context);
//                                       })),
//                                       //todo penting done
//                                       IconButton(
//                                         icon: Icon(Icons.share),
//                                         onPressed: () => share(
//                                             context,
//                                             infoShare +
//                                                 'Lalu sebagai siswa masukkan code kelas untuk siswa kita :\n' +
//                                                 data.invitationStudentCode,
//                                             'Kode Undang Siswa'),
//                                       )
//                                     ],
//                                   ),
//                                   SizedBox(height: 10),
//                                 ],
//                               )),
//                           Text(
//                             'Kode diatas bisa digunakan untuk\nmengundang guru dan siswa menjadi bagian\ndari kelas secara instan, tekan tombol hijau\nuntuk menyalin kode',
//                             textAlign: TextAlign.center,
//                             style: TextStyle(color: Color(0xFFB9B9B9)),
//                           )
//                         ],
//                       )
//                   ],
//                 ),
//               ),
//             ],
//           );
//         });
//   }
//
//   int getTotalCount(String v, DataDetailClassId data) {
//     if (v == listMenuDetailClass.elementAt(0)) {
//       return data.totalCourse;
//     } else if (v == listMenuDetailClass.elementAt(1)) {
//       return data.totalSiswa;
//     } else if (v == listMenuDetailClass.elementAt(2)) {
//       return data.totalGuru;
//     } else {
//       return -1;
//     }
//   }
//
//   String getName(String v) {
//     if (v == listMenuDetailClass.elementAt(0)) {
//       return 'Mata \nPelajaran';
//     } else if (v == listMenuDetailClass.elementAt(1)) {
//       return 'Data \nMurid';
//     } else if (v == listMenuDetailClass.elementAt(2)) {
//       return 'Data \nGuru';
//     } else {
//       return 'Data \nWali Murid';
//     }
//   }
//
//   void dialogDev(BuildContext context) {
//     showDialog<void>(
//         context: context,
//         builder: (context) => StatefulBuilder(builder: (BuildContext context,
//                 void Function(void Function()) setState) {
//               return Dialog(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(12)),
//                   child: Container(
//                       width: S.w * .5,
//                       padding: EdgeInsets.symmetric(
//                           vertical: S.w * .05, horizontal: S.w * .05),
//                       child: Column(
//                         mainAxisSize: MainAxisSize.min,
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Text('Ubah Wali Kelas',
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold, fontSize: 16)),
//                           SizedBox(
//                             height: 16,
//                           ),
//                           DropdownButtonFormField<String>(
//                             value: teacherUserId,
//                             icon: Icon(Icons.keyboard_arrow_down),
//                             iconSize: 24,
//                             elevation: 16,
//                             decoration: InputDecoration(
//                               contentPadding: EdgeInsets.symmetric(
//                                   vertical: 10.0, horizontal: 20),
//                               enabledBorder: OutlineInputBorder(
//                                 borderSide:
//                                     BorderSide(color: SiswamediaTheme.border),
//                                 borderRadius: BorderRadius.circular(23.0),
//                               ),
//                             ),
//                             style: TextStyle(color: SiswamediaTheme.green),
//                             onChanged: (String newValue) {
//                               setState(() {
//                                 teacherUserId = newValue;
//                               });
//                             },
//                             items: [
//                               DropdownMenuItem(
//                                 child: Text('Pilih Guru'),
//                                 value: '',
//                               ),
//                               for (DataListTeachers value in teacher)
//                                 DropdownMenuItem(
//                                   value: value.userId.toString(),
//                                   child: Text(value.fullName),
//                                 )
//                             ],
//                           ),
//                           SizedBox(
//                             height: 16,
//                           ),
//                           InkWell(
//                               onTap: () async {
//                                 var result =
//                                     await ClassServices.updateWaliClass(
//                                         reqBody: <String, dynamic>{
//                                       'user_id': int.parse(teacherUserId)
//                                     },
//                                         id: widget.id);
//                                 if (result.statusCode == 200 ||
//                                     result.statusCode == 201) {
//                                   Navigator.pop(context);
//
//                                   await prefs.then((value) {
//                                     ///string
//                                     var userId = value.getString('user_id');
//                                     if (userId != teacherUserId) {
//                                       if (authState
//                                               .state.currentState.classId ==
//                                           widget.id) {
//                                         authState.setState((s) => s
//                                             .setCurrentClass(
//                                                 className: authState.state
//                                                     .currentState.className,
//                                                 classId: authState
//                                                     .state.currentState.classId,
//                                                 classRole: 'GURU'));
//                                       }
//                                       authState.setState((s) =>
//                                           s.setSelectedClassRole('GURU'));
//                                     } else {
//                                       if (authState
//                                               .state.currentState.classId ==
//                                           widget.id) {
//                                         authState.setState((s) => s
//                                             .setCurrentClass(
//                                                 className: authState.state
//                                                     .currentState.className,
//                                                 classId: authState
//                                                     .state.currentState.classId,
//                                                 classRole: 'WALIKELAS'));
//                                       }
//                                       authState.setState((s) =>
//                                           s.setSelectedClassRole('WALIKELAS'));
//                                     }
//                                     return CustomFlushBar.successFlushBar(
//                                       '${teacher.firstWhere((element) => element.userId.toString() == teacherUserId).fullName} Berhasil Menjadi WaliKelas',
//                                       context,
//                                     );
//                                   });
//                                   //todo refresh the state
//                                   await detailClass.state
//                                       .getDetailClass(widget.id);
//                                   await detailClass.notify();
//                                   // await detailClass.setState((s) => s.getDetailClass(widget.id));
//                                   // _getClassId();
//                                 } else {
//                                   CustomFlushBar.errorFlushBar(
//                                     result.message,
//                                     context,
//                                   );
//                                 }
//                               },
//                               child: Container(
//                                   width: S.w,
//                                   height: 45,
//                                   decoration: BoxDecoration(
//                                       color: SiswamediaTheme.green,
//                                       borderRadius: BorderRadius.circular(23)),
//                                   child: Center(
//                                       child: Text('Ganti Wali Kelas',
//                                           style: TextStyle(
//                                               color: SiswamediaTheme.white))))),
//                           SizedBox(height: 10)
//                         ],
//                       )));
//             }));
//   }
// }
