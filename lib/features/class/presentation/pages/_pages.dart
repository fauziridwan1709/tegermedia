import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/aw/bases/_base.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/core/screen/_screen.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/features/class/presentation/states/_states.dart';
import 'package:tegarmedia/features/class/presentation/widgets/_widgets.dart';
import 'package:tegarmedia/states/auth/_auth.dart';

import '../../../../aw/widgets/_widgets.dart';

part 'change_class.dart';
part 'class_invitation_institution.dart';
part 'class_step_create.dart';
part 'create_class_by_school.dart';
part 'detail_class_institution.dart';
part 'list_my_class.dart';
part 'list_student.dart';
part 'list_teacher.dart';
part 'list_teacher_waiting_approval.dart';
part 'test.dart';
part 'tutorial_overlay.dart';
