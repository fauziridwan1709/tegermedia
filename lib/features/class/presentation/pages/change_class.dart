part of '_pages.dart';
//
// class ChangeClass extends StatefulWidget {
//   final int id;
//   final List<String> role;
//   final bool stateRouteRegister;
//   ChangeClass({@required this.id, this.role, this.stateRouteRegister = false});
//   @override
//   _ChangeClassState createState() => _ChangeClassState();
// }
//
// class _ChangeClassState extends BaseState<ChangeClass, ClassState> {
//   final classState = GS.classes();
//   final authState = GS.auth();
//
//   @override
//   var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
//
//   @override
//   void dispose() {
//     super.dispose();
//   }
//
//   @override
//   ScaffoldAttribute buildAttribute() {
//     return ScaffoldAttribute();
//   }
//
//   @override
//   Future<void> retrieveData() async {
//     await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
//       await GS.setState<ClassState>((s) => s.retrieveData(widget.id));
//     });
//   }
//
//   @override
//   Widget buildAppBar(BuildContext context) {
//     return SiswamediaAppBar(
//       backgroundColor: Colors.white,
//       shadowColor: Colors.transparent,
//       centerTitle: true,
//       title: Text('Ganti kelas', style: semiBlack.copyWith(fontSize: S.w / 22)),
//       context: context,
//       flexibleSpace: Container(
//         decoration: BoxDecoration(
//           gradient: LinearGradient(
//             colors: [Color(0xffB5D548), Colors.green],
//             begin: Alignment.topCenter,
//             end: Alignment(0, 1.5),
//           ),
//         ),
//       ),
//       leading: IconButton(
//           icon: Icon(
//             Icons.arrow_back,
//             color: Colors.white,
//           ),
//           onPressed: () {
//             Navigator.of(context).pop();
//           }),
//     );
//   }
//
//   @override
//   Widget buildBody(BuildContext context, ReactiveModel<ClassState> snapshot) {
//     return RefreshIndicator(
//         key: refreshIndicatorKey,
//         onRefresh: retrieveData,
//         child: WhenRebuilder<ClassState>(
//           observe: () => snapshot,
//           onWaiting: () => WaitingView(),
//           onError: (dynamic error) => ErrorView(error: error),
//           onIdle: () => WaitingView(),
//           onData: (data) {
//             //todo for radit
//             if (data.classMap.isEmpty) {
//               return ListView(children: [
//                 Container(
//                   margin: EdgeInsets.only(top: 16),
//                   child: ClassNotFound(
//                       label:
//                           '${authState.state.currentState.isAdmin ? '\nSekolah ini belum memiliki kelas, Silahkan tambah kelas kemudian Undang Guru dan Siswa' : 'Anda belum memiliki kelas'}'),
//                 ),
//               ]);
//             }
//             return ListView(
//               children: [
//                 Column(children: [
//                   Container(
//                     margin: EdgeInsets.only(top: 16),
//                     child: ListClass(
//                       tipe: 'change',
//                       data: classState.state.classMap.values.toList(),
//                     ),
//                   ),
//                 ]),
//                 SizedBox(height: 40),
//               ],
//             );
//           },
//         ));
//   }
//
//   @override
//   void init() {
//     // TODO: implement init
//   }
//
//   @override
//   Future<bool> onBackPressed() async {
//     pop(context);
//     return true;
//   }
// }
