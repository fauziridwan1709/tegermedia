part of '_pages.dart';
//
// class CreateClassBySchool extends StatefulWidget {
//   final int schoolId;
//   CreateClassBySchool({@required this.schoolId});
//
//   @override
//   _CreateClassBySchoolState createState() => _CreateClassBySchoolState();
// }
//
// class _CreateClassBySchoolState extends State<CreateClassBySchool> {
//   final authState = Injector.getAsReactive<AuthState>();
//   List<String> listSemester = ['I', 'II'];
//   List<int> kelas = List.generate(12, (index) => index);
//
//   TextEditingController name = TextEditingController(text: '');
//   TextEditingController jurusan = TextEditingController(text: '');
//   var nameNode = FocusNode();
//   var jurusanNode = FocusNode();
//   String tahunAjaran = '';
//   String schoolId = '';
//   String semester = '';
//   String tingkatKelas = '';
//
//   bool isLoading = false;
//   GlobalKey<ScaffoldState> _scaffoldKey;
//
//   @override
//   void initState() {
//     super.initState();
//     _scaffoldKey = GlobalKey<ScaffoldState>();
//   }
//
//   final fireStoreInstance = FirebaseFirestore.instance;
//
//   Future<void> createInstance({DocumentModel data}) async {
//     await fireStoreInstance.collection('document').add(<String, dynamic>{
//       'class_id': data.classId,
//       'document_list': data.document,
//       'name': data.name,
//       'type': data.type,
//       'school_id': data.schoolId,
//       'parent': data.parent
//     });
//   }
//
//   @override
//   void dispose() {
//     name.dispose();
//     jurusan.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () {
//         Navigator.pop(context);
//         return;
//       },
//       child: Scaffold(
//         key: _scaffoldKey,
//         appBar: AppBar(
//           backgroundColor: Colors.white,
//           shadowColor: Colors.transparent,
//           centerTitle: true,
//           title: TitleAppbar(label: 'Buat Kelas'),
//           leading: IconButton(
//               icon: Icon(
//                 Icons.arrow_back,
//                 color: SiswamediaTheme.green,
//               ),
//               onPressed: () {
//                 Navigator.pop(context);
//               }),
//         ),
//         body: GestureDetector(
//           onTap: () {
//             if (jurusanNode.hasFocus || nameNode.hasFocus) {
//               jurusanNode.unfocus();
//               nameNode.unfocus();
//             }
//           },
//           child: Container(
//             color: Colors.white,
//             padding: EdgeInsets.symmetric(horizontal: 30),
//             child: ListView(
//               children: <Widget>[
//                 SizedBox(height: defaultMargin),
//                 Column(
//                   children: <Widget>[
//                     DropdownButtonFormField<String>(
//                       value: tahunAjaran,
//                       icon: Icon(Icons.keyboard_arrow_down,
//                           color: SiswamediaTheme.green),
//                       iconSize: 24,
//                       elevation: 16,
//                       decoration: InputDecoration(
//                         contentPadding:
//                             EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                         enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                         focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: SiswamediaTheme.green),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                       ),
//                       style: descBlack.copyWith(
//                           fontSize: S.w / 30, color: Color(0xff4B4B4B)),
//                       onChanged: (String newValue) {
//                         setState(() {
//                           tahunAjaran = newValue;
//                         });
//                       },
//                       items: [
//                         DropdownMenuItem(
//                           child: Text('Tahun Ajaran'),
//                           value: '',
//                         ),
//                         for (String value in listTahunAjaran())
//                           DropdownMenuItem(
//                             value: value,
//                             child: Text(value),
//                           )
//                       ],
//                     ),
//                     SizedBox(
//                       height: 16,
//                     ),
//                     DropdownButtonFormField<String>(
//                       value: semester,
//                       icon: Icon(Icons.keyboard_arrow_down,
//                           color: SiswamediaTheme.green),
//                       iconSize: 24,
//                       elevation: 16,
//                       decoration: InputDecoration(
//                         contentPadding:
//                             EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                         enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                         focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: SiswamediaTheme.green),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                       ),
//                       style: descBlack.copyWith(
//                           fontSize: S.w / 30, color: Color(0xff4B4B4B)),
//                       onChanged: (String newValue) {
//                         setState(() => semester = newValue);
//                       },
//                       items: [
//                         DropdownMenuItem(
//                           child: Text('Semester'),
//                           value: '',
//                         ),
//                         for (String value in listSemester)
//                           DropdownMenuItem(
//                             value: value,
//                             child: Text(value),
//                           )
//                       ],
//                     ),
//                     SizedBox(
//                       height: 16,
//                     ),
//                     DropdownButtonFormField<String>(
//                       value: tingkatKelas,
//                       icon: Icon(Icons.keyboard_arrow_down,
//                           color: SiswamediaTheme.green),
//                       iconSize: 24,
//                       elevation: 16,
//                       decoration: InputDecoration(
//                         contentPadding:
//                             EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                         enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                         focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: SiswamediaTheme.green),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                       ),
//                       style: descBlack.copyWith(
//                           fontSize: S.w / 30, color: Color(0xff4B4B4B)),
//                       onChanged: (String newValue) {
//                         setState(() {
//                           tingkatKelas = newValue;
//                         });
//                       },
//                       items: [
//                         DropdownMenuItem(
//                           child: Text('Tingkat Kelas'),
//                           value: '',
//                         ),
//                         for (int value in kelas)
//                           DropdownMenuItem(
//                             value: (value + 1).toString(),
//                             child: Text((value + 1).toString()),
//                           )
//                       ],
//                     ),
//                     SizedBox(
//                       height: 16,
//                     ),
//                     TextField(
//                       controller: jurusan,
//                       focusNode: jurusanNode,
//                       style: descBlack.copyWith(fontSize: S.w / 30),
//                       decoration: InputDecoration(
//                         hintText: 'Jurusan',
//                         hintStyle: descBlack.copyWith(
//                             fontSize: S.w / 30, color: Color(0xffB9B9B9)),
//                         contentPadding:
//                             EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                         enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                         focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: SiswamediaTheme.green),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                       ),
//                     ),
//                     SizedBox(
//                       height: 16,
//                     ),
//                     TextField(
//                       controller: name,
//                       focusNode: nameNode,
//                       style: descBlack.copyWith(fontSize: S.w / 30),
//                       decoration: InputDecoration(
//                         hintText: 'Nama Kelas',
//                         hintStyle: descBlack.copyWith(
//                             fontSize: S.w / 30, color: Color(0xffB9B9B9)),
//                         contentPadding:
//                             EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                         enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                         focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: SiswamediaTheme.green),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 SizedBox(height: 20),
//                 GestureDetector(
//                   onTap: () async {
//                     if (name.text == '') {
//                       return CustomFlushBar.errorFlushBar(
//                         'Nama Kelas wajib di isi!',
//                         context,
//                       );
//                     }
//
//                     if (tahunAjaran == null || tahunAjaran == '') {
//                       return CustomFlushBar.errorFlushBar(
//                         'Tahun Ajaran wajib dipilih!',
//                         context,
//                       );
//                     }
//
//                     if (tingkatKelas == null || tingkatKelas == '') {
//                       return CustomFlushBar.errorFlushBar(
//                         'Tingkat Kelas wajib dipilih!',
//                         context,
//                       );
//                     }
//
//                     if (semester == null || semester == '') {
//                       return CustomFlushBar.errorFlushBar(
//                         'Semester wajib dipilih!',
//                         context,
//                       );
//                     }
//                     if (jurusan.text == '') {
//                       return CustomFlushBar.errorFlushBar(
//                         'Jurusan wajib dipilih!',
//                         context,
//                       );
//                     }
//                     confirmation(context);
//                   },
//                   child: Container(
//                     width: S.w * .7,
//                     height: S.w * .13,
//                     decoration: BoxDecoration(
//                         borderRadius: radius(100),
//                         color: SiswamediaTheme.green),
//                     padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
//                     child: Center(
//                         child: Text('Lanjutkan',
//                             style: TextStyle(
//                                 fontSize: S.w / 28,
//                                 color: Colors.white,
//                                 fontWeight: FontWeight.w600))),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   void confirmation(BuildContext context) {
//     showDialog<void>(
//         context: context,
//         builder: (context2) => StatefulBuilder(builder: (BuildContext context2,
//                 void Function(void Function()) setState) {
//               return Dialog(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(12)),
//                   child: Container(
//                       width: S.w * .5,
//                       padding: EdgeInsets.symmetric(
//                           vertical: S.w * .05, horizontal: S.w * .05),
//                       child: Column(
//                           mainAxisSize: MainAxisSize.min,
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           children: [
//                             Text('Konfirmasi',
//                                 style: TextStyle(
//                                     fontWeight: FontWeight.bold, fontSize: 16)),
//                             Text(
//                               'Apakah Anda yakin ingin menambahkan kelas ini ?',
//                               style: TextStyle(fontSize: 16),
//                               textAlign: TextAlign.center,
//                             ),
//                             SizedBox(
//                               height: 35,
//                             ),
//                             Row(
//                               children: [
//                                 Expanded(
//                                     child: InkWell(
//                                         onTap: () {
//                                           Navigator.pop(context);
//                                         },
//                                         child: Container(
//                                             height: 45,
//                                             decoration: BoxDecoration(
//                                                 color:
//                                                     SiswamediaTheme.nearlyBlack,
//                                                 borderRadius:
//                                                     BorderRadius.circular(5)),
//                                             child: Center(
//                                                 child: Text('Tidak',
//                                                     style: TextStyle(
//                                                         color: SiswamediaTheme
//                                                             .white)))))),
//                                 SizedBox(
//                                   width: 20,
//                                 ),
//                                 Expanded(
//                                     child: InkWell(
//                                         onTap: () async {
//                                           var result = await ClassServices
//                                               .createClassBySchool(
//                                                   reqBody: <String, dynamic>{
//                                                 'name': name.text.toString(),
//                                                 'tingkat': tingkatKelas,
//                                                 'tahun_ajaran': tahunAjaran,
//                                                 'semester': semester,
//                                                 'jurusan':
//                                                     jurusan.text.toString(),
//                                                 'school_id': widget.schoolId
//                                               });
//
//                                           if (result.statusCode == 200) {
//                                             Navigator.pop(context);
//                                             var reqBody = <String, dynamic>{
//                                               'name': name.text.toString(),
//                                               'mimeType':
//                                                   'application/vnd.google-apps.folder'
//                                             };
//
//                                             var reqBody2 = <String, dynamic>{
//                                               'role': 'reader',
//                                               'type': 'anyone'
//                                             };
//                                             dialogFolder(context);
//                                             await GoogleDrive.createDriveFolder(
//                                                     context: context,
//                                                     reqBody: reqBody,
//                                                     reqBody2: reqBody2)
//                                                 .then((value) async {
//                                               Navigator.pop(context);
//                                               var reqP = <String, dynamic>{
//                                                 'name': 'Informasi Kelas',
//                                                 'mimeType':
//                                                     'application/vnd.google-apps.folder',
//                                                 'parents': [value.id.toString()]
//                                               };
//                                               dialogFolder(context);
//                                               await GoogleDrive
//                                                   .createDriveFolder(
//                                                 context: context,
//                                                 reqBody: reqP,
//                                                 reqBody2: reqBody2,
//                                               ).then((value2) async {
//                                                 var getClassBySchoolId =
//                                                     await ClassServices
//                                                         .getClassBySchoolID(
//                                                             id: authState
//                                                                 .state
//                                                                 .currentState
//                                                                 .schoolId,
//                                                             param: true,
//                                                             cache: false);
//                                                 //todo penting
//                                                 await authState.state
//                                                     .fetchRole();
//
//                                                 var documentModel =
//                                                     DocumentModel(
//                                                         schoolId: authState
//                                                             .state
//                                                             .currentState
//                                                             .schoolId,
//                                                         classId: result.data.id,
//                                                         name: 'Informasi Kelas',
//                                                         document: value2.id,
//                                                         parent: value.id,
//                                                         type: 'kelas');
//                                                 await createInstance(
//                                                         data: documentModel)
//                                                     .then((value) {
//                                                   Navigator.pop(context);
//                                                   //todo fetch document
//                                                   dialog(context);
//                                                   Injector.getAsReactive<ClassState>().setState(
//                                                       (s) => s.retrieveData(
//                                                           data: ClassBySchoolModel(
//                                                               param: GetClassModel(
//                                                                   schoold: authState
//                                                                       .state
//                                                                       .currentState
//                                                                       .schoolId,
//                                                                   me: true))),
//                                                       silent: true);
//                                                 });
//                                               });
//                                             });
//                                           } else {
//                                             Navigator.pop(context);
//                                             CustomFlushBar.errorFlushBar(
//                                               result.message,
//                                               context,
//                                             );
//                                           }
//                                         },
//                                         child: Container(
//                                             height: 45,
//                                             decoration: BoxDecoration(
//                                                 color: SiswamediaTheme.green,
//                                                 borderRadius:
//                                                     BorderRadius.circular(5)),
//                                             child: Center(
//                                                 child: isLoading
//                                                     ? Container(
//                                                         height: 30,
//                                                         width: 30,
//                                                         child: CircularProgressIndicator(
//                                                             valueColor:
//                                                                 AlwaysStoppedAnimation<Color>(
//                                                                     Colors.grey[
//                                                                         300]),
//                                                             backgroundColor:
//                                                                 Colors.white))
//                                                     : Text('Ya',
//                                                         style: TextStyle(
//                                                             color:
//                                                                 SiswamediaTheme
//                                                                     .white)))))),
//                               ],
//                             )
//                           ])));
//             }));
//   }
//
//   void dialog(BuildContext context) {
//     showDialog<void>(
//         context: context,
//         barrierDismissible: false,
//         builder: (context2) => StatefulBuilder(builder: (BuildContext context2,
//                 void Function(void Function()) setState) {
//               return WillPopScope(
//                 onWillPop: () async => false,
//                 child: CustomDialog(
//                   image: 'assets/icons/kelas/confirmation.png',
//                   title: 'Berhasil',
//                   description:
//                       'Tambah Kelas Berhasil, Tekan untuk melihat list kelas terbaru Anda',
//                   actions: ['OK'],
//                   onTap: () async {
//                     var result = await ClassServices.detailSchool(
//                         id: authState.state.currentState.schoolId);
//                     //todo penting
//                     // await auth.setListRole(result.data.roles);
//                     Navigator.pop(context2);
//                     Navigator.pop(context2);
//                   },
//                 ),
//               );
//             }));
//   }
// }
//
// void dialogFolder(BuildContext context) {
//   showDialog<void>(
//       context: context,
//       barrierDismissible: false,
//       builder: (context2) => WillPopScope(
//             onWillPop: () async => false,
//             child: Dialog(
//                 shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(12)),
//                 child: Container(
//                     width: S.w / 2,
//                     padding: EdgeInsets.symmetric(
//                         horizontal: S.w * .05, vertical: S.w * .05),
//                     decoration: BoxDecoration(
//                       color: Colors.white,
//                       borderRadius: BorderRadius.circular(12),
//                     ),
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         Image.asset('assets/kelas/dokumen.png',
//                             height: S.w / 3, width: S.w / 3),
//                         Text('Sedang Membuat Folder...',
//                             textAlign: TextAlign.center,
//                             style: semiBlack.copyWith(fontSize: S.w / 26)),
//                         SizedBox(height: S.w * .05),
//                         Padding(
//                             padding:
//                                 EdgeInsets.symmetric(horizontal: S.w * .15),
//                             child: SpinKitSquareCircle(
//                               color: Colors.black54,
//                               size: 30,
//                             )),
//                       ],
//                     ))),
//           ));
// }
