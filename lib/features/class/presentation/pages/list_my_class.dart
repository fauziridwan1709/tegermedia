part of '_pages.dart';

///todos
///move the injector
///
class ListMyClass extends StatefulWidget {
  final int schoolId;
  final List<String> schoolRole;
  final bool stateRouteRegister;

  ListMyClass({@required this.schoolId, this.schoolRole, this.stateRouteRegister = false})
      : assert(schoolId != null, 'schoolId cannot be null');

  @override
  _ListMyClassState createState() => _ListMyClassState();
}

class _ListMyClassState extends BaseState<ListMyClass, ClassState> {
  final authState = Injector.getAsReactive<AuthState>();
  final classState = Injector.getAsReactive<ClassState>();
  final dialogKey = GlobalKey<ScaffoldState>();

  bool _visible;

  int _count;

  // @override
  // void initState() {
  //   super.initState();
  //   _visible = true;
  //   _getDetailSchool();
  //   _getCountWaitingApproval();
  // }
  //
  // @override
  // void dispose() {
  //   super.dispose();
  // }

  // void _getDetailSchool() async {
  //   var detailSchool = await ClassServices.detailSchool(
  //       id: authState.state.currentState.schoolId);
  //   if (detailSchool.statusCode == 200) {
  //     if (authState.state.currentState.schoolRole != null &&
  //         authState.state.currentState.schoolRole.isNotEmpty) {
  //       var data = UserCurrentState(
  //         schoolId: detailSchool.data.id,
  //         schoolRole: detailSchool.data.roles,
  //         schoolName: detailSchool.data.name,
  //         type: 'SEKOLAH',
  //         className: authState.state.currentState.className,
  //         classRole: authState.state.currentState.classRole,
  //         classId: authState.state.currentState.classId,
  //         isAdmin: authState.state.currentState.isAdmin,
  //       );
  //       await authState.setState((s) => s.changeCurrentState(state: data),
  //           silent: true);
  //     }
  //   }
  // }

  @override
  Future<void> retrieveData() async {
    if (classState.isWaiting) {
      setState(() => _visible = false);
    }
    await Future<void>.delayed(Duration(seconds: 1)).then((_) async {
      await classState.setState((s) async => await s.retrieveData(widget.schoolId),
          onData: (context, data) => setState(
                () => _visible = false,
              ),
          onRebuildState: (_) => setState(() => _visible = true),
          catchError: true,
          onError: (context, dynamic error) {});
    });
  }

  @override
  Widget buildAppBar(BuildContext context) {
    return SiswamediaAppBar2(
      context: context,
      backgroundColor: Colors.white,
      shadowColor: Colors.transparent,
      centerTitle: true,
      title: Text(
        'Daftar Kelas',
        style: boldBlack.copyWith(fontSize: 22, color: Colors.white),
      ),
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xffB5D548), SiswamediaTheme.green],
            begin: Alignment.topCenter,
            end: Alignment(0, 1.5),
          ),
        ),
      ),
      leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => pop(context)),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  var refreshIndicatorKey;

  @override
  void init() {
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      S().init(context);
    });
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<ClassState> snapshot, SizingInformation sizeInfo) {
    print('narrowLayout');
    return RefreshIndicator(
      key: refreshIndicatorKey,
      onRefresh: retrieveData,
      child: WhenRebuilder<ClassState>(
        observe: () => snapshot,
        onIdle: () => WaitingView(),
        onWaiting: () => WaitingView(),
        onError: (dynamic error) => ErrorView(error: error),
        onData: (data) {
          return AnimatedOpacity(
            opacity: 1.0,
            duration: Duration(milliseconds: 450),
            child: ListView(
              shrinkWrap: true,
              children: [
                data.classMap.isEmpty
                    ? ClassNotFound(
                        label:
                            '${authState.state.currentState.isAdmin ? '\n${authState.state.currentState.type == 'PERSONAL' ? 'Media' : 'Sekolah'} ini belum memiliki kelas, Silahkan tambah kelas kemudian Undang Guru dan Siswa' : 'Anda belum memiliki kelas'}')
                    : Container(
                        margin: EdgeInsets.only(top: 16),
                        child: ListClass(
                          data: data.classMap.values.toList(),
                          onTapClass: (myClass) {
                            authState.setState((s) => s.setSelectedClassRole(myClass.role));
                          },
                        ),
                      ),
                ButtonWidthCondition(
                    role: ['siswa'],
                    // authState: authState,
                    id: widget.schoolId,
                    dialogKey: dialogKey,
                    rKey: refreshIndicatorKey,
                    count: _count),
                SizedBox(height: 40),
              ],
            ),
          );
        },
      ),
    );
  }

  @override
  Widget buildWideLayout(
      BuildContext context, ReactiveModel<ClassState> snapshot, SizingInformation sizeInfo) {
    return SizedBox();
  }
}
