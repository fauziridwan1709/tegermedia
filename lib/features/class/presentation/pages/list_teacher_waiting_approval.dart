part of '_pages.dart';
//
// class ListTeachersWaitingApproval extends StatefulWidget {
//   final int schoolId;
//   final Function refreshCount;
//
//   ListTeachersWaitingApproval(
//       {@required this.schoolId, @required this.refreshCount});
//
//   @override
//   _ListTeachersWaitingApprovalState createState() =>
//       _ListTeachersWaitingApprovalState();
// }
//
// class _ListTeachersWaitingApprovalState
//     extends State<ListTeachersWaitingApproval> {
//   var scaffoldKey = GlobalKey<ScaffoldState>();
//   // List<DataSchoolApproval> _list = [];
//   bool _isLoading = false;
//
//   @override
//   void initState() {
//     super.initState();
//     _getListTeacherWaitingApproval();
//   }
//
//   Future<void> _getListTeacherWaitingApproval() async {
//     setState(() {
//       _isLoading = true;
//     });
//     var result =
//         await SchoolServices.getSchoolApproval(schoolID: widget.schoolId);
//     print(result.toJson());
//     if (result.statusCode == 200) {
//       widget.refreshCount();
//       setState(() {
//         _list = result.data;
//         _isLoading = false;
//       });
//     } else {
//       setState(() {
//         _list = [];
//         _isLoading = false;
//       });
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         key: scaffoldKey,
//         appBar: AppBar(
//             backgroundColor: Colors.white,
//             shadowColor: Colors.transparent,
//             centerTitle: true,
//             title: TitleAppbar(label: 'Menunggu Verifikasi'),
//             leading: IconButton(
//                 icon: Icon(
//                   Icons.arrow_back,
//                   color: SiswamediaTheme.green,
//                 ),
//                 onPressed: () {
//                   Navigator.pop(context);
//                 })),
//         body: ListView(
//           children: <Widget>[
//             SizedBox(height: defaultMargin),
//             _isLoading
//                 ? Column(
//                     children: List.generate(5, (index) => SkeletonScreen()),
//                   )
//                 : _list.isEmpty
//                     ? ClassNotFound(
//                         label:
//                             'Daftar Guru yang menunggu \nverifikasi tidak ada.',
//                       )
//                     : Column(
//                         children: List.generate(_list.length, (index) {
//                         var data = _list[index];
//                         return Container(
//                             margin: EdgeInsets.symmetric(
//                                 vertical: 5, horizontal: defaultMargin),
//                             width: MediaQuery.of(context).size.width,
//                             padding: EdgeInsets.symmetric(
//                                 horizontal: 16, vertical: 10),
//                             decoration: BoxDecoration(
//                                 boxShadow: [
//                                   BoxShadow(
//                                       color:
//                                           SiswamediaTheme.grey.withOpacity(0.2),
//                                       offset: const Offset(0.2, 0.2),
//                                       blurRadius: 3.0),
//                                 ],
//                                 color: Colors.white,
//                                 borderRadius: BorderRadius.circular(10),
//                                 border: Border.all(color: Color(0xFF00000029))),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 Expanded(
//                                     child: Column(
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.start,
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.center,
//                                         children: [
//                                       Text(data.fullName,
//                                           style: TextStyle(
//                                               fontSize: 16,
//                                               fontWeight: FontWeight.w600,
//                                               color: SiswamediaTheme.green)),
//                                       Text("Email : ${data.email}",
//                                           style: TextStyle(fontSize: 10)),
//                                     ])),
//                                 Row(
//                                   children: [
//                                     InkWell(
//                                       onTap: () {
//                                         _buildJoinSchool(
//                                             context, data.userId, false);
//                                       },
//                                       child: Container(
//                                         padding: EdgeInsets.all(5),
//                                         decoration: BoxDecoration(
//                                             color: SiswamediaTheme.green,
//                                             shape: BoxShape.circle),
//                                         child: Icon(Icons.check,
//                                             color: Colors.white, size: 22),
//                                       ),
//                                     ),
//                                     SizedBox(
//                                       width: 5,
//                                     ),
//                                     InkWell(
//                                       onTap: () {
//                                         _buildJoinSchool(
//                                             context, data.userId, true);
//                                       },
//                                       child: Container(
//                                         padding: EdgeInsets.all(5),
//                                         decoration: BoxDecoration(
//                                             color: Colors.red,
//                                             shape: BoxShape.circle),
//                                         child: Icon(Icons.close,
//                                             color: Colors.white, size: 22),
//                                       ),
//                                     ),
//                                   ],
//                                 )
//                               ],
//                             ));
//                       }))
//           ],
//         ));
//   }
//
//   void _buildJoinSchool(BuildContext context, int userId, bool isDelete) {
//     double width = MediaQuery.of(context).size.width;
//     showDialog<void>(
//         context: context,
//         builder: (context) => StatefulBuilder(builder: (BuildContext context,
//                 void Function(void Function()) setState) {
//               return Dialog(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(12)),
//                   child: Container(
//                       height: 230,
//                       width: width * .5,
//                       padding: EdgeInsets.symmetric(
//                           vertical: width * .05, horizontal: width * .05),
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Text("Konfirmasi",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold, fontSize: 16)),
//                           SizedBox(
//                             height: 35,
//                           ),
//                           Text(
//                               "Apakah Anda yakin guru ini ${isDelete ? "Tidak" : ""} terdaftar di sekolah ini ?",
//                               textAlign: TextAlign.center,
//                               style: TextStyle(fontSize: 12)),
//                           SizedBox(
//                             height: 16,
//                           ),
//                           Row(
//                             children: [
//                               isDelete
//                                   ? _buildActionDelete(
//                                       widget.schoolId,
//                                       userId,
//                                     )
//                                   : _buildActionAccept(widget.schoolId, userId),
//                               Spacer(),
//                               InkWell(
//                                   onTap: () => Navigator.of(context).pop(),
//                                   child: Container(
//                                       height: 45,
//                                       width: 110,
//                                       decoration: BoxDecoration(
//                                           color: SiswamediaTheme.nearlyBlack,
//                                           borderRadius:
//                                               BorderRadius.circular(10)),
//                                       child: Center(
//                                           child: Text("Tidak",
//                                               style: TextStyle(
//                                                   color:
//                                                       SiswamediaTheme.white)))))
//                             ],
//                           )
//                         ],
//                       )));
//             }));
//   }
//
//   InkWell _buildActionDelete(int schoolId, int userId) {
//     return InkWell(
//         onTap: () async {
//           var data = await SchoolServices.rejectRequestJoinTeacher(
//               schoolID: widget.schoolId, userId: userId);
//
//           if (data.statusCode == 200) {
//             await _getListTeacherWaitingApproval();
//             Navigator.pop(context);
//           } else {
//             CustomFlushBar.errorFlushBar(
//               data.message,
//               context,
//             );
//           }
//         },
//         child: Container(
//             height: 45,
//             width: 115,
//             decoration: BoxDecoration(
//                 color: SiswamediaTheme.green,
//                 borderRadius: BorderRadius.circular(10)),
//             child: Center(
//                 child: Text("Ya",
//                     style: TextStyle(color: SiswamediaTheme.white)))));
//   }
//
//   InkWell _buildActionAccept(int schoolId, int userId) {
//     return InkWell(
//         onTap: () async {
//           var data = await SchoolServices.approvalTeachers(
//               schoolID: widget.schoolId, userId: userId);
//
//           if (data.statusCode == 200) {
//             await _getListTeacherWaitingApproval();
//             Navigator.pop(context);
//           } else {
//             CustomFlushBar.errorFlushBar(
//               data.message,
//               context,
//             );
//           }
//         },
//         child: Container(
//             height: 45,
//             width: 115,
//             decoration: BoxDecoration(
//                 color: SiswamediaTheme.green,
//                 borderRadius: BorderRadius.circular(10)),
//             child: Center(
//                 child: Text('Ya',
//                     style: TextStyle(color: SiswamediaTheme.white)))));
//   }
// }
