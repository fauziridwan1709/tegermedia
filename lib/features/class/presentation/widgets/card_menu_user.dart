part of '_widgets.dart';

class CardMenuUserClass extends StatelessWidget {
  final int count;
  final String image;
  final String title;
  final VoidCallback onTap;

  CardMenuUserClass({this.count, this.image, this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    var backgroundColor = Color(0xFFF6F6F6);
    var countColor = Color(0xFFFC7A7A);

    return Stack(
      children: [
        Column(
          children: [
            InkWell(
              onTap: onTap,
              child: Material(
                color: Colors.transparent,
                child: Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: radius(8),
                  ),
                  child:
                      Image.asset(image, height: S.w * .08, width: S.w * .08),
                ),
              ),
            ),
            SizedBox(height: 5),
            SizedBox(
              width: S.w * .2,
              child: Center(
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: descBlack.copyWith(
                      color: Colors.black38, fontSize: S.w / 32),
                ),
              ),
            )
          ],
        ),
        if (count != -1)
          Positioned(
            top: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: countColor,
                shape: BoxShape.circle,
              ),
              child: Center(
                child: Text(
                  '$count',
                  style: semiWhite.copyWith(fontSize: S.w / 40),
                ),
              ),
            ),
          )
      ],
    );
  }
}
