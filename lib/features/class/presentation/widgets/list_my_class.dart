part of '_widgets.dart';

class ListClass extends StatelessWidget {
  final String tipe;
  final List<MyClassModel> data;
  final VoidCallback onTap;
  final Function(MyClassModel e) onTapClass;

  ListClass({this.data, this.tipe, this.onTap, this.onTapClass});

  final className = TextEditingController();
  final classDescription = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var authState = Injector.getAsReactive<AuthState>();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        children: [
          // if (authState.state.currentState.isAdmin) AdminLabel(),
          AdminLabel(),
          Image.asset('assets/icons/kelas/kelas.png', height: S.w * .6, width: S.w * .7),
          Divider(color: Colors.grey),
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 15),
            child: Text(
              'Daftar Kelas saya',
              style: descBlack.copyWith(color: Colors.black, fontSize: S.w / 26),
              textAlign: TextAlign.center,
            ),
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: data
                  .map((e) => Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child: InkWell(
                            onTap: tipe == 'change'
                                ? () async {
                                    var schoolRole = authState.state.currentState.schoolRole;
                                    vUtils.setLog(schoolRole.isEmpty);
                                    SiswamediaTheme.infoLoading(
                                        context: context, info: 'Sedang mengganti kelas');
                                    var data = UserCurrentState(
                                        className: e.name,
                                        classId: e.id,
                                        classRole: e.role,
                                        isAdmin: authState.state.currentState.isAdmin,
                                        type: authState.state.currentState.type,
                                        schoolName: authState.state.currentState.schoolName,
                                        schoolRole: schoolRole.isEmpty ? [e.role] : schoolRole,
                                        schoolId: authState.state.currentState.schoolId);
                                    var result = await ClassServices.setCurrentState(
                                        data: json.encode(data.toJson()));
                                    authState.state.changeCurrentState(state: data);

                                    var materi = MateriDetail(
                                        jenjang: e.tingkat.contains('SMA')
                                            ? e.tingkat
                                            : generateJenjang(e.tingkat),
                                        kurikulum: '2013',
                                        kelas: 'kelas ' + e.tingkat,
                                        pelajaran: null,
                                        pengguna: e.role == 'SISWA' ? 'SISWA' : 'GURU');
                                    if (e.role == 'ORTU') {
                                      //todo penting
                                      // await authState.setState((s) =>
                                      //     s.setListAnak(e.studentIdList));
                                    }

                                    if (result.statusCode != 200) {
                                      Navigator.pop(context);
                                      CustomFlushBar.errorFlushBar(
                                        result.message,
                                        context,
                                      );
                                    } else {
                                      //todo remove listStudent
                                      await GlobalState.materi().state.setMateri(materi);

                                      //todo penting
                                      await authState.setState((s) => s.setCurrentClass(
                                          className: e.name, classId: e.id, classRole: e.role));
                                      GlobalState.materi().notify();
                                      GlobalState.auth().notify();
                                      Navigator.pop(context);
                                      CustomFlushBar.successFlushBar(
                                        'Mengganti ke kelas ${e.name}',
                                        context,
                                      );
                                      Cleaner()..cleanState();
                                    }
                                  }
                                : () {
                                    if (!e.isApprove && !authState.state.currentState.isAdmin) {
                                      dialogDev(context,
                                          title: 'Konfirmasi',
                                          desc:
                                              'Mohon Maaf anda belum di verifikasi oleh admin sekolah!');
                                    } else {
                                      authState.setState((s) => s.setSelectedClassRole(e.role));
                                      GlobalState.detailClass()
                                          .setState((s) => s.setId(e.id), silent: true);
                                      //todo refactor penting
                                      // navigate(context,
                                      //     DetailClassInstansi(id: e.id),
                                      //     settings: RouteSettings(
                                      //         name: '/detail-class'));
                                    }
                                  },
                            child: buildContainerItem(context, e)),
                      ))
                  .toList()),
        ],
      ),
    );
  }

  Stack buildContainerItem(BuildContext context, MyClassModel meClass) {
    var authState = Injector.getAsReactive<AuthState>();
    return Stack(children: [
      StateBuilder<AuthState>(
          observe: () => authState,
          builder: (context, _) {
            return Container(
              width: S.w,
              padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .035),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                      color: authState.state.currentState.classId == meClass.id
                          ? SiswamediaTheme.green
                          : Color(0xffCFCFCF))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Icon(
                        CustomIcons.kelas,
                        color: authState.state.currentState.classId == meClass.id
                            ? SiswamediaTheme.green
                            : Colors.black87,
                      ),
                      SizedBox(width: 15),
                      Expanded(
                        child: Text(meClass.name,
                            style: TextStyle(
                                fontSize: S.w / 26,
                                fontWeight: FontWeight.w800,
                                color: authState.state.currentState.classId == meClass.id
                                    ? SiswamediaTheme.green
                                    : Colors.black87)),
                      ),
                      SizedBox(width: 20),
                      if (meClass.role.isNotEmpty)
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 6, vertical: 5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(color: Color(0xffCFCFCF), width: 1),
                                color: Color(0xffF6FFE9)),
                            child: Text('${meClass.role}', style: TextStyle(fontSize: S.w / 36))),
                    ]),
                  ),
                  SizedBox(width: 10),
                  if ((meClass.role == 'WALIKELAS' || authState.state.currentState.isAdmin) &&
                      tipe != 'change')
                    InkWell(
                        onTap: () {
                          className.text = meClass.name;
                          classDescription.text = meClass.description;
                          dialog(context, meClass);
                        },
                        child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            decoration: BoxDecoration(
                                color: SiswamediaTheme.green,
                                borderRadius: BorderRadius.circular(100)),
                            child: Icon(Icons.edit, color: Colors.white)))
                ],
              ),
            );
          }),
      Positioned(
          top: 0,
          right: 0,
          child: Container(
            height: 40,
            width: MediaQuery.of(context).size.width * 0.4,
            decoration: BoxDecoration(
                color: Colors.red[700],
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30.0), topRight: Radius.circular(10))),
            child: Center(child: Text('Belum Verifikasi', style: TextStyle(color: Colors.white))),
          )),
    ]);
  }

  void dialog(BuildContext context, MyClassModel detailClass) {
    var classState = Injector.getAsReactive<ClassState>();
    showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (_) =>
            StatefulBuilder(builder: (BuildContext _, void Function(void Function()) setState) {
              return Center(
                child: SingleChildScrollView(
                  child: Dialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                      child: Container(
                          width: S.w,
                          padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .05),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText('Kelasku', Kind.heading1),
                              SizedBox(
                                height: 10,
                              ),
                              CustomText('Ubah atau hapus kelas', Kind.descBlack,
                                  align: TextAlign.center),
                              SizedBox(
                                height: 10,
                              ),

                              // DropdownButtonFormField<String>(
                              //   value: teacherUserId,
                              //   icon: Icon(Icons.keyboard_arrow_down),
                              //   iconSize: 24,
                              //   elevation: 16,
                              //   decoration: fullRadiusField(context,
                              //       hint: 'Pilih Guru'),
                              //   style: normalText(context),
                              //   onChanged: (String newValue) {
                              //     setState(() {});
                              //   },
                              //   items: [
                              //     DropdownMenuItem(
                              //       child: Text('Pilih Guru',
                              //           style: hintText(context)),
                              //       value: '',
                              //     ),
                              //     // for (DataListTeachers value in teacher)
                              //     //   DropdownMenuItem(
                              //     //     value: value.userId.toString(),
                              //     //     child: Text(
                              //     //       value.fullName,
                              //     //       style: normalText(context),
                              //     //     ),
                              //     //   )
                              //   ],
                              // ),
                              SizedBox(
                                height: 16,
                              ),
                              TextField(
                                controller: className,
                                style: normalText(context),
                                decoration: fullRadiusField(context, hint: 'Nama Kelas'),
                              ),
                              SizedBox(height: 16),
                              // TextField(
                              //   controller: classDescription,
                              //   style: normalText(context),
                              //   maxLines: 4,
                              //   minLines: 3,
                              //   decoration: InputDecoration(
                              //     hintText: 'Deskripsi kelas',
                              //     hintStyle: descBlack.copyWith(
                              //         fontSize: S.w / 30,
                              //         color: Color(0xffB9B9B9)),
                              //     contentPadding: EdgeInsets.symmetric(
                              //         vertical: 10.0, horizontal: 25),
                              //     enabledBorder: OutlineInputBorder(
                              //       borderSide:
                              //           BorderSide(color: Color(0xffD8D8D8)),
                              //       borderRadius: BorderRadius.circular(12),
                              //     ),
                              //     focusedBorder: OutlineInputBorder(
                              //       borderSide: BorderSide(
                              //           color: SiswamediaTheme.green),
                              //       borderRadius: BorderRadius.circular(12),
                              //     ),
                              //   ),
                              // ),
                              SizedBox(height: 16),
                              InkWell(
                                  onTap: () async {
                                    detailClass.name = className.text;
                                    detailClass.description = classDescription.text;
                                    //todo penting refactor
                                    // var data = ClassBySchoolModel(
                                    //     cudModel: CUDClassModel());
                                    ClassBySchoolModel data;
                                    await GlobalState.classes().state.updateData(context, data);
                                    await GlobalState.classes().notify();
                                    // setState(
                                    //     (s) => s.updateData(context, data));
                                  },
                                  child: Container(
                                      width: S.w,
                                      height: 45,
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.green,
                                          borderRadius: BorderRadius.circular(23)),
                                      child: Center(
                                          child: Text('Simpan',
                                              style: TextStyle(
                                                  fontSize: S.w / 30,
                                                  fontWeight: FontWeight.w600,
                                                  color: SiswamediaTheme.white))))),
                              SizedBox(height: 10),
                              InkWell(
                                  onTap: () async {
                                    await GlobalState.classes()
                                        .state
                                        .deleteData(context, detailClass.id);

                                    GlobalState.classes().notify();
                                  },
                                  child: Container(
                                      width: S.w,
                                      height: 45,
                                      decoration: BoxDecoration(
                                          color: SiswamediaTheme.nearlyBlack,
                                          borderRadius: BorderRadius.circular(23)),
                                      child: Center(
                                          child: Text('Hapus',
                                              style: TextStyle(
                                                  fontSize: S.w / 30,
                                                  fontWeight: FontWeight.w600,
                                                  color: SiswamediaTheme.white)))))
                            ],
                          ))),
                ),
              );
            }));
  }
}
