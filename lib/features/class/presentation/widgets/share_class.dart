part of '_widgets.dart';

class ShareClass extends StatelessWidget {
  const ShareClass({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        margin: EdgeInsets.symmetric(
            horizontal: width * .04, vertical: width * .04),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Selamat Datang di kelasku",
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            RaisedButton(
              elevation: 0,
              color: SiswamediaTheme.green,
              onPressed: () {},
              child: const Text('Bagikan Kode',
                  style: TextStyle(fontSize: 20, color: Colors.white)),
            ),
          ],
        ));
  }
}
