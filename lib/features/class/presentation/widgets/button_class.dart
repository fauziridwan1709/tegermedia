part of '_widgets.dart';

class ButtonWidthCondition extends StatelessWidget {
  ButtonWidthCondition({
    Key key,
    @required this.role,
    @required this.id,
    this.refreshCount,
    this.count,
    this.rKey,
    this.dialogKey,
  }) : super(key: key);

  final List<String> role;
  final int id;
  final int count;
  final Function refreshCount;
  final GlobalKey<RefreshIndicatorState> rKey;
  final GlobalKey<ScaffoldState> dialogKey;

  final authState = GS.auth();

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
          margin: EdgeInsets.only(left: 25, right: 25, bottom: 10, top: 10),
          child: Divider()),
      if (authState.state.currentState.isAdmin &&
          authState.state.currentState.type != 'PERSONAL')
        ButtonAction(
            label: 'Verifikasi Guru',
            subLabel: 'Menunggu Persetujuan : $count',
            onTap: () {},
            //todo refactor arrow function
            // navigate(
            // context,
            // ListTeacherWaitingApproval(school,
            //     refreshCount: () => refreshCount())),
            icon: 'assets/icons/kelas/add.png'),
      if (authState.state.currentState.isAdmin)
        ButtonAction(
          label: 'Tambah Kelas',
          icon: 'assets/icons/kelas/add.png',
          onTap: () {
            //TODO uncomment dont delete this
            showDialog<void>(
                context: dialogKey.currentContext,
                builder: (context) {
                  return Dialog(
                      shape: RoundedRectangleBorder(borderRadius: radius(12)),
                      child: Padding(
                        padding: EdgeInsets.all(14),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CustomText(
                              'Ingin menambah kelas dari google classroom?',
                              Kind.heading3,
                              align: TextAlign.center,
                            ),
                            SizedBox(height: 20),
                            ButtonDialogTambah(
                                icon: Icons.sync,
                                onTap: () {},
                                //todo refactor arrow function
                                // popAndNavigate(
                                // context, ClassroomSync(rKey: rKey)),
                                label: 'Sync Google Classroom'),
                            SizedBox(height: 14),
                            ButtonDialogTambah(
                                icon: Icons.add,
                                onTap: () {},
                                //todo refactor arrow function
                                // popAndNavigate(
                                // context, CreateClassBySchool(schoolId: id)),
                                label: 'Tambah Kelas Baru'),
                            SizedBox(height: 14),
                          ],
                        ),
                      ));
                });
            // navigate(context, CreateClassBySchool(schoolId: id));
          },
        ),
      ButtonAction(
        label: 'Join Kelas',
        icon: 'assets/icons/kelas/join.png',
        onTap: () {
          showDialog<void>(
              context: context,
              builder: (context) {
                return Dialog(
                  shape: RoundedRectangleBorder(borderRadius: radius(12)),
                  child: Container(
                    width: S.w * .8,
                    padding: EdgeInsets.all(S.w * .025),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        CustomText('Pilih Role', Kind.heading1),
                        CustomText('Role yang dipilih untuk join kelas baru',
                            Kind.descBlack),
                        SizedBox(height: S.w * .05),
                        Row(
                          children: [
                            Expanded(
                              child: RaisedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  //todo refactor penting
                                  // Navigator.push<dynamic>(
                                  //   context,
                                  //   MaterialPageRoute<dynamic>(
                                  //       builder: (BuildContext context) =>
                                  //           InvitationClassInstansi(
                                  //             role: 'SISWA',
                                  //             navigateFromProfile: false,
                                  //           )),
                                  // );
                                },
                                color: SiswamediaTheme.green,
                                child: Center(
                                    child: Text(
                                  'SISWA',
                                  style: semiWhite.copyWith(fontSize: S.w / 30),
                                )),
                                shape: RoundedRectangleBorder(
                                    borderRadius: radius(200)),
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: RaisedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  //todo refactor penting
                                  // Navigator.push<dynamic>(
                                  //   context,
                                  //   MaterialPageRoute<dynamic>(
                                  //       builder: (BuildContext context) =>
                                  //           InvitationClassInstansi(
                                  //             role: 'GURU',
                                  //             navigateFromProfile: false,
                                  //           )),
                                  // );
                                },
                                color: SiswamediaTheme.lightBlack,
                                child: Center(
                                    child: Text(
                                  'GURU',
                                  style: semiWhite.copyWith(fontSize: S.w / 30),
                                )),
                                shape: RoundedRectangleBorder(
                                    borderRadius: radius(200)),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              });
        },
      ),
    ]);
  }
}
