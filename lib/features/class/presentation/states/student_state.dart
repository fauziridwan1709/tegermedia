part of '_states.dart';

class StudentState implements FutureState<StudentState, int> {
  final StudentRepository repo = StudentRepositoryImpl();
  List<StudentModel> studentModel;

  StudentState({this.studentModel});

  @override
  String cacheKey;

  @override
  bool getCondition() {
    return studentModel != null;
  }

  @override
  Future<void> retrieveData(int classId) async {
    var result = await repo.getAllStudent(classId);
    result.fold((failure) {
      throw failure;
    }, (result) {
      result.statusCode.translate(
          ifSuccess: () {
            studentModel = result.data;
          },
          ifElse: () => throw result.toFailure());
    });
  }

  Future<void> kickMember(int classId, UserIdModel userId) async {
    var result = await repo.kickStudent(classId, userId);
    result.fold((failure) {
      throw failure;
    }, (result) {
      result.statusCode.translate(
          ifSuccess: () {
            retrieveData(classId);
          },
          ifElse: () => null);
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case NetworkFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case TimeoutFailure:
        return 'Timeout, try again';
      default:
        return 'Unexpected error';
    }
  }
}
