part of '_states.dart';

class DetailClassState extends FutureState<DetailClassState, int> {
  final ClassRepository repo = ClassRepositoryImpl();
  @override
  String cacheKey = TIME_DETAIL_CLASS;
  int id;

  HashMap<int, MyClassModel> detailClassMap = HashMap<int, MyClassModel>();

  @override
  bool getCondition() {
    return !detailClassMap.containsKey(id);
  }

  Future<void> getDetailClass(int classId) async {
    var result = await repo.getMyClass(classId);
    result.fold<void>((failure) => throw failure, (result) {
      result.statusCode.translate(
          ifSuccess: () {
            detailClassMap[classId] = result.data;
          },
          ifElse: () => throw result.toFailure());
    });
  }

  ///[data] as classId
  @override
  Future<void> retrieveData(int data) async {
    var result = await repo.getMyClass(data);
    result.fold<void>((failure) => throw failure, (result) {
      result.statusCode.translate(
          ifSuccess: () {
            detailClassMap[data] = result.data;
          },
          ifElse: () => throw result.toFailure());
    });
  }

  Future<void> changeWaliKelas(
      BuildContext context, int classId, UserIdModel userId) async {
    var result = await repo.changeWaliKelas(classId, userId);
    result.fold((failure) => throw failure, (result) {
      result.statusCode.translate(ifSuccess: () {
        //todo
      }, ifElse: () {
        //todo
      });
    });
  }

  void init(int classId) {
    id = classId;
    cacheKey = '$TIME_DETAIL_CLASS$classId';
  }

  void updateCountCourse(int id, bool isAdded) {
    if (isAdded) {
      detailClassMap[id].totalCourse++;
    } else {
      detailClassMap[id].totalCourse--;
    }
  }
}
