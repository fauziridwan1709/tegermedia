import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/features/class/data/models/_models.dart';
import 'package:tegarmedia/features/class/data/repositories/_repositories.dart';
import 'package:tegarmedia/features/class/domain/repositories/_repositories.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'class_state.dart';
part 'detail_class_state.dart';
part 'student_state.dart';
part 'teacher_state.dart';
