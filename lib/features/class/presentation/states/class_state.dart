part of '_states.dart';

const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';

class ClassState implements FutureState<ClassState, int> {
  final ClassRepository repo = ClassRepositoryImpl();
  HashMap<int, MyClassModel> classMap = HashMap<int, MyClassModel>();

  int countWaitingApproval;

  @override
  String cacheKey = TIME_LIST_CLASS;

  @override
  bool getCondition() {
    return classMap.isNotEmpty;
  }

  Future<void> createData(BuildContext context, MyClassModel data) async {
    // TODO: implement createData
    classMap[data.id] = data;
  }

  Future<void> deleteData(BuildContext context, int id) async {
    pop(context);
    SiswamediaTheme.infoLoading(context: context, info: 'Menghapus kelas..');
    var result = await repo.deleteMyClass(id);

    result.fold<void>((failure) {
      pop(context);
      CustomFlushBar.errorFlushBar(_mapFailureToMessage(failure), context);
    }, (result) {
      pop(context);
      result.statusCode.translate<void>(
          ifSuccess: () {
            CustomFlushBar.successFlushBar(result.message, context);
            classMap.remove(id);
          },
          ifElse: () => CustomFlushBar.errorFlushBar(result.message, context));
    });
  }

  ///[data] as schoolId
  @override
  Future<void> retrieveData(int schoolId) async {
    var result = await repo.getAllMyClass(schoolId);
    await getCountWaitingApproval(schoolId);
    result.fold<void>((failure) {
      throw failure;
    }, (result) {
      print('data' + result.statusCode.toString());
      result.statusCode.translate<void>(
          ifSuccess: () {
            print(result.data);
            classMap = HashMap<int, MyClassModel>();
            result.data.forEach((element) {
              classMap[element.id] = element;
            });
            print(classMap);
          },
          ifElse: () => throw result.toFailure());
    });
  }

  Future<void> updateData(BuildContext context, MyClassModel model) async {
    Navigator.pop(context);
    SiswamediaTheme.infoLoading(context: context, info: 'Mengubah nama kelas');
    var result = await repo.updateMyClass(model);

    result.fold<void>((failure) {
      pop(context);
      CustomFlushBar.errorFlushBar(_mapFailureToMessage(failure), context);
    }, (result) {
      pop(context);
      result.statusCode.translate<void>(
          ifSuccess: () {
            CustomFlushBar.successFlushBar(result.message, context);
            classMap[model.id] = model;
          },
          ifElse: () => CustomFlushBar.errorFlushBar(result.message, context));
    });
  }

  Future<Decide<Failure, Parsed<ResultMessageModel>>> joinClass(
      InvitationClassModel model) async {
    // Navigator.pop(context);
    // SiswamediaTheme.infoLoading(context: context, info: 'Mengubah nama kelas');
    var result = await repo.joinClass(model);
    return result;

    // result.fold<void>((failure) {
    //   pop(context);
    //   CustomFlushBar.errorFlushBar(_mapFailureToMessage(failure), context);
    // }, (result) {
    //   pop(context);
    //   result.statusCode.translate(() {
    //     CustomFlushBar.successFlushBar(result.message, context);
    //     classMap[model.id] = model;
    //   }, () => CustomFlushBar.errorFlushBar(result.message, context));
    // });
  }

  Future<void> getCountWaitingApproval(int schoolId) async {
    var result = await repo.getCountWaitingApproval(schoolId);
    result.fold((failure) {}, (result) {
      result.statusCode.translate(
        ifSuccess: () => countWaitingApproval = result.data.count,
      );
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case NetworkFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      case TimeoutFailure:
        return 'Timeout, try again';
      default:
        return 'Unexpected error';
    }
  }

  // void addData(ClassBySchoolData data) {}
}
