part of '_states.dart';

class TeacherState implements FutureState<TeacherState, int> {
  final TeacherRepository repo = TeacherRepositoryImpl();

  List<TeacherModel> teacherModel;

  TeacherState({this.teacherModel});

  @override
  String cacheKey;

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  @override
  Future<void> retrieveData(int classId) async {
    var result = await repo.getAllTeacher(classId);
    result.fold((failure) => throw failure, (result) {
      result.statusCode.translate(ifSuccess: () {
        teacherModel = result.data;
      }, ifElse: () {
        throw result.toFailure();
      });
    });
  }

  //todo delete
  void clearData() {
    teacherModel = null;
  }
}
