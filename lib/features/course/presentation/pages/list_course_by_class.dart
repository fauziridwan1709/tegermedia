// part of '../_pages.dart';
//
// class ListCourseByClass extends StatefulWidget {
//   final int schoolId;
//   final int classId;
//   final int id;
//   final String role;
//
//   ListCourseByClass({this.id, this.role, this.schoolId, this.classId});
//
//   @override
//   _ListCourseByClassState createState() => _ListCourseByClassState();
// }
//
// class _ListCourseByClassState
//     extends BaseStateReBuilder<ListCourseByClass, CourseState> {
//   bool isShowCode = false;
//
//   @override
//   var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
//
//   TextEditingController name = TextEditingController(text: '');
//   TextEditingController desc = TextEditingController(text: '');
//   List<DataListTeachers> teacher = [];
//
//   bool _visible;
//   String teacherUserId = '';
//   String action = 'ADD';
//   int courseId = 0;
//   bool isLoading = false;
//
//   @override
//   void initState() {
//     super.initState();
//     _visible = true;
//     // getTeachers();
//   }
//
//   @override
//   Future<void> retrieveData() async {
//     vUtils.setLog(widget.classId);
//     if (GlobalState.course().isWaiting) {
//       setState(() => _visible = false);
//     }
//     var dataCourse = CourseByClassModel(
//         statusCode: 200, param: GetCourseModel(classId: widget.classId));
//     vUtils.setLog(dataCourse.param.classId);
//     await GlobalState.course().setState((s) => s.retrieveData(data: dataCourse),
//         onData: (context, data) => setState(
//               () => _visible = false,
//             ),
//         onRebuildState: (_) => setState(() => _visible = true));
//     getTeachers();
//   }
//
//   void getTeachers() async {
//     await GlobalState.teachers()
//         .setState((s) => s.retrieveData(widget.classId));
//     GlobalState.course().notify();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Injector(
//       inject: [Inject(() => TeacherState())],
//       builder: (context) {
//         return WillPopScope(
//           onWillPop: () async {
//             await GlobalState.course().refresh();
//             return true;
//           },
//           child: Scaffold(
//             appBar: AppBar(
//               backgroundColor: Colors.white,
//               shadowColor: Colors.transparent,
//               centerTitle: true,
//               title: TitleAppbar(label: 'Mata Pelajaran'),
//               leading: IconButton(
//                   icon: Icon(
//                     Icons.arrow_back,
//                     color: SiswamediaTheme.green,
//                   ),
//                   onPressed: () {
//                     GlobalState.course().refresh();
//                     popUntil(context, '/detail-class');
//                   }),
//               actions: widget.role == 'WALIKELAS' ||
//                       GlobalState.auth().state.currentState.isAdmin
//                   ? [
//                       IconButton(
//                         icon: Icon(Icons.add, color: Colors.green),
//                         onPressed: () {
//                           setState(() {
//                             action = 'ADD';
//                             name.text = '';
//                             teacherUserId = '';
//                           });
//                           dialog(context);
//                         },
//                       ),
//                     ]
//                   : [],
//             ),
//             body: StateBuilder<CourseState>(
//                 observe: () => GlobalState.course(),
//                 builder: (context, snapshot) {
//                   return RefreshIndicator(
//                       key: refreshIndicatorKey,
//                       onRefresh: retrieveData,
//                       child: WhenRebuilder<CourseState>(
//                           observe: () => GlobalState.course(),
//                           onIdle: () => WaitingView(),
//                           onWaiting: () => WaitingView(),
//                           onError: (dynamic error) => ErrorView(error: error),
//                           onData: (data) {
//                             if (data.courseMap.keys.isEmpty) {
//                               return ErrorView(
//                                   error:
//                                       SiswamediaException('Belum ada course'));
//                             }
//                             return AnimatedOpacity(
//                               opacity: _visible ? 1.0 : 0.0,
//                               duration: Duration(milliseconds: 450),
//                               child: Container(
//                                 width: S.w,
//                                 child: ListView(
//                                   padding: EdgeInsets.only(
//                                       left: S.w * .05,
//                                       right: S.w * .05,
//                                       bottom: 30,
//                                       top: 20),
//                                   // itemCount: listProvider.course.length,
//                                   children: data.courseMap.values
//                                       .map((detail) => InkWell(
//                                             onTap: () {
//                                               navigate(
//                                                   context,
//                                                   DetailCourseInstansi(
//                                                     classId: widget.classId,
//                                                     id: detail.id,
//                                                   ));
//                                             },
//                                             child: Container(
//                                               width: S.w,
//                                               height: 69,
//                                               padding: EdgeInsets.symmetric(
//                                                   horizontal: 16),
//                                               margin: EdgeInsets.symmetric(
//                                                   vertical: 5),
//                                               decoration: BoxDecoration(
//                                                   boxShadow: [
//                                                     BoxShadow(
//                                                         color: SiswamediaTheme
//                                                             .grey
//                                                             .withOpacity(0.2),
//                                                         offset: const Offset(
//                                                             0.2, 0.2),
//                                                         blurRadius: 3.0),
//                                                   ],
//                                                   color: Colors.white,
//                                                   borderRadius:
//                                                       BorderRadius.circular(10),
//                                                   border: Border.all(
//                                                       color:
//                                                           Color(0xFF00000029))),
//                                               child: Row(children: [
//                                                 Expanded(
//                                                     child: Column(
//                                                         crossAxisAlignment:
//                                                             CrossAxisAlignment
//                                                                 .start,
//                                                         mainAxisAlignment:
//                                                             MainAxisAlignment
//                                                                 .center,
//                                                         children: [
//                                                       Text(detail.name,
//                                                           style: TextStyle(
//                                                               fontSize: 16,
//                                                               fontWeight:
//                                                                   FontWeight
//                                                                       .w600,
//                                                               color:
//                                                                   SiswamediaTheme
//                                                                       .green)),
//                                                       Text(
//                                                           'Guru : ${detail.listGuru.isNotEmpty ? detail.listGuru[0].nama : '-'}',
//                                                           style: TextStyle(
//                                                               fontSize: 10)),
//                                                     ])),
//                                                 Row(
//                                                   children: [
//                                                     widget.role == 'WALIKELAS'
//                                                         ? InkWell(
//                                                             onTap: () {
//                                                               setState(() {
//                                                                 action = 'EDIT';
//                                                                 teacherUserId = detail
//                                                                         .listGuru
//                                                                         .isNotEmpty
//                                                                     ? detail
//                                                                         .listGuru[
//                                                                             0]
//                                                                         .userId
//                                                                         .toString()
//                                                                     : '';
//                                                                 name.text =
//                                                                     detail.name;
//                                                                 courseId =
//                                                                     detail.id;
//                                                                 desc.text = detail
//                                                                     .description;
//                                                               });
//                                                               dialog(context);
//                                                             },
//                                                             child: Container(
//                                                                 padding: EdgeInsets
//                                                                     .symmetric(
//                                                                         horizontal:
//                                                                             10,
//                                                                         vertical:
//                                                                             10),
//                                                                 decoration: BoxDecoration(
//                                                                     color: SiswamediaTheme
//                                                                         .green,
//                                                                     borderRadius:
//                                                                         BorderRadius.circular(
//                                                                             100)),
//                                                                 child: Icon(
//                                                                     Icons.edit,
//                                                                     color: Colors
//                                                                         .white)))
//                                                         : SizedBox(),
//                                                     SizedBox(width: 5),
//                                                   ],
//                                                 )
//                                               ]),
//                                             ),
//                                           ))
//                                       .toList(),
//                                 ),
//                               ),
//                             );
//                           }));
//                 }),
//           ),
//         );
//       },
//     );
//   }
//
//   void dialogDev(BuildContext context) {
//     // var S.w = MediaQuery.of(context).size.width;
//     showDialog<void>(
//         context: context,
//         builder: (context) => StatefulBuilder(builder: (BuildContext context,
//                 void Function(void Function()) setState) {
//               // String role = someCapitalizedString(data.role);
//               return Dialog(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(12)),
//                   child: Container(
//                       height: 300,
//                       width: S.w * .5,
//                       padding: EdgeInsets.symmetric(
//                           vertical: S.w * .05, horizontal: S.w * .05),
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Text('Oh sorry!',
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold, fontSize: 16)),
//                           SizedBox(
//                             height: 35,
//                           ),
//                           Text(
//                               'Oops! Feature ini masih dalam proses pengembangan! Mohon tunggu update dalam waktu dekat',
//                               textAlign: TextAlign.center,
//                               style: TextStyle(fontSize: 12)),
//                           SizedBox(
//                             height: 16,
//                           ),
//                           InkWell(
//                               onTap: () => Navigator.of(context).pop(),
//                               child: Container(
//                                   width: S.w,
//                                   height: 45,
//                                   decoration: BoxDecoration(
//                                       color: SiswamediaTheme.green,
//                                       borderRadius: BorderRadius.circular(23)),
//                                   child: Center(
//                                       child: Text('Keluar',
//                                           style: TextStyle(
//                                               color: SiswamediaTheme.white)))))
//                         ],
//                       )));
//             }));
//   }
//
//   void dialog(BuildContext context) {
//     var dataCourse = CourseByClassModel(
//         statusCode: 200, param: GetCourseModel(classId: widget.classId));
//     showDialog<void>(
//         context: context,
//         barrierDismissible: true,
//         builder: (context) => StatefulBuilder(builder: (BuildContext context,
//                 void Function(void Function()) setState) {
//               // String role = someCapitalizedString(data.role);
//               return Center(
//                 child: SingleChildScrollView(
//                   child: Dialog(
//                       shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(12)),
//                       child: action == 'DELETE'
//                           ? Container(
//                               width: S.w,
//                               padding: EdgeInsets.symmetric(
//                                   vertical: S.w * .05, horizontal: S.w * .05),
//                               child: Column(
//                                   mainAxisSize: MainAxisSize.min,
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   children: [
//                                     Text('Hapus Mata Pelajaran',
//                                         style: TextStyle(
//                                             fontWeight: FontWeight.bold,
//                                             fontSize: 16)),
//                                     Text(
//                                       'Apakah Anda yakin ingin menghapus mata pelajaran ini ?',
//                                       style: TextStyle(fontSize: 16),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 35,
//                                     ),
//                                     Row(
//                                       children: [
//                                         Expanded(
//                                             child: InkWell(
//                                                 onTap: () async {
//                                                   var result =
//                                                       await ClassServices
//                                                           .deleteCourse(
//                                                               id: courseId);
//
//                                                   if (result.statusCode ==
//                                                       200) {
//                                                     GlobalState.detailClass()
//                                                         .setState((s) =>
//                                                             s.updateCountCourse(
//                                                                 widget.classId,
//                                                                 false));
//                                                     Navigator.pop(context);
//                                                     setState(() {
//                                                       name.text = '';
//                                                       teacherUserId = '';
//                                                     });
//                                                     CustomFlushBar
//                                                         .successFlushBar(
//                                                       result.message,
//                                                       context,
//                                                     );
//
//                                                     await GlobalState.course()
//                                                         .state
//                                                         .retrieveData(
//                                                             data: dataCourse);
//                                                     await GlobalState.course()
//                                                         .notify();
//                                                     // GlobalState.course().setState((s) => s.retrieveData());
//                                                   } else {
//                                                     CustomFlushBar
//                                                         .errorFlushBar(
//                                                       result.message,
//                                                       context,
//                                                     );
//                                                   }
//                                                 },
//                                                 child: Container(
//                                                     height: 45,
//                                                     decoration: BoxDecoration(
//                                                         color: SiswamediaTheme
//                                                             .nearlyBlack,
//                                                         borderRadius:
//                                                             BorderRadius
//                                                                 .circular(5)),
//                                                     child: Center(
//                                                         child: Text('Ya',
//                                                             style: TextStyle(
//                                                                 color: SiswamediaTheme
//                                                                     .white)))))),
//                                         SizedBox(
//                                           width: 20,
//                                         ),
//                                         Expanded(
//                                             child: InkWell(
//                                                 onTap: () {
//                                                   Navigator.pop(context);
//                                                 },
//                                                 child: Container(
//                                                     height: 45,
//                                                     decoration: BoxDecoration(
//                                                         color: SiswamediaTheme
//                                                             .green,
//                                                         borderRadius:
//                                                             BorderRadius
//                                                                 .circular(5)),
//                                                     child: Center(
//                                                         child: Text('Tidak',
//                                                             style: TextStyle(
//                                                                 color: SiswamediaTheme
//                                                                     .white))))))
//                                       ],
//                                     )
//                                   ]))
//                           : Container(
//                               width: S.w,
//                               padding: EdgeInsets.symmetric(
//                                   vertical: S.w * .05, horizontal: S.w * .05),
//                               child: Column(
//                                 mainAxisSize: MainAxisSize.min,
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   CustomText(
//                                       '${action != 'ADD' ? 'Ubah' : 'Tambah'} Mata Pelajaran',
//                                       Kind.heading1),
//                                   SizedBox(
//                                     height: 10,
//                                   ),
//                                   CustomText(
//                                       'Isi data-data terkait mata pelajaran yang akan dibuat',
//                                       Kind.descBlack,
//                                       align: TextAlign.center),
//                                   SizedBox(
//                                     height: 10,
//                                   ),
//                                   DropdownButtonFormField<String>(
//                                     value: teacherUserId,
//                                     icon: Icon(Icons.keyboard_arrow_down),
//                                     iconSize: 24,
//                                     elevation: 16,
//                                     decoration: fullRadiusField(context,
//                                         hint: 'Pilih Guru'),
//                                     style: normalText(context),
//                                     onChanged: (String newValue) {
//                                       setState(() {
//                                         teacherUserId = newValue;
//                                       });
//                                     },
//                                     items: [
//                                       DropdownMenuItem(
//                                         child: Text('Pilih Guru',
//                                             style: hintText(context)),
//                                         value: '',
//                                       ),
//                                       for (TeacherClassroomModel value
//                                           in GlobalState.teachers()
//                                               .state
//                                               .teacherModel
//                                               .data)
//                                         DropdownMenuItem(
//                                           value: value.userId.toString(),
//                                           child: Text(
//                                             value.fullName,
//                                             style: normalText(context),
//                                           ),
//                                         )
//                                     ],
//                                   ),
//                                   SizedBox(
//                                     height: 16,
//                                   ),
//                                   TextField(
//                                     controller: name,
//                                     style: normalText(context),
//                                     decoration: fullRadiusField(context,
//                                         hint: 'Nama Mata Pelajaran',
//                                         label: true),
//                                   ),
//                                   SizedBox(height: 16),
//                                   TextField(
//                                     controller: desc,
//                                     style: normalText(context),
//                                     maxLines: 4,
//                                     minLines: 3,
//                                     decoration: InputDecoration(
//                                       hintText: 'Deskripsi Mata Pelajaran',
//                                       hintStyle: descBlack.copyWith(
//                                           fontSize: S.w / 30,
//                                           color: Color(0xffB9B9B9)),
//                                       contentPadding: EdgeInsets.symmetric(
//                                           vertical: 10.0, horizontal: 25),
//                                       enabledBorder: OutlineInputBorder(
//                                         borderSide: BorderSide(
//                                             color: Color(0xffD8D8D8)),
//                                         borderRadius: BorderRadius.circular(12),
//                                       ),
//                                       focusedBorder: OutlineInputBorder(
//                                         borderSide: BorderSide(
//                                             color: SiswamediaTheme.green),
//                                         borderRadius: BorderRadius.circular(12),
//                                       ),
//                                     ),
//                                   ),
//                                   SizedBox(height: 16),
//                                   isLoading
//                                       ? Center(
//                                           child: CircularProgressIndicator())
//                                       : InkWell(
//                                           onTap: () async {
//                                             if (teacherUserId == '') {
//                                               CustomFlushBar.errorFlushBar(
//                                                 'Nama Guru wajib diisi!',
//                                                 context,
//                                               );
//                                             } else if (name.text == '') {
//                                               CustomFlushBar.errorFlushBar(
//                                                 'Nama Mata Pelajaran Wajib diisi!',
//                                                 context,
//                                               );
//                                             } else {
//                                               if (action == 'ADD') {
//                                                 setState(() {
//                                                   isLoading = true;
//                                                 });
//                                                 var result = await ClassServices
//                                                     .createCourse(reqBody: <
//                                                         String, dynamic>{
//                                                   'name': name.text,
//                                                   'teacher_user_id':
//                                                       int.parse(teacherUserId),
//                                                   'description': desc.text,
//                                                   'class_id': widget.id,
//                                                   'school_id': widget.schoolId,
//                                                   'type': 'SEKOLAH'
//                                                 });
//
//                                                 var classDetail =
//                                                     await ClassServices
//                                                         .detailCourse(
//                                                             id: result.data.id);
//
//                                                 // await ClassServices.joinCourse(
//                                                 //     reqData: <String, String>{
//                                                 //       'invitation_code': classDetail
//                                                 //           .data.invitationTeacherCode,
//                                                 //       'role': 'GURU',
//                                                 //       'type': 'SEKOLAH'
//                                                 //     });
//
//                                                 if (result.statusCode == 200 &&
//                                                     classDetail.statusCode ==
//                                                         200) {
//                                                   setState(() {
//                                                     name.text = '';
//                                                     teacherUserId = '';
//                                                     desc.text = '';
//                                                     isLoading = false;
//                                                   });
//                                                   GlobalState.detailClass()
//                                                       .setState((s) =>
//                                                           s.updateCountCourse(
//                                                               widget.classId,
//                                                               true));
//
//                                                   if (GlobalState.course()
//                                                       .state
//                                                       .courseMap
//                                                       .isEmpty) {
//                                                     await GlobalState.course()
//                                                         .setState((s) =>
//                                                             s.retrieveData(
//                                                                 data:
//                                                                     dataCourse));
//                                                   } else {
//                                                     await GlobalState.course()
//                                                         .state
//                                                         .retrieveData(
//                                                             data: dataCourse);
//                                                     await GlobalState.course()
//                                                         .notify();
//                                                   }
//                                                   Navigator.pop(context);
//                                                   CustomFlushBar
//                                                       .successFlushBar(
//                                                     'Berhasil Ditambahkan',
//                                                     context,
//                                                   );
//                                                 } else {
//                                                   setState(() {
//                                                     isLoading = false;
//                                                   });
//                                                   CustomFlushBar.errorFlushBar(
//                                                     result.message,
//                                                     context,
//                                                   );
//                                                 }
//                                               } else {
//                                                 var result = await ClassServices
//                                                     .updateCourse(
//                                                         id: courseId,
//                                                         reqBody: <String,
//                                                             dynamic>{
//                                                       'name': name.text,
//                                                       'teacher_user_id':
//                                                           int.parse(
//                                                               teacherUserId),
//                                                       'description': desc.text,
//                                                       'class_id': widget.id,
//                                                       'school_id':
//                                                           widget.schoolId,
//                                                       'type': 'SEKOLAH'
//                                                     });
//
//                                                 if (result.statusCode == 200) {
//                                                   Navigator.pop(context);
//                                                   await GlobalState.course()
//                                                       .state
//                                                       .retrieveData(
//                                                           data: dataCourse);
//                                                   await GlobalState.course()
//                                                       .notify();
//                                                   CustomFlushBar
//                                                       .successFlushBar(
//                                                     'Berhasil Diupdate',
//                                                     context,
//                                                   );
//                                                   setState(() {
//                                                     name.text = '';
//                                                     desc.text = '';
//                                                     teacherUserId = '';
//                                                   });
//                                                 } else {
//                                                   CustomFlushBar.errorFlushBar(
//                                                     result.message,
//                                                     context,
//                                                   );
//                                                 }
//                                               }
//                                             }
//                                           },
//                                           child: Container(
//                                               width: S.w,
//                                               height: 45,
//                                               decoration: BoxDecoration(
//                                                   color: SiswamediaTheme.green,
//                                                   borderRadius:
//                                                       BorderRadius.circular(
//                                                           23)),
//                                               child: Center(
//                                                   child: Text(
//                                                       action == 'EDIT'
//                                                           ? 'SIMPAN'
//                                                           : 'TAMBAHKAN',
//                                                       style: TextStyle(
//                                                           fontSize: S.w / 30,
//                                                           fontWeight:
//                                                               FontWeight.w600,
//                                                           color: SiswamediaTheme
//                                                               .white))))),
//                                   action == 'ADD'
//                                       ? SizedBox()
//                                       : SizedBox(height: 10),
//                                   action == 'ADD'
//                                       ? SizedBox()
//                                       : InkWell(
//                                           onTap: () {
//                                             setState(() {
//                                               action = 'DELETE';
//                                             });
//                                           },
//                                           child: Container(
//                                               width: S.w,
//                                               height: 45,
//                                               decoration: BoxDecoration(
//                                                   color: SiswamediaTheme
//                                                       .nearlyBlack,
//                                                   borderRadius:
//                                                       BorderRadius.circular(
//                                                           23)),
//                                               child: Center(
//                                                   child: Text('Hapus',
//                                                       style: TextStyle(
//                                                           fontSize: S.w / 30,
//                                                           fontWeight:
//                                                               FontWeight.w600,
//                                                           color: SiswamediaTheme
//                                                               .white)))))
//                                 ],
//                               ))),
//                 ),
//               );
//             }));
//   }
// }
