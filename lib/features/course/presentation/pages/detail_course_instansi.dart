// part of '../_pages.dart';
//
// class DetailCourseInstansi extends StatefulWidget {
//   final int id;
//   final int classId;
//   DetailCourseInstansi({@required this.id, this.classId});
//   @override
//   _DetailCourseInstansiState createState() => _DetailCourseInstansiState();
// }
//
// class _DetailCourseInstansiState extends State<DetailCourseInstansi> {
//   final authState = Injector.getAsReactive<AuthState>();
//   final teacherState = Injector.getAsReactive<TeacherState>();
//   ModelDetailClassId data;
//   bool isLoading = true;
//
//   @override
//   void initState() {
//     super.initState();
//     WidgetsBinding.instance.addPostFrameCallback((_) async {
//       //todo penting
//       // if (classProvider.teachers == null) {
//       //   await classProvider.getTeachersInClass(widget.classId);
//       // }
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           backgroundColor: Colors.white,
//           shadowColor: Colors.transparent,
//           centerTitle: true,
//           title: TitleAppbar(label: 'Detail Mata Pelajaran'),
//           leading: IconButton(
//               icon: Icon(
//                 Icons.arrow_back,
//                 color: SiswamediaTheme.green,
//               ),
//               onPressed: () {
//                 Navigator.pop(context);
//               }),
//         ),
//         body: FutureBuilder<ModelDetailCourse>(
//             future: ClassServices.detailCourse(id: widget.id),
//             builder: (context, snapshot) {
//               if (snapshot.connectionState == ConnectionState.waiting) {
//                 return Padding(
//                   padding: EdgeInsets.only(top: S.w / 3),
//                   child: Center(child: CircularProgressIndicator()),
//                 );
//               } else if (snapshot.connectionState == ConnectionState.done) {
//                 if (snapshot.data.statusCode == 200) {
//                   var data = snapshot.data.data;
//                   return DetailMataPelajaran(data: data);
//                 } else {
//                   return Container(
//                       child: Center(child: Text('Terjadi Kesalahan...')));
//                 }
//               } else {
//                 return Container(child: Text('Terjadi Kesalahan...'));
//               }
//             }));
//   }
// }
//
// class DetailMataPelajaran extends StatelessWidget {
//   final DetailCourse data;
//   final GlobalKey<ScaffoldState> scaffoldKey;
//   const DetailMataPelajaran({this.data, this.scaffoldKey});
//
//   @override
//   Widget build(BuildContext context) {
//     var teacherState = Injector.getAsReactive<TeacherState>();
//     var realData = teacherState.state.teacherModel.data.where((element) => data
//         .listGuru
//         .where((element2) => element2.userId == element.userId)
//         .isNotEmpty);
//     return ListView(
//       children: [
//         Container(
//           margin: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: 24),
//           child: Column(
//             children: [
//               Text(someCapitalizedString(data.name),
//                   textAlign: TextAlign.center,
//                   style: boldBlack.copyWith(fontSize: S.w / 22)),
//               SizedBox(height: 10),
//               CustomText(
//                   '${data.description == '' ? 'Tidak ada deskripsi' : data.description}',
//                   Kind.descBlack,
//                   align: TextAlign.center),
//               SizedBox(height: 10),
//               CustomText('Kontak Guru', Kind.heading1, align: TextAlign.left),
//               SizedBox(height: 10),
//               if (realData.length == 1)
//                 SizedBox(
//                   width: S.w * .5,
//                   child: AspectRatio(
//                       aspectRatio: .7,
//                       child: CardDaftarGuru(data: realData.single)),
//                 ),
//               if (realData.length > 1)
//                 GridView.count(
//                   padding: EdgeInsets.all(10),
//                   shrinkWrap: true,
//                   crossAxisCount: 2,
//                   childAspectRatio: .7,
//                   children:
//                       realData.map((e) => CardDaftarGuru(data: e)).toList(),
//                   // children: data.,
//                 ),
//               SizedBox(height: 16),
//               Divider(),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
// }
//
// class CardDaftarGuru extends StatelessWidget {
//   final TeacherClassroomModel data;
//   final ReactiveModel<TeacherState> teacherState;
//
//   CardDaftarGuru({this.data, this.teacherState});
//
//   @override
//   Widget build(BuildContext context) {
//     var auth = Injector.getAsReactive<AuthState>().state;
//     var profile = Injector.getAsReactive<ProfileState>().state.profile;
//     return Container(
//         padding: EdgeInsets.all(S.w * .05),
//         decoration: SiswamediaTheme.whiteRadiusShadow,
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             ClipRRect(
//                 borderRadius: BorderRadius.circular(2000),
//                 child: Image.network(data.image,
//                     height: S.w * .15, width: S.w * .15)),
//             SizedBox(height: 10),
//             CustomText(data.fullName, Kind.headingMax2Line,
//                 align: TextAlign.center),
//             CustomText(
//               '${data.role}',
//               Kind.descBlack,
//             ),
//             SizedBox(height: 10),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 RaisedButton(
//                     shape: RoundedRectangleBorder(borderRadius: radius(200)),
//                     onPressed: () {
//                       showDialog<void>(
//                           context: context,
//                           builder: (context) => Dialog(
//                               shape: RoundedRectangleBorder(
//                                   borderRadius: radius(12)),
//                               child: Container(
//                                 width: S.w,
//                                 padding: EdgeInsets.all(S.w * .05),
//                                 child: Column(
//                                     mainAxisSize: MainAxisSize.min,
//                                     children: [
//                                       CustomText('Kontak', Kind.heading1),
//                                       SizedBox(height: 10),
//                                       Column(
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.start,
//                                         children: [
//                                           SizedBox(
//                                               width: 60,
//                                               child: CustomText(
//                                                   'Telepon', Kind.descBlack)),
//                                           Row(
//                                             children: [
//                                               Expanded(
//                                                   child: Container(
//                                                 padding: EdgeInsets.symmetric(
//                                                     horizontal: 10,
//                                                     vertical: 5),
//                                                 decoration: BoxDecoration(
//                                                     color: Color(0xFFECECEC),
//                                                     borderRadius:
//                                                         BorderRadius.circular(
//                                                             3),
//                                                     border: Border.all(
//                                                         color:
//                                                             Color(0xFFECECEC))),
//                                                 child: Text(
//                                                     (data.noTelp == ''
//                                                             ? 'Tidak ada'
//                                                             : data.noTelp)
//                                                         .toString(),
//                                                     style: TextStyle(
//                                                         color: SiswamediaTheme
//                                                             .green)),
//                                               )),
//                                               GestureDetector(
//                                                   onTap: () {
//                                                     if (data.noTelp != '') {
//                                                       Navigator.pop(context);
//                                                       share(
//                                                           context,
//                                                           data.noTelp,
//                                                           'Tindakan');
//                                                     } else {
//                                                       CustomFlushBar
//                                                           .errorFlushBar(
//                                                         'Tidak ada nomor telepon',
//                                                         context,
//                                                       );
//                                                     }
//                                                   },
//                                                   child: Container(
//                                                       padding:
//                                                           EdgeInsets.symmetric(
//                                                               horizontal: 5,
//                                                               vertical: 5),
//                                                       decoration: BoxDecoration(
//                                                           color: SiswamediaTheme
//                                                               .green,
//                                                           borderRadius:
//                                                               BorderRadius
//                                                                   .circular(3)),
//                                                       child: Icon(
//                                                         Icons.share,
//                                                         color: Colors.white,
//                                                         size: S.w * .05,
//                                                       ))),
//                                             ],
//                                           ),
//                                         ],
//                                       ),
//                                       SizedBox(height: 5),
//                                       Column(
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.start,
//                                         children: [
//                                           SizedBox(
//                                               width: 60,
//                                               child: CustomText(
//                                                   'Email', Kind.descBlack)),
//                                           Row(
//                                             children: [
//                                               Expanded(
//                                                   child: Container(
//                                                 padding: EdgeInsets.symmetric(
//                                                     horizontal: 10,
//                                                     vertical: 5),
//                                                 decoration: BoxDecoration(
//                                                     color: Color(0xFFECECEC),
//                                                     borderRadius:
//                                                         BorderRadius.circular(
//                                                             3),
//                                                     border: Border.all(
//                                                         color:
//                                                             Color(0xFFECECEC))),
//                                                 child: Text(data.email ?? '',
//                                                     style: TextStyle(
//                                                         color: SiswamediaTheme
//                                                             .green)),
//                                               )),
//                                               GestureDetector(
//                                                   onTap: () {
//                                                     if (data.email != '') {
//                                                       Navigator.pop(context);
//                                                       share(context, data.email,
//                                                           'Tindakan');
//                                                     } else {
//                                                       CustomFlushBar
//                                                           .errorFlushBar(
//                                                         'Tidak ada email',
//                                                         context,
//                                                       );
//                                                     }
//                                                   },
//                                                   child: Container(
//                                                     padding:
//                                                         EdgeInsets.symmetric(
//                                                             horizontal: 5,
//                                                             vertical: 5),
//                                                     decoration: BoxDecoration(
//                                                         color: SiswamediaTheme
//                                                             .green,
//                                                         borderRadius:
//                                                             BorderRadius
//                                                                 .circular(3)),
//                                                     child: Icon(Icons.share,
//                                                         color: Colors.white,
//                                                         size: S.w * .05),
//                                                   )),
//                                             ],
//                                           ),
//                                         ],
//                                       ),
//                                       SizedBox(height: 10),
//                                       Container(
//                                           child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.center,
//                                         children: [
//                                           Card(
//                                               elevation: 4,
//                                               shadowColor:
//                                                   Colors.grey.withOpacity(.2),
//                                               shape: RoundedRectangleBorder(
//                                                   borderRadius:
//                                                       BorderRadius.circular(6)),
//                                               child: InkWell(
//                                                 onTap: () async {
//                                                   if (data.email != '') {
//                                                     var email =
//                                                         'mailto:${data.email}?subject=News&body=Hai%20Siswamedia';
//                                                     if (await canLaunch(
//                                                         email)) {
//                                                       await launch(email);
//                                                     } else {
//                                                       throw 'Could not launch $email';
//                                                     }
//                                                   } else {
//                                                     CustomFlushBar
//                                                         .errorFlushBar(
//                                                       'Tidak ada email',
//                                                       context,
//                                                     );
//                                                   }
//                                                 },
//                                                 child: Container(
//                                                   height: 50,
//                                                   padding: EdgeInsets.symmetric(
//                                                       vertical: 10,
//                                                       horizontal: 10),
//                                                   child: Center(
//                                                     child: Row(
//                                                       children: [
//                                                         Icon(Icons.email),
//                                                         SizedBox(width: 5),
//                                                         CustomText(
//                                                             'Kirim Email',
//                                                             Kind.descBlack),
//                                                       ],
//                                                     ),
//                                                   ),
//                                                 ),
//                                               )),
//                                           Card(
//                                             elevation: 4,
//                                             shadowColor:
//                                                 Colors.grey.withOpacity(.2),
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius:
//                                                     BorderRadius.circular(6)),
//                                             child: InkWell(
//                                               onTap: () async {
//                                                 if (data.noTelp != '') {
//                                                   var telp =
//                                                       'tel:${data.noTelp}';
//                                                   if (await canLaunch(telp)) {
//                                                     await launch(telp);
//                                                   } else {
//                                                     throw 'Could not launch $telp';
//                                                   }
//                                                 } else {
//                                                   CustomFlushBar.errorFlushBar(
//                                                     'Tidak ada no telepon',
//                                                     context,
//                                                   );
//                                                 }
//                                               },
//                                               child: Container(
//                                                 height: 50,
//                                                 padding: EdgeInsets.symmetric(
//                                                     vertical: 10,
//                                                     horizontal: 10),
//                                                 child: Center(
//                                                   child: Row(
//                                                     children: [
//                                                       Icon(Icons.phone),
//                                                       SizedBox(width: 5),
//                                                       CustomText('Telepon',
//                                                           Kind.descBlack),
//                                                     ],
//                                                   ),
//                                                 ),
//                                               ),
//                                             ),
//                                           )
//                                         ],
//                                       )),
//                                     ]),
//                               ))
//                           // child
//                           );
//                     },
//                     color: SiswamediaTheme.green,
//                     child: Center(
//                       child: Text(
//                         'Kontak',
//                         style: semiWhite.copyWith(fontSize: S.w / 30),
//                       ),
//                     ))
//               ],
//             ),
//             SizedBox(height: 10),
//             if (data.role != 'WALIKELAS' &&
//                 (auth.selectedClassRole.isGuruOrWaliKelas ||
//                     auth.currentState.isAdmin) &&
//                 profile.email != data.email)
//               InkWell(
//                 onTap: () {
//                   delete(context, data);
//                 },
//                 child: Icon(
//                   Icons.remove_circle_outline_outlined,
//                 ),
//               )
//           ],
//         ));
//   }
//
//   void delete(BuildContext context, TeacherClassroomModel data) {
//     showDialog<void>(
//         context: context,
//         builder: (context2) => Dialog(
//             shape:
//                 RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
//             child: Container(
//               width: S.w,
//               padding: EdgeInsets.all(S.w * .05),
//               child: Column(mainAxisSize: MainAxisSize.min, children: [
//                 Text('Keluarkan member ini?',
//                     style: secondHeadingBlack.copyWith(fontSize: S.w / 28)),
//                 SizedBox(height: 15),
//                 Text('Apakah Anda yakin untuk mengeluarkan member ini ?',
//                     textAlign: TextAlign.center,
//                     style: descBlack.copyWith(fontSize: S.w / 34)),
//                 SizedBox(height: 20),
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: S.w * .01),
//                   child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: [
//                         Container(
//                           height: S.w / 10,
//                           width: S.w / 3.5,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(6),
//                             color: SiswamediaTheme.green,
//                           ),
//                           child: InkWell(
//                             onTap: () async {
//                               // ignore: unawaited_futures
//                               showDialog<void>(
//                                   context: context,
//                                   builder: (_) => Center(
//                                       child: CircularProgressIndicator()));
//                               await ClassServices.kickMemberClass(
//                                       classId: data.classId,
//                                       userId: data.userId)
//                                   .then((value) async {
//                                 if (value.statusCode == SUCCESS ||
//                                     value.statusCode == 201) {
//                                   Navigator.pop(context);
//                                   Navigator.pop(context);
//
//                                   await teacherState.state
//                                       .retrieveData(data.classId);
//                                   // setState(
//                                   //     (s) => s.retrieveData(data.classId));
//                                   await teacherState.notify();
//                                   CustomFlushBar.successFlushBar(
//                                     'Berhasil mengeluarkan member',
//                                     context,
//                                   );
//                                 } else {
//                                   Navigator.pop(context);
//                                   Navigator.pop(context);
//                                   CustomFlushBar.errorFlushBar(
//                                     'Terjadi kesalahan ketika mengeluarkan member',
//                                     context,
//                                   );
//                                 }
//                               });
//
//                               //Navigator.push(context,
//                               //  MaterialPageRoute<void>(builder: (_) => null));
//                             },
//                             child: Center(
//                               child: Text('Ya',
//                                   style: TextStyle(color: Colors.white)),
//                             ),
//                           ),
//                         ),
//                         Container(
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(6),
//                             color: SiswamediaTheme.lightBlack,
//                           ),
//                           height: S.w / 10,
//                           width: S.w / 3.5,
//                           child: InkWell(
//                             onTap: () => Navigator.pop(context),
//                             child: Center(
//                               child: Text('Tidak',
//                                   style: TextStyle(color: Colors.white)),
//                             ),
//                           ),
//                         ),
//                       ]),
//                 )
//               ]),
//             )));
//   }
// }
