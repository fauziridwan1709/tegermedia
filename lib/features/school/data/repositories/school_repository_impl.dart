part of '_repositories.dart';

class SchoolRepositoryImpl extends SchoolRepository {
  final remoteDataSource = SchoolRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<bool>>> checkSchoolName(
      String schoolName) async {
    return await apiCall(remoteDataSource.checkSchoolName(schoolName));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> createSchool(SchoolModel model,
      {bool isPersonal = false}) async {
    return await apiCall(remoteDataSource.createSchool(model));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> deleteSchool(
      int schoolId) async {
    return await apiCall(remoteDataSource.deleteSchool(schoolId));
  }

  @override
  Future<Decide<Failure, Parsed<List<School>>>> getAllSchool() async {
    return await apiCall(remoteDataSource.getAllSchool());
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> joinSchool(
      InvitationFormModel model) async {
    return await apiCall(remoteDataSource.joinSchool(model));
  }

  @override
  Future<Decide<Failure, Parsed<ResultMessage>>> updateSchool(
      SchoolModel model) async {
    return await apiCall(remoteDataSource.updateSchool(model));
  }
}
