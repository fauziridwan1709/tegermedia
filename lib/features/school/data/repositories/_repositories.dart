import 'package:tegarmedia/aw/entities/_entities.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/school/data/datasources/_datasources.dart';
import 'package:tegarmedia/features/school/data/models/_models.dart';
import 'package:tegarmedia/features/school/domain/entities/_entities.dart';
import 'package:tegarmedia/features/school/domain/repositories/_repositories.dart';

part 'school_repository_impl.dart';
