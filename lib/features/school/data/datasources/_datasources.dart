import 'package:tegarmedia/aw/entities/_entities.dart';
import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/features/school/data/models/_models.dart';
import 'package:tegarmedia/features/school/domain/entities/_entities.dart';

part 'school_remote_data_source.dart';
