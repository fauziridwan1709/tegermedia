part of '_datasources.dart';

abstract class SchoolRemoteDataSource {
  Future<Parsed<List<School>>> getAllSchool();
  Future<Parsed<ResultMessage>> createSchool(SchoolModel model,
      {bool isPersonal = false});
  Future<Parsed<ResultMessage>> joinSchool(InvitationFormModel model);
  Future<Parsed<bool>> checkSchoolName(String schoolName);
  Future<Parsed<ResultMessage>> updateSchool(SchoolModel model);
  Future<Parsed<ResultMessage>> deleteSchool(int schoolId);
}

class SchoolRemoteDataSourceImpl implements SchoolRemoteDataSource {
  @override
  Future<Parsed<List<School>>> getAllSchool() async {
    var url = '${Endpoints.schools}?limit=1000';
    var resp = await getIt(url);
    return resp.parse(ListSchoolModel.fromJson(resp.bodyAsMap).list);
  }

  @override
  Future<Parsed<ResultMessage>> createSchool(SchoolModel model,
      {bool isPersonal = false}) async {
    var url = '${Endpoints.schools}';
    var reqBody = model.toJson();
    reqBody['personal'] = isPersonal;
    var resp = await postIt(url, reqBody);
    return resp.parse(ResultMessageModel());
  }

  @override
  Future<Parsed<ResultMessage>> joinSchool(InvitationFormModel model) async {
    var url = '${Endpoints.schools}?limit=1000';
    var resp = await postIt(url, model.toJson());
    return resp.parse(ResultMessageModel());
  }

  @override
  Future<Parsed<bool>> checkSchoolName(String schoolName) async {
    var param = 'school_name=$schoolName';
    var url = '${Endpoints.schools}?$param';
    var resp = await getIt(url);
    return resp.parse(resp.bodyAsMap as bool);
  }

  @override
  Future<Parsed<ResultMessage>> updateSchool(SchoolModel model) async {
    var url = '${Endpoints.schools}/${model.id}';
    var resp = await putIt(url, model.toJson());
    return resp.parse(ResultMessageModel());
  }

  @override
  Future<Parsed<ResultMessage>> deleteSchool(int schoolId) async {
    var url = '${Endpoints.schools}/$schoolId';
    var resp = await deleteIt(url);
    return resp.parse(ResultMessageModel());
  }
}
