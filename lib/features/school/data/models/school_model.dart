part of '_models.dart';

class SchoolModel extends School {
  SchoolModel({
    int id,
    String invitationCode,
    String name,
    String provinsiId,
    String namaProvinsi,
    String kotaId,
    String namaKabupaten,
    String type,
    String grade,
    String address,
    String namaKepalaSekolah,
    String phoneNumber,
    String schoolEmail,
    String penanggungJawabEmail,
    String namaAdmin,
    bool isAdmin,
    bool personal,
    List<String> roles,
    bool isVerified,
    bool isApprove,
    String createdAt,
    String updatedAt,
    String deletedAt,
  }) : super(
          id: id,
          invitationCode: invitationCode,
          name: name,
          provinsiId: provinsiId,
          namaProvinsi: namaProvinsi,
          kotaId: kotaId,
          namaKabupaten: namaKabupaten,
          type: type,
          grade: grade,
          address: address,
          namaKepalaSekolah: namaKepalaSekolah,
          phoneNumber: phoneNumber,
          schoolEmail: schoolEmail,
          penanggungJawabEmail: penanggungJawabEmail,
          namaAdmin: namaAdmin,
          isAdmin: isAdmin,
          personal: personal,
          roles: roles,
          isVerified: isVerified,
          isApprove: isApprove,
          createdAt: createdAt,
          updatedAt: updatedAt,
          deletedAt: deletedAt,
        );

  SchoolModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    invitationCode = json['invitation_code'];
    name = json['name'];
    provinsiId = json['provinsi_id'];
    namaProvinsi = json['nama_provinsi'];
    kotaId = json['kota_id'];
    namaKabupaten = json['nama_kabupaten'];
    type = json['type'];
    grade = json['grade'];
    address = json['address'];
    isAdmin = json['is_admin'];
    personal = json['personal'];
    namaKepalaSekolah = json['nama_kepala_sekolah'];
    phoneNumber = json['phone_number'];
    schoolEmail = json['school_email'];
    penanggungJawabEmail = json['penanggung_jawab_email'];
    namaAdmin = json['nama_admin'];
    roles = json['roles'].cast<String>();
    isVerified = json['is_verified'];
    isApprove = json['is_approve'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['invitation_code'] = invitationCode;
    data['name'] = name;
    data['provinsi_id'] = provinsiId;
    data['nama_provinsi'] = namaProvinsi;
    data['kota_id'] = kotaId;
    data['nama_kabupaten'] = namaKabupaten;
    data['type'] = personal ? 'Lainnya' : type;
    data['is_admin'] = isAdmin;
    data['personal'] = personal;
    data['grade'] = personal ? 'UMUM' : grade;
    data['address'] = address;
    data['nama_kepala_sekolah'] = namaKepalaSekolah;
    data['phone_number'] = phoneNumber;
    data['school_email'] = schoolEmail;
    data['penanggung_jawab_email'] = penanggungJawabEmail;
    data['nama_admin'] = namaAdmin;
    data['roles'] = roles;
    data['is_verified'] = isVerified;
    data['is_approve'] = isApprove;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}
