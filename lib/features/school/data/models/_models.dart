import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/features/school/domain/entities/_entities.dart';

part 'invitation_form_model.dart';
part 'list_school_model.dart';
part 'school_model.dart';
