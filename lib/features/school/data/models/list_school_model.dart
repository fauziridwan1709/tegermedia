part of '_models.dart';

class ListSchoolModel extends Models {
  List<SchoolModel> list;

  ListSchoolModel.ListSchoolModel({this.list});

  ListSchoolModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <SchoolModel>[];
      json['data'].forEach((dynamic v) {
        list.add(SchoolModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
