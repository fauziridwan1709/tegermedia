part of '_models.dart';

class InvitationFormModel extends InvitationForm {

  InvitationFormModel({
    String code,
    String role
  }) : super(code: code,role: role);

  InvitationFormModel.fromJson(Map<String,dynamic> json) {
    code = json['invitation_code'];
    role = json['role'];
  }

  Map<String,dynamic> toJson() {
    final data = <String,dynamic>{};
    data['invitation_code'] = code;
    data['role'] = role;
    return data;
  }
}

