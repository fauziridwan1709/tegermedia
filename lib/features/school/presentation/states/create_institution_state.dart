part of '_states.dart';

class CreateInstitutionState
    implements FutureState<CreateInstitutionState, int> {
  List<String> listTypeSchool = ['Negeri', 'Swasta', 'Lainnya'];
  List<String> listJenjang = ['TK', 'SD', 'SMP', 'SMA', 'SMK', 'UMUM'];
  final fireStoreInstance = FirebaseFirestore.instance;
  final schoolRepo = SchoolRepositoryImpl();
  final regionRepo = RegionRepositoryImpl();
  var schoolModel = SchoolModel();

  @override
  String cacheKey;

  @override
  bool getCondition() {
    return true;
  }

  @override
  Future<void> retrieveData(int k) async {
    // String data = '';
  }

  Future<void> createSchool(SchoolModel model, BuildContext context) async {
    var resp = await schoolRepo.createSchool(model);
    resp.fold<void>(
        (failure) => throw failure,
        (result) => result.statusCode.translate<void>(ifSuccess: () {
              CustomFlushBar.successFlushBar(result.message, context);
            }, ifElse: () {
              CustomFlushBar.errorFlushBar(result.message, context);
            }));
  }

  Future<void> getCity(String id) async {
    var query = QueryCity();
    var resp = await regionRepo.getCities(query);
    resp.fold<void>(
        (failure) => throw failure,
        (result) => result.statusCode.translate<void>(
            ifElse: () {
              throw result.statusCode.toFailure('message');
            },
            ifSuccess: () {}));
  }

  Future<void> getProvince() async {
    var query = QueryProvince();
    var resp = await regionRepo.getProvinces(query);
    resp.fold<void>(
        (failure) => throw failure,
        (result) => result.statusCode.translate<void>(
            ifElse: () {
              throw result.statusCode.toFailure('message');
            },
            ifSuccess: () {}));
  }

  Future<void> createInstance({DocumentModel data}) async {
    await fireStoreInstance.collection('document').add(<String, dynamic>{
      'class_id': data?.classId,
      'school_id': data?.schoolId,
      'document_list': data?.document,
      'name': data?.name,
      'type': data?.type,
      'parent': data?.parent
    });
  }

  Future<void> createDriveFolder() async {}

  // void dialog(BuildContext context) {
  //   showDialog<void>(
  //       context: context,
  //       builder: (_) => StatefulBuilder(builder:
  //               (BuildContext __, void Function(void Function()) setState) {
  //             return Dialog(
  //                 shape: RoundedRectangleBorder(
  //                     borderRadius: BorderRadius.circular(12)),
  //                 child: Container(
  //                     width: S.w * .8,
  //                     padding: EdgeInsets.symmetric(
  //                         vertical: S.w * .1, horizontal: S.w * .05),
  //                     child: Column(
  //                       // crossAxisAlignment: CrossAxisAlignment.center,
  //                       mainAxisAlignment: MainAxisAlignment.center,
  //                       mainAxisSize: MainAxisSize.min,
  //                       children: [
  //                         Text('Kirim Permohonan',
  //                             style: TextStyle(
  //                                 fontWeight: FontWeight.bold, fontSize: 16)),
  //                         Container(
  //                           width: S.w - 48 * 2,
  //                           height: 100,
  //                           decoration: BoxDecoration(
  //                               image: DecorationImage(
  //                                   image: AssetImage(
  //                                       'assets/icons/kelas/newsletter.png'))),
  //                         ),
  //                         SizedBox(height: 15),
  //                         Text(
  //                             'Anda akan mendapatkan email dari\nkami mengenai verifikasi paling lama\n7 hari kerja. Pastikan email yang anda\nmasukkan adalah email Aktif.',
  //                             style: TextStyle(fontSize: 12),
  //                             textAlign: TextAlign.center),
  //                         SizedBox(height: 15),
  //                         InkWell(
  //                           onTap: () async {
  //                             SiswamediaTheme.loading(context,
  //                                 isDismissible: false);
  //                             await Injector.getAsReactive<ClassState>()
  //                                 .refresh();
  //                             var field = schoolModel.toJson();
  //                             // var field = <String, dynamic>{
  //                             //   'name': _name.text,
  //                             //   'phone_number': _phoneNumber.text,
  //                             //   'provinsi_id': provinsiID,
  //                             //   'kota_id': kotaID,
  //                             //   'type': widget.isPersonal ? 'Lainnya' : type,
  //                             //   'grade': widget.isPersonal ? 'UMUM' : grade,
  //                             //   'address': _address.text,
  //                             //   'school_email': _schoolEmail.text,
  //                             //   'nama_kepala_sekolah': _namaKepalaSekolah.text,
  //                             //   'penanggung_jawab_email': _schoolEmail.text,
  //                             //   'personal': widget.isPersonal
  //                             // };
  //
  //                             await createSchool(schoolModel, context);
  //                             var data = await ClassServices.createSchool(
  //                                 reqBody: field);
  //
  //                             if (data.statusCode.isSuccessOrCreated) {
  //                               var school = await ClassServices.getMeSchool(
  //                                   cache: false);
  //
  //                               var newSchool = school.data
  //                                   .where(
  //                                       (element) => element.id == data.data.id)
  //                                   .first;
  //                               var currentState = UserCurrentState(
  //                                   schoolId: data.data.id,
  //                                   schoolName: newSchool.name,
  //                                   schoolRole: newSchool.roles,
  //                                   type: 'SEKOLAH',
  //                                   className: null,
  //                                   classId: null,
  //                                   classRole: null,
  //                                   isAdmin: newSchool.isAdmin);
  //                               await authState.setState(
  //                                   (s) => s.choseSchoolById(data.data.id));
  //                               await authState.setState((s) =>
  //                                   s.changeCurrentState(state: currentState));
  //                               Navigator.pop(context);
  //                               Navigator.pop(context);
  //                               if (widget.isPersonal) {
  //                                 Navigator.popUntil(
  //                                     context, (route) => route.isFirst);
  //                                 CustomFlushBar.successFlushBar(
  //                                   'Sekolah ${newSchool.name} berhasil dibuat!',
  //                                   context,
  //                                 );
  //                                 await GlobalState.school()
  //                                     .setState((s) => s.retrieveData());
  //                               } else {
  //                                 var reqBody = <String, dynamic>{
  //                                   'name': _name.text.toString(),
  //                                   'mimeType':
  //                                       'application/vnd.google-apps.folder',
  //                                 };
  //
  //                                 var reqBody2 = <String, dynamic>{
  //                                   'role': 'reader',
  //                                   'type': 'anyone',
  //                                 };
  //                                 dialogFolder(context);
  //                                 await GoogleDrive.createDriveFolder(
  //                                         context: context,
  //                                         reqBody: reqBody,
  //                                         reqBody2: reqBody2)
  //                                     .then((value) async {
  //                                   if (value.statusCode == 200) {
  //                                     Navigator.pop(context);
  //                                     var reqP = <String, dynamic>{
  //                                       'name': 'Informasi Sekolah',
  //                                       'mimeType':
  //                                           'application/vnd.google-apps.folder',
  //                                       'parents': [value.id.toString()]
  //                                     };
  //                                     dialogFolder(context);
  //                                     await GoogleDrive.createDriveFolder(
  //                                       context: context,
  //                                       reqBody: reqP,
  //                                       reqBody2: reqBody2,
  //                                     ).then((value2) async {
  //                                       var documentModel = DocumentModel(
  //                                           classId: -1,
  //                                           schoolId: data.data.id,
  //                                           name: 'Informasi Sekolah',
  //                                           parent: value.id,
  //                                           document: value2.id,
  //                                           type: 'sekolah');
  //                                       await createInstance(
  //                                               data: documentModel)
  //                                           .then((value) async {
  //                                         //todo
  //                                         // await documentProvider
  //                                         //     .addFolder(documentModel);
  //
  //                                         var result = await ClassServices
  //                                             .setCurrentState(
  //                                                 data: json.encode(
  //                                                     currentState.toJson()));
  //                                         Navigator.pop(context);
  //                                         if (result.statusCode != 200) {
  //                                           CustomFlushBar.errorFlushBar(
  //                                             result.message,
  //                                             context,
  //                                           );
  //                                         } else {
  //                                           Navigator.popUntil(context,
  //                                               (route) => route.isFirst);
  //                                           await GlobalState.school().setState(
  //                                               (s) => s.retrieveData());
  //                                           CustomFlushBar.successFlushBar(
  //                                             'Folder sekolah berhasil dibuat!',
  //                                             context,
  //                                           );
  //                                         }
  //
  //                                         // Navigator.pushReplacement(
  //                                         //   context,
  //                                         //   MaterialPageRoute<void>(
  //                                         //       builder: (context) =>
  //                                         //           ProfilePage()),
  //                                         // );
  //                                       });
  //                                     });
  //                                   } else {
  //                                     var result =
  //                                         await ClassServices.setCurrentState(
  //                                             data: json.encode(
  //                                                 currentState.toJson()));
  //                                     Navigator.pop(context);
  //                                     if (result.statusCode != 200) {
  //                                       CustomFlushBar.errorFlushBar(
  //                                         result.message,
  //                                         context,
  //                                       );
  //                                     } else {
  //                                       Navigator.popUntil(
  //                                           context, (route) => route.isFirst);
  //                                       await GlobalState.school()
  //                                           .setState((s) => s.retrieveData());
  //                                     }
  //                                     CustomFlushBar.errorFlushBar(
  //                                       'Terjadi Kesalahan dalam mengenerate Google Drive folder, anda bisa membuat folder sekolah manual di menu dokumen',
  //                                       context,
  //                                     );
  //                                   }
  //                                 });
  //                               }
  //                             } else {
  //                               Navigator.of(context).pop();
  //                               CustomFlushBar.errorFlushBar(
  //                                 'Sepertinya Sekolah tersebut telah terdaftar',
  //                                 context,
  //                               );
  //                             }
  //                           },
  //                           child: Container(
  //                               height: 35,
  //                               width: 147,
  //                               decoration: BoxDecoration(
  //                                   color: SiswamediaTheme.green,
  //                                   borderRadius: BorderRadius.circular(4)),
  //                               child: Center(
  //                                   child: Text('Kirim Formulir',
  //                                       style:
  //                                           TextStyle(color: Colors.white)))),
  //                         ),
  //                       ],
  //                     )));
  //           }));
  // }
}
