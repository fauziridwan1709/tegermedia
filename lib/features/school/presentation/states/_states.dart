import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:flutter/material.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/features/region/data/repositories/_repositories.dart';
import 'package:tegarmedia/features/region/domain/entities/_entities.dart';
import 'package:tegarmedia/features/school/data/models/_models.dart';
import 'package:tegarmedia/features/school/data/repositories/_repositories.dart';
import 'package:tegarmedia/models/document/_document.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'create_institution_state.dart';
