// part of '_school.dart';
//
// class SchoolList extends StatelessWidget {
//   final List<DataSchoolModel> mySchool;
//   final bool isPersonal;
//   SchoolList({this.mySchool, this.isPersonal = false});
//
//   final classState = Injector.getAsReactive<ClassState>();
//   @override
//   Widget build(BuildContext context) {
//     final authState = GlobalState.auth();
//     // var meSchool = listProvider.meSchool;
//     //todo later penting
//     return Column(
//       children: [
//         Container(
//             margin: EdgeInsets.symmetric(
//               horizontal: S.w * .05,
//             ),
//             child:
//                 Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//               SizedBox(height: 10),
//               Text(isPersonal ? 'Merdeka Belajar' : 'Sekolah Media PJJ',
//                   style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
//               SizedBox(height: 16),
//               Container(
//                   width: S.w,
//                   child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: List.generate(
//                           mySchool.length,
//                           (index) => InkWell(
//                               onTap: () async {
//                                 SiswamediaTheme.infoLoading(
//                                     context: context,
//                                     info: 'Sedang mengganti kelas');
//                                 await changeSchool(
//                                     context: context,
//                                     meSchool: mySchool[index]);
//                                 Cleaner()..cleanState();
//                               },
//                               child: Container(
//                                 margin: EdgeInsets.only(bottom: 10),
//                                 width: MediaQuery.of(context).size.width,
//                                 padding: EdgeInsets.symmetric(
//                                     horizontal: S.w * .05, vertical: 13),
//                                 decoration: BoxDecoration(
//                                     boxShadow: [
//                                       BoxShadow(
//                                           color: SiswamediaTheme.grey
//                                               .withOpacity(0.2),
//                                           offset: const Offset(0.2, 0.2),
//                                           blurRadius: 3.0),
//                                     ],
//                                     color:
//                                         authState.state.currentState.schoolId ==
//                                                 mySchool[index].id
//                                             ? SiswamediaTheme.green
//                                             : Colors.white,
//                                     borderRadius: BorderRadius.circular(10),
//                                     border:
//                                         Border.all(color: Color(0xFF00000029))),
//                                 child: Column(
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.start,
//                                     children: [
//                                       Text(
//                                           '${mySchool[index].name} ${mySchool[index].isAdmin ? '- Admin' : ''}',
//                                           style: TextStyle(
//                                               fontSize: 18,
//                                               color: authState
//                                                           .state
//                                                           .currentState
//                                                           .schoolId ==
//                                                       mySchool[index].id
//                                                   ? Colors.white
//                                                   : Colors.black)),
//                                       Text(
//                                           'Sebagai : ${mySchool[index].roles.isEmpty ? 'belum ada role kelas' : mySchool[index].roles.join('/')}',
//                                           style: TextStyle(
//                                               fontSize: 10,
//                                               color: authState
//                                                           .state
//                                                           .currentState
//                                                           .schoolId ==
//                                                       mySchool[index].id
//                                                   ? Colors.white
//                                                   : Colors.black))
//                                     ]),
//                               ))).toList()))
//             ])),
//         SizedBox(height: 5),
//         InkWell(
//           onTap: () =>
//               context.push(SelectEntitasInstansi(isPersonal: isPersonal)),
//           child: Container(
//             width: S.w * .9,
//             height: 50,
//             decoration: BoxDecoration(
//                 color: Colors.white,
//                 border: Border.all(color: Color(0xFFD9D9D9)),
//                 borderRadius: BorderRadius.circular(10)),
//             child: Icon(Icons.add_circle, size: 35, color: Color(0xFF4b4b4b)),
//           ),
//         ),
//         SizedBox(
//           height: 25,
//         )
//       ],
//     );
//   }
//
//   ///button event handler
//   Future<void> changeSchool(
//       {BuildContext context, DataSchoolModel meSchool}) async {
//     final authState = GlobalState.auth();
//     try {
//       var currentState = UserCurrentState(
//           schoolId: meSchool.id,
//           schoolName: meSchool.name,
//           schoolRole: meSchool.roles,
//           type: meSchool.personal ? 'PERSONAL' : 'SEKOLAH',
//           className: null,
//           classId: null,
//           classRole: null,
//           isAdmin: meSchool.isAdmin);
//       await ClassServices.setCurrentState(
//               data: json.encode(currentState.toJson()))
//           .then((value) async {
//         Navigator.pop(context);
//         if (value.statusCode == 200 || value.statusCode == 201) {
//           authState.state.changeCurrentState(state: currentState);
//           await authState.state.choseSchoolById(meSchool.id);
//           authState.notify();
//           await classState.refresh();
//           CustomFlushBar.successFlushBar(
//               'Mengganti ke sekolah ${meSchool.name}', context);
//         } else if (value.statusCode == NO_INTERNET_STATUS_CODE) {
//           CustomFlushBar.errorFlushBar(value.message, context);
//         } else if (value.statusCode == TIMEOUT_EXCEPTION_STATUS_CODE) {
//           CustomFlushBar.errorFlushBar(value.message, context);
//         } else {
//           CustomFlushBar.errorFlushBar('terjadi kesalahan', context);
//         }
//       });
//     } catch (e) {
//       print(e);
//     }
//   }
// }
