part of '_pages.dart';

// class CreateInstitution extends StatefulWidget {
//   final bool isPersonal;
//
//   CreateInstitution({this.isPersonal = false});
//
//   @override
//   _CreateInstitutionState createState() => _CreateInstitutionState();
// }
//
// class _CreateInstitutionState
//     extends BaseState<CreateInstitution, CreateInstitutionState>
//     with SchoolNameValidator {
//   @override
//   GlobalKey<RefreshIndicatorState> refreshIndicatorKey =
//       GlobalKey<RefreshIndicatorState>();
//
//   final auth = GlobalState.auth();
//   final authState = GlobalState.auth().state;
//
//   TextEditingController _name;
//   TextEditingController _address;
//   TextEditingController _namaKepalaSekolah;
//   TextEditingController _phoneNumber;
//   TextEditingController _schoolEmail;
//
//   @override
//   void init() {
//     _name = TextEditingController();
//     _address = TextEditingController();
//     _namaKepalaSekolah = TextEditingController();
//     _phoneNumber = TextEditingController();
//     _schoolEmail = TextEditingController();
//   }
//
//   String type = '';
//   String grade = '';
//   String provinsiID = '';
//   String kotaID = '';
//   String selectedLang = 'English';
//   String prov = 'Kota Langsa';
//
//   bool isLoading = false;
//
//   @override
//   Widget buildAppBar(BuildContext context) {
//     // TODO: implement buildAppBar
//     return AppBar(
//       backgroundColor: Colors.white,
//       shadowColor: Colors.transparent,
//       centerTitle: true,
//       title: TitleAppbar(
//           label: 'Daftar ${widget.isPersonal ? 'Media PJJ' : 'Instansi'}'),
//       leading: IconButton(
//         icon: Icon(
//           Icons.arrow_back,
//           color: SiswamediaTheme.green,
//         ),
//         onPressed: () => context.pop(),
//       ),
//     );
//   }
//
//   @override
//   ScaffoldAttribute buildAttribute() {
//     return ScaffoldAttribute();
//   }
//
//   @override
//   Widget buildNarrowLayout(
//       BuildContext context,
//       ReactiveModel<CreateInstitutionState> snapshot,
//       SizingInformation sizeInfo) {
//     var screenSize = sizeInfo.screenSize;
//     return SizedBox(
//         width: screenSize.width,
//         child: ListView(
//           padding: EdgeInsets.symmetric(horizontal: S.w * .08),
//           children: <Widget>[
//             SizedBox(height: defaultMargin),
//             Column(
//               children: <Widget>[
//                 DropdownButtonFormField<String>(
//                   value: provinsiID,
//                   icon: Icon(Icons.keyboard_arrow_down,
//                       color: SiswamediaTheme.green),
//                   iconSize: 24,
//                   elevation: 16,
//                   decoration: InputDecoration(
//                     contentPadding:
//                         EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                     enabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                       borderRadius: BorderRadius.circular(S.w),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: SiswamediaTheme.green),
//                       borderRadius: BorderRadius.circular(S.w),
//                     ),
//                   ),
//                   style: descBlack.copyWith(
//                       fontSize: S.w / 30, color: Color(0xff4B4B4B)),
//                   onChanged: (String newValue) {
//                     setState(() {
//                       kotaID = '';
//                       provinsiID = newValue.toString();
//                       kabupaten = ModelListWilayah(statusCode: null, data: []);
//                     });
//                     getKota(newValue.toString());
//                   },
//                   items: [
//                     DropdownMenuItem(
//                       child: Text('Pilih Provinsi'),
//                       value: '',
//                     ),
//                     for (DataWilayah value in provinsi.data)
//                       DropdownMenuItem(
//                         value: value.id.toString(),
//                         child: Text('${value.nama}'),
//                       )
//                   ],
//                 ),
//                 SizedBox(
//                   height: 16,
//                 ),
//                 DropdownButtonFormField<String>(
//                   value: kotaID,
//                   icon: Icon(Icons.keyboard_arrow_down,
//                       color: SiswamediaTheme.green),
//                   iconSize: 24,
//                   elevation: 16,
//                   decoration: InputDecoration(
//                     contentPadding:
//                         EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                     enabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                       borderRadius: BorderRadius.circular(S.w),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: SiswamediaTheme.green),
//                       borderRadius: BorderRadius.circular(S.w),
//                     ),
//                   ),
//                   style: descBlack.copyWith(
//                       fontSize: S.w / 30, color: Color(0xff4B4B4B)),
//                   onChanged: (String newValue) {
//                     kotaID = newValue.toString();
//                     // setState(() => kotaID = newValue),
//                   },
//                   items: [
//                     DropdownMenuItem(
//                       child: Text('Pilih Kota'),
//                       value: '',
//                     ),
//                     for (DataWilayah value in kabupaten.data)
//                       DropdownMenuItem(
//                         value: value.id.toString(),
//                         child: Text('${value.nama}'),
//                       )
//                   ],
//                 ),
//                 SizedBox(
//                   height: 16,
//                 ),
//                 if (!widget.isPersonal)
//                   DropdownButtonFormField<String>(
//                     value: type,
//                     icon: Icon(Icons.keyboard_arrow_down,
//                         color: SiswamediaTheme.green),
//                     iconSize: 24,
//                     elevation: 16,
//                     decoration: InputDecoration(
//                       contentPadding:
//                           EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                       enabledBorder: OutlineInputBorder(
//                         borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                         borderRadius: BorderRadius.circular(S.w),
//                       ),
//                       focusedBorder: OutlineInputBorder(
//                         borderSide: BorderSide(color: SiswamediaTheme.green),
//                         borderRadius: BorderRadius.circular(S.w),
//                       ),
//                     ),
//                     style: descBlack.copyWith(
//                         fontSize: S.w / 30, color: Color(0xff4B4B4B)),
//                     onChanged: (String newValue) {
//                       if (newValue == 'Lainnya' || newValue == '') {
//                         setState(() {
//                           type = newValue.toString();
//                         });
//                       } else {
//                         setState(() {
//                           type = newValue.toString();
//                         });
//                       }
//                     },
//                     items: [
//                       DropdownMenuItem(
//                         child: Text('Pilih Jenis Sekolah / Instansi'),
//                         value: '',
//                       ),
//                       for (String value in listTypeSchool)
//                         DropdownMenuItem(
//                           value: value.toString(),
//                           child: Text(value),
//                         )
//                     ],
//                   ),
//                 if (!widget.isPersonal)
//                   SizedBox(
//                     height: 16,
//                   ),
//                 Column(children: [
//                   if (!widget.isPersonal)
//                     DropdownButtonFormField<String>(
//                       value: grade,
//                       icon: Icon(Icons.keyboard_arrow_down,
//                           color: SiswamediaTheme.green),
//                       iconSize: 24,
//                       elevation: 16,
//                       decoration: InputDecoration(
//                         contentPadding:
//                             EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                         enabledBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                         focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: SiswamediaTheme.green),
//                           borderRadius: BorderRadius.circular(S.w),
//                         ),
//                       ),
//                       style: descBlack.copyWith(
//                           fontSize: S.w / 30, color: Color(0xff4B4B4B)),
//                       onChanged: (String newValue) =>
//                           setState(() => grade = newValue),
//                       items: [
//                         DropdownMenuItem(
//                           child: Text('Pilih Jenjang Sekolah / Instansi'),
//                           value: '',
//                         ),
//                         for (String value in listJenjang)
//                           DropdownMenuItem(
//                             value: value.toString(),
//                             child: Text(value),
//                           )
//                       ],
//                     ),
//                   SizedBox(height: 16),
//                   StreamBuilder<String>(
//                       stream: validateThisSchoolName,
//                       builder: (context, snapshot) {
//                         return TextField(
//                           controller: _name,
//                           onChanged: onSchoolNameChanged,
//                           style: descBlack.copyWith(fontSize: S.w / 30),
//                           decoration: _inputDecoration(
//                               'Nama ${widget.isPersonal ? 'Media Pjj' : 'Sekolah / Instansi'}',
//                               snapshot.hasError,
//                               snapshot.error),
//                         );
//                       }),
//                   SizedBox(
//                     height: 16,
//                   ),
//                   TextField(
//                     controller: _address,
//                     style: descBlack.copyWith(fontSize: S.w / 30),
//                     maxLines: 4,
//                     minLines: 3,
//                     decoration: InputDecoration(
//                       hintText:
//                           'Alamat ${widget.isPersonal ? '' : 'Sekolah / Instansi'}',
//                       hintStyle: descBlack.copyWith(
//                           fontSize: S.w / 30, color: Color(0xffB9B9B9)),
//                       contentPadding:
//                           EdgeInsets.symmetric(vertical: 10.0, horizontal: 25),
//                       enabledBorder: OutlineInputBorder(
//                         borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                         borderRadius: BorderRadius.circular(12),
//                       ),
//                       focusedBorder: OutlineInputBorder(
//                         borderSide: BorderSide(color: SiswamediaTheme.green),
//                         borderRadius: BorderRadius.circular(12),
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     height: 16,
//                   ),
//                   StreamBuilder<String>(
//                       stream: validateThisHeadMasterName,
//                       builder: (context, snapshot) {
//                         return TextField(
//                           controller: _namaKepalaSekolah,
//                           style: descBlack.copyWith(fontSize: S.w / 30),
//                           onChanged: onHeadMasterNameChanged,
//                           decoration: _inputDecoration(
//                               'Nama ${widget.isPersonal ? 'Penanggung Jawab' : 'Kepala Sekolah / Instansi'}',
//                               snapshot.hasError,
//                               snapshot.error),
//                         );
//                       }),
//                   SizedBox(
//                     height: 16,
//                   ),
//                   StreamBuilder<String>(
//                       stream: validateThisPhoneNumber,
//                       builder: (context, snapshot) {
//                         return TextField(
//                           keyboardType: TextInputType.number,
//                           inputFormatters: <TextInputFormatter>[
//                             FilteringTextInputFormatter.digitsOnly
//                           ],
//                           controller: _phoneNumber,
//                           onChanged: onPhoneNumberChanged,
//                           style: descBlack.copyWith(fontSize: S.w / 30),
//                           decoration: _inputDecoration(
//                               'No Telp. ${widget.isPersonal ? '' : 'Sekolah / Instansi'}',
//                               snapshot.hasError,
//                               snapshot.error),
//                         );
//                       }),
//                   SizedBox(
//                     height: 16,
//                   ),
//                   TextField(
//                     keyboardType: TextInputType.emailAddress,
//                     controller: _schoolEmail,
//                     style: descBlack.copyWith(fontSize: S.w / 30),
//                     decoration: InputDecoration(
//                       hintText:
//                           'Email ${widget.isPersonal ? 'Media' : 'Sekolah / Instansi'}',
//                       hintStyle: descBlack.copyWith(
//                           fontSize: S.w / 30, color: Color(0xffB9B9B9)),
//                       contentPadding:
//                           EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//                       enabledBorder: OutlineInputBorder(
//                         borderSide: BorderSide(color: Color(0xffD8D8D8)),
//                         borderRadius: BorderRadius.circular(S.w),
//                       ),
//                       focusedBorder: OutlineInputBorder(
//                         borderSide: BorderSide(color: SiswamediaTheme.green),
//                         borderRadius: BorderRadius.circular(S.w),
//                       ),
//                     ),
//                   ),
//                 ]),
//               ],
//             ),
//             SizedBox(
//               height: 20,
//             ),
//             GestureDetector(
//               onTap: () {
//                 print('mantap');
//                 Pattern pattern =
//                     r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//                 var regex = RegExp(pattern);
//                 if (!regex.hasMatch(_schoolEmail.text)) {
//                   return CustomFlushBar.errorFlushBar(
//                     'Email tidak valid',
//                     context,
//                   );
//                 }
//
//                 if (grade == '' && !widget.isPersonal) {
//                   return CustomFlushBar.errorFlushBar(
//                     'Jenjang sekolah belum di isi',
//                     context,
//                   );
//                 }
//
//                 if (type == '' && !widget.isPersonal) {
//                   return CustomFlushBar.errorFlushBar(
//                     'Jenis sekolah belum di isi',
//                     context,
//                   );
//                 }
//
//                 if (provinsiID == '') {
//                   return CustomFlushBar.errorFlushBar(
//                     'Provinsi sekolah belum di isi',
//                     context,
//                   );
//                 }
//
//                 if (kotaID == '') {
//                   return CustomFlushBar.errorFlushBar(
//                     'Kota sekolah belum di isi',
//                     context,
//                   );
//                 }
//                 dialog(context);
//               },
//               child: Container(
//                 width: S.w * .7,
//                 height: S.w * .13,
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(12),
//                     color: SiswamediaTheme.green),
//                 padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
//                 child: Center(
//                     child: Text('Lanjutkan',
//                         style: TextStyle(
//                             fontSize: S.w / 28,
//                             color: Colors.white,
//                             fontWeight: FontWeight.w600))),
//               ),
//             ),
//             SizedBox(height: 50)
//           ],
//         ));
//   }
//
//   InputDecoration _inputDecoration(
//       String hintText, bool hasError, String errorText) {
//     return InputDecoration(
//       errorText: errorText,
//       suffixIcon: Icon(
//         hasError ? Icons.clear : Icons.check_circle_rounded,
//         color: hasError ? Colors.red : SiswamediaTheme.green,
//       ),
//       hintText: hintText,
//       hintStyle:
//           descBlack.copyWith(fontSize: S.w / 30, color: Color(0xffB9B9B9)),
//       contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25),
//       enabledBorder: OutlineInputBorder(
//         borderSide: BorderSide(color: Color(0xffD8D8D8)),
//         borderRadius: BorderRadius.circular(S.w),
//       ),
//       focusedErrorBorder: OutlineInputBorder(
//         borderSide: BorderSide(color: Colors.red),
//         borderRadius: BorderRadius.circular(S.w),
//       ),
//       errorBorder: OutlineInputBorder(
//         borderSide: BorderSide(color: Colors.red),
//         borderRadius: BorderRadius.circular(S.w),
//       ),
//       focusedBorder: OutlineInputBorder(
//         borderSide: BorderSide(color: SiswamediaTheme.green),
//         borderRadius: BorderRadius.circular(S.w),
//       ),
//     );
//   }
//
//   @override
//   Widget buildWideLayout(
//       BuildContext context,
//       ReactiveModel<CreateInstitutionState> snapshot,
//       SizingInformation sizeInfo) {
//     // TODO: implement buildWideLayout
//     return Container();
//   }
//
//   @override
//   Future<bool> onBackPressed() async {
//     return true;
//   }
//
//   @override
//   Future<void> retrieveData() {
//     // TODO: implement retrieveData
//     throw UnimplementedError();
//   }
// }
