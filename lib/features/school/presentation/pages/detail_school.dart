// part of '_school.dart';
//
// class DetailSchool extends StatefulWidget {
//   // final int id;
//   // DetailSchool({@required this.id});
//
//   @override
//   _DetailSchoolState createState() => _DetailSchoolState();
// }
//
// class _DetailSchoolState extends State<DetailSchool> {
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     final authState = GlobalState.auth();
//     var isPersonal = authState.state.currentState.type == 'PERSONAL';
//     return Scaffold(
//         appBar: AppBar(
//           backgroundColor: Colors.white,
//           shadowColor: Colors.transparent,
//           centerTitle: true,
//           title: TitleAppbar(
//               label: 'Detail ${isPersonal ? 'Media PJJ' : 'Sekolah'}'),
//           leading: IconButton(
//               icon: Icon(
//                 Icons.arrow_back,
//                 color: SiswamediaTheme.green,
//               ),
//               onPressed: () {
//                 Navigator.pop(context);
//               }),
//         ),
//         body: ListView(
//           children: [
//             FutureBuilder<ResultSchoolModel>(
//                 future: ClassServices.detailSchool(
//                     id: authState.state.currentState.schoolId),
//                 builder: (context, snapshot) {
//                   if (snapshot.connectionState == ConnectionState.waiting) {
//                     return Padding(
//                       padding: EdgeInsets.only(top: S.w / 3),
//                       child: Center(child: CircularProgressIndicator()),
//                     );
//                   } else {
//                     var data = snapshot.data.data;
//                     return Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       children: [
//                         Center(
//                             child: Container(
//                           margin: EdgeInsets.only(
//                               left: 20, right: 20, top: 24, bottom: 8),
//                           child: Column(
//                             children: [
//                               Text(
//                                 data.name,
//                                 textAlign: TextAlign.center,
//                                 style: boldBlack.copyWith(fontSize: S.w / 18),
//                               ),
//                               SizedBox(height: 10),
//                               Container(
//                                   padding: EdgeInsets.symmetric(
//                                       horizontal: 10, vertical: 5),
//                                   margin: EdgeInsets.only(bottom: 16),
//                                   decoration: BoxDecoration(
//                                       borderRadius: BorderRadius.circular(5),
//                                       color: data.isVerified
//                                           ? SiswamediaTheme.green
//                                           : Colors.red),
//                                   child: Text(
//                                     data.isVerified
//                                         ? 'Sudah Terverifikasi'
//                                         : 'Belum Terverifikasi',
//                                     style: TextStyle(
//                                         color: Colors.white, fontSize: 10),
//                                   )),
//                               Text(
//                                 '${isPersonal ? 'Penanggung Jawab' : 'Kepala Sekolah'} : ${data.namaKepalaSekolah}',
//                                 style: semiBlack.copyWith(fontSize: S.w / 28),
//                               ),
//                               SizedBox(height: 4),
//                               Text('Kontak : ${data.phoneNumber}',
//                                   style:
//                                       semiBlack.copyWith(fontSize: S.w / 28)),
//                               Container(
//                                 margin: EdgeInsets.symmetric(
//                                     horizontal: 20, vertical: 24),
//                                 child: Divider(
//                                   color: Colors.grey,
//                                 ),
//                               ),
//                             ],
//                           ),
//                         )),
//                         Container(
//                             margin:
//                                 EdgeInsets.only(left: 20, right: 20, bottom: 8),
//                             child: Column(
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               children: [
//                                 Text(
//                                   'Alamat : ',
//                                   style: semiBlack.copyWith(fontSize: S.w / 24),
//                                 ),
//                                 Text(data.address,
//                                     style:
//                                         descBlack.copyWith(fontSize: S.w / 32)),
//                                 SizedBox(height: 20),
//                                 Text(
//                                   'Kabupaten : ',
//                                   style: semiBlack.copyWith(fontSize: S.w / 24),
//                                 ),
//                                 Text(data.namaKabupaten,
//                                     style:
//                                         descBlack.copyWith(fontSize: S.w / 32)),
//                                 SizedBox(height: 20),
//                                 Text(
//                                   'Provinsi : ',
//                                   style: semiBlack.copyWith(fontSize: S.w / 24),
//                                 ),
//                                 Text(data.namaProvinsi,
//                                     style:
//                                         descBlack.copyWith(fontSize: S.w / 32)),
//                                 SizedBox(height: 20),
//                                 Text(
//                                   'Email : ',
//                                   style: semiBlack.copyWith(fontSize: S.w / 24),
//                                 ),
//                                 Text(data.schoolEmail,
//                                     style:
//                                         descBlack.copyWith(fontSize: S.w / 32)),
//                                 SizedBox(height: 20),
//                                 Text(
//                                   'Tipe ${isPersonal ? '' : 'Sekolah'} : ',
//                                   style: semiBlack.copyWith(fontSize: S.w / 24),
//                                 ),
//                                 Text(
//                                   data.type,
//                                   style: descBlack.copyWith(fontSize: S.w / 32),
//                                 ),
//                               ],
//                             )),
//                         SizedBox(height: 20),
//                         // SimpleContainer(
//                         //     // margin: EdgeInsets.symmetric(horizontal: S.w * .05),
//                         //     // height: S.w * .105,
//                         //     radius: BorderRadius.circular(12),
//                         //     color: SiswamediaTheme.green,
//                         //     child: Text('Hapus Instansi',
//                         //         style:
//                         //             semiWhite.copyWith(color: Colors.white))),
//                         // Container(
//                         //   color: SiswamediaTheme.green,
//                         //   child: Padding(
//                         //     padding: EdgeInsets.all(8),
//                         //     child: Text('Mantep dan keren'),
//                         //   ),
//                         // )
//                       ],
//                     );
//                   }
//                 })
//           ],
//         ));
//   }
// }
