part of '_entities.dart';

class InvitationForm extends Models {
  String code;
  String role;

  InvitationForm({this.code, this.role});
}
