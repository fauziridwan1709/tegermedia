part of '_entities.dart';

class School extends Models {
  int id;
  String invitationCode;
  String name;
  String provinsiId;
  String namaProvinsi;
  String kotaId;
  String namaKabupaten;
  String type;
  String grade;
  String address;
  String namaKepalaSekolah;
  String phoneNumber;
  String schoolEmail;
  String penanggungJawabEmail;
  String namaAdmin;
  bool isAdmin;
  bool personal;
  List<String> roles;
  bool isVerified;
  bool isApprove;
  String createdAt;
  String updatedAt;
  String deletedAt;

  School(
      {this.id,
      this.invitationCode,
      this.name,
      this.provinsiId,
      this.namaProvinsi,
      this.kotaId,
      this.namaKabupaten,
      this.type,
      this.isAdmin,
      this.personal,
      this.grade,
      this.address,
      this.namaKepalaSekolah,
      this.phoneNumber,
      this.schoolEmail,
      this.penanggungJawabEmail,
      this.namaAdmin,
      this.roles,
      this.isVerified,
      this.isApprove,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});
}
