part of '_repositories.dart';

abstract class SchoolRepository {
  Future<Decide<Failure, Parsed<List<School>>>> getAllSchool();
  Future<Decide<Failure, Parsed<ResultMessage>>> createSchool(SchoolModel model,
      {bool isPersonal = false});
  Future<Decide<Failure, Parsed<ResultMessage>>> joinSchool(
      InvitationFormModel model);
  Future<Decide<Failure, Parsed<bool>>> checkSchoolName(String schoolName);
  Future<Decide<Failure, Parsed<ResultMessage>>> updateSchool(
      SchoolModel model);
  Future<Decide<Failure, Parsed<ResultMessage>>> deleteSchool(int schoolId);
}
