import 'package:flutter/material.dart';
import 'package:share/share.dart';

class ShareService {

  static Future<void> share(BuildContext context, String textOrLink, String desc) async {
    final RenderBox box = context.findRenderObject();
    await Share.share(textOrLink,
        subject: desc, sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }
}