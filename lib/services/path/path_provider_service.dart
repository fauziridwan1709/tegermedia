import 'dart:io';

import 'package:path_provider/path_provider.dart';

class PathProviderService {
  static Future<Directory> get getTemp async => await getTemporaryDirectory();
}
