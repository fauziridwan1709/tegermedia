part of '_academic.dart';

class AcademicServices {
  static Future<AkademisModel> getData(String url) async {
    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var resp = await client.get(url).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));

      final data = json.decode(resp.body) as Map<String, dynamic>;
      return AkademisModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return AkademisModel(rows: []);
    } on SocketException catch (e) {
      print(e);
      return AkademisModel(rows: []);
    } catch (e) {
      return AkademisModel(rows: []);
    }
  }
}
