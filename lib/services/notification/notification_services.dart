part of '_notification.dart';

class NotificationServices {
  static Future<void> registerToken({String token}) async {
    var url = '$baseUrl/$version/notifications/register-token?token=$token';

    var prefs = await SharedPreferences.getInstance();
    var tokenAuth = prefs.getString('auth_siswamedia_api');

    try {
      var resp = await client.put(url, headers: {
        'Authorization': tokenAuth,
      });
      print(resp.body);
    } catch (e) {
      print(e);
    }
  }

  static Future<ModelNotification> getNotifications({String token}) async {
    var url =
        '$baseUrl/$version/notifications?limit=1000&page=1&sortby=id&order=desc';
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.get(url, headers: {'authorization': token});
    Utils.log(url, info: 'url');
    Utils.logWithDotes(resp.body, info: 'response body');
    var data = json.decode(resp.body) as Map<String, dynamic>;
    var result = ModelNotification.fromJson(data);

    if (result.statusCode == 200) {
      return result;
    } else {
      return ModelNotification(
          message: 'Terjadi Kesalahan', statusCode: 500, data: []);
    }
  }

  static Future<ModelCountNotif> getCountNotif({String token}) async {
    var url = '$baseUrl/$version/notifications/count';
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.get(url, headers: {'authorization': token});

    var data = json.decode(resp.body) as Map<String, dynamic>;

    var result = ModelCountNotif.fromJson(data);

    return result;
  }
}
