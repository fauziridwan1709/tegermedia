part of '_schedule.dart';

class GCalendarServices {
  static Future<void> getCalendarList() async {
    var url = '$baseGoogleApiUrl/calendar/v3/users/me/calendarList';
    var prefs = await SharedPreferences.getInstance();
    try {
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {'Authorization': token});
    } catch (e) {
      print(e);
    }
  }

  static Future<void> createCalendar({dynamic reqBody}) async {
    var url = '$baseGoogleApiUrl/calendar/v3/users/me/calendarList';
    var prefs = await SharedPreferences.getInstance();
    try {
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client
          .post(url, body: reqBody, headers: {'Authorization': token});
    } catch (e) {
      print(e);
    }
  }

  static Future<void> getCalendarById({int id}) async {
    var url = '$baseGoogleApiUrl/calendar/v3/users/me/calendarList/$id';
    var prefs = await SharedPreferences.getInstance();
    try {
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {'Authorization': token});
    } catch (e) {
      print(e);
    }
  }

  static Future<void> getAllEvents({String id}) async {
    var url = '$baseGoogleApiUrl/calendar/v3/users/me/calendarList/$id/events';
    var prefs = await SharedPreferences.getInstance();
    try {
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {'Authorization': token});
    } catch (e) {
      print(e);
    }
  }
}
