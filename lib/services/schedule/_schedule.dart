import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/ui/pages/Conference/_conference.dart';

import '../../states/global_state.dart';

part 'gcalender_services.dart';
part 'schedule_services.dart';
