part of '_schedule.dart';

class ScheduleServices {
  static Future<ScheduleModel> getAllScheduleByClass(
      {int classId, int schoolId, bool isOrtu = false}) async {
    // var url = '$baseUrl/$ver/schedules?limit=100&class_id=$classId';
    var isWaliKelas = GlobalState.auth().state.currentState.classRole.isWaliKelas;
    var url =
        '$baseUrl/$version/schedules/class/$classId${isOrtu ? '?type=ORTU' : ''}${isWaliKelas ? '?access=WALIKELAS' : ''}';

    print('url $url');
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');
      var resp = await client.get(url, headers: {'Authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      final data = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(url, info: 'url');
      Utils.logWithDotes(resp.body, info: 'data jadwal di absen');
      return ScheduleModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      throw TimeoutFailure();
    } on SocketException catch (e) {
      print(e);
      throw NetworkFailure();
    } catch (e) {
      print(e);
      print('error');
      throw GeneralFailure('Something wrong');
    }
  }

  static Future<GeneralResultAPI> createSchedule(
      {CreateScheduleModel reqBody, String id, BuildContext context, bool overlap}) async {
    var url = '$baseUrl/$version/schedules?overlap=$overlap';
    print('url ha');
    print(url);
    var _googleSignIn = SiswamediaGoogleSignIn.getInstance();
    await _googleSignIn.requestScopes(calendarScopes);
    try {
      await _googleSignIn.signIn();
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(reqBody.toJson()), headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('timeout membuat jadwal'));
      Utils.log(resp.statusCode, info: 'bikin jadwal status code');
      Utils.log(resp.body, info: 'bikin jadwal body');
      Map<String, dynamic> data = json.decode(resp.body);

      if (!resp.statusCode.isSuccessOrCreated) {
        if (data['data']['overlap'] != null) {
          return GeneralResultAPI(
              message: data['message'], statusCode: resp.statusCode, data: data['data']['overlap']);
        }
        return GeneralResultAPI(statusCode: resp.statusCode, message: data['message']);
      }
      var url2 = '$baseGoogleApiUrl/calendar/v3/calendars/$id/events?sendUpdates=all';
      var students = <Map<String, String>>[];
      //todo penting student
      // for (var i = 0; i < listProvider.students.data.length; i++) {
      //   print(listProvider.students.data[i].email);
      //   students.add({
      //     'email': listProvider.students.data[i].email,
      //     'displayName': listProvider.students.data[i].fullName,
      //   });
      // }

      final requestBody = <String, dynamic>{
        'start': {'dateTime': '${reqBody.startDate}'},
        'end': {'dateTime': '${reqBody.endDate}'},
        'summary': reqBody.name,
        'description': reqBody.description,
        'attendees': students
      };
      print(_googleSignIn.currentUser.email);

      var resp2 = await client
          .post(url2,
              body: json.encode(requestBody), headers: await _googleSignIn.currentUser.authHeaders)
          .timeout(Duration(seconds: 15),
              onTimeout: () => throw TimeoutException('gagal sinkron ke google calendar'));
      // print(resp2.body);
      return GeneralResultAPI(
          statusCode: StatusCodeConst.success, message: 'Berhasil membuat jadwal');
    } on SocketException {
      return GeneralResultAPI(
          statusCode: StatusCodeConst.noInternet, message: 'Tidak ada Internet');
    } on TimeoutException catch (e) {
      return GeneralResultAPI(statusCode: StatusCodeConst.timeout, message: e.message);
    } catch (e) {
      print(e);
      return GeneralResultAPI(
          statusCode: StatusCodeConst.general, message: 'Terjadi Kesalahan, coba lagi');
    }
  }

  static Future<ScheduleModelId> getScheduleById({int id}) async {
    var url = '$baseUrl/$version/schedules/$id';
    var box = Hive.box<dynamic>('users');
    String token = box.get('token');

    try {
      var resp = await client.get(url, headers: {'Authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      return ScheduleModelId.fromJson(json.decode(resp.body));
    } on TimeoutException catch (e) {
      print(e);
      return ScheduleModelId(
        statusCode: StatusCodeConst.timeout,
        message: e.message,
        data: null,
      );
    } on SocketException catch (e) {
      print('No connection');
      return ScheduleModelId(
        statusCode: StatusCodeConst.noInternet,
        message: e.message,
        data: null,
      );
    } catch (e) {
      print(e);
      return ScheduleModelId(statusCode: StatusCodeConst.general, message: e.message, data: null);
    }
  }

  static Future<DeleteScheduleIdModel> deleteScheduleById({int id}) async {
    var url = '$baseUrl/$version/schedules/$id';
    var pref = await SharedPreferences.getInstance();
    // var box = Hive.box<dynamic>('users');
    // String token = box.get('token');
    var token = await pref.getString(AUTH_KEY);

    try {
      var resp = await client.delete(url, headers: {'Authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print('---delete jadwal---');
      print(resp.body);
      return DeleteScheduleIdModel.fromJson(json.decode(resp.body));
    } on TimeoutException catch (e) {
      print(e);
      return DeleteScheduleIdModel(
        statusCode: StatusCodeConst.timeout,
        message: e.message,
      );
    } on SocketException catch (e) {
      print('No connection');
      return DeleteScheduleIdModel(
        statusCode: StatusCodeConst.noInternet,
        message: e.message,
      );
    } catch (e) {
      print(e);
      return DeleteScheduleIdModel(statusCode: StatusCodeConst.general, message: e.message);
    }
  }
}
