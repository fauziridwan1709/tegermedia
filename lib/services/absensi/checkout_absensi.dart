part of '_absensi.dart';

class CheckoutAbsensi {
  static Future<GeneralResultApi> create({
    CheckoutModel model,
  }) async {
    var url = '$baseUrl/$version/absensi/checkout';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(model.toJson()), headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));

      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      return GeneralResultApi(statusCode: StatusCodeConst.success, message: 'Sukses');
    } on TimeoutException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.general, message: e.message);
    }
  }
}
