part of '_absensi.dart';

class AbsensiService {
  static Future<StudentPermit> getDataIzin({String role, String id}) async {
    String url;
    url = '$baseUrl/$version/student-permit/class/$id?role=$role';
    print(url.toString());
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    print(token);

    var resp = await client.get(url, headers: {'authorization': token});

    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data.toString());
    StudentPermit result = StudentPermit.fromJson(data);

    print(result.statusCode);
    return result;
  }

  static Future<GlobalModel> deleteIzin(String id) async {
    var url = '$baseUrl/v2/student-permit/$id';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    print(url);
    print(token);
    var resp = await client.delete(url, headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    }).timeout(Duration(seconds: 10),
        onTimeout: () => throw TimeoutException('Timeout, try Again'));
    vUtils.setLog(resp.body.toString());
    vUtils.setLog(resp.statusCode.toString());
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data.toString());

    return GlobalModel.fromJson(data);
  }
}
