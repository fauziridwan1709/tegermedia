part of '_absensi.dart';

class SummaryAbsensi {
  static Future<AbsensiSummaryModel> getSummary({
    int classId,
  }) async {
    var authState = GlobalState.auth().state;
    var url =
        '$baseUrl/$version/absensi/summary/class?classID=$classId${authState.currentState.classRole.isOrtu ? '&userID=${authState.anak.first.studentId}' : ''}';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client
          .get(url, headers: {'Content-Type': 'application/json', 'Authorization': token}).timeout(
              Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      Utils.log(url, info: 'url');
      Utils.log(resp.statusCode, info: 'status code');
      Utils.logWithDotes(resp.body, info: 'data get summary absensi siswa');
      var data = json.decode(resp.body) as Map<String, dynamic>;
      return AbsensiSummaryModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return AbsensiSummaryModel(
        data: null,
        statusCode: StatusCodeConst.timeout,
      );
    } on SocketException catch (e) {
      print(e);
      return AbsensiSummaryModel(
        data: null,
        statusCode: StatusCodeConst.noInternet,
      );
    } catch (e) {
      print(e);
      return AbsensiSummaryModel(data: null, statusCode: 400);
    }
  }

  static Future<AbsensiSummaryModel> getSummaryUserId({
    int classId,
    int userId,
  }) async {
    var url = '$baseUrl/$version/absensi/summary/class?classID=$classId&userID=$userId';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client
          .get(url, headers: {'Content-Type': 'application/json', 'Authorization': token}).timeout(
              Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = json.decode(resp.body) as Map<String, dynamic>;
      return AbsensiSummaryModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return AbsensiSummaryModel(
        data: null,
        statusCode: StatusCodeConst.timeout,
      );
    } on SocketException catch (e) {
      print(e);
      return AbsensiSummaryModel(
        data: null,
        statusCode: StatusCodeConst.noInternet,
      );
    } catch (e) {
      return AbsensiSummaryModel(data: null, statusCode: 400);
    }
  }
}
