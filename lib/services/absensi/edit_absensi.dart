part of '_absensi.dart';

///call API for edit absensi feature
class EditAbsensiServices {
  static Future<EditAbsenModel> getAbsenMonth(int courseId,
      {int selectedMonth}) async {
    var month = selectedMonth ?? DateTime.now().month;
    var url =
        '$baseUrl/$version/absensi/course/month?courseID=$courseId&role=siswa&month=$month';
    var pref = await SharedPreferences.getInstance();
    var token = pref.getString(AUTH_KEY);
    var resp = await client.get(url, headers: {
      'Authorization': token,
    });
    print(resp.body);
    return EditAbsenModel.fromJson(
        json.decode(resp.body) as Map<String, dynamic>);
  }

  static Future<GeneralResultApi> updateAbsen({ChangeAbsenModel model}) async {
    var url = '$baseUrl/$version/absensi';
    var pref = await SharedPreferences.getInstance();
    var token = pref.getString(AUTH_KEY);
    var resp = await client.put(url,
        body: json.encode(model.toJson()),
        headers: {'Authorization': token, 'Content-Type': 'application/json'});
    Utils.log(url, info: 'url');
    Utils.log(model.toJson(), info: 'request body');
    Utils.logWithDotes(resp.body, info: 'data');
    return GeneralResultApi.fromJson(
        json.decode(resp.body) as Map<String, dynamic>);
  }
}
