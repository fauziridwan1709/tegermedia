part of '_absensi.dart';

class CreateAbsensi {
  static Future<ResultCreate> create({
    CreateAbsensiModel model,
  }) async {
    var url = '$baseUrl/$version/absensi';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(model.toJson()), headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));

      var data = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(resp.statusCode, info: 'status Code');
      Utils.log(data, info: 'data absen masuk');
      return ResultCreate(statusCode: resp.statusCode, id: data['data']['id']);
    } on TimeoutException catch (e) {
      print(e);
      return ResultCreate(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print('No connection');
      return ResultCreate(statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return ResultCreate(statusCode: 400, message: e.toString());
    }
  }

  static Future<GeneralResultApi> update({
    Map<String, dynamic> model,
  }) async {
    var url = '$baseUrl/$version/absensi';
    print('---update absen---');

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(model), headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      vUtils.setLog(resp.body);
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      print(model);
      return GeneralResultApi(statusCode: StatusCodeConst.success, message: 'Sukses');
    } on TimeoutException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.general, message: e.message);
    }
  }

  static Future<GlobalModel> createIzin({Map<String, dynamic> body}) async {
    var url = '$baseUrl/v2/student-permit';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    print(url);
    print(token);
    var resp = await client.post(url, body: json.encode(body), headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    }).timeout(Duration(seconds: 10),
        onTimeout: () => throw TimeoutException('Timeout, try Again'));
    vUtils.setLog(resp.body.toString());
    vUtils.setLog(resp.statusCode.toString());
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data.toString());
    return GlobalModel.fromJson(data);
  }

  static Future<GlobalModel> responseGuru({Map<String, dynamic> body, String id}) async {
    var url = '$baseUrl/v2/student-permit/$id/respond';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    var resp = await client.post(url, body: json.encode(body), headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    }).timeout(Duration(seconds: 10),
        onTimeout: () => throw TimeoutException('Timeout, try Again'));
    Utils.log(url, info: 'url');
    Utils.logWithDotes(body, info: 'data body');
    Utils.logWithDotes(resp.body, info: 'response body');
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data.toString());
    return GlobalModel.fromJson(data);
  }
}

class ResultCreate {
  ResultCreate({this.statusCode, this.id, this.message});
  final int id;
  final int statusCode;
  final String message;
}
