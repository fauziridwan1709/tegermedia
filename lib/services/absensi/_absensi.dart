import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/models/absensi/student_permit.dart';
import 'package:tegarmedia/models/global_model/golbal_model.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/states/global_state.dart';
import 'package:tegarmedia/utils/v_utils.dart';

part 'absensi_service.dart';
part 'checkout_absensi.dart';
part 'create_absensi.dart';
part 'edit_absensi.dart';
part 'summary_absensi.dart';
