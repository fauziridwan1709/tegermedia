import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/states/global_state.dart';
import 'package:tegarmedia/ui/pages/Conference/_conference.dart';
import 'package:tegarmedia/ui/pages/Forum/_forum.dart';

part 'filepicker_service.dart';
part 'forum_action.dart';
part 'forum_services.dart';
