part of '_forum.dart';

class ForumServices {
  static Future<ModelResultCreateId> createForum({ForumCreateModel reqBody}) async {
    var url = '$baseUrl/$version/forum';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var resp = await client.post(url, body: json.encode(reqBody.toJson()), headers: {
        'Authorization': token,
        'Content-Type': 'application/json',
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      Utils.log(url, info: 'url');
      Utils.logWithDotes(reqBody.toJson(), info: 'create forum');
      Utils.logWithDotes(resp.body, info: 'create forum');
      if (!resp.statusCode.isSuccessOrCreated) {
        throw BadRequestException(cause: resp.reasonPhrase, statusCode: resp.statusCode);
      }
      final data = json.decode(resp.body) as Map<String, dynamic>;
      return ModelResultCreateId.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return ModelResultCreateId(statusCode: StatusCodeConst.timeout);
    } on SocketException {
      print('No connection');
      return ModelResultCreateId(statusCode: StatusCodeConst.noInternet);
    } on BadRequestException catch (e) {
      return ModelResultCreateId(statusCode: e.statusCode);
    } catch (e) {
      print(e);
      throw BadRequestException(statusCode: 400);
    }
  }

  static Future<AllForum> getAllForum({int schoolId, bool isClass = false}) async {
    var classId = GlobalState.auth().state.currentState.classId;
    var param = '${isClass ? 'class_id=$classId&' : ''}school_id=$schoolId';
    var url = '$baseUrl/$version/forum?$param';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var resp = await client.get(url, headers: {'Authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      final data = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(url, info: 'all forum url');
      Utils.logWithDotes(resp.body, info: 'get all forum');
      return AllForum.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return AllForum(statusCode: StatusCodeConst.timeout, message: e.message, data: []);
    } on SocketException catch (e) {
      print(e);
      return AllForum(statusCode: StatusCodeConst.noInternet, message: e.message, data: []);
    } catch (e) {
      print(e);
      return AllForum(statusCode: StatusCodeConst.general, message: e.message, data: []);
    }
  }

  static Future<ForumDetailId> getForumById({int id}) async {
    var url = '$baseUrl/$version/forum/$id';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var resp = await client.get(url, headers: {'Authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var result = ForumDetailId.fromJson(json.decode(resp.body) as Map<String, dynamic>);
      print('---result---');
      print(result);
      return result;
    } on TimeoutException catch (e) {
      print(e);
      return ForumDetailId(data: null, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print(e);
      return ForumDetailId(data: null, statusCode: StatusCodeConst.noInternet);
    } catch (e) {
      print(e);
      return ForumDetailId(data: null, statusCode: StatusCodeConst.general);
    }
  }

  static Future<ResultDeleteForum> deleteForum({int id}) async {
    var url = '$baseUrl/$version/forum/$id';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');
      var resp = await client.delete(url, headers: {'Authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      return ResultDeleteForum.fromJson(json.decode(resp.body) as Map<String, dynamic>);
    } on TimeoutException catch (e) {
      print(e);
      return ResultDeleteForum(message: e.message, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print(e);
      return ResultDeleteForum(message: e.message, statusCode: StatusCodeConst.noInternet);
    } catch (e) {
      print(e);
      return ResultDeleteForum(message: 'Terjadi Kesalahan', statusCode: StatusCodeConst.general);
    }
  }

  static Future<GeneralResultAPI> updateForum({String judulForum, String deskripsi, int id}) async {
    var url = '$baseUrl/$version/forum';
    var dataMap = <String, dynamic>{'judul': judulForum, 'deskripsi': deskripsi, 'forum_id': id};
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var resp = await client.put(url, body: json.encode(dataMap), headers: {
        'Authorization': token,
        'Content-Type': 'application/json',
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      if (resp.statusCode != 200 && resp.statusCode != 201) {
        throw BadRequestException(cause: resp.reasonPhrase, statusCode: resp.statusCode);
      }
      print(resp.body);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      return GeneralResultAPI.fromJson(data);
    } on TimeoutException {
      return GeneralResultAPI(statusCode: StatusCodeConst.timeout);
    } on SocketException {
      return GeneralResultAPI(statusCode: 503);
    } on BadRequestException catch (e) {
      return GeneralResultAPI(statusCode: e.statusCode);
    } catch (e) {
      print(e);
      throw BadRequestException(statusCode: 400);
    }
  }

  static Future<ModelResultCreateId> comment({int id, ForumCommentModel reqBody}) async {
    var url = '$baseUrl/$version/forum/$id/reply';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var resp = await client.post(url, body: json.encode(reqBody.toJson()), headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp.body);
      print('------');
      return ModelResultCreateId.fromJson(json.decode(resp.body) as Map<String, dynamic>);
    } on TimeoutException catch (e) {
      print(e);
      return ModelResultCreateId(data: null, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print(e);
      return ModelResultCreateId(data: null, statusCode: StatusCodeConst.noInternet);
    } catch (e) {
      print(e);
      return ModelResultCreateId(data: null, statusCode: StatusCodeConst.general);
    }
  }
}
