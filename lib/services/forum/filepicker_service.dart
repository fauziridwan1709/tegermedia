part of '_forum.dart';

Future<Map<FileData, Uint8List>> getFile(
    {BuildContext context,
    List<String> extension,
    bool isImage = false}) async {
  try {
    var files = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowCompression: true,
        allowedExtensions: extension,
        allowMultiple: false);

    var file = files.files.first;

    var dataMap = <FileData, Uint8List>{};
    var dataFile = FileData(
      type: file.path.split('.').last,
      path: file.path,
      name: file.name,
    );
    if (isImage) {
      var finalFile = await compressFile(File(file.path));
      dataMap[dataFile] = finalFile.readAsBytesSync();
    } else {
      dataMap[dataFile] = file.bytes;
    }

    return dataMap;
  } catch (e) {
    print('Error');
    await FilePicker.platform.clearTemporaryFiles();
    print(e);
    return null;
  }
}

Future<dynamic> getFileAny({BuildContext context}) async {
  try {
    var files = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowMultiple: false);
    var file = files.files.first;
    return file.bytes;
  } catch (e) {
    print('Error');
    await FilePicker.platform.clearTemporaryFiles();
    print(e);
    return null;
  }
}

Future<File> compressFile(File file) async {
  var compressedFile = await FlutterNativeImage.compressImage(
    file.path,
    quality: 25,
  );
  return compressedFile;
}
