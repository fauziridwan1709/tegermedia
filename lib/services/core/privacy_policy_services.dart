part of '_core.dart';

class PrivacyPolicyServices {
  static Future<ModelTermAndConditions> getData() async {
    var url =
        '$prod/g2j/?id=15QRV-cgUysHp7Nccl3-mCjdXsT6T-QZ-rw1m-2HBUsM&sheet=5&columns=false';

    try {
      var resp = await client.get(url);

      print(resp.body);
      var data = json.decode(resp.body) as Map<String, dynamic>;
      return ModelTermAndConditions.fromJson(data);
    } catch (e) {
      print(e);
      return ModelTermAndConditions(rows: []);
    }
  }
}
