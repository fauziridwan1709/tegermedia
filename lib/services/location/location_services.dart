part of '_location.dart';

class WilayahServices {
  static Future<ModelListWilayah> getWilayah() async {
    var url = '$baseUrl/$version/wilayah/provinsi';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {'authorization': token});

      final data = json.decode(resp.body) as Map<String, dynamic>;

      return ModelListWilayah.fromJson(data);
    } catch (err) {
      print(err);
      return ModelListWilayah(statusCode: null, data: []);
    }
  }

  static dynamic getKabupaten(String id) async {
    var url = '$baseUrl/$version/wilayah/kabupaten?provinsi_id=$id';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {'authorization': token});

      final data = json.decode(resp.body) as Map<String, dynamic>;
      return data;
    } catch (err) {
      print(err);
    }
  }
}
