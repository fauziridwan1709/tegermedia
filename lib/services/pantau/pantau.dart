part of '_pantau.dart';

class PantauServices {
  static Future<GeneralResultApi> verify({String code, int classId}) async {
    var url = '$baseUrl/$version/pantau/';

    try {
      var reqMap = {'token': code, 'class_id': classId};
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');
      var resp = await client.post(url, body: json.encode(reqMap), headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp.body);
      return GeneralResultApi(statusCode: resp.statusCode, message: 'Sukses');
    } on TimeoutException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.general, message: e.message);
    }
  }
}
