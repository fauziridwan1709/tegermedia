
import 'dart:convert';

import 'dart:io';

class SocketUtil {
  Socket _socket;
  bool socketInit = false;
  static const String SERVER_IP = "10.0.2.2";
  static const int SERVER_PORT = 12345;

  Future<bool> sendMessage(String message) async {
    try {
      _socket.add(utf8.encode(message));
    } catch (e) {
      print(e.toString());
      return false;
    }
    return true;
  }

  Future<bool> initSocket(
      Function connectListener, Function messageListener) async {
    try {
      print('Connecting to socket');
      _socket = await Socket.connect(SERVER_IP, SERVER_PORT);
      connectListener(true);
      _socket.listen((List<int> event) {
        messageListener(utf8.decode(event));
      });
      socketInit = true;
    } catch (e) {
      print(e.toString());
      connectListener(false);
      return false;
    }
    return true;
  }

  void closeSocket() {
    _socket.close();
    _socket = null;
  }

  void cleanUp() {
    if (null != _socket) {
      _socket.destroy();
    }
  }
}