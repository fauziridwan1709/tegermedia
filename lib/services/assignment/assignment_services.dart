part of '_assignment.dart';

class AssignmentServices {
  static Future<ModelAssignmentsNew> getAssignment(
      {String param,
      String role,
      List<int> course,
      int page,
      int limit = 10}) async {
    try {
      var courses = course ?? [0];
      if (courses.isEmpty) {
        courses = [0];
      }
      final authState = GlobalState.auth().state;
      var filter = courses.join(',');
      var url =
          '$baseUrl/$version/me/${(role.isSiswaOrOrtu) ? 'assessments' : 'assessments-guru'}?page=$page&limit=$limit&$param${(role.isWaliKelas || (authState.currentState?.isAdmin ?? false)) ? '&access=WALIKELAS&course_id=$filter' : ''}';

      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {'authorization': token});
      var data = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(url, info: 'url');
      Utils.log(resp.statusCode, info: 'status code');
      Utils.logWithDotes(resp.body, info: 'data');
      var result = ModelAssignmentsNew.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      print('error');
    }
  }

  static Future<ModelAssessmentsTeacherById> getAssessmentsTeacherById(
      {@required int assessmentId}) async {
    var url = '$baseUrl/$version/assessments/$assessmentId';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {'authorization': token});
      var data = json.decode(resp.body) as Map<String, dynamic>;

      var result = ModelAssessmentsTeacherById.fromJson(data);
      return result;
    } catch (err) {
      print('error1');
      print(err);
      return ModelAssessmentsTeacherById(
          statusCode: null, message: 'Terjadi Kesalahan');
    }
  }

  static Future<ModelDetailAssessments> getDetail(
      {int assesmentID, int refrenceID}) async {
    var url =
        '$baseUrl/$version/assessments/$assesmentID/reference/$refrenceID';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {'authorization': token});
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);

      var result = ModelDetailAssessments.fromJson(data);
      return result;
    } catch (err) {
      return ModelDetailAssessments(
          statusCode: null, message: 'Terjadi Kesalahan');
    }
  }

  static Future<ModelResultCreateAssignment> postAssessmentsStudents(
      {int assesmentID,
      int refrenceID,
      ModelRefrenceAssessmentsStudens body}) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    var url =
        '$baseUrl/$version/assessments/$assesmentID/reference/$refrenceID';
    var reqBody = json.encode(body.toJson());

    print(reqBody);
    try {
      var resp = await client.put(url, body: reqBody, headers: {
        'Content-Type': 'application/json',
        'authorization': token
      });
      print(resp.body);

      var data = json.decode(resp.body) as Map<String, dynamic>;
      return ModelResultCreateAssignment(
          message: data['message'], statusCode: data['status_code']);
    } catch (err) {
      return ModelResultCreateAssignment(
          message: 'Terjadi Kesalahan', statusCode: 500);
    }
  }

  static Future<ModelResultCreateAssignment> postReply(
      {int assesmentID,
      int refrenceID,
      ModelRefrenceAssessmentsStudens body}) async {
    final data = <String, dynamic>{};
    if (body.attachments != null) {
      data['reply_attachments'] =
          body.attachments.map((v) => v.toJson()).toList();
    }
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    var url =
        '$baseUrl/$version/assessments/$assesmentID/reference/$refrenceID';
    var reqBody = json.encode(data);

    print(reqBody);
    try {
      var resp = await client.put(url, body: reqBody, headers: {
        'Content-Type': 'application/json',
        'authorization': token
      });
      print(resp.body);

      var data = json.decode(resp.body) as Map<String, dynamic>;
      return ModelResultCreateAssignment(
          message: data['message'], statusCode: data['status_code']);
    } catch (err) {
      return ModelResultCreateAssignment(
          message: 'Terjadi Kesalahan', statusCode: 500);
    }
  }

  static Future<ModelMeAssesmentsStatitistic> getAssessmentsStatistic(
      {int id}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/me/assessments/statistic';

      var resp = await client.get(url, headers: {'Authorization': token});
      var data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ModelMeAssesmentsStatitistic.fromJson(data);

      return result;
    } catch (e) {
      print(e);
      return ModelMeAssesmentsStatitistic(statusCode: 400);
    }
  }

  //todo
  static Future<ModelResultCreateAssignment> postAssessments(
      {Map<String, dynamic> body}) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    var url = '$baseUrl/$version/assessments';

    try {
      print(body);
      var reqBody = json.encode(body);

      var resp = await client.post(url, body: reqBody, headers: {
        'Content-Type': 'application/json',
        'authorization': token
      });

      var data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ModelResultCreateAssignment.fromJson(data);
      return result;
    } catch (err) {
      return ModelResultCreateAssignment(
          message: 'Terjadi Kesalahan', statusCode: 500);
    }
  }

  static Future<ModelAssignmentReference> getAssessmentsReference(
      {@required int assessmentsId}) async {
    var classId = GlobalState.auth().state.currentState.classId;
    var url =
        '$baseUrl/$version/assessments/$assessmentsId/reference?class_id=$classId';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {'authorization': token});
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = ModelAssignmentReference.fromJson(data);
      return result;
    } catch (err) {
      print(err);
      return ModelAssignmentReference(statusCode: null, data: []);
    }
  }

  static Future<ModelDetailAssessments> getAssessmentsReferenceDetail(
      {@required int assessmentsId, @required int referenceId}) async {
    var url =
        '$baseUrl/$version/assessments/$assessmentsId/reference/$referenceId';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {'authorization': token});
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = ModelDetailAssessments.fromJson(data);
      return result;
    } catch (err) {
      print(err);
      return ModelDetailAssessments(
          statusCode: 500, message: 'Terjadi Kesalahan');
    }
  }

  static Future<ModelScoreAssessments> createScoreAssignment(
      {@required int assessmentsId,
      @required int referenceId,
      @required String nilai}) async {
    var url =
        '$baseUrl/$version/assessments/$assessmentsId/reference/$referenceId/nilai?nilai=$nilai&status=Dikembalikan';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.put(url, headers: {'authorization': token});
      var data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ModelScoreAssessments.fromJson(data);
      return result;
    } catch (err) {
      return ModelScoreAssessments(
          statusCode: 500, message: 'Terjadi Kesalahan');
    }
  }

  static Future<ModelScoreAssessments> reply(
      {@required int assessmentsId,
      @required int referenceId,
      @required List<MainAttachments> att,
      String nilai}) async {
    var url =
        '$baseUrl/$version/assessments/$assessmentsId/reference/$referenceId/nilai?nilai=$nilai';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');
      final js = <String, dynamic>{};
      // var list = <Map<String, dynamic>>[];
      // att.forEach((element) {
      //   list.add({'file': element.file, 'file_name': element.fileName});
      // });
      print(nilai);
      js['reply_attachment'] = att.map((v) => v.toJson()).toList();

      print(js);
      var resp = await client.put(url, body: json.encode(js), headers: {
        'authorization': token,
        'Content-Type': 'application/json'
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(resp.body);
      var result = ModelScoreAssessments.fromJson(data);
      return result;
    } catch (err) {
      return ModelScoreAssessments(
          statusCode: 500, message: 'Terjadi Kesalahan');
    }
  }

  static Future<GeneralResultApi> updateAssessment(
      int id, Map<String, dynamic> reqBody) async {
    var url = '$baseUrl/$version/assessments/$id';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    print(reqBody);
    var resp = await client.put(url,
        body: json.encode(reqBody),
        headers: {'authorization': token, 'Content-Type': 'application/json'});
    print(resp.body);
    var data = json.decode(resp.body) as Map<String, dynamic>;
    return GeneralResultApi(
        statusCode: resp.statusCode, message: data['message'] ?? 'message');
  }

  static Future<GeneralResultApi> deleteAssessment(int id) async {
    var url = '$baseUrl/$version/assessments/$id';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.delete(url, headers: {'authorization': token});
    var data = json.decode(resp.body) as Map<String, dynamic>;
    return GeneralResultApi(
        statusCode: resp.statusCode, message: data['message'] ?? 'message');
  }

  static Future<ModelResultCreateId> createAssignmentV2(
      Map<String, dynamic> model) async {
    var url = '$baseUrl/$version/assessments-v2';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.post(url,
        body: json.encode(model),
        headers: {'authorization': token, 'Content-Type': 'application/json'});
    Utils.log(token, info: 'token');
    Utils.log(url, info: 'url');
    Utils.log(resp.statusCode, info: 'status code');
    Utils.log(model, info: 'model');
    Utils.logWithDotes(resp.body, info: 'data add assignment');
    var data = json.decode(resp.body) as Map<String, dynamic>;
    return ModelResultCreateId.fromJson(data);
  }

  static Future<ModelMultiSelectClass> getAssignmentV2() async {
    var schoolId = GlobalState.auth().state.currentState.schoolId;
    var url = '$baseUrl/$version/course-teach/$schoolId';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);

    var resp = await client.get(url, headers: {'authorization': token});
    var data = json.decode(resp.body) as Map<String, dynamic>;
    var result = ModelMultiSelectClass.fromJson(data);
    return result;
  }
}
