import 'dart:io';

import 'package:logger/logger.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/services/path/path_provider_service.dart';

class CacheImageServices {
  static bool isCached = false;
  static File imageFile;

  static Future<void> cacheImage(File file, {String filename}) async {
    Directory dir = await PathProviderService.getTemp;
    String newPath = '${dir.path}/${filename ?? file.path.filename}';
    Logger().d('Caching photo profile to this path: $newPath');
    await file.copy(newPath);
  }

  static Future<void> cachePhotoProfile(File file, int userId) async {
    await cacheImage(file, filename: 'userPP$userId');
  }

  static Future<void> isPhotoProfileCached(int userId) async {
    Directory dir = await PathProviderService.getTemp;
    File file = File('${dir.path}/userPP$userId');
    isCached = file.existsSync();
    if (isCached) {
      imageFile = file;
    }
  }
}
