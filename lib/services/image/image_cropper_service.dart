import 'dart:io';

import 'package:image_cropper/image_cropper.dart';
import 'package:tegarmedia/core/_core.dart';

class ImageCropperService {
  static List<CropAspectRatioPreset> get androidRatioPreset => [
        // CropAspectRatioPreset.original,
        CropAspectRatioPreset.square,
        // CropAspectRatioPreset.ratio3x2,
        // CropAspectRatioPreset.ratio4x3,
        // CropAspectRatioPreset.ratio16x9
      ];

  static List<CropAspectRatioPreset> get iOSRatioPreset => [
        ...androidRatioPreset,
        // CropAspectRatioPreset.ratio5x3,
        // CropAspectRatioPreset.ratio5x4,
        // CropAspectRatioPreset.ratio7x5,
      ];

  static Future<File> crop(File imageFile) async {
    var croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: Platform.isAndroid ? androidRatioPreset : iOSRatioPreset,
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Crop Image',
            activeControlsWidgetColor: SiswamediaTheme.green,
            toolbarColor: SiswamediaTheme.green,
            toolbarWidgetColor: SiswamediaTheme.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: true),
        iosUiSettings: IOSUiSettings(
          title: 'Crop Image',
        ));
    return croppedFile;
  }
}
