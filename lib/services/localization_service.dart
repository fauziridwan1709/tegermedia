import 'package:intl/intl.dart';

class LocalizationService {
  static String formatDate(String dateFormat, String value) {
    try {
      return DateFormat(dateFormat).format(
        DateTime.parse(value).toLocal(),
      );
    } catch (e) {
      return DateFormat(dateFormat).format(
        DateTime.parse(DateTime.now().toIso8601String()).toLocal(),
      );
    }
  }
}
