import 'dart:convert';
import 'dart:core';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/features/studyJournal/data/models/quiz.dart';
import 'package:tegarmedia/models/journey/courser_journey.dart';
import 'package:tegarmedia/models/journey/get_one_journey.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/states/global_state.dart';

import '../../models/assignment/_assignment.dart';
import '../../models/journey/courser_journey.dart';

part 'get_journey_services.dart';
