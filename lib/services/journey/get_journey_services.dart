part of '_journey.dart';

class GetJourneyServices {
  static Future<CourseJourney> getJourney(String courseId, String query) async {
    try {
      var url = '$baseUrl/$version/study-journal/course/$courseId?$query';

      print(url.toString());
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      print(token.toString());
      var resp = await client.get(url, headers: {'authorization': token});

      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = CourseJourney.fromJson(data);
      return result;
    } catch (ex) {
      throw Exception('Error $ex');
    }
  }

  static Future<List<Comment>> addComment(
      String journalId, Map<String, dynamic> query) async {
    try {
      var url = '$baseUrl/v2/study-journal/$journalId/comment';

      print(url.toString());
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      print(token.toString());
      var resp = await client
          .post(url, body: query, headers: {'authorization': token});

      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('Log.d => comment ' + data['data'].toString());
      var comment = <Comment>[];
      data['data'].forEach((dynamic val) {
        print('comment ' + val.toString());
        comment.add(Comment.fromJson(val as Map<String, dynamic>));
      });
      return comment;
    } catch (ex) {
      throw Exception('Error $ex');
    }
  }

  static Future<CourseJourneyData> getDetailJournal(String journalId) async {
    try {
      var url = '$baseUrl/v2/study-journal/$journalId';

      print(url.toString());
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      print(token.toString());
      var resp = await client.get(url, headers: {'authorization': token});

      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('Log.d => getDetailJournal ' + data.toString());
      GetCourseJourney journal = GetCourseJourney.fromJson(data);
      return journal.data;
    } catch (ex) {
      throw Exception('Error $ex');
    }
  }

  static Future<ModelAssignmentReference> getAssessment(String query) async {
    try {
      var auth = GlobalState.auth();
      var isSiswa = auth.state.currentState.classRole.isSiswaOrOrtu;
      var url =
          '$baseUrl/$version/me/assessments${isSiswa ? '' : '-guru'}?$query';

      print(url.toString());
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      print(token.toString());
      var resp = await client.get(url, headers: {'authorization': token});
      Utils.logWithDotes(resp.body, info: 'data asssessment');
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = ModelAssignmentReference.fromJson(data);
      return result;
    } catch (ex) {
      throw Exception('Error $ex');
    }
  }

  static Future<List<ListQuiz>> getQuiz(String query) async {
    try {
      var url = '$baseUrl/$version/me/quiz?$query';

      print(url.toString());
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      print(token.toString());
      var resp = await client.get(url, headers: {'authorization': token});

      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('get quize' + data.toString());
      var result = Quiz.fromJson(data);
      return result.data.listData;
    } catch (ex) {
      throw Exception('Error $ex');
    }
  }
}
