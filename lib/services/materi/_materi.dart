import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/provider/_provider.dart';
import 'package:tegarmedia/services/client.dart';

part 'materi_api_services.dart';
part 'materi_quiz_services.dart';
part 'materi_services.dart';
part 'student_rec_services.dart';
