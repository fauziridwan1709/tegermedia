part of '_materi.dart';

class MateriServices {
  static Future<MateriVideoModel> getVideoData(
    String kelas,
    String url,
  ) async {
    print(kelas);
    var now = DateTime.now();
    try {
      bool isWeb;
      try {
        if (!Platform.isWindows) {
          var box = Hive.box<dynamic>('materi');
          DateTime past = box.get('jadwalvideo$kelas}') ?? DateTime.now();
          if (box.get('video$kelas') != null &&
              now.difference(past) < Duration(hours: 1)) {
            print('caching??');
            Map<String, dynamic> result = box.get('video$kelas');
            return MateriVideoModel.fromJson(result);
          } else {
            var resp = await client.get(url);
            dynamic data = json.decode(resp.body) as Map<String, dynamic>;
            print('caching?');
            await box.put('video$kelas', data);
            await box.put('jadwalvideo$kelas', now);
            return MateriVideoModel.fromJson(data);
          }
        }
      } catch (e) {
        print(e);
        isWeb = true;
        return null;
      }
      if (isWeb) {
        var resp = await client.get(url);
        dynamic data = json.decode(resp.body) as Map<String, dynamic>;
        return MateriVideoModel.fromJson(data);
      }
    } catch (e) {
      print(e);
      return MateriVideoModel(rows: []);
    }
  }

  static Future<MateriDocModel> getDocData(
    String kelas,
    String url,
  ) async {
    print(kelas);

    var now = DateTime.now();

    try {
      print('try1');
      var box = Hive.box<dynamic>('materi');
      DateTime past = box.get('jadwaldoc$kelas') ?? DateTime.now();
      if (box.get('doc$kelas') != null &&
          now.difference(past) < Duration(hours: 1)) {
        Map<String, dynamic> result = box.get('doc$kelas');
        var hasil = MateriDocModel.fromJson(result);
        print('aduh');
        return hasil;
      } else {
        var resp = await client.get(url);
        dynamic data = json.decode(resp.body) as Map<String, dynamic>;
        await box.put('doc$kelas', data);
        await box.put('jadwaldoc$kelas', now);
        return MateriDocModel.fromJson(data);
      }
    } catch (e) {
      print('Masuk sini dong');
      print(e);
      return MateriDocModel(rows: []);
    }
  }

  static Future<MateriPresModel> getPresData(
    String kelas,
    String url,
  ) async {
    print(kelas);

    var now = DateTime.now();
    try {
      bool isWeb;
      try {
        if (!Platform.isWindows) {
          var box = Hive.box<dynamic>('materi');
          DateTime past = box.get('jadwalpres$kelas') ?? DateTime.now();
          if (box.get('pres$kelas') != null &&
              now.difference(past) < Duration(hours: 1)) {
            Map<String, dynamic> result = box.get('pres$kelas');
            print('caching?');
            return MateriPresModel.fromJson(result);
          } else {
            var resp = await client.get(url);
            dynamic data = json.decode(resp.body) as Map<String, dynamic>;
            await box.put('pres$kelas', data);
            print('ini nge cache');
            await box.put('jadwalpres$kelas', now);
            return MateriPresModel.fromJson(data);
          }
        }
      } catch (e) {
        print(e);
        isWeb = true;
      }
      if (isWeb) {
        var resp = await client.get(url);
        dynamic data = json.decode(resp.body) as Map<String, dynamic>;
        print('Berhasil');
        return MateriPresModel.fromJson(data);
      }
    } catch (e) {
      print(e);
      print('gagal');
      return MateriPresModel(rows: []);
    }
  }

  static Future<JadwalTayangModel> getJadwalData(
    String url,
  ) async {
    var now = DateTime.now();

    if (kIsWeb) {
      var resp = await client.get(url);
      dynamic data = json.decode(resp.body) as Map<String, dynamic>;
      return JadwalTayangModel.fromJson(data);
    } else {
      var box = Hive.box<dynamic>('materi');
      DateTime past = box.get('dateJadwalTayang}') ?? DateTime.now();
      print(past);
      if (box.get('jadwal') != null &&
          past != null &&
          now.difference(past) < Duration(days: 6)) {
        print('caching?');
        return JadwalTayangModel.fromJson(box.get('jadwal'));
      } else {
        await box.put('dateJadwalTayang', now);
        var resp = await client.get(url);
        dynamic data = json.decode(resp.body) as Map<String, dynamic>;
        print(data);
        print('bisa');
        await box.put('jadwal', data);
        return JadwalTayangModel.fromJson(data);
      }
    }
  }

  static Future<MateriVideoModel> getVidFavData(
    String jenjang,
    String kelasa,
  ) async {
    var kodeApi = JenjangData.getApiKodePelajaran(jenjang) ?? 5;
    var url =
        'https://api.siswamedia.com/g2j/?id=1MdNYQaxSb8Zq9TJpcCyD4OSeiTrx9kgrF3C0EO358gQ&sheet=$kodeApi&columns=false';
    var fileName = 'userDataFav$kodeApi.json';
    var prefs = await SharedPreferences.getInstance();
    var now = DateTime.now();
    var past = prefs.getString('dateFav') ?? now.toString();
    Directory dir;
    if (Platform.isIOS) {
      dir = await getApplicationDocumentsDirectory();
    } else {
      dir = await getTemporaryDirectory();
    }

    var file = File(dir.path + '/' + fileName);

    try {
      if (file.existsSync() &&
          now.difference(DateTime.parse(past)) < Duration(minutes: 30)) {
        print('Loading from cache');
        var jsonData = file.readAsStringSync();
        return MateriVideoModel.fromJson(json.decode(jsonData));
      } else {
        print('aneh');
        var resp = await client.get(url);
        dynamic data = json.decode(resp.body) as Map<String, dynamic>;
        print(data['rows']);
        /*
        if (kelas.contains('IPA')) {
          for (int i = 0; i < data['rows'].length; i++) {
            String id = data['rows'][i]['link'];
            String kelass = data['rows'][i]['kelas'].toString();
            if (kelass.contains('IPA')) {
              print(id);
              print('IPA');
              var api = await APIService.instance.fetchVideos(
                  id: id.substring(id.indexOf('=') + 1, id.indexOf('=') + 12));
              data['rows'][i]['likes'] = api.likes;
              data['rows'][i]['watch'] = api.watch;
              print(api.watch);
            } else {
              data['rows'][i]['likes'] = '0';
              data['rows'][i]['watch'] = '0';
            }
          }
        } else if (kelas.contains('IPS')) {
          for (int i = 0; i < data['rows'].length; i++) {
            String id = data['rows'][i]['link'];
            String kelass = data['rows'][i]['kelas'].toString();
            if (kelass.contains('IPS')) {
              print('IPS');
              print(id);
              var api = await APIService.instance.fetchVideos(
                  id: id.substring(id.indexOf('=') + 1, id.indexOf('=') + 12));
              data['rows'][i]['likes'] = api.likes;
              data['rows'][i]['watch'] = api.watch;
              print(api.watch);
            } else {
              data['rows'][i]['likes'] = '0';
              data['rows'][i]['watch'] = '0';
            }
          }
        } else {
          for (int i = 0; i < data['rows'].length; i++) {
            String id = data['rows'][i]['link'];
            print(id);
            var api = await APIService.instance.fetchVideos(
                id: id.substring(id.indexOf('=') + 1, id.indexOf('=') + 12));
            data['rows'][i]['likes'] = api.likes;
            data['rows'][i]['watch'] = api.watch;
          }
        }

        data['rows'].sort(
            (a, b) => (int.parse(b['watch'])).compareTo(int.parse(a['watch'])));
        print(data);


        */
        //var dio = Dio();
        //var init = await dio.download(data['rows'], dir.path + '/' + fileName);
        file.writeAsStringSync(json.encode(data),
            flush: true, mode: FileMode.write);

        return MateriVideoModel.fromJson(data);
      }
    } on SocketException catch (e) {
      print(e);
      return MateriVideoModel(rows: []);
    } catch (e) {
      print(e);
      return MateriVideoModel(rows: []);
    }
  }

  static Future<MateriVideoModel> getVideoDataSearch(
    String kelas,
    String url,
  ) async {
    print(kelas);
    try {
      var resp = await client.get(url).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      dynamic data = json.decode(resp.body) as Map<String, dynamic>;
      return MateriVideoModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return MateriVideoModel(rows: []);
    } on SocketException catch (e) {
      print(e);
      return MateriVideoModel(rows: []);
    } catch (e) {
      print(e);
      return MateriVideoModel(rows: []);
    }
  }

  static Future<MateriDocModel> getDocDataSearch(
    String kelas,
    String url,
  ) async {
    try {
      var resp = await client.get(url).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      dynamic data = json.decode(resp.body) as Map<String, dynamic>;
      return MateriDocModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return MateriDocModel(rows: []);
    } on SocketException catch (e) {
      print(e);
      return MateriDocModel(rows: []);
    } catch (e) {
      print(e);
      return MateriDocModel(rows: []);
    }
  }

  static Future<MateriPresModel> getPresDataSearch(
    String kelas,
    String url,
  ) async {
    print(kelas);

    try {
      var resp = await client.get(url).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      dynamic data = json.decode(resp.body) as Map<String, dynamic>;
      return MateriPresModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return MateriPresModel(rows: []);
    } on SocketException catch (e) {
      print(e);
      return MateriPresModel(rows: []);
    } catch (e) {
      print(e);
      return MateriPresModel(rows: []);
    }
  }
}
