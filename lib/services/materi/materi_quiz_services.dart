part of '_materi.dart';

class MateriQuizServices {
  static var client = Client();
  static Future<MateriQuizModel> getData({FilterBankSoal value}) async {
    var url =
        "https://api.siswamedia.com/g2j/?id=1VTedfoSXmis0lOoubDAA0kGG2pSNtT73fB9CGjQsQaU&sheet=2&columns=false${value != null ? "&q=${value.matapelajaran}" : ""}";

    try {
      print(value.matapelajaran);
      var resp = await client.get(url).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = json.decode(resp.body) as Map<String, dynamic>;
      return MateriQuizModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return MateriQuizModel(
        rows: [],
      );
    } on SocketException catch (e) {
      print(e);
      return MateriQuizModel(
        rows: [],
      );
    } catch (e) {
      print(e);
      return MateriQuizModel(rows: []);
    }
  }

  static Future<QuizModel> getQuizData(
    String kelas,
    String materi,
    String judul,
  ) async {
    var link = await getJson(kelas: kelas, materi: materi);

    var resp = await client.get('${link.result}&q=$judul');

    dynamic data = json.decode(resp.body) as Map<String, dynamic>;
    print(data);
    return QuizModel.fromJson(data);
  }

  static Future<LinkResultApi> getJson({String kelas, String materi}) async {
    print('data');
    print(kelas);
    print(materi);
    if (kelas.contains('11')) {
      kelas = '11';
    } else if (kelas.contains('12')) {
      kelas = '12';
    }
    var url =
        'https://api.siswamedia.com/g2j/?id=1GU89LkfJXwiVDSVcMk7tZ4-VexIQIVISQknxfDS_FNY&sheet=7&columns=false&q=$kelas PG - $materi';

    try {
      var resp = await client.get(url).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = json.decode(resp.body) as Map<String, dynamic>;
      var result = MateriQuizLink.fromJson(data['rows'][0]);
      print('--- result ---');
      return LinkResultApi(
          result: result.linkapi, statusCode: StatusCodeConst.success, message: 'Sukses');
    } on TimeoutException catch (e) {
      print(e);
      return LinkResultApi(
          result: 'noData', statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print(e);
      return LinkResultApi(
          result: 'noData', statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return LinkResultApi(
          result: 'noData', statusCode: StatusCodeConst.general, message: e.message);
    }
  }
}
