part of '_materi.dart';

const String API_KEY = 'AIzaSyBcUtN0aKXe8-eYqJGdiYjsljmw93N3zyM';

class APIService {
  APIService._instantiate();

  static final APIService instance = APIService._instantiate();

  final String _baseUrl = 'www.googleapis.com';
  String _nextPageToken = '';

  Future<List<Video>> fetchVideosFromPlaylist({String playlistId}) async {
    var parameters = {
      'part': 'snippet',
      'playlistId': playlistId,
      'maxResults': '23',
      'pageToken': _nextPageToken,
      'key': API_KEY,
    };
    var uri = Uri.https(
      _baseUrl,
      '/youtube/v3/playlistItems',
      parameters,
    );
    var headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    // Get Playlist Videos
    var response = await get(uri, headers: headers);
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as Map<String, dynamic>;

      _nextPageToken = data['nextPageToken'] ?? '';
      List<dynamic> videosJson = data['items'];
      print(videosJson);

      var videos = <Video>[];
      for (var i = 0; i < videosJson.length; i++) {
        videos.add(Video(
            thumbnailUrl:
                (videosJson[i]['snippet']['thumbnails']['high']['url'] == null)
                    ? ''
                    : videosJson[i]['snippet']['thumbnails']['high']['url'],
            id: videosJson[i]['snippet']['resourceId']['videoId'],
            title: videosJson[i]['snippet']['title']));
      }
      return videos;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }

  Future<VideoRate> fetchVideos({String id}) async {
    var parameters = {
      'part': 'id, statistics',
      'id': id,
      'maxResults': '500',
      'key': API_KEY,
    };
    var uri = Uri.https(
      _baseUrl,
      '/youtube/v3/videos',
      parameters,
    );
    var headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    // VideoRate videoRate = new VideoRate();
    var response = await get(uri, headers: headers);
    print(response.statusCode);
    if (response.statusCode == 200) {
      var data = json.decode(response.body) as Map<String, dynamic>;
      print(data);
      //print(data['items'][0]['statistics']['viewCount']);
      if (data['items'].length == 0) {
        return VideoRate(likes: '0', watch: '0');
      }
      return VideoRate(
          likes: data['items'][0]['statistics']['likeCount'],
          watch: data['items'][0]['statistics']['viewCount']);
    } else {
      return VideoRate(likes: '0', watch: '0');
    }
  }
}

class VideoRate {
  final String likes;
  final String watch;

  VideoRate({
    this.likes,
    this.watch,
  });
}

class Video {
  final String id;
  final String title;
  final String thumbnailUrl;

  Video({
    this.id,
    this.title,
    this.thumbnailUrl,
  });

  factory Video.fromMap(Map<String, dynamic> snippet) {
    return Video(
      id: snippet['resourceId']['videoId'],
      title: snippet['title'],
      thumbnailUrl: snippet['thumbnails']['high']['url'],
    );
  }
}
