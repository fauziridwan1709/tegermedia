part of '_conference.dart';

class ConferenceServices {
  static Future<ConferenceModel> createConference(
      {ConferenceDetail reqBody, int courseID}) async {
    var url = '$baseUrl/$version/confrences';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      if (courseID == 0) {
        var resp = await client
            .post(url, body: json.encode(reqBody.toJson()), headers: {
          'Authorization': token,
          'Content-Type': 'application/json',
        });
        return ConferenceModel.fromJson(
            json.decode(resp.body) as Map<String, dynamic>);
      } else {
        var resp = await client.post(url,
            body: jsonEncode(<String, dynamic>{
              'title': reqBody.title,
              'description': reqBody.description,
              'course_id': courseID,
              'date_start': reqBody.date,
              'duration': reqBody.duration,
              'url': reqBody.url
            }),

            // json.encode(reqBody.toJson()),
            headers: {
              'Authorization': token,
              'Content-Type': 'application/json',
            });
        return ConferenceModel.fromJson(
            json.decode(resp.body) as Map<String, dynamic>);
      }
    } catch (e) {
      print(e);
      return ConferenceModel(data: []);
    }
  }

  static Future<ConferenceListModel> getConference(
      {bool isOrtu = false}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var url = '$baseUrl/$version/confrences${isOrtu ? '?type=ORTU' : ''}';
      var resp = await client.get(url, headers: {'Authorization': token});
      Utils.log(url, info: 'url');
      Utils.logWithDotes(resp.body, info: 'data konferensi');
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ConferenceListModel.fromJson(data);

      return result;
    } catch (e) {
      print(e);
      return ConferenceListModel(data: []);
    }
  }
}
