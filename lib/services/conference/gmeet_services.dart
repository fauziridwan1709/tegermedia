part of '_conference.dart';

class GoogleMeet {
  static Future<MeetConference> createEvent({Map reqBody, String id, BuildContext context}) async {
    ///google calendar API
    var url = '$baseGoogleApiUrl/calendar/v3/calendars/$id/events?conferenceDataVersion=1';
    var _googleSignIn = SiswamediaGoogleSignIn.getInstance();

    try {
      CustomShowDialog(
        imageAsset: 'assets/kelas/konferensi.png',
        description: 'Sedang Membuat Link...',
      ).showTheDialog(context);
      await _googleSignIn.requestScopes(calendarScopes);
      await _googleSignIn.signIn();

      var resp = await client.post(url,
          body: json.encode(reqBody), headers: await _googleSignIn.currentUser.authHeaders);
      print('bisa gmeet');
      print(resp.statusCode);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      return MeetConference.fromJson(resp.statusCode, data);
    } on TimeoutException catch (e) {
      print(e);
      return MeetConference(statusCode: StatusCodeConst.timeout, conferenceData: ConferenceData());
    } on SocketException catch (e) {
      print(e);
      return MeetConference(
          statusCode: StatusCodeConst.noInternet, conferenceData: ConferenceData());
    } catch (e) {
      print(e);
      print('masuk');
      return MeetConference(statusCode: 400, conferenceData: ConferenceData());
    }
  }
}
