part of '_conference.dart';

Availability checkAvailability(Duration difference, Duration time) {
  print(difference);
  if (difference > Duration(minutes: 10)) {
    return Availability(status: 'Mulai', isAvailable: false);
  }
  if (difference > Duration(minutes: 0)) {
    return Availability(status: 'Mulai', isAvailable: true);
  } else if (difference > time) {
    return Availability(status: 'Sedang Berlangsung', isAvailable: true);
  }
  return Availability(status: 'Berakhir', isAvailable: false);
}
