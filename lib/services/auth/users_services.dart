part of '_auth.dart';

class UsersServices {
  ///implementation status: Complete
  static Future<int> signIn({Map<String, dynamic> reqData}) async {
    var url = '$baseUrlGoogle/v1/api/auth/login';
    // var url = '$baseUrl/$version/users/sign-in';

    var body = json.encode(reqData);
    var resp = await client.post(url, body: body, headers: {'Content-Type': 'application/json'});
    print('statusCode is ${resp.statusCode}');
    if (resp.statusCode == 422) {
      throw UnprocessableEntityException(cause: resp.bodyAsMap['messages'].join(','));
    } else if (resp.statusCode != 200) {
      throw BadRequestException(statusCode: resp.statusCode);
    }
    final data = json.decode(resp.body) as Map<String, dynamic>;
    print(data);
    var signInModel = UserSignInModel.fromJson(data);
    var prefs = await SharedPreferences.getInstance();
    Utils.log(signInModel.data.token, info: 'jwt token');
    await Pref.saveToken(signInModel.data.token);
    // await prefs.setString('auth_siswamedia_api', signInModel.data.token);
    await prefs.setString('refresh_token', signInModel.data.refreshToken);
    // await prefs.setBool('is_no_class', signInModel.data.isNoClasses);
    // await prefs.setBool('is_no_school', signInModel.data.isNoSchools);
    // await prefs.setString('nama', signInModel.data.nama);
    // await prefs.setString('email', signInModel.data.email);
    // await prefs.setString('user_id', signInModel.data.userID);
    return resp.statusCode;
  }

  ///implementation status: Complete
  static Future<SchoolModel> getMySchools() async {
    var url = '$baseUrl/$version/me/schools';
    var pref = await SharedPreferences.getInstance();
    var token = pref.getString(AUTH_KEY);
    var resp = await client
        .get(url, headers: {'Authorization': token, 'Content-Type': 'application/json'});
    if (!resp.statusCode.isSuccessOrCreated) {
      throw BadRequestException(statusCode: resp.statusCode);
    }
    final data = json.decode(resp.body) as Map<String, dynamic>;
    return SchoolModel.fromJson(data);
  }

  static Future<Map<int, String>> uploadImage() async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    // return null;
    // try {
    //   final picker = ImagePicker();
    //   var imageFile = await picker.getImage(source: ImageSource.gallery);
    //   var dataImage = File(imageFile.path).readAsBytesSync();
    //   var uri = Uri.parse('$baseUrl/v2/upload/file');
    //   var request = MultipartRequest('POST', uri)
    //     ..files.add(await MultipartFile.fromPath('file', imageFile.path));
    //   request.headers['authorization'] = token;
    //   var response = await request.send();
    //   // var url = '$baseUrl/v2/upload/file';
    //   // var resp = await client.post(url, body: dataImage, headers: {
    //   //   'authorization': token,
    //   //   'content-type': 'application/json'
    //   // });
    //   print(response.reasonPhrase);
    //   return 200;
    // } catch (e) {
    //   return 400;
    // }
    try {
      // final picker = ImagePicker();
      var imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
      var dataImage = File(imageFile.path).readAsBytesSync();
      var url = '$baseGoogleApiUrl/upload/drive/v3/files?uploadType=media&fields=*';
      var signIn = SiswamediaGoogleSignIn.getInstance();
      await signIn.requestScopes(driveScopes);
      await signIn.signInSilently();
      var header = await signIn.currentUser.authHeaders;
      var resp = await client.post(url,
          body: dataImage,
          // body: json.encode({
          //   'parents': ['$parent'],
          //   'originalFilename': 'nama',
          //   'contentHints': {'indexableText': '$coba'},
          // }),
          headers: header);
      // request.fields
      header['content-type'] = 'application/json';
      print(resp.body);
      var data = json.decode(resp.body) as Map<String, dynamic>;
      var resp2 = await client.post('$baseGoogleApiUrl/drive/v3/files/${data['id']}/permissions',
          body: json.encode({'role': 'reader', 'type': 'anyone'}), headers: header);
      print(resp2.body);
      // return resp.statusCode;
      return {200: data['thumbnailLink']};
    } catch (e) {
      print(e);
      return {400: 'null'};
    }
  }

  static Future<ResultUpdateProfile> updateProfile(
    // UserDetail body,
    DataUserProfile2 body,
  ) async {
    // var url = '$baseUrl/$version/me/profile';
    var url = '$baseUrlGoogle/v1/api/user/token';
    print('---body---');
    print(json.encode(body));
    print(url);
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    try {
      var reqBody = json.encode(body);
      var resp = await client.put(url, headers: await Pref.getSiswamediaHeader(), body: reqBody);
      dynamic data = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(url, info: 'url');
      Utils.log(resp.statusCode, info: 'status code');
      Utils.log(body, info: 'body');
      Utils.logWithDotes(resp.body, info: 'data update profile');

      return ResultUpdateProfile.fromJson(data);
    } catch (e) {
      return ResultUpdateProfile(statusCode: 500, message: 'Terjadi kesalahan...');
    }
  }

  @deprecated
  static Future<UserProfile> getMyProfile() async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    Utils.logWithDotes(token, info: 'token');
    var url = '$baseUrl/$version/me/profile';
    var resp = await client.get(
      url,
      headers: {'Authorization': token, 'Content-Type': 'application/json'},
    ).timeout(const Duration(seconds: 10));
    Utils.log(resp.statusCode, info: 'status code');
    Utils.logWithDotes(resp.body, info: 'profile body');

    if (resp.statusCode ~/ 200 != 1) {
      print('failed');
      throw SiswamediaException('Failed');
    }
    final data = json.decode(resp.body) as Map<String, dynamic>;
    print(data);
    return UserProfile.fromJson(data);
  }

  ///implementation status: completed
  static Future<ClassMeModel> getMyClass() async {
    var url = '$baseUrl/$version/me/classes';
    var prefs = await SharedPreferences.getInstance();
    try {
      var resp = await client.get(url, headers: {
        'Authorization': prefs.getString('auth_siswamedia_api'),
      });
      if (resp.statusCode != 200) {
        throw BadRequestException(statusCode: resp.statusCode);
      }
      dynamic data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ClassMeModel.fromJson(data);
      return result;
    } on SocketException {
      return ClassMeModel(data: [], statusCode: 503);
    } on BadRequestException catch (e) {
      return ClassMeModel(data: [], statusCode: e.statusCode);
    }
  }
}
