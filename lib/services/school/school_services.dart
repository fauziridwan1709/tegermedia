part of '_school.dart';

class SchoolServices {
  static dynamic getSchools(int provinsiID, int kotaID, String grade) async {
    var url =
        '$baseUrl/$version/schools?kota_id=$kotaID&provinsi_id=$provinsiID&grade=$grade&limit=1000';

    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {'authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));

      var data = json.decode(resp.body) as Map<String, dynamic>;

      return data;
    } on TimeoutException catch (e) {
      print(e);
      return null;
    } on SocketException catch (e) {
      print(e);
      return null;
    } catch (err) {
      return null;
    }
  }

  static Future<int> joinSchool({BuildContext context, @required Map reqData}) async {
    var url = '$baseUrl/$version/join-schools';

    //TODO exception Handler rapihin seragamin
    if (context != null) {
      SiswamediaTheme.loading(context);
    }
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var body = json.encode(reqData);
    var resp = await client.post(url, body: body, headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    }).timeout(Duration(seconds: 10),
        onTimeout: () => throw TimeoutException('Timeout, try Again'));
    if (resp.statusCode != 200) {
      throw SiswamediaException(resp.reasonPhrase);
    }

    dynamic data = json.decode(resp.body) as Map<String, dynamic>;
    return data;
  }

  static Future<JoinSchoolForm> createClass({
    int schoolID,
  }) async {
    var url = '$baseUrl/$version/join-schools-form';

    //TODO exception Handler [TimeoutException, SocketException]
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var body = json.encode({'school_id': schoolID});
    var resp = await client.post(url, body: body, headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    }).timeout(Duration(seconds: 10),
        onTimeout: () => throw TimeoutException('Timeout, try Again'));
    print(resp.body);
    var data = JoinSchoolForm.fromJson(json.decode(resp.body));
    return data;
  }

  static Future<ResultSchoolModel> approvalTeachers({
    int schoolID,
    int userId,
  }) async {
    var url = '$baseUrl/$version/schools/$schoolID/approval/$userId';

    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client
          .put(url, headers: {'Content-Type': 'application/json', 'Authorization': token}).timeout(
              Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = ResultSchoolModel.fromJson(json.decode(resp.body));
      return data;
    } on TimeoutException catch (e) {
      print(e);
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print('No connection');
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.noInternet);
    } catch (err) {
      print(err);
      return ResultSchoolModel(message: 'Terjadi Kesalahan', statusCode: 400);
    }
  }

  static Future<ResultSchoolModel> rejectRequestJoinTeacher({
    int schoolID,
    int userId,
  }) async {
    var url = '$baseUrl/$version/schools/$schoolID/approval/$userId';

    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.delete(url, headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = ResultSchoolModel.fromJson(json.decode(resp.body));
      return data;
    } on TimeoutException catch (e) {
      print(e);
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print('No connection');
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.noInternet);
    } catch (err) {
      return ResultSchoolModel(message: 'Terjadi Kesalahan', statusCode: 400);
    }
  }

  static Future<ModelSchoolApproval> getSchoolApproval({
    int schoolID,
  }) async {
    var url = '$baseUrl/$version/schools/$schoolID/approval';

    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client
          .get(url, headers: {'Content-Type': 'application/json', 'Authorization': token}).timeout(
              Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = ModelSchoolApproval.fromJson(json.decode(resp.body));
      return data;
    } on TimeoutException catch (e) {
      print(e);
      return ModelSchoolApproval(message: e.message, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print('No connection');
      return ModelSchoolApproval(message: e.message, statusCode: StatusCodeConst.noInternet);
    } catch (err) {
      return ModelSchoolApproval(message: 'Terjadi Kesalahan', statusCode: 400);
    }
  }

  static Future<ModelCountApproval> countWaitingApproval({
    int schoolID,
  }) async {
    var url = '$baseUrl/$version/schools/$schoolID/approval-count';

    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client
          .get(url, headers: {'Content-Type': 'application/json', 'Authorization': token}).timeout(
              Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = ModelCountApproval.fromJson(json.decode(resp.body));
      return data;
    } on TimeoutException catch (e) {
      print(e);
      return ModelCountApproval(statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print(e);
      return ModelCountApproval(statusCode: StatusCodeConst.noInternet);
    } catch (err) {
      return ModelCountApproval(statusCode: 400);
    }
  }
}
