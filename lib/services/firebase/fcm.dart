import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:logger/logger.dart';

class FCM {
  static FirebaseMessaging _fcm;
  static bool isSubscribed = false;

  /// Returns an instance using the default [FirebaseApp].
  static FirebaseMessaging get instance {
    return _fcm ??= FirebaseMessaging.instance;
  }

  static Future<void> subscribeToTopic(String topic) async {
    try {
      print('--options---');
      print(FCM.instance.app.options);
      await FCM.instance.subscribeToTopic(topic);
      isSubscribed = true;
    } catch (e) {
      Logger().d(e.toString());
    }
  }

  static Future<void> unsubscribeFromTopic(String topic) async {
    try {
      await FCM.instance.unsubscribeFromTopic(topic);
      isSubscribed = false;
    } catch (e) {
      Logger().d(e.toString());
    }
  }
}
