part of '_home.dart';

class HomeServices {
  static Future<HomeModel> getData(String type, {int typeWeb}) async {
    var url =
        'https://api.siswamedia.com/g2j/?id=11hYfS45oX0rmzq6JBrCWFSYlNjkoGmXER0dzatCIIaE&sheet=${getCode(type)}&columns=false';

    var now = DateTime.now();

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    var directory = await Utils.cachePath();
    var box = Hive.box<dynamic>('home');
    DateTime past = box.get('date${getCode(type)}');
    // if (true) {
    if (box.get('home${getCode(type)}') != null && now.difference(past) < Duration(hours: 12)) {
      dynamic localData = box.get('home${getCode(type)}');
      // Logger().d(localData);

      // var data = HomeModel.fromJson(jsonDecode(dota));
      var data = HomeModel.fromJson(localData);
      print('cached');
      for (var element in data.rows) {
        element.cached = true;
        element.gambar = '$directory/${element.judul.split('/').join('')}.jpg';
      }
      return data;
    } else {
      await box.put('date${getCode(type)}', now);
      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      Utils.logWithDotes(url, info: 'url');
      Utils.logWithDotes(resp.body, info: 'response body');
      var data = json.decode(resp.body) as Map<String, dynamic>;
      await box.put('home${getCode(type)}', json.decode(resp.body));
      var dataModel = HomeModel.fromJson(data);

      for (var data in dataModel.rows) {
        Utils.cacheImageFromUrl(data.gambar, data.judul);
        data.cached = false;
      }
      return dataModel;
    }
  }

  static int getCode(String type) {
    var list = <String>[
      'SERTIMEDIA NEWS',
      'SCHOOL NEWS',
      'DONASI',
      'ACARA',
      'SPESIAL BUAT KAMU',
      'SISWAMEDIA YOUTUBE'
    ];
    return list.indexOf(type) + 1;
  }

  static Future<void> createContent({String reqBody}) async {
    var url = '$baseUrl/$version/contents';
    var box = Hive.box<dynamic>('users');
    var resp = await client.post(url, body: <String, dynamic>{
      'name': reqBody
    }, headers: {
      'Authorization': box.get('token'),
    });
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data);
  }

  static Future<void> deleteContent({String reqBody}) async {
    var url = '$baseUrl/$version/contents';
    var box = Hive.box<dynamic>('users');
    var resp = await client.delete(url, headers: {
      'Authorization': box.get('token') as String,
    });
    var data = json.decode(resp.body) as Map<String, dynamic>;
  }

  static Future<HomeModel> getContent(String type) async {
    var url =
        '$baseUrl/$version/contents?category_name=${getCode(type).toString().toUpperCase()}&status=PUBLISHED';
    var box = Hive.box<dynamic>('home');
    try {
      var now = DateTime.now();

      DateTime past = box.get('date${getCode(type)}');
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      if (box.get('home${getCode(type)}') != null && now.difference(past) < Duration(hours: 2)) {
        print('Loading from cache');
        return HomeModel.fromJson(box.get('home${getCode(type)}'));
      } else {
        await box.put('date${getCode(type)}', now);
        var resp = await client.get(url, headers: {
          'Authorization': token,
        });
        print(resp.body);
        var data = json.decode(resp.body) as Map<String, dynamic>;
        await box.put('home${getCode(type)}', data);
        return HomeModel.fromJson(data);
      }
    } catch (e) {
      print(e);
      return HomeModel.fromJson(e);
    }
  }
}
