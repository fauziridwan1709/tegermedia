part of '_class.dart';

class Oauth2 {
  static var client = Client();
  static var messageExist = 'record_already_exist';
  static Future<void> blabla({String url}) async {}

  static Future<OauthTokenUrl> getOauthUrl() async {
    var url = '$baseUrlGoogle/$gSVersion/api/oauth/classroom/tokenurl';
    print("$url");
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    var resp =
        await client.get(url, headers: {'Authorization': 'Bearer $token'});
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data);
    if (resp.statusCode == 422 && data['messages'][0] == messageExist) {
      print('masuk');
      throw AlreadyExistException(message: 'Record already exist');
    }
    return OauthTokenUrl.fromJson(
        json.decode(resp.body) as Map<String, dynamic>);
  }

  static Future<String> getLoginUrl() async {
    var url = '$baseUrlGoogle/$gSVersion/api/auth/google';
    print("$url");
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    var resp =
        await client.get(url, headers: {'Authorization': 'Bearer $token'});
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data);
    return data['data'] as String;
  }
}
