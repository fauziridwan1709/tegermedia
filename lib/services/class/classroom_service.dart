part of '_class.dart';

class ClassroomServices {
  static var client = Client();
  static Future<ClassroomCourse> getCoursesByTeacher({Email email}) async {
    var url = 'https://classroom.googleapis.com/v1/courses?teacherId=me';

    var signIn = SiswamediaGoogleSignIn.getInstance();
    await signIn.requestScopes(classroomScopes);
    await signIn.signIn();
    var pref = await SharedPreferences.getInstance();
    var token = pref.getString(AUTH_KEY);
    var bearerToken = 'Bearer $token';
    var resp = await client.get(url, headers: await signIn.currentUser.authHeaders);
    print(resp.body);
    return ClassroomCourse.fromJson(json.decode(resp.body) as Map<String, dynamic>);
  }

  static Future<ClassroomTeachersData> getAllTeachers({String courseId}) async {
    var url = 'https://classroom.googleapis.com/v1/courses/$courseId/teachers';

    var pref = await SharedPreferences.getInstance();
    var signIn = SiswamediaGoogleSignIn.getInstance();
    await signIn.requestScopes(classroomScopes);
    await signIn.signIn();
    var token = pref.getString(AUTH_KEY);
    var bearerToken = 'Bearer $token';
    var resp = await client.get(url, headers: await signIn.currentUser.authHeaders);
    print(resp.body);
    return ClassroomTeachersData.fromJson(json.decode(resp.body) as Map<String, dynamic>);
  }

  static Future<ClassroomStudentsData> getAllStudents({String courseId}) async {
    // var url =
    //     '$baseUrlGoogle/$gSVersion/api/googleClassroom/student?course_id=$courseId&limit=50';
    var url = 'https://classroom.googleapis.com/v1/courses/$courseId/students';

    var pref = await SharedPreferences.getInstance();
    var signIn = SiswamediaGoogleSignIn.getInstance();
    await signIn.requestScopes(classroomScopes);
    await signIn.signInSilently();
    var token = pref.getString(AUTH_KEY);
    var bearerToken = 'Bearer $token';
    var resp = await client.get(url, headers: await signIn.currentUser.authHeaders);
    print(resp.body);
    return ClassroomStudentsData.fromJson(json.decode(resp.body) as Map<String, dynamic>);
  }
}
