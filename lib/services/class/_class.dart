import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/states/global_state.dart';
import 'package:tegarmedia/ui/pages/Conference/_conference.dart';
import 'package:tegarmedia/utils/v_utils.dart';

part 'class_services.dart';
part 'classroom_create_service.dart';
part 'classroom_service.dart';
part 'oauth2.dart';
part 'teacher_services.dart';
