part of '_class.dart';

var box = Hive.box<dynamic>('me');

class ClassServices {
  static Future<ModelListCourse> getMeCourse({bool cache = true}) async {
    var url = '$baseUrl/$version/me/courses?type=PERSONAL';

    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {
        'Authorization': token,
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      final data = json.decode(resp.body) as Map<String, dynamic>;
      return ModelListCourse.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return ModelListCourse(statusCode: StatusCodeConst.timeout, data: []);
    } on SocketException catch (e) {
      print(e);
      return ModelListCourse(statusCode: StatusCodeConst.noInternet, data: []);
    } catch (e) {
      return ModelListCourse(data: []);
    }
  }

  static Future<SchoolModel> getMeSchool({bool cache = true}) async {
    var url = '$baseUrl/$version/me/schools';

    //TODO exception Handler [TimeoutException, SocketException]
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {
        'Authorization': token,
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      final data = json.decode(resp.body) as Map<String, dynamic>;

      return SchoolModel.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return SchoolModel(message: e.message, statusCode: StatusCodeConst.timeout, data: []);
    } on SocketException catch (e) {
      print('No connection');
      return SchoolModel(message: e.message, statusCode: StatusCodeConst.noInternet, data: []);
    } catch (e) {
      return SchoolModel(data: []);
    }
  }

  static Future<ResultCreateClassModel> createClass(
    String name,
  ) async {
    var url = '$baseUrl/$version/courses/personal-teachers';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      await prefs.setString('isRole', 'GURU');

      var resp = await client.post(url, body: {'name': name}, headers: {'Authorization': token});
      final data = json.decode(resp.body) as Map<String, dynamic>;

      var result = ResultCreateClassModel.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      return ResultCreateClassModel(statusCode: 400);
    }
  }

  static Future<ResultCreateClassModel> createSchool({Map reqBody}) async {
    var url = '$baseUrl/$version/schools';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var body = json.encode(reqBody);

      var resp = await client.post(url, body: body, headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      }).timeout(GLOBAL_TIMEOUT);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = ResultCreateClassModel.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      return ResultCreateClassModel(statusCode: 500, message: 'Terjadi Kesalahn');
    }
  }

  static Future<int> joinCourse({Map<String, String> reqData}) async {
    var url = '$baseUrl/$version/join-course';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var body = json.encode(reqData);
      var resp = await client.post(url,
          body: body, headers: {'Content-Type': 'application/json', 'Authorization': token});

      var data = json.decode(resp.body) as Map<String, dynamic>;
      return resp.statusCode;
    } catch (err) {
      print(err);
      return 400;
    }
  }

  ///almost implemented
  static Future<ResultInvitation> joinClass(
      {BuildContext context, @required Map<String, dynamic> reqData}) async {
    var url = '$baseUrl/$version/join-classes';

    try {
      if (context != null) {
        SiswamediaTheme.loading(context);
      }
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var body = json.encode(reqData);
      var resp = await client.post(url,
          body: body, headers: {'Content-Type': 'application/json', 'Authorization': token});
      final data = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(url, info: 'url');
      Utils.log(resp.statusCode, info: 'status code');
      Utils.logWithDotes(resp.body, info: 'data join sebagai orang tua');
      if (resp.statusCode != 200 && resp.statusCode != 201) {
        throw SiswamediaException(data['message']);
      }

      var result = ResultInvitation.fromJson(data);
      return result;
    } catch (e) {
      return ResultInvitation(
          statusCode: 400, message: e is SiswamediaException ? e.message : e.toString());
    }
  }

  static Future<ClassByRefrence> getRefrenceByCourse(int id, String role, {String type}) async {
    var url =
        '$baseUrl/$version/courses/$id/reference?reference_role=$role${type != null ? '&reference_type=$type' : ''}';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      await prefs.setString('id', id.toString());

      var resp = await client.get(url, headers: {'Authorization': token});
      final data = json.decode(resp.body) as Map<String, dynamic>;

      var result = ClassByRefrence.fromJson(data);
      print(id);
      print(role);
      print(result.toJson());
      return result;
    } catch (e) {
      return ClassByRefrence(data: []);
    }
  }

  static Future<ModelMeCheck> getMeCheck() async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/me/check';
      var resp = await client.get(url, headers: {'Authorization': token});
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ModelMeCheck.fromJson(data);

      return result;
    } catch (e) {
      return ModelMeCheck(statusCode: 400);
    }
  }

  static Future<ModelDetailCourse> detailCourse({@required int id}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/courses/$id';
      var resp = await client.get(url, headers: {'Authorization': token});
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ModelDetailCourse.fromJson(data);
      return result;
    } catch (e) {
      return ModelDetailCourse(statusCode: 400);
    }
  }

  ///implementation status: completed
  static Future<ResultSchoolModel> detailSchool({@required int id}) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    var url = '$baseUrl/$version/schools/$id';
    var resp = await client.get(url, headers: {'Authorization': token});
    if (resp.statusCode != 200) {
      throw SiswamediaException(resp.reasonPhrase);
    }
    final data = json.decode(resp.body) as Map<String, dynamic>;
    var result = ResultSchoolModel.fromJson(data);

    return result;
  }

  static Future<ResultInvitation> createClassBySchool({Map<String, dynamic> reqBody}) async {
    try {
      Utils.logWithDotes(reqBody, info: 'manteeeeeeeeep');
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/classes';
      var body = ModelCreateClass.fromJson(reqBody);
      var bodyEncode = json.encode(body.toJson());

      var resp = await client.post(url, body: bodyEncode, headers: {
        'Authorization': token,
        'Content-Type': 'application/json',
      });
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ResultInvitation.fromJson(data);

      return result;
    } catch (e) {
      return ResultInvitation(statusCode: 400);
    }
  }

  static Future<ResultCreateClassModel> createCourse({Map<String, dynamic> reqBody}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/courses';
      var bodyEncode = json.encode(reqBody);

      var resp = await client.post(url, body: bodyEncode, headers: {
        'Authorization': token,
        'Content-Type': 'application/json',
      });
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ResultCreateClassModel.fromJson(data);
      return result;
    } catch (e) {
      return ResultCreateClassModel(statusCode: 400);
    }
  }

  static Future<ResultCreateClassModel> updateCourse({Map<String, dynamic> reqBody, int id}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/courses/$id';
      var bodyEncode = json.encode(reqBody);

      var resp = await client.put(url, body: bodyEncode, headers: {
        'Authorization': token,
        'Content-Type': 'application/json',
      });
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ResultCreateClassModel.fromJson(data);
      return result;
    } catch (e) {
      return ResultCreateClassModel(statusCode: 400);
    }
  }

  static Future<ResultCreateClassModel> updateWaliClass(
      {Map<String, dynamic> reqBody, int id}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/classes/$id/promote-teacher-to-walikelas';
      var bodyEncode = json.encode(reqBody);

      var resp = await client.put(url, body: bodyEncode, headers: {
        'Authorization': token,
        'Content-Type': 'application/json',
      });
      vUtils.setLog(resp.body);
      vUtils.setLog(id);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ResultCreateClassModel.fromJson(data);
      return result;
    } catch (e) {
      return ResultCreateClassModel(statusCode: 400);
    }
  }

  static Future<ResultCreateClassModel> deleteCourse({int id}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/courses/$id';

      var resp = await client.delete(url, headers: {
        'Authorization': token,
        'Content-Type': 'application/json',
      });
      Utils.log(url, info: 'url');
      Utils.logWithDotes(resp.body, info: 'data delete course');
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ResultCreateClassModel.fromJson(data);
      return result;
    } catch (e) {
      return ResultCreateClassModel(statusCode: 400);
    }
  }

  static Future<ClassBySchoolModel> getClassBySchoolID(
      {int id, bool param = false, bool cache = true}) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);

    var onlyMe = GlobalState.auth().state.currentState.isAdmin ? 'false' : param.toString();
    var url = '$baseUrl/$version/schools/$id/classes?onlyme=$onlyMe&limit=1000';

    var resp = await client.get(url, headers: await Pref.getHeader());
    final data = json.decode(resp.body) as Map<String, dynamic>;
    Utils.log(url, info: 'url');
    Utils.log(url, info: 'url');
    Utils.log(resp.statusCode, info: 'status code');
    Utils.logWithDotes(resp.body, info: 'data get all class by school id');
    return ClassBySchoolModel.fromJson(data);
  }

  static Future<ModelDetailClassId> getClassByID({int id}) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);

    var url = '$baseUrl/$version/classes/$id';
    var resp = await client.get(url, headers: {'Authorization': token});
    Utils.log(url, info: 'url');
    Utils.logWithDotes(resp.body, info: 'data get class by id');
    final data = json.decode(resp.body) as Map<String, dynamic>;
    var result = ModelDetailClassId.fromJson(data);

    return result;
  }

  static Future<CourseByClassModel> getCourseByClass({int classId}) async {
    var url = '$baseUrl/$version/classes/$classId/courses?type=SEKOLAH&limit=1000&sortby=id';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      print(resp.body);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = CourseByClassModel.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      return CourseByClassModel(data: []);
    }
  }

  static Future<ModelAllClass> getAllClass() async {
    var url = '$baseUrl/$version/classes';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ModelAllClass.fromJson(data);
      return result;
    } catch (e) {
      return ModelAllClass(data: []);
    }
  }

  static Future<TeachersClassroomModel> getTeacherClassroom({int id}) async {
    var url = '$baseUrl/$version/classes/$id/teachers';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      final data = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(url, info: 'url');
      Utils.log(resp.statusCode, info: 'status code');
      Utils.logWithDotes(resp.body, info: 'data list teacher');
      var result = TeachersClassroomModel.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      return TeachersClassroomModel(data: []);
    }
  }

  static Future<StudentsClassroomModel> getStudentClassRoom({int id}) async {
    var url = '$baseUrl/$version/classes/$id/students';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      final data = json.decode(resp.body) as Map<String, dynamic>;
      print('mantap');
      Utils.log(url, info: 'url');
      Utils.log(resp.statusCode, info: 'status code');
      Utils.logWithDotes(resp.body, info: 'data get student classroom');
      var result = StudentsClassroomModel.fromJson(data);
      return result;
    } on SocketException {
      return StudentsClassroomModel(statusCode: 503);
    } catch (e) {
      return StudentsClassroomModel(statusCode: 400, data: []);
    }
  }

  static Future<ResultCreateClassModel> setCurrentState({dynamic data}) async {
    var url = '$baseUrl/$version/me/current-class?data=$data';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.put(url, headers: {
        'Authorization': token,
      }).timeout(Duration(seconds: 10), onTimeout: () => throw TimeoutException('Try Again'));
      print(resp.body);

      final data = json.decode(resp.body) as Map<String, dynamic>;
      return ResultCreateClassModel.fromJson(data);
    } on TimeoutException catch (e) {
      return ResultCreateClassModel(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      return ResultCreateClassModel(
          statusCode: StatusCodeConst.noInternet, message: 'Tidak ada Internet');
    } catch (e) {
      return ResultCreateClassModel(statusCode: 500, message: 'Terjadi kesalahan');
    }
  }

  static Future<ModelCurrentClass> getCurrentClass({dynamic data}) async {
    var url = '$baseUrl/$version/me/current-class';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      print('get current class');
      print(resp.body);
      print(resp.statusCode);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      return ModelCurrentClass.fromJson(data);
    } catch (e) {
      print(e);
      print('masuk');
      return ModelCurrentClass(statusCode: 500);
    }
  }

  static Future<GeneralResultAPI> updateClass({ClassBySchoolData detailClass}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/classes/${detailClass.id}';
      Utils.logWithDotes(detailClass.description, info: 'class description');
      var newData = <String, dynamic>{
        'name': detailClass.name,
        'tingkat': detailClass.tingkat,
        'tahun_ajaran': detailClass.tahunAjaran,
        'semester': detailClass.semester,
        'description': detailClass.description,
        'jurusan': detailClass.jurusan,
        'school_id': detailClass.schoolId
      };

      var resp = await client.put(url,
          body: json.encode(newData),
          headers: {'Authorization': token, 'Content-Type': 'application/json'});
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = GeneralResultAPI.fromJson(data);
      return result;
    } on SocketException {
      return GeneralResultAPI(statusCode: StatusCodeConst.noInternet, message: 'No Internet');
    } on TimeoutException {
      return GeneralResultAPI(statusCode: StatusCodeConst.timeout, message: 'Timeout');
    } catch (e) {
      return GeneralResultAPI(statusCode: StatusCodeConst.general, message: 'failed');
    }
  }

  static Future<GeneralResultAPI> deleteClass({int id}) async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/classes/$id';

      var resp = await client.delete(url, headers: {
        'Authorization': token,
      });
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var result = GeneralResultAPI.fromJson(data);
      return result;
    } on SocketException {
      return GeneralResultAPI(statusCode: StatusCodeConst.noInternet, message: 'No Internet');
    } on TimeoutException {
      return GeneralResultAPI(statusCode: StatusCodeConst.timeout, message: 'Timeout');
    } catch (e) {
      return GeneralResultAPI(statusCode: StatusCodeConst.general, message: 'failed');
    }
  }

  static Future<GeneralResultAPI> kickMemberClass({int classId, int userId}) async {
    var dataMap = <String, dynamic>{'class_id': classId, 'user_id': userId};
    try {
      var client = Client();
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);
      var url = '$baseUrl/$version/member/';

      final resp = await client.send(Request('DELETE', Uri.parse(url))
        ..headers['Authorization'] = token
        ..headers['content-type'] = 'application/json'
        ..body = json.encode(dataMap));

      final data = await resp.stream.bytesToString();
      print(data);
      client.close();
      var result = GeneralResultAPI(statusCode: StatusCodeConst.success, message: data);
      return result;
    } on SocketException {
      return GeneralResultAPI(statusCode: StatusCodeConst.noInternet, message: 'No Internet');
    } on TimeoutException {
      return GeneralResultAPI(statusCode: StatusCodeConst.timeout, message: 'Timeout');
    } catch (e) {
      return GeneralResultAPI(statusCode: StatusCodeConst.general, message: 'failed');
    }
  }
}
