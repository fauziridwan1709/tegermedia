part of '_class.dart';

class TeacherServices {
  static Future<ModelListTeachers> getListTeachers({int classId}) async {
    var url = '$baseUrl/$version/classes/$classId/teachers';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      var result = ModelListTeachers.fromJson(data);
      return result;
    } catch (e) {
      return ModelListTeachers(data: []);
    }
  }
}
