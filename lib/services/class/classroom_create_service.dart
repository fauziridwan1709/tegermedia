part of '_class.dart';

class ClassroomCreateServices {
  static Future<ClassroomCreateResultModel> createClassFromClassroom(
      GoogleClassroomClass reqBody) async {
    var url = '$baseUrl/$version/classes/gcr';
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(AUTH_KEY);
    // var bearerToken = 'Bearer $token';
    print(reqBody.toJson());
    try {
      var resp = await client.post(url,
          body: json.encode(reqBody.toJson()),
          headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
          });
      // print(resp.body);
      return ClassroomCreateResultModel.fromJson(
          json.decode(resp.body) as Map<String, dynamic>);
    } catch (e) {
      print(e);
    }
  }
}

class ClassroomCreateResultModel {
  int statusCode;
  String message;
  ResultId resultId;

  ClassroomCreateResultModel({this.statusCode, this.message, this.resultId});

  ClassroomCreateResultModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    resultId = json['data'] != null ? ResultId.fromJson(json['data']) : null;
  }
}

// class ResultId {
//   int id;
//
//   ResultId({this.id});
//
//   ResultId.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//   }
// }
