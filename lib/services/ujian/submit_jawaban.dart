part of '_ujian.dart';

class SubmitJawaban {
  static Future<AnswerSheet> answerSheet({
    int quizId,
    int userId,
  }) async {
    var url = '$baseUrl/$version/quiz/$quizId/review-jawaban?user_id=$userId';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');
      var resp = await client.get(url, headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      print('---result---');
      return AnswerSheet.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return AnswerSheet(data: [], statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print('No connection');
      return AnswerSheet(data: [], statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return AnswerSheet(data: [], statusCode: StatusCodeConst.general, message: e.message);
    }
  }
}
