import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/client.dart';

part 'gdrive_services.dart';
