part of '_document.dart';

class GoogleDrive {
  static Future<DriveModel> createDriveFolder(
      {Map reqBody, Map reqBody2, String id, BuildContext context}) async {
    var url = '$baseGoogleApiUrl/drive/v3/files';
    var _googleSignIn = SiswamediaGoogleSignIn.getInstance();
    await _googleSignIn.requestScopes(driveScopes);

    await _googleSignIn.signIn();

    try {
      var headers = await _googleSignIn.currentUser.authHeaders;
      print(headers);
      var reqMap = {'Content-Type': 'application/json'};
      headers.addAll(reqMap);
      var resp = await client.post(url, body: json.encode(reqBody), headers: headers).timeout(
          Duration(seconds: 30),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp.body);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      var drive = DriveModel.fromJson(resp.statusCode, data);
      print(drive.id);
      var url2 = '$baseGoogleApiUrl/drive/v3/files/${drive.id}/permissions';
      var resp2 = await client.post(url2, body: json.encode(reqBody2), headers: headers).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp2.body);
      return drive;
    } on TimeoutException catch (e) {
      print(e);
      return DriveModel(statusCode: StatusCodeConst.timeout, id: null);
    } on SocketException catch (e) {
      print(e);
      return DriveModel(statusCode: StatusCodeConst.noInternet, id: null);
    } catch (e) {
      print(e);
      return DriveModel(id: null);
    }
  }

  static Future<DriveModel> addPermissions(
      {Map reqBody, String parent, BuildContext context}) async {
    var _googleSignIn = SiswamediaGoogleSignIn.getInstance();
    await _googleSignIn.requestScopes(driveScopes);
    //ignore: unawaited_futures
    showDialog<void>(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
                width: S.w * .9,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/kelas/dokumen.png', height: S.w / 3, width: S.w / 3),
                    Text('Sedang Memberi Akses...',
                        textAlign: TextAlign.center, style: semiBlack.copyWith(fontSize: S.w / 26)),
                    SizedBox(height: S.w * .05),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: S.w * .15),
                        child: SpinKitThreeBounce(
                          color: Colors.black54,
                          size: 45,
                        )),
                  ],
                ))));
    await _googleSignIn.signInSilently();

    try {
      var headers = await _googleSignIn.currentUser.authHeaders;
      var reqMap = {'Content-Type': 'application/json'};
      headers.addAll(reqMap);
      var url = '$baseGoogleApiUrl/drive/v3/files/$parent/permissions';
      var resp = await client.post(url, body: json.encode(reqBody), headers: headers).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp.body);
      final data = json.decode(resp.body) as Map<String, dynamic>;
      return DriveModel.fromJson(resp.statusCode, data);
    } on TimeoutException catch (e) {
      Navigator.pop(context);
      print(e);
      return DriveModel(statusCode: StatusCodeConst.timeout, id: null);
    } on SocketException catch (e) {
      Navigator.pop(context);
      print(e);
      return DriveModel(statusCode: StatusCodeConst.noInternet, id: null);
    } catch (e) {
      Navigator.pop(context);
      print(e);
      return DriveModel(id: null);
    }
  }

  static Future<AllSubFile> getAllFile({String parent}) async {
    print(parent);
    var url =
        '$baseGoogleApiUrl/drive/v3/files?q=%27$parent%27%20in%20parents&fields=files%2FwebViewLink%2C%20files%2Fname%2C%20files%2Fid%2C%20files%2FmimeType&key=AIzaSyAqI_Q-SFUJNnOZZPIIU1_jdWHaNTIZ_2A';

    var resp = await client.get(url, headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
    }).timeout(GLOBAL_TIMEOUT, onTimeout: () => throw TimeoutException('Timeout, try Again'));
    print(resp.body);
    final data = json.decode(resp.body) as Map<String, dynamic>;
    var result = AllSubFile.fromJson(resp.statusCode, data);
    return result;
  }

  static Future<void> deleteFile({String id}) async {
    var url = '$baseGoogleApiUrl/drive/v3/files/$id';

    var signIn = await SiswamediaGoogleSignIn.getInstance().signIn();
    var headers = await signIn.authHeaders;
    headers.addAll({HttpHeaders.contentTypeHeader: 'application/json'});
    var resp = await client
        .delete(url, headers: headers)
        .timeout(GLOBAL_TIMEOUT, onTimeout: () => throw TimeoutException('Timeout, try Again'));
    print(resp.statusCode);
    print(resp.body);
    // return result;
  }

  static Future<GeneralResultApi> uploadFile(
      {String contentType,
      String contentLength,
      String parent,
      String name,
      Uint8List coba,
      BuildContext context}) async {
    var url = '$baseGoogleApiUrl/upload/drive/v3/files?uploadType=media';
    var _googleSignIn = SiswamediaGoogleSignIn.getInstance();
    await _googleSignIn.requestScopes(driveScopes);
    //ignore: unawaited_futures
    showDialog<void>(
        context: context,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
                width: S.w * .9,
                padding: EdgeInsets.symmetric(horizontal: S.w * .05, vertical: S.w * .05),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/kelas/dokumen.png', height: S.w / 3, width: S.w / 3),
                    Text('Sedang Mengupload File...',
                        textAlign: TextAlign.center, style: semiBlack.copyWith(fontSize: S.w / 26)),
                    SizedBox(height: S.w * .05),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: S.w * .15),
                        child: SpinKitSquareCircle(
                          color: Colors.black54,
                          size: 30,
                        )),
                  ],
                ))));
    await _googleSignIn.signInSilently();

    try {
      var headers = await _googleSignIn.currentUser.authHeaders;
      // var reqMap = {
      //   'Content-Type': contentType,
      //   'Content-Length': contentLength
      // };

      var headers2 = await _googleSignIn.currentUser.authHeaders;
      var reqMap2 = {'Content-Type': 'application/json'};
      headers2.addAll(reqMap2);
      print(headers);
      var resp = await client
          .post(url,
              body: coba,
              // body: json.encode({
              //   'parents': ['$parent'],
              //   'originalFilename': 'nama',
              //   'contentHints': {'indexableText': '$coba'},
              // }),
              headers: headers)
          .timeout(Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp.body);
      final jsonData = json.decode(resp.body) as Map<String, dynamic>;
      var data = DriveModel.fromJson(resp.statusCode, jsonData);
      var url2 = '$baseGoogleApiUrl/drive/v3/files/${data.id}?addParents=$parent';
      print(parent);
      print('init');
      print(data.id);
      var url3 = '$baseUrl/drive/v3/files/${data.id}/permissions';
      var resp3 = await client
          .post(url3, body: json.encode({'role': 'reader', 'type': 'anyone'}), headers: headers2)
          .timeout(Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print('-patchUrl--');
      Logger().d(url2);
      var resp2 = await client
          .patch(url2, body: json.encode({'name': name}), headers: headers2)
          .timeout(Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp2.body);
      return GeneralResultApi(statusCode: StatusCodeConst.success, message: 'Sukses');
    } on TimeoutException catch (e) {
      Navigator.pop(context);
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      Navigator.pop(context);
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      Navigator.pop(context);
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.general, message: e.message);
    }
  }

  static Future<void> getFileById({int id}) async {
    var url = '$baseGoogleApiUrl/drive/v3/files/$id';
    var prefs = await SharedPreferences.getInstance();
    try {
      var token = prefs.getString('auth_siswamedia_api');
      var resp = await client.get(url, headers: {'Authorization': token}).timeout(
          Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print(resp.body);
    } on TimeoutException catch (e) {
      print(e);
    } on SocketException catch (e) {
      print(e);
    } catch (e) {
      print(e);
    }
  }
}
