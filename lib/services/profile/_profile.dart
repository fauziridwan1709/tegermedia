import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/services/image/cache_image_service.dart';
import 'package:tegarmedia/services/image/image_cropper_service.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/states/global_state.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'profile_services.dart';
