part of '_profile.dart';

class ProfileServices {
  static final _SUCCESS_UPLOAD = 'Berhasil mengganti photo profile';
  static final _FAIL_UPLOAD = 'Gagal mengupload photo profile baru, coba lagi';

  static Future<void> bindGMail(BuildContext context) async {
    var signInMethod = SiswamediaGoogleSignIn.getInstance();
    await signInMethod.signIn();
    var auth = await signInMethod.currentUser.authentication;
    var body = <String, dynamic>{
      'register_type': 'gmail',
      'email': signInMethod.currentUser.email,
      'password': auth.accessToken
    };
    var url = '$baseUrlGoogle/v1/api/auth/bind';
    var resp = await postIt(url, body);
    var data = json.decode(resp.body) as Map<String, dynamic>;
    resp.statusCode.translate(ifSuccess: () {
      CustomFlushBar.successFlushBar('Berhasil', context);
    }, ifElse: () {
      CustomFlushBar.errorFlushBar(data['message'], context);
    });
  }

  static Future<BindAccount> getAllBindAccount() async {
    var url = '$baseUrlGoogle/v1/api/userCredential/select';
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    return BindAccount.fromJson(json.decode(resp.body) as Map<String, dynamic>);
  }

  static Future<UserProfile2> getProfile() async {
    var url = '$baseUrlGoogle/v1/api/user/token';
    // throw Exception();
    var resp = await getIt(url, headers: await Pref.getIdentityHeader());
    Utils.log(url, info: 'url');
    Utils.log(resp.statusCode, info: 'status code');
    Utils.logWithDotes(resp.body, info: 'data get profile');
    if (!resp.statusCode.isSuccessOrCreated) {
      throw Exception();
    }
    return UserProfile2.fromJson(json.decode(resp.body) as Map<String, dynamic>);
  }

  static Future<Map<int, String>> updateProfile(Map<String, dynamic> model) async {
    try {
      var url = '$baseUrlGoogle/v1/api/user/token';
      var resp = await putIt(url, model, isIdentity: true);
      var message = json.decode(resp.body) as Map<String, dynamic>;
      Utils.log(resp.statusCode, info: 'status Code');
      List<dynamic> va = message['messages'];
      if (!resp.statusCode.isSuccessOrCreated) {
        throw SiswamediaException(va.join(','));
      }
      return <int, String>{resp.statusCode: message['messages']};
    } catch (e) {
      print(e);
      if (e is SiswamediaException) {
        return <int, String>{303: e.message};
      }
      return <int, String>{303: 'Terjadi kesalahan'};
    }
  }

  static Future<void> uploadPhotoProfile(BuildContext context, String gender) async {
    var url = '$baseUrlGoogle/v1/api/file';
    SiswamediaTheme.loading(context);
    try {
      var pickedImage = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: 10);
      var imageFile = await ImageCropperService.crop(pickedImage);
      if (imageFile != null) {
        var request = MultipartRequest('POST', Uri.parse(url))
          ..files.add(await MultipartFile.fromPath('file', imageFile.path))
          ..fields.addAll({'type': 'user_profile'})
          ..headers.addAll(await Pref.getIdentityHeader());
        print('--- total length ---');
        print(File(imageFile.path).lengthSync());
        var profile = GlobalState.profile().state.profile;
        var response = request.send();
        response.asStream().listen((value) async {
          var resp = await value.stream.bytesToString();
          var data = ResultUploadImage.fromJson(json.decode(resp));
          Utils.log(resp, info: 'response body');
          if (value.statusCode.isSuccessOrCreated) {
            await updateProfile(<String, dynamic>{
              'profile_image_id': data.data.id,
              'gender': gender ?? profile.gender,
              'name': profile.name,
              'phone': profile.phone,
              'addres': profile.address,
            }).then((result) {
              result.keys.first.translate(ifSuccess: () async {
                int userId = profile.id;
                await CacheImageServices.cachePhotoProfile(imageFile, userId);
                pop(context);
                CustomFlushBar.successFlushBar(_SUCCESS_UPLOAD, context);
                await GlobalState.profile().setState((s) => s.retrieveData(), onData: (_, data) {
                  print('ganti image');
                });
              }, ifElse: () {
                //todo fail
                pop(context);
                CustomFlushBar.errorFlushBar(result.values.first, context);
              });
            });
          } else {
            pop(context);
            CustomFlushBar.errorFlushBar(_FAIL_UPLOAD, context);
          }
        });
      } else {
        pop(context);
      }
    } catch (e) {
      pop(context);
      Utils.log(e, info: 'error');
      CustomFlushBar.errorFlushBar(e.toString(), context);
    }
  }
}

class UserProfile2 {
  DataUserProfile2 data;
  Null meta;

  UserProfile2({this.data, this.meta});

  UserProfile2.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? DataUserProfile2.fromJson(json['data']) : null;
    meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['meta'] = meta;
    return data;
  }
}

class DataUserProfile2 {
  int id;
  String code;
  String email;
  String name;
  int profileImageId;
  String profileImageUrl;
  String profileImagePresignedUrl;
  String gender;
  String phone;
  int cityId;
  String cityCode;
  String cityName;
  String provinceCode;
  String provinceName;
  String address;
  String status;
  String createdAt;
  String updatedAt;
  String deletedAt;

  DataUserProfile2(
      {this.id,
      this.code,
      this.email,
      this.name,
      this.profileImageId,
      this.profileImageUrl,
      this.profileImagePresignedUrl,
      this.gender,
      this.phone,
      this.cityId,
      this.cityCode,
      this.cityName,
      this.provinceCode,
      this.provinceName,
      this.address,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  DataUserProfile2.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    email = json['email'];
    name = json['name'];
    profileImageId = json['profile_image_id'];
    profileImageUrl = json['profile_image_url'];
    profileImagePresignedUrl = json['profile_image_presigned_url'];
    gender = json['gender'];
    phone = json['phone'];
    cityId = json['city_id'];
    cityCode = json['city_code'];
    cityName = json['city_name'];
    provinceCode = json['province_code'];
    provinceName = json['province_name'];
    address = json['address'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['email'] = email;
    data['name'] = name;
    data['profile_image_id'] = profileImageId;
    data['profile_image_url'] = profileImageUrl;
    data['profile_image_presigned_url'] = profileImagePresignedUrl;
    data['gender'] = gender;
    data['phone'] = phone;
    data['city_id'] = cityId;
    data['city_code'] = cityCode;
    data['city_name'] = cityName;
    data['province_code'] = provinceCode;
    data['province_name'] = provinceName;
    data['address'] = address;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}

class BindAccount {
  List<DataBindAccount> data;
  Null meta;

  BindAccount({this.data, this.meta});

  BindAccount.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <DataBindAccount>[];
      json['data'].forEach((dynamic v) {
        data.add(DataBindAccount.fromJson(v as Map<String, dynamic>));
      });
    }
    meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['meta'] = meta;
    return data;
  }
}

class DataBindAccount {
  int id;
  int userId;
  String type;
  String email;
  String emailValidAt;
  String password;
  RegistrationDetails registrationDetails;
  String registrationDetailsEncrypted;
  String status;
  String createdAt;
  String updatedAt;
  String deletedAt;

  DataBindAccount(
      {this.id,
      this.userId,
      this.type,
      this.email,
      this.emailValidAt,
      this.password,
      this.registrationDetails,
      this.registrationDetailsEncrypted,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  DataBindAccount.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    type = json['type'];
    email = json['email'];
    emailValidAt = json['email_valid_at'];
    password = json['password'];
    registrationDetails = json['registration_details'] != null
        ? RegistrationDetails.fromJson(json['registration_details'])
        : null;
    registrationDetailsEncrypted = json['registration_details_encrypted'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['type'] = type;
    data['email'] = email;
    data['email_valid_at'] = emailValidAt;
    data['password'] = password;
    if (registrationDetails != null) {
      data['registration_details'] = registrationDetails.toJson();
    }
    data['registration_details_encrypted'] = registrationDetailsEncrypted;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}

class RegistrationDetails {
  String email;
  String password;
  String registerType;
  String accessToken;
  String expiry;
  String refreshToken;
  String tokenType;

  RegistrationDetails(
      {this.email,
      this.password,
      this.registerType,
      this.accessToken,
      this.expiry,
      this.refreshToken,
      this.tokenType});

  RegistrationDetails.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    registerType = json['register_type'];
    accessToken = json['access_token'];
    expiry = json['expiry'];
    refreshToken = json['refresh_token'];
    tokenType = json['token_type'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['password'] = password;
    data['register_type'] = registerType;
    data['access_token'] = accessToken;
    data['expiry'] = expiry;
    data['refresh_token'] = refreshToken;
    data['token_type'] = tokenType;
    return data;
  }
}

class ResultUploadImage {
  DataResultImage data;
  Null meta;

  ResultUploadImage({this.data, this.meta});

  ResultUploadImage.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? DataResultImage.fromJson(json['data']) : null;
    meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['meta'] = meta;
    return data;
  }
}

class DataResultImage {
  int id;
  String type;
  String url;
  String tempUrl;
  int userUpload;
  String createdAt;
  String updatedAt;
  String deletedAt;

  DataResultImage(
      {this.id,
      this.type,
      this.url,
      this.tempUrl,
      this.userUpload,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  DataResultImage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    url = json['url'];
    tempUrl = json['temp_url'];
    userUpload = json['user_upload'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['type'] = type;
    data['url'] = url;
    data['temp_url'] = tempUrl;
    data['user_upload'] = userUpload;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}

Future<void> snippetUploadPhotoProfile(File file, String token) async {
  var uri = Uri.parse('url nya');
  try {
    var request = MultipartRequest('POST', uri)
      ..files.add(await MultipartFile.fromPath('file', file.path));
    request.headers['Authorization'] = 'Bearer $token';
    request.headers['Content-Type'] = 'type nya apa';
    var response = request.send();
    response.asStream().listen((value) async {
      ///read response
      //todo var resp = await value.stream.bytesToString();
      ///parse response
      //todo var data = ResultUploadImage.fromJson(json.decode(resp));
    });
  } catch (e) {
    Utils.log(e, info: 'error');
  }
// var resp = await postIt(url, body);
// return resp;
}
