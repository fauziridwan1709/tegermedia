import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/models/interaction/chat.dart';
import 'package:tegarmedia/models/interaction/participants.dart';
import 'package:tegarmedia/models/interaction/studentInteraksi.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/states/global_state.dart';

part "interaction_service.dart";
