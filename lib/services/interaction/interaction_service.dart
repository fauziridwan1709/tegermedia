part of '_interaction.dart';

class InteractionServices {
  static Future<InteractionData> getStudyRoomTeacher({String token, String id}) async {
    String url;
    url = '$baseUrl/$version/study-room/teacher/$id';
    print(url.toString());
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.get(url, headers: {'authorization': token});

    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data.toString());
    print('token: ' + token);
    var result = InteractionData.fromJson(data);

    if (result.statusCode == 200) {
      return result;
    } else {
      return InteractionData(statusCode: 500, data: []);
    }
  }

  static Future<DetailChats> sendMessage(String message, String room_id) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    var url = '$urlChat/v1/api/chat';
    var body = <String, dynamic>{};
    body['room_id'] = room_id;
    body['message'] = message;

    print(url.toString());

    var resp = await client.post(url, headers: {'Authorization': 'Bearer $token'}, body: body);

    DetailChats chat;
    var data = json.decode(resp.body) as Map<String, dynamic>;
    Utils.log(url, info: 'url');
    Utils.log(body, info: 'model');
    Utils.log(resp.statusCode, info: 'status code');
    Utils.logWithDotes(resp.body, info: 'data chat');
    chat = DetailChats.fromJson(data);
    return chat;
  }

  static Future<List<DetailChats>> getChats(String id, {String room_id}) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    var queryParameters = {
      'room_id': room_id,
      'limit': '50',
      'message': '',
      'last_id': id,
    };
    var uri = Uri.https('dev-chat.siswamedia.com', '/v1/api/chat', queryParameters);
    print('url : ' + uri.toString());
    var resp = await client.get(uri, headers: {'Authorization': 'Bearer $token'});
    Chat chat;
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data.toString());
    chat = Chat.fromJson(data);
    return chat.data;
  }

  static Future<DetailParticipants> getParticipants(String id, {String room_id}) async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    var queryParameters = {
      'status': 'active',
    };
    var uri = Uri.https('dev-chat.siswamedia.com', '/v1/api/room/id/$room_id', queryParameters);
    print('url : ' + uri.toString());
    var resp = await client.get(uri, headers: {'Authorization': 'Bearer $token'});
    DetailParticipants chat;
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print(data.toString());
    chat = DetailParticipants.fromJson(data);
    return chat;
  }

  static Future<EndClassResult> endClass(String id) async {
    String url;
    url = '$baseUrl/$version/study-room/open/$id';
    print(url.toString());
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    EndClassResult endClassResult;
    var resp = await client.get(url, headers: {'authorization': token});
    print('error');
    var data = json.decode(resp.body) as Map<String, dynamic>;
    print('response : ' + data.toString());
    endClassResult = EndClassResult.fromJson(data);
    if (endClassResult.statusCode == 200) {
      return endClassResult;
    } else {
      return EndClassResult(statusCode: 500, message: '');
    }
  }

  static Future joinParticipants(String id) async {
    String url;
    url = '$urlChat/v1/api/room/participant/id/$id';
    print(url.toString());
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.put(url, headers: {'authorization': 'Bearer ' + token});

    var data = json.decode(resp.body) as Map<String, dynamic>;
    print('response nyoba : ' + data.toString());
  }

  static Future<StudentData> getStudent({String token, String classId = ''}) async {
    var authState = GlobalState.auth().state;
    String url;
    url =
        '$baseUrl/$version/study-room/student/$classId${authState.currentState.classRole.isOrtu ? '?type=ORTU' : ''}';

    print(url.toString());
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.get(url, headers: {'authorization': token});

    var data = json.decode(resp.body) as Map<String, dynamic>;
    print('response : ' + data.toString());
    print('token : ' + token);
    var result = StudentData.fromJson(data);

    if (result.statusCode == 200) {
      return result;
    } else {
      return StudentData(statusCode: 500, data: []);
    }
  }

  static Future<ResultSchoolModel> deleteRoom({
    int idInteraction,
  }) async {
    var url = '$baseUrl/$version/study-room/$idInteraction';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.delete(url, headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = ResultSchoolModel.fromJson(json.decode(resp.body));
      print('aish' + data.statusCode.toString());
      return data;
    } on TimeoutException catch (e) {
      print(e);
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print('No connection');
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.noInternet);
    } catch (err) {
      return ResultSchoolModel(message: 'Terjadi Kesalahan', statusCode: 400);
    }
  }

  static Future<ResultSchoolModel> updateRoom({
    int idInteraction,
    Map<String, dynamic> body,
  }) async {
    var url = '$baseUrl/$version/study-room/$idInteraction';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.put(url, body: json.encode(body), headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = ResultSchoolModel.fromJson(json.decode(resp.body));
      print('aish' + data.statusCode.toString());
      return data;
    } on TimeoutException catch (e) {
      print(e);
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print('No connection');
      return ResultSchoolModel(message: e.message, statusCode: StatusCodeConst.noInternet);
    } catch (err) {
      return ResultSchoolModel(message: 'Terjadi Kesalahan', statusCode: 400);
    }
  }

  static Future<CreateRoomData> createRoom({
    Map<String, dynamic> data,
  }) async {
    var url = '$baseUrl/$version/study-room';
    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');
      print(data);
      print('log.e' + url);
      print('log.e' + token);
      var resp = await client
          .post(url,
              headers: {'Content-Type': 'application/json', 'Authorization': token},
              body: json.encode(data))
          .timeout(Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('Timeout, try Again'));
      print('log.d' + url);
      print('log.d' + resp.headers.toString());
      var dataModel = CreateRoomData.fromJson(json.decode(resp.body));
      print('aish' + dataModel.statusCode.toString());
      return dataModel;
    } on TimeoutException catch (e) {
      print(e);
      return CreateRoomData(statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print('No connection');
      return CreateRoomData(statusCode: StatusCodeConst.noInternet);
    } catch (err) {
      return CreateRoomData(statusCode: 400);
    }
  }
}
