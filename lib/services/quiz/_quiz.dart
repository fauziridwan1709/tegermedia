import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/features/studyJournal/presentation/states/_states.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/ui/pages/Conference/_conference.dart';

import '../../states/global_state.dart';

part 'penilaian_services.dart';
part 'quiz_services.dart';
part 'quiz_setting.dart';
