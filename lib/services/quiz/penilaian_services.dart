part of '_quiz.dart';

class PenilaianServices {
  static Future<GeneralResultApi> submitJawaban(
      {int quizId, List<Map<String, dynamic>> jawaban}) async {
    var url = '$baseUrl/$version/bank-soal/myanswer';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      Map<String, dynamic> value;

      value = <String, dynamic>{'quiz_id': quizId, 'kumpulan_jawaban': jawaban};
      print(value);
      var resp = await client.post(url, body: json.encode(value), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      print('---result---');
      return GeneralResultApi(statusCode: StatusCodeConst.success, message: 'Sukses');
    } on TimeoutException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.general, message: e.message);
    }
  }

  static Future<GeneralResultApi> nilaiLembarJawab(
      {int quizId, List<Map<String, dynamic>> nilai}) async {
    var url = '$baseUrl/$version/bank-soal/answer/score';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      Map<String, dynamic> value;

      value = <String, dynamic>{'quiz_id': quizId, 'kumpulan_nilai': nilai};
      print(value);
      var resp = await client.post(url, body: json.encode(value), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = json.decode(resp.body) as Map<String, dynamic>;
      if (resp.statusCode == 409) {
        throw ConflictException(cause: 'Terjadi Konflik');
      }
      print(data);
      if (data['message'] == 'quiz_answer_ref_id is required') {
        throw GeneralException(cause: 'user ini belum mengerjakan quiz nya');
      }
      print('---result---');
      return GeneralResultApi(statusCode: StatusCodeConst.success, message: 'Sukses');
    } on TimeoutException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.noInternet, message: e.message);
    } on ConflictException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.conflict, message: e.cause);
    } on GeneralException catch (e) {
      print(e);
      return GeneralResultApi(statusCode: StatusCodeConst.general, message: e.cause);
    }
  }
}
