part of '_quiz.dart';

class MockHttpClient extends Mock implements Client {}

class QuizServices {
  static Future<AnswerSheet> answerSheet({
    int quizId,
    int userId,
  }) async {
    var url = '$baseUrl/$version/quiz/$quizId/review-jawaban?user_id=$userId';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');
      print(quizId);
      print(userId);

      var resp = await client.get(url, headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));

      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      print('---result---');
      return AnswerSheet.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return AnswerSheet(data: [], statusCode: StatusCodeConst.timeout, message: e.message);
    } on SocketException catch (e) {
      print(e);
      return AnswerSheet(data: [], statusCode: StatusCodeConst.noInternet, message: e.message);
    } catch (e) {
      print(e);
      return AnswerSheet(data: [], statusCode: 400);
    }
  }

  static Future<ResultCreateUjian> createQuiz({BankModel value}) async {
    var url = '$baseUrl/$version/quiz';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(value.toJson()), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      }).timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Timeout, try Again'));
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('---result---');
      print(resp.body);
      return ResultCreateUjian.fromJson(data);
    } on TimeoutException catch (e) {
      print(e);
      return ResultCreateUjian(data: Result(id: 0), statusCode: StatusCodeConst.timeout);
    } on SocketException catch (e) {
      print(e);
      return ResultCreateUjian(data: Result(id: 0), statusCode: StatusCodeConst.noInternet);
    } catch (e) {
      print(e);
      return ResultCreateUjian(data: Result(id: 0), statusCode: StatusCodeConst.general);
    }
  }

  static Future<void> createBankSoal(int id, {QuizItem item}) async {
    print('data');
    var url = '$baseUrl/$version/quiz/$id/bank-soal';
    Map<String, dynamic> valuee;
    if (item.pilihand == '-') {
      valuee = <String, dynamic>{
        'score': 1,
        'correct_answer': item.jawaban,
        'question_type': 'PG',
        'question_description': item.pertanyaan,
        'image': item.attpertanyaan,
        'quiz_pg_references': [
          {'order_of_question': 'A', 'pg_description': item.pilihana},
          {'order_of_question': 'B', 'pg_description': item.pilihanb},
          {'order_of_question': 'C', 'pg_description': item.pilihanc}
        ]
      };
    } else if (item.pilihand != '-') {
      valuee = <String, dynamic>{
        'score': 1,
        'correct_answer': item.jawaban,
        'question_type': 'PG',
        'question_description': item.pertanyaan,
        'image': item.attpertanyaan,
        'quiz_pg_references': [
          {'order_of_question': 'A', 'pg_description': item.pilihana},
          {'order_of_question': 'B', 'pg_description': item.pilihanb},
          {'order_of_question': 'C', 'pg_description': item.pilihanc},
          {'order_of_question': 'D', 'pg_description': item.pilihand}
        ]
      };
      if (item.pilihane != '-') {
        valuee = <String, dynamic>{
          'score': 1,
          'correct_answer': item.jawaban,
          'question_type': 'PG',
          'question_description': item.pertanyaan,
          'image': item.attpertanyaan,
          'quiz_pg_references': [
            {'order_of_question': 'A', 'pg_description': item.pilihana},
            {'order_of_question': 'B', 'pg_description': item.pilihanb},
            {'order_of_question': 'C', 'pg_description': item.pilihanc},
            {'order_of_question': 'D', 'pg_description': item.pilihand},
            {'order_of_question': 'E', 'pg_description': item.pilihane}
          ]
        };
      }
    }

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(valuee), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('create bank soal id');
      print(data);
      print('---result---');
    } catch (e) {
      print(e);
    }
  }

  static Future<void> createBankSoalBatch(int id, {List<Map<String, dynamic>> item}) async {
    print('data');
    var url = '$baseUrl/$version/quiz/$id/bank-soal';
    Map<String, dynamic> valuee;
    valuee = <String, dynamic>{'kumpulan_soal': item};

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(valuee), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      });
      print(valuee);
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('create bank soal id');
      print(data);
      print('---result---');
    } catch (e) {
      print(e);
    }
  }

  static Future<void> createBankSoalEssay(int id, {QuizItem item}) async {
    print('data');
    var url = '$baseUrl/$version/quiz/$id/bank-soal';

    try {
      var valuee = {
        'score': 10,
        'correct_answer': 'null',
        'question_type': 'ESSAY',
        'question_description': item.pertanyaan,
        'image': item.attpertanyaan,
        'quiz_pg_references': <Map<String, dynamic>>[]
      };
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.post(url, body: json.encode(valuee), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('create bank soal id');
      print(data);
      print('---result---');
    } catch (e) {
      print(e);
    }
  }

  static Future<QuizModelKelas> getBankSoalByQuiz(
    int id,
  ) async {
    var url = '$baseUrl/$version/quiz/$id/bank-soal';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('---data bank soal by quiz---');
      print(resp.body);
      print(data);
      print('---result---');
      return QuizModelKelas.fromJson(data);
    } catch (e) {
      print(e);
      print('---error---');
      return QuizModelKelas(data: []);
    }
  }

  static Future<void> getBankSoalById(
    int id,
  ) async {
    var url = '$baseUrl/$version/quiz/$id/bank-soal?question_type=PG';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('---data bank soal by quiz---');
      print(resp.body);
      print(data);
      print('---result---');
    } catch (e) {
      print(e);
      print('---error---');
    }
  }

  static Future<QuizListModel> getQuiz(
      {Client mockClient, int classId, bool isOrtu = false, bool isJournal = false}) async {
    var _client = mockClient ?? client;
    var isWaliKelas = GlobalState.auth().state.currentState.classRole.isWaliKelas;
    var createJournal = Injector.getAsReactive<CreateStudyJournalState>();
    var url =
        '$baseUrl/$version/me/quiz?quiz_type=SEKOLAH&class_id=$classId${isOrtu ? '&type=ORTU' : ''}${isWaliKelas ? '&access=WALIKELAS' : ''}${isJournal ? '&course_id=${createJournal.state.createModel.courseId ?? ''}' : ''}';
    try {
      var resp = await _client.get(url, headers: await Pref.getSiswamediaHeader());
      print(resp.body);
      if (resp.statusCode != 200) {
        throw BadRequestException(statusCode: resp.statusCode);
      }
      Utils.logWithDotes(resp.body, info: 'data quiz');
      var data = json.decode(resp.body) as Map<String, dynamic>;
      var result = QuizListModel.fromJson(data);
      return result;
    } on SocketException {
      return QuizListModel(statusCode: 503);
    } on BadRequestException catch (e) {
      return QuizListModel(statusCode: e.statusCode);
    } catch (e) {
      print(e);
      return QuizListModel(statusCode: 400);
    }
  }

  static Future<List<QuizListDetail>> getQuizMediaSekolah(
      {Client mockClient, int classId, bool isOrtu = false, bool isJournal = false}) async {
    var _client = mockClient ?? client;
    var isWaliKelas = GlobalState.auth().state.currentState.classRole.isWaliKelas;
    var createJournal = Injector.getAsReactive<CreateStudyJournalState>();
    var url = 'https://sekolah.nusatek.dev/api/quiz/mobile_login';
    var headers = {
      'Accept': 'application/json',
      'Authorization': 'Basic amtnamt4Z1VJdWdrYSVeMzY6amtnamt4Z1VJdWdrYSVeMzY=',
      'Content-Type': 'application/json'
    };
    try {
      var profile = GlobalState.profile();
      var dataMap = <String, dynamic>{
        'email': profile.state.profile.email,
        // 'email': 'testmuridsiswamedia@gmail.com',
      };
      var resp = await _client.post(url, body: jsonEncode(dataMap), headers: headers);
      Map<String, dynamic> token = jsonDecode(resp.body);
      var headersQuiz = {'Authorization': 'Bearer ${token['data']}'};
      var urlQuiz = 'https://sekolah.nusatek.dev/api/quiz/mobile/list_quiz?offset=0&limit=10';
      var respQuiz = await _client.get(urlQuiz, headers: headersQuiz);

      print(respQuiz.body);

      if (resp.statusCode != 200) {
        throw BadRequestException(statusCode: resp.statusCode);
      }
      List<QuizListDetail> listData = <QuizListDetail>[];
      Map<String, dynamic> json = jsonDecode(respQuiz.body);

      if (json['data'] != null) {
        listData = <QuizListDetail>[];
        json['data'].forEach((dynamic v) {
          listData.add(QuizListDetail(
            name: v['title'] as String,
            startDate: v['date_start'] as String,
            endDate: v['date_end'] as String,
            description: v['link'] as String,
            courseName: v['subject_name'] as String,
            totalTime: v['time_limit'] as int,
            hasBeenDone: v['is_taked'] as bool,
            totalPg: v['total'] as int,
            status: v['status'] as int,
            totalNilai: getNilai(v),
            // totalPg: json['status_text'],
          ));
        });
      }
      Utils.logWithDotes(respQuiz.body, info: 'data quiz');
      return listData;
    } on SocketException {
      return null;
    } on BadRequestException catch (e) {
      return null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  static double getNilai(Map<String, dynamic> v) {
    try {
      return (v['student_take']['own_mark'] / v['student_take']['total_mark']) * 100;
    } catch (e) {
      return null;
    }
  }

  static Future<QuizByIdModel> getQuizById(
    int id,
  ) async {
    var url = '$baseUrl/$version/quiz/$id';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('data3');
      print(resp.body);
      var result = QuizByIdModel.fromJson(data);
      print('---result---');
      return result;
    } catch (e) {
      print(e);
      print('---error---');
      return QuizByIdModel();
    }
  }

  static Future<QuizListModel> getQuizByMe() async {
    var url = '$baseUrl/$version/me/quiz';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print('data3');
      print(resp.body);
      print(data);
      var result = QuizListModel.fromJson(data);
      print('---result---');
      return result;
    } catch (e) {
      print(e);
      print('---error---');
      return QuizListModel();
    }
  }

  static Future<GeneralResultApi> updateQuiz(int id, Map<String, dynamic> reqMap) async {
    var url = '$baseUrl/$version/quiz/$id';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.put(url,
        body: json.encode(reqMap),
        headers: {'Authorization': token, 'Content-Type': 'application/json'});
    Utils.log(url, info: 'url');
    Utils.log(resp.statusCode, info: 'status code');
    Utils.logWithDotes(reqMap, info: 'req map');
    Utils.logWithDotes(resp.body, info: 'data update quiz');
    var data = json.decode(resp.body) as Map<String, dynamic>;
    return GeneralResultApi(
        statusCode: resp.statusCode, message: data['message'] ?? 'sorry message empty!');
  }

  static Future<GeneralResultApi> deleteQuiz(int id) async {
    var url = '$baseUrl/$version/quiz/$id';

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');

    var resp = await client.delete(url, headers: {
      'Authorization': token,
    });
    var data = json.decode(resp.body) as Map<String, dynamic>;
    return GeneralResultApi(
        statusCode: resp.statusCode, message: data['message'] ?? 'sorry message empty!');
  }
}

class BankModel {
  int id;
  int schoolId;
  String schoolName;
  int classId;
  String className;
  int courseId;
  String courseName;
  String quizType;
  String name;
  int totalTime;
  int totalPg;
  int totalEssay;
  String createdAt;
  String updatedAt;
  String startDate;
  String endDate;
  bool isHidden;
  bool isRandom;

  BankModel({
    this.id,
    this.schoolId,
    this.schoolName,
    this.classId,
    this.className,
    this.courseId,
    this.courseName,
    this.quizType,
    this.name,
    this.totalTime,
    this.totalPg,
    this.totalEssay,
    this.createdAt,
    this.updatedAt,
    this.startDate,
    this.endDate,
    this.isHidden,
    this.isRandom,
  });

  BankModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    courseName = json['course_name'];
    quizType = json['quiz_type'];
    name = json['name'];
    totalTime = json['total_time'];
    totalPg = json['total_pg'];
    totalEssay = json['total_essay'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    isHidden = json['is_hidden'];
    isRandom = json['is_random'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['class_id'] = classId;
    data['class_name'] = className;
    data['course_id'] = courseId;
    data['course_name'] = courseName;
    data['quiz_type'] = quizType;
    data['name'] = name;
    data['total_time'] = totalTime;
    data['total_pg'] = totalPg;
    data['total_essay'] = totalEssay;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['is_hidden'] = isHidden;
    data['is_random'] = isRandom;
    return data;
  }
}

class ResultCreateUjian {
  int statusCode;
  Result data;

  ResultCreateUjian({this.statusCode, this.data});

  ResultCreateUjian.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = json['data'] != null ? Result.fromJson(json['data']) : null;
  }
}

class Result {
  int id;
  Result({this.id});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }
}
