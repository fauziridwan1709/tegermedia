part of '_quiz.dart';

class QuizSettingServices {
  static Future<GeneralResultAPI> switchHide({QuizListDetail data}) async {
    var url = '$baseUrl/$version/quiz/${data.id}';
    var dataMap = <String, dynamic>{
      'is_hidden': !data.isHidden,
      'is_random': data.isRandom
    };

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.put(url, body: json.encode(dataMap), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      });
      print(resp.body);
      var data = json.decode(resp.body) as Map<String, dynamic>;
      return GeneralResultAPI.fromJson(data);
    } catch (e) {
      print(e);
      return GeneralResultAPI(statusCode: 400, message: 'Error');
    }
  }

  static Future<GeneralResultAPI> switchRandom({QuizListDetail data}) async {
    var url = '$baseUrl/$version/quiz/${data.id}';
    var dataMap = <String, dynamic>{
      'is_random': !data.isRandom,
      'is_hide': data.isHidden
    };

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString(AUTH_KEY);

      var resp = await client.put(url, body: json.encode(dataMap), headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      return GeneralResultAPI.fromJson(data);
    } catch (e) {
      print(e);
      return GeneralResultAPI(statusCode: 400, message: 'Error');
    }
  }
}
