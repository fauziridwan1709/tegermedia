part of '_score.dart';

class ScoreServices {
  static Future<ModeleSummaryScore> getSummaryScore(
      {Map<String, String> params}) async {
    var url = Uri.https(baseUri, '/$version/rekap-nilai', params);
    print(url);

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = ModeleSummaryScore.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      return ModeleSummaryScore(data: []);
    }
  }

  static Future<ModelDetailSummaryScore> getDetailSummaryScore(
      {int courseId, String params}) async {
    var url = '$baseUrl/$version/rekap-nilai/$courseId?$params';

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = ModelDetailSummaryScore.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      return ModelDetailSummaryScore(data: null);
    }
  }

  static Future<ModelReportCardGrades> getReportCardGrades(
      {Map<String, String> params}) async {
    var url = Uri.https(baseUri, '/$version/rekap-rapor', params);

    print(url);

    try {
      var prefs = await SharedPreferences.getInstance();
      var token = prefs.getString('auth_siswamedia_api');

      var resp = await client.get(url, headers: {
        'Authorization': token,
      });
      var data = json.decode(resp.body) as Map<String, dynamic>;
      print(data);
      var result = ModelReportCardGrades.fromJson(data);
      return result;
    } catch (e) {
      print(e);
      return ModelReportCardGrades(data: []);
    }
  }
}
