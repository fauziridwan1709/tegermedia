import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:excel/excel.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/models/score/_score.dart';
import 'package:tegarmedia/services/client.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'export_score.dart';
part 'score_services.dart';
