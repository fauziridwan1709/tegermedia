part of '_score.dart';

class ExportScoreServices {
  static Future<void> exportRekapPerSiswaToExcel({
    BuildContext context,
    String param,
    String mataPelajaran,
    String tahunAjaran,
    String namaKelas,
    String namaSiswa,
    bool isTugas,
    List<DataSummaryScore> data,
  }) async {
    var excel = Excel.createExcel();
    excel.rename('Sheet1', REKAP_PER_SISWA);
    var sheetObject = excel[REKAP_PER_SISWA];
    sheetObject.merge(CellIndex.indexByString('A1'), CellIndex.indexByString('E1'),
        customValue: 'Rekap Nilai $namaKelas');
    sheetObject.merge(CellIndex.indexByString('A2'), CellIndex.indexByString('E2'),
        customValue: '$tahunAjaran / $namaKelas / $namaSiswa');

    sheetObject.insertRowIterables(<String>[
      'No',
      'Nama Siswa',
      'Nilai Rata - rata Tugas',
      'Nilai Rata - rata Quiz',
      'Nilai Rata Rata Siswa'
    ], 3);
    var j = 4;
    for (var i = 0; i < data.length; i++) {
      await ScoreServices.getDetailSummaryScore(courseId: data[i].courseId, params: param)
          .then((getData) {});
    }
    var downloadDirectory = await getDownloadsDirectory();

    await excel.encode().then((value) {
      File('$downloadDirectory/rekap_nilai_$namaSiswa.xlsx')
        ..createSync(recursive: true)
        ..writeAsBytesSync(value);
    });
  }

  static Future<void> exportRekapPelajaranSiswaToExcel(
      {BuildContext context,
      String param,
      String tahunAjaran,
      String namaSiswa,
      bool isTugas,
      DataDetailSummaryScore data,
      List<CourseTeacher> teachers}) async {
    try {
      // ignore: unawaited_futures
      showDialog<void>(
          context: context, builder: (context) => Center(child: CircularProgressIndicator()));
      var excel = Excel.createExcel();
      excel.rename('Sheet1', REKAP_PER_SISWA_QUIZ);
      excel.copy(REKAP_PER_SISWA_QUIZ, REKAP_PER_SISWA_TUGAS);
      var sheetObject = excel[REKAP_PER_SISWA_QUIZ];
      var sheetObject2 = excel[REKAP_PER_SISWA_TUGAS];
      // excel.updateCell(
      //     REKAP_PER_SISWA_TUGAS, CellIndex.indexByColumnRow(rowIndex: 0), '',
      //     cellStyle: CellStyle(
      //         backgroundColorHex: '#1AFF1A',
      //         horizontalAlign: HorizontalAlign.Right));

      sheetObject.merge(
        CellIndex.indexByString('A1'),
        CellIndex.indexByString('E1'),
        customValue: 'Rekap Nilai Quiz ${data.courseName}',
      );

      sheetObject2.merge(
        CellIndex.indexByString('A1'),
        CellIndex.indexByString('E1'),
        customValue: 'Rekap Nilai Tugas ${data.courseName}',
      );

      // excel.updateCell(REKAP_PER_SISWA, CellIndex.indexByColumnRow(rowIndex: 1),
      //     'Rekap Nilai Quiz ${data.courseName}',
      //     cellStyle: CellStyle(
      //         backgroundColorHex: '#1AFF1A',
      //         horizontalAlign: HorizontalAlign.Center,
      //         fontSize: 14));

      sheetObject.merge(CellIndex.indexByString('A2'), CellIndex.indexByString('E2'),
          customValue: '$tahunAjaran / ${data.className} / $namaSiswa');

      sheetObject2.merge(CellIndex.indexByString('A2'), CellIndex.indexByString('E2'),
          customValue: '$tahunAjaran / ${data.className} / $namaSiswa');

      sheetObject.insertRowIterables(<String>['No', 'Tanggal', 'Nama Quiz', 'Nilai', 'Guru'], 2);

      sheetObject2.insertRowIterables(<String>['No', 'Tanggal', 'Nama Quiz', 'Nilai', 'Guru'], 2);

      var j = 3;
      var k = 3;
      if (data.haveAssignment) {
        data.listAssessment.forEach((element) {
          var gurus = <String>[];
          teachers.forEach((element) {
            gurus.add(element.teacherName);
          });

          sheetObject2.appendRow(<dynamic>[
            (j - 2).toString(),
            formattedDate(element.startDate),
            element.assessmentName,
            !element.score.isNegative ? element.score : 'Belum Dinilai atau Dikerjakan',
            gurus.join(', ')
          ]);
          j++;
        });
      }
      if (data.haveQuiz) {
        data.listQuiz.forEach((element) {
          var gurus = <String>[];
          teachers.forEach((element) {
            gurus.add(element.teacherName);
          });

          sheetObject.appendRow(<dynamic>[
            (k - 2).toString(),
            formattedDate(element.startDate),
            element.quizName,
            element.hasBeenDone
                ? element.score.isNegative
                    ? 'Belum Dinilai'
                    : element.score
                : 'Belum Dikerjakan',
            gurus.join(', ')
          ]);
          k++;
        });
      }
      var downloadsDirectory = await DownloadsPathProvider.downloadsDirectory;

      var status = await Permission.storage.status;
      if (status.isDenied) {
        await Permission.storage.request().isGranted.then((value) async {
          await excel.encode().then((value) async {
            var file = await File(
                '${downloadsDirectory.path}/rekap_nilai_${data.courseName}_$namaSiswa.xlsx')
              ..createSync(recursive: true)
              ..writeAsBytesSync(value);
            if (file.existsSync()) {
              Navigator.pop(context);
              CustomFlushBar.successFlushBar(
                'Berhasil mengekspor rekap quiz dan tugas',
                context,
              );
              await OpenFile.open(file.path);
            }
          });
        });
      } else if (status.isDenied) {
        await Permission.storage.request().isGranted.then((value) async {
          await excel.encode().then((value) async {
            var file = await File(
                '${downloadsDirectory.path}/rekap_nilai_${data.courseName}_$namaSiswa.xlsx')
              ..createSync(recursive: true)
              ..writeAsBytesSync(value);
            if (file.existsSync()) {
              Navigator.pop(context);
              CustomFlushBar.successFlushBar(
                'Berhasil mengekspor rekap quiz dan tugas',
                context,
              );
              await OpenFile.open(file.path);
            }
          });
        });
      } else {
        await Permission.storage.request().isGranted.then((value) async {
          print('mantap');
          await excel.encode().then((value) async {
            var file = await File(
                '${downloadsDirectory.path}/rekap_nilai_${data.courseName}_$namaSiswa.xlsx')
              ..createSync(recursive: true)
              ..writeAsBytesSync(value);
            if (file.existsSync()) {
              Navigator.pop(context);
              CustomFlushBar.successFlushBar(
                'Berhasil mengekspor rekap quiz dan tugas',
                context,
              );
              await OpenFile.open(file.path);
            }
          });
        });
      }
    } catch (e) {
      print(e);
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        'Gagal mengekspor',
        context,
      );
    }
  }
}
