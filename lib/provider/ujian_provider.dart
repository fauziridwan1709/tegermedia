part of '_provider.dart';

class QuizProviderKelas extends ChangeNotifier {
  FilterBankSoal _filter = FilterBankSoal();
  MateriQuizDetail _item = MateriQuizDetail();

  List<QuizItem> _list = <QuizItem>[];
  List<QuizModelDetail> _listQuiz = <QuizModelDetail>[];
  Map<String, dynamic> _selected = <String, dynamic>{};

  Penilaian _penilaian = Penilaian();
  QuizListDetail _detail = QuizListDetail();

  FilterBankSoal get filter => _filter;
  MateriQuizDetail get item => _item;

  List<QuizItem> get list => _list;
  List<QuizModelDetail> get listQuiz => _listQuiz;
  Map<String, dynamic> get selected => _selected;

  Penilaian get penilaian => _penilaian;
  QuizListDetail get detail => _detail;

  Future<void> switchHidden(BuildContext context) async {
    await QuizSettingServices.switchHide(data: _detail).then((value) {
      if (value.statusCode == StatusCodeConst.success) {
        _detail.isHidden = !_detail.isHidden;
      } else {
        CustomFlushBar.errorFlushBar(
          value.message,
          context,
        );
      }
    });
    notifyListeners();
  }

  Future<void> switchRandom(BuildContext context) async {
    await QuizSettingServices.switchRandom(data: _detail).then((value) {
      if (value.statusCode == StatusCodeConst.success) {
        _detail.isRandom = !_detail.isRandom;
      } else {
        CustomFlushBar.errorFlushBar(
          value.message,
          context,
        );
      }
    });
    notifyListeners();
  }

  Future<void> setSelected(Map<String, dynamic> newValue) async {
    _selected = newValue;
    notifyListeners();
  }

  Future<void> setQuestion(List<QuizItem> newValue) async {
    _list = newValue;
    notifyListeners();
  }

  Future<void> setQuizQuestion(List<QuizModelDetail> value) async {
    _listQuiz = value;
    notifyListeners();
  }

  Future<void> setFilter(FilterBankSoal value) async {
    _filter = value;
    notifyListeners();
  }

  Future<void> chooseItem(MateriQuizDetail value) async {
    _item = value;
    notifyListeners();
  }

  Future<void> getPenilaian(Penilaian value) async {
    _penilaian = value;
    notifyListeners();
  }

  Future<void> setDetail(QuizListDetail value) async {
    _detail = value;
    notifyListeners();
  }

  ///penilaian
  Future<Map<QuizAnswerData, bool>> _currentAssessment = Future.value(null);
  int _nilai = 0;
  int _totalSoal = 0;

  Future<Map<QuizAnswerData, bool>> get currentAssessment => _currentAssessment;
  int get nilai => _nilai;
  int get totalSoal => _totalSoal;

  Future<void> reset() async {
    _currentAssessment = Future.value(null);
    notifyListeners();
  }

  Future<void> addBool(QuizAnswerData key, bool status) async {
    if (_currentAssessment == Future.value(null)) {
      _currentAssessment = Future.value(<QuizAnswerData, bool>{});
      await _currentAssessment.then((value) => value[key] = status);
    } else {
      await _currentAssessment.then((value) => value[key] = status);
    }
    notifyListeners();
  }

  Future<void> setData(Map<QuizAnswerData, bool> newVal) async {
    _currentAssessment = Future.value(newVal);
    notifyListeners();
  }

  Future<void> addNilai(int benar) async {
    _nilai = benar;
    notifyListeners();
  }

  // Future<void> removeNilai(int salah) {
  //   _nilai = salah;
  //   notifyListeners();
  // }

  Future<void> setTotal(int newvalue) async {
    _nilai += newvalue;
  }
}

class FilterBankSoal {
  String kelas;
  String kurikulum;
  String matapelajaran;

  FilterBankSoal({this.kelas, this.kurikulum, this.matapelajaran});
}

class Penilaian {
  int courseId;
  String type;

  Penilaian({this.courseId, this.type});
}
