part of '../_provider.dart';

// class QuizProviderKelas extends ChangeNotifier {
//   FilterBankSoal _filter = FilterBankSoal();
//   MateriQuizDetail _item = MateriQuizDetail();
//
//   List<QuizItem> _list = List<QuizItem>();
//   List<QuizModelDetail> _listQuiz = List<QuizModelDetail>();
//   Map<String, dynamic> _selected = Map<String, dynamic>();
//
//   Penilaian _penilaian = Penilaian();
//   QuizListDetail _detail = QuizListDetail();
//
//   get filter => _filter;
//   get item => _item;
//
//   get list => _list;
//   get listQuiz => _listQuiz;
//   get selected => _selected;
//
//   get penilaian => _penilaian;
//   get detail => _detail;
//
//   setSelected(Map<String, dynamic> newValue) {
//     _selected = newValue;
//     notifyListeners();
//   }
//
//   setQuestion(List<QuizItem> newValue) {
//     _list = newValue;
//     notifyListeners();
//   }
//
//   setQuizQuestion(List<QuizModelDetail> value) {
//     _listQuiz = value;
//     notifyListeners();
//   }
//
//   setFilter(FilterBankSoal value) {
//     _filter = value;
//     notifyListeners();
//   }
//
//   chooseItem(MateriQuizDetail value) {
//     _item = value;
//     notifyListeners();
//   }
//
//   getPenilaian(Penilaian value) {
//     _penilaian = value;
//     notifyListeners();
//   }
//
//   setDetail(QuizListDetail value) {
//     _detail = value;
//     notifyListeners();
//   }
// }
//
// class FilterBankSoal {
//   String kelas;
//   String kurikulum;
//   String matapelajaran;
//
//   FilterBankSoal({this.kelas, this.kurikulum, this.matapelajaran});
// }
//
// class Penilaian {
//   int courseId;
//   String type;
//
//   Penilaian({this.courseId, this.type});
// }
