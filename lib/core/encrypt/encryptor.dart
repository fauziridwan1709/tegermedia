import 'package:encrypt/encrypt.dart';
import 'package:tegarmedia/core/environment/_environment.dart';

class Encriptor {
  static String encrypt(String val) {
    var key = Key.fromUtf8(Env.AES_FRONT_KEY);
    var iv = IV.fromUtf8(Env.AES_FRONT_IV);
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
    print('---this is encrypter');
    print(encrypter.encrypt(val, iv: iv).base64);
    return encrypter.encrypt(val, iv: iv).base64;
  }
}
