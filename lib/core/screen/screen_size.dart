part of '../_core.dart';

class S {
  static double w;
  static double h;
  static double hPad;
  static double vPad;
  static double hMar;
  static double vMar;

  void init(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.landscape) {
      w = MediaQuery.of(context).size.height;
      h = MediaQuery.of(context).size.width;
      hPad = w * .05;
      vPad = w * .05;
      hMar = w * .05;
      vMar = w * .03;
    } else {
      w = MediaQuery.of(context).size.width;
      h = MediaQuery.of(context).size.height;
      hPad = w * .05;
      vPad = w * .05;
      hMar = w * .05;
      vMar = w * .03;
    }
  }
}

int gridCount(double ratio) {
  if (ratio > 1.6) {
    return 9;
  } else if (ratio > 1.0) {
    return 6;
  } else {
    return 5;
  }
}

int gridCountHome(double ratio) {
  if (ratio > 2.2) {
    return 4;
  } else if (ratio > 1.3) {
    return 3;
  } else if (ratio > .9) {
    return 2;
  } else {
    return 1;
  }
}

int getSublist(double ratio) {
  if (ratio > 2.2) {
    return 4;
  } else if (ratio > 1.3) {
    return 3;
  } else {
    return 2;
  }
}
