part of '_screen.dart';

class SizingInformationBuilder extends StatelessWidget {
  final Widget Function(
      BuildContext context, SizingInformation sizingInformation) builder;
  const SizingInformationBuilder({Key key, this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(builder: (context, constraints) {
      var sizingInformation = SizingInformation(
        orientation: mediaQuery.orientation,
        deviceType: getDeviceScreenType(mediaQuery),
        localWidgetSize: Size(constraints.maxWidth, constraints.maxHeight),
        screenSize: mediaQuery.size,
      );
      return builder(context, sizingInformation);
    });
  }

  DeviceScreenType getDeviceScreenType(MediaQueryData mediaQuery) {
    var orientation = mediaQuery.orientation;
    var deviceWidth = 0.0;
    if (orientation == Orientation.landscape) {
      deviceWidth = mediaQuery.size.height;
    } else {
      deviceWidth = mediaQuery.size.width;
    }
    //todo desktop
    // if (deviceWidth > 950) {
    //   return DeviceScreenType.;
    // }
    if (deviceWidth > 600) {
      return DeviceScreenType.tablet;
    }
    return DeviceScreenType.mobile;
  }
}
