part of '_hive.dart';

class HiveDataBase {
  HiveDataBase._();

  static Future<void> init() async {
    var document = await getApplicationDocumentsDirectory();
    Hive.init(document.path);
    await Hive.openBox<dynamic>('home');
    await Hive.openBox<dynamic>('materi');
    await Hive.openBox<dynamic>('users');
    await Hive.openBox<dynamic>('me');
  }
}
