part of '_core.dart';

class SiswamediaGoogleSignIn {
  SiswamediaGoogleSignIn._();

  static final GoogleSignIn _googleSignIn = GoogleSignIn(
      // clientId: '962988932469-bnf5apljmsm2su3mf01v4l7ifag488gv.apps.googleusercontent.com',
      // clientId: '962988932469-83v5dfd808r2u2k52eq02kq1g4gmmetp.apps.googleusercontent.com',
      // clientId: '962988932469-t61apk0aqh6idr9eo685io1331t7cvr3.apps.googleusercontent.com',
      scopes: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email',
      ]);

  static GoogleSignIn getInstance() {
    return _googleSignIn;
  }

  static Future<bool> requestCalendar() async {
    var data = await _googleSignIn.requestScopes([
      'https://www.googleapis.com/auth/calendar',
    ]);
    return data;
  }
}
