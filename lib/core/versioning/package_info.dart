part of '_versioning.dart';

// PackageInfo _packageInfo = PackageInfo(
//   appName: 'Unknown',
//   packageName: 'Unknown',
//   version: 'Unknown',
//   buildNumber: 'Unknown',
// );

// @override
// void initState() {
//   super.initState();
//   _initPackageInfo();
// }
//
// Future<void> _initPackageInfo() async {
//   setState(() {
//     _packageInfo = info;
//   });
// }

class SiswamediaPackageInfo {
  SiswamediaPackageInfo({
    this.appName,
    this.packageName,
    this.version,
    this.buildNumber,
  });

  static Future<void> init() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    _packageInfo = SiswamediaPackageInfo(
        appName: info.appName,
        packageName: info.packageName,
        version: info.version,
        buildNumber: info.buildNumber);
  }

  static SiswamediaPackageInfo _packageInfo;

  static SiswamediaPackageInfo getInstance() {
    return _packageInfo;
  }

  final String appName;
  final String packageName;
  final String version;
  final String buildNumber;
}
