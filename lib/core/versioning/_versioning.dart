import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:html/parser.dart' show parse;
// import 'package:new_version/new_version.dart';
import 'package:package_info/package_info.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/extension/_extension.dart';

part 'check_version.dart';
part 'getAndroidStoreVersion.dart';
part 'package_info.dart';
