part of '_versioning.dart';

class StoreVersionService {
  static String get _appId => 'com.siswamedia.main';
  String version = '';

  static Future<String> _getAndroidStoreVersion(String id) async {
    final url = 'https://play.google.com/store/apps/details?id=$id';
    final response = await client.get(url);
    if (response.statusCode != 200) {
      print('Can\'t find an app in the Play Store with the id: $id');
      return null;
    }
    final document = parse(response.body);
    final elements = document.getElementsByClassName('hAyfc');
    final versionElement = elements.firstWhere(
      (elm) => elm.querySelector('.BgcNfc').text == 'Current Version',
    );
    var version = versionElement.querySelector('.htlgb').text;
    return version;
  }

  static Future<String> _getIOSStoreVersion(String id) async {
    final url = 'https://itunes.apple.com/lookup?bundleId=$id';
    final response = await client.get(url);
    if (response.statusCode != 200) {
      print('Can\'t find an app in the App Store with the id: $id');
      return null;
    }
    Map<String, dynamic> jsonObj = json.decode(response.body);
    String version = jsonObj['results'][0]['version'];
    return version;
  }

  static Future<String> getStoreVersion(BuildContext context) async {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
        return _getAndroidStoreVersion(_appId);
        break;
      case TargetPlatform.iOS:
        return _getIOSStoreVersion(_appId);
        break;
      default:
        print('This target platform is not yet supported by this package.');
        return null;
    }
  }
}
