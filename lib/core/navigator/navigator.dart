import 'package:flutter/material.dart';

//naviator 1.0
Future<void> navigate(BuildContext context, Widget destination,
    {RouteSettings settings}) {
  return Navigator.push(context,
      MaterialPageRoute<void>(builder: (_) => destination, settings: settings));
}

Future<void> navigateWithName(
    BuildContext context, Widget destination, String routeName) {
  return Navigator.push(
      context,
      MaterialPageRoute<void>(
          builder: (_) => destination,
          settings: RouteSettings(name: routeName)));
}

Future<void> navigateReplace(BuildContext context, Widget destination,
    {RouteSettings settings}) {
  return Navigator.pushReplacement(context,
      MaterialPageRoute<void>(builder: (_) => destination, settings: settings));
}

Future<void> popAndNavigate(BuildContext context, Widget destination,
    {RouteSettings settings}) {
  pop(context);
  return Navigator.push(context,
      MaterialPageRoute<void>(builder: (_) => destination, settings: settings));
}

void popUntil(BuildContext context, String name) {
  if (name == null) throw UnimplementedError('you must specify the route name');
  Navigator.popUntil(context, ModalRoute.withName(name));
}

void pop(BuildContext context) {
  Navigator.pop(context);
}

void pop2(BuildContext context) {
  pop(context);
  pop(context);
}
