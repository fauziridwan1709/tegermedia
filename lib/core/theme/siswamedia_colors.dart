part of '_theme.dart';

class SiswamediaColors {
  ///BLACK
  static const Color black = Color(0xFF1F2933);
  static const Color black2 = Color(0xFF323F4B);
  static const Color black3 = Color(0xFF475A6B);
  static const Color black4 = Color(0xFF7B8794);
  static const Color black5 = Color(0xFF9AA5B1);
  static const Color black6 = Color(0xFFCBD2D9);
  static const Color black7 = Color(0xFFE4E7EB);
  static const Color black8 = Color(0xFFF2F3F5);
  static const Color black9 = Color(0xFFF8F9FA);

  ///WHITE
  static const Color white = Color(0xFFFFFFFF);

  ///GREEN
  static const Color green = Color(0xFF7AA939);
  static const Color green2 = Color(0xFF8FC247);
  static const Color green3 = Color(0xFFABD175);
  static const Color green4 = Color(0xFFC7E0A3);
  static const Color green5 = Color(0xFFECF5E0);
  static const Color green6 = Color(0xFFF5FAF0);

  ///BLUE
  static const Color blue = Color(0xFF2C51B0);
  static const Color blue2 = Color(0xFF75A2E3);
  static const Color blue3 = Color(0xFFE9F4FF);
  static const Color blue4 = Color(0xFFFBFEFF);

  ///RED
  static const Color red = Color(0xFFFF4B4B);
  static const Color red2 = Color(0xFFFFE8E8);
  static const Color red3 = Color(0xFFFFF8F8);

  ///GREEN ALERT
  static const Color greenAlert = Color(0xFF00BB1D);
  static const Color greenAlert2 = Color(0xFFEBFFEE);

  ///YELLOW
  static const Color yellow = Color(0xFFFFD200);
}
