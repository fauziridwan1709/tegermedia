part of '_theme.dart';

enum ThemeType {
  ///normal theme
  normal,

  ///dark theme
  dark,
}

abstract class SiswamediaTheme {
  ThemeData normalTheme();
  ThemeData darkTheme();
  ThemeData mapTheme(ThemeType type);
}
