part of '_constants.dart';

class ByteConst {
  static const int gb = 1073741824;
  static const int mb = 1048576;
  static const int kb = 1024;

}