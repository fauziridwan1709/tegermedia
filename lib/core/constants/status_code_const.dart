part of '_constants.dart';

class StatusCodeConst {
  static const int success = 200;
  static const int created = 201;
  static const int noContent = 204;
  static const int badRequest = 400;
  static const int notFound = 404;
  static const int platform = 405;
  static const int timeout = 408;
  static const int conflict = 409;
  static const int general = 500;
  static const int noInternet = 503;
}
