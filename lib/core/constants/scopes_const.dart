part of '_constants.dart';

class ScopesConst {
  static const List<String> calendarScopes = <String>[
    'https://www.googleapis.com/auth/calendar',
  ];

  static const List<String> driveScopes = <String>[
    'https://www.googleapis.com/auth/drive.file',
  ];

  static const List<String> classroomScopes = <String>[
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/classroom.courses',
    'https://www.googleapis.com/auth/classroom.rosters',
  ];
}
