part of '_constants.dart';

class CacheKeyConst {
  static const String sppPendingState = 'SppPendingState';
  static const String sppSuccessState = 'SppSuccessState';
  static const String pulsaPendingState = 'PulsaPendingState';
  static const String pulsaSuccessState = 'PulsaSuccessState';
  static const String postPaidPendingState = 'PostPaidPendingState';
  static const String postPaidSuccessState = 'PostPaidSuccessState';
  static const String sppPpobState = 'sppPpobState';
}
