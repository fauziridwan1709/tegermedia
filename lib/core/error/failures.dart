part of '_error.dart';

abstract class Decide<T, K> {
  const Decide();
  B fold<B>(B ifLeft(T failure), B ifRight(K result));
  bool isLeft() => fold((_) => true, (_) => false);
  bool isRight() => fold((_) => false, (_) => true);
}

class Left<L, R> extends Decide<L, R> {
  final L _l;
  const Left(this._l);
  L get value => _l;

  @override
  B fold<B>(B Function(L t) ifLeft, B Function(R k) ifRight) => ifLeft(_l);
}

class Right<L, R> extends Decide<L, R> {
  final R _r;
  const Right(this._r);
  R get value => _r;

  @override
  B fold<B>(B Function(L t) ifLeft, B Function(R k) ifRight) => ifRight(_r);
}

abstract class Failure implements Exception {}

class NetworkFailure extends Failure {}

class CacheFailure extends Failure {}

class NotFoundFailure extends Failure {}

class BadRequestFailure extends Failure {}

class GeneralFailure implements Failure {
  String message;
  GeneralFailure(this.message);
}

class ServerFailure implements Failure {
  String message;
  ServerFailure(this.message);
}

class LargeFileFailure implements Failure {}

class AlreadyExistFailure implements Failure {}

class TimeoutFailure implements Failure {}

class ParseFailure implements Failure {}
