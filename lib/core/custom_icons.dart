part of '_core.dart';

class CustomIcons {
  CustomIcons._();

  static const _kFontFam = 'CustomIcons';
  static const _kFontFam2 = 'CustomIcons2';
  // static const _kFontPkg = null;

  static const IconData academy = IconData(0xe800, fontFamily: _kFontFam);
  static const IconData materi = IconData(0xe801, fontFamily: _kFontFam);
  static const IconData home = IconData(0xe802, fontFamily: _kFontFam);
  static const IconData kelas = IconData(0xe803, fontFamily: _kFontFam);
  static const IconData store =
      IconData(0xe800, fontFamily: _kFontFam2, fontPackage: null);
}

class ImageIcons {
  ImageIcons._();

  // static const Widget materi = Image.asset('');
}
