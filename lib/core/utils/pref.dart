part of '_utils.dart';

class Pref {
  final String AUTHORIZATION = 'Authorization';
  final String CONTENT_TYPE = 'Content-Type';
  static Map<String, String> header = {'Authorization': '', 'Content-Type': ''};

  static void saveToken(String token) async {
    var ref = await SharedPreferences.getInstance();
    var encryptedKey = Encriptor.encrypt(AUTH_KEY);
    //todo for refactor
    // var getToken = 'Bearer $token';
    await ref.setString('key', encryptedKey);
    // ref.setString(encryptedKey, getToken);
    await ref.setString(encryptedKey, token);
    await ref.setString(AUTH_KEY, token);
  }

  static Future<void> saveLassSelectedClassAsStringList(UserCurrentState state) async {
    var ref = await SharedPreferences.getInstance();
    var key = 'school${state.schoolId}lastClass${state.classId}';
    var value = <String>[state.classId.toString(), state.className, state.classRole];
    await ref.setStringList(key, value);
  }

  static Future<String> getToken() async {
    var ref = await SharedPreferences.getInstance();
    return ref.getString('token');
  }

  static Future<Map<String, String>> getHeader() async {
    var ref = await SharedPreferences.getInstance();
    var token = ref.getString(AUTH_KEY);
    return {'Authorization': token};
  }

  ///API v2
  static Future<Map<String, String>> getSiswamediaHeader() async {
    try {
      var ref = await SharedPreferences.getInstance();
      var encryptedKey = ref.getString('key') ?? '';
      header['Authorization'] = ref.getString(encryptedKey) ?? '';
      return {
        'Authorization': ref.getString(encryptedKey) ?? '',
        'Content-Type': 'application/json'
      };
    } catch (e) {
      print('-error');
      return {};
    }
  }

  static Future<Map<String, String>> getInfixHeader() async {
    try {
      return {
        'Accept': 'application/json',
        'Authorization': 'Basic amtnamt4Z1VJdWdrYSVeMzY6amtnamt4Z1VJdWdrYSVeMzY=',
        'Content-Type': 'application/json'
      };
    } catch (e) {
      print('-error');
      return {};
    }
  }

  static Future<Map<String, String>> getIdentityHeader() async {
    var ref = await SharedPreferences.getInstance();
    var encryptedKey = ref.getString('key');
    return {
      'Authorization': 'Bearer ${ref.getString(encryptedKey)}',
      'Content-Type': 'application/json'
    };
  }

  static Future<Map<String, String>> getIdentityHeaderOrder() async {
    var ref = await SharedPreferences.getInstance();
    var encryptedKey = ref.getString('key');
    return {
      'Authorization': 'Bearer ${ref.getString(encryptedKey)}',
      'Content-Type': 'application/json',
    };
  }

  static Future<Map<String, String>> getIdentityHeaderSignature() async {
    var ref = await SharedPreferences.getInstance();
    var encryptedKey = ref.getString('key');
    return {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${ref.getString(encryptedKey)}',
      // 'Signature':'CFC1DD0B4F34860AE706693063CEA667B6447D6653208FE1084AB4BB7317715C'
    };
  }
}
