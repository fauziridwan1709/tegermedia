part of '_utils.dart';

class PrecacheImage {
  //path di icons
  static const String materi = 'assets/icons/materi';
  static const String dokumen = 'assets/icons/dokumen';
  static const String kelas = 'assets/icons/kelas';
  static const String akademis = 'assets/icons/akademis';
  static const String home = 'assets/icons/home';
  static void cache(BuildContext context) {
    precacheImage(AssetImage('assets/navigation/feed_off.png'), context);
    precacheImage(AssetImage('assets/navigation/feed_on.png'), context);
    precacheImage(AssetImage('assets/navigation/home_off.png'), context);
    precacheImage(AssetImage('assets/navigation/home_on.png'), context);
    precacheImage(AssetImage('assets/navigation/materi_off.png'), context);
    precacheImage(AssetImage('assets/navigation/materi_on.png'), context);
    precacheImage(AssetImage('assets/navigation/pesan_off.png'), context);
    precacheImage(AssetImage('assets/navigation/pesan_on.png'), context);
    //Materi
    precacheImage(AssetImage('assets/materi_aplikasi.png'), context);
    precacheImage(AssetImage('assets/materi_topnav_background.png'), context);
    precacheImage(AssetImage('$materi/paud.png'), context);
    precacheImage(AssetImage('$materi/tk.png'), context);
    precacheImage(AssetImage('$materi/sd.png'), context);
    precacheImage(AssetImage('$materi/smp.png'), context);
    precacheImage(AssetImage('$materi/sma.png'), context);
    precacheImage(AssetImage('$materi/smk.png'), context);
    precacheImage(AssetImage('$materi/Biologi.png'), context);
    precacheImage(AssetImage('$materi/Ekonomi.png'), context);
    precacheImage(AssetImage('$materi/Fisika.png'), context);
    precacheImage(AssetImage('$materi/Akuntansi.png'), context);
    precacheImage(AssetImage('$materi/Antropologi.png'), context);
    precacheImage(AssetImage('$materi/Seni Budaya.png'), context);
    precacheImage(AssetImage('$materi/Agama Islam.png'), context);
    precacheImage(AssetImage('$materi/Agama Khonghucu.png'), context);
    precacheImage(AssetImage('$materi/Agama Kristen.png'), context);
    precacheImage(AssetImage('$materi/Agama Hindu.png'), context);
    precacheImage(AssetImage('$materi/IPA.png'), context);
    precacheImage(AssetImage('$materi/IPS.png'), context);
    precacheImage(AssetImage('$materi/PJOK.png'), context);
    precacheImage(AssetImage('$materi/PPKn.png'), context);
    precacheImage(AssetImage('$materi/Kepribadian.png'), context);
    precacheImage(AssetImage('$materi/Menghitung.png'), context);
    precacheImage(AssetImage('$materi/Membaca.png'), context);
    precacheImage(AssetImage('$materi/Panduan.png'), context);
    precacheImage(AssetImage('$materi/Prakarya.png'), context);
    precacheImage(AssetImage('$materi/Geografi.png'), context);
    precacheImage(AssetImage('$materi/Bahasa Indonesia.png'), context);
    precacheImage(AssetImage('$materi/Bahasa Inggris.png'), context);
    precacheImage(AssetImage('$materi/Kimia.png'), context);
    precacheImage(AssetImage('$materi/Matematika.png'), context);
    precacheImage(AssetImage('$materi/Sejarah.png'), context);
    precacheImage(AssetImage('$materi/Asah Otak.png'), context);
    precacheImage(AssetImage('$materi/Kompetensi.png'), context);
    precacheImage(AssetImage('$materi/Karakter.png'), context);
    //assets/icons/materi/materi umum
    precacheImage(AssetImage('$materi/umum/Bahasa dan Budaya.png'), context);
    precacheImage(AssetImage('$materi/umum/Hobi.png'), context);
    precacheImage(AssetImage('$materi/umum/Interpersonal Skill.png'), context);
    precacheImage(AssetImage('$materi/umum/Kecerdasan.png'), context);
    precacheImage(AssetImage('$materi/umum/Kewirausahaan dan Finansial.png'), context);
    precacheImage(AssetImage('$materi/umum/Persiapan Kuliah.png'), context);
    precacheImage(AssetImage('$materi/umum/Personal Growth.png'), context);
    precacheImage(AssetImage('$materi/umum/Sains.png'), context);
    precacheImage(AssetImage('$materi/umum/Teknologi & Desain.png'), context);
    //assets/icons/dokumen
    precacheImage(AssetImage('$dokumen/empty.png'), context);
    precacheImage(AssetImage('$dokumen/keperluan.png'), context);
    //assets/icons/kelas
    precacheImage(AssetImage('assets/kelas/Jadwal.png'), context);
    precacheImage(AssetImage('assets/kelas/kelasku.png'), context);
    precacheImage(AssetImage('assets/kelas/konferensi.png'), context);
    precacheImage(AssetImage('assets/kelas/kuis.png'), context);
    precacheImage(AssetImage('$kelas/add.png'), context);
    precacheImage(AssetImage('$kelas/belum-ada-tugas.png'), context);
    precacheImage(AssetImage('$kelas/book_lover.png'), context);
    precacheImage(AssetImage('$kelas/conference_empty.png'), context);
    precacheImage(AssetImage('$kelas/conference_success.png'), context);
    precacheImage(AssetImage('$kelas/confirmation.png'), context);
    precacheImage(AssetImage('$kelas/empty.png'), context);
    precacheImage(AssetImage('$kelas/join.png'), context);
    precacheImage(AssetImage('$kelas/kelas.png'), context);
    precacheImage(AssetImage('$kelas/kelas_empty.png'), context);
    precacheImage(AssetImage('$kelas/kelas_personal_empty.png'), context);
    precacheImage(AssetImage('$kelas/newsletter.png'), context);
    precacheImage(AssetImage('$kelas/Tugas.png'), context);
    precacheImage(AssetImage('$kelas/with_love.png'), context);
    //assets/icons/kelas
    precacheImage(AssetImage('$akademis/absen.png'), context);
    precacheImage(AssetImage('$akademis/academic_background.png'), context);
    precacheImage(AssetImage('$akademis/administrasi_icon.png'), context);
    precacheImage(AssetImage('$akademis/alumni.png'), context);
    precacheImage(AssetImage('$akademis/bayar.png'), context);
    precacheImage(AssetImage('$akademis/belajar covid.png'), context);
    precacheImage(AssetImage('$akademis/belajar kombinasi.png'), context);
    precacheImage(AssetImage('$akademis/belajar offline.png'), context);
    precacheImage(AssetImage('$akademis/belajar online.png'), context);
    precacheImage(AssetImage('$akademis/confrence.png'), context);
    precacheImage(AssetImage('$akademis/grafik.png'), context);
    precacheImage(AssetImage('$akademis/kuis.png'), context);
    precacheImage(AssetImage('$akademis/materi.png'), context);
    precacheImage(AssetImage('$akademis/nilai.png'), context);
    precacheImage(AssetImage('$akademis/pembayaran.png'), context);
    precacheImage(AssetImage('$akademis/registrasi.png'), context);
    precacheImage(AssetImage('$akademis/tugas.png'), context);
    precacheImage(AssetImage('$akademis/zona.png'), context);

    //home icons
    precacheImage(AssetImage('$home/absensi.png'), context);
    precacheImage(AssetImage('$home/aktifitas.png'), context);
    precacheImage(AssetImage('$home/alumni.png'), context);
    precacheImage(AssetImage('$home/artikel.png'), context);
    precacheImage(AssetImage('$home/bantuan.png'), context);
    precacheImage(AssetImage('$home/beasiswa.png'), context);
    precacheImage(AssetImage('$home/belanja.png'), context);
    precacheImage(AssetImage('$home/board_icon.png'), context);
    precacheImage(AssetImage('$home/dokumen.png'), context);
    precacheImage(AssetImage('$home/donasi.png'), context);
    precacheImage(AssetImage('$home/forum.png'), context);
    precacheImage(AssetImage('$home/gudang materi.png'), context);
    precacheImage(AssetImage('$home/jadwal.png'), context);
    precacheImage(AssetImage('$home/kelasku.png'), context);
    precacheImage(AssetImage('$home/konferensi.png'), context);
    precacheImage(AssetImage('$home/lainnya.png'), context);
    precacheImage(AssetImage('$home/nilai.png'), context);
    precacheImage(AssetImage('$home/pendaftaran.png'), context);
    precacheImage(AssetImage('$home/protokol.png'), context);
    precacheImage(AssetImage('$home/topup.png'), context);
    precacheImage(AssetImage('$home/tugas.png'), context);
    precacheImage(AssetImage('$home/ujian.png'), context);

    //dialog
    precacheImage(AssetImage('assets/dialog/pilih_kelas.png'), context);
    precacheImage(AssetImage('assets/dialog/sorry_no_student.png'), context);

    //forum
    precacheImage(AssetImage('assets/images/no_forum.png'), context);

    //other
    precacheImage(AssetImage('assets/404.png'), context);
    precacheImage(AssetImage('assets/images/png/payment_info.png'), context);
    precacheImage(AssetImage('assets/icons/pantau/pantau.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-01_dashboard.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-02_student profile.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-03_regular timetable.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-04_exam timetable.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-05_extracurricular schedule.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-06_assignment.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-07_quiz.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-08_e-learning.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-09_virtual class.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-10_study Result.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-11_student evaluate.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-12_call teacher.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-13_student attendance.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-14_student leave.png'), context);
    precacheImage(AssetImage('assets/Siswamedia Icon-15_e-payment.png'), context);
    precacheImage(AssetImage('assets/jadwal_detail.png'), context);
    precacheImage(AssetImage('assets/jadwal_upload.png'), context);
    precacheImage(AssetImage('assets/BG_profile.png'), context);

    //payment
    precacheImage(AssetImage('assets/icons/pantau/pantau.png'), context);
    precacheImage(AssetImage('assets/images/png/payment_Acara.png'), context);
    precacheImage(AssetImage('assets/images/png/payment_info.png'), context);
    precacheImage(AssetImage('assets/images/png/payment_Kegiatan.png'), context);
    precacheImage(AssetImage('assets/images/png/payment_Perlengkapan.png'), context);
    precacheImage(AssetImage('assets/images/png/payment_Spp.png'), context);
    precacheImage(AssetImage('assets/images/png/social.png'), context);
    precacheImage(AssetImage('assets/images/png/payment_info.png'), context);

    //network
    precacheImage(
        NetworkImage('https://i.ibb.co/TRj0YQ5/Banner-Mobile-Apps-Fitur-Quiz-Siswamedia-02.png'),
        context);
    precacheImage(NetworkImage('https://i.ibb.co/pdybc4K/Banner-Mobile-Apps-PJJ-01.png'), context);
    precacheImage(
        NetworkImage('https://i.ibb.co/chrQYDT/Banner-Mobile-Apps-Tips-PJJ-orang-tua-03.png'),
        context);
    precacheImage(
        NetworkImage(
            'https://i.ibb.co/GkLp8R5/Banner-Mobile-Apps-Proses-Pengambilan-Keputusan-04.png'),
        context);
    precacheImage(
        NetworkImage(
            'https://image.freepik.com/free-vector/group-students-watching-online-webinar_74855-5514.jpg'),
        context);
    precacheImage(
        NetworkImage(
            'https://img.freepik.com/free-vector/kids-online-lessons-concept_23-2148520727.jpg?size=626&ext=jpg&ga=GA1.2.1688244698.1578555744'),
        context);
    precacheImage(
        NetworkImage('http://ourcanadaproject.ca/wp-content/uploads/2017/01/masset-1483974193.jpg'),
        context);
    precacheImage(
        NetworkImage('https://th.bing.com/th/id/OIP.wSdY9Vda2B1pUmH6wFDN4wHaEr?pid=Api&rs=1'),
        context);
  }
}
