import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/encrypt/encryptor.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';

part 'debug_mode.dart';
part 'precache_image_util.dart';
part 'pref.dart';
part 'utils.dart';
