part of '_utils.dart';

final id = 'com.siswamedia.main';
final url = 'https://play.google.com/store/apps/details?id=$id';

class Utils {
  static void log(dynamic message, {String info}) {
    if (info != null) {
      print('--- $info ---');
    }
    print(message);
  }

  ///with two dotes
  static void logWithDotes(dynamic message, {String info}) {
    log(message, info: info);
    log('.');
    log('.');
  }

  static double remap(double value, double originalMinValue, double originalMaxValue,
      double translatedMinValue, double translatedMaxValue) {
    if (originalMaxValue - originalMinValue == 0) {
      return 0;
    }

    return (value - originalMinValue) /
            (originalMaxValue - originalMinValue) *
            (translatedMaxValue - translatedMinValue) +
        translatedMinValue;
  }

  static Future<bool> isImageInCached(String url) async {
    var directory = await cachePath();
    var file = File(directory + '/' + url);
    if (file.existsSync()) {
      return true;
    }
    return false;
  }

  static void cacheImageFromUrl(String url, String filename) async {
    var response = await client.get(url);
    var directory = await cachePath();
    var firstPath = directory;
    // await Directory(firstPath).create(recursive: true);
    var filePath = File(directory + '/' + '${filename.split('/').join('')}.jpg');
    filePath.writeAsBytesSync(response.bodyBytes, flush: true, mode: FileMode.write);
  }

  static Future<String> cachePath() async {
    if (Platform.isIOS) {
      var dir = await getApplicationDocumentsDirectory();
      return dir.path;
    } else {
      var dir = await getTemporaryDirectory();
      return dir.path;
    }
  }
}
