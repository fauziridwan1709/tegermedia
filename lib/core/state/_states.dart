import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/states/auth/_auth.dart';
import 'package:tegarmedia/states/global_state.dart';

part 'cleaner.dart';
part 'future_state.dart';
part 'global_state.dart';
