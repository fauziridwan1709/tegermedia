part of '_states.dart';

class Cleaner {
  void cleanState() {
    GlobalState.schedule().refresh();
    GlobalState.absensi().refresh();
    GlobalState.document().refresh();
    GlobalState.course().refresh();
    GlobalState.assignment().refresh();
    GlobalState.interaction().refresh();
    GlobalState.conference().refresh();
    GlobalState.journey().refresh();
  }

  void cleanLogout() {
    GlobalState.auth().refresh();
    GlobalState.profile().refresh();
    GlobalState.classes().refresh();
    GlobalState.schedule().refresh();
    GlobalState.notification().refresh();
    GlobalState.detailClass().refresh();
    GlobalState.school().refresh();
    GlobalState.course().refresh();
    GlobalState.assignment().refresh();
    GlobalState.forum().refresh();
    GlobalState.document().refresh();
    GlobalState.conference().refresh();
    GlobalState.interaction().refresh();
    GlobalState.sppPending().refresh();
    GlobalState.sppSuccess().refresh();
    GlobalState.pulsaPending().refresh();
    GlobalState.pulsaSuccess().refresh();
    GlobalState.edukasiPending().refresh();
    GlobalState.edukasiSuccess().refresh();
  }
}
