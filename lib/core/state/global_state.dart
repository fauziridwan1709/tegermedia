part of '_states.dart';

class GS {
  // static var injectData = <Injectable>[
  //   Inject(() => ClassState()),
  //   Inject(() => NotificationState()),
  //   Inject(() => ConferenceState()),
  //   Inject(() => NotificationState()),
  //   Inject(() => ProgressMessageState()),
  //   Inject(() => EditAbsensiState()),
  //   Inject(() => ClassroomState()),
  //   Inject(() => ProfileState()),
  //   Inject(() => FilterAssignmentState()),
  //   Inject(() => DocumentState()),
  //   Inject(() => AssignmentState()),
  //   Inject(() => PaymentState()),
  //   Inject(() => AuthState()),
  //   Inject(() => ProgressUploadState()),
  //   Inject(() => ScheduleState()),
  //   Inject(() => AbsensiState()),
  //   Inject(() => MateriState()),
  //   Inject(() => ForumState()),
  //   Inject(() => MultiSelectClassState()),
  //   Inject(() => SchoolState()),
  //   Inject(() => CourseState()),
  //   Inject(() => DetailClassState()),
  //   Inject(() => InteractionState()),
  // ];

  // @override
  static ReactiveModel<T> instance<T>() {
    return Injector.getAsReactive<T>();
  }

  static T state<T>() {
    return Injector.getAsReactive<T>().state;
  }

  static Future<void> setState<T>(
    Function(T) func, {
    void Function(BuildContext context) onSetState,
    void Function(BuildContext context) onRebuildState,
    void Function(BuildContext context, dynamic error) onError,
    void Function(BuildContext context, T model) onData,
  }) async {
    return Injector.getAsReactive<T>().setState((T s) => func,
        onSetState: onSetState,
        onData: onData,
        onError: onError,
        onRebuildState: onRebuildState);
  }

  static ReactiveModel<AuthState> auth() {
    return Injector.getAsReactive<AuthState>();
  }

  // static ReactiveModel<SchoolState> school() {
  //   return Injector.getAsReactive<SchoolState>();
  // }
  //
  // static ReactiveModel<ProfileState> profile() {
  //   return Injector.getAsReactive<ProfileState>();
  // }
  //
  // static ReactiveModel<ClassState> classes() {
  //   return Injector.getAsReactive<ClassState>();
  // }
  //
  // static ReactiveModel<DetailClassState> detailClass() {
  //   return Injector.getAsReactive<DetailClassState>();
  // }
  //
  // static ReactiveModel<ScheduleState> schedule() {
  //   return Injector.getAsReactive<ScheduleState>();
  // }
  //
  // static ReactiveModel<ForumState> forum() {
  //   return Injector.getAsReactive<ForumState>();
  // }
  //
  // static ReactiveModel<CourseState> course() {
  //   return Injector.getAsReactive<CourseState>();
  // }
  //
  // static ReactiveModel<AbsensiState> absensi() {
  //   return Injector.getAsReactive<AbsensiState>();
  // }
  //
  // static ReactiveModel<EditAbsenSelectedState> editAbsensiSelected() {
  //   return Injector.getAsReactive<EditAbsenSelectedState>();
  // }
  //
  // static ReactiveModel<AssignmentState> assignment() {
  //   return Injector.getAsReactive<AssignmentState>();
  // }
  //
  // static ReactiveModel<MultiSelectClassState> multiSelectClass() {
  //   return Injector.getAsReactive<MultiSelectClassState>();
  // }
  //
  //
  // static ReactiveModel<ConferenceState> conference() {
  //   return Injector.getAsReactive<ConferenceState>();
  // }
  //
  // static ReactiveModel<DocumentState> document() {
  //   return Injector.getAsReactive<DocumentState>();
  // }
  //
  // static ReactiveModel<MateriState> materi() {
  //   return Injector.getAsReactive<MateriState>();
  // }
  //
  // static ReactiveModel<QuizState> quiz() {
  //   return Injector.getAsReactive<QuizState>();
  // }
  //
  // static ReactiveModel<NotificationState> notification() {
  //   return Injector.getAsReactive<NotificationState>();
  // }
  //
  // static ReactiveModel<InteractionState> interaction() {
  //   return Injector.getAsReactive<InteractionState>();
  // }
  //
  // static ReactiveModel<TeacherState> teachers() {
  //   return Injector.getAsReactive<TeacherState>();
  // }
  //
  // static ReactiveModel<StudentState> students() {
  //   return Injector.getAsReactive<StudentState>();
  // }
}
