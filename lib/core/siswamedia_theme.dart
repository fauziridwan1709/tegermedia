part of '_core.dart';

class SiswamediaTheme {
  SiswamediaTheme.privateConstructor();
  // static MaterialColor primary = MaterialColor(
  //   0xFF7BA939,
  //   <int, Color>{
  //     50: Color(0xFF7BA939),
  //     100: Color(0xFF7BA939),
  //     200: Color(0xFF7BA939),
  //     300: Color(0xFF7BA939),
  //     400: Color(0xFF7BA939),
  //     500: Color(0xFF7BA939),
  //     600: Color(0xFF7BA939),
  //     700: Color(0xFF7BA939),
  //     800: Color(0xFF7BA939),
  //     900: Color(0xFF7BA939),
  //   },
  // );

  static MaterialColor primary = MaterialColor(
    0xFFB23134,
    <int, Color>{
      50: Color(0xFFB23134),
      100: Color(0xFFB23134),
      200: Color(0xFFB23134),
      300: Color(0xFFB23134),
      400: Color(0xFFB23134),
      500: Color(0xFFB23134),
      600: Color(0xFFB23134),
      700: Color(0xFFB23134),
      800: Color(0xFFB23134),
      900: Color(0xFFB23134),
    },
  );
  static MaterialColor materialGreen = MaterialColor(
    0xFFB23134,
    <int, Color>{
      100: Color(0xFFD65258),
      400: Color(0xFFB23134),
    },
  );
  static const Color green = Color(0xFFB23134);
  static const Color transparent = Colors.transparent;
  static const Color shadow = Color(0x08000014);

  ///red
  static const Color red = Color(0xFFFF4B4B);
  static const Color newRed = Color(0xFFEF5E5E);
  static const Color textError = Color(0xFFEB7979);

  ///white
  static MaterialColor textGrey = MaterialColor(
    0xff949494,
    <int, Color>{
      100: Color(0xFFDFDFDF),
      200: Color(0xFF7E7E7E),
      300: Color(0xFFF8F8F8),
      400: Color(0xFF9D9D9D),
      500: Color(0xFF4B4B4B),
      260: Color(0xFF858585),
    },
  );
  static const Color greyBg = Color(0xFFEFEFEF);
  static const Color lightGreen = Color(0xFFE8F5D6);
  static const Color greybgButton = Color(0xFFBFBFBF);
  static const Color greyDivider = Color(0xFFF9F9F9);
  static const Color greyIcon = Color(0xFFA9A9A9);
  static const Color moreLightGreen = Color(0xFFF6FFE9);
  static const Color border = Color(0xffCFCFCF);
  static const Color lightBorder = Color(0xFFECECEC);
  static const Color borderGrey = Color(0xffD5D5D5);
  static const Color nearlyWhite = Color(0xFFFAFAFA);
  static const Color white = Color(0xFFFFFFFF);
  static const Color background = Color(0xFFF2F3F8);
  static const Color newBackground = Color(0xFFF2F3F5);

  static MaterialColor grey = Colors.grey;
  static const Color grey600 = Color(0xFFCBD2D9);

  static const Color black = Color(0xFF000000);
  static const Color black400 = Color(0xFF43425D);
  static const Color nearBlack = Color(0xFF4B4B4B);
  static const Color blue = Color(0xFF6385C1);
  static const Color lightBlue = Color(0xff639BC1);
  static const Color textLightBlue = Color(0xff79A7EB);
  static const Color blueBackground = Color(0xFF43A0DE);
  static const Color textBlue = Color(0xff7171FF);
  static const Color lightBlack = Color(0xff707070);

  static const Color semiBoldText = Color(0xFF4B4B4B);

  static const Color nearlyBlack = Color(0xFF213333);
  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);

  ///for rubik font
  static const Color rubikBlack1 = Color(0xFF1F2933);
  static const Color rubikBlack4 = Color(0xFF7B8794);
  static const Color rubikBlue1 = Color(0xFF2C51B0);
  static const String fontName = 'Roboto';

  static TextStyle descBlack = GoogleFonts.openSans(
    color: Colors.black54,
    fontWeight: FontWeight.normal,
    fontSize: 10,
  );
  static TextStyle semiBlack = GoogleFonts.openSans(
    color: Colors.black54,
    fontWeight: FontWeight.w600,
    fontSize: 12,
  );
  static TextStyle boldBlack = GoogleFonts.openSans(
    color: Colors.black87,
    fontWeight: FontWeight.w700,
    fontSize: 12,
  );
  static TextStyle countDown = GoogleFonts.openSans(
    fontWeight: FontWeight.w600,
    color: newRed,
    fontSize: 14,
  );

  static void loading(BuildContext context, {bool isDismissible = true}) {
    showDialog<void>(
        barrierDismissible: isDismissible,
        context: context,
        builder: (context) => Center(child: CircularProgressIndicator()));
  }

  static void infoLoading({BuildContext context, String info}) {
    showDialog<void>(
        barrierDismissible: true,
        context: context,
        builder: (context) => WillPopScope(
              onWillPop: () async => true,
              child: Dialog(
                  shape: RoundedRectangleBorder(borderRadius: radius(12)),
                  child: Container(
                    width: S.w * .8,
                    padding: EdgeInsets.symmetric(vertical: S.w * .05, horizontal: S.w * .05),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Padding(
                                padding: EdgeInsets.only(left: S.w * .05),
                                child: SpinKitDoubleBounce(
                                  color: SiswamediaTheme.green,
                                  size: 30,
                                )),
                            SizedBox(
                              width: 30,
                              height: 45,
                            ),
                            Expanded(child: CustomText(info ?? '', Kind.heading3)),
                          ],
                        ),
                      ],
                    ),
                  )),
            ));
  }

  static BoxDecoration dropDownDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(S.w), color: Colors.white, boxShadow: [shadowContainer]);

  static BoxDecoration whiteRadiusShadow = BoxDecoration(
      color: white, borderRadius: BorderRadius.circular(12), boxShadow: [shadowContainer]);

  static BoxShadow shadowContainer =
      BoxShadow(color: shadow, offset: Offset(0, 0), spreadRadius: 2, blurRadius: 2);
}

BorderRadius radius(double width) {
  return BorderRadius.circular(width);
}

const double defaultMargin = 20;

TextStyle blackTextFont =
    GoogleFonts.raleway().copyWith(color: Colors.black, fontWeight: FontWeight.w500);

const TextStyle headingWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontFamily: 'OpenSans');

const TextStyle secondHeadingWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontFamily: 'OpenSans');

const TextStyle descWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontFamily: 'OpenSans');

const TextStyle boldWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontFamily: 'OpenSansBold');

const TextStyle semiWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontFamily: 'OpenSans');

const TextStyle headingBlack =
    TextStyle(color: Colors.black87, fontWeight: FontWeight.w700, fontFamily: 'OpenSans');

const TextStyle secondHeadingBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontFamily: 'OpenSans');

const TextStyle descBlack =
    TextStyle(color: Colors.black54, fontWeight: FontWeight.normal, fontFamily: 'OpenSans');

const TextStyle mediumBlack =
    TextStyle(color: Colors.black87, fontWeight: FontWeight.w500, fontFamily: 'OpenSans');

const TextStyle boldBlack =
    TextStyle(color: Colors.black87, fontWeight: FontWeight.w700, fontFamily: 'OpenSans');

const TextStyle semiBlack =
    TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontFamily: 'OpenSans');

TextStyle hintText(BuildContext context) {
  return TextStyle(fontSize: S.w / 30, color: Colors.black38, fontWeight: FontWeight.normal);
}

TextStyle normalText(BuildContext context) {
  return TextStyle(fontSize: S.w / 30, color: Colors.black87, fontWeight: FontWeight.normal);
}

TextStyle greenText(BuildContext context) {
  return TextStyle(fontSize: S.w / 30, color: SiswamediaTheme.green, fontWeight: FontWeight.normal);
}

InputDecoration descriptionFiled(BuildContext context, {String hint}) {
  return InputDecoration(
    hintText: hint,
    hintStyle: hintText(context),
    contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
    border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(color: SiswamediaTheme.border)),
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(color: SiswamediaTheme.border)),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(color: SiswamediaTheme.green)),
  );
}

InputDecoration textField(BuildContext context, {String hint}) {
  return InputDecoration(
    hintText: hint,
    hintStyle: hintText(context),
    contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
    border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: SiswamediaTheme.border)),
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: SiswamediaTheme.border)),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: SiswamediaTheme.green)),
  );
}

InputDecoration fullRadiusField(BuildContext context, {String hint, bool label = false}) {
  return inputDecoration.copyWith(
    hintText: hint,
    hintStyle: hintText(context),
    contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 24),
    labelText: label ? hint : '',
  );
}

InputDecoration get inputDecoration => InputDecoration(
      hintStyle: TextStyle(fontSize: 12),
      contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(200),
          borderSide: BorderSide(color: SiswamediaTheme.border)),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(200),
          borderSide: BorderSide(color: SiswamediaTheme.border)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(200),
          borderSide: BorderSide(color: SiswamediaTheme.border)),
    );
