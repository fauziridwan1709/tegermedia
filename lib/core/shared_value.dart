part of '_core.dart';

Future<String> getToken() async {
  var pref = await SharedPreferences.getInstance();
  return pref.getString(AUTH_KEY);
}

const String prod = 'https://api.siswamedia.com';
const String staging = 'https://stg-api.siswamedia.com';
const String googleApiDev = 'https://dev-id.siswamedia.com';
const String googleApiProd = 'https://id.siswamedia.com';
const String baseGoogleApiUrl = 'https://www.googleapis.com';

const String uriProd = 'api.siswamedia.com';
const String uriStag = 'stg-api.siswamedia.com';
const String devChat = 'https://dev-chat.siswamedia.com';
const String prodChat = 'https://chat.siswamedia.com';

// /currentstate of url
// const String baseUri = uriProd;
// const String baseUrl = prod;
// const String baseUrlGoogle = googleApiProd;
// const String urlChat = prodChat;
// const String apiUrl = '$baseUrl/$version';

String baseUri = SiswamediaConfig.isStaging ? uriStag : uriProd;
String baseUrl = SiswamediaConfig.isStaging ? staging : prod;
String baseUrlGoogle =
    SiswamediaConfig.isStaging ? googleApiDev : googleApiProd;
String urlChat = SiswamediaConfig.isStaging ? devChat : prodChat;
String apiUrl =
    SiswamediaConfig.isStaging ? '$baseUrl/$version' : '$baseUrl/$version';

const String homeRoute = '/';
const String feedRoute = 'kelas';

const String FLAG = 'flag-production';
const String ppob_dev = 'https://dev-ppob.siswamedia.com';
const String market_dev = 'https://dev-market.siswamedia.com';
// const String market_dev = 'http://127.0.0.1:8080';

const String socket_url_dev = 'wss://dev-socket.siswamedia.com/v1/ws';
const String FULL_NAME = 'fullname';
const String SCHOOL_NAME = 'school_name';
const String SCHOOL_ID = 'school_id';
const String SCHOOL_TYPE = 'type';
const String SCHOOL_ROLE = 'school_role';
const String CLASS_NAME = 'class_name';
const String CLASS_ID = 'class_id';
const String CLASS_ROLE = 'class_role';
const String IS_ADMIN = 'is_admin';
const String FAVORITE = 'favoritev1.2';

///success


const String REKAP_PER_SISWA_QUIZ = 'Rekap Per Siswa Quiz';
const String REKAP_PER_SISWA_TUGAS = 'Rekap Per Siswa Tugas';
const String REKAP_PER_SISWA = 'Rekap Per Siswa';
const String REKAP_PER_KELAS = 'Rekap Per Kelas';
