part of '_core.dart';

///this exceptions will no longer used (deprecated soon)

class SiswamediaError {
  static SocketException noInternet() {
    return SocketException('No Internet connection');
  }

  static TimeoutException timeout() {
    return TimeoutException('Timeout, try again');
  }

  static SiswamediaException general() {
    return SiswamediaException('Terjadi kesalahan');
  }
}

class SiswamediaException {
  final String message;
  SiswamediaException(this.message);
}

class LargeFileException implements Exception {
  String cause;
  LargeFileException({this.cause});
}

class ErrorUploadException implements Exception {
  String cause;
  ErrorUploadException({this.cause});
}

class GeneralException implements Exception {
  String cause;
  GeneralException({this.cause});
}

class BadRequestException implements Exception {
  String cause;
  int statusCode;
  BadRequestException({this.cause, this.statusCode});
}

class ConflictException implements Exception {
  String cause;
  int statusCode;

  ConflictException({this.cause, this.statusCode});
}

class EmptyException implements Exception {
  String message;
  EmptyException({this.message});
}

class GeneralError implements Exception {
  int statusCode;
  String message;
  GeneralError({this.statusCode, this.message});
}

class AlreadyExistException implements Exception {
  String message;
  AlreadyExistException({this.message});
}

class UploadImageFailed implements Exception {
  String message;
  UploadImageFailed({this.message});
}

class UploadFileFailed implements Exception {
  String message;
  UploadFileFailed({this.message});
}

class UnprocessableEntityException implements Exception {
  String cause;
  UnprocessableEntityException({this.cause});
}
