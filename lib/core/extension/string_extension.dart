part of '_extension.dart';

extension StringExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${substring(1)}';
  String get allInCaps => toUpperCase();
  String get capitalizeFirstOfEach => this != null
      ? split(' ')
          .map((word) => word != ''
              ? (word.trimLeft()[0].toUpperCase() +
                  word.substring(1).toLowerCase())
              : '')
          .join(' ')
      : this;
  String get capitalize => this[0].toUpperCase() + substring(1);
  String get extensionType => contains('.')
      ? split('.').last.length < 8
          ? split('.').last
          : ''
      : '';
  String get showNarration => isEmpty ? 'Tidak ada deskripsi' : this;
  int get toInteger => int.parse(this);
  bool get isSiswaOrOrtu => (this == 'SISWA' || this == 'ORTU');
  bool get isGuruOrWaliKelas => (this == 'GURU' || this == 'WALIKELAS');
  bool get isOrtu => (this == 'ORTU');
  bool get isSiswa => (this == 'SISWA');
  bool get isGuru => (this == 'GURU');
  bool get isWaliKelas => (this == 'WALIKELAS');
  bool get isUpdate => (this == 'update');
  bool get isMale => (this == 'Laki-Laki');

  String get filename => (split('/').last);
}
