part of '_extension.dart';

extension DateTimeExtension on DateTime {
  DateTime get yearMonthDay => DateTime(year, month, day);
  DateTime get yearMonthDayHourMinute =>
      DateTime(year, month, day, hour, minute);
  DateTime get startMonth =>
      DateTime.parse(timeInString).subtract(Duration(days: day - 1));
  String get timeInString => toIso8601String().substring(0, 19);
  int get totalDayInMonth {
    var beginningNextMonth =
        (month < 12) ? DateTime(year, month + 1, 1) : DateTime(year + 1, 1, 1);
    return beginningNextMonth.subtract(Duration(days: 1)).day;
  }
}
