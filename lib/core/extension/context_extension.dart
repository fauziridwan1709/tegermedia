part of '_extension.dart';

extension BuildContextExtension on BuildContext {
  Future<T> push<T>(Widget widget, {String name}) => name == null
      ? Navigator.push<T>(this, MaterialPageRoute(builder: (_) => widget))
      : Navigator.push<T>(
          this, MaterialPageRoute(builder: (_) => widget, settings: RouteSettings(name: name)));

  Future<T> pushReplacement<T>(Widget widget) =>
      Navigator.pushReplacement<T, T>(this, MaterialPageRoute(builder: (_) => widget));

  void pop() => Navigator.pop(this);
}
