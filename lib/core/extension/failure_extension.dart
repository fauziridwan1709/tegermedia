part of '_extension.dart';

extension FailureExtension on Failure {
  String translate() {
    if (this is NetworkFailure) {
      return 'No Internet, try again';
    } else if (this is TimeoutFailure) {
      return 'Timeout, try again';
    } else if (this is BadRequestFailure) {
      return 'Bad Request';
    } else {
      return 'server error';
    }
  }
}

// Handling Socket Message
List<dynamic> exSocketMessage<T>(String message) {
  var listReturn = <dynamic>[];
  Map<String, dynamic> data = jsonDecode(message);
  if (data['title'] == 'paymentSuccess-') {
    listReturn.add(true);
    return listReturn;
  } else if (data['title'] == 'title' || data['title'] == 'test') {
    // only testing socket
    listReturn.add(true);
  } else {
    listReturn.add(false);
  }
  return listReturn;
}
