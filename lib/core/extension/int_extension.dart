part of '_extension.dart';

extension IntegerExtension on int {
  bool get isSuccessOrCreated => this == StatusCodeConst.success || this == StatusCodeConst.created;
  bool get isBadRequest => this == StatusCodeConst.badRequest;
  bool get isNotFound => this == StatusCodeConst.notFound;
  bool get isZero => this == 0;

  ///translate [statusCode]
  B translate<B>({B Function() ifSuccess, B Function() ifElse}) {
    if (isSuccessOrCreated) {
      return ifSuccess();
    } else {
      return ifElse();
    }
  }

  ///extension untuk [statusCode]
  Failure toFailure(String message) {
    //todo add more failure
    if (isBadRequest) {
      return BadRequestFailure();
    } else if (isNotFound) {
      return NotFoundFailure();
    } else {
      return GeneralFailure(message);
    }
  }
}
