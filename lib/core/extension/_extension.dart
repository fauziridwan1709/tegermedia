import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:tegarmedia/aw/models/_models.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/features/payment/data/models/payment/nicepayment_model.dart';
import 'package:tegarmedia/features/payment/presentation/pages/_pages.dart';
import 'package:tegarmedia/features/payment/presentation/pages/webview_payment_page.dart';
import 'package:tegarmedia/features/ppdb/presentation/pages/_pages.dart';

part '../../utils/v_nav.dart';
part 'bool_extension.dart';
part 'context_extension.dart';
part 'date_time_extension.dart';
part 'device_screen_type_extension.dart';
part 'failure_extension.dart';
part 'int_extension.dart';
part 'parsed_extension.dart';
part 'response_extension.dart';
part 'string_extension.dart';
