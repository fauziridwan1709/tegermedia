part of '_extension.dart';

class Parsed<T> {
  int statusCode;
  String message;
  T data;

  Parsed.fromJson(Map<String, dynamic> json, int statusCode, T type) {
    this.statusCode = statusCode;
    message =
        ((json['payload'] is String ? json['payload'] : null) ?? json['message']) ?? json['status'];
    data = type;
  }
}

extension ResponseExtension<T extends Models> on Response {
  // ignore: avoid_shadowing_type_parameters
  Parsed<T> parse<T>(T t) {
    return Parsed.fromJson(json.decode(body) as Map<String, dynamic>, statusCode, t);
  }

  Map<String, dynamic> get bodyAsMap => json.decode(body) as Map<String, dynamic>;

  Map<String, dynamic> get dataBodyAsMap => (json.decode(body) as Map<String, dynamic>)['data'];
  Map<String, dynamic> get dataPayloadAsMap =>
      (bodyMap)['payload'] is String || bodyMap['payload'] == null
          ? <String, dynamic>{}
          : (json.decode(body) as Map<String, dynamic>)['payload'];

  Map<String, dynamic> get bodyMap => (jsonDecode(body) as Map<String, dynamic>);
}
