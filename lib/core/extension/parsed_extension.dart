part of '_extension.dart';

extension ParsedExtension<T> on Parsed<T> {
  Failure toFailure() {
    return statusCode.toFailure(message);
  }
}
