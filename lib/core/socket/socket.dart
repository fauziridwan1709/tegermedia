import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/models/interaction/chat.dart';
import 'package:tegarmedia/models/interaction/message.dart';
import 'package:tegarmedia/models/interaction/participants.dart';
import 'package:tegarmedia/utils/v_utils.dart';
import 'package:websocket_manager/websocket_manager.dart';

import '../_core.dart';

class SocketServices {
  SocketServices._privateConstructor();
  DataMessage prevMsg;
  static final SocketServices _instance = SocketServices._privateConstructor();
  static SocketServices get instance {
    return _instance;
  }

  WebsocketManager socket;
  int count = 0;
  Future connectSocket({@required ValueChanged onMsgReceived}) async {
    print(socket_url_dev.toString());

    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('auth_siswamedia_api');
    var headers = {'Authorization': 'Bearer $token'};
    print('header ' + headers.toString());
    socket = WebsocketManager(socket_url_dev, headers);

    vUtils.setLog('socket connect');
    // Connect to server
    await socket.connect();

    // Connect Close
    socket.onClose((dynamic message) {
      print('socket close');
    });
    // Listen to server messages
    socket.onMessage((dynamic message) {
      print('socket msg: $message');
      Map<String, dynamic> body = jsonDecode(message);
      print('cek ' + body.toString());
      String id;
      DataMessage dataChats;
      if (body['title'].contains('addChat')) {
        dataChats = DataMessage<DetailChats>.fromJson(body, (dynamic datae) {
          return DetailChats.fromJson(datae);
        });
      } else {
        dataChats = DataMessage<DataParticipants>.fromJson(body, (dynamic datae) {
          return DataParticipants.fromJson(datae);
        });
      }

      if (count == 0) {
        count++;
        if (prevMsg != null) {
          if (prevMsg.title != dataChats.title) {
            prevMsg = dataChats;
            onMsgReceived(dataChats);
          } else {
            count++;
          }
        } else {
          prevMsg = dataChats;
          onMsgReceived(dataChats);
        }
      } else {
        if (prevMsg.title != dataChats.title) {
          count = 0;
          prevMsg = dataChats;
          onMsgReceived(dataChats);
        } else {
          count++;
        }
      }
    });
  }

  void close() {
    try {
      socket.close();
    } catch (e) {
      Utils.log(e, info: 'closing socket');
    }
  }
}
