import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:tegarmedia/core/error/_error.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/data/_static_data.dart';

part 'api_call.dart';
part 'http.dart';

var client = Client();
