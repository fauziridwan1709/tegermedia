part of '_client.dart';

Future<Decide<Failure, T>> apiCall<T>(Future<T> t) async {
  try {
    var futureCall = await t;
    return Right(futureCall);

    ///no internet connection
  } on SocketException {
    return Left(NetworkFailure());

    ///request timeout
  } on TimeoutException {
    return Left(TimeoutFailure());

    ///wrong parse pelajari lagi json nya
  } on FormatException catch (e) {
    Logger().d(e);
    return Left(ParseFailure());

    ///internal server error or more general
  } catch (e) {
    Logger().d(e);
    return Left(GeneralFailure('' + e.toString()));
  }
}
