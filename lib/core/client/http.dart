part of '_client.dart';

Future<Response> getIt(String url, {Map<String, String> headers, Client client}) async {
  var getHeaders = headers ?? await Pref.getSiswamediaHeader();
  var _getClient = client ?? Client();
  Logger().d(url);
  Logger().d(getHeaders);
  var resp = await _getClient.get(url, headers: getHeaders).timeout(GLOBAL_TIMEOUT);
  Logger().d(jsonDecode(resp.body));
  return resp;
}

Future<Response> postIt(String url, Map<String, dynamic> reqBody,
    {Map<String, String> headers, Client client}) async {
  var getHeaders = headers ?? await Pref.getSiswamediaHeader();
  var _getClient = client ?? Client();
  Logger().d(url);
  Logger().d(getHeaders);
  Logger().d(reqBody);
  var resp = await _getClient
      .post(url, headers: getHeaders, body: json.encode(reqBody))
      .timeout(GLOBAL_TIMEOUT);
  Logger().d(jsonDecode(resp.body));
  return resp;
}

Future<Response> putIt(String url, Map<String, dynamic> reqBody,
    {bool isIdentity = false, Client client}) async {
  var getHeaders = isIdentity ? Pref.getIdentityHeader() : Pref.getSiswamediaHeader();
  var _getClient = client ?? Client();
  Logger().d(url);
  Logger().d(getHeaders);
  var resp = await _getClient
      .put(url, headers: await getHeaders, body: json.encode(reqBody))
      .timeout(GLOBAL_TIMEOUT);
  Logger().d(jsonDecode(resp.body));
  return resp;
}

Future<Response> deleteIt(String url) async {
  Logger().d(url);
  var resp =
      await client.delete(url, headers: await Pref.getSiswamediaHeader()).timeout(GLOBAL_TIMEOUT);
  Logger().d(jsonDecode(resp.body));
  return resp;
}
