part of '_core.dart';

class FontTheme {
  static TextStyle rubik10w500Black = GoogleFonts.rubik(
    color: SiswamediaTheme.black,
    fontSize: 10,
    fontWeight: FontWeight.normal,
  );

  static TextStyle openSans10w500Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 10,
    fontWeight: FontWeight.normal,
  );
  static TextStyle openSans12w500Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 12,
    fontWeight: FontWeight.normal,
  );
  static TextStyle openSans14w500Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 14,
    fontWeight: FontWeight.normal,
  );

  static TextStyle openSans14w600Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );

  static TextStyle openSans16w500Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 16,
    fontWeight: FontWeight.normal,
  );

  static TextStyle openSans16w600Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );

  static TextStyle openSans18w500Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 18,
    fontWeight: FontWeight.normal,
  );

  static TextStyle openSans20w500Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 20,
    fontWeight: FontWeight.normal,
  );

  static TextStyle openSans12w700Black = GoogleFonts.openSans(
    color: SiswamediaTheme.black,
    fontSize: 12,
    fontWeight: FontWeight.bold,
  );

  static TextStyle openSans10w500White = GoogleFonts.openSans(
    color: SiswamediaTheme.white,
    fontSize: 10,
    fontWeight: FontWeight.normal,
  );
  static TextStyle openSans12w500White = GoogleFonts.openSans(
    color: SiswamediaTheme.white,
    fontSize: 12,
    fontWeight: FontWeight.normal,
  );
  static TextStyle openSans14w500White = GoogleFonts.openSans(
    color: SiswamediaTheme.white,
    fontSize: 14,
    fontWeight: FontWeight.normal,
  );

  static TextStyle openSans10w500Green = GoogleFonts.openSans(
    color: SiswamediaTheme.green,
    fontSize: 10,
    fontWeight: FontWeight.normal,
  );

  static TextStyle openSans12w500Green = GoogleFonts.openSans(
    color: SiswamediaTheme.green,
    fontSize: 12,
    fontWeight: FontWeight.normal,
  );

  ///Rubik
  static TextStyle rubik12w400black4 = GoogleFonts.rubik(
    color: SiswamediaTheme.rubikBlack4,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  static TextStyle rubik12w500black1 = GoogleFonts.rubik(
    color: SiswamediaTheme.rubikBlack1,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  );

  static TextStyle rubik16w500black1 = GoogleFonts.rubik(
    color: SiswamediaTheme.rubikBlack1,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  );

  static TextStyle rubik12w400blue1 = GoogleFonts.rubik(
    color: SiswamediaTheme.rubikBlue1,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  static TextStyle rubik12w500green = GoogleFonts.rubik(
    color: SiswamediaTheme.green,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  );
}
