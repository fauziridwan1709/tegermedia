part of '../_validators.dart';

class SchoolNameValidator {
  final _schoolNameController = BehaviorSubject<String>();
  final _schoolHeadMasterController = BehaviorSubject<String>();
  final _phoneNumberController = BehaviorSubject<String>();

  Stream<String> get validateThisSchoolName =>
      _schoolNameController.stream.transform(validateSchoolName);

  Stream<String> get validateThisHeadMasterName =>
      _schoolHeadMasterController.stream.transform(validateSchoolName);

  Stream<String> get validateThisPhoneNumber =>
      _schoolHeadMasterController.stream.transform(validatePhoneNumber);

  void onSchoolNameChanged(String s) {
    _schoolNameController.sink.add(s);
  }

  void onHeadMasterNameChanged(String s) {
    _schoolHeadMasterController.sink.add(s);
  }

  void onPhoneNumberChanged(String s) {
    _phoneNumberController.sink.add(s);
  }

  final StreamTransformer<String, String> validateSchoolName =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String inputString, EventSink<String> sink) async {
    var headers = await Pref.getHeader();
    var resp =
        await client.get('$baseUrl/$version/school?school_name=$inputString', headers: headers);
    var boolean = json.decode(resp.body) as Map<String, dynamic>;
    bool value = boolean['data'];
    try {
      if (inputString.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else if (inputString.length < 5) {
        sink.addError('Harus lebih dari 5 karakter');
      } else if (value) {
        sink.addError('Nama telah digunakan');
      } else {
        sink.add(inputString);
      }
    } catch (e) {
      sink.addError('input wrong');
    }
  });

  final StreamTransformer<String, String> validateHeadMasterName =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String inputString, EventSink<String> sink) async {
    try {
      if (inputString.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else if (inputString.length < 3) {
        sink.addError('Harus lebih dari 2 karakter');
      } else {
        sink.add(inputString);
      }
    } catch (e) {
      sink.addError('Wrong input');
    }
  });

  final StreamTransformer<String, String> validatePhoneNumber =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String inputString, EventSink<String> sink) async {
    try {
      if (inputString.isEmpty) {
        sink.addError('Tidak boleh kosong');
      } else if (inputString.length < 9) {
        sink.addError('Harus lebih dari 8 karakter');
      } else {
        sink.add(inputString);
      }
    } catch (e) {
      sink.addError('Wrong input');
    }
  });

  String isEmpty(String value) {
    if (value.isEmpty) {
      return 'Tidak boleh kosong';
    }
    return '';
  }

  void dispose() {
    _schoolNameController?.close();
    _schoolHeadMasterController?.close();
    _phoneNumberController?.close();
    _phoneNumberController?.close();
  }
}
