import 'dart:convert';
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/subjects.dart';
import 'package:tegarmedia/app.dart';
import 'package:tegarmedia/features/payment/presentation/pages/_pages.dart';

// Referensi
// https://pub.dev/packages/flutter_local_notifications

class NotificationPlugin {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final BehaviorSubject<ReceivedNotification> didRecievedLocalNotificationSubject =
      BehaviorSubject<ReceivedNotification>();
  InitializationSettings initializedSettings;

  NotificationPlugin._() {
    init();
  }

  void init() {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    if (Platform.isIOS) {
      _requestIOSPermission();
    }
    print('init notification');
    initializePlatformSpesifics();
  }

  Future<void> initializePlatformSpesifics() async {
    print('platform notification');
    try {
      var initializedSettingAndroid = AndroidInitializationSettings('app_icon');
      var initializedSettingIOS = IOSInitializationSettings(
          requestAlertPermission: true,
          requestBadgePermission: true,
          requestSoundPermission: true,
          onDidReceiveLocalNotification: (id, title, body, payload) async {
            //
            var recievedNotification =
                ReceivedNotification(id: id, title: title, body: body, payload: payload);
            didRecievedLocalNotificationSubject.add(recievedNotification);
            //
          });
      initializedSettings =
          InitializationSettings(android: initializedSettingAndroid, iOS: initializedSettingIOS);
      await flutterLocalNotificationsPlugin.initialize(initializedSettings,
          onSelectNotification: (String payload) async {
        if (payload != null) {
          debugPrint('notification payload: $payload');
          onNotificationClick(payload);
        }
      });
    } catch (e) {
      print('error notification');
      print(e);
    }
  }

  void onNotificationClick(dynamic payload) async {
    if (payload != null) {
      Map<String, dynamic> map = json.decode(payload);
      if (map['type'] == 'MARKET_NOTIFICATION') {
        // final TransactionRemoteDataSource remoteDataSource = TransactionRemoteDataSourceImpl();
        // var resp = await remoteDataSource.getTransactionDetail(map['data']);
        // var model = resp.data;
        await locator<NavigationService>().navigateTo(PaymentHistoryPage());
      }
      if (map['type'] == 'PPOB_NOTIFICATION') {
        await locator<NavigationService>().navigateTo(PaymentHistoryPage());
        // final TransactionRemoteDataSource remoteDataSource = TransactionRemoteDataSourceImpl();
        // var resp = await remoteDataSource.getDetailTransactionPulses(map['data']);
        // TransactionPulses model = resp.data.data;
        //     await locator<NavigationService>().navigateTo(PaymentHistoryDetailPage(
        //       model: Transaction(
        //           iD: model.id,
        //           paymentStatus: model.ayopopStatus,
        //           bank: model.payment.mitraCode,
        //           expiredAt: model.payment.expiredAt,
        //           transactionDate:
        //           model.payment.transactionDate??
        // model.createdAt,
        //           paymentProvider:
        //           model.payment.paymentProvider,
        //           adminFee: model.payment.adminFee,
        //           totalAmount: model.total),
        //     ));
      }
      // if (_schedule.callEnd.toLocal().isBefore(DateTime.now())) {
      //   locator<NavigationService>().openExpiredCall();
      // } else {
      //   locator<NavigationService>().openCallPage(
      //     _schedule,
      //     _user,
      //   );
      // }
    }
    // print(payload);
  }

  Future<void> showNotification(
      int id, String title, String body, String payload, String types) async {
    print('showing notification');
    var androidChannelSpesifics = AndroidNotificationDetails(
        'CHANNEL_ID', 'CHANNEL_NAME', 'CHANNEL_DESCRIPTION',
        importance: Importance.max,
        priority: Priority.max,
        visibility: NotificationVisibility.public,
        playSound: true,
        autoCancel: false,
        timeoutAfter: 5000,
        icon: 'app_icon',
        enableVibration: true,
        styleInformation: DefaultStyleInformation(true, true));

    var iosChannelSpesifics = IOSNotificationDetails();
    var platformChannelSpesifics =
        NotificationDetails(android: androidChannelSpesifics, iOS: iosChannelSpesifics);
    var payloads = jsonEncode(<String, String>{'type': payload, 'data': types});
    await flutterLocalNotificationsPlugin.show(id, title, body, platformChannelSpesifics,
        payload: payloads);
  }

  void setOnNotificationClick(Function onNotificationClick) {
    flutterLocalNotificationsPlugin.initialize(initializedSettings,
        onSelectNotification: (String payloadAndTypes) async {
      var payloadAndTypesSlice = payloadAndTypes.split(':');
      onNotificationClick(payloadAndTypesSlice[0], payloadAndTypesSlice[1]);
    });
  }

  void _requestIOSPermission() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<IOSFlutterLocalNotificationsPlugin>()
        .requestPermissions(alert: false, badge: false, sound: true);
  }

  void setListenerForLowerVersion(Function onLowerVersionNotification) {
    didRecievedLocalNotificationSubject.listen((recievedNotification) {
      onLowerVersionNotification(recievedNotification);
    });
  }
}

NotificationPlugin notificationPlugin = NotificationPlugin._();

class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;
  ReceivedNotification(
      {@required this.id, @required this.title, @required this.body, @required this.payload});
}
