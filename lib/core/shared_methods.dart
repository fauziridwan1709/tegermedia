part of '_core.dart';

String someCapitalizedString(String data) => data.toLowerCase().capitalize;

String formattedDate(String date) {
  var dateValue = DateTime.parse(date).toLocal();
  var formattedDate = DateFormat('dd MMM yyyy - HH:mm').format(dateValue);
  return formattedDate;
}

List<String> listTahunAjaran() {
  var listYear = <String>[];
  var year = DateTime.now().year;
  listYear = List.generate(10, (index) => '${year + index - 1}/${year + index}');
  return listYear;
}

int formatNumber(String nilai) {
  return int.parse(nilai);
}

String generateJenjang(String grade) {
  var number = int.parse(grade);
  if (number > 9) {
    return 'sma';
  } else if (number > 6) {
    return 'smp';
  } else if (number > 0) {
    return 'sd';
  } else {
    return 'tk';
  }
}

String formatNilai(double score) {
  if (score.isNegative) {
    return '-';
  } else if (score.roundToDouble() == score) {
    return '${score.round()}';
  } else {
    return '${score.toStringAsFixed(2)}';
  }
  // ${detail.score.isNegative ? '-' : detail.score.roundToDouble() == detail.score ? detail.score.round() : detail.score.toStringAsFixed(2)}
}

bool isDifferenceLessThanThirtyMin(String time) {
  return DateTime.now().difference(DateTime.parse(time)) < Duration(minutes: 30);
}

bool isDifferenceLessThanFiveMin(String time) {
  return DateTime.now().difference(DateTime.parse(time)) < Duration(minutes: 5);
}

bool isDifferenceLessThanMin(String time) {
  return DateTime.now().difference(DateTime.parse(time)) < Duration(minutes: 1);
}

bool isDifferenceLessThanFifteen(String time) {
  return DateTime.now().difference(DateTime.parse(time)) < Duration(seconds: 15);
}
