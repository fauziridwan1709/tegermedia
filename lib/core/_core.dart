import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

export 'dart:math';

export 'package:tegarmedia/core/extension/_extension.dart';

export '_core_export.dart';

part 'api_version.dart';
part 'custom_icons.dart';
part 'font_theme.dart';
part 'google_sign_in.dart';
part 'screen/screen_size.dart';
part 'shared_methods.dart';
part 'shared_value.dart';
part 'siswamedia_error.dart';
part 'siswamedia_theme.dart';
