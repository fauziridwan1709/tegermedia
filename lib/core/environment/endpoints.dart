part of '_environment.dart';

class Endpoints {
  static const String infix_dev = 'https://sekolah.nusatek.dev/api';
  static String baseUrl = apiUrl;
  static String baseIdentityUrl = '$baseUrlGoogle/v1/api';
  static String ppobDev = ppob_dev;
  static String marketUrl = market_dev;
  static String studyJournal = '$baseUrl/study-journal';
  static String listSpp = 'http://127.0.0.1:8080/v1/tuition';
  static String orderId = '/core/order/create';
  static String orderPayment = '/dashboard/transaction/detail';
  static String orderInquiry = '/core/order/inquiry';
  static String dataPacketPulses = '/core/product/list';
  static String paymentMethodsPPOB = '/core/payment/config';
  static String orderPaymentMethodsPPob = '$ppobDev/core/order/payment';
  static String registerInvoice = '$marketUrl/v1/payment/inquiry';
  static String getAllSppBill = '$marketUrl/v1/tuition';
  static String voucherAvailability = '$marketUrl/v1/voucher';
  static String paymentMethodsMarket = '$marketUrl/v1/payment/method';
  static String transaction = '$marketUrl/v1/transaction';
  static String schools = '$baseUrl/schools';
  static String classes = '$baseUrl/classes';
  static String courses = '$baseUrl/courses';
  static String region = '$baseIdentityUrl';
  static String admission = '$marketUrl/v1/admission';
  static String historyPulses = '$ppobDev/dashboard/transaction/list';
  static String historyPulsesDetail = '$ppobDev/dashboard/transaction/detail';
  static String coreProductList = '$ppobDev/core/product/list';
  static String coreOrderInquiry = '$ppobDev/core/order/inquiry';
  static String coreOrderCreate = '$ppobDev/core/order/create';
  static String corePaymentConfig = '$ppobDev/core/payment/config';

  ///market
  static String marketPayment = '$marketUrl/v1/payment/cancel';

  ///
  static const String confirmPayment = '$infix_dev/quiz/mobile/list_quiz';

  ///quiz
  static const String quizMobileListQuiz = '$infix_dev/quiz/mobile/list_quiz';
  static const String quizMobileLogin = '$infix_dev/quiz/mobile_login';
}
