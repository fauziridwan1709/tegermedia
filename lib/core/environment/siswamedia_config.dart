part of '_environment.dart';

enum Flavor { staging, production }

class SiswamediaConfig {
  static Flavor appFlavor;

  static Future<void> init(Flavor flavor) async {
    appFlavor = flavor;
    if (Platform.isMacOS) {
      final prefsStore = await PreferencesStore.create();
      FirebaseAuth.initialize('AIzaSyCilgOoSZ2hVuWgG-w1kjcIXaafAYmUE30', prefsStore);
      Firestore.initialize('siswamedia-new');
    } else {
      await Firebase.initializeApp();
    }

    await HiveDataBase.init();
    notificationPlugin.init();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark,
      statusBarColor: SiswamediaTheme.transparent,
    ));
  }

  static bool get isStaging => appFlavor == Flavor.staging;
}

class PreferencesStore extends TokenStore {
  static const keyToken = 'auth_token';

  static Future<PreferencesStore> create() async =>
      PreferencesStore._internal(await SharedPreferences.getInstance());

  final SharedPreferences _prefs;

  PreferencesStore._internal(this._prefs);

  @override
  Token read() => _prefs.containsKey(keyToken)
      ? Token.fromMap(json.decode(_prefs.get(keyToken) as String) as Map<String, dynamic>)
      : null;

  @override
  void write(Token token) {
    if (token == null) {
      return;
    }
    _prefs.setString(keyToken, json.encode(token.toMap()));
  }

  @override
  void delete() => _prefs.remove(keyToken);
}
