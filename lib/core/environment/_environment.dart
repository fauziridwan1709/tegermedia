import 'dart:convert';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firedart/firedart.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/hive/_hive.dart';
import 'package:tegarmedia/core/notification_plugin.dart';

part 'endpoints.dart';
part 'environment.dart';
part 'siswamedia_config.dart';
