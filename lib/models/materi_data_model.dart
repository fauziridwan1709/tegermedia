part of '_models.dart';

class MateriDetail {
  String jenjang;
  String kelas;
  String kurikulum;
  String judul;
  String pelajaran;
  String name;
  String pengguna;

  MateriDetail(
      {this.jenjang,
      this.kelas,
      this.kurikulum,
      this.judul,
      this.pelajaran,
      this.name,
      this.pengguna});
}
