part of '_models.dart';

class GeneralResultApi {
  int statusCode;
  String message;

  GeneralResultApi({this.statusCode, this.message});

  GeneralResultApi.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
  }
}
