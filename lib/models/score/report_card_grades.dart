part of '_score.dart';

class ModelReportCardGrades {
  int statusCode;
  String message;
  List<DataReportCardGrades> data;

  ModelReportCardGrades({this.statusCode, this.message, this.data});

  ModelReportCardGrades.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataReportCardGrades>[];
      json['data'].forEach((dynamic v) {
        data.add(DataReportCardGrades.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataReportCardGrades {
  String namaMatapelajaran;
  double nilaiRataRata;

  DataReportCardGrades({this.namaMatapelajaran, this.nilaiRataRata});

  DataReportCardGrades.fromJson(Map<String, dynamic> json) {
    namaMatapelajaran = json['nama_matapelajaran'];
    nilaiRataRata = json['nilai_rata_rata'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nama_matapelajaran'] = namaMatapelajaran;
    data['nilai_rata_rata'] = nilaiRataRata;
    return data;
  }
}
