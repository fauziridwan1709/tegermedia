part of '_score.dart';

class ModeleSummaryScore {
  int statusCode;
  String message;
  List<DataSummaryScore> data;

  ModeleSummaryScore({this.statusCode, this.message, this.data});

  ModeleSummaryScore.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataSummaryScore>[];
      json['data'].forEach((dynamic v) {
        data.add(DataSummaryScore.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataSummaryScore {
  int classId;
  String className;
  int courseId;
  List<CourseTeacher> courseTeacher;
  String courseName;
  double rataRataTotal;
  bool haveAssignment;
  bool haveQuiz;

  DataSummaryScore(
      {this.classId,
      this.className,
      this.courseId,
      this.courseTeacher,
      this.courseName,
      this.rataRataTotal,
      this.haveAssignment,
      this.haveQuiz});

  DataSummaryScore.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    if (json['course_teacher'] != null) {
      courseTeacher = <CourseTeacher>[];
      json['course_teacher'].forEach((dynamic v) {
        courseTeacher.add(CourseTeacher.fromJson(v as Map<String, dynamic>));
      });
    }
    courseName = json['course_name'];
    rataRataTotal = json['rata_rata_total'] / 1.0;
    haveAssignment = json['have_assignment'];
    haveQuiz = json['have_quiz'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = classId;
    data['class_name'] = className;
    data['course_id'] = courseId;
    if (courseTeacher != null) {
      data['course_teacher'] = courseTeacher.map((v) => v.toJson()).toList();
    }
    data['course_name'] = courseName;
    data['rata_rata_total'] = rataRataTotal;
    data['have_assignment'] = haveAssignment;
    data['have_quiz'] = haveQuiz;
    return data;
  }
}

class CourseTeacher {
  String teacherName;

  CourseTeacher({this.teacherName});

  CourseTeacher.fromJson(Map<String, dynamic> json) {
    teacherName = json['teacher_name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['teacher_name'] = teacherName;
    return data;
  }
}
