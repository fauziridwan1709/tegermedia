part of '_score.dart';

class ModelDetailSummaryScore {
  int statusCode;
  String message;
  DataDetailSummaryScore data;

  ModelDetailSummaryScore({this.statusCode, this.message, this.data});

  ModelDetailSummaryScore.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null
        ? DataDetailSummaryScore.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataDetailSummaryScore {
  int classId;
  String className;
  int courseId;
  String courseName;
  double rataRataTotal;
  bool haveAssignment;
  bool haveQuiz;
  List<ListAssessment> listAssessment;
  List<ListQuiz> listQuiz;

  DataDetailSummaryScore(
      {this.classId,
      this.className,
      this.courseId,
      this.courseName,
      this.rataRataTotal,
      this.haveAssignment,
      this.haveQuiz,
      this.listAssessment,
      this.listQuiz});

  DataDetailSummaryScore.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    courseName = json['course_name'];
    rataRataTotal = json['rata_rata_total'] / 1.0;
    haveAssignment = json['have_assignment'];
    haveQuiz = json['have_quiz'];
    if (json['list_assessment'] != null) {
      listAssessment = <ListAssessment>[];
      json['list_assessment'].forEach((dynamic v) {
        listAssessment.add(ListAssessment.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['list_quiz'] != null) {
      listQuiz = <ListQuiz>[];
      json['list_quiz'].forEach((dynamic v) {
        listQuiz.add(ListQuiz.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = classId;
    data['class_name'] = className;
    data['course_id'] = courseId;
    data['course_name'] = courseName;
    data['rata_rata_total'] = rataRataTotal;
    data['have_assignment'] = haveAssignment;
    data['have_quiz'] = haveQuiz;
    if (listAssessment != null) {
      data['list_assessment'] = listAssessment.map((v) => v.toJson()).toList();
    }
    if (listQuiz != null) {
      data['list_quiz'] = listQuiz.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListAssessment {
  String assessmentName;
  String startDate;
  int score;

  ListAssessment({this.assessmentName, this.startDate, this.score});

  ListAssessment.fromJson(Map<String, dynamic> json) {
    assessmentName = json['assessment_name'];
    startDate = json['start_date'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['assessment_name'] = assessmentName;
    data['start_date'] = startDate;
    data['score'] = score;
    return data;
  }
}

class ListQuiz {
  String quizName;
  String startDate;
  double score;
  bool hasBeenDone;

  ListQuiz({this.quizName, this.startDate, this.score, this.hasBeenDone});

  ListQuiz.fromJson(Map<String, dynamic> json) {
    quizName = json['quiz_name'];
    startDate = json['start_date'];
    score = json['score'] / 1.0;
    hasBeenDone = json['has_been_done'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['quiz_name'] = quizName;
    data['start_date'] = startDate;
    data['score'] = score / 1.0;
    data['has_been_done'] = hasBeenDone;
    return data;
  }
}
