part of '_models.dart';

class NotificationModel {
  int id;
  String types;
  String title;
  String body;
  String payload;

  NotificationModel({this.id, this.types, this.title, this.body, this.payload});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    types = json['types'];
    title = json['title'];
    body = json['body'];
    payload = json['payload'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['types'] = types;
    data['title'] = title;
    data['body'] = body;
    data['payload'] = payload;
    return data;
  }
}
