part of '../_models.dart';

class HomeModel {
  List<HomeItem> rows;

  HomeModel({this.rows});

  HomeModel.fromJson(Map<dynamic, dynamic> json) {
    if (json['rows'] != null) {
      rows = <HomeItem>[];
      json['rows'].forEach((dynamic v) {
        rows.add(HomeItem.fromJson(v as Map<dynamic, dynamic>));
      });
    }
  }
}

class HomeItem {
  String judul;
  String gambar;
  String tanggal;
  String penerbit;
  String status;
  String link;
  bool cached;

  HomeItem({
    this.judul,
    this.gambar,
    this.tanggal,
    this.penerbit,
    this.status,
    this.link,
    this.cached,
  });

  HomeItem.fromJson(Map<dynamic, dynamic> json) {
    judul = getJudul(json);
    gambar = (json['image'] ?? json['gambar']).toString();
    tanggal = (json['createdat'] ??
            json['publishedat'] ??
            json['tanggal'] ??
            json['publishedon'])
        .toString();
    status = json['status'].toString();
    penerbit = getPenerbit(json);
    link = (json['link'] ?? json['advertiserurl']).toString();
  }
}

String getJudul(Map<dynamic, dynamic> json) {
  if (json['judul'] == null) {
    return json['title'].toString();
  } else {
    return json['judul'].toString();
  }
}

String getPenerbit(Map<dynamic, dynamic> json) {
  if (json['penerbit'] == null) {
    if (json['createdby'] == null) {
      if (json['creator'] == null) {
        return json['advertiser'].toString();
      } else {
        return json['creator'].toString();
      }
    } else {
      return json['createdby'].toString();
    }
  } else {
    return json['penerbit'].toString();
  }
}
