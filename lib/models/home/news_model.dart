part of '../_models.dart';

class NewsModel {
  List<Rows> rows;

  NewsModel({this.rows});

  NewsModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows =  <Rows>[];
      json['rows'].forEach((Map<String, dynamic> v) {
        rows.add( Rows.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data =  <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rows {
  String judul;
  String gambar;
  String tanggal;
  String penerbit;
  String status;
  String link;

  Rows(
      {this.judul,
      this.gambar,
      this.tanggal,
      this.penerbit,
      this.status,
      this.link});

  Rows.fromJson(Map<String, dynamic> json) {
    judul = json['judul'];
    gambar = json['gambar'];
    tanggal = json['tanggal'];
    penerbit = json['penerbit'];
    status = json['status'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final data =  <String, dynamic>{};
    data['judul'] = judul;
    data['gambar'] = gambar;
    data['tanggal'] = tanggal;
    data['penerbit'] = penerbit;
    data['status'] = status;
    data['link'] = link;
    return data;
  }
}
