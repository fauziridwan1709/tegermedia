class CourseJourney {
  dynamic statusCode;
  dynamic count;
  List<CourseJourneyData> data;

  CourseJourney({this.statusCode, this.data});

  CourseJourney.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    count = json['total_count'];
    if (json['data']['list_data'] != null) {
      data = <CourseJourneyData>[];
      json['data']['list_data'].forEach((dynamic v) {
        data.add(CourseJourneyData.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CourseJourneyData {
  String name;
  String startTime;
  int teacherUserId;
  String guide;
  List<Comment> comment;
  List<AttachmentResponse> attachment;
  String videoUrl;
  int journalId;

  CourseJourneyData(
      {this.name,
      this.startTime,
      this.teacherUserId,
      this.guide,
      this.comment,
      this.attachment,
      this.videoUrl,
      this.journalId});

  CourseJourneyData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    startTime = json['start_time'];
    teacherUserId = json['teacher_user_id'];
    guide = json['guide'];
    if (json['comment'] != null) {
      comment = <Comment>[];
      json['comment'].forEach((dynamic v) {
        comment.add(Comment.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['attachment'] != null) {
      attachment = <AttachmentResponse>[];
      json['attachment'].forEach((dynamic v) {
        attachment.add(AttachmentResponse.fromJson(v as Map<String, dynamic>));
      });
    }
    videoUrl = json['video_url'];
    journalId = json['journal_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['start_time'] = startTime;
    data['teacher_user_id'] = teacherUserId;
    data['guide'] = guide;
    if (comment != null) {
      data['comment'] = comment.map((v) => v.toJson()).toList();
    }
    if (attachment != null) {
      data['attachment'] = attachment.map((v) => v.toJson()).toList();
    }
    data['video_url'] = videoUrl;
    data['journal_id'] = journalId;
    return data;
  }
}

class Comment {
  int commentId;
  String comment;
  String sentDate;
  int senderUserId;
  String senderName;

  Comment(
      {this.commentId,
      this.comment,
      this.sentDate,
      this.senderUserId,
      this.senderName});

  Comment.fromJson(Map<String, dynamic> json) {
    commentId = json['comment_id'];
    comment = json['comment'];
    sentDate = json['sent_date'];
    senderUserId = json['sender_user_id'];
    senderName = json['sender_name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['comment_id'] = commentId;
    data['comment'] = comment;
    data['sent_date'] = sentDate;
    data['sender_user_id'] = senderUserId;
    data['sender_name'] = senderName;
    return data;
  }
}

class AttachmentResponse {
  String name;
  String link;

  AttachmentResponse({this.name, this.link});

  AttachmentResponse.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['link'] = link;
    return data;
  }
}
