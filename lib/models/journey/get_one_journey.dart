import 'courser_journey.dart';

class GetCourseJourney {
  int statusCode;
  CourseJourneyData data;

  GetCourseJourney({this.statusCode, this.data});

  GetCourseJourney.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = CourseJourneyData.fromJson(json['data']);
  }
}
