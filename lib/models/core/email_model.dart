part of '_core.dart';

class Email {
  String email;
  String type;

  Email({this.email, this.type});

  Email.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    type = json['type'];
  }
}
