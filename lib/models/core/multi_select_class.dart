part of '_core.dart';

//ms mean multi select
class ModelMultiSelectClass {
  int _statusCode;
  List<DataMultiSelectClass> _data;

  ModelMultiSelectClass({int statusCode, List<DataMultiSelectClass> data}) {
    _statusCode = statusCode;
    _data = data;
  }

  int get statusCode => _statusCode;
  set statusCode(int statusCode) => _statusCode = statusCode;
  List<DataMultiSelectClass> get data => _data;
  set data(List<DataMultiSelectClass> data) => _data = data;

  ModelMultiSelectClass.fromJson(Map<String, dynamic> json) {
    _statusCode = json['status_code'];
    if (json['data'] != null) {
      _data = <DataMultiSelectClass>[];
      json['data'].forEach((dynamic v) {
        _data.add(DataMultiSelectClass.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = _statusCode;
    if (_data != null) {
      data['data'] = _data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataMultiSelectClass {
  int _classId;
  int _courseId;
  String _courseName;
  String _className;
  String _year;
  String _semester;

  DataMultiSelectClass(
      {int classId,
      int courseId,
      String courseName,
      String className,
      String year,
      String semester}) {
    _classId = classId;
    _courseId = courseId;
    _courseName = courseName;
    _className = className;
    _year = year;
    _semester = semester;
  }

  int get classId => _classId;
  set classId(int classId) => _classId = classId;
  int get courseId => _courseId;
  set courseId(int courseId) => _courseId = courseId;
  String get courseName => _courseName;
  set courseName(String courseName) => _courseName = courseName;
  String get className => _className;
  set className(String className) => _className = className;
  String get year => _year;
  set year(String year) => _year = year;
  String get semester => _semester;
  set semester(String semester) => _semester = semester;

  DataMultiSelectClass.fromJson(Map<String, dynamic> json) {
    _classId = json['class_id'];
    _courseId = json['course_id'];
    _courseName = json['course_name'];
    _className = json['class_name'];
    _year = json['year'];
    _semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = _classId;
    data['course_id'] = _courseId;
    data['course_name'] = _courseName;
    data['class_name'] = _className;
    data['year'] = _year;
    data['semester'] = _semester;
    return data;
  }
}
