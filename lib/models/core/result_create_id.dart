part of '_core.dart';

class ModelResultCreateId {
  int _statusCode;
  String _message;
  ResultId _data;

  ModelResultCreateId({int statusCode, String message, ResultId data}) {
    _statusCode = statusCode;
    _message = message;
    _data = data;
  }

  int get statusCode => _statusCode;
  set statusCode(int statusCode) => _statusCode = statusCode;
  String get message => _message;
  set message(String message) => _message = message;
  ResultId get data => _data;
  set data(ResultId data) => _data = data;

  ModelResultCreateId.fromJson(Map<String, dynamic> json) {
    _statusCode = json['status_code'];
    _message = json['message'];
    _data = json['data'] != null ? ResultId.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = _statusCode;
    data['message'] = _message;
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    return data;
  }
}

class ResultId {
  int _id;

  ResultId({int id}) {
    _id = id;
  }

  int get id => _id;
  set id(int id) => _id = id;

  ResultId.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = _id;
    return data;
  }
}
