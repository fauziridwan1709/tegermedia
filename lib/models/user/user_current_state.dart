part of '_user.dart';

///current state of chosen school and class
class UserCurrentState {
  int schoolId = -1;
  List<String> schoolRole;
  String schoolName;
  String type;
  bool isAdmin;
  int classId;
  String className;
  String classRole;

  UserCurrentState({
    this.schoolId,
    this.schoolRole,
    this.schoolName,
    this.type,
    this.isAdmin,
    this.classId,
    this.className,
    this.classRole,
  });

  UserCurrentState.fromJson(Map<String, dynamic> json) {
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    type = json['type'];
    isAdmin = json['is_admin'];
    classId = json['class_id'];
    className = json['class_name'];
    classRole = json['class_role'];
    schoolRole = json['school_role'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['type'] = type;
    data['is_admin'] = isAdmin;
    data['class_id'] = classId;
    data['class_name'] = className;
    data['class_role'] = classRole;
    data['school_role'] = schoolRole;
    return data;
  }
}
