part of '_models.dart';

class ModelUploadFile {
  int statusCode;
  String message;
  DataFile data;

  ModelUploadFile({this.statusCode, this.message, this.data});

  ModelUploadFile.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? DataFile.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataFile {
  String fileName;
  String file;

  DataFile({this.fileName, this.file});

  DataFile.fromJson(Map<String, dynamic> json) {
    fileName = json['file_name'];
    file = json['file'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['file_name'] = fileName;
    data['file'] = file;
    return data;
  }
}
