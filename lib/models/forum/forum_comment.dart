part of '_forum.dart';

class ForumCommentModel {
  int parentCommentId;
  String commentDate;
  String comments;
  List<ForumCreateCommentsAttach> forumCommentsAttachments;

  ForumCommentModel(
      {this.parentCommentId,
      this.commentDate,
      this.comments,
      this.forumCommentsAttachments});

  ForumCommentModel.fromJson(Map<String, dynamic> json) {
    parentCommentId = json['parent_comment_id'];
    commentDate = json['comment_date'];
    comments = json['comments'];
    if (json['forum_comments_attachments'] != null) {
      forumCommentsAttachments = <ForumCreateCommentsAttach>[];
      json['forum_comments_attachments'].forEach((dynamic v) {
        forumCommentsAttachments
            .add(ForumCreateCommentsAttach.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['parent_comment_id'] = parentCommentId;
    data['comment_date'] = commentDate;
    data['comments'] = comments;
    if (forumCommentsAttachments != null) {
      data['forum_comments_attachments'] =
          forumCommentsAttachments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ForumCreateCommentsAttach {
  String fileHashId;
  String fileLink;
  String fileName;
  String fileType;

  ForumCreateCommentsAttach(
      {this.fileHashId, this.fileLink, this.fileName, this.fileType});

  ForumCreateCommentsAttach.fromJson(Map<String, dynamic> json) {
    fileHashId = json['file_hash_id'];
    fileLink = json['file_link'];
    fileName = json['file_name'];
    fileType = json['file_type'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['file_hash_id'] = fileHashId;
    data['file_link'] = fileLink;
    data['file_name'] = fileName;
    data['file_type'] = fileType;
    return data;
  }
}
