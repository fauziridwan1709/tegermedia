part of '_forum.dart';

class ForumDetailId {
  int statusCode;
  String message;
  ForumDetailItem data;

  ForumDetailId({this.statusCode, this.message, this.data});

  ForumDetailId.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? ForumDetailItem.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ForumDetailItem {
  int iD;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String judulForum;
  String deskripsi;
  int creatorUserId;
  int courseId;
  int classId;
  int schoolId;
  String postingDate;
  String fullName;
  String image;
  bool deleteAccess;
  List<ForumAttachments> forumAttachments;
  List<Comments> comments;

  ForumDetailItem(
      {this.iD,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.judulForum,
      this.deskripsi,
      this.creatorUserId,
      this.courseId,
      this.classId,
      this.schoolId,
      this.postingDate,
      this.fullName,
      this.image,
      this.deleteAccess,
      this.forumAttachments,
      this.comments});

  ForumDetailItem.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    deletedAt = json['DeletedAt'];
    judulForum = json['judul_forum'];
    deskripsi = json['deskripsi'];
    creatorUserId = json['creator_user_id'];
    courseId = json['course_id'];
    classId = json['class_id'];
    schoolId = json['school_id'];
    postingDate = json['posting_date'];
    fullName = json['full_name'];
    image = json['image'];
    deleteAccess = json['delete_access'];
    if (json['forum_attachments'] != null) {
      forumAttachments = <ForumAttachments>[];
      json['forum_attachments'].forEach((dynamic v) {
        forumAttachments
            .add(ForumAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['comments'] != null) {
      comments = <Comments>[];
      json['comments'].forEach((dynamic v) {
        comments.add(Comments.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    data['DeletedAt'] = deletedAt;
    data['judul_forum'] = judulForum;
    data['deskripsi'] = deskripsi;
    data['creator_user_id'] = creatorUserId;
    data['course_id'] = courseId;
    data['class_id'] = classId;
    data['school_id'] = schoolId;
    data['posting_date'] = postingDate;
    data['full_name'] = fullName;
    data['image'] = image;
    data['delete_access'] = deleteAccess;
    if (forumAttachments != null) {
      data['forum_attachments'] =
          forumAttachments.map((v) => v.toJson()).toList();
    }
    if (comments != null) {
      data['comments'] = comments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ForumAttachments {
  int iD;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String fileHashId;
  String fileLink;
  String fileName;
  Null file;
  String fileType;
  int forumId;

  ForumAttachments(
      {this.iD,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.fileHashId,
      this.fileLink,
      this.fileName,
      this.file,
      this.fileType,
      this.forumId});

  ForumAttachments.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    deletedAt = json['DeletedAt'];
    fileHashId = json['file_hash_id'];
    fileLink = json['file_link'];
    fileName = json['file_name'];
    file = json['File'];
    fileType = json['file_type'];
    forumId = json['forum_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    data['DeletedAt'] = deletedAt;
    data['file_hash_id'] = fileHashId;
    data['file_link'] = fileLink;
    data['file_name'] = fileName;
    data['File'] = file;
    data['file_type'] = fileType;
    data['forum_id'] = forumId;
    return data;
  }
}

class Comments {
  int iD;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String comments;
  String fullName;
  String image;
  int userId;
  int forumId;
  int parentCommentId;
  String commentDate;
  List<ForumCommentAttachment> forumCommentAttachment;

  Comments(
      {this.iD,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.fullName,
      this.image,
      this.comments,
      this.userId,
      this.forumId,
      this.parentCommentId,
      this.commentDate,
      this.forumCommentAttachment});

  Comments.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    deletedAt = json['DeletedAt'];
    fullName = json['full_name'];
    image = json['image'];
    comments = json['comments'];
    userId = json['user_id'];
    forumId = json['forum_id'];
    parentCommentId = json['parent_comment_id'];
    commentDate = json['comment_date'];
    if (json['forum_comment_attachment'] != null) {
      forumCommentAttachment = <ForumCommentAttachment>[];
      json['forum_comment_attachment'].forEach((dynamic v) {
        forumCommentAttachment
            .add(ForumCommentAttachment.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    data['DeletedAt'] = deletedAt;
    data['comments'] = comments;
    data['image'] = image;
    data['full_name'] = fullName;
    data['user_id'] = userId;
    data['forum_id'] = forumId;
    data['parent_comment_id'] = parentCommentId;
    data['comment_date'] = commentDate;
    if (forumCommentAttachment != null) {
      data['forum_comment_attachment'] =
          forumCommentAttachment.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ForumCommentAttachment {
  int iD;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String fileHashId;
  String fileLink;
  String fileName;
  Null file;
  String fileType;
  int forumCommentId;

  ForumCommentAttachment(
      {this.iD,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.fileHashId,
      this.fileLink,
      this.fileName,
      this.file,
      this.fileType,
      this.forumCommentId});

  ForumCommentAttachment.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    deletedAt = json['DeletedAt'];
    fileHashId = json['file_hash_id'];
    fileLink = json['file_link'];
    fileName = json['file_name'];
    file = json['File'];
    fileType = json['file_type'];
    forumCommentId = json['forum_comment_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    data['DeletedAt'] = deletedAt;
    data['file_hash_id'] = fileHashId;
    data['file_link'] = fileLink;
    data['file_name'] = fileName;
    data['File'] = file;
    data['file_type'] = fileType;
    data['forum_comment_id'] = forumCommentId;
    return data;
  }
}
