part of '_forum.dart';

class AllForum {
  int statusCode;
  String message;
  List<ForumItem> data;

  AllForum({this.statusCode, this.message, this.data});

  AllForum.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <ForumItem>[];
      json['data'].forEach((dynamic v) {
        data.add(ForumItem.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ForumItem {
  int iD;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String judulForum;
  String deskripsi;
  int creatorUserId;
  int courseId;
  int classId;
  int schoolId;
  String postingDate;
  String fullName;
  String image;
  bool deleteAccess;
  int amountComments;

  ForumItem(
      {this.iD,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.judulForum,
      this.deskripsi,
      this.creatorUserId,
      this.courseId,
      this.classId,
      this.schoolId,
      this.postingDate,
      this.fullName,
      this.image,
      this.deleteAccess,
      this.amountComments});

  ForumItem.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    deletedAt = json['DeletedAt'];
    judulForum = json['judul_forum'];
    deskripsi = json['deskripsi'];
    creatorUserId = json['creator_user_id'];
    courseId = json['course_id'];
    classId = json['class_id'];
    schoolId = json['school_id'];
    postingDate = json['posting_date'];
    fullName = json['full_name'];
    image = json['image'];
    deleteAccess = json['delete_access'];
    amountComments = json['amount_comments'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    data['DeletedAt'] = deletedAt;
    data['judul_forum'] = judulForum;
    data['deskripsi'] = deskripsi;
    data['creator_user_id'] = creatorUserId;
    data['course_id'] = courseId;
    data['class_id'] = classId;
    data['school_id'] = schoolId;
    data['posting_date'] = postingDate;
    data['full_name'] = fullName;
    data['image'] = image;
    data['delete_access'] = deleteAccess;
    data['amount_comments'] = amountComments;
    return data;
  }
}
