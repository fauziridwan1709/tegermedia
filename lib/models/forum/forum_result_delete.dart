part of '_forum.dart';

class ResultDeleteForum {
  int statusCode;
  String message;

  ResultDeleteForum({this.statusCode, this.message});

  ResultDeleteForum.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    return data;
  }
}
