part of '_forum.dart';

class ForumCreateModel {
  String judulForum;
  String deskripsi;
  String postingDate;
  int schoolId;
  int classId;
  List<ForumCreateAttachments> forumAttachments;

  ForumCreateModel(
      {this.judulForum,
      this.deskripsi,
      this.postingDate,
      this.schoolId,
      this.classId,
      this.forumAttachments});

  ForumCreateModel.fromJson(Map<String, dynamic> json) {
    judulForum = json['judul_forum'];
    deskripsi = json['deskripsi'];
    postingDate = json['posting_date'];
    schoolId = json['school_id'];
    classId = json['class_id'];
    if (json['forum_attachments'] != null) {
      forumAttachments = <ForumCreateAttachments>[];
      json['forum_attachments'].forEach((dynamic v) {
        forumAttachments
            .add(ForumCreateAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['judul_forum'] = judulForum;
    data['deskripsi'] = deskripsi;
    data['posting_date'] = postingDate;
    data['school_id'] = schoolId;
    data['class_id'] = classId;
    if (forumAttachments != null) {
      data['forum_attachments'] =
          forumAttachments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ForumCreateAttachments {
  String fileHashId;
  String fileLink;
  String fileName;
  String fileType;

  ForumCreateAttachments(
      {this.fileHashId, this.fileLink, this.fileName, this.fileType});

  ForumCreateAttachments.fromJson(Map<String, dynamic> json) {
    fileHashId = json['file_hash_id'];
    fileLink = json['file_link'];
    fileName = json['file_name'];
    fileType = json['file_type'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['file_hash_id'] = fileHashId;
    data['file_link'] = fileLink;
    data['file_name'] = fileName;
    data['file_type'] = fileType;
    return data;
  }
}
