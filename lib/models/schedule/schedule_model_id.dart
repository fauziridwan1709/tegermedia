part of '_schedule.dart';

class ScheduleModelId {
  int statusCode;
  String message;
  ScheduleItemId data;

  ScheduleModelId({this.statusCode, this.message, this.data});

  ScheduleModelId.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? ScheduleItemId.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ScheduleItemId {
  int id;
  int classId;
  int userId;
  String fullName;
  String className;
  int schoolId;
  String schoolName;
  String name;
  String description;
  String startDate;
  String endDate;
  String createdAt;
  String updatedAt;

  ScheduleItemId(
      {this.id,
      this.classId,
      this.userId,
      this.fullName,
      this.className,
      this.schoolId,
      this.schoolName,
      this.name,
      this.description,
      this.startDate,
      this.endDate,
      this.createdAt,
      this.updatedAt});

  ScheduleItemId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    classId = json['class_id'];
    userId = json['user_id'];
    fullName = json['full_name'];
    className = json['class_name'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    name = json['name'];
    description = json['description'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['class_id'] = classId;
    data['user_id'] = userId;
    data['full_name'] = fullName;
    data['class_name'] = className;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['name'] = name;
    data['description'] = description;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
