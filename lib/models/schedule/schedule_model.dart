part of '_schedule.dart';

class ScheduleModel {
  int statusCode;
  List<ScheduleItem> data;

  ScheduleModel({this.statusCode, this.data});

  ScheduleModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <ScheduleItem>[];
      json['data'].forEach((dynamic v) {
        data.add(ScheduleItem.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ScheduleItem {
  int iD;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  int classId;
  int schoolId;
  int userId;
  int courseId;
  int category;
  String name;
  String description;
  String startDate;
  String endDate;
  int iconData;
  int checkedInId;
  bool checkedOut;
  String role;

  ScheduleItem(
      {this.iD,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.classId,
      this.schoolId,
      this.userId,
      this.courseId,
      this.category,
      this.name,
      this.description,
      this.startDate,
      this.endDate,
      this.iconData,
      this.checkedInId,
      this.checkedOut,
      this.role});

  ScheduleItem.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    deletedAt = json['DeletedAt'];
    classId = json['class_id'];
    schoolId = json['school_id'];
    userId = json['user_id'];
    courseId = json['course_id'];
    category = json['category'];
    name = json['name'];
    description = json['description'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    iconData = json['icon_data'];
    checkedInId = json['checked_in_id'];
    checkedOut = json['checked_out'];
    role = json['role'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    data['DeletedAt'] = deletedAt;
    data['class_id'] = classId;
    data['school_id'] = schoolId;
    data['user_id'] = userId;
    data['course_id'] = courseId;
    data['category'] = category;
    data['name'] = name;
    data['description'] = description;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['icon_data'] = iconData;
    data['checked_in_id'] = checkedInId;
    data['checked_out'] = checkedOut;
    data['role'] = role;
    return data;
  }
}

//
// class ScheduleModel {
//   int statusCode;
//   String message;
//   List<ScheduleItem> data;
//
//   ScheduleModel({this.statusCode, this.message, this.data});
//
//   ScheduleModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['status_code'];
//     message = json['message'];
//     if (json['data'] != null) {
//       data = <ScheduleItem>[];
//       json['data'].forEach((Map<String, dynamic> v) {
//         data.add(ScheduleItem.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['status_code'] = statusCode;
//     data['message'] = message;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class ScheduleItem {
//   int id;
//   int classId;
//   int userId;
//   String fullName;
//   String className;
//   int schoolId;
//   String schoolName;
//   String name;
//   String description;
//   String startDate;
//   String endDate;
//   String createdAt;
//   String updatedAt;
//
//   ScheduleItem(
//       {this.id,
//       this.classId,
//       this.userId,
//       this.fullName,
//       this.className,
//       this.schoolId,
//       this.schoolName,
//       this.name,
//       this.description,
//       this.startDate,
//       this.endDate,
//       this.createdAt,
//       this.updatedAt});
//
//   ScheduleItem.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     classId = json['class_id'];
//     userId = json['user_id'];
//     fullName = json['full_name'];
//     className = json['class_name'];
//     schoolId = json['school_id'];
//     schoolName = json['school_name'];
//     name = json['name'];
//     description = json['description'];
//     startDate = json['start_date'];
//     endDate = json['end_date'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['id'] = id;
//     data['class_id'] = classId;
//     data['user_id'] = userId;
//     data['full_name'] = fullName;
//     data['class_name'] = className;
//     data['school_id'] = schoolId;
//     data['school_name'] = schoolName;
//     data['name'] = name;
//     data['description'] = description;
//     data['start_date'] = startDate;
//     data['end_date'] = endDate;
//     data['created_at'] = createdAt;
//     data['updated_at'] = updatedAt;
//     return data;
//   }
// }
