part of '_schedule.dart';

class CreateScheduleModel {
  int classId;
  int schoolId;
  int courseId;
  int iconData;
  int category;
  String name;
  String description;
  String startDate;
  String endDate;

  CreateScheduleModel({
    this.classId,
    this.schoolId,
    this.courseId,
    this.iconData,
    this.category,
    this.name,
    this.description,
    this.startDate,
    this.endDate,
  });

  CreateScheduleModel.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    schoolId = json['school_id'];
    courseId = json['course_id'];
    iconData = json['icon_data'];
    category = json['category'];
    name = json['name'];
    description = json['description'];
    startDate = json['start_date'];
    endDate = json['end_date'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = classId;
    data['school_id'] = schoolId;
    data['course_id'] = courseId;
    data['icon_data'] = iconData;
    data['category'] = category;
    data['name'] = name;
    data['description'] = description;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    return data;
  }
}
