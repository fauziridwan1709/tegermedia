part of '../_models.dart';

class QuizByIdModel {
  int statusCode;
  String message;
  QuizByIdData data;

  QuizByIdModel({this.statusCode, this.message, this.data});

  QuizByIdModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? QuizByIdData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class QuizByIdData {
  int id;
  int schoolId;
  String schoolName;
  int classId;
  String className;
  int courseId;
  String courseName;
  String quizType;
  String name;
  int totalTime;
  int totalPg;
  int totalEssay;
  double totalNilai;
  String startDate;
  String endDate;
  List<Students> students;
  List<Null> quizAttachments;
  bool isHidden;
  bool isRandom;

  QuizByIdData({
    this.id,
    this.schoolId,
    this.schoolName,
    this.classId,
    this.className,
    this.courseId,
    this.courseName,
    this.quizType,
    this.name,
    this.totalTime,
    this.totalPg,
    this.totalEssay,
    this.totalNilai,
    this.startDate,
    this.endDate,
    this.students,
    this.quizAttachments,
    this.isHidden,
    this.isRandom,
  });

  QuizByIdData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    courseName = json['course_name'];
    quizType = json['quiz_type'];
    name = json['name'];
    totalTime = json['total_time'];
    totalPg = json['total_pg'];
    totalEssay = json['total_essay'];
    totalNilai = json['total_nilai'] / 1.0;
    startDate = json['start_date'];
    endDate = json['end_date'];
    if (json['students'] != null) {
      students = <Students>[];
      json['students'].forEach((dynamic v) {
        students.add(Students.fromJson(v as Map<String, dynamic>));
      });
    }
    // if (json['quiz_attachments'] != null) {
    //   quizAttachments = <Null>[];
    //   json['quiz_attachments'].forEach((dynamic v) {
    //     quizAttachments.add(Null.fromJson(v as Map<String, dynamic>));
    //   });
    // }
    isHidden = json['is_hidden'];
    isRandom = json['is_random'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['class_id'] = classId;
    data['class_name'] = className;
    data['course_id'] = courseId;
    data['course_name'] = courseName;
    data['quiz_type'] = quizType;
    data['name'] = name;
    data['total_time'] = totalTime;
    data['total_pg'] = totalPg;
    data['total_essay'] = totalEssay;
    data['total_nilai'] = totalNilai;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    if (students != null) {
      data['students'] = students.map((v) => v.toJson()).toList();
    }
    // if (quizAttachments != null) {
    //   data['quiz_attachments'] =
    //       quizAttachments.map((v) => v.toJson()).toList();
    // }
    data['is_hidden'] = isHidden;
    data['is_random'] = isRandom;
    return data;
  }
}

class Students {
  int userId;
  String fullName;
  String image;
  bool hasBeenDone;
  double totalNilai;

  Students(
      {this.userId,
      this.fullName,
      this.image,
      this.hasBeenDone,
      this.totalNilai});

  Students.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    fullName = json['full_name'];
    image = json['image'];
    hasBeenDone = json['has_been_done'];
    totalNilai = json['total_nilai'] / 1.0;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = userId;
    data['full_name'] = fullName;
    data['image'] = image;
    data['has_been_done'] = hasBeenDone;
    data['total_nilai'] = totalNilai;
    return data;
  }
}
