part of '../_models.dart';

class QuizModelKelas {
  int statusCode;
  String message;
  List<QuizModelDetail> data;

  QuizModelKelas({this.statusCode, this.message, this.data});

  QuizModelKelas.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <QuizModelDetail>[];
      json['data'].forEach((dynamic v) {
        data.add(QuizModelDetail.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuizModelDetail {
  int id;
  int quizId;
  String quizName;
  String questionType;
  String questionDesc;
  String correctAnswer;
  String image;
  int score;
  List<QuizReference> reference;

  QuizModelDetail(
      {this.id,
      this.quizId,
      this.quizName,
      this.questionType,
      this.questionDesc,
      this.correctAnswer,
      this.image,
      this.score,
      this.reference});

  QuizModelDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    quizId = json['quiz_id'];
    quizName = json['quiz_name'];
    questionType = json['question_type'];
    questionDesc = json['question_description'];
    correctAnswer = json['correct_answer'];
    image = json['image'];
    score = json['score'];
    if (json['quiz_pg_references'] != null) {
      reference = <QuizReference>[];
      json['quiz_pg_references'].forEach((dynamic v) {
        reference.add(QuizReference.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['quiz_id'] = quizId;
    data['quiz_name'] = quizName;
    data['question_type'] = questionType;
    data['question_description'] = questionDesc;
    data['correct_answer'] = correctAnswer;
    data['image'] = image;
    data['score'] = score;
    if (reference != null) {
      data['quiz_pg_references'] = reference.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuizReference {
  int id;
  int bankSoalId;
  String orderQuestion;
  String pgDescription;

  QuizReference(
      {this.id, this.bankSoalId, this.orderQuestion, this.pgDescription});

  QuizReference.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bankSoalId = json['quiz_bank_soal_id'];
    orderQuestion = json['order_of_question'];
    pgDescription = json['pg_description'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['quiz_bank_soal_id'] = bankSoalId;
    data['order_of_question'] = orderQuestion;
    data['pg_description'] = pgDescription;
    return data;
  }
}

class QuizListModel {
  int statusCode;
  String message;
  List<QuizListDetail> data;

  QuizListModel({this.statusCode, this.message, this.data});

  QuizListModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <QuizListDetail>[];
      json['data'].forEach((dynamic v) {
        data.add(QuizListDetail.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuizListDetail {
  int id;
  int schoolId;
  String schoolName;
  int classId;
  String className;
  int courseId;
  String courseName;
  String quizType;
  String name;
  int totalTime;
  int totalPg;
  int totalEssay;
  double totalNilai;
  String startDate;
  String endDate;
  Null students;
  Null quizAttachments;
  bool hasBeenDone;
  bool isHidden;
  bool isRandom;
  String description;
  int status;

  QuizListDetail(
      {this.id,
      this.schoolId,
      this.schoolName,
      this.classId,
      this.className,
      this.courseId,
      this.courseName,
      this.quizType,
      this.name,
      this.totalTime,
      this.totalPg,
      this.totalEssay,
      this.totalNilai,
      this.startDate,
      this.endDate,
      this.students,
      this.quizAttachments,
      this.hasBeenDone,
      this.isHidden,
      this.isRandom,
      this.description,
      this.status,
      });

  QuizListDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    courseName = json['course_name'];
    quizType = json['quiz_type'];
    name = json['name'];
    totalTime = json['total_time'];
    totalPg = json['total_pg'];
    totalEssay = json['total_essay'];
    totalNilai = json['total_nilai'] / 1.0;
    startDate = json['start_date'];
    endDate = json['end_date'];
    students = json['students'];
    quizAttachments = json['quiz_attachments'];
    hasBeenDone = json['has_been_done'];
    isHidden = json['is_hidden'];
    isRandom = json['is_random'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['class_id'] = classId;
    data['class_name'] = className;
    data['course_id'] = courseId;
    data['course_name'] = courseName;
    data['quiz_type'] = quizType;
    data['name'] = name;
    data['total_time'] = totalTime;
    data['total_pg'] = totalPg;
    data['total_essay'] = totalEssay;
    data['total_nilai'] = totalNilai;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['students'] = students;
    data['quiz_attachments'] = quizAttachments;
    data['has_been_done'] = hasBeenDone;
    data['is_hidden'] = isHidden;
    data['is_random'] = isRandom;
    data['description'] = description;
    return data;
  }
}
