part of '_models.dart';

class ProtocolModel {
  List<ProtocolItem> rows;

  ProtocolModel({this.rows});

  ProtocolModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <ProtocolItem>[];
      json['rows'].forEach((dynamic v) {
        rows.add(ProtocolItem.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProtocolItem {
  String topik;
  String subtopik;
  String protokol;
  String isi;

  ProtocolItem({this.topik, this.subtopik, this.protokol, this.isi});

  ProtocolItem.fromJson(Map<String, dynamic> json) {
    topik = json['topik'];
    subtopik = json['subtopik'];
    protokol = json['protokol'];
    isi = json['isi'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['topik'] = topik;
    data['subtopik'] = subtopik;
    data['protokol'] = protokol;
    data['isi'] = isi;
    return data;
  }
}
