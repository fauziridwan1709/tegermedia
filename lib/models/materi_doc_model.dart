part of '_models.dart';

class MateriDocModel {
  List<MateriDocItemModel> rows;

  MateriDocModel({this.rows});

  MateriDocModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <MateriDocItemModel>[];
      json['rows'].forEach((dynamic v) {
        rows.add(MateriDocItemModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MateriDocItemModel {
  String kurikulum;
  String kelas;
  String matapelajaran;
  String judul;
  String link;
  String pengguna;
  String jenissumber;
  String status;

  MateriDocItemModel(
      {this.kurikulum,
      this.kelas,
      this.matapelajaran,
      this.judul,
      this.link,
      this.pengguna,
      this.jenissumber,
      this.status});

  MateriDocItemModel.fromJson(Map<String, dynamic> json) {
    kurikulum = json['kurikulum'].toString();
    kelas = json['kelas'].toString();
    matapelajaran = json['matapelajaran'].toString();
    judul = json['judul'].toString();
    link = json['link'].toString();
    pengguna = json['pengguna'].toString();
    jenissumber = json['jenissumber'].toString();
    status = json['status'].toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['kurikulum'] = kurikulum;
    data['kelas'] = kelas;
    data['matapelajaran'] = matapelajaran;
    data['judul'] = judul;
    data['link'] = link;
    data['pengguna'] = pengguna;
    data['jenissumber'] = jenissumber;
    data['status'] = status;
    return data;
  }
}
