part of '../_models.dart';

class AnswerSheet {
  int statusCode;
  String message;
  List<QuizAnswerData> data;

  AnswerSheet({this.statusCode, this.message, this.data});

  AnswerSheet.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <QuizAnswerData>[];
      json['data'].forEach((dynamic v) {
        data.add(QuizAnswerData.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuizAnswerData {
  int id;
  int quizId;
  String quizName;
  String questionType;
  String questionDescription;
  String correctAnswer;
  String image;
  int score;
  List<QuizPgReferences> quizPgReferences;
  String createdAt;
  String updatedAt;
  QuizAnswer quizAnswer;

  QuizAnswerData(
      {this.id,
      this.quizId,
      this.quizName,
      this.questionType,
      this.questionDescription,
      this.correctAnswer,
      this.image,
      this.score,
      this.quizPgReferences,
      this.createdAt,
      this.updatedAt,
      this.quizAnswer});

  QuizAnswerData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    quizId = json['quiz_id'];
    quizName = json['quiz_name'];
    questionType = json['question_type'];
    questionDescription = json['question_description'];
    correctAnswer = json['correct_answer'];
    image = json['image'];
    score = json['score'];
    if (json['quiz_pg_references'] != null) {
      quizPgReferences = <QuizPgReferences>[];
      json['quiz_pg_references'].forEach((dynamic v) {
        quizPgReferences
            .add(QuizPgReferences.fromJson(v as Map<String, dynamic>));
      });
    }
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    quizAnswer = json['quiz_answer'] != null
        ? QuizAnswer.fromJson(json['quiz_answer'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['quiz_id'] = quizId;
    data['quiz_name'] = quizName;
    data['question_type'] = questionType;
    data['question_description'] = questionDescription;
    data['correct_answer'] = correctAnswer;
    data['image'] = image;
    data['score'] = score;
    if (quizPgReferences != null) {
      data['quiz_pg_references'] =
          quizPgReferences.map((v) => v.toJson()).toList();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (quizAnswer != null) {
      data['quiz_answer'] = quizAnswer.toJson();
    }
    return data;
  }
}

class QuizPgReferences {
  int id;
  int quizBankSoalId;
  String orderOfQuestion;
  String pgDescription;

  QuizPgReferences(
      {this.id, this.quizBankSoalId, this.orderOfQuestion, this.pgDescription});

  QuizPgReferences.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    quizBankSoalId = json['quiz_bank_soal_id'];
    orderOfQuestion = json['order_of_question'];
    pgDescription = json['pg_description'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['quiz_bank_soal_id'] = quizBankSoalId;
    data['order_of_question'] = orderOfQuestion;
    data['pg_description'] = pgDescription;
    return data;
  }
}

class QuizAnswer {
  int id;
  String answerDescription;
  String noted;

  QuizAnswer({this.id, this.answerDescription, this.noted});

  QuizAnswer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    answerDescription = json['answer_description'];
    noted = json['noted'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['answer_description'] = answerDescription;
    data['noted'] = noted;
    return data;
  }
}
