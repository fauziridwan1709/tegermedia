part of '_materi.dart';

class StudentRecModel {
  StudentRecModel({this.rows});
  List<StudentRecItem> rows;

  StudentRecModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <StudentRecItem>[];
      json['rows'].forEach((dynamic e) {
        rows.add(StudentRecItem.fromJson(e as Map<String, dynamic>));
      });
    }
  }
}

class StudentRecItem {
  String namaaplikasi;
  String jenis;
  String kelas;
  String kategori;
  String fungsi;
  String pelajaran;
  String keterangan;
  String screenshot;
  String icon;
  String link;
  String status;

  StudentRecItem(
      {this.namaaplikasi,
      this.jenis,
      this.kelas,
      this.kategori,
      this.fungsi,
      this.pelajaran,
      this.keterangan,
      this.screenshot,
      this.icon,
      this.link,
      this.status});

  StudentRecItem.fromJson(Map<String, dynamic> json) {
    namaaplikasi = json['namaaplikasi'].toString();
    jenis = json['jenis'].toString();
    kelas = json['kelas'].toString();
    kategori = json['kategori'].toString();
    fungsi = json['fungsi'].toString();
    pelajaran = json['pelajaran'].toString();
    keterangan = json['keterangan'].toString();
    screenshot = json['screenshot'].toString();
    icon = json['icon'].toString();
    link = json['link'].toString();
    status = json['status'].toString();
  }
}
