part of '_materi.dart';

class MateriQuizModel {
  List<MateriQuizDetail> rows;

  MateriQuizModel({this.rows});

  MateriQuizModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <MateriQuizDetail>[];
      json['rows'].forEach((dynamic v) {
        rows.add(MateriQuizDetail.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MateriQuizDetail {
  String kelas;
  String matapelajaran;
  String materi;
  String jumlahsoal;
  String menit;

  MateriQuizDetail(
      {this.kelas,
      this.matapelajaran,
      this.materi,
      this.jumlahsoal,
      this.menit});

  MateriQuizDetail.fromJson(Map<String, dynamic> json) {
    kelas = json['kelas'].toString();
    matapelajaran = json['matapelajaran'].toString();
    materi = json['materi'].toString();
    jumlahsoal = json['jumlahsoal'].toString();
    menit = json['menit'].toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['kelas'] = kelas;
    data['matapelajaran'] = matapelajaran;
    data['materi'] = materi;
    data['jumlahsoal'] = jumlahsoal;
    data['menit'] = menit;
    return data;
  }
}

class MateriQuizLink {
  String materiaplikasi;
  String sheetid;
  String sheetno;
  String linkgsheet;
  String linkapi;

  MateriQuizLink(
      {this.materiaplikasi,
      this.sheetid,
      this.sheetno,
      this.linkgsheet,
      this.linkapi});

  MateriQuizLink.fromJson(Map<String, dynamic> json) {
    materiaplikasi = json['materiaplikasi'].toString();
    sheetid = json['sheetid'].toString();
    sheetno = json['sheetno.'].toString();
    linkgsheet = json['linkgsheet'].toString();
    linkapi = json['linkapi'].toString();
  }
}
