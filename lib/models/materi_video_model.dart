part of '_models.dart';

class MateriVideoModel {
  List<MateriVideoItemModel> rows;

  MateriVideoModel({this.rows});

  MateriVideoModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <MateriVideoItemModel>[];
      json['rows'].forEach((dynamic v) {
        rows.add(MateriVideoItemModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MateriVideoItemModel {
  String kurikulum;
  String kelas;
  String matapelajaran;
  String judul;
  String link;
  String thumb;
  String pengguna;
  String jenissumber;
  String status;
  String likes;
  String watch;

  MateriVideoItemModel(
      {this.kurikulum,
      this.kelas,
      this.matapelajaran,
      this.judul,
      this.link,
      this.thumb,
      this.pengguna,
      this.jenissumber,
      this.status,
      this.likes,
      this.watch});

  MateriVideoItemModel.fromJson(Map<String, dynamic> json) {
    kurikulum = json['kurikulum'].toString();
    kelas = json['kelas'].toString();
    matapelajaran = json['matapelajaran'];
    judul = json['judul'];
    link = json['link'].toString();
    thumb = json['thumb'];
    pengguna = json['pengguna'];
    jenissumber = json['jenissumber'];
    status = json['status'];
    likes = json['likes'];
    watch = json['watch'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['kurikulum'] = kurikulum;
    data['kelas'] = kelas;
    data['matapelajaran'] = matapelajaran;
    data['judul'] = judul;
    data['link'] = link;
    data['thumb'] = thumb;
    data['pengguna'] = pengguna;
    data['jenissumber'] = jenissumber;
    data['status'] = status;
    data['likes'] = likes;
    data['watch'] = watch;
    return data;
  }
}
