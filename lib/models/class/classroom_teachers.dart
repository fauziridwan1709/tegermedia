part of '_class.dart';

class ClassroomTeachers {
  ClassroomTeachersData _data;
  Null _meta;

  ClassroomTeachers({ClassroomTeachersData data, Null meta}) {
    _data = data;
    _meta = meta;
  }

  ClassroomTeachersData get data => _data;
  set data(ClassroomTeachersData data) => _data = data;
  Null get meta => _meta;
  set meta(Null meta) => _meta = meta;

  ClassroomTeachers.fromJson(Map<String, dynamic> json) {
    _data = json['teachers'] != null
        ? ClassroomTeachersData.fromJson(json['teachers'])
        : null;
    _meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    data['meta'] = _meta;
    return data;
  }
}

class ClassroomTeachersData {
  List<GoogleClassroomTeachers> _teachers;

  ClassroomTeachersData({List<GoogleClassroomTeachers> teachers}) {
    _teachers = teachers;
  }

  List<GoogleClassroomTeachers> get teachers => _teachers;
  set teachers(List<GoogleClassroomTeachers> teachers) => _teachers = teachers;

  ClassroomTeachersData.fromJson(Map<String, dynamic> json) {
    if (json['teachers'] != null) {
      _teachers = <GoogleClassroomTeachers>[];
      json['teachers'].forEach((dynamic v) {
        _teachers
            .add(GoogleClassroomTeachers.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (_teachers != null) {
      data['teachers'] = _teachers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GoogleClassroomTeachers {
  String _courseId;
  Profile _profile;
  String _userId;

  GoogleClassroomTeachers({String courseId, Profile profile, String userId}) {
    _courseId = courseId;
    _profile = profile;
    _userId = userId;
  }

  String get courseId => _courseId;
  set courseId(String courseId) => _courseId = courseId;
  Profile get profile => _profile;
  set profile(Profile profile) => _profile = profile;
  String get userId => _userId;
  set userId(String userId) => _userId = userId;

  GoogleClassroomTeachers.fromJson(Map<String, dynamic> json) {
    _courseId = json['courseId'];
    _profile =
        json['profile'] != null ? Profile.fromJson(json['profile']) : null;
    _userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['courseId'] = _courseId;
    if (_profile != null) {
      data['profile'] = _profile.toJson();
    }
    data['userId'] = _userId;
    return data;
  }
}

class Permissions {
  String _permission;

  Permissions({String permission}) {
    _permission = permission;
  }

  String get permission => _permission;
  set permission(String permission) => _permission = permission;

  Permissions.fromJson(Map<String, dynamic> json) {
    _permission = json['permission'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['permission'] = _permission;
    return data;
  }
}
