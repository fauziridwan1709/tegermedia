part of '_class.dart';

class GoogleClassroomClass {
  String _name;
  String _courseName;
  String _gcrId;
  int _schoolId;
  String _tahunAjaran;
  String _semester;
  String _jurusan;
  String _tingkat;
  List<String> _studentList;
  List<String> _teacherList;

  GoogleClassroomClass(
      {String name,
      String courseName,
      String gcrId,
      int schoolId,
      String tahunAjaran,
      String semester,
      String jurusan,
      String tingkat,
      List<String> studentList,
      List<String> teacherList}) {
    _name = name;
    _courseName = courseName;
    _gcrId = gcrId;
    _schoolId = schoolId;
    _tahunAjaran = tahunAjaran;
    _semester = semester;
    _jurusan = jurusan;
    _tingkat = tingkat;
    _studentList = studentList;
    _teacherList = teacherList;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get courseName => _courseName;
  set courseName(String courseName) => _courseName = courseName;
  String get gcrId => _gcrId;
  set gcrId(String gcrId) => _gcrId = gcrId;
  int get schoolId => _schoolId;
  set schoolId(int schoolId) => _schoolId = schoolId;
  String get tahunAjaran => _tahunAjaran;
  set tahunAjaran(String tahunAjaran) => _tahunAjaran = tahunAjaran;
  String get semester => _semester;
  set semester(String semester) => _semester = semester;
  String get jurusan => _jurusan;
  set jurusan(String jurusan) => _jurusan = jurusan;
  String get tingkat => _tingkat;
  set tingkat(String tingkat) => _tingkat = tingkat;
  List<String> get studentList => _studentList;
  set studentList(List<String> studentList) => _studentList = studentList;
  List<String> get teacherList => _teacherList;
  set teacherList(List<String> teacherList) => _teacherList = teacherList;

  GoogleClassroomClass.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _courseName = json['course_name'];
    _gcrId = json['gcr_id'];
    _schoolId = json['school_id'];
    _tahunAjaran = json['tahun_ajaran'];
    _semester = json['semester'];
    _jurusan = json['jurusan'];
    _tingkat = json['tingkat'];
    _studentList = json['student_list'].cast<String>();
    _teacherList = json['teacher_list'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = _name;
    data['course_name'] = _courseName;
    data['gcr_id'] = _gcrId;
    data['school_id'] = _schoolId;
    data['tahun_ajaran'] = _tahunAjaran;
    data['semester'] = _semester;
    data['jurusan'] = _jurusan;
    data['tingkat'] = _tingkat;
    data['student_list'] = _studentList;
    data['teacher_list'] = _teacherList;
    return data;
  }
}
