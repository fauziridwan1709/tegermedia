part of '_class.dart';

class ClassroomCourse {
  List<Courses> courses;

  ClassroomCourse({this.courses});

  ClassroomCourse.fromJson(Map<String, dynamic> json) {
    if (json['courses'] != null) {
      courses = <Courses>[];
      json['courses'].forEach((dynamic v) {
        courses.add(Courses.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (courses != null) {
      data['courses'] = courses.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Courses {
  String id;
  String name;

  Courses({this.id, this.name});

  Courses.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

// class ClassroomCourse {
//   ClassroomCourseData _data;
//   Null _meta;
//
//   ClassroomCourse({ClassroomCourseData data, Null meta}) {
//     _data = data;
//     _meta = meta;
//   }
//
//   ClassroomCourseData get data => _data;
//   set data(ClassroomCourseData data) => _data = data;
//   Null get meta => _meta;
//   set meta(Null meta) => _meta = meta;
//
//   ClassroomCourse.fromJson(Map<String, dynamic> json) {
//     _data = json['data'] != null
//         ? ClassroomCourseData.fromJson(json['data'])
//         : null;
//     _meta = json['meta'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     if (_data != null) {
//       data['data'] = _data.toJson();
//     }
//     data['meta'] = _meta;
//     return data;
//   }
// }
//
// class ClassroomCourseData {
//   List<Courses> _courses;
//
//   ClassroomCourseData({List<Courses> courses}) {
//     _courses = courses;
//   }
//
//   List<Courses> get courses => _courses;
//   set courses(List<Courses> courses) => _courses = courses;
//
//   ClassroomCourseData.fromJson(Map<String, dynamic> json) {
//     if (json['courses'] != null) {
//       _courses = <Courses>[];
//       json['courses'].forEach((dynamic v) {
//         _courses.add(Courses.fromJson(v as Map<String, dynamic>));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     if (_courses != null) {
//       data['courses'] = _courses.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Courses {
//   String _alternateLink;
//   String _calendarId;
//   String _courseGroupEmail;
//   String _courseState;
//   String _creationTime;
//   String _descriptionHeading;
//   String _enrollmentCode;
//   String _id;
//   String _name;
//   String _ownerId;
//   TeacherFolder _teacherFolder;
//   String _teacherGroupEmail;
//   String _updateTime;
//
//   Courses(
//       {String alternateLink,
//       String calendarId,
//       String courseGroupEmail,
//       String courseState,
//       String creationTime,
//       String descriptionHeading,
//       String enrollmentCode,
//       String id,
//       String name,
//       String ownerId,
//       TeacherFolder teacherFolder,
//       String teacherGroupEmail,
//       String updateTime}) {
//     _alternateLink = alternateLink;
//     _calendarId = calendarId;
//     _courseGroupEmail = courseGroupEmail;
//     _courseState = courseState;
//     _creationTime = creationTime;
//     _descriptionHeading = descriptionHeading;
//     _enrollmentCode = enrollmentCode;
//     _id = id;
//     _name = name;
//     _ownerId = ownerId;
//     _teacherFolder = teacherFolder;
//     _teacherGroupEmail = teacherGroupEmail;
//     _updateTime = updateTime;
//   }
//
//   String get alternateLink => _alternateLink;
//   set alternateLink(String alternateLink) => _alternateLink = alternateLink;
//   String get calendarId => _calendarId;
//   set calendarId(String calendarId) => _calendarId = calendarId;
//   String get courseGroupEmail => _courseGroupEmail;
//   set courseGroupEmail(String courseGroupEmail) =>
//       _courseGroupEmail = courseGroupEmail;
//   String get courseState => _courseState;
//   set courseState(String courseState) => _courseState = courseState;
//   String get creationTime => _creationTime;
//   set creationTime(String creationTime) => _creationTime = creationTime;
//   String get descriptionHeading => _descriptionHeading;
//   set descriptionHeading(String descriptionHeading) =>
//       _descriptionHeading = descriptionHeading;
//   String get enrollmentCode => _enrollmentCode;
//   set enrollmentCode(String enrollmentCode) => _enrollmentCode = enrollmentCode;
//   String get id => _id;
//   set id(String id) => _id = id;
//   String get name => _name;
//   set name(String name) => _name = name;
//   String get ownerId => _ownerId;
//   set ownerId(String ownerId) => _ownerId = ownerId;
//   TeacherFolder get teacherFolder => _teacherFolder;
//   set teacherFolder(TeacherFolder teacherFolder) =>
//       _teacherFolder = teacherFolder;
//   String get teacherGroupEmail => _teacherGroupEmail;
//   set teacherGroupEmail(String teacherGroupEmail) =>
//       _teacherGroupEmail = teacherGroupEmail;
//   String get updateTime => _updateTime;
//   set updateTime(String updateTime) => _updateTime = updateTime;
//
//   Courses.fromJson(Map<String, dynamic> json) {
//     _alternateLink = json['alternateLink'];
//     _calendarId = json['calendarId'];
//     _courseGroupEmail = json['courseGroupEmail'];
//     _courseState = json['courseState'];
//     _creationTime = json['creationTime'];
//     _descriptionHeading = json['descriptionHeading'];
//     _enrollmentCode = json['enrollmentCode'];
//     _id = json['id'];
//     _name = json['name'];
//     _ownerId = json['ownerId'];
//     _teacherFolder = json['teacherFolder'] != null
//         ? TeacherFolder.fromJson(json['teacherFolder'])
//         : null;
//     _teacherGroupEmail = json['teacherGroupEmail'];
//     _updateTime = json['updateTime'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['alternateLink'] = _alternateLink;
//     data['calendarId'] = _calendarId;
//     data['courseGroupEmail'] = _courseGroupEmail;
//     data['courseState'] = _courseState;
//     data['creationTime'] = _creationTime;
//     data['descriptionHeading'] = _descriptionHeading;
//     data['enrollmentCode'] = _enrollmentCode;
//     data['id'] = _id;
//     data['name'] = _name;
//     data['ownerId'] = _ownerId;
//     if (_teacherFolder != null) {
//       data['teacherFolder'] = _teacherFolder.toJson();
//     }
//     data['teacherGroupEmail'] = _teacherGroupEmail;
//     data['updateTime'] = _updateTime;
//     return data;
//   }
// }
//
// class TeacherFolder {
//   String _alternateLink;
//   String _id;
//   String _title;
//
//   TeacherFolder({String alternateLink, String id, String title}) {
//     _alternateLink = alternateLink;
//     _id = id;
//     _title = title;
//   }
//
//   String get alternateLink => _alternateLink;
//   set alternateLink(String alternateLink) => _alternateLink = alternateLink;
//   String get id => _id;
//   set id(String id) => _id = id;
//   String get title => _title;
//   set title(String title) => _title = title;
//
//   TeacherFolder.fromJson(Map<String, dynamic> json) {
//     _alternateLink = json['alternateLink'];
//     _id = json['id'];
//     _title = json['title'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['alternateLink'] = _alternateLink;
//     data['id'] = _id;
//     data['title'] = _title;
//     return data;
//   }
// }
