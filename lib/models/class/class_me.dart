part of '_class.dart';

class ClassBySchoolModel {
  int _statusCode;
  String _message;
  CUDClassModel _cudModel;
  CreateClassModel _createModel;
  UpdateClassModel _updateModel;
  DeleteClassModel _deleteModel;
  GetClassModel _param;
  List<ClassBySchoolData> _dataModel;

  ClassBySchoolModel({
    int statusCode,
    String message,
    List<ClassBySchoolData> data,
    CUDClassModel cudModel,
    CreateClassModel createModel,
    UpdateClassModel updateModel,
    DeleteClassModel deleteModel,
    GetClassModel param,
  }) {
    _statusCode = statusCode;
    _message = message;
    _dataModel = data;
    _cudModel = cudModel;
    _createModel = createModel;
    _deleteModel = deleteModel;
    _updateModel = updateModel;
    _param = param;
  }

  int get statusCode => _statusCode;
  set statusCode(int statusCode) => _statusCode = statusCode;
  String get message => _message;
  set message(String message) => _message = message;
  List<ClassBySchoolData> get dataModel => _dataModel;
  set data(List<ClassBySchoolData> data) => _dataModel = data;

  @deprecated
  CreateClassModel get createModel => _createModel;
  set createModel(CreateClassModel data) => _createModel = data;

  @deprecated
  UpdateClassModel get updateModel => _updateModel;
  set updateModel(UpdateClassModel data) => _updateModel = data;

  @deprecated
  DeleteClassModel get deleteModel => _deleteModel;
  set deleteModel(DeleteClassModel data) => _deleteModel = data;

  CUDClassModel get cudModel => _cudModel;
  set cudModel(CUDClassModel data) => _cudModel = data;

  GetClassModel get param => _param;
  set param(GetClassModel data) => _param = data;

  ClassBySchoolModel.fromJson(Map<String, dynamic> json) {
    _statusCode = json['status_code'];
    _message = json['message'];
    if (json['data'] != null) {
      _dataModel = <ClassBySchoolData>[];
      json['data'].forEach((dynamic v) {
        _dataModel.add(ClassBySchoolData.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = _statusCode;
    data['message'] = _message;
    if (_dataModel != null) {
      data['data'] = _dataModel.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ClassBySchoolData {
  int _id;
  int _schoolId;
  String _schoolName;
  String _name;
  String _description;
  String _tahunAjaran;
  String _semester;
  String _jurusan;
  String _tingkat;
  String _invitationStudentCode;
  String _invitationTeacherCode;
  String _role;
  String _namaWalikelas;
  String _nomorTelpWalikelas;
  bool _isSudahGabung;
  int _totalGuru;
  int _totalSiswa;
  bool _isApprove;
  String _createdAt;
  String _updatedAt;
  int _totalCourse;
  List<StudentIdList> _studentIdList;
  String _googleClassRoomId;

  ClassBySchoolData(
      {int id,
      int schoolId,
      String schoolName,
      String name,
      String description,
      String tahunAjaran,
      String semester,
      String jurusan,
      String tingkat,
      String invitationStudentCode,
      String invitationTeacherCode,
      String role,
      String namaWalikelas,
      String nomorTelpWalikelas,
      bool isSudahGabung,
      int totalGuru,
      int totalSiswa,
      bool isApprove,
      String createdAt,
      String updatedAt,
      int totalCourse,
      List<StudentIdList> studentIdList,
      String googleClassRoomId}) {
    _id = id;
    _schoolId = schoolId;
    _schoolName = schoolName;
    _name = name;
    _description = description;
    _tahunAjaran = tahunAjaran;
    _semester = semester;
    _jurusan = jurusan;
    _tingkat = tingkat;
    _invitationStudentCode = invitationStudentCode;
    _invitationTeacherCode = invitationTeacherCode;
    _role = role;
    _namaWalikelas = namaWalikelas;
    _nomorTelpWalikelas = nomorTelpWalikelas;
    _isSudahGabung = isSudahGabung;
    _totalGuru = totalGuru;
    _totalSiswa = totalSiswa;
    _isApprove = isApprove;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _totalCourse = totalCourse;
    _studentIdList = studentIdList;
    _googleClassRoomId = googleClassRoomId;
  }

  int get id => _id;
  set id(int id) => _id = id;
  int get schoolId => _schoolId;
  set schoolId(int schoolId) => _schoolId = schoolId;
  String get schoolName => _schoolName;
  set schoolName(String schoolName) => _schoolName = schoolName;
  String get name => _name;
  set name(String name) => _name = name;
  String get description => _description;
  set description(String description) => _description = description;
  String get tahunAjaran => _tahunAjaran;
  set tahunAjaran(String tahunAjaran) => _tahunAjaran = tahunAjaran;
  String get semester => _semester;
  set semester(String semester) => _semester = semester;
  String get jurusan => _jurusan;
  set jurusan(String jurusan) => _jurusan = jurusan;
  String get tingkat => _tingkat;
  set tingkat(String tingkat) => _tingkat = tingkat;
  String get invitationStudentCode => _invitationStudentCode;
  set invitationStudentCode(String invitationStudentCode) =>
      _invitationStudentCode = invitationStudentCode;
  String get invitationTeacherCode => _invitationTeacherCode;
  set invitationTeacherCode(String invitationTeacherCode) =>
      _invitationTeacherCode = invitationTeacherCode;
  String get role => _role;
  set role(String role) => _role = role;
  String get namaWalikelas => _namaWalikelas;
  set namaWalikelas(String namaWalikelas) => _namaWalikelas = namaWalikelas;
  String get nomorTelpWalikelas => _nomorTelpWalikelas;
  set nomorTelpWalikelas(String nomorTelpWalikelas) =>
      _nomorTelpWalikelas = nomorTelpWalikelas;
  bool get isSudahGabung => _isSudahGabung;
  set isSudahGabung(bool isSudahGabung) => _isSudahGabung = isSudahGabung;
  int get totalGuru => _totalGuru;
  set totalGuru(int totalGuru) => _totalGuru = totalGuru;
  int get totalSiswa => _totalSiswa;
  set totalSiswa(int totalSiswa) => _totalSiswa = totalSiswa;
  bool get isApprove => _isApprove;
  set isApprove(bool isApprove) => _isApprove = isApprove;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  int get totalCourse => _totalCourse;
  set totalCourse(int totalCourse) => _totalCourse = totalCourse;
  List<StudentIdList> get studentIdList => _studentIdList;
  set studentIdList(List<StudentIdList> studentIdList) =>
      _studentIdList = studentIdList;
  String get googleClassRoomId => _googleClassRoomId;
  set googleClassRoomId(String googleClassRoomId) =>
      _googleClassRoomId = googleClassRoomId;

  ClassBySchoolData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _schoolId = json['school_id'];
    _schoolName = json['school_name'];
    _name = json['name'];
    _description = json['description'];
    _tahunAjaran = json['tahun_ajaran'];
    _semester = json['semester'];
    _jurusan = json['jurusan'];
    _tingkat = json['tingkat'];
    _invitationStudentCode = json['invitation_student_code'];
    _invitationTeacherCode = json['invitation_teacher_code'];
    _role = json['role'];
    _namaWalikelas = json['nama_walikelas'];
    _nomorTelpWalikelas = json['nomor_telp_walikelas'];
    _isSudahGabung = json['is_sudah_gabung'];
    _totalGuru = json['total_guru'];
    _totalSiswa = json['total_siswa'];
    _isApprove = json['is_approve'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _totalCourse = json['total_course'];
    if (json['student_id_list'] != null) {
      _studentIdList = <StudentIdList>[];
      json['student_id_list'].forEach((dynamic v) {
        _studentIdList.add(StudentIdList.fromJson(v as Map<String, dynamic>));
      });
    }
    _googleClassRoomId = json['google_class_room_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = _id;
    data['school_id'] = _schoolId;
    data['school_name'] = _schoolName;
    data['name'] = _name;
    data['description'] = _description;
    data['tahun_ajaran'] = _tahunAjaran;
    data['semester'] = _semester;
    data['jurusan'] = _jurusan;
    data['tingkat'] = _tingkat;
    data['invitation_student_code'] = _invitationStudentCode;
    data['invitation_teacher_code'] = _invitationTeacherCode;
    data['role'] = _role;
    data['nama_walikelas'] = _namaWalikelas;
    data['nomor_telp_walikelas'] = _nomorTelpWalikelas;
    data['is_sudah_gabung'] = _isSudahGabung;
    data['total_guru'] = _totalGuru;
    data['total_siswa'] = _totalSiswa;
    data['is_approve'] = _isApprove;
    data['created_at'] = _createdAt;
    data['updated_at'] = _updatedAt;
    data['total_course'] = _totalCourse;
    if (_studentIdList != null) {
      data['student_id_list'] = _studentIdList.map((v) => v.toJson()).toList();
    }
    data['google_class_room_id'] = _googleClassRoomId;
    return data;
  }
}

class CreateClassModel {}

class UpdateClassModel {}

class DeleteClassModel {}

class CUDClassModel {
  ClassBySchoolData detail;

  CUDClassModel(this.detail);
}

class GetClassModel {
  int _schoolId;
  bool _me;

  GetClassModel({int schoold, bool me}) {
    _schoolId = schoold;
    _me = me;
  }

  set me(bool data) => _me = data;
  bool get me => _me;

  set schoolId(int data) => _schoolId = data;
  int get schoolId => _schoolId;
}

class StudentIdList {
  int _studentId;

  StudentIdList({int studentId}) {
    _studentId = studentId;
  }

  int get studentId => _studentId;
  set studentId(int studentId) => _studentId = studentId;

  StudentIdList.fromJson(Map<String, dynamic> json) {
    _studentId = json['student_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['student_id'] = _studentId;
    return data;
  }
}
