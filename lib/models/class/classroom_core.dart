part of '_class.dart';

class ClassroomCore {
  Courses classroomCourses;
  ClassroomStudentsData students;
  ClassroomTeachersData teachers;

  ClassroomCore({this.classroomCourses, this.students, this.teachers});
}
