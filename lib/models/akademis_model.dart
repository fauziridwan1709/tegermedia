part of '_models.dart';

class AkademisModel {
  List<AkademisDetailModel> rows;

  AkademisModel({this.rows});

  AkademisModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <AkademisDetailModel>[];
      json['rows'].forEach((dynamic v) async {
        rows.add(AkademisDetailModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AkademisDetailModel {
  String topik;
  String subtopik;
  String protokol;
  String isi;

  AkademisDetailModel({this.topik, this.subtopik, this.protokol, this.isi});

  AkademisDetailModel.fromJson(Map<String, dynamic> json) {
    topik = json['topik'].toString();
    subtopik = json['subtopik'].toString();
    protokol = json['protokol'].toString();
    isi = json['isi'].toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['topik'] = topik;
    data['subtopik'] = subtopik;
    data['protokol'] = protokol;
    data['isi'] = isi;
    return data;
  }
}
