part of '_models.dart';

class JadwalModel {
  String judul;
  String kategori;
  DateTime start;
  DateTime end;

  JadwalModel({this.judul, this.kategori, this.start, this.end});
}
