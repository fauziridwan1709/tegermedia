
class GlobalModel {
  int statusCode;
  String message;
  GlobalModel({this.statusCode, this.message});

  GlobalModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    return data;
  }
}