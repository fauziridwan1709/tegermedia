part of '_models.dart';

class SignInModel {
  int statusCode;
  DataSigin data;
  Null error;

  SignInModel({this.statusCode, this.data, this.error});

  SignInModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = json['data'] != null ? DataSigin.fromJson(json['data']) : null;
    error = json['Error'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['Error'] = error;
    return data;
  }
}

class DataSigin {
  String token;
  bool isNoSchools;
  bool isNoClasses;
  bool isNoCourses;
  String userID;
  String email;
  String nama;

  DataSigin({this.token, this.isNoSchools, this.isNoClasses, this.isNoCourses});

  DataSigin.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    userID = json['user_id'];
    isNoSchools = json['is_no_schools'];
    isNoClasses = json['is_no_classes'];
    isNoCourses = json['is_no_courses'];
    email = json['email'];
    nama = json['nama'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['token'] = token;
    data['is_no_schools'] = isNoSchools;
    data['is_no_classes'] = isNoClasses;
    data['is_no_courses'] = isNoCourses;
    data['user_id'] = userID;
    data['nama'] = nama;
    data['email'] = email;
    return data;
  }
}

class UserLoginModel {
  String email;
  String googleUserId;
  String googleAccessToken;
  String googleAuthCode;
  String googleFullName;
  String googleImageProfile;

  UserLoginModel(
      {this.email,
      this.googleUserId,
      this.googleAccessToken,
      this.googleAuthCode,
      this.googleFullName,
      this.googleImageProfile});

  UserLoginModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    googleUserId = json['google_user_id'];
    googleAccessToken = json['google_access_token'];
    googleAuthCode = json['google_auth_code'];
    googleFullName = json['google_full_name'];
    googleImageProfile = json['google_image_profile'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['google_user_id'] = googleUserId;
    data['google_access_token'] = googleAccessToken;
    data['google_auth_code'] = googleAuthCode;
    data['google_full_name'] = googleFullName;
    data['google_image_profile'] = googleImageProfile;
    return data;
  }
}

class SignInBodyModel {
  String _registerType;
  String _email;
  String _password;

  SignInBodyModel({String registerType, String email, String password}) {
    _registerType = registerType;
    _email = email;
    _password = password;
  }

  String get registerType => _registerType;
  set registerType(String registerType) => _registerType = registerType;
  String get email => _email;
  set email(String email) => _email = email;
  String get password => _password;
  set password(String password) => _password = password;

  SignInBodyModel.fromJson(Map<String, dynamic> json) {
    _registerType = json['register_type'];
    _email = json['email'];
    _password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['register_type'] = _registerType;
    data['email'] = _email;
    data['password'] = _password;
    return data;
  }
}

class UserSignInModel {
  UserSignInData _data;
  Null _meta;

  UserSignInModel({UserSignInData data, Null meta}) {
    _data = data;
    _meta = meta;
  }

  UserSignInData get data => _data;
  set data(UserSignInData data) => _data = data;
  Null get meta => _meta;
  set meta(Null meta) => _meta = meta;

  UserSignInModel.fromJson(Map<String, dynamic> json) {
    _data = json['data'] != null ? UserSignInData.fromJson(json['data']) : null;
    _meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    data['meta'] = _meta;
    return data;
  }
}

class UserSignInData {
  String _token;
  String _expiredDate;
  String _refreshToken;
  String _refreshExpiredDate;
  String _latestAction;

  UserSignInData(
      {String token,
      String expiredDate,
      String refreshToken,
      String refreshExpiredDate,
      String latestAction}) {
    _token = token;
    _expiredDate = expiredDate;
    _refreshToken = refreshToken;
    _refreshExpiredDate = refreshExpiredDate;
    _latestAction = latestAction;
  }

  String get token => _token;
  set token(String token) => _token = token;
  String get expiredDate => _expiredDate;
  set expiredDate(String expiredDate) => _expiredDate = expiredDate;
  String get refreshToken => _refreshToken;
  set refreshToken(String refreshToken) => _refreshToken = refreshToken;
  String get refreshExpiredDate => _refreshExpiredDate;
  set refreshExpiredDate(String refreshExpiredDate) =>
      _refreshExpiredDate = refreshExpiredDate;
  String get latestAction => _latestAction;
  set latestAction(String latestAction) => _latestAction = latestAction;

  UserSignInData.fromJson(Map<String, dynamic> json) {
    _token = json['token'];
    _expiredDate = json['expired_date'];
    _refreshToken = json['refresh_token'];
    _refreshExpiredDate = json['refresh_expired_date'];
    _latestAction = json['latest_action'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['token'] = _token;
    data['expired_date'] = _expiredDate;
    data['refresh_token'] = _refreshToken;
    data['refresh_expired_date'] = _refreshExpiredDate;
    data['latest_action'] = _latestAction;
    return data;
  }
}
