# Models

example

```dart
class AbsensiModel{
  CreateAbsensiModel createModel;
  UpdateAbsensiModel updateModel;
  DeleteAbsensiModel  deleteModel;
  DataAbsensiModel dataModel;
  
  AbsensiModel({this.createModel,this.updateModel,this.deleteModel,this.dataModel});
}
class CreateAbsensiModel {
  int classId;
  int courseId;
  int scheduleId;
  String checkInTime;

  CreateAbsensiModel(
      {this.classId, this.courseId, this.scheduleId, this.checkInTime});

  CreateAbsensiModel.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    courseId = json['course_id'];
    scheduleId = json['schedule_id'];
    checkInTime = json['check_in_time'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = classId;
    data['course_id'] = courseId;
    data['schedule_id'] = scheduleId;
    data['check_in_time'] = checkInTime;
    return data;
  }
}

class UpdateAbsensiModel {}

class DeleteAbsensiModel {}

class DataAbsensiModel {}
```
