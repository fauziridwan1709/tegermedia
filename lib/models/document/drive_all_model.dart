part of '_document.dart';

class AllSubFile {
  int statusCode;
  List<Files> files;

  AllSubFile({this.statusCode, this.files});

  AllSubFile.fromJson(int code, Map<String, dynamic> json) {
    statusCode = code;
    if (json['files'] != null) {
      files = <Files>[];
      json['files'].forEach((dynamic v) {
        files.add(Files.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (files != null) {
      data['files'] = files.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Files {
  String id;
  String name;
  String mimeType;
  String webViewLink;

  Files({this.id, this.name, this.mimeType, this.webViewLink});

  Files.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    mimeType = json['mimeType'];
    webViewLink = json['webViewLink'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['mimeType'] = mimeType;
    data['webViewLink'] = webViewLink;
    return data;
  }
}
