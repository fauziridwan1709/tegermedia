part of '_document.dart';

class DriveModel {
  int statusCode;
  String kind;
  String id;
  String name;
  String mimeType;

  DriveModel({this.statusCode, this.kind, this.id, this.name, this.mimeType});

  DriveModel.fromJson(int status, Map<String, dynamic> json) {
    statusCode = status;
    kind = json['kind'] as String;
    id = json['id'] as String;
    name = json['name'] as String;
    mimeType = json['mimeType'] as String;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['statusCode'] = statusCode;
    data['kind'] = kind;
    data['id'] = id;
    data['name'] = name;
    data['mimeType'] = mimeType;
    return data;
  }
}
