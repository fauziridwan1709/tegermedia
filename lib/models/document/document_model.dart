part of '_document.dart';

class DocumentModel {
  int schoolId;
  int classId;
  String name;
  String document;
  String type;
  String parent;

  DocumentModel({this.parent, this.schoolId, this.classId, this.name, this.document, this.type});

  DocumentModel.fromSnapshot(QueryDocumentSnapshot snapshot)
      : schoolId = (snapshot.data() as Map<dynamic, dynamic>)['school_id'],
        classId = (snapshot.data() as Map<dynamic, dynamic>)['class_id'],
        document = (snapshot.data() as Map<dynamic, dynamic>)['document_list'],

        ///ini
        name = (snapshot.data() as Map<dynamic, dynamic>)['name'],
        parent = (snapshot.data() as Map<dynamic, dynamic>)['parent'],

        ///ini
        type = (snapshot.data() as Map<dynamic, dynamic>)['type'];

//documentList = List.from(snapshot.data()['document_list']);
}
