// part of '../_models.dart';
//
// class GSheetSiswamediaAds {
//   String version;
//   String encoding;
//   AdsFeed feed;
//
//   GSheetSiswamediaAds({this.version, this.encoding, this.feed});
//
//   GSheetSiswamediaAds.fromJson(Map<String, dynamic> json) {
//     version = json['version'];
//     encoding = json['encoding'];
//     feed = json['feed'] != null ?  AdsFeed.fromJson(json['feed']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['version'] = this.version;
//     data['encoding'] = this.encoding;
//     if (this.feed != null) {
//       data['feed'] = this.feed.toJson();
//     }
//     return data;
//   }
// }
//
// class AdsFeed {
//   String xmlns;
//   String xmlnsOpenSearch;
//   String xmlnsGsx;
//   AdsId id;
//   AdsId updated;
//   List<AdsCategory> category;
//   AdsTitle title;
//   List<AdsLink> link;
//   List<AdsAuthor> author;
//   AdsId openSearchTotalResults;
//   AdsId openSearchStartIndex;
//   List<AdsEntry> entry;
//
//   AdsFeed(
//       {this.xmlns,
//       this.xmlnsOpenSearch,
//       this.xmlnsGsx,
//       this.id,
//       this.updated,
//       this.category,
//       this.title,
//       this.link,
//       this.author,
//       this.openSearchTotalResults,
//       this.openSearchStartIndex,
//       this.entry});
//
//   AdsFeed.fromJson(Map<String, dynamic> json) {
//     xmlns = json['xmlns'];
//     xmlnsOpenSearch = json['xmlns\$openSearch'];
//     xmlnsGsx = json['xmlns\$gsx'];
//     id = json['id'] != null ?  AdsId.fromJson(json['id']) : null;
//     updated =
//         json['updated'] != null ?  AdsId.fromJson(json['updated']) : null;
//     if (json['category'] != null) {
//       category =  List<AdsCategory>();
//       json['category'].forEach((v) {
//         category.add( AdsCategory.fromJson(v));
//       });
//     }
//     title = json['title'] != null ?  AdsTitle.fromJson(json['title']) : null;
//     if (json['link'] != null) {
//       link =  List<AdsLink>();
//       json['link'].forEach((v) {
//         link.add( AdsLink.fromJson(v));
//       });
//     }
//     if (json['author'] != null) {
//       author =  List<AdsAuthor>();
//       json['author'].forEach((v) {
//         author.add( AdsAuthor.fromJson(v));
//       });
//     }
//     openSearchTotalResults = json['openSearch\$totalResults'] != null
//         ?  AdsId.fromJson(json['openSearch\$totalResults'])
//         : null;
//     openSearchStartIndex = json['openSearch\$startIndex'] != null
//         ?  AdsId.fromJson(json['openSearch\$startIndex'])
//         : null;
//     if (json['entry'] != null) {
//       entry =  List<AdsEntry>();
//       json['entry'].forEach((v) {
//         entry.add( AdsEntry.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['xmlns'] = this.xmlns;
//     data['xmlns\$openSearch'] = this.xmlnsOpenSearch;
//     data['xmlns\$gsx'] = this.xmlnsGsx;
//     if (this.id != null) {
//       data['id'] = this.id.toJson();
//     }
//     if (this.updated != null) {
//       data['updated'] = this.updated.toJson();
//     }
//     if (this.category != null) {
//       data['category'] = this.category.map((v) => v.toJson()).toList();
//     }
//     if (this.title != null) {
//       data['title'] = this.title.toJson();
//     }
//     if (this.link != null) {
//       data['link'] = this.link.map((v) => v.toJson()).toList();
//     }
//     if (this.author != null) {
//       data['author'] = this.author.map((v) => v.toJson()).toList();
//     }
//     if (this.openSearchTotalResults != null) {
//       data['openSearch\$totalResults'] = this.openSearchTotalResults.toJson();
//     }
//     if (this.openSearchStartIndex != null) {
//       data['openSearch\$startIndex'] = this.openSearchStartIndex.toJson();
//     }
//     if (this.entry != null) {
//       data['entry'] = this.entry.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class AdsId {
//   String t;
//
//   AdsId({this.t});
//
//   AdsId.fromJson(Map<String, dynamic> json) {
//     t = json['\$t'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['\$t'] = this.t;
//     return data;
//   }
// }
//
// class AdsCategory {
//   String scheme;
//   String term;
//
//   AdsCategory({this.scheme, this.term});
//
//   AdsCategory.fromJson(Map<String, dynamic> json) {
//     scheme = json['scheme'];
//     term = json['term'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['scheme'] = this.scheme;
//     data['term'] = this.term;
//     return data;
//   }
// }
//
// class AdsTitle {
//   String type;
//   String t;
//
//   AdsTitle({this.type, this.t});
//
//   AdsTitle.fromJson(Map<String, dynamic> json) {
//     type = json['type'];
//     t = json['\$t'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['type'] = this.type;
//     data['\$t'] = this.t;
//     return data;
//   }
// }
//
// class AdsLink {
//   String rel;
//   String type;
//   String href;
//
//   AdsLink({this.rel, this.type, this.href});
//
//   AdsLink.fromJson(Map<String, dynamic> json) {
//     rel = json['rel'];
//     type = json['type'];
//     href = json['href'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['rel'] = this.rel;
//     data['type'] = this.type;
//     data['href'] = this.href;
//     return data;
//   }
// }
//
// class AdsAuthor {
//   AdsId name;
//   AdsId email;
//
//   AdsAuthor({this.name, this.email});
//
//   AdsAuthor.fromJson(Map<String, dynamic> json) {
//     name = json['name'] != null ?  AdsId.fromJson(json['name']) : null;
//     email = json['email'] != null ?  AdsId.fromJson(json['email']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     if (this.name != null) {
//       data['name'] = this.name.toJson();
//     }
//     if (this.email != null) {
//       data['email'] = this.email.toJson();
//     }
//     return data;
//   }
// }
//
// class AdsEntry {
//   AdsId id;
//   AdsId updated;
//   List<AdsCategory> category;
//   AdsTitle title;
//   AdsTitle content;
//   List<AdsLink> link;
//   AdsId gsxJudul;
//   AdsId gsxGambar;
//   AdsId gsxTanggal;
//   AdsId gsxPenerbit;
//   AdsId gsxStatus;
//   AdsId gsxLink;
//
//   AdsEntry(
//       {this.id,
//       this.updated,
//       this.category,
//       this.title,
//       this.content,
//       this.link,
//       this.gsxJudul,
//       this.gsxGambar,
//       this.gsxTanggal,
//       this.gsxPenerbit,
//       this.gsxStatus,
//       this.gsxLink});
//
//   AdsEntry.fromJson(Map<String, dynamic> json) {
//     id = json['id'] != null ?  AdsId.fromJson(json['id']) : null;
//     updated =
//         json['updated'] != null ?  AdsId.fromJson(json['updated']) : null;
//     if (json['category'] != null) {
//       category =  List<AdsCategory>();
//       json['category'].forEach((v) {
//         category.add( AdsCategory.fromJson(v));
//       });
//     }
//     title = json['title'] != null ?  AdsTitle.fromJson(json['title']) : null;
//     content =
//         json['content'] != null ?  AdsTitle.fromJson(json['content']) : null;
//     if (json['link'] != null) {
//       link =  List<AdsLink>();
//       json['link'].forEach((v) {
//         link.add( AdsLink.fromJson(v));
//       });
//     }
//     gsxJudul = json['gsx\$judul'] != null
//         ?  AdsId.fromJson(json['gsx\$judul'])
//         : null;
//     gsxGambar = json['gsx\$gambar'] != null
//         ?  AdsId.fromJson(json['gsx\$gambar'])
//         : null;
//     gsxTanggal = json['gsx\$tanggal'] != null
//         ?  AdsId.fromJson(json['gsx\$tanggal'])
//         : null;
//     gsxPenerbit = json['gsx\$penerbit'] != null
//         ?  AdsId.fromJson(json['gsx\$penerbit'])
//         : null;
//     gsxStatus = json['gsx\$status'] != null
//         ?  AdsId.fromJson(json['gsx\$status'])
//         : null;
//     gsxLink = json['gsx\$link'] != null
//         ?  AdsId.fromJson(json['gsx\$link'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     if (this.id != null) {
//       data['id'] = this.id.toJson();
//     }
//     if (this.updated != null) {
//       data['updated'] = this.updated.toJson();
//     }
//     if (this.category != null) {
//       data['category'] = this.category.map((v) => v.toJson()).toList();
//     }
//     if (this.title != null) {
//       data['title'] = this.title.toJson();
//     }
//     if (this.content != null) {
//       data['content'] = this.content.toJson();
//     }
//     if (this.link != null) {
//       data['link'] = this.link.map((v) => v.toJson()).toList();
//     }
//     if (this.gsxJudul != null) {
//       data['gsx\$judul'] = this.gsxJudul.toJson();
//     }
//     if (this.gsxGambar != null) {
//       data['gsx\$gambar'] = this.gsxGambar.toJson();
//     }
//     if (this.gsxTanggal != null) {
//       data['gsx\$tanggal'] = this.gsxTanggal.toJson();
//     }
//     if (this.gsxPenerbit != null) {
//       data['gsx\$penerbit'] = this.gsxPenerbit.toJson();
//     }
//     if (this.gsxStatus != null) {
//       data['gsx\$status'] = this.gsxStatus.toJson();
//     }
//     if (this.gsxLink != null) {
//       data['gsx\$link'] = this.gsxLink.toJson();
//     }
//     return data;
//   }
// }
