part of '_assignment.dart';

class ModelCreateAssignmentV2 {
  String _name;
  String _description;
  String _startDate;
  String _endDate;
  List<Attachments> _attachments;
  List<ListCourseClassId> _listCourseClassId;

  ModelCreateAssignmentV2(
      {String name,
      String description,
      String startDate,
      String endDate,
      List<Attachments> attachments,
      List<ListCourseClassId> listCourseClassId}) {
    _name = name;
    _description = description;
    _startDate = startDate;
    _endDate = endDate;
    _attachments = attachments;
    _listCourseClassId = listCourseClassId;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get description => _description;
  set description(String description) => _description = description;
  String get startDate => _startDate;
  set startDate(String startDate) => _startDate = startDate;
  String get endDate => _endDate;
  set endDate(String endDate) => _endDate = endDate;
  List<Attachments> get attachments => _attachments;
  set attachments(List<Attachments> attachments) => _attachments = attachments;
  List<ListCourseClassId> get listCourseClassId => _listCourseClassId;
  set listCourseClassId(List<ListCourseClassId> listCourseClassId) =>
      _listCourseClassId = listCourseClassId;

  ModelCreateAssignmentV2.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _description = json['description'];
    _startDate = json['start_date'];
    _endDate = json['end_date'];
    if (json['attachments'] != null) {
      _attachments = <Attachments>[];
      json['attachments'].forEach((dynamic v) {
        _attachments.add(Attachments.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['list_course_class_id'] != null) {
      _listCourseClassId = <ListCourseClassId>[];
      json['list_course_class_id'].forEach((dynamic v) {
        _listCourseClassId
            .add(ListCourseClassId.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = _name;
    data['description'] = _description;
    data['start_date'] = _startDate;
    data['end_date'] = _endDate;
    if (_attachments != null) {
      data['attachments'] = _attachments.map((v) => v.toJson()).toList();
    }
    if (_listCourseClassId != null) {
      data['list_course_class_id'] =
          _listCourseClassId.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListCourseClassId {
  int _classId;
  int _courseId;

  ListCourseClassId({int classId, int courseId}) {
    _classId = classId;
    _courseId = courseId;
  }

  int get classId => _classId;
  set classId(int classId) => _classId = classId;
  int get courseId => _courseId;
  set courseId(int courseId) => _courseId = courseId;

  ListCourseClassId.fromJson(Map<String, dynamic> json) {
    _classId = json['class_id'];
    _courseId = json['course_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = _classId;
    data['course_id'] = _courseId;
    return data;
  }
}
