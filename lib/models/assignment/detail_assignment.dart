part of '_assignment.dart';

class ModelDetailAssessments {
  int statusCode;
  String message;
  DataDetailAssessments data;

  ModelDetailAssessments({this.statusCode, this.message, this.data});

  ModelDetailAssessments.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null
        ? DataDetailAssessments.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataDetailAssessments {
  int id;
  int userId;
  String fullName;
  String imageUser;
  bool newComment;
  String email;
  String status;
  int assessmentId;
  String nilai;
  String name;
  String className;
  String description;
  String mainDescription;
  List<MainAttachments> attachments;
  List<MainAttachments> mainAttachments;
  List<ReplyAttachments> replyAttachments;
  List<CommentsAssignment> comments;
  String createdAt;
  String updatedAt;

  DataDetailAssessments(
      {this.id,
      this.userId,
      this.fullName,
      this.imageUser,
      this.newComment,
      this.email,
      this.status,
      this.assessmentId,
      this.nilai,
      this.name,
      this.className,
      this.description,
      this.mainDescription,
      this.attachments,
      this.mainAttachments,
      this.replyAttachments,
      this.comments,
      this.createdAt,
      this.updatedAt});

  DataDetailAssessments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    fullName = json['full_name'];
    imageUser = json['image_user'];
    newComment = json['new_comment'];
    email = json['email'];
    status = json['status'];
    assessmentId = json['assessment_id'];
    nilai = json['nilai'];
    name = json['name'];
    className = json['class_name'];
    description = json['description'];
    mainDescription = json['main_description'];
    if (json['attachments'] != null) {
      attachments = <MainAttachments>[];
      json['attachments'].forEach((dynamic v) {
        attachments.add(MainAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['main_attachments'] != null) {
      mainAttachments = <MainAttachments>[];
      json['main_attachments'].forEach((dynamic v) {
        mainAttachments
            .add(MainAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['reply_attachments'] != null) {
      replyAttachments = <ReplyAttachments>[];
      json['reply_attachments'].forEach((dynamic v) {
        replyAttachments
            .add(ReplyAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['comments'] != null) {
      comments = <CommentsAssignment>[];
      json['comments'].forEach((dynamic v) {
        comments.add(CommentsAssignment.fromJson(v as Map<String, dynamic>));
      });
    }
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['full_name'] = fullName;
    data['image_user'] = imageUser;
    data['new_comment'] = newComment;
    data['email'] = email;
    data['status'] = status;
    data['assessment_id'] = assessmentId;
    data['nilai'] = nilai;
    data['name'] = name;
    data['class_name'] = className;
    data['description'] = description;
    data['main_description'] = mainDescription;
    if (attachments != null) {
      data['attachments'] = attachments.map((v) => v.toJson()).toList();
    }
    if (mainAttachments != null) {
      data['main_attachments'] =
          mainAttachments.map((v) => v.toJson()).toList();
    }
    if (this.replyAttachments != null) {
      data['reply_attachments'] =
          this.replyAttachments.map((v) => v.toJson()).toList();
    }
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class ReplyAttachments {
  int id;
  int assessmentReferenceId;
  String fileName;
  String file;

  ReplyAttachments(
      {this.id, this.assessmentReferenceId, this.fileName, this.file});

  ReplyAttachments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    assessmentReferenceId = json['assessment_reference_id'];
    fileName = json['file_name'];
    file = json['file'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['assessment_reference_id'] = this.assessmentReferenceId;
    data['file_name'] = this.fileName;
    data['file'] = this.file;
    return data;
  }
}

class CommentsAssignment {
  int commentId;
  String comment;
  String sentDate;
  int senderUserId;
  String senderName;

  CommentsAssignment(
      {this.commentId,
      this.comment,
      this.sentDate,
      this.senderUserId,
      this.senderName});

  CommentsAssignment.fromJson(Map<String, dynamic> json) {
    commentId = json['comment_id'];
    comment = json['comment'];
    sentDate = json['sent_date'];
    senderUserId = json['sender_user_id'];
    senderName = json['sender_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['comment_id'] = this.commentId;
    data['comment'] = this.comment;
    data['sent_date'] = this.sentDate;
    data['sender_user_id'] = this.senderUserId;
    data['sender_name'] = this.senderName;
    return data;
  }
}

class MainAttachments {
  String fileName;
  String file;

  MainAttachments({this.fileName, this.file});

  MainAttachments.fromJson(Map<String, dynamic> json) {
    fileName = json['file_name'];
    file = json['file'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['file_name'] = fileName;
    data['file'] = file;
    return data;
  }
}
