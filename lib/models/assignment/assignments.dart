part of '_assignment.dart';

class ModelResultCreateAssignment {
  int _statusCode;
  String _message;
  Id _data;

  ModelResultCreateAssignment({int statusCode, String message, Id data}) {
    _statusCode = statusCode;
    _message = message;
    _data = data;
  }

  int get statusCode => _statusCode;
  set statusCode(int statusCode) => _statusCode = statusCode;
  String get message => _message;
  set message(String message) => _message = message;
  Id get data => _data;
  set data(Id data) => _data = data;

  ModelResultCreateAssignment.fromJson(Map<String, dynamic> json) {
    _statusCode = json['status_code'];
    _message = json['message'];
    _data = json['data'] != null ? Id.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = _statusCode;
    data['message'] = _message;
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    return data;
  }
}

class Id {
  int id;

  Id({this.id});

  Id.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    return data;
  }
}

class ModelAssignments {
  List<DataAssignmentReference> data;
  String message;
  int statusCode;

  ModelAssignments({this.statusCode, this.message, this.data});

  ModelAssignments.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'] as int;
    message = json['message'] as String;
    if (json['data'] != null) {
      data = <DataAssignmentReference>[];
      json['data'].forEach((dynamic v) {
        data.add(DataAssignmentReference.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ResultModelTugasRefrence {
  int statusCode;
  String message;
  DataModelTugasRefrence data;

  ResultModelTugasRefrence({this.statusCode, this.message, this.data});

  ResultModelTugasRefrence.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'] as int;
    message = json['message'] as String;
    data = json['data'] != null
        ? DataModelTugasRefrence.fromJson(json['data'] as Map<String, dynamic>)
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataModelTugasRefrence {
  int id;
  int userId;
  String fullName;
  String email;
  String status;
  int assessmentId;
  String name;
  String description;
  List<MainAttachments> attachments;
  List<MainAttachments> mainAttachments;

  String createdAt;
  String updatedAt;

  DataModelTugasRefrence(
      {this.id,
      this.userId,
      this.fullName = '',
      this.email = '',
      this.status = '',
      this.assessmentId,
      this.name = '',
      this.description = '',
      this.attachments,
      this.mainAttachments,
      this.createdAt,
      this.updatedAt});

  DataModelTugasRefrence.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    userId = json['user_id'] as int;
    fullName = json['full_name'] as String;
    email = json['email'] as String;
    status = json['status'] as String;
    assessmentId = json['assessment_id'] as int;
    name = json['name'] as String;
    description = json['description'] as String;
    if (json['attachments'] != null) {
      attachments = <MainAttachments>[];
      json['attachments'].forEach((dynamic v) {
        attachments.add(MainAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['main_attachments'] != null) {
      mainAttachments = <MainAttachments>[];
      json['main_attachments'].forEach((dynamic v) {
        mainAttachments
            .add(MainAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['full_name'] = fullName;
    data['email'] = email;
    data['status'] = status;
    data['assessment_id'] = assessmentId;
    data['name'] = name;
    data['description'] = description;
    if (attachments != null) {
      data['attachments'] = attachments.map((v) => v.toJson()).toList();
    }
    if (mainAttachments = null) {
      data['main_attachments'] =
          mainAttachments.map((v) => v.toJson()).toList();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class ModelRefrenceAssessmentsStudens {
  String description;
  String status;
  List<MainAttachments> attachments;

  ModelRefrenceAssessmentsStudens(
      {this.description, this.status, this.attachments});

  ModelRefrenceAssessmentsStudens.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    status = json['status'];
    if (json['attachments'] != null) {
      attachments = <MainAttachments>[];
      json['attachments'].forEach((dynamic v) {
        attachments.add(MainAttachments.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['description'] = description;
    data['status'] = status;
    if (attachments != null) {
      data['attachments'] = attachments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ModelMeAssesmentsStatitistic {
  int statusCode;
  String message;
  DetailMeAssesmentsStatitistic data;

  ModelMeAssesmentsStatitistic({this.statusCode, this.message, this.data});

  ModelMeAssesmentsStatitistic.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null
        ? DetailMeAssesmentsStatitistic.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DetailMeAssesmentsStatitistic {
  bool isAssessments;
  int assessmentId;
  String updatedAt;
  int totalAssessment;
  int totalSudahDikerjakan;

  DetailMeAssesmentsStatitistic(
      {this.isAssessments,
      this.assessmentId,
      this.updatedAt,
      this.totalAssessment,
      this.totalSudahDikerjakan});

  DetailMeAssesmentsStatitistic.fromJson(Map<String, dynamic> json) {
    isAssessments = json['is_assessments'];
    assessmentId = json['assessment_id'];
    updatedAt = json['updated_at'];
    totalAssessment = json['total_assessment'];
    totalSudahDikerjakan = json['total_sudah_dikerjakan'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['is_assessments'] = isAssessments;
    data['assessment_id'] = assessmentId;
    data['updated_at'] = updatedAt;
    data['total_assessment'] = totalAssessment;
    data['total_sudah_dikerjakan'] = totalSudahDikerjakan;
    return data;
  }
}

class ModelAssessmentsTeacherById {
  int _statusCode;
  String _message;
  DataAssessmentsTeacherById _data;

  ModelAssessmentsTeacherById(
      {int statusCode, String message, DataAssessmentsTeacherById data}) {
    _statusCode = statusCode;
    _message = message;
    _data = data;
  }

  int get statusCode => _statusCode;
  set statusCode(int statusCode) => _statusCode = statusCode;
  String get message => _message;
  set message(String message) => _message = message;
  DataAssessmentsTeacherById get data => _data;
  set data(DataAssessmentsTeacherById data) => _data = data;

  ModelAssessmentsTeacherById.fromJson(Map<String, dynamic> json) {
    _statusCode = json['status_code'];
    _message = json['message'];
    _data = json['data'] != null
        ? DataAssessmentsTeacherById.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = _statusCode;
    data['message'] = _message;
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    return data;
  }
}

class DataAssessmentsTeacherById {
  int _id;
  int _classId;
  String _className;
  int _courseId;
  int _userId;
  String _fullName;
  String _email;
  String _image;
  String _courseName;
  String _name;
  String _description;
  String _status;
  String _startDate;
  String _endDate;
  List<Attachments> _attachments;
  String _createdAt;
  String _updatedAt;
  List<ListClassCourse> _listClassCourse;

  DataAssessmentsTeacherById(
      {int id,
      int classId,
      String className,
      int courseId,
      int userId,
      String fullName,
      String email,
      String image,
      String courseName,
      String name,
      String description,
      String status,
      String startDate,
      String endDate,
      List<Attachments> attachments,
      String createdAt,
      String updatedAt,
      List<ListClassCourse> listClassCourse}) {
    _id = id;
    _classId = classId;
    _className = className;
    _courseId = courseId;
    _userId = userId;
    _fullName = fullName;
    _email = email;
    _image = image;
    _courseName = courseName;
    _name = name;
    _description = description;
    _status = status;
    _startDate = startDate;
    _endDate = endDate;
    _attachments = attachments;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _listClassCourse = listClassCourse;
  }

  int get id => _id;
  set id(int id) => _id = id;
  int get classId => _classId;
  set classId(int classId) => _classId = classId;
  String get className => _className;
  set className(String className) => _className = className;
  int get courseId => _courseId;
  set courseId(int courseId) => _courseId = courseId;
  int get userId => _userId;
  set userId(int userId) => _userId = userId;
  String get fullName => _fullName;
  set fullName(String fullName) => _fullName = fullName;
  String get email => _email;
  set email(String email) => _email = email;
  String get image => _image;
  set image(String image) => _image = image;
  String get courseName => _courseName;
  set courseName(String courseName) => _courseName = courseName;
  String get name => _name;
  set name(String name) => _name = name;
  String get description => _description;
  set description(String description) => _description = description;
  String get status => _status;
  set status(String status) => _status = status;
  String get startDate => _startDate;
  set startDate(String startDate) => _startDate = startDate;
  String get endDate => _endDate;
  set endDate(String endDate) => _endDate = endDate;
  List<Attachments> get attachments => _attachments;
  set attachments(List<Attachments> attachments) => _attachments = attachments;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  List<ListClassCourse> get listClassCourse => _listClassCourse;
  set listClassCourse(List<ListClassCourse> listClassCourse) =>
      _listClassCourse = listClassCourse;

  DataAssessmentsTeacherById.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _classId = json['class_id'];
    _className = json['class_name'];
    _courseId = json['course_id'];
    _userId = json['user_id'];
    _fullName = json['full_name'];
    _email = json['email'];
    _image = json['image'];
    _courseName = json['course_name'];
    _name = json['name'];
    _description = json['description'];
    _status = json['status'];
    _startDate = json['start_date'];
    _endDate = json['end_date'];
    if (json['attachments'] != null) {
      _attachments = <Attachments>[];
      json['attachments'].forEach((dynamic v) {
        _attachments.add(Attachments.fromJson(v as Map<String, dynamic>));
      });
    }
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    if (json['list_class_course'] != null) {
      _listClassCourse = <ListClassCourse>[];
      json['list_class_course'].forEach((dynamic v) {
        _listClassCourse
            .add(ListClassCourse.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = _id;
    data['class_id'] = _classId;
    data['class_name'] = _className;
    data['course_id'] = _courseId;
    data['user_id'] = _userId;
    data['full_name'] = _fullName;
    data['email'] = _email;
    data['image'] = _image;
    data['course_name'] = _courseName;
    data['name'] = _name;
    data['description'] = _description;
    data['status'] = _status;
    data['start_date'] = _startDate;
    data['end_date'] = _endDate;
    if (_attachments != null) {
      data['attachments'] = _attachments.map((v) => v.toJson()).toList();
    }
    data['created_at'] = _createdAt;
    data['updated_at'] = _updatedAt;
    if (_listClassCourse != null) {
      data['list_class_course'] =
          _listClassCourse.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Attachments {
  int _id;
  int _assessmentId;
  String _fileName;
  String _file;

  Attachments({int id, int assessmentId, String fileName, String file}) {
    _id = id;
    _assessmentId = assessmentId;
    _fileName = fileName;
    _file = file;
  }

  int get id => _id;
  set id(int id) => _id = id;
  int get assessmentId => _assessmentId;
  set assessmentId(int assessmentId) => _assessmentId = assessmentId;
  String get fileName => _fileName;
  set fileName(String fileName) => _fileName = fileName;
  String get file => _file;
  set file(String file) => _file = file;

  Attachments.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _assessmentId = json['assessment_id'];
    _fileName = json['file_name'];
    _file = json['file'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = _id;
    data['assessment_id'] = _assessmentId;
    data['file_name'] = _fileName;
    data['file'] = _file;
    return data;
  }
}

class ListClassCourse {
  int _classId;
  int _courseId;
  int _assessmentRelationId;
  String _courseName;
  String _className;
  String _year;
  String _semester;

  ListClassCourse(
      {int classId,
      int courseId,
      int assessmentRelationId,
      String courseName,
      String className,
      String year,
      String semester}) {
    _classId = classId;
    _courseId = courseId;
    _assessmentRelationId = assessmentRelationId;
    _courseName = courseName;
    _className = className;
    _year = year;
    _semester = semester;
  }

  int get classId => _classId;
  set classId(int classId) => _classId = classId;
  int get courseId => _courseId;
  set courseId(int courseId) => _courseId = courseId;
  int get assessmentRelationId => _assessmentRelationId;
  set assessmentRelationId(int assessmentRelationId) =>
      _assessmentRelationId = assessmentRelationId;
  String get courseName => _courseName;
  set courseName(String courseName) => _courseName = courseName;
  String get className => _className;
  set className(String className) => _className = className;
  String get year => _year;
  set year(String year) => _year = year;
  String get semester => _semester;
  set semester(String semester) => _semester = semester;

  ListClassCourse.fromJson(Map<String, dynamic> json) {
    _classId = json['class_id'];
    _courseId = json['course_id'];
    _assessmentRelationId = json['assessment_relation_id'];
    _courseName = json['course_name'];
    _className = json['class_name'];
    _year = json['year'];
    _semester = json['semester'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = _classId;
    data['course_id'] = _courseId;
    data['assessment_relation_id'] = _assessmentRelationId;
    data['course_name'] = _courseName;
    data['class_name'] = _className;
    data['year'] = _year;
    data['semester'] = _semester;
    return data;
  }
}
