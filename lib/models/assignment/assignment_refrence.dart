part of '_assignment.dart';

class ModelAssignmentReference {
  int statusCode;
  String message;
  List<DataAssignmentReference> data;

  ModelAssignmentReference({this.statusCode, this.message, this.data});

  ModelAssignmentReference.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataAssignmentReference>[];
      json['data'].forEach((dynamic v) {
        data.add(DataAssignmentReference.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataAssignmentReference {
  int id;
  int classId;
  String className;
  int courseId;
  int userId;
  String nilai;
  String fullName;
  String email;
  String imageUser;
  String courseName;
  String name;
  String description;
  String status;
  String startDate;
  String endDate;
  String createdAt;
  String updatedAt;
  String mainDescription;
  int assessmentId;

  DataAssignmentReference({
    this.id,
    this.classId,
    this.className,
    this.courseId,
    this.userId,
    this.fullName,
    this.nilai,
    this.email,
    this.imageUser,
    this.courseName,
    this.name,
    this.description,
    this.status,
    this.startDate,
    this.endDate,
    this.createdAt,
    this.updatedAt,
    this.assessmentId,
    this.mainDescription,
  });

  DataAssignmentReference.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    userId = json['user_id'];
    nilai = json['nilai'].toString();
    fullName = json['full_name'];
    email = json['email'];
    imageUser = json['image_user'];
    courseName = json['course_name'];
    name = json['name'];
    description = json['description'];
    status = json['status'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    assessmentId = json['assessment_id'];
    mainDescription = json['main_description'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['class_id'] = classId;
    data['class_name'] = className;
    data['course_id'] = courseId;
    data['user_id'] = userId;
    data['full_name'] = fullName;
    data['nilai'] = nilai;
    data['email'] = email;
    data['image'] = imageUser;
    data['course_name'] = courseName;
    data['name'] = name;
    data['description'] = description;
    data['status'] = status;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['assessment_id'] = assessmentId;
    data['main_description'] = mainDescription;
    return data;
  }
}

class ModelAssignmentsNew {
  int statusCode;
  DataAssignmentNew data;

  ModelAssignmentsNew({this.statusCode, this.data});

  ModelAssignmentsNew.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = json['data'] != null
        ? new DataAssignmentNew.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataAssignmentNew {
  List<ListData> listData;
  int totalData;

  DataAssignmentNew({this.listData, this.totalData});

  DataAssignmentNew.fromJson(Map<String, dynamic> json) {
    if (json['list_data'] != null) {
      listData = new List<ListData>();
      json['list_data'].forEach((dynamic v) {
        listData.add(new ListData.fromJson(v as Map<String, dynamic>));
      });
    }
    totalData = json['total_data'] ?? json['total_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.listData != null) {
      data['list_data'] = this.listData.map((v) => v.toJson()).toList();
    }
    data['total_data'] = this.totalData;
    return data;
  }
}

class ListData {
  int id;
  int assessmentId;
  bool newComment;
  int classId;
  String className;
  int courseId;
  int userId;
  String fullName;
  String email;
  String image;
  String courseName;
  String name;
  String description;
  String status;
  String startDate;
  String endDate;
  String createdAt;
  String updatedAt;
  List<ListClassCourse> listClassCourse;

  ListData(
      {this.id,
      this.assessmentId,
      this.newComment,
      this.classId,
      this.className,
      this.courseId,
      this.userId,
      this.fullName,
      this.email,
      this.image,
      this.courseName,
      this.name,
      this.description,
      this.status,
      this.startDate,
      this.endDate,
      this.createdAt,
      this.updatedAt,
      this.listClassCourse});

  ListData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    assessmentId = json['assessment_id'];
    newComment = json['new_comment'];
    classId = json['class_id'];
    className = json['class_name'];
    courseId = json['course_id'];
    userId = json['user_id'];
    fullName = json['full_name'];
    email = json['email'];
    image = json['image'];
    courseName = json['course_name'];
    name = json['name'];
    description = json['description'];
    status = DateTime.parse(json['end_date']).isBefore(DateTime.now())
        ? 'Selesai'
        : json['status'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['list_class_course'] != null) {
      listClassCourse = new List<ListClassCourse>();
      json['list_class_course'].forEach((dynamic v) {
        listClassCourse
            .add(new ListClassCourse.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['assessment_id'] = this.assessmentId;
    data['new_comment'] = this.newComment;
    data['class_id'] = this.classId;
    data['class_name'] = this.className;
    data['course_id'] = this.courseId;
    data['user_id'] = this.userId;
    data['full_name'] = this.fullName;
    data['email'] = this.email;
    data['image'] = this.image;
    data['course_name'] = this.courseName;
    data['name'] = this.name;
    data['description'] = this.description;
    data['status'] = this.status;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.listClassCourse != null) {
      data['list_class_course'] =
          this.listClassCourse.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
