part of '_assignment.dart';

class ModelScoreAssessments {
  int statusCode;
  String message;

  ModelScoreAssessments({this.statusCode, this.message});

  ModelScoreAssessments.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    return data;
  }
}
