part of '_models.dart';

class ModelTermAndConditions {
  List<DataTermAndConditions> rows;

  ModelTermAndConditions({this.rows});

  ModelTermAndConditions.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <DataTermAndConditions>[];
      json['rows'].forEach((dynamic v) {
        rows.add(DataTermAndConditions.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataTermAndConditions {
  String isi;
  String status;
  String subtopik;
  String topik;

  DataTermAndConditions({this.isi, this.status, this.subtopik, this.topik});

  DataTermAndConditions.fromJson(Map<String, dynamic> json) {
    isi = json['isi'].toString();
    status = json['status'].toString();
    subtopik = json['subtopik'].toString();
    topik = json['topik'].toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['isi'] = isi;
    data['status'] = status;
    data['subtopik'] = subtopik;
    data['topik'] = topik;
    return data;
  }
}
