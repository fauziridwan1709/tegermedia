part of '_conference.dart';

class ConferenceModel {
  int statusCode;
  List<ConferenceDetail> data;

  ConferenceModel({this.statusCode, this.data});

  ConferenceModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'] as int;
    if (json['data'] != null) {
      data = <ConferenceDetail>[];
      json['data'].forEach((dynamic v) {
        data.add(ConferenceDetail.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ConferenceDetail {
  int id;
  String title;
  String description;
  String date;
  int duration;
  int classId;
  String url;
  int courseId;

  ConferenceDetail(
      {this.id,
      this.title,
      this.date,
      this.duration,
      this.classId,
      this.description,
      this.url,
      this.courseId});

  ConferenceDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    title = json['title'] as String;
    description = json['description'] as String;
    date = json['date_start'];
    duration = json['duration'];
    classId = json['class_id'];
    url = json['url'];
    courseId = json['course_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['class_id'] = classId;
    data['title'] = title;
    data['description'] = description;
    data['date_start'] = date;
    data['duration'] = duration;
    data['url'] = url;
    data['course_id'] = courseId;
    return data;
  }
}

class ConferenceListModel {
  int statusCode;
  String message;
  List<ConferenceListModelData> data;

  ConferenceListModel({this.statusCode, this.message, this.data});

  ConferenceListModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <ConferenceListModelData>[];
      json['data'].forEach((dynamic v) {
        data.add(ConferenceListModelData.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ConferenceListModelData {
  int id;
  String title;
  String description;
  String dateStart;
  int duration;
  int classId;
  String creatorName;
  String url;
  bool isCreator;
  String createdAt;
  int courseId;

  ConferenceListModelData(
      {this.id,
      this.description,
      this.title,
      this.dateStart,
      this.duration,
      this.classId,
      this.creatorName,
      this.url,
      this.isCreator,
      this.createdAt,
      this.courseId});

  ConferenceListModelData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['description'];
    title = json['title'];
    dateStart = json['date_start'];
    duration = json['duration'];
    classId = json['class_id'];
    creatorName = json['creator_name'];
    url = json['url'];
    isCreator = json['is_creator'];
    createdAt = json['created_at'];
    courseId = json['course_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['description'] = description;
    data['title'] = title;
    data['date_start'] = dateStart;
    data['duration'] = duration;
    data['class_id'] = classId;
    data['creator_name'] = creatorName;
    data['url'] = url;
    data['is_creator'] = isCreator;
    data['created_at'] = createdAt;
    data['course_id'] = courseId;
    return data;
  }
}
