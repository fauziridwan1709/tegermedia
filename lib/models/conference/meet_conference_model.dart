part of '_conference.dart';

class MeetConference {
  int statusCode;
  String kind;
  String etag;
  String id;
  String status;
  String htmlLink;
  String created;
  String updated;
  String summary;
  String description;
  Creator creator;
  Creator organizer;
  Start start;
  Start end;
  String iCalUID;
  int sequence;
  String hangoutLink;
  ConferenceData conferenceData;
  Reminders reminders;

  MeetConference(
      {this.kind,
      this.etag,
      this.id,
      this.status,
      this.htmlLink,
      this.created,
      this.updated,
      this.summary,
      this.description,
      this.creator,
      this.organizer,
      this.start,
      this.end,
      this.iCalUID,
      this.sequence,
      this.hangoutLink,
      this.conferenceData,
      this.reminders,
      this.statusCode});

  MeetConference.fromJson(int statusCoded, Map<String, dynamic> json) {
    statusCode = statusCoded;
    kind = json['kind'];
    etag = json['etag'];
    id = json['id'];
    status = json['status'];
    htmlLink = json['htmlLink'];
    created = json['created'];
    updated = json['updated'];
    summary = json['summary'];
    description = json['description'];
    creator =
        json['creator'] != null ? Creator.fromJson(json['creator']) : null;
    organizer =
        json['organizer'] != null ? Creator.fromJson(json['organizer']) : null;
    start = json['start'] != null ? Start.fromJson(json['start']) : null;
    end = json['end'] != null ? Start.fromJson(json['end']) : null;
    iCalUID = json['iCalUID'];
    sequence = json['sequence'];
    hangoutLink = json['hangoutLink'];
    conferenceData = json['conferenceData'] != null
        ? ConferenceData.fromJson(json['conferenceData'])
        : null;
    reminders = json['reminders'] != null
        ? Reminders.fromJson(json['reminders'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['kind'] = kind;
    data['etag'] = etag;
    data['id'] = id;
    data['status'] = status;
    data['htmlLink'] = htmlLink;
    data['created'] = created;
    data['updated'] = updated;
    data['summary'] = summary;
    data['description'] = description;
    if (creator != null) {
      data['creator'] = creator.toJson();
    }
    if (organizer != null) {
      data['organizer'] = organizer.toJson();
    }
    if (start != null) {
      data['start'] = start.toJson();
    }
    if (end != null) {
      data['end'] = end.toJson();
    }
    data['iCalUID'] = iCalUID;
    data['sequence'] = sequence;
    data['hangoutLink'] = hangoutLink;
    if (conferenceData != null) {
      data['conferenceData'] = conferenceData.toJson();
    }
    if (reminders != null) {
      data['reminders'] = reminders.toJson();
    }
    return data;
  }
}

class Creator {
  String email;
  bool self;

  Creator({this.email, this.self});

  Creator.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    self = json['self'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['self'] = self;
    return data;
  }
}

class Start {
  String dateTime;

  Start({this.dateTime});

  Start.fromJson(Map<String, dynamic> json) {
    dateTime = json['dateTime'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['dateTime'] = dateTime;
    return data;
  }
}

class ConferenceData {
  CreateRequest createRequest;
  List<EntryPoints> entryPoints;
  ConferenceSolution conferenceSolution;
  String conferenceId;
  String signature;

  ConferenceData(
      {this.createRequest,
      this.entryPoints,
      this.conferenceSolution,
      this.conferenceId,
      this.signature});

  ConferenceData.fromJson(Map<String, dynamic> json) {
    createRequest = json['createRequest'] != null
        ? CreateRequest.fromJson(json['createRequest'])
        : null;
    if (json['entryPoints'] != null) {
      entryPoints = <EntryPoints>[];
      json['entryPoints'].forEach((dynamic v) {
        entryPoints.add(EntryPoints.fromJson(v));
      });
    }
    conferenceSolution = json['conferenceSolution'] != null
        ? ConferenceSolution.fromJson(json['conferenceSolution'])
        : null;
    conferenceId = json['conferenceId'];
    signature = json['signature'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (createRequest != null) {
      data['createRequest'] = createRequest.toJson();
    }
    if (entryPoints != null) {
      data['entryPoints'] = entryPoints.map((v) => v.toJson()).toList();
    }
    if (conferenceSolution != null) {
      data['conferenceSolution'] = conferenceSolution.toJson();
    }
    data['conferenceId'] = conferenceId;
    data['signature'] = signature;
    return data;
  }
}

class CreateRequest {
  String requestId;
  ConferenceSolutionKey conferenceSolutionKey;
  Status status;

  CreateRequest({this.requestId, this.conferenceSolutionKey, this.status});

  CreateRequest.fromJson(Map<String, dynamic> json) {
    requestId = json['requestId'];
    conferenceSolutionKey = json['conferenceSolutionKey'] != null
        ? ConferenceSolutionKey.fromJson(json['conferenceSolutionKey'])
        : null;
    status = json['status'] != null ? Status.fromJson(json['status']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['requestId'] = requestId;
    if (conferenceSolutionKey != null) {
      data['conferenceSolutionKey'] = conferenceSolutionKey.toJson();
    }
    if (status != null) {
      data['status'] = status.toJson();
    }
    return data;
  }
}

class ConferenceSolutionKey {
  String type;

  ConferenceSolutionKey({this.type});

  ConferenceSolutionKey.fromJson(Map<String, dynamic> json) {
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['type'] = type;
    return data;
  }
}

class Status {
  String statusCode;

  Status({this.statusCode});

  Status.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['statusCode'] = statusCode;
    return data;
  }
}

class EntryPoints {
  String entryPointType;
  String uri;
  String label;

  EntryPoints({this.entryPointType, this.uri, this.label});

  EntryPoints.fromJson(Map<String, dynamic> json) {
    entryPointType = json['entryPointType'];
    uri = json['uri'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['entryPointType'] = entryPointType;
    data['uri'] = uri;
    data['label'] = label;
    return data;
  }
}

class ConferenceSolution {
  ConferenceSolutionKey key;
  String name;
  String iconUri;

  ConferenceSolution({this.key, this.name, this.iconUri});

  ConferenceSolution.fromJson(Map<String, dynamic> json) {
    key = json['key'] != null
        ? ConferenceSolutionKey.fromJson(json['key'])
        : null;
    name = json['name'];
    iconUri = json['iconUri'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (key != null) {
      data['key'] = key.toJson();
    }
    data['name'] = name;
    data['iconUri'] = iconUri;
    return data;
  }
}

class Reminders {
  bool useDefault;

  Reminders({this.useDefault});

  Reminders.fromJson(Map<String, dynamic> json) {
    useDefault = json['useDefault'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['useDefault'] = useDefault;
    return data;
  }
}
