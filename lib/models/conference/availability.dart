part of '_conference.dart';

class Availability {
  final String status;
  final bool isAvailable;
  Availability({this.status, this.isAvailable});
}
