part of '../_models.dart';

class ModelNotification {
  int statusCode;
  String message;
  List<DataNotification> data;

  ModelNotification({this.statusCode, this.message, this.data});

  ModelNotification.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataNotification>[];
      json['data'].forEach((dynamic v) {
        data.add(DataNotification.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataNotification {
  int id;
  int userId;
  String title;
  String body;
  String type;
  String goto;
  bool isRead;
  String createdAt;
  String updatedAt;

  DataNotification(
      {this.id,
      this.userId,
      this.title,
      this.body,
      this.type,
      this.goto,
      this.isRead,
      this.createdAt,
      this.updatedAt});

  DataNotification.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    title = json['title'];
    body = json['body'];
    type = json['type'];
    goto = json['goto'];
    isRead = json['is_read'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['title'] = title;
    data['body'] = body;
    data['type'] = type;
    data['goto'] = goto;
    data['is_read'] = isRead;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class ModelCountNotif {
  int statusCode;
  String message;
  DataCountNotif data;

  ModelCountNotif({this.statusCode, this.message, this.data});

  ModelCountNotif.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? DataCountNotif.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataCountNotif {
  int count;

  DataCountNotif({this.count});

  DataCountNotif.fromJson(Map<String, dynamic> json) {
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['count'] = count;
    return data;
  }
}
