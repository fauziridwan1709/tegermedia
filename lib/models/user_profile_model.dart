part of '_models.dart';

class ResultUpdateProfile {
  int statusCode;
  String message;
  ResultUpdateProfile({this.statusCode, this.message});

  ResultUpdateProfile.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'] as int;
    message = json['message'] as String;
  }
}

class UserProfile {
  int _statusCode;
  UserDetail _data;

  UserProfile({int statusCode, UserDetail data}) {
    _statusCode = statusCode;
    _data = data;
  }

  int get statusCode => _statusCode;
  set statusCode(int statusCode) => _statusCode = statusCode;
  UserDetail get data => _data;
  set data(UserDetail data) => _data = data;

  UserProfile.fromJson(Map<String, dynamic> json) {
    _statusCode = json['status_code'];
    _data = json['data'] != null ? UserDetail.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = _statusCode;
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    return data;
  }
}

class UserDetail {
  String _image;
  String _fullName;
  String _noTelp;
  String _provinsiName;
  String _provinsiId;
  String _kabupatenId;
  String _kabupatenName;
  String _kecamatanId;
  String _kecamatanName;
  String _desaId;
  String _desaName;
  String _email;
  String _gender;
  String _detailAddress;
  bool _pantauActive;
  String _tokenPantau;
  int _userId;
  bool _isNoSchools;
  bool _isNoClasses;
  bool _isNoCourses;

  UserDetail(
      {String image,
      String fullName,
      String noTelp,
      String provinsiName,
      String provinsiId,
      String kabupatenId,
      String kabupatenName,
      String kecamatanId,
      String kecamatanName,
      String desaId,
      String desaName,
      String email,
      String gender,
      String detailAddress,
      bool pantauActive,
      String tokenPantau,
      int userId,
      bool isNoSchools,
      bool isNoClasses,
      bool isNoCourses}) {
    _image = image;
    _fullName = fullName;
    _noTelp = noTelp;
    _provinsiName = provinsiName;
    _provinsiId = provinsiId;
    _kabupatenId = kabupatenId;
    _kabupatenName = kabupatenName;
    _kecamatanId = kecamatanId;
    _kecamatanName = kecamatanName;
    _desaId = desaId;
    _desaName = desaName;
    _email = email;
    _gender = gender;
    _detailAddress = detailAddress;
    _pantauActive = pantauActive;
    _tokenPantau = tokenPantau;
    _userId = userId;
    _isNoSchools = isNoSchools;
    _isNoClasses = isNoClasses;
    _isNoCourses = isNoCourses;
  }

  String get image => _image;
  set image(String image) => _image = image;
  String get fullName => _fullName;
  set fullName(String fullName) => _fullName = fullName;
  String get noTelp => _noTelp;
  set noTelp(String noTelp) => _noTelp = noTelp;
  String get provinsiName => _provinsiName;
  set provinsiName(String provinsiName) => _provinsiName = provinsiName;
  String get provinsiId => _provinsiId;
  set provinsiId(String provinsiId) => _provinsiId = provinsiId;
  String get kabupatenId => _kabupatenId;
  set kabupatenId(String kabupatenId) => _kabupatenId = kabupatenId;
  String get kabupatenName => _kabupatenName;
  set kabupatenName(String kabupatenName) => _kabupatenName = kabupatenName;
  String get kecamatanId => _kecamatanId;
  set kecamatanId(String kecamatanId) => _kecamatanId = kecamatanId;
  String get kecamatanName => _kecamatanName;
  set kecamatanName(String kecamatanName) => _kecamatanName = kecamatanName;
  String get desaId => _desaId;
  set desaId(String desaId) => _desaId = desaId;
  String get desaName => _desaName;
  set desaName(String desaName) => _desaName = desaName;
  String get email => _email;
  set email(String email) => _email = email;
  String get gender => _gender;
  set gender(String gender) => _gender = gender;
  String get detailAddress => _detailAddress;
  set detailAddress(String detailAddress) => _detailAddress = detailAddress;
  bool get pantauActive => _pantauActive;
  set pantauActive(bool pantauActive) => _pantauActive = pantauActive;
  String get tokenPantau => _tokenPantau;
  set tokenPantau(String tokenPantau) => _tokenPantau = tokenPantau;
  int get userId => _userId;
  set userId(int userId) => _userId = userId;
  bool get isNoSchools => _isNoSchools;
  set isNoSchools(bool isNoSchools) => _isNoSchools = isNoSchools;
  bool get isNoClasses => _isNoClasses;
  set isNoClasses(bool isNoClasses) => _isNoClasses = isNoClasses;
  bool get isNoCourses => _isNoCourses;
  set isNoCourses(bool isNoCourses) => _isNoCourses = isNoCourses;

  UserDetail.fromJson(Map<String, dynamic> json) {
    _image = json['image'];
    _fullName = json['full_name'];
    _noTelp = json['no_telp'];
    _provinsiName = json['provinsi_name'];
    _provinsiId = json['provinsi_id'];
    _kabupatenId = json['kabupaten_id'];
    _kabupatenName = json['kabupaten_name'];
    _kecamatanId = json['kecamatan_id'];
    _kecamatanName = json['kecamatan_name'];
    _desaId = json['desa_id'];
    _desaName = json['desa_name'];
    _email = json['email'];
    _gender = json['gender'];
    _detailAddress = json['detail_address'];
    _pantauActive = json['pantau_active'];
    _tokenPantau = json['token_pantau'];
    _userId = json['user_id'];
    _isNoSchools = json['is_no_schools'];
    _isNoClasses = json['is_no_classes'];
    _isNoCourses = json['is_no_courses'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['image'] = _image;
    data['full_name'] = _fullName;
    data['no_telp'] = _noTelp;
    data['provinsi_name'] = _provinsiName;
    data['provinsi_id'] = _provinsiId;
    data['kabupaten_id'] = _kabupatenId;
    data['kabupaten_name'] = _kabupatenName;
    data['kecamatan_id'] = _kecamatanId;
    data['kecamatan_name'] = _kecamatanName;
    data['desa_id'] = _desaId;
    data['desa_name'] = _desaName;
    data['email'] = _email;
    data['gender'] = _gender;
    data['detail_address'] = _detailAddress;
    data['pantau_active'] = _pantauActive;
    data['token_pantau'] = _tokenPantau;
    data['user_id'] = _userId;
    data['is_no_schools'] = _isNoSchools;
    data['is_no_classes'] = _isNoClasses;
    data['is_no_courses'] = _isNoCourses;
    return data;
  }
}
