part of '_absensi.dart';

class CreateAbsensiModel {
  int classId;
  int courseId;
  int scheduleId;
  int userId;
  String checkInTime;

  CreateAbsensiModel(
      {this.classId,
      this.courseId,
      this.scheduleId,
      this.userId,
      this.checkInTime});

  CreateAbsensiModel.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    courseId = json['course_id'];
    scheduleId = json['schedule_id'];
    userId = json['student_user_id'];
    checkInTime = json['check_in_time'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['class_id'] = classId;
    data['course_id'] = courseId;
    data['schedule_id'] = scheduleId;
    data['student_user_id'] = userId;
    data['check_in_time'] = checkInTime;
    return data;
  }
}
