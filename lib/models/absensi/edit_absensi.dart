part of '_absensi.dart';

class EditAbsenModel {
  int statusCode;
  AbsenBulanan data;

  EditAbsenModel({this.statusCode, this.data});

  EditAbsenModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = json['data'] != null ? AbsenBulanan.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class AbsenBulanan {
  List<StudentsFullName> studentsFullName;
  List<CourseDate> courseDate;
  List<RecordAbsen> recordAbsen;

  AbsenBulanan({this.studentsFullName, this.courseDate, this.recordAbsen});

  AbsenBulanan.fromJson(Map<String, dynamic> json) {
    if (json['students_full_name'] != null) {
      studentsFullName = <StudentsFullName>[];
      json['students_full_name'].forEach((dynamic v) {
        studentsFullName
            .add(StudentsFullName.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['course_date'] != null) {
      courseDate = <CourseDate>[];
      json['course_date'].forEach((dynamic v) {
        courseDate.add(CourseDate.fromJson(v as Map<String, dynamic>));
      });
    }
    if (json['record_absen'] != null) {
      recordAbsen = <RecordAbsen>[];
      json['record_absen'].forEach((dynamic v) {
        recordAbsen.add(RecordAbsen.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (studentsFullName != null) {
      data['students_full_name'] =
          studentsFullName.map((v) => v.toJson()).toList();
    }
    if (courseDate != null) {
      data['course_date'] = courseDate.map((v) => v.toJson()).toList();
    }
    if (recordAbsen != null) {
      data['record_absen'] = recordAbsen.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class StudentsFullName {
  String fullName;
  int userId;

  StudentsFullName({this.fullName, this.userId});

  StudentsFullName.fromJson(Map<String, dynamic> json) {
    fullName = json['full_name'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['full_name'] = fullName;
    data['user_id'] = userId;
    return data;
  }
}

class CourseDate {
  String scheduleDate;

  CourseDate({this.scheduleDate});

  CourseDate.fromJson(Map<String, dynamic> json) {
    scheduleDate = json['schedule_date'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['schedule_date'] = scheduleDate;
    return data;
  }
}

class RecordAbsen {
  String fullName;
  int courseId;
  int userId;
  int absenId;
  String scheduleFullDate;
  int status;
  int checkInTimeHour;
  int checkInTimeMinute;
  String scheduleDate;
  String scheduleMonth;
  String notes;
  int scheduleId;

  RecordAbsen(
      {this.fullName,
      this.courseId,
      this.userId,
      this.absenId,
      this.scheduleFullDate,
      this.status,
      this.checkInTimeHour,
      this.checkInTimeMinute,
      this.scheduleDate,
      this.scheduleMonth,
      this.notes,
      this.scheduleId});

  RecordAbsen.fromJson(Map<String, dynamic> json) {
    fullName = json['full_name'];
    courseId = json['course_id'];
    userId = json['user_id'];
    absenId = json['absen_id'];
    scheduleFullDate = json['schedule_full_date'];
    status = json['status'];
    checkInTimeHour = json['check_in_time_hour'];
    checkInTimeMinute = json['check_in_time_minute'];
    scheduleDate = json['schedule_date'];
    scheduleMonth = json['schedule_month'];
    notes = json['notes'];
    scheduleId = json['schedule_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['full_name'] = fullName;
    data['course_id'] = courseId;
    data['user_id'] = userId;
    data['absen_id'] = absenId;
    data['schedule_full_date'] = scheduleFullDate;
    data['status'] = status;
    data['check_in_time_hour'] = checkInTimeHour;
    data['check_in_time_minute'] = checkInTimeMinute;
    data['schedule_date'] = scheduleDate;
    data['schedule_month'] = scheduleMonth;
    data['notes'] = notes;
    data['schedule_id'] = scheduleId;
    return data;
  }
}
