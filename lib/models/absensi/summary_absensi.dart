part of '_absensi.dart';

class AbsensiSummaryModel {
  int statusCode;
  SummaryAbsensiItem data;

  AbsensiSummaryModel({this.statusCode, this.data});

  AbsensiSummaryModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data =
        json['data'] != null ? SummaryAbsensiItem.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class SummaryAbsensiItem {
  GeneralAbsenSummary generalAbsenSummary;
  List<CoursesAbsenSummary> coursesAbsenSummary;

  SummaryAbsensiItem({this.generalAbsenSummary, this.coursesAbsenSummary});

  SummaryAbsensiItem.fromJson(Map<String, dynamic> json) {
    generalAbsenSummary = json['general_absen_summary'] != null
        ? GeneralAbsenSummary.fromJson(json['general_absen_summary'])
        : null;
    if (json['courses_absen_summary'] != null) {
      coursesAbsenSummary = <CoursesAbsenSummary>[];
      json['courses_absen_summary'].forEach((dynamic v) {
        coursesAbsenSummary
            .add(CoursesAbsenSummary.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (generalAbsenSummary != null) {
      data['general_absen_summary'] = generalAbsenSummary.toJson();
    }
    if (coursesAbsenSummary != null) {
      data['courses_absen_summary'] =
          coursesAbsenSummary.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GeneralAbsenSummary {
  int totalExistSchedule;
  int attend;
  int absen;
  int halfAttend;

  GeneralAbsenSummary(
      {this.totalExistSchedule, this.attend, this.absen, this.halfAttend});

  GeneralAbsenSummary.fromJson(Map<String, dynamic> json) {
    totalExistSchedule = json['total_exist_schedule'];
    attend = json['attend'];
    absen = json['absen'];
    halfAttend = json['half_attend'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['total_exist_schedule'] = totalExistSchedule;
    data['attend'] = attend;
    data['absen'] = absen;
    data['half_attend'] = halfAttend;
    return data;
  }
}

class CoursesAbsenSummary {
  int totalExistSchedule;
  int attend;
  int absen;
  int halfAttend;
  int permit;
  int sick;
  int courseId;

  String courseName;

  CoursesAbsenSummary(
      {this.totalExistSchedule,
      this.attend,
      this.absen,
      this.halfAttend,
      this.permit,
      this.sick,
      this.courseId,
      this.courseName});

  CoursesAbsenSummary.fromJson(Map<String, dynamic> json) {
    totalExistSchedule = json['total_exist_schedule'];
    attend = json['attend'];
    absen = json['absen'];
    halfAttend = json['half_attend'];
    permit = json['permit'];
    sick = json['sick'];
    courseId = json['course_id'];
    courseName = json['course_name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['total_exist_schedule'] = totalExistSchedule;
    data['attend'] = attend;
    data['absen'] = absen;
    data['half_attend'] = halfAttend;
    data['permit'] = permit;
    data['sick'] = sick;
    data['course_id'] = courseId;
    data['course_name'] = courseName;
    return data;
  }
}
