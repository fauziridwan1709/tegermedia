class StudentPermit {
  int statusCode;
  List<DataStudentPermit> data;

  StudentPermit({this.statusCode, this.data});

  StudentPermit.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <DataStudentPermit>[];
      json['data'].forEach((dynamic v) {
        data.add(DataStudentPermit.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataStudentPermit {
  int studentPermitId;
  String createdAt;
  String parentName;
  String studentName;
  String from;
  String to;
  int dayDuration;
  int hourDuration;
  String message;
  List<Attachments> attachments;
  String respondMessage;
  int status;

  DataStudentPermit(
      {this.studentPermitId,
      this.createdAt,
      this.parentName,
      this.studentName,
      this.from,
      this.to,
      this.dayDuration,
      this.hourDuration,
      this.message,
      this.attachments,
      this.respondMessage,
      this.status});

  DataStudentPermit.fromJson(Map<String, dynamic> json) {
    studentPermitId = json['student_permit_id'];
    createdAt = json['created_at'];
    parentName = json['parent_name'];
    studentName = json['student_name'];
    from = json['from'];
    to = json['to'];
    dayDuration = json['day_duration'];
    hourDuration = json['hour_duration'];
    message = json['message'];
    if (json['attachments'] != null) {
      attachments = new List<Attachments>();
      json['attachments'].forEach((dynamic v) {
        attachments.add(new Attachments.fromJson(v as Map<String, dynamic>));
      });
    }
    respondMessage = json['respond_message'] ?? "Permohonan Izin Absensi";
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_permit_id'] = this.studentPermitId;
    data['created_at'] = this.createdAt;
    data['parent_name'] = this.parentName;
    data['student_name'] = this.studentName;
    data['from'] = this.from;
    data['to'] = this.to;
    data['day_duration'] = this.dayDuration;
    data['hour_duration'] = this.hourDuration;
    data['message'] = this.message;
    if (this.attachments != null) {
      data['attachments'] = this.attachments.map((v) => v.toJson()).toList();
    }
    data['respond_message'] = this.respondMessage;
    data['status'] = this.status;
    return data;
  }

  bool get isSudahDiRespon => this.status == 1;
  bool get isBelumDiRespon => this.status == 0;
}

class Attachments {
  String name;
  String link;

  Attachments({this.name, this.link});

  Attachments.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['link'] = this.link;
    return data;
  }
}
