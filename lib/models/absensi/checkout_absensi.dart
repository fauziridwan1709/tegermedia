part of '_absensi.dart';

class CheckoutModel {
  int checkInId;
  String checkOutTime;

  CheckoutModel({this.checkInId, this.checkOutTime});

  CheckoutModel.fromJson(Map<String, dynamic> json) {
    checkInId = json['check_in_id'];
    checkOutTime = json['check_out_time'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['check_in_id'] = checkInId;
    data['check_out_time'] = checkOutTime;
    return data;
  }
}
