part of '_absensi.dart';

class ChangeAbsenModel {
  int _absenId;
  bool _updateTime;
  int _hours;
  int _minute;
  int _status;
  String _notes;

  ChangeAbsenModel(
      {String notes,
      int absenId,
      bool updateTime,
      int hours,
      int minute,
      int status}) {
    _absenId = absenId;
    _updateTime = updateTime;
    _hours = hours;
    _minute = minute;
    _status = status;
    _notes = notes;
  }

  int get absenId => _absenId;
  set absenId(int absenId) => _absenId = absenId;
  bool get updateTime => _updateTime;
  set updateTime(bool updateTime) => _updateTime = updateTime;
  int get hours => _hours;
  set hours(int hours) => _hours = hours;
  int get minute => _minute;
  set minute(int minute) => _minute = minute;
  int get status => _status;
  set status(int status) => _status = status;
  String get notes => _notes;
  set notes(String notes) => _notes = notes;

  ChangeAbsenModel.fromJson(Map<String, dynamic> json) {
    _absenId = json['absen_id'];
    _updateTime = json['update_time'];
    _hours = json['hours'];
    _minute = json['minute'];
    _status = json['status'];
    _notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['absen_id'] = _absenId;
    data['update_time'] = _updateTime;
    data['hours'] = _hours;
    data['minute'] = _minute;
    data['status'] = _status;
    data['notes'] = _notes;
    return data;
  }
}
