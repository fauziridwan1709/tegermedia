part of '_models.dart';

class JenjangData {
  static const semuaKelas = [
    'kelas 1',
    'kelas 2',
    'kelas 3',
    'kelas 4',
    'kelas 5',
    'kelas 6',
    'kelas 7',
    'kelas 8',
    'kelas 9',
    'kelas 10',
    'kelas 11 IPA',
    'kelas 11 IPS',
    'kelas 12 IPA',
    'kelas 12 IPS'
  ];
  static const jenjang = ['tk', 'sd', 'smp', 'sma'];
  static const kurikulum = ['2013', '2006'];
  static const jurusan = ['bahagia', 'ketawa yuk', 'haha hihi huhu'];
  static const kelasSmk = ['kelas 10', 'kelas 11', 'kelas 12'];
  static const kelasSma = [
    'kelas 10 IPA',
    'kelas 10 IPS',
    'kelas 11 IPA',
    'kelas 11 IPS',
    'kelas 12 IPA',
    'kelas 12 IPS'
  ];
  static const kelasSmp = ['kelas 7', 'kelas 8', 'kelas 9'];
  static const kelasSd = [
    'kelas 1',
    'kelas 2',
    'kelas 3',
    'kelas 4',
    'kelas 5',
    'kelas 6'
  ];
  static const kelasTk = ['kelas 0 Kecil', 'kelas 0 Besar'];
  static const kelasPaud = ['paud-A', 'Paud-B'];
  static const kelas = [kelasTk, kelasSd, kelasSmp, kelasSma, kelasSmk];

  static int getApiKodePres(String name, String pelajaran) {
    for (int i = 0; i < jenjang.length; i++) {
      if (name == jenjang[0]) {
        return 17;
      } else if (name == jenjang[i]) {
        return 14 - i;
      }
    }
    return 0;
  }

  static int getApiKodeDoc(String name, String pelajaran) {
    for (int i = 0; i < jenjang.length; i++) {
      if (pelajaran == 'Kecerdasan' ||
          pelajaran == 'Personal Growth' ||
          pelajaran == 'Bahasa' ||
          pelajaran == 'Persiapan Kuliah') {
        return 10;
      } else if (name == jenjang[0]) {
        return 9;
      } else if (name == jenjang[i]) {
        return 9 - i;
      } //else if (name == jenjang[4]) //1,2,3,4 = 6,7,8,9 {
      //return 16;
    }
    return 0;
  }

  static int getApiKodePelajaran(String name) {
    for (int i = 0; i < jenjang.length; i++) {
      if (name == jenjang[0]) {
        return 4;
      } else if (name == jenjang[i]) {
        return 4 - i;
      } //else if (name == jenjang[4]) {
      //return 15;
      //}
    }
    return 5;
  }

  static List<String> getKelas(String name) {
    return kelas[jenjang.indexOf(name)];
  }

  static int getApiKodeJadwal(String asset) {
    if (asset == "rri") {
      return 3;
    } else
      return 4;
  }
}

class TipePelajaran {
  static const tipeBelajarSma = ["E-book", "Presentasi", "Video", "App"];
  static const kurikulum = ["kurtilas", "ktsp"];
  static const jurusan = ["bahagia", "ketawa yuk", "haha hihi huhu"];
  static const umum = [
    "Kecerdasan",
    "Personal Growth",
    "Bahasa",
    "Persiapan Kuliah"
  ];
}

class MataPelajaran {
  static List<String> getMataPelajaran(String kelas) {
    if (kelas == '7' || kelas == '8' || kelas == '9') {
      return mataPelajaranSmp;
    } else if (kelas == '10' || kelas == '11' || kelas == '12') {
      return mataPelajaranSma;
    } else if (kelas == '10 IPA' || kelas == '11 IPA' || kelas == '12 IPA') {
      return mataPelajaranSmaIPA;
    } else if (kelas == '10 IPS' || kelas == '11 IPS' || kelas == '12 IPS') {
      return mataPelajaranSmaIPS;
    } else if (kelas == '1' ||
        kelas == '2' ||
        kelas == '3' ||
        kelas == '4' ||
        kelas == '5' ||
        kelas == '6') {
      return mataPelajaranSd;
    } else if (kelas == '0 Kecil' || kelas == '0 Besar') {
      return mataPelajaranTk;
    }
    return [];
  }

  static List<String> getMataPelajaranLain(String kelas) {
    if (kelas == '7' || kelas == '8' || kelas == '9') {
      return mataPelajaranSmpLain;
    } else if (kelas == '10' || kelas == '11' || kelas == '12') {
      return mataPelajaranSmaLain;
    } else if (kelas == '11 IPA' || kelas == '12 IPA' || kelas == '10 IPA') {
      return mataPelajaranSmaIPALain;
    } else if (kelas == '11 IPS' || kelas == '12 IPS' || kelas == '10 IPS') {
      return mataPelajaranSmaIPSLain;
    } else if (kelas == '1' ||
        kelas == '2' ||
        kelas == '3' ||
        kelas == '4' ||
        kelas == '5' ||
        kelas == '6') {
      return null;
    } else if (kelas == '0 Kecil' || kelas == '0 Besar') {
      return null;
    }
    return [];
  }

  static const List<String> mataPelajaranLainnya = [
    'Agama',
    'Antropologi',
    'Olahraga',
    'Pancasila',
    'Prakarya',
    'Sejarah',
    'Sosiologi',
  ];

  static const List<String> mataPelajaranSmaIPA = [
    'Biologi',
    'Fisika',
    'Bahasa Indonesia',
    'Bahasa Inggris',
    'Kimia',
    'Matematika',
    'PPKn',
    'lainnya'
  ];

  static const List<String> mataPelajaranSmaIPALain = [
    'PJOK',
    'Prakarya',
    'Seni Budaya',
    'Agama Islam',
    'Agama Kristen',
    'Agama Katolik',
    'Agama Hindu',
    'Agama Buddha',
    'Agama Khonghucu'
  ];

  static const List<String> mataPelajaranSmaIPS = [
    'Prakarya',
    'Sejarah',
    'Sosiologi',
    'Seni Budaya',
    'Geografi',
    'Akuntansi',
    'Antropologi',
    'lainnya'
  ];

  static const List<String> mataPelajaranSmaIPSLain = [
    'Matematika',
    'Bahasa Indonesia',
    'Bahasa Inggris',
    'PJOK',
    'PPKn',
    'Agama Islam',
    'Agama Kristen',
    'Agama Katolik',
    'Agama Hindu',
    'Agama Buddha',
    'Agama Khonghucu',
  ];

  static const List<String> mataPelajaranSma = [
    'Biologi',
    'Ekonomi',
    'Fisika',
    'Bahasa Indonesia',
    'Bahasa Inggris',
    'Kimia',
    'Matematika',
    'lainnya'
  ];

  static const List<String> mataPelajaranSmaLain = [
    'Antropologi',
    'PJOK',
    'PPKn',
    'Prakarya',
    'Sejarah',
    'Sosiologi',
    'Seni Budaya',
    'Geografi',
    'Akuntansi',
    'Agama Islam',
    'Agama Kristen',
    'Agama Katolik',
    'Agama Hindu',
    'Agama Buddha',
    'Agama Khonghucu',
  ];

  static const List<String> mataPelajaranSmp = [
    'Bahasa Indonesia',
    'IPA',
    'IPS',
    'Bahasa Inggris',
    'Matematika',
    'PJOK',
    'PPKn',
    'lainnya'
  ];

  static const List<String> mataPelajaranSmpLain = [
    'Prakarya',
    'Seni Budaya',
    'Agama Islam',
    'Agama Kristen',
    'Agama Katolik',
    'Agama Hindu',
    'Agama Buddha',
    'Agama Khonghucu',
  ];

  static const List<String> mataPelajaranSd = [
    'Matematika',
    'Tematik',
    'Agama Islam',
    'Agama Kristen',
    'Agama Katolik',
    'Agama Hindu',
    'Agama Khonghucu',
  ];
  static const List<String> mataPelajaranTk = [
    'Kepribadian',
    'Menghitung',
    'Membaca',
    'Bahasa Indonesia',
    'Bahasa Inggris'
  ];
}

class QuizChoice {
  static const choice = ['A', 'B', 'C', 'D', 'E'];
}
