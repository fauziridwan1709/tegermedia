part of '_models.dart';
//
// class NewsModel {
//   List<Rows> rows;
//
//   NewsModel({this.rows});
//
//   NewsModel.fromJson(Map<String, dynamic> json) {
//     if (json['rows'] != null) {
//       rows = new List<Rows>();
//       json['rows'].forEach((v) {
//         rows.add(new Rows.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.rows != null) {
//       data['rows'] = this.rows.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Rows {
//   String judul;
//   String gambar;
//   String tanggal;
//   String penerbit;
//   String status;
//   String link;
//
//   Rows(
//       {this.judul,
//       this.gambar,
//       this.tanggal,
//       this.penerbit,
//       this.status,
//       this.link});
//
//   Rows.fromJson(Map<String, dynamic> json) {
//     judul = json['judul'];
//     gambar = json['gambar'];
//     tanggal = json['tanggal'];
//     penerbit = json['penerbit'];
//     status = json['status'];
//     link = json['link'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['judul'] = this.judul;
//     data['gambar'] = this.gambar;
//     data['tanggal'] = this.tanggal;
//     data['penerbit'] = this.penerbit;
//     data['status'] = this.status;
//     data['link'] = this.link;
//     return data;
//   }
// }
