part of '_models.dart';

class QuizModel {
  QuizModel({this.rows});
  List<QuizItem> rows;

  QuizModel.fromJson(Map<String, dynamic> json) {
    if (json['rows'] != null) {
      rows = <QuizItem>[];
      json['rows'].forEach((dynamic e) {
        rows.add(QuizItem.fromJson(e as Map<String, dynamic>));
      });
    }
  }
}

class QuizItem {
  String kurikulum;
  String kelas;
  String matapelajaran;
  String judul;
  String materi;
  String typesoal;
  String pertanyaan;
  String attpertanyaan;
  String pilihana;
  String atta;
  String pilihanb;
  String attb;
  String pilihanc;
  String attc;
  String pilihand;
  String attd;
  String pilihane;
  String atte;
  String jawaban;
  String attjwb;
  String bobot;
  String status;
  String publish;

  QuizItem({
    this.kurikulum,
    this.kelas,
    this.matapelajaran,
    this.judul,
    this.materi,
    this.typesoal,
    this.pertanyaan,
    this.attpertanyaan,
    this.pilihana,
    this.atta,
    this.pilihanb,
    this.attb,
    this.pilihanc,
    this.attc,
    this.pilihand,
    this.attd,
    this.pilihane,
    this.atte,
    this.jawaban,
    this.attjwb,
    this.bobot,
    this.status,
    this.publish,
  });

  QuizItem.fromJson(Map<String, dynamic> json) {
    kurikulum = json['kurikulum'].toString();
    kelas = json['kelas'].toString();
    matapelajaran = json['matapelajaran'].toString();
    judul = json['judul'].toString();
    materi = json['materi'].toString();
    typesoal = json['typesoal'].toString();
    pertanyaan = json['pertanyaan'].toString();
    attpertanyaan = json['attpertanyaan'].toString();
    pilihana = json['pilihana'].toString();
    atta = json['atta'].toString();
    pilihanb = json['pilihanb'].toString();
    attb = json['attb'].toString();
    pilihanc = json['pilihanc'].toString();
    attc = json['attc'].toString();
    pilihand = json['pilihand'].toString();
    attd = json['attd'].toString();
    pilihane = json['pilihane'].toString();
    atte = json['atte'].toString();
    jawaban = json['jawaban'].toString();
    attjwb = json['attjwb'].toString();
    bobot = json['score'].toString();
    status = json['status'].toString();
    publish = json['publish'].toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['kurikulum'] = kurikulum;
    data['kelas'] = kelas;
    data['matapelajaran'] = matapelajaran;
    data['judul'] = judul;
    data['materi'] = materi;
    data['typesoal'] = typesoal;
    data['matapelajaran'] = matapelajaran;
    data['pertanyaan'] = pertanyaan;
    data['attpertanyaan'] = attpertanyaan;
    data['pilihana'] = pilihana;
    data['atta'] = atta;
    data['pilihanb'] = pilihanb;
    data['attb'] = attb;
    data['pilihanc'] = pilihanc;
    data['attc'] = attc;
    data['pilihand'] = pilihand;
    data['attd'] = attd;
    data['pilihane'] = pilihane;
    data['atte'] = atte;
    data['jawaban'] = jawaban;
    data['attjwb'] = attjwb;
    data['score'] = bobot;
    data['status'] = status;
    data['publish'] = publish;
    return data;
  }
}
