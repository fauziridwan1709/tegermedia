part of '_course.dart';

class CourseByClassModel {
  int statusCode;
  String message;
  CUDCourseModel cudModel;
  GetCourseModel param;
  List<CourseByClassData> data;

  CourseByClassModel(
      {this.statusCode, this.message, this.data, this.cudModel, this.param});

  CourseByClassModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <CourseByClassData>[];
      json['data'].forEach((dynamic v) {
        data.add(CourseByClassData.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CourseByClassData {
  int id;
  String name;
  String description;
  String state;
  int schoolId;
  int classId;
  String invitationStudentCode;
  String invitationTeacherCode;
  List<ListGuru> listGuru;
  List<String> namaGuru;
  List<String> namaGuruDanWalikelas;
  String namaWalikelas;
  String nomorTelpWalikelas;
  String role;
  int totalSiswa;
  int totalGuru;
  bool isSudahGabung;
  String createdAt;
  String updatedAt;

  CourseByClassData(
      {this.id,
      this.name,
      this.description,
      this.state,
      this.schoolId,
      this.classId,
      this.invitationStudentCode,
      this.invitationTeacherCode,
      this.listGuru,
      this.namaGuru,
      this.namaGuruDanWalikelas,
      this.namaWalikelas,
      this.nomorTelpWalikelas,
      this.role,
      this.totalSiswa,
      this.totalGuru,
      this.isSudahGabung,
      this.createdAt,
      this.updatedAt});

  CourseByClassData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    state = json['state'];
    schoolId = json['school_id'];
    classId = json['class_id'];
    invitationStudentCode = json['invitation_student_code'];
    invitationTeacherCode = json['invitation_teacher_code'];
    if (json['list_guru'] != null) {
      listGuru = <ListGuru>[];
      json['list_guru'].forEach((dynamic v) {
        listGuru.add(ListGuru.fromJson(v as Map<String, dynamic>));
      });
    }
    namaGuru = json['nama_guru'].cast<String>();
    namaGuruDanWalikelas = json['nama_guru_dan_walikelas'].cast<String>();
    namaWalikelas = json['nama_walikelas'];
    nomorTelpWalikelas = json['nomor_telp_walikelas'];
    role = json['role'];
    totalSiswa = json['total_siswa'];
    totalGuru = json['total_guru'];
    isSudahGabung = json['is_sudah_gabung'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['state'] = state;
    data['school_id'] = schoolId;
    data['class_id'] = classId;
    data['invitation_student_code'] = invitationStudentCode;
    data['invitation_teacher_code'] = invitationTeacherCode;
    if (listGuru != null) {
      data['list_guru'] = listGuru.map((v) => v.toJson()).toList();
    }
    data['nama_guru'] = namaGuru;
    data['nama_guru_dan_walikelas'] = namaGuruDanWalikelas;
    data['nama_walikelas'] = namaWalikelas;
    data['nomor_telp_walikelas'] = nomorTelpWalikelas;
    data['role'] = role;
    data['total_siswa'] = totalSiswa;
    data['total_guru'] = totalGuru;
    data['is_sudah_gabung'] = isSudahGabung;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class CUDCourseModel {
  CourseByClassData detail;

  CUDCourseModel(this.detail);
}

class GetCourseModel {
  int classId;

  GetCourseModel({this.classId});
}

class ListGuru {
  int userId;
  String nama;

  ListGuru({this.userId, this.nama});

  ListGuru.fromJson(Map<String, dynamic> json) {
    userId = json['user_Id'];
    nama = json['nama'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_Id'] = userId;
    data['nama'] = nama;
    return data;
  }
}
