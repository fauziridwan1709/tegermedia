part of '_models.dart';

class ModelListWilayah {
  int statusCode = 200;
  List<DataWilayah> data = [];

  ModelListWilayah({this.statusCode, this.data});

  ModelListWilayah.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <DataWilayah>[];
      json['data'].forEach((dynamic v) {
        data.add(DataWilayah.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataWilayah {
  int id;
  String nama;

  DataWilayah({this.id = 0, this.nama = ''});

  DataWilayah.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nama = json['nama'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['nama'] = nama;
    return data;
  }
}
