part of '_models.dart';

class News {
  final String title;
  final String desc;
  final String image;

  News(this.title, this.desc, this.image);
}

List<News> dummyDataNews = [
  News('Berita Pendidikan', 'Akan diberlakukan Sekolah Online',
      'assets/card_news.jpg'),
  News('Berita Pendidikan', 'Akan diberlakukan Sekolah Online',
      'assets/card_news.jpg'),
  News('Berita Pendidikan', 'Akan diberlakukan Sekolah Online',
      'assets/card_news.jpg'),
  News('Berita Pendidikan', 'Akan diberlakukan Sekolah Online',
      'assets/card_news.jpg'),
  News('Berita Pendidikan', 'Akan diberlakukan Sekolah Online',
      'assets/card_news.jpg'),
  News('Berita Pendidikan', 'Akan diberlakukan Sekolah Online',
      'assets/card_news.jpg')
];
