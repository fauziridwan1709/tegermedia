part of '_oauth.dart';

class OauthTokenUrl {
  OauthTokenData _data;
  Null _meta;

  OauthTokenUrl({OauthTokenData data, Null meta}) {
    _data = data;
    _meta = meta;
  }

  OauthTokenData get data => _data;
  set data(OauthTokenData data) => _data = data;
  Null get meta => _meta;
  set meta(Null meta) => _meta = meta;

  OauthTokenUrl.fromJson(Map<String, dynamic> json) {
    _data = json['data'] != null ? OauthTokenData.fromJson(json['data']) : null;
    _meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (_data != null) {
      data['data'] = _data.toJson();
    }
    data['meta'] = _meta;
    return data;
  }
}

class OauthTokenData {
  String _url;

  OauthTokenData({String url}) {
    _url = url;
  }

  String get url => _url;
  set url(String url) => _url = url;

  OauthTokenData.fromJson(Map<String, dynamic> json) {
    _url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['url'] = _url;
    return data;
  }
}
