part of '_models.dart';

class JadwalTayangModel {
  List<JadwalTayangItem> rows;

  JadwalTayangModel({this.rows});

  JadwalTayangModel.fromJson(Map<dynamic, dynamic> json) {
    if (json['rows'] != null) {
      rows = <JadwalTayangItem>[];
      json['rows'].forEach((dynamic v) {
        rows.add(JadwalTayangItem.fromJson(v as Map<dynamic, dynamic>));
      });
    }
  }

  Map<dynamic, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (rows != null) {
      data['rows'] = rows.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class JadwalTayangItem {
  String kelas;
  int hari;
  String waktutayang;
  String acara;

  JadwalTayangItem({
    this.kelas,
    this.hari,
    this.waktutayang,
    this.acara,
  });

  JadwalTayangItem.fromJson(Map<dynamic, dynamic> json) {
    kelas = json['kelas'];
    hari = json['hari'];
    waktutayang = json['waktutayang'];
    acara = json['acara'];
  }

  Map<dynamic, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['kelas'] = kelas;
    data['hari'] = hari;
    data['waktutayang'] = waktutayang;
    data['acara'] = acara;
    return data;
  }
}
