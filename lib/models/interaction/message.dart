typedef Parser<T> = T Function(dynamic json);

class DataMessage<T> {
  String title;
  T message;

  DataMessage({this.title, this.message});

  DataMessage.fromJson(
    Map<String, dynamic> json,
    Parser<T> parser,
  ) {
    title = json['title'];

    message = json['message'] != null ? parser(json['message']) : null;
  }
}
