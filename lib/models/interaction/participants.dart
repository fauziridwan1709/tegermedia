class DetailParticipants {
  DataParticipants data;
  Null meta;

  DetailParticipants({this.data, this.meta});

  DetailParticipants.fromJson(Map<String, dynamic> json) {
    data =
        json['data'] != null ? DataParticipants.fromJson(json['data']) : null;
    meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['meta'] = meta;
    return data;
  }
}

class DataParticipants {
  String id;
  String code;
  String type;
  String name;
  int userId;
  String status;
  List<Participants> participants;
  String createdAt;
  String updatedAt;

  DataParticipants(
      {this.id,
      this.code,
      this.type,
      this.name,
      this.userId,
      this.status,
      this.participants,
      this.createdAt,
      this.updatedAt});

  DataParticipants.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    type = json['type'];
    name = json['name'];
    userId = json['user_id'];
    status = json['status'];
    if (json['participants'] != null) {
      participants = <Participants>[];
      json['participants'].forEach((dynamic v) {
        participants.add(Participants.fromJson(v as Map<String, dynamic>));
      });
    }
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['type'] = type;
    data['name'] = name;
    data['user_id'] = userId;
    data['status'] = status;
    if (participants != null) {
      data['participants'] = participants.map((v) => v.toJson()).toList();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class Participants {
  int id;
  User user;
  String createdAt;

  Participants({this.id, this.user, this.createdAt});

  Participants.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['user'] != null) {
      user = User.fromJson(json['user']);
    }
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    if (user != null) {
      data['user'] = user.toJson();
    }
    data['created_at'] = createdAt;
    return data;
  }
}

class User {
  int id;
  String code;
  String email;
  String name;
  int profileImageId;
  String profileImageUrl;
  String profileImagePresignedUrl;
  String gender;
  String phone;
  int cityId;
  String cityCode;
  String cityName;
  String provinceCode;
  String provinceName;
  String address;
  String status;
  String createdAt;
  String updatedAt;
  String deletedAt;

  User(
      {this.id,
      this.code,
      this.email,
      this.name,
      this.profileImageId,
      this.profileImageUrl,
      this.profileImagePresignedUrl,
      this.gender,
      this.phone,
      this.cityId,
      this.cityCode,
      this.cityName,
      this.provinceCode,
      this.provinceName,
      this.address,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    email = json['email'];
    name = json['name'];
    profileImageId = json['profile_image_id'];
    profileImageUrl = json['profile_image_url'];
    profileImagePresignedUrl = json['profile_image_presigned_url'];
    gender = json['gender'];
    phone = json['phone'];
    cityId = json['city_id'];
    cityCode = json['city_code'];
    cityName = json['city_name'];
    provinceCode = json['province_code'];
    provinceName = json['province_name'];
    address = json['address'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['email'] = email;
    data['name'] = name;
    data['profile_image_id'] = profileImageId;
    data['profile_image_url'] = profileImageUrl;
    data['profile_image_presigned_url'] = profileImagePresignedUrl;
    data['gender'] = gender;
    data['phone'] = phone;
    data['city_id'] = cityId;
    data['city_code'] = cityCode;
    data['city_name'] = cityName;
    data['province_code'] = provinceCode;
    data['province_name'] = provinceName;
    data['address'] = address;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}
