class Chat {
  List<DetailChats> data;
  Null meta;

  Chat({this.data, this.meta});

  Chat.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <DetailChats>[];
      json['data'].forEach((dynamic v) {
        data.add(DetailChats.fromJson(v as Map<String, dynamic>));
      });
    }
    meta = json['meta'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['meta'] = meta;
    return data;
  }
}

class DetailChats {
  String id;
  String roomId;
  int userId;
  User user;
  String type;
  String message;
  String fileUrl;
  String fileKey;
  String createdAt;
  String updatedAt;
  String deletedAt;

  DetailChats(
      {this.id,
      this.roomId,
      this.userId,
      this.user,
      this.type,
      this.message,
      this.fileUrl,
      this.fileKey,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  DetailChats.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roomId = json['room_id'];
    userId = json['user_id'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    type = json['type'];
    message = json['message'];
    fileUrl = json['file_url'];
    fileKey = json['file_key'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['room_id'] = roomId;
    data['user_id'] = userId;
    if (user != null) {
      data['user'] = user.toJson();
    }
    data['type'] = type;
    data['message'] = message;
    data['file_url'] = fileUrl;
    data['file_key'] = fileKey;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}

class User {
  int id;
  String code;
  String email;
  String name;
  int profileImageId;
  String profileImageUrl;
  String profileImagePresignedUrl;
  String gender;
  String phone;
  int cityId;
  String cityCode;
  String cityName;
  String provinceCode;
  String provinceName;
  String address;
  String status;
  String createdAt;
  String updatedAt;
  String deletedAt;

  User(
      {this.id,
      this.code,
      this.email,
      this.name,
      this.profileImageId,
      this.profileImageUrl,
      this.profileImagePresignedUrl,
      this.gender,
      this.phone,
      this.cityId,
      this.cityCode,
      this.cityName,
      this.provinceCode,
      this.provinceName,
      this.address,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    email = json['email'];
    name = json['name'];
    profileImageId = json['profile_image_id'];
    profileImageUrl = json['profile_image_url'];
    profileImagePresignedUrl = json['profile_image_presigned_url'];
    gender = json['gender'];
    phone = json['phone'];
    cityId = json['city_id'];
    cityCode = json['city_code'];
    cityName = json['city_name'];
    provinceCode = json['province_code'];
    provinceName = json['province_name'];
    address = json['address'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['email'] = email;
    data['name'] = name;
    data['profile_image_id'] = profileImageId;
    data['profile_image_url'] = profileImageUrl;
    data['profile_image_presigned_url'] = profileImagePresignedUrl;
    data['gender'] = gender;
    data['phone'] = phone;
    data['city_id'] = cityId;
    data['city_code'] = cityCode;
    data['city_name'] = cityName;
    data['province_code'] = provinceCode;
    data['province_name'] = provinceName;
    data['address'] = address;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}
