class StudentData {
  int statusCode;
  List<StudentInteraction> data;

  StudentData({this.statusCode, this.data});

  StudentData.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <StudentInteraction>[];
      json['data'].forEach((dynamic v) {
        data.add(StudentInteraction.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class StudentInteraction {
  String title;
  String startDate;
  String chatRoomId;
  int studyRoomRefId;
  String courseName;
  String className;
  String url;
  bool opened = false;

  StudentInteraction(
      {this.title,
      this.startDate,
      this.studyRoomRefId,
      this.courseName,
      this.className,
      this.url,
      this.opened});

  StudentInteraction.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    startDate = json['start_date'];
    chatRoomId = json['chat_room_id'];
    studyRoomRefId = json['study_room_ref_id'];
    courseName = json['course_name'];
    className = json['class_name'];
    url = json['url'];
    opened = json['opened'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['start_date'] = startDate;
    data['study_room_ref_id'] = studyRoomRefId;
    data['course_name'] = courseName;
    data['chat_room_id'] = chatRoomId;
    data['class_name'] = className;
    data['url'] = url;
    data['opened'] = opened;
    return data;
  }
}
