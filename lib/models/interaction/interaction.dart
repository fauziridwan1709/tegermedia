part of '../_models.dart';

class InteractionData {
  int statusCode;
  List<InteractionModel> data;

  InteractionData({this.statusCode, this.data});

  InteractionData.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <InteractionModel>[];
      json['data'].forEach((dynamic v) {
        data.add(InteractionModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InteractionModel {
  String title;
  String startDate;
  String chatRoomId;
  List<ListCourseClassId> listCourseClassId;
  int studyRoomId;
  String url;
  bool started;

  InteractionModel(
      {this.title,
      this.startDate,
      this.chatRoomId,
      this.listCourseClassId,
      this.studyRoomId,
      this.url,
      this.started});

  InteractionModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    startDate = json['start_date'];
    chatRoomId = json['chat_room_id'];
    if (json['list_course_class_id'] != null) {
      listCourseClassId = <ListCourseClassId>[];
      json['list_course_class_id'].forEach((dynamic v) {
        listCourseClassId
            .add(ListCourseClassId.fromJson(v as Map<String, dynamic>));
      });
    }
    studyRoomId = json['study_room_id'];
    url = json['url'];
    started = json['started'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['start_date'] = startDate;
    data['chat_room_id'] = chatRoomId;
    if (listCourseClassId != null) {
      data['list_course_class_id'] =
          listCourseClassId.map((v) => v.toJson()).toList();
    }
    data['study_room_id'] = studyRoomId;
    data['url'] = url;
    data['started'] = started;
    return data;
  }
}

class ListCourseClassId {
  int classId;
  int courseId;
  String courseName;
  String className;
  String year;
  String semester;
  int studyRoomRelationId;

  ListCourseClassId(
      {this.classId,
      this.courseId,
      this.courseName,
      this.className,
      this.year,
      this.semester,
      this.studyRoomRelationId});

  ListCourseClassId.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    courseId = json['course_id'];
    courseName = json['course_name'];
    className = json['class_name'];
    year = json['year'];
    semester = json['semester'];
    studyRoomRelationId = json['study_room_relation_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['class_id'] = classId;
    data['course_id'] = courseId;
    data['course_name'] = courseName;
    data['class_name'] = className;
    data['year'] = year;
    data['semester'] = semester;
    data['study_room_relation_id'] = studyRoomRelationId;
    return data;
  }
}
