import 'dart:convert';
import 'package:flutter/material.dart';
export 'core/_core.dart';
export 'assignment/_assignment.dart';
export 'document/_document.dart';
export 'forum/_forum.dart';
export 'conference/_conference.dart';
export 'absensi/_absensi.dart';
export 'course/_course.dart';
export 'materi/_materi.dart';
export 'schedule/_schedule.dart';
export 'class/_class.dart';
export 'oauth/_oauth.dart';
export 'user/_user.dart';
export 'class/_class.dart';

part 'news.dart';
part 'tabIcon_data.dart';
part 'news_model.dart';
part 'protocol_model.dart';
part 'materi_video_model.dart';
part 'materi_doc_model.dart';
part 'class.dart';
part 'sign_in_model.dart';
part 'user_profile_model.dart';
part 'upload_image.dart';
part 'student_jenjang_data.dart';
part 'wilayah_model.dart';
part 'materi_data_model.dart';
part 'materi_pres_model.dart';
part 'jadwal_tayang_model.dart';
part 'jadwal_kelas_model.dart';
part 'akademis_model.dart';
part 'quiz_model.dart';
part 'school_model.dart';
part 'notification_model.dart';
part 'model_upload_file.dart';
part 'quiz/quiz_model_kelas.dart';
part 'quiz/quiz_data.dart';
part 'interaction/interaction.dart';
part 'interaction/endClassResult.dart';
part 'interaction/createRoom.dart';
part 'course_detail_model.dart';

part 'notification/notification.dart';

part 'term_and_conditions.dart';

part 'home/home_model.dart';
part 'home/news_model.dart';

part 'ujian/answer_sheet.dart';
part 'quiz/quiz_by_id.dart';

part 'general_result_api_model.dart';
part 'link_result_api_model.dart';
