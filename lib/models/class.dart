part of '_models.dart';

ClassModel classModelFromJson(String str) =>
    ClassModel.fromJson(json.decode(str));

String classModelToJson(ClassModel data) => json.encode(data.toJson());

class ClassModel {
  ClassModel({
    this.data,
  });

  List<Datum> data;

  factory ClassModel.fromJson(Map<String, dynamic> json) => ClassModel(
        data: List<Datum>.from(
            json['data'].map((Map<String, dynamic> x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'data': List<dynamic>.from(data.map<dynamic>((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.alternateLink,
    this.calendarId,
    this.courseGroupEmail,
    this.courseState,
    this.creationTime,
    this.description,
    this.descriptionHeading,
    this.enrollmentCode,
    this.id,
    this.name,
    this.ownerId,
    this.room,
    this.section,
    this.teacherFolder,
    this.teacherGroupEmail,
    this.updateTime,
  });

  String alternateLink;
  String calendarId;
  String courseGroupEmail;
  String courseState;
  DateTime creationTime;
  String description;
  String descriptionHeading;
  String enrollmentCode;
  String id;
  String name;
  String ownerId;
  String room;
  String section;
  TeacherFolder teacherFolder;
  String teacherGroupEmail;
  DateTime updateTime;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        alternateLink: json['alternateLink'],
        calendarId: json['calendarId'],
        courseGroupEmail: json['courseGroupEmail'],
        courseState: json['courseState'],
        creationTime: DateTime.parse(json['creationTime']),
        description: json['description'] ?? '',
        descriptionHeading: json['descriptionHeading'],
        enrollmentCode: json['enrollmentCode'],
        id: json['id'],
        name: json['name'],
        ownerId: json['ownerId'],
        room: json['room'],
        section: json['section'],
        teacherFolder: TeacherFolder.fromJson(json['teacherFolder']),
        teacherGroupEmail: json['teacherGroupEmail'],
        updateTime: DateTime.parse(json['updateTime']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'alternateLink': alternateLink,
        'calendarId': calendarId,
        'courseGroupEmail': courseGroupEmail,
        'courseState': courseState,
        'creationTime': creationTime.toIso8601String(),
        'description': description ?? '',
        'descriptionHeading': descriptionHeading,
        'enrollmentCode': enrollmentCode,
        'id': id,
        'name': name,
        'ownerId': ownerId,
        'room': room,
        'section': section,
        'teacherFolder': teacherFolder.toJson(),
        'teacherGroupEmail': teacherGroupEmail,
        'updateTime': updateTime.toIso8601String(),
      };
}

class TeacherFolder {
  TeacherFolder({
    this.alternateLink,
    this.id,
    this.title,
  });

  String alternateLink;
  String id;
  String title;

  factory TeacherFolder.fromJson(Map<String, dynamic> json) => TeacherFolder(
        alternateLink: json['alternateLink'],
        id: json['id'],
        title: json['title'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'alternateLink': alternateLink,
        'id': id,
        'title': title,
      };
}

class CreateClassModel {
  String name;
  CreateClassModel({this.name});
}

class ResultCreateClassModel {
  int statusCode;
  String message;
  DetailResult data;

  ResultCreateClassModel({this.statusCode, this.message, this.data});

  ResultCreateClassModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? DetailResult.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DetailResult {
  int id;

  DetailResult({this.id});

  DetailResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    return data;
  }
}

class ModelListCourse {
  int statusCode;
  List<CourseDetail> data;

  ModelListCourse({this.statusCode, this.data});

  ModelListCourse.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <CourseDetail>[];
      json['data'].forEach((dynamic v) {
        data.add(CourseDetail.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CourseDetail {
  int id;
  String name;
  String description;
  String state;
  String invitationStudentCode;
  String invitationTeacherCode;
  String type;
  String role;
  int schoolId;
  int classId;
  int totalSiswa;
  List<String> namaGuru;
  List<String> namaGuruDanWalikelas;
  String namaWalikelas;
  String nomorTelpWalikelas;
  String createdAt;
  String updatedAt;

  CourseDetail(
      {this.id,
      this.name,
      this.description,
      this.state,
      this.invitationStudentCode,
      this.invitationTeacherCode,
      this.type,
      this.role,
      this.schoolId,
      this.classId,
      this.totalSiswa,
      this.namaGuru,
      this.namaGuruDanWalikelas,
      this.namaWalikelas,
      this.nomorTelpWalikelas,
      this.createdAt,
      this.updatedAt});

  CourseDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    state = json['state'];
    invitationStudentCode = json['invitation_student_code'];
    invitationTeacherCode = json['invitation_teacher_code'];
    type = json['type'];
    role = json['role'];
    schoolId = json['school_id'];
    classId = json['class_id'];
    totalSiswa = json['total_siswa'];
    namaGuru = json['nama_guru'].cast<String>();
    namaGuruDanWalikelas = json['nama_guru_dan_walikelas'].cast<String>();
    namaWalikelas = json['nama_walikelas'];
    nomorTelpWalikelas = json['nomor_telp_walikelas'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['state'] = state;
    data['invitation_student_code'] = invitationStudentCode;
    data['invitation_teacher_code'] = invitationTeacherCode;
    data['type'] = type;
    data['role'] = role;
    data['school_id'] = schoolId;
    data['class_id'] = classId;
    data['total_siswa'] = totalSiswa;
    data['nama_guru'] = namaGuru;
    data['nama_guru_dan_walikelas'] = namaGuruDanWalikelas;
    data['nama_walikelas'] = namaWalikelas;
    data['nomor_telp_walikelas'] = nomorTelpWalikelas;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class CoursePersonalModel {
  int statusCode;
  List<CoursePersonalDetail> data;

  CoursePersonalModel({this.statusCode, this.data});

  CoursePersonalModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <CoursePersonalDetail>[];
      json['data'].forEach((dynamic v) {
        data.add(CoursePersonalDetail.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CoursePersonalDetail {
  int id;
  String name;
  String description;
  String invitationStudentCode;
  String invitationTeacherCode;
  String type;
  String role;
  String namaWalikelas;
  int classId;

  CoursePersonalDetail(
      {this.id,
      this.name,
      this.description,
      this.invitationStudentCode,
      this.invitationTeacherCode,
      this.type,
      this.role,
      this.namaWalikelas,
      this.classId});

  CoursePersonalDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    invitationStudentCode = json['invitation_student_code'];
    invitationTeacherCode = json['invitation_teacher_code'];
    type = json['type'];
    role = json['role'];
    namaWalikelas = json['nama_walikelas'];
    classId = json['class_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['invitation_student_code'] = invitationStudentCode;
    data['invitation_teacher_code'] = invitationTeacherCode;
    data['type'] = type;
    data['role'] = role;
    data['nama_walikelas'] = namaWalikelas;
    data['class_id'] = classId;
    return data;
  }
}

class ClassInvitation {
  String invitationCode;
  String role;
  String type;

  ClassInvitation({this.invitationCode, this.role, this.type});

  ClassInvitation.fromJson(Map<String, dynamic> json) {
    invitationCode = json['invitation_code'];
    role = json['role'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['invitation_code'] = invitationCode;
    data['role'] = role;
    data['type'] = type;
    return data;
  }
}

class ResultInvitation {
  int statusCode;
  String message;
  ResultInvitationDetail data;

  ResultInvitation({this.statusCode, this.message, this.data});

  ResultInvitation.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null
        ? ResultInvitationDetail.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ResultInvitationDetail {
  int id;
  String name;

  ResultInvitationDetail({this.id, this.name = ''});

  ResultInvitationDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

class ClassByRefrence {
  int statusCode;
  List<DataClassByRefrence> data;

  ClassByRefrence({this.statusCode, this.data});

  ClassByRefrence.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <DataClassByRefrence>[];
      json['data'].forEach((dynamic v) {
        data.add(DataClassByRefrence.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataClassByRefrence {
  int id;
  int courseId;
  int userId;
  String fullName;
  String image;
  String type;
  String role;
  String email;
  String noTelp;

  DataClassByRefrence(
      {this.id,
      this.courseId,
      this.userId,
      this.fullName,
      this.image,
      this.type,
      this.email,
      this.noTelp,
      this.role});

  DataClassByRefrence.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    courseId = json['course_id'];
    userId = json['user_id'];
    fullName = json['full_name'];
    image = json['image'];
    type = json['type'];
    role = json['role'];
    noTelp = json['no_telp'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['course_id'] = courseId;
    data['user_id'] = userId;
    data['full_name'] = fullName;
    data['image'] = image;
    data['type'] = type;
    data['role'] = role;
    data['email'] = email;
    data['no_telp'] = noTelp;
    return data;
  }
}

class RegistrationData {
  String name;
  int provinsiId;
  int kotaId;
  String type;
  String grade;
  String address;
  String namaKepalaSekolah;
  String phoneNumber;
  String schoolEmail;
  String penanggungJawabEmail;

  RegistrationData(
      {this.name = '',
      this.provinsiId = 0,
      this.kotaId = 0,
      this.type = '',
      this.grade = '',
      this.address = '',
      this.namaKepalaSekolah = '',
      this.phoneNumber = '',
      this.schoolEmail = '',
      this.penanggungJawabEmail = ''});

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['provinsi_id'] = provinsiId;
    data['kota_id'] = kotaId;
    data['type'] = type;
    data['grade'] = grade;
    data['address'] = address;
    data['nama_kepala_sekolah'] = namaKepalaSekolah;
    data['phone_number'] = phoneNumber;
    data['school_email'] = schoolEmail;
    data['penanggung_jawab_email'] = penanggungJawabEmail;
    return data;
  }
}

class ModelMeCheck {
  int statusCode;
  DataMeCheck data;

  ModelMeCheck({this.statusCode, this.data});

  ModelMeCheck.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = json['data'] != null ? DataMeCheck.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataMeCheck {
  bool isNoSchools;
  bool isNoClasses;
  bool isNoCourses;

  DataMeCheck(
      {this.isNoSchools = false,
      this.isNoClasses = false,
      this.isNoCourses = false});

  DataMeCheck.fromJson(Map<String, dynamic> json) {
    isNoSchools = json['is_no_schools'];
    isNoClasses = json['is_no_classes'];
    isNoCourses = json['is_no_courses'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['is_no_schools'] = isNoSchools;
    data['is_no_classes'] = isNoClasses;
    data['is_no_courses'] = isNoCourses;
    return data;
  }
}

class ModelCreateClass {
  String name;
  String tingkat;
  String tahunAjaran;
  String semester;
  String jurusan;
  String description;
  int schoolId;

  ModelCreateClass(
      {this.name,
      this.tingkat,
      this.tahunAjaran,
      this.semester,
      this.jurusan,
      this.description,
      this.schoolId});

  ModelCreateClass.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    tingkat = json['tingkat'];
    tahunAjaran = json['tahun_ajaran'];
    description = json['description'];
    semester = json['semester'];
    jurusan = json['jurusan'];
    schoolId = json['school_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['tingkat'] = tingkat;
    data['tahun_ajaran'] = tahunAjaran;
    data['semester'] = semester;
    data['jurusan'] = jurusan;
    data['description'] = description;
    data['school_id'] = schoolId;
    return data;
  }
}

class ModelDetailClassId {
  int statusCode;
  String message;
  DataDetailClassId data;

  ModelDetailClassId({this.statusCode, this.message, this.data});

  ModelDetailClassId.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data =
        json['data'] != null ? DataDetailClassId.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataDetailClassId {
  int id;
  int schoolId;
  String schoolName;
  String name;
  String description;
  String tahunAjaran;
  String semester;
  String jurusan;
  String tingkat;
  String invitationStudentCode;
  String invitationTeacherCode;
  String role;
  String namaWalikelas;
  String nomorTelpWalikelas;
  bool isSudahGabung;
  int totalGuru;
  int totalSiswa;
  String createdAt;
  String updatedAt;
  int totalCourse;

  DataDetailClassId(
      {this.id,
      this.schoolId,
      this.schoolName,
      this.name,
      this.description,
      this.tahunAjaran,
      this.semester,
      this.jurusan,
      this.tingkat,
      this.invitationStudentCode,
      this.invitationTeacherCode,
      this.role,
      this.namaWalikelas,
      this.nomorTelpWalikelas,
      this.isSudahGabung,
      this.totalGuru,
      this.totalSiswa,
      this.createdAt,
      this.updatedAt,
      this.totalCourse});

  DataDetailClassId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    schoolName = json['school_name'];
    name = json['name'];
    description = json['description'];
    tahunAjaran = json['tahun_ajaran'];
    semester = json['semester'];
    jurusan = json['jurusan'];
    tingkat = json['tingkat'];
    invitationStudentCode = json['invitation_student_code'];
    invitationTeacherCode = json['invitation_teacher_code'];
    role = json['role'];
    namaWalikelas = json['nama_walikelas'];
    nomorTelpWalikelas = json['nomor_telp_walikelas'];
    isSudahGabung = json['is_sudah_gabung'];
    totalGuru = json['total_guru'];
    totalSiswa = json['total_siswa'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    totalCourse = json['total_course'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['school_name'] = schoolName;
    data['name'] = name;
    data['description'] = description;
    data['tahun_ajaran'] = tahunAjaran;
    data['semester'] = semester;
    data['jurusan'] = jurusan;
    data['tingkat'] = tingkat;
    data['invitation_student_code'] = invitationStudentCode;
    data['invitation_teacher_code'] = invitationTeacherCode;
    data['role'] = role;
    data['nama_walikelas'] = namaWalikelas;
    data['nomor_telp_walikelas'] = nomorTelpWalikelas;
    data['is_sudah_gabung'] = isSudahGabung;
    data['total_guru'] = totalGuru;
    data['total_siswa'] = totalSiswa;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['total_course'] = totalCourse;
    return data;
  }
}

// List Teachers
class ModelListTeachers {
  int statusCode;
  String message;
  List<DataListTeachers> data;

  ModelListTeachers({this.statusCode, this.message, this.data});

  ModelListTeachers.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataListTeachers>[];
      json['data'].forEach((dynamic v) {
        data.add(DataListTeachers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataListTeachers {
  int userId;
  int classId;
  String fullName;
  String role;
  String createdAt;
  String updatedAt;

  DataListTeachers(
      {this.userId,
      this.classId,
      this.fullName,
      this.role,
      this.createdAt,
      this.updatedAt});

  DataListTeachers.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    classId = json['class_id'];
    fullName = json['full_name'];
    role = json['role'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = userId;
    data['class_id'] = classId;
    data['full_name'] = fullName;
    data['role'] = role;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class ModelCurrentClass {
  int statusCode;
  String data;

  ModelCurrentClass({this.statusCode, this.data});

  ModelCurrentClass.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['data'] = this.data;
    return data;
  }
}

class ClassMeModel {
  int statusCode;
  List<ClassMeData> data;

  ClassMeModel({this.statusCode, this.data});

  ClassMeModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    if (json['data'] != null) {
      data = <ClassMeData>[];
      json['data'].forEach((dynamic v) {
        data.add(ClassMeData.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ClassMeData {
  int id;
  int schoolId;
  String name;
  String tahunAjaran;
  String semester;
  String jurusan;
  String tingkat;
  String invitationStudentCode;
  String invitationTeacherCode;
  int userId;

  ClassMeData(
      {this.id,
      this.schoolId,
      this.name,
      this.tahunAjaran,
      this.semester,
      this.jurusan,
      this.tingkat,
      this.invitationStudentCode,
      this.invitationTeacherCode,
      this.userId});

  ClassMeData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    name = json['name'];
    tahunAjaran = json['tahun_ajaran'];
    semester = json['semester'];
    jurusan = json['jurusan'];
    tingkat = json['tingkat'];
    invitationStudentCode = json['invitation_student_code'];
    invitationTeacherCode = json['invitation_teacher_code'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['school_id'] = schoolId;
    data['name'] = name;
    data['tahun_ajaran'] = tahunAjaran;
    data['semester'] = semester;
    data['jurusan'] = jurusan;
    data['tingkat'] = tingkat;
    data['invitation_student_code'] = invitationStudentCode;
    data['invitation_teacher_code'] = invitationTeacherCode;
    data['user_id'] = userId;
    return data;
  }
}
