part of '_models.dart';

class LinkResultApi {
  String result;
  String message;
  int statusCode;

  LinkResultApi({this.result, this.message, this.statusCode});
}
