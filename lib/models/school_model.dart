part of '_models.dart';

class SchoolModel {
  int statusCode;
  String message;
  List<DataSchoolModel> data;

  SchoolModel({this.statusCode, this.message, this.data});

  SchoolModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataSchoolModel>[];
      json['data'].forEach((dynamic v) {
        data.add(DataSchoolModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataSchoolModel {
  int id;
  String invitationCode;
  String name;
  String provinsiId;
  String namaProvinsi;
  String kotaId;
  String namaKabupaten;
  String type;
  String grade;
  String address;
  String namaKepalaSekolah;
  String phoneNumber;
  String schoolEmail;
  String penanggungJawabEmail;
  String namaAdmin;
  bool isAdmin;
  bool personal;
  List<String> roles;
  bool isVerified;
  bool isApprove;
  String createdAt;
  String updatedAt;
  String deletedAt;

  DataSchoolModel(
      {this.id,
      this.invitationCode,
      this.name,
      this.provinsiId,
      this.namaProvinsi,
      this.kotaId,
      this.namaKabupaten,
      this.type,
      this.isAdmin,
      this.personal,
      this.grade,
      this.address,
      this.namaKepalaSekolah,
      this.phoneNumber,
      this.schoolEmail,
      this.penanggungJawabEmail,
      this.namaAdmin,
      this.roles,
      this.isVerified,
      this.isApprove,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  DataSchoolModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    invitationCode = json['invitation_code'];
    name = json['name'];
    provinsiId = json['provinsi_id'];
    namaProvinsi = json['nama_provinsi'];
    kotaId = json['kota_id'];
    namaKabupaten = json['nama_kabupaten'];
    type = json['type'];
    grade = json['grade'];
    address = json['address'];
    isAdmin = json['is_admin'];
    personal = json['personal'];
    namaKepalaSekolah = json['nama_kepala_sekolah'];
    phoneNumber = json['phone_number'];
    schoolEmail = json['school_email'];
    penanggungJawabEmail = json['penanggung_jawab_email'];
    namaAdmin = json['nama_admin'];
    roles = json['roles'].cast<String>();
    isVerified = json['is_verified'];
    isApprove = json['is_approve'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['invitation_code'] = invitationCode;
    data['name'] = name;
    data['provinsi_id'] = provinsiId;
    data['nama_provinsi'] = namaProvinsi;
    data['kota_id'] = kotaId;
    data['nama_kabupaten'] = namaKabupaten;
    data['type'] = type;
    data['is_admin'] = isAdmin;
    data['personal'] = personal;
    data['grade'] = grade;
    data['address'] = address;
    data['nama_kepala_sekolah'] = namaKepalaSekolah;
    data['phone_number'] = phoneNumber;
    data['school_email'] = schoolEmail;
    data['penanggung_jawab_email'] = penanggungJawabEmail;
    data['nama_admin'] = namaAdmin;
    data['roles'] = roles;
    data['is_verified'] = isVerified;
    data['is_approve'] = isApprove;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    return data;
  }
}

class ResultSchoolModel {
  int statusCode;
  String message;
  DataSchoolModel data;

  ResultSchoolModel({this.statusCode, this.message, this.data});

  ResultSchoolModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? DataSchoolModel.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ModelSchoolApproval {
  int statusCode;
  String message;
  List<DataSchoolApproval> data;

  ModelSchoolApproval({this.statusCode, this.data, this.message});

  ModelSchoolApproval.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataSchoolApproval>[];
      json['data'].forEach((dynamic v) {
        data.add(DataSchoolApproval.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataSchoolApproval {
  int schoolId;
  int userId;
  String email;
  String fullName;
  String createdAt;

  DataSchoolApproval(
      {this.schoolId, this.userId, this.email, this.fullName, this.createdAt});

  DataSchoolApproval.fromJson(Map<String, dynamic> json) {
    schoolId = json['school_id'];
    userId = json['user_id'];
    email = json['email'];
    fullName = json['full_name'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['school_id'] = schoolId;
    data['user_id'] = userId;
    data['email'] = email;
    data['full_name'] = fullName;
    data['created_at'] = createdAt;
    return data;
  }
}

class ModelCountApproval {
  int statusCode;
  DataCount data;

  ModelCountApproval({this.statusCode, this.data});

  ModelCountApproval.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    data = json['data'] != null ? DataCount.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataCount {
  int count;

  DataCount({this.count});

  DataCount.fromJson(Map<String, dynamic> json) {
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['count'] = count;
    return data;
  }
}

class JoinSchoolForm {
  int statusCode;
  String message;
  JoinSchoolFormData data;

  JoinSchoolForm({this.statusCode, this.message, this.data});

  JoinSchoolForm.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data =
        json['data'] != null ? JoinSchoolFormData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status_code'] = statusCode;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class JoinSchoolFormData {
  int iD;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String invitationCode;
  String name;
  String provinsiId;
  String kotaId;
  String type;
  String grade;
  String address;
  String namaKepalaSekolah;
  String phoneNumber;
  String schoolEmail;
  String penanggungJawabEmail;
  bool isVerified;
  bool isApprove;

  JoinSchoolFormData(
      {this.iD,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.invitationCode,
      this.name,
      this.provinsiId,
      this.kotaId,
      this.type,
      this.grade,
      this.address,
      this.namaKepalaSekolah,
      this.phoneNumber,
      this.schoolEmail,
      this.penanggungJawabEmail,
      this.isVerified,
      this.isApprove});

  JoinSchoolFormData.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    createdAt = json['CreatedAt'];
    updatedAt = json['UpdatedAt'];
    deletedAt = json['DeletedAt'];
    invitationCode = json['invitation_code'];
    name = json['name'];
    provinsiId = json['provinsi_id'];
    kotaId = json['kota_id'];
    type = json['type'];
    grade = json['grade'];
    address = json['address'];
    namaKepalaSekolah = json['nama_kepala_sekolah'];
    phoneNumber = json['phone_number'];
    schoolEmail = json['school_email'];
    penanggungJawabEmail = json['penanggung_jawab_email'];
    isVerified = json['is_verified'];
    isApprove = json['is_approve'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['CreatedAt'] = createdAt;
    data['UpdatedAt'] = updatedAt;
    data['DeletedAt'] = deletedAt;
    data['invitation_code'] = invitationCode;
    data['name'] = name;
    data['provinsi_id'] = provinsiId;
    data['kota_id'] = kotaId;
    data['type'] = type;
    data['grade'] = grade;
    data['address'] = address;
    data['nama_kepala_sekolah'] = namaKepalaSekolah;
    data['phone_number'] = phoneNumber;
    data['school_email'] = schoolEmail;
    data['penanggung_jawab_email'] = penanggungJawabEmail;
    data['is_verified'] = isVerified;
    data['is_approve'] = isApprove;
    return data;
  }
}
