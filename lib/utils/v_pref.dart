import 'package:shared_preferences/shared_preferences.dart';

class vPref {
  static void saveToken(String token) async {
    var ref = await SharedPreferences.getInstance();
    await ref.setString('token', token);
  }

  static Future<String> getToken() async {
    var ref = await SharedPreferences.getInstance();
    return ref.getString('token');
  }

  static Future<Map<String, String>> getHeader() async {
    var ref = await SharedPreferences.getInstance();
    return {'Authorization': "Bearer ${ref.getString("token")}"};
  }
}
