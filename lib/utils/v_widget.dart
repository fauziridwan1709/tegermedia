import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

Widget vText(String title,
    {Color color = Colors.black,
    double fontSize,
    FontWeight fontWeight,
    TextOverflow overflow = TextOverflow.fade,
    TextAlign align,
    bool number = false,
    bool SF = true,
    dynamic height,
    final bool money = false,
    final int decimalDigits,
    dynamic decoration,
    int maxLines}) {
  return Text(
    title != null && title != "null"
        ? money || number
            ? NumberFormat.currency(
                    locale: "id", symbol: !number ? "Rp " : "", decimalDigits: decimalDigits)
                .format(int.tryParse(title) ?? 0)
            : title
        : "",
    style: TextStyle(
      fontFamily: SF ? 'SF' : null,
      color: color,
      fontSize: fontSize,
      fontWeight: fontWeight,
      decoration: decoration,
      height: height,
    ),
    overflow: overflow,
    textAlign: align,
    maxLines: maxLines,
  );
}
