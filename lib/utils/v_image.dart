class VImage{
  //Provider
  static const provider = 'assets/images/png/provider/';
  static const kartuHalo = '${provider}kartu_halo.png';
  static const indosatMatrix = '${provider}indosat_matrix.png';
  static const smartfreen = '${provider}smartfren.png';
  static const tri = '${provider}tri.png';
  static const xl = '${provider}xl.png';
}