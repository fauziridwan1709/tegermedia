part of '../core/extension/_extension.dart';

class VNav {
  void payment(BuildContext context, int total, String orderID, String origin) {
    context.push<void>(
        PaymentMethodPage(origin: origin, orderID: orderID, totalPrice: total.toDouble()));
  }

  void detailSchool(
      BuildContext context, String title, String name, String Description, String imageUrl) {
    context.push<void>(DetailSchoolPage(
      title: title,
      Description: Description,
      imageUrl: imageUrl,
      name: name,
    ));
  }

  void webViewPayment(BuildContext context, String url, NicepayPayment data) {
    context.push<void>(WebViewPaymentPage(
      url: url,
      data: data,
    ));
  }
}
