import 'package:tegarmedia/features/payment/data/models/provider_model.dart';

class VConst {
  List<ProviderModels> providerModels = [
    ProviderModels('XL', [
      '0859',
      '0877',
      '0878',
      '0817',
      '0818',
      '0819',
    ]),
    ProviderModels('Telkomsel', [
      '0811',
      '0812',
      '0813',
      '0821',
      '0822',
      '0823',
      '0852',
      '0853',
      '0851',
    ]),
    ProviderModels('TRI', [
      '0898',
      '0899',
      '0895',
      '0896',
      '0897',
    ]),
    ProviderModels('Indosat', [
      '0814',
      '0815',
      '0816',
      '0855',
      '0856',
      '0857',
      '0858',
    ]),
    ProviderModels('AXIS', [
      '0832',
      '0833',
      '0838',
      '0831',
    ]),
    ProviderModels('Smartfren', [
      '0889',
      '0881',
      '0882',
      '0883',
      '0886',
      '0887',
      '0888',
      '0884',
      '0885',
    ]),
  ];
}
