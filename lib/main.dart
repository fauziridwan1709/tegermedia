import 'package:flutter/material.dart';
import 'package:tegarmedia/app.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/provider/_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SiswamediaConfig.init(Flavor.production);
  await setupLocator();
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider<QuizProviderKelas>(create: (_) => QuizProviderKelas()),
  ], child: MyApp()));
}
