part of '_base.dart';

abstract class BaseStateful<T extends StatefulWidget> extends State<T>
    with Diagnosticable
    implements BaseStateNormal {
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  @override
  void initState() {
    super.initState();
    init();
  }

  ScaffoldAttribute buildAttribute();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onBackPressed,
        child: SimpleScaffold(
          attr: buildAttribute(),
          body: SizingInformationBuilder(
            builder: (context, sizeInfo) {
              if (sizeInfo.deviceType == DeviceScreenType.mobile) {
                return buildNarrowLayout(context, sizeInfo);
              }
              return buildWideLayout(context, sizeInfo);
            },
          ),
          appBar: buildAppBar(context),
        ));
  }
}
