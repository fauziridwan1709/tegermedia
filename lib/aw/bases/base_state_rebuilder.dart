part of '_base.dart';

abstract class BaseStateReBuilder<K extends FutureState<K, dynamic>> {
  void init();

  Widget buildAppBar(BuildContext context);

  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<K> snapshot, SizingInformation sizeInfo);

  Widget buildWideLayout(
      BuildContext context, ReactiveModel<K> snapshot, SizingInformation sizeInfo);

  Future<void> retrieveData();

  @protected
  Future<bool> onBackPressed();
}

abstract class BaseStateReBuilder2<T extends StatefulWidget, K extends FutureState<T, K>>
    extends State<T> with Diagnosticable {
  @protected
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  @protected
  @mustCallSuper
  @override
  void initState() {
    super.initState();
    Initializer(
      reactiveModel: Injector.getAsReactive<K>(),
      rIndicator: refreshIndicatorKey,
      state: Injector.getAsReactive<K>().state.getCondition(),
      cacheKey: Injector.getAsReactive<K>().state.cacheKey,
    ).initialize();
    init();
  }

  void init();

  Widget buildAppBar(BuildContext context);

  Widget buildBody(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onBackPressed,
        child: Scaffold(
          appBar: buildAppBar(context),
          body: StateBuilder(
              observe: () => Injector.getAsReactive<K>(),
              builder: (context, snapshot) => buildBody(context)),
        ));
  }

  @protected
  Future<void> retrieveData();

  Future<void> onBackPressed();
}
