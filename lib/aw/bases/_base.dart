import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/aw/widgets/_widgets.dart';
import 'package:tegarmedia/core/screen/_screen.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/states/core/_core.dart';

part 'base_pagination_state.dart';
part 'base_state.dart';
part 'base_state_normal.dart';
part 'base_state_rebuilder.dart';
part 'base_stateful.dart';
