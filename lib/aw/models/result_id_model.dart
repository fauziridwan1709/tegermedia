part of '_models.dart';

class ResultIdModel extends ResultId {
  ResultIdModel({int id}) : super(id: id);

  ResultIdModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    return data;
  }
}
