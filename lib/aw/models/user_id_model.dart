part of '_models.dart';

class UserIdModel extends UserId {
  UserIdModel({int id}) : super(id: id);

  UserIdModel.fromJson(Map<String, dynamic> json) {
    id = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = id;
    return data;
  }
}
