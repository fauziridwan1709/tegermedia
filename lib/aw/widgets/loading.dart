part of '_widgets.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: SpinKitDoubleBounce(
      color: SiswamediaTheme.green,
      size: 30,
    ));
  }
}
