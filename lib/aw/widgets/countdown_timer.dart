part of '_widgets.dart';

class CustomCountdown extends StatelessWidget {
  const CustomCountdown({
    Key key,
    this.expirationTime,
    this.type,
    this.backgroundColor,
    this.digitColor,
    @required this.onExpiredWidget,
  }) : super(key: key);

  final String expirationTime;
  final CountdownType type;
  final Color backgroundColor;
  final Color digitColor;
  final Widget onExpiredWidget;

  @override
  Widget build(BuildContext context) {
    return CountdownTimer(
      endTime: DateTime.parse(expirationTime).millisecondsSinceEpoch,
      widgetBuilder: (_, time) {
        if (time == null) {
          // Navigator.popUntil(context, (route) => route.isFirst);
          return onExpiredWidget;
        }
        List<Widget> list = [];
        if (time.days != null) {
          list.add(Row(
            children: <Widget>[
              Text(time.days.toString()),
            ],
          ));
        }
        if (time.hours != null) {
          list.add(Row(
            children: <Widget>[
              Text(
                '${(time.hours < 10) ? '0' : ''}',
                style: SiswamediaTheme.countDown,
              ),
              Text(time.hours.toString(), style: SiswamediaTheme.countDown),
            ],
          ));
        }
        if (time.min != null) {
          String digit = time.min.toString();
          list.add(Row(
            children: <Widget>[
              if (time.hours == null) ...[
                _buildDigit('0'),
                _buildDigit('0'),
              ],
              Text(' Jam ', style: SiswamediaTheme.countDown),
              if (digit.length < 2) ...[
                _buildDigit('0'),
                _buildDigit(digit.substring(0, 1)),
              ] else ...[
                _buildDigit(digit.substring(0, 1)),
                _buildDigit(digit.substring(1, 2)),
              ],
            ],
          ));
        }
        if (time.sec != null) {
          String digit = time.sec.toString();
          list.add(Row(
            children: <Widget>[
              if (time.min == null) ...[
                _buildDigit('0'),
                _buildDigit('0'),
              ],
              Text(' Menit ', style: SiswamediaTheme.countDown),
              if (digit.length < 2) ...[
                _buildDigit('0'),
                _buildDigit(digit.substring(0, 1)),
              ] else ...[
                _buildDigit(digit.substring(0, 1)),
                _buildDigit(digit.substring(1, 2)),
              ],
            ],
          ));
        }
        list.add(WidthSpace(10));
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: list,
        );
      },
    );
  }

  Widget _buildDigit(String digit) {
    if (type == CountdownType.background) {
      return Text(
        digit,
        style: SiswamediaTheme.countDown.copyWith(color: digitColor),
      );
    }
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2, vertical: 3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: backgroundColor,
      ),
      child: Text(
        digit,
        style: SiswamediaTheme.countDown.copyWith(color: digitColor),
      ),
    );
  }
}
