part of '_widgets.dart';

class SimpleScaffold extends Scaffold {
  SimpleScaffold({ScaffoldAttribute attr, PreferredSizeWidget appBar, Widget body})
      : super(
            key: attr.scaffoldKey,
            resizeToAvoidBottomInset: attr.resize,
            backgroundColor: attr.backgroundColor,
            floatingActionButton: attr.FAB,
            floatingActionButtonLocation: attr.FABLocation,
            appBar: appBar,
            body: body);
}

class ScaffoldAttribute {
  GlobalKey<ScaffoldState> scaffoldKey;
  Color backgroundColor;
  Widget FAB;
  FloatingActionButtonLocation FABLocation;
  bool resize;

  ScaffoldAttribute(
      {this.scaffoldKey, this.backgroundColor, this.FAB, this.FABLocation, this.resize = true});
}
