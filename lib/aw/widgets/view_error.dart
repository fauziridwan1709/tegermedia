part of '_widgets.dart';

class ErrorView extends StatelessWidget {
  final dynamic error;
  ErrorView({this.error}) : assert(error != null);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return ListView(
      shrinkWrap: true,
      children: [
        if (MediaQuery.of(context).orientation == Orientation.portrait)
          SizedBox(height: width * .3),
        Image.asset(
          (error is SiswamediaException) ? 'assets/dialog/sorry_no_student.png' : 'assets/404.png',
          width: width * 1,
          height: width * .65,
        ),
        SizedBox(height: 20),
        Text(
          _mapFailureToMessage(error),
          style: semiBlack.copyWith(fontSize: 14),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40),
      ],
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case NetworkFailure:
        return 'No Internet Connection';
      case CacheFailure:
        return 'cache failure';
      case TimeoutFailure:
        return 'Timeout, try again';
      default:
        return 'Unexpected error';
    }
  }
}
