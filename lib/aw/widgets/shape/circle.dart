part of '_shape.dart';

class Circle extends Container {
  Circle({
    double size,
    Color color,
    List<BoxShadow> boxShadow,
    EdgeInsetsGeometry padding,
    EdgeInsetsGeometry margin,
    Widget child,
  }) : super(
            width: size,
            height: size,
            padding: padding,
            margin: margin,
            child: child,
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: color, boxShadow: boxShadow));
}
