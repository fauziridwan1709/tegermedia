part of '_widgets.dart';

class SimpleSelector extends StatelessWidget {
  final VoidCallback onTap;
  final String title;

  SimpleSelector({@required this.title, this.onTap})
      : assert(title != null, 'title cannot be null');

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: SiswamediaTheme.white,
        border: Border.all(color: SiswamediaTheme.border),
        borderRadius: radius(6),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SimpleText(title, style: SiswamediaTextStyle.subtitle),
                Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: SiswamediaTheme.green,
                  size: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
