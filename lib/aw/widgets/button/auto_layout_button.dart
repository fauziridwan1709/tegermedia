part of '_button.dart';

enum ButtonType {
  ///wrap text inside button
  text,

  ///wrap widget inside button
  widget,
}

class AutoLayoutButton extends CustomContainer {
  AutoLayoutButton(
      {Key key,
      String text,
      Color color,
      Color textColor,
      EdgeInsetsGeometry padding = const EdgeInsets.all(0),
      EdgeInsetsGeometry margin,
      Color backGroundColor,
      BorderRadius radius,
      ButtonType type = ButtonType.text,
      VoidCallback onTap,
      double fontSize,
      TextAlign textAlign,
      BoxBorder border,
      Widget child})
      : super(
            key: key,
            padding: padding,
            margin: margin,
            radius: radius,
            color: onTap == null ? SiswamediaTheme.border : backGroundColor,
            onTap: onTap,
            border: border,
            boxShadow: [SiswamediaTheme.shadowContainer],
            child: type == ButtonType.text
                ? SimpleText(text,
                    fontSize: fontSize,
                    style: SiswamediaTextStyle.subtitle,
                    textAlign: textAlign ?? TextAlign.left,
                    color: textColor)
                : child);
}
