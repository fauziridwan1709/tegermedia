part of '_widgets.dart';

enum SiswamediaTextStyle { title, subtitle, description }

class SimpleText extends Text {
  SimpleText(String text,
      {double fontSize,
      Color color,
      SiswamediaTextStyle style = SiswamediaTextStyle.description,
      String fontFamily,
      TextAlign textAlign,
      int maxLines})
      : super(text,
            maxLines: maxLines,
            overflow: TextOverflow.clip,
            textAlign: textAlign,
            style: _mapNewsTextStyle(style).copyWith(
                color: color, fontSize: fontSize, fontFamily: fontFamily));
}

TextStyle _mapNewsTextStyle(SiswamediaTextStyle style) {
  if (style == SiswamediaTextStyle.title) {
    return SiswamediaTheme.boldBlack;
  } else if (style == SiswamediaTextStyle.subtitle) {
    return SiswamediaTheme.semiBlack;
  } else {
    return SiswamediaTheme.descBlack;
  }
}
