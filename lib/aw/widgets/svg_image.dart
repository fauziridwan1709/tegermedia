part of '_widgets.dart';

//todo next update
class SVGImage extends SvgPicture {
  SVGImage(String assetName, {Color color, double height, double width})
      : super.asset('assets/images/svg/$assetName', color: color, height: height, width: width);
}
