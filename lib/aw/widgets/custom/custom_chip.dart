part of '_custom.dart';

class CustomChip extends StatelessWidget {
  const CustomChip({Key key, this.isSelected, this.label}) : super(key: key);

  final bool isSelected;
  final String label;

  @override
  Widget build(BuildContext context) {
    return CustomContainer(
      radius: BorderRadius.circular(4),
      border: Border.all(color: SiswamediaTheme.primary),
      color: isSelected ? SiswamediaTheme.primary : SiswamediaTheme.white,
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Text(
        label,
        style: FontTheme.openSans14w600Black
            .copyWith(color: isSelected ? SiswamediaTheme.white : SiswamediaTheme.primary),
      ),
    );
  }
}
