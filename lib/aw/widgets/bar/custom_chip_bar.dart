part of '_bar.dart';

class CustomChipBar extends StatelessWidget {
  const CustomChipBar(
      {Key key,
      @required this.chips,
      this.currentIndex = 0,
      this.mainAxisSpacing = 5.0,
      this.crossAxisSpacing = 5.0,
      this.onTap})
      : super(key: key);

  final int currentIndex;
  final double mainAxisSpacing;
  final double crossAxisSpacing;
  final List<CustomChip> chips;
  final ValueChanged<int> onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: SiswamediaTheme.white,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      height: 60,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: chips.length,
          itemBuilder: (_, index) {
            var getChip = chips[index];
            CustomChip _chip = CustomChip(label: getChip.label, isSelected: index == currentIndex);
            return CustomContainer(
              height: 30,
              onTap: () {
                onTap(index);
              },
              padding:
                  EdgeInsets.symmetric(horizontal: mainAxisSpacing, vertical: crossAxisSpacing),
              child: _chip,
            );
          }),
    );
  }
}
