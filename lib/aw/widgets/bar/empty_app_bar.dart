part of '_bar.dart';

class EmptyAppBar extends PreferredSize {
  EmptyAppBar()
      : super(
          preferredSize: Size.fromHeight(0),
          child: Container(),
        );
}
