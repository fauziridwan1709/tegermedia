part of '_widgets.dart';

class EmptyView extends StatelessWidget {
  const EmptyView({Key key, this.title, this.description, this.isEmpty, this.child})
      : super(key: key);

  final String title;
  final String description;
  final bool isEmpty;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    if (isEmpty) {
      return ListView(children: [
        HeightSpace(100),
        Image.asset(
          'assets/images/png/payment_empty.png',
          height: 180,
          width: 180,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title, style: SiswamediaTheme.boldBlack),
          ],
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 60),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  description,
                  style: SiswamediaTheme.descBlack.copyWith(fontSize: 12, color: Colors.grey),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ]);
    }
    return child;
  }
}
