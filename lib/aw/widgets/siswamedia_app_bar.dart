part of '_widgets.dart';

class SiswamediaAppBar2 extends AppBar {
  SiswamediaAppBar2({
    Key key,
    Widget title,
    @required BuildContext context,
    List<Widget> actions,
    IconData icon = Icons.arrow_back,
    Function additionalFunction,
    Function onBackFunction,
    PreferredSizeWidget bottom,
    Widget flexibleSpace,
    Widget leading,
    Color backgroundColor = Colors.white,
    Color shadowColor,
    bool centerTitle = true,
  })  : assert(context != null, 'context cannot be null'),
        super(
            key: key,
            title: title,
            leading: leading ??
                IconButton(
                    icon: Icon(icon),
                    color: SiswamediaTheme.green,
                    onPressed: () {
                      if (onBackFunction == null) {
                        if (additionalFunction != null) additionalFunction();
                        pop(context);
                      } else {
                        onBackFunction();
                      }
                    }),
            actions: actions,
            brightness: Brightness.light,
            centerTitle: centerTitle,
            backgroundColor: backgroundColor,
            shadowColor: shadowColor ?? Colors.grey.withOpacity(.2),
            flexibleSpace: flexibleSpace,
            bottom: bottom);
}
