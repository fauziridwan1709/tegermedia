part of '_widgets.dart';

class Content extends StatelessWidget {
  final String title;
  final Widget child;
  final Color textColor;
  final double height;
  final double titleFontSize;
  final TextStyle titleStyle;

  const Content(this.title,
      {@required this.child, this.titleStyle, this.textColor, this.titleFontSize, this.height = 8})
      : assert(child != null, 'child cannot be null');

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: (titleStyle ?? TextStyle())
                .copyWith(fontSize: titleFontSize, color: textColor ?? SiswamediaTheme.black),
          ),
          SizedBox(height: height),
          child
        ],
      ),
    );
  }
}
