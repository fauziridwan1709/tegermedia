part of '_avatar.dart';

class AvatarPP extends StatelessWidget {
  const AvatarPP({this.tag, this.height = 45, this.width = 45});

  final String tag;
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    var profile = GlobalState.profile();
    return Hero(
      tag: tag,
      child: Material(
        color: Colors.transparent,
        child: Container(
            width: width,
            height: height,
            // margin: EdgeInsets.only(right: 16),
            decoration: BoxDecoration(
                color: Colors.transparent,
                shape: BoxShape.circle,
                image: DecorationImage(image: AssetImage('assets/profile-default.png'))),
            foregroundDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.transparent,
                shape: BoxShape.rectangle,
                image: DecorationImage(
                    image: CacheImageServices.isCached
                        ? FileImage(CacheImageServices.imageFile)
                        : NetworkImage(
                            profile.state.profile.profileImagePresignedUrl,
                          ),
                    fit: BoxFit.cover))),
      ),
    );
  }
}
