import 'package:flutter/material.dart';
import 'package:tegarmedia/services/image/cache_image_service.dart';
import 'package:tegarmedia/states/global_state.dart';

part 'avatar_circle.dart';
part 'avatar_photo_profile.dart';
part 'avatar_rounded_rectangle.dart';
