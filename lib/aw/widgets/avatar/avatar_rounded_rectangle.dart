part of '_avatar.dart';

class AvatarRoundedRectangle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var profile = GlobalState.profile();
    return Hero(
      tag: 'image',
      child: Material(
        color: Colors.transparent,
        child: Container(
            width: 80,
            height: 80,
            foregroundDecoration: BoxDecoration(
                color: Colors.transparent,
                shape: BoxShape.circle,
                image: DecorationImage(image: AssetImage('assets/profile-default.png'))),
            decoration: BoxDecoration(
                color: Colors.transparent,
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: CacheImageServices.isCached
                        ? FileImage(CacheImageServices.imageFile)
                        : NetworkImage(
                            profile.state.profile.profileImagePresignedUrl,
                          ),
                    fit: BoxFit.cover))),
      ),
    );
  }
}
