part of '_widgets.dart';

class HeightSpace extends SizedBox {
  HeightSpace(double height) : super(height: height);
}

class WidthSpace extends SizedBox {
  WidthSpace(double width) : super(width: width);
}
