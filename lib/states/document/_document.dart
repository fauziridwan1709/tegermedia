import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/_services.dart';

part 'document_state.dart';
