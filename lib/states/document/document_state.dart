part of '_document.dart';

class DocumentState {
  List<String> listDocSchool = <String>[];
  List<String> listDocClass = <String>[];

  HashMap<int, String> classDoc = HashMap<int, String>();
  String schoolDoc;
  HashMap<int, List<Files>> documentClassMap = HashMap<int, List<Files>>();
  List<Files> documentSchool = <Files>[];
  HashMap<int, HashMap<DocumentModel, List<Files>>> classDocumentsMap;
  HashMap<DocumentModel, List<Files>> schoolDocuments;

  void addFile(int classId, String parent, Files file, String type) {}

  void addFolder(int classId, String type, DocumentModel model) {
    // if (type == 'sekolah') {
    //   schoolDocuments[model] = <Files>[];
    // } else {
    //   classDocumentsMap[classId][model] = <Files>[];
    // }
  }

  Future<void> retrieveClassDocument(int schoolId) async {
    try {
      Utils.log(schoolId, info: 'get document');
      final fireStoreInstance = FirebaseFirestore.instance;
      var result = await fireStoreInstance
          .collection('document')
          .where('school_id', isEqualTo: schoolId)
          .get();
      var dataMap = HashMap<int, HashMap<DocumentModel, List<Files>>>();
      var documents = HashMap<DocumentModel, List<Files>>();
      Utils.log(result.docs);
      if (result.docs.isNotEmpty) {
        await Future.forEach(result.docs,
            (QueryDocumentSnapshot element) async {
          var resp = DocumentModel.fromSnapshot(element);
          print('element' + resp.parent == resp.document);
          if (resp.classId == -1) {
            // var model = Files(name: resp.name,id: resp.document);
            // documentSchool.add(model);
            schoolDoc = resp.parent;
          } else {
            classDoc.update(resp.classId, (value) {
              // var model = Files(name: resp.name,id: resp.document);
              value = resp.parent;
              return value;
            }, ifAbsent: () {
              return resp.parent;
            });
          }
          // print(resp.name);
          // print(resp.classId);
          // await GoogleDrive.getAllFile(parent: resp.document).then((file) {
          //   if (resp.classId == -1) {
          //     documents[resp] = (file.files);
          //   } else {
          //     dataMap.update(resp.classId, (value) {
          //       value[resp] = file.files;
          //       return value;
          //     }, ifAbsent: () {
          //       var data = HashMap<DocumentModel, List<Files>>();
          //       data[resp] = file.files;
          //       return data;
          //     });
          //   }
          // });
        }).then((dynamic _) {
          print('dynamic');
          // listDocClass.add(value)
          classDocumentsMap = dataMap;
          schoolDocuments = documents;
        });
      }
    } on SocketException {
      print('no internet');
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('Timeout try again');
    } catch (e) {
      print(e);
      Utils.log(e, info: 'terjadi kesalahan');
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  void addToStack(String val, {String type = 'school'}) {
    if (type == 'school') {
      listDocSchool.add(val);
    } else {
      listDocClass.add(val);
    }
  }

  void removeFromStack({String type}) {
    if (type == 'school') {
      listDocSchool.removeLast();
    } else {
      listDocClass.removeLast();
    }
  }

  void resetStack() {
    print('dispo');
    listDocSchool.clear();
    listDocClass.clear();
  }

  Future<void> retrieveClassDoc(int schoolId) async {
    try {
      final fireStoreInstance = FirebaseFirestore.instance;
      var result = await fireStoreInstance
          .collection('document')
          .where('school_id', isEqualTo: schoolId)
          .get();
      var dataMap = HashMap<int, HashMap<DocumentModel, List<Files>>>();
      var documents = HashMap<DocumentModel, List<Files>>();
      if (result.docs.isNotEmpty) {
        await Future.forEach(result.docs,
            (QueryDocumentSnapshot element) async {
          var resp = DocumentModel.fromSnapshot(element);
          // print(resp.name);
          // print(resp.classId);
          await GoogleDrive.getAllFile(parent: resp.document).then((file) {
            if (resp.classId == -1) {
              documents[resp] = (file.files);
            } else {
              dataMap.update(resp.classId, (value) {
                value[resp] = file.files;
                return value;
              }, ifAbsent: () {
                var data = HashMap<DocumentModel, List<Files>>();
                data[resp] = file.files;
                return data;
              });
            }
          });
        }).then((dynamic _) {
          print('dynamic');
          classDocumentsMap = dataMap;
          schoolDocuments = documents;
        });
      }
    } on SocketException {
      print('no internet');
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('Timeout try again');
    } catch (e) {
      print(e);
      throw SiswamediaException('Terjadi kesalahan');
    }
  }
}
