part of '_absensi.dart';

class EditAbsensiState {
  EditAbsenModel editAbsenModel;

  EditAbsensiState({this.editAbsenModel});

  void editAbsen(int userId, int target,
      {bool isNew = false, bool isToday = true, int tanggal, int absenId, String desc}) {
    var data = editAbsenModel.data.recordAbsen.where((element) =>
        (element.userId == userId) &&
        int.parse(element.scheduleDate) == (isToday ? DateTime.now().day : tanggal));
    data.first.status = target;
    data.first.checkInTimeHour = DateTime.now().toLocal().hour;
    data.first.notes = desc;
    if (isNew) data.first.absenId = absenId;
  }

  Future<void> confirmUpdateAbsen() async {
    try {
      var resp = await EditAbsensiServices.updateAbsen();
      if (resp.statusCode != StatusCodeConst.success &&
          resp.statusCode != StatusCodeConst.created) {
        throw GeneralError();
      }
    } on TimeoutException {
      throw SiswamediaException('timeout');
    } on SocketException {
      throw SocketException('No Internet');
    } on GeneralError {
      throw SiswamediaException('Something failed');
    } catch (e) {
      throw SiswamediaException('Something failed');
    }
  }

  Future<void> getAbsen(int courseId) async {
    try {
      var resp = await EditAbsensiServices.getAbsenMonth(courseId);
      if (resp.statusCode != StatusCodeConst.success &&
          resp.statusCode != StatusCodeConst.created) {
        throw GeneralError();
      }
      editAbsenModel = resp;
    } on TimeoutException {
      throw SiswamediaException('timeout');
    } on SocketException {
      throw SocketException('no Internet');
    } on GeneralError {
      throw SiswamediaException('Something failed');
    } catch (e) {
      throw SiswamediaException('Something failed');
    }
  }

  Future<AbsenBulanan> monthAbsen(int courseId, int selectedMonth) async {
    try {
      var resp = await EditAbsensiServices.getAbsenMonth(courseId, selectedMonth: selectedMonth);

      if (resp.statusCode != StatusCodeConst.success &&
          resp.statusCode != StatusCodeConst.created) {
        throw GeneralError();
      }
      return resp.data;
    } on TimeoutException {
      throw SiswamediaException('timeout');
    } on SocketException {
      throw SocketException('no Internet');
    } on GeneralError {
      throw SiswamediaException('Something failed');
    } catch (e) {
      throw SiswamediaException('Something failed');
    }
  }
}
