part of '_absensi.dart';

class EditAbsenSelectedState {
  HashMap<int, AbsenBulanan> dataMap = HashMap<int, AbsenBulanan>();
  EditAbsenModel editAbsenModel;

  Future<void> retrieveData(int courseId, int month) async {
    try {
      var resp = await EditAbsensiServices.getAbsenMonth(courseId,
          selectedMonth: month);
      dataMap[month] = resp.data;
    } on SocketException {
      throw SocketException('No Internet Connection');
    } on TimeoutException {
      throw TimeoutException('Timeout, poor connection try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  void exportToExcel() {}
}
