import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/client/_client.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/models/absensi/student_permit.dart';
import 'package:tegarmedia/models/global_model/golbal_model.dart';
import 'package:tegarmedia/services/absensi/_absensi.dart';
import 'package:tegarmedia/states/auth/_auth.dart';
import 'package:tegarmedia/states/global_state.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'absensi_state.dart';
part 'edit_absen_selected_state.dart';
part 'edit_absen_state.dart';
