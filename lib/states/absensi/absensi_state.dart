part of '_absensi.dart';

class AbsensiState {
  AbsensiSummaryModel model;

  List<DataStudentPermit> dataIzin = [];

  Future<void> getIzin() async {
    var auth = Injector.getAsReactive<AuthState>().state;
    dataIzin.clear();
    try {
      var data = await AbsensiService.getDataIzin(
        role: auth.currentState.classRole,
        id: auth.currentState.classId.toString(),
      );

      dataIzin = data.data;
    } on SocketException {
      throw SocketException('No Internet connection');
    } on TimeoutException {
      throw TimeoutException('Timeout, try again');
    } catch (e) {
      print(e.toString());
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  Future<GlobalModel> deleteIzin(String izin) async {
    try {
      var data = await AbsensiService.deleteIzin(izin);
      return data;
    } on SocketException {
      throw SocketException('No Internet connection');
    } on TimeoutException {
      throw TimeoutException('Timeout, try again');
    } catch (e) {
      print(e.toString());
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  Future<void> getSummary(int id) async {
    try {
      var data = await SummaryAbsensi.getSummary(classId: id);
      model = data;
    } on SocketException {
      throw SocketException('No Internet connection');
    } on TimeoutException {
      throw TimeoutException('Timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  Future<void> getSummaryToday(int id) async {
    try {
      var data = await SummaryAbsensi.getSummary(classId: id);
      model = data;
    } on SocketException {
      throw SocketException('No Internet connection');
    } on TimeoutException {
      throw TimeoutException('Timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  Future<void> exportToExcel(BuildContext context, int selectedMonth, String className) async {
    // ignore: unawaited_futures
    showDialog<void>(
        context: context, builder: (context) => Center(child: CircularProgressIndicator()));
    var classId = GlobalState.auth().state.currentState.classId;
    var resp =
        await getIt('$baseUrl/v2/absensi/summary/export?classID=$classId&month=$selectedMonth');
    print(resp.statusCode);
    if (resp.statusCode.isSuccessOrCreated) {
      print('resp body');
      print(resp.body);
      var downloadsDirectory = await DownloadsPathProvider.downloadsDirectory;
      var status = await Permission.storage.status;
      var path =
          '${downloadsDirectory.path}/rekap_absen_${className}_${bulanFull[selectedMonth - 1]}.xlsx';
      if (status.isDenied) {
        await Permission.storage.request().isGranted.then((value) async {
          var filePath = File(path);
          filePath.writeAsBytesSync(resp.bodyBytes, flush: true, mode: FileMode.write);
          if (filePath.existsSync()) {
            Navigator.pop(context);
            CustomFlushBar.successFlushBar(
              'Berhasil mengekspor rekap quiz dan tugas',
              context,
            );
            await OpenFile.open(filePath.path);
          }
        });
      } else if (status.isDenied) {
        await Permission.storage.request().isGranted.then((value) async {
          var filePath = File(path);
          filePath.writeAsBytesSync(resp.bodyBytes, flush: true, mode: FileMode.write);
          if (filePath.existsSync()) {
            Navigator.pop(context);
            CustomFlushBar.successFlushBar(
              'Berhasil mengekspor rekap quiz dan tugas',
              context,
            );
            await OpenFile.open(filePath.path);
          }
        });
      } else {
        await Permission.storage.request().isGranted.then((value) async {
          print('mantap');
          var filePath = File(path);
          filePath.writeAsBytesSync(resp.bodyBytes, flush: true, mode: FileMode.write);
          if (filePath.existsSync()) {
            Navigator.pop(context);
            CustomFlushBar.successFlushBar(
              'Berhasil mengekspor rekap quiz dan tugas',
              context,
            );
            await OpenFile.open(filePath.path);
          }
        });
      }
    } else {
      context.pop();
      CustomFlushBar.errorFlushBar('Tidak ada absen pada bulan tersebut', context);
    }
  }
}
