part of '_home.dart';

class SchoolNewsState {
  HomeModel sekolah;

  bool get getCondition => sekolah != null;

  Future<void> retrieveData() async {
    try {
      var resp = await HomeServices.getData('SCHOOL NEWS');
      sekolah = resp;
    } catch (e) {
      throw Exception();
    }
  }
}
