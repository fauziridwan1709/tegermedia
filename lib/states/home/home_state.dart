part of '_home.dart';

class HomeState {
  HomeModel sekolah;
  HomeModel news;
  HomeModel dana;
  HomeModel youtube;

  Future<void> retrieveData() async {
    sekolah = await HomeServices.getData('SCHOOL NEWS');
    news = await HomeServices.getData('SERTIMEDIA NEWS');
    dana = await HomeServices.getData('SPESIAL BUAT KAMU');
    youtube = await HomeServices.getData('SISWAMEDIA YOUTUBE');
  }

  bool getCondition() {
    return sekolah != null && news != null && dana != null && youtube != null;
  }

  Future<void> retrieveNews() async {}
}
