part of '_interaction.dart';

class InteractionState implements FutureState<InteractionState, int> {
  List<InteractionModel> interactionData = [];
  List<InteractionModel> interactionRiwayat = [];
  List<InteractionModel> interactionBerlangsung = [];
  List<InteractionModel> interactionSelected = [];
  List<StudentInteraction> studentInteraction = [];
  List<StudentInteraction> studentRiwayat = [];
  List<StudentInteraction> studentSelected = [];
  List<StudentInteraction> studentBerlangsung = [];
  bool isRiwayat = false;
  List<DetailChats> detailsChats = [];
  DataParticipants detailsParticipants;
  int count = 0;
  int chatCount = 0;

  // @deprecated

  ///dont get instance here
  var authState = GlobalState.auth().state;

  void clearData() {
    interactionData = [];
    count = 0;
  }

  void setCount(int value) {
    count = value;
  }

  @override
  String cacheKey;

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  @override
  Future<void> updateData(BuildContext context, int data) {
    // TODO: implement updateData
    throw UnimplementedError();
  }

  void selectedRiwayat() {
    isRiwayat = !isRiwayat;
    getData();
  }

  Future<void> joinParticipant(String room_id) async {
    try {
      if (authState.currentState.classRole == 'SISWA') {
        await InteractionServices.joinParticipants(room_id);
      }
    } catch (ex) {
      print(ex);
    }
  }

  void sendMessage(String message, String room_id) async {
    try {
      var response = await InteractionServices.sendMessage(message, room_id);

      response;
    } catch (ex) {
      print('log error' + ex.toString());
    }
  }

  Future<void> endClass(String id) async {
    var status = '';
    //todo
    var authState2 = GlobalState.auth().state;
    if (authState.currentState.classRole == 'SISWA') {
      status = 'reference/';
    } else {
      status = 'room/';
    }
    var response = await InteractionServices.endClass(status + id);
    print('end class' + response.message);
    return response;
  }

  void realTimeAddData(DetailChats chat) {
    chatCount++;
    detailsChats.add(chat);
    print('data' + chatCount.toString());
  }

  void realTimeAddParticipants(DataParticipants participants) {
    chatCount++;
    detailsParticipants = participants;
    print('data' + chatCount.toString());
  }

  void clearChats() {
    chatCount = 0;
  }

  void getChats(String id, String room_id) async {
    try {
      print('add');

      var response = await InteractionServices.getChats(id, room_id: room_id);

      clearChats();
      detailsChats.clear();
      response.forEach((element) {
        detailsChats.add(element);
      });
      detailsChats = detailsChats.reversed.toList();
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }

  void getParticipants(String id, String room_id) async {
    try {
      print('add');

      var response =
          await InteractionServices.getParticipants(id, room_id: room_id);

      print('response : ' + response.data.name);
      detailsParticipants = response.data;
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }

  void getData() {
    if (isRiwayat) {
      studentSelected = studentRiwayat;
      interactionSelected = interactionRiwayat;
    } else {
      studentSelected = studentBerlangsung;
      interactionSelected = interactionBerlangsung;
    }
    print(interactionSelected.length.toString());
  }

  @override
  Future<void> retrieveData(int data) async {
    try {
      var idClass = '';
      interactionSelected.clear();
      studentSelected.clear();
      detailsChats.clear();
      studentRiwayat.clear();
      studentBerlangsung.clear();
      interactionRiwayat.clear();
      interactionBerlangsung.clear();
      var authState2 = GlobalState.auth().state.currentState;
      Utils.log(GlobalState.auth().state.currentState, info: 'it is null');
      Utils.log(authState.currentState, info: 'make sure not null');
      if (authState2.classRole.isSiswaOrOrtu) {
        idClass = authState2.classId.toString();
        studentInteraction = [];
        var response = await InteractionServices.getStudent(classId: idClass);
        // if (response.data.isEmpty) throw EmptyException();
        studentInteraction = response.data;
        int i = 0;
        studentInteraction.forEach((element) {
          if (element.opened) {
            studentRiwayat.add(element);
            print(i.toString());
          } else {
            studentBerlangsung.add(element);
            print(i.toString());
          }
          i++;
        });
      } else {
        idClass = authState.currentState.classId.toString();
        interactionData = [];
        var response =
            await InteractionServices.getStudyRoomTeacher(id: idClass);
        if (response.data.isEmpty) throw EmptyException();
        interactionData = response.data;
        interactionData.forEach((element) {
          if (element.started) {
            interactionRiwayat.add(element);
          } else {
            interactionBerlangsung.add(element);
          }
        });
      }
      getData();
    } on SocketException {
      interactionData = [];
      studentInteraction = [];
      throw SiswamediaException('There is no Internet Connection');
    } on EmptyException {
      interactionData = [];
      studentInteraction = [];
      print('here');
    } catch (e) {
      print(e);
      throw SiswamediaException('there is no Internet Connection');
    }
  }

  Future<ResultSchoolModel> deletedId(int id) async {
    try {
      var response = await InteractionServices.deleteRoom(idInteraction: id);

      return response;
    } on SocketException {
      throw SiswamediaException('There is no Internet Connection');
    } on EmptyException {
      print('here');
      throw SiswamediaException('Empty');
    }
  }
}
