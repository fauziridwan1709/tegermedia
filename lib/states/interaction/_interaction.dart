import 'package:flutter/src/widgets/framework.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/models/interaction/chat.dart';
import 'package:tegarmedia/models/interaction/participants.dart';
import 'package:tegarmedia/models/interaction/studentInteraksi.dart';

import 'package:tegarmedia/services/interaction/_interaction.dart';
import '../../core/_core.dart';
import '../_states.dart';

part 'interaction_state.dart';
