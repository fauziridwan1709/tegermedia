part of '_conference.dart';

class ConferenceState {
  List<ConferenceListModelData> conferenceModel;

  ConferenceState({this.conferenceModel});

  Future<void> retrieveData(bool isOrtu) async {
    try {
      var response = await ConferenceServices.getConference(isOrtu: isOrtu);
      if (response.data.isEmpty) throw EmptyException();
      conferenceModel = response.data;
    } on SocketException {
      conferenceModel = null;

      throw await SiswamediaException('There is no Internet Connection');
    } on EmptyException {
      conferenceModel = null;
      print('here');
      throw SiswamediaException('Tidak ada konferensi terbaru');
    }
  }

  Future<void> createConference() async {}
}
