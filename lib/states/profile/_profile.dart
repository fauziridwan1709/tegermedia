import 'dart:async';
import 'dart:io';

import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/services/image/cache_image_service.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/states/future.dart';

part 'profile_state.dart';
