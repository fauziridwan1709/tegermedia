part of '_profile.dart';

class ProfileState implements FutureState<ProfileState, int> {
  @override
  String cacheKey;

  bool _isPhotoProfileCached;
  DataUserProfile2 profile;
  String userName;

  ///whether photo profile cached or not
  bool get isPPCached => _isPhotoProfileCached;

  void setProfile(DataUserProfile2 userDetail) {
    profile = userDetail;
  }

  @override
  Future<void> retrieveData({int data}) async {
    try {
      var resp = await ProfileServices.getProfile().timeout(GLOBAL_TIMEOUT);
      profile = resp.data;
      await CacheImageServices.isPhotoProfileCached(profile.id);
      _isPhotoProfileCached = CacheImageServices.isCached;
      Logger().d(_isPhotoProfileCached);
      await SharedPreferences.getInstance().then((value) {
        value.setString(FULL_NAME, profile.name);
        // profile.userId = value.getString('user_id').toInteger();
      });
      print('berhasil get profile');
    } on SocketException {
      await SharedPreferences.getInstance().then((value) {
        userName = value.getString(FULL_NAME);
      });
      print('socket');
      throw SocketException('No Internet Connection');
    } on TimeoutException catch (e) {
      await SharedPreferences.getInstance().then((value) {
        userName = value.getString(FULL_NAME);
      });
      print('timeout');
      throw SiswamediaException(e.message);
    } catch (e) {
      print(e);
      print('global_error');
      await SharedPreferences.getInstance().then((value) {
        userName = value.getString(FULL_NAME);
      });
      print('error get profile');
      throw SiswamediaException(e.toString());
    }
  }

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  void changeImage(String image) {
    profile.profileImagePresignedUrl = image;
  }
}
