part of '_quiz.dart';

class QuizState {
  List<QuizItem> _item = <QuizItem>[];
  MateriQuizDetail _detail = MateriQuizDetail();
  String _url = '';

  List<QuizItem> get item => _item;
  String get url => _url;
  MateriQuizDetail get detail => _detail;

  Future<void> setDetail(MateriQuizDetail value) async {
    _detail = value;
  }

  Future<void> setQuestion(List<QuizItem> newValue) async {
    _item = newValue;
  }

  Future<void> addQuestion(QuizItem newValue) async {
    _item.add(newValue);
  }

  Future<void> setUrl(String value) async {
    _url = value;
  }
}
