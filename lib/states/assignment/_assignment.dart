import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/models/user/_user.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/states/future.dart';

import '../../utils/v_utils.dart';

part 'assignment_state.dart';
part 'filter_assignment_state.dart';
part 'select_class_state.dart';
