part of '_assignment.dart';

class AssignmentState implements FutureState<AssignmentState, int> {
  @override
  String cacheKey;

  String role;
  String param;
  ModelAssignmentsNew assignmentData;

  void initType(String role, UserCurrentState state) {
    this.role = role;
    // if (state.type == 'PERSONAL') {
    // param = 'course_id=${state.schoolId}&class_id=0';
    // } else {
    param =
        'school_id=${state.schoolId}&class_id=${state.classId}${state.classRole == 'ORTU' ? '&type=ORTU' : ''}';
    // }
  }

  //todo implement priority queue
  void addAssignment(ListData data) {
    assignmentData.data.listData =
        List.from(assignmentData.data.listData.reversed);
    assignmentData.data.listData.add(data);
    assignmentData.data.listData =
        List.from(assignmentData.data.listData.reversed);
  }

  void deleteAssignment(int id, GlobalKey<AnimatedListState> key) {
    // tugasData.data.removeWhere((element) => element.id == id);
    assignmentData.data.listData.removeAt(id);
  }

  void updateAssignment(int id, DataAssignmentReference data) {
    var result =
        assignmentData.data.listData.firstWhere((element) => element.id == id);
    result.id = data.id;
    result.description = data.description;
    result.startDate = data.startDate;
    result.endDate = data.endDate;
  }

  void kumpulkanAssignment(int id) {
    var result = assignmentData.data.listData
        .firstWhere((element) => element.assessmentId == id);
    result.status = 'Dikumpulkan';
  }

  @override
  Future<void> retrieveData({int data, List<int> course, int page}) async {
    try {
      var response = await AssignmentServices.getAssignment(
              param: param, role: role, course: course, page: page)
          .timeout(GLOBAL_TIMEOUT);
      assignmentData = response;
      vUtils.setLog('berhasil atau engga');
    } on SocketException {
      throw SocketException('Tidak ada koneksi Internet');
    } on TimeoutException {
      throw TimeoutException('Timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi Kesalahan');
    }
  }

  void updateComment({ListData data}) {
    data.newComment = false;
  }

  @override
  Future<void> createData(BuildContext context, int data) {
    // TODO: implement createData
    throw UnimplementedError();
  }

  @override
  Future<void> deleteData(BuildContext context, int id) {
    // TODO: implement deleteData
    throw UnimplementedError();
  }

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  @override
  Future<void> updateData(BuildContext context, int data) {
    // TODO: implement updateData
    throw UnimplementedError();
  }
}
