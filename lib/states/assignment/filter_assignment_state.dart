part of '_assignment.dart';

const String TUGAS_STATUS_KEY = 'tugas_status_filter';
const String TUGAS_SORT_KEY = 'tugas_sort_filter';

const String TUGAS_SORT_KEY_GURU = 'tugas_sort_filter_guru';

class FilterAssignmentState {
  FilterTugas filter;

  Future<void> retrieveCurrentFilter() async {
    var prefs = await SharedPreferences.getInstance();
    var statuses = prefs.getStringList(TUGAS_STATUS_KEY) ?? listStatus;
    var sortBy = prefs.getString(TUGAS_SORT_KEY) ?? listSort.elementAt(0);
    var statusMap = <String, bool>{};
    var courseMap = <int, bool>{};
    listStatus.forEach((element) {
      statusMap.addAll(<String, bool>{'$element': statuses.contains(element)});
    });
    var resp = await ClassServices.getCourseByClass(
        classId: GlobalState.auth().state.currentState.classId);
    resp.data.forEach((element) {
      courseMap.addAll(<int, bool>{element.id: true});
    });
    print(statusMap);
    filter = FilterTugas(status: statusMap, sort: sortBy, course: courseMap);
  }

  Future<void> changeStatus(String value) async {
    var prefs = await SharedPreferences.getInstance();
    filter.status.update(value, (value) => !value);
    var data = <String>[];
    filter.status.forEach((key, value) {
      if (value) {
        data.add(key);
      }
    });
    await prefs.setStringList(TUGAS_STATUS_KEY, data);
    print(await prefs.getStringList(TUGAS_STATUS_KEY));
  }

  Future<void> changeStatusCourse(int value) async {
    filter.course.update(value, (value) => !value);
    var course = <int>[];
    filter.course.forEach((key, value) {
      if (value == true) {
        course.add(key);
      }
    });
    print(course);
    GlobalState.assignment()
        .setState((s) => s.retrieveData(course: course, page: 1));
    // GlobalState.assignment().state.retrieveData();
    var data = <int>[];
    filter.course.forEach((key, value) {
      if (value) {
        data.add(key);
      }
    });
  }

  Future<void> updateSort(String value) async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString(TUGAS_SORT_KEY, value);
    filter.sort = value;
  }
}

class FilterAssignmentStateGuru {
  FilterTugas filter;

  Future<void> retrieveCurrentFilter() async {
    var prefs = await SharedPreferences.getInstance();
    var sortBy = prefs.getString(TUGAS_SORT_KEY_GURU) ?? listSort.elementAt(0);
    var statusMap = <String, bool>{};
    // listStatus.forEach((element) {
    //   statusMap.addAll(<String, bool>{'$element': statuses.contains(element)});
    // });
    // print(statusMap);
    filter = FilterTugas(status: statusMap, sort: sortBy);
  }

  Future<void> changeStatus(String value) async {
    var prefs = await SharedPreferences.getInstance();
    filter.status.update(value, (value) => !value);
    var data = <String>[];
    filter.status.forEach((key, value) {
      if (value) {
        data.add(key);
      }
    });
    await prefs.setStringList(TUGAS_STATUS_KEY, data);
    print(await prefs.getStringList(TUGAS_STATUS_KEY));
  }

  Future<void> changeStatusCourse(int value) async {
    filter.course.update(value, (value) => !value);
    var data = <int>[];
    filter.course.forEach((key, value) {
      if (value) {
        data.add(key);
      }
    });
  }

  Future<void> updateSort(String value) async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString(TUGAS_SORT_KEY, value);
    filter.sort = value;
  }
}

class FilterTugas {
  Map<String, bool> status;
  Map<int, bool> course;
  String sort;

  FilterTugas({this.status, this.course, this.sort});
}

const listStatus = <String>[
  'Belum Dikerjakan',
  'Selesai',
  'Dikumpulkan',
  'Dikembalikan'
];
const listSort = <String>['Tugas terbaru'];

const listSortGuru = <String>['Tugas terbaru', 'Terdekat'];
