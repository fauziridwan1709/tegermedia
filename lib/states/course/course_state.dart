part of '_course.dart';

class CourseState implements FutureState<CourseState, int> {
  @override
  String cacheKey;

  List<CourseDetail> courses;

  HashMap<int, CourseDetail> courseMap = HashMap<int, CourseDetail>();

  @override
  Future<void> retrieveData({int data}) async {
    try {
      var course = await ClassServices.getMeCourse();
      courses = course.data;
    } on SocketException {
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  @override
  Future<void> createData(BuildContext context, int data) {
    // TODO: implement createData
    throw UnimplementedError();
  }

  @override
  Future<void> deleteData(BuildContext context, int id) {
    // TODO: implement deleteData
    throw UnimplementedError();
  }

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  @override
  Future<void> updateData(BuildContext context, int data) {
    // TODO: implement updateData
    throw UnimplementedError();
  }
}
