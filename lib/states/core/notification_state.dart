part of '_core.dart';

class NotificationState implements FutureState<NotificationState, int> {
  @override
  String cacheKey = 'NOTIFICATION';

  int count = 0;
  List<DataNotification> notificationModel;

  void setCount(int value) {
    count = value;
  }

  Future<void> getCount() async {
    try {
      var countNotifications = await NotificationServices.getCountNotif();
      if (countNotifications.statusCode != StatusCodeConst.success) {
        throw SiswamediaException('error');
      }
      count = countNotifications.data.count;
    } on SocketException {
      throw SocketException('no Internet connection');
    } catch (e) {
      throw SiswamediaException('Cannot get notification');
    }
  }

  @override
  Future<void> retrieveData({int data}) async {
    try {
      var response = await NotificationServices.getNotifications();
      notificationModel = response.data;
    } on SocketException {
      notificationModel = null;

      throw await SiswamediaException('There is no Internet Connection');
    } on EmptyException {
      notificationModel = null;
      print('here');
    }
  }

  @override
  Future<void> createData(BuildContext context, int data) {
    // TODO: implement createData
    throw UnimplementedError();
  }

  @override
  Future<void> deleteData(BuildContext context, int id) {
    // TODO: implement deleteData
    throw UnimplementedError();
  }

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  @override
  Future<void> updateData(BuildContext context, int data) {
    // TODO: implement updateData
    throw UnimplementedError();
  }
}
