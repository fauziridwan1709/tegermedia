part of '_core.dart';

class MultiSelectClassState implements FutureState<MultiSelectClassState, int> {
  @override
  String cacheKey;

  List<DataMultiSelectClass> listData;
  HashMap<int, DataMultiSelectClass> selected =
      HashMap<int, DataMultiSelectClass>();

  String searchKey = '';

  void switchSelected(DataMultiSelectClass value) {
    if (selected.containsKey(value.classId) &&
        selected[value.classId].courseId == value.courseId) {
      selected.remove(value.classId);
    } else {
      selected[value.classId] = value;
    }
  }

  void clearSelect() {
    selected = HashMap<int, DataMultiSelectClass>();
  }

  void search(String value) {
    searchKey = value;
  }

  void fetchSelected(DataMultiSelectClass value) {
    selected[value.classId] = value;
  }

  @override
  Future<void> retrieveData({int data}) async {
    try {
      var response = await AssignmentServices.getAssignmentV2();
      listData = response.data;
    } on SocketException {
      throw SocketException('No internet connection');
    } on TimeoutException {
      throw TimeoutException('Timeout. try again');
    } catch (e) {
      throw SiswamediaException('Mohon maaf, Terjadi kesalahan');
    }
  }

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  @override
  Future<void> createData(BuildContext context, int data) {
    // TODO: implement createData
    throw UnimplementedError();
  }

  @override
  Future<void> deleteData(BuildContext context, int id) {
    // TODO: implement deleteData
    throw UnimplementedError();
  }

  @override
  Future<void> updateData(BuildContext context, int data) {
    // TODO: implement updateData
    throw UnimplementedError();
  }
}
