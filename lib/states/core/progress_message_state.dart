part of '_core.dart';

class ProgressMessageState {
  String message;

  ProgressMessageState({this.message = ''});

  void updateProgress(String m) {
    message = m;
  }

  void onDone() {
    message = '';
  }
}
