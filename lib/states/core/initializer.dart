part of '_core.dart';

enum InitializerDelay {
  min1,
  min5,
  sec15,
}

///initializer with refresh indicatorKey
class Initializer {
  ReactiveModel rm;
  String ck;
  GlobalKey<RefreshIndicatorState> rIndicator;
  bool state;
  InitializerDelay delay;

  Initializer(
      {@required ReactiveModel reactiveModel,
      @required String cacheKey,
      @required this.rIndicator,
      @required this.state,
      this.delay = InitializerDelay.sec15})
      : assert(reactiveModel != null),
        assert(cacheKey != '' && cacheKey != null),
        assert(rIndicator != null),
        assert(state != null),
        rm = reactiveModel,
        ck = cacheKey;

  void initialize() {
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(ck)) {
        var date = value.getString(ck);
        if (_getDelayTime(delay, date) && state) {
        } else {
          await value.setString(ck, DateTime.now().toIso8601String());
          await rIndicator.currentState?.show();
        }
      } else {
        await value.setString(ck, DateTime.now().toIso8601String());
        await rIndicator.currentState?.show();
      }
    });
  }
}

bool _getDelayTime(InitializerDelay delay, String date) {
  if (delay == InitializerDelay.min1) {
    return isDifferenceLessThanMin(date);
  } else if (delay == InitializerDelay.min5) {
    return isDifferenceLessThanFiveMin(date);
  } else if (delay == InitializerDelay.sec15) {
    return isDifferenceLessThanFifteen(date);
  } else {
    return isDifferenceLessThanFiveMin(date);
  }
}
