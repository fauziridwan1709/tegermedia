part of '_core.dart';

class ProgressUploadState {
  int progress = 0;
  int contentLength = 80;

  void setContent(int value) {
    contentLength = value;
  }

  void setProgress(int value) {
    progress = value;
  }
}
