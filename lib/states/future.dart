abstract class FutureState<T, K> {
  String cacheKey;

  bool getCondition();

  Future<void> retrieveData({K data});
}
