part of '_auth.dart';

class AuthState {
  AuthState({this.currentState});
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  String _authSiswamediaAPI = '';
  String refreshToken;
  String selectedClassRole;
  bool isLogin = false;
  bool isNoClasses = true;
  String _authCode;

  List<StudentIdList> anak;
  List<String> favoriteMenu;

  UserCurrentState currentState;
  final myClass = <ClassMeData>[];
  DataSchoolModel chosenSchool;

  String get token => _authSiswamediaAPI;
  String get authCode => _authCode;

  Future<void> initialize() async {
    print('initialize');
    await prefs.then((pref) async {
      if (pref.get(AUTH_KEY) == null || !pref.containsKey(FLAG)) {
        isLogin = false;
        if (!pref.containsKey(FLAG)) {
          await resetState();
        }
        if (pref.get(AUTH_KEY) == null) {
          isNoClasses = true;
        }
      } else {
        isNoClasses = pref.getBool('is_no_class');
        isLogin = true;
      }
    });
  }

  Future<void> resetState() async {
    var data = UserCurrentState(
      schoolId: null,
      schoolName: null,
      schoolRole: [],
      type: null,
      classId: null,
      className: null,
      classRole: null,
      isAdmin: null,
    );
    await ClassServices.setCurrentState(data: json.encode(data.toJson()));
    await SiswamediaGoogleSignIn.getInstance().signOut();
  }

  Future<void> loginApple(BuildContext context) async {
    try {
      // await SignInWithApple.
      // await AppleSignIn.performRequests([
      //   AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
      // ]).then((value) async {
      //   Utils.log(value.credential.email, info: 'apple email');
      //   Utils.log(value.credential.identityToken, info: 'apple email');
      // });
      await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      ).then((value) async {
        print('this is value');
        Utils.log(value.email, info: 'apple email');
        Utils.log(value.identityToken, info: 'apple identity token');
        print(value.email);
        pop(context);
        var email = Encriptor.encrypt(value.email);
        await UsersServices.signIn(reqData: <String, dynamic>{
          'email': email,
          'register_type': 'apple',
          'password': value.identityToken,
        })
            .timeout(Duration(seconds: 10),
                onTimeout: () => throw TimeoutException('gagal try again'))
            .then((statusCode) async {
          statusCode.translate(
              ifSuccess: () => isLogin = true,
              ifElse: () {
                CustomFlushBar.errorFlushBar('Gagal Login', context);
              });
        });
      });
    } catch (e) {
      CustomFlushBar.errorFlushBar('Gagal Login', context);
    }
  }

  Future<int> login(BuildContext context) async {
    try {
      var signInInstance = SiswamediaGoogleSignIn.getInstance();
      print(signInInstance.clientId);
      await signInInstance.signOut();
      await signInInstance.signIn();
      var googleSignInAuth =
          await signInInstance.currentUser.authentication.timeout(GLOBAL_TIMEOUT);

      var data = SiswamediaGoogleSignIn.getInstance();
      print('token access ' + googleSignInAuth.accessToken);
      // print('id token' + googleSignInAuth.idToken);
      print('gmail ${data.currentUser}');
      print('gmail.com');
      await UsersServices.signIn(reqData: <String, dynamic>{
        'email': data.currentUser.email,
        'register_type': 'gmail',
        'password': googleSignInAuth.accessToken,
      })
          .timeout(GLOBAL_TIMEOUT, onTimeout: () => throw TimeoutException('gagal try again'))
          .then((statusCode) async {
        if (statusCode == 200) {
          await FirebaseMessaging.instance.getToken().then((token) async {
            print('token: $token');
            await NotificationServices.registerToken(token: token);
          });
          isLogin = true;
        } else {
          throw SiswamediaException('Login Failed');
        }
      });
      return StatusCodeConst.success;
    } on SocketException {
      return StatusCodeConst.noInternet;
    } on SiswamediaException catch (e) {
      print(e);
      return StatusCodeConst.general;
    } on UnprocessableEntityException catch (e) {
      return 422;
    } on PlatformException catch (e) {
      print(e.code);
      if (e.code == 'network_error') {
        return StatusCodeConst.noInternet;
      }
      print(e.stacktrace);
      print(e.details);
      print(e);
      return StatusCodeConst.platform;
    } on TimeoutException {
      return StatusCodeConst.timeout;
    } catch (e) {
      print(e);
      print('ad');
      return 404;
    }
  }

  Future<int> loginV2(BuildContext context) async {
    try {
      var response = await Oauth2.getLoginUrl();
      await navigate(context, WebViewHome(authorize: true, url: response));
      await UsersServices.signIn(reqData: <String, dynamic>{
        // 'email': data.currentUser.email,
        'register_type': 'gmail',
        // 'password': googleSignInAuth.accessToken,
      })
          .timeout(Duration(seconds: 10),
              onTimeout: () => throw TimeoutException('gagal try again'))
          .then((statusCode) async {
        if (statusCode == 200) {
          isLogin = true;
        } else {
          throw SiswamediaException('Login Failed');
        }
      });
      return StatusCodeConst.success;
    } on SocketException {
      return StatusCodeConst.noInternet;
    } on SiswamediaException catch (e) {
      print(e);
      return StatusCodeConst.general;
    } on PlatformException catch (e) {
      print(e.code);
      print(e);
      return StatusCodeConst.platform;
    } on TimeoutException {
      return StatusCodeConst.timeout;
    } catch (e) {
      print(e);
      print('ad');
      return 404;
    }
  }

  Future<int> logout(BuildContext context) async {
    try {
      await FirebaseMessaging.instance.deleteToken();
      removeToken();
      await SiswamediaGoogleSignIn.getInstance().signOut().timeout(Duration(seconds: 10),
          onTimeout: () => throw TimeoutException('Gagal Logout, Coba Lagi'));
      Cleaner().cleanLogout();
      return StatusCodeConst.success;
    } on SocketException {
      return StatusCodeConst.noInternet;
    } on TimeoutException {
      return StatusCodeConst.timeout;
    } catch (e) {
      print(e);
      return StatusCodeConst.general;
    }
  }

  void setSelectedClassRole(String role) {
    selectedClassRole = role;
  }

  void setListAnak(List<StudentIdList> data) {
    anak = data;
  }

  void setListFavorite(List<String> fav) {
    favoriteMenu = fav;
    prefs.then((value) {
      value.setStringList(FAVORITE, fav);
    });
  }

  void changeRefreshToken(String token) {
    refreshToken = token;
  }

  Future<void> fetchRole() async {
    var result = await ClassServices.detailSchool(id: currentState.schoolId);
    currentState.schoolRole = result.data.roles;
    var prefs = await SharedPreferences.getInstance();
    if (result.statusCode == 200) {
      await prefs.setStringList('role', currentState.schoolRole);
    }
  }

  ///memilih sekolah dengan id
  Future<void> choseSchoolById(int id) async {
    var school = await ClassServices.detailSchool(id: id);
    chosenSchool = school.data;
    await SharedPreferences.getInstance().then((value) {
      value.setStringList('school_role', currentState.schoolRole);
      if (value.containsKey(CLASS_NAME)) {
        currentState.className = value.getString(CLASS_NAME);
      }
    });
  }

  ///memilih sekolah dengan objeknya langsung
  void chooseSchool(DataSchoolModel school) async {
    chosenSchool = school;
    await SharedPreferences.getInstance().then((value) {
      value.setStringList('school_role', currentState.schoolRole);
      currentState.className = value.getString('class_name');
    });
  }

  Future<void> setListRole(List<String> role) async {
    await prefs.then((value) {
      value.setStringList('role', role);
    });
    currentState.schoolRole = role;
  }

  Future<void> setNamaSekolah(String schoolName) async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString('schoolName', schoolName);
    currentState.schoolName = schoolName;
  }

  Future<void> setBoolClass() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('isClass') == null) {
      isNoClasses = true;
    } else {
      var role = prefs.getBool('isclass');
      isNoClasses = role;
    }
  }

  void removeToken() {
    try {
      SharedPreferences.getInstance().then((prefs) {
        prefs.remove('auth_siswamedia_api');
        prefs.remove('schoolId');
        prefs.remove('role');
        prefs.remove('type');
        prefs.remove(FULL_NAME);
      });
      var profile = GlobalState.profile();
      isLogin = false;
      _authSiswamediaAPI = '';
      currentState = null;
      String topic = '/topics/${profile.state.profile.id}';
      FCM.instance.unsubscribeFromTopic(topic);
    } catch (e) {
      Logger().d(e.toString());
    }
  }

  Future<void> setCurrentClass({int classId, String className, String classRole}) async {
    currentState.className = className;
    currentState.classId = classId;
    currentState.classRole = classRole;
    await prefs.then((value) {
      value.setInt('class_id', classId);
      value.setString('class_name', className);
      value.setString('class_role', classRole);
    });
  }

  Future<void> initializeState({int userId}) async {
    await setBoolClass();
    try {
      if (isLogin && currentState == null) {
        var result = await ClassServices.getCurrentClass();
        if (result.statusCode != 200 || result.data == null) {
          throw Exception();
        }

        ///currentState Data
        final stateData = json.decode(result.data) as Map<String, dynamic>;
        var classState = Injector.getAsReactive<ClassState>();
        var data = UserCurrentState(
          schoolId: stateData[SCHOOL_ID],
          schoolName: stateData['school_name'],
          schoolRole: stateData['school_role'] == null ? [] : stateData[SCHOOL_ROLE].cast<String>(),
          type: stateData[SCHOOL_TYPE],
          classId: stateData[CLASS_ID],
          className: stateData[CLASS_NAME],
          classRole: stateData[CLASS_ROLE],
          isAdmin: stateData[IS_ADMIN],
        );
        Logger().d('--- currentState ---');
        Logger().d(data);
        await prefs.then((pref) {
          if (pref.containsKey(FAVORITE)) {
            var favorites = pref.getStringList(FAVORITE);
            setListFavorite(favorites);
          } else {
            pref.setStringList(FAVORITE, MenuData.menu.keys.toList());
            setListFavorite(MenuData.menu.keys.toList());
          }
          pref.setInt('class_id', stateData[CLASS_ID]);
          pref.setString('class_name', stateData[CLASS_NAME]);
          pref.setString('class_role', stateData[CLASS_ROLE]);
        });
        changeCurrentState(state: data);
        await ClassServices.setCurrentState(data: json.encode(data.toJson()));
        var model =
            ClassBySchoolModel(param: GetClassModel(schoold: stateData[SCHOOL_ID], me: true));
        await prefs.then((pref) async {
          if (pref.containsKey(TIME_LIST_CLASS)) {
            var date = pref.getString(TIME_LIST_CLASS);
            if (isDifferenceLessThanThirtyMin(date) && classState.state.classModel != null) {
            } else {
              await pref.setString(TIME_LIST_CLASS, DateTime.now().toIso8601String());
              await classState.setState((s) => s.retrieveData(data: model));
            }
          } else {
            await pref.setString(TIME_LIST_CLASS, DateTime.now().toIso8601String());
            await classState.setState((s) => s.retrieveData(data: model));
          }
        }).then((_) {
          if (currentState.classRole == 'ORTU') {
            setListAnak(classState.state.classMap.values
                .firstWhere((element) => element.id == stateData[CLASS_ID])
                .studentIdList);
          }
        });

        await choseSchoolById(stateData['school_id']);
      }
    } catch (e) {
      print(e);
      changeCurrentState(
          state: UserCurrentState(
        schoolId: null,
        schoolName: null,
        schoolRole: [],
        type: null,
        classId: null,
        className: null,
        classRole: null,
        isAdmin: null,
      ));
    }
  }

  void changeCurrentState({UserCurrentState state}) {
    currentState = state;
    Logger().d('--- change user current state ---');
    Logger().d(currentState.toJson());
    Utils.logWithDotes(state.toJson(), info: 'change current state');
    Utils.logWithDotes(currentState.type, info: 'type');
    prefs.then((value) {
      if (currentState.className != null) {
        value.setString(CLASS_NAME, currentState.className);
      } else {
        value.remove(CLASS_NAME);
      }
    });
  }
}
