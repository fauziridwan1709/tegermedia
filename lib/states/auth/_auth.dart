import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/core/encrypt/encryptor.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/models/user/_user.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/services/firebase/fcm.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/pages/HomePage/_home.dart';
import 'package:tegarmedia/ui/pages/_pages.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'auth_state.dart';
