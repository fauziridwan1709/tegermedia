# Modeling the reactive state

extends FutureState<T,K>.
K is generic model, which is include CreateModel, DeleteModel, UpdateModel, DataModel
visit [link] for more about models

example

```dart
class CourseState implements FutureState<CourseState, CourseModel> {
  ///put cacheKey here
  @override
  String cacheKey;
  
  ///put condition here value to notify when the state need to rebuild 
  @override
  bool condition;

  List<CourseDetail> courses;

  @override
  Future<void> retrieveData(CourseModel data) async {
    try {
      var course = await CourseServices.getMeCourse();
      courses = course.data;
    } on SocketException {
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  @override
  Future<void> createData(CourseModel data) async {
    try {
      var course = await CourseServices.createCourse(data.createModel);
      courses = course.data;
    } on SocketException {
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  @override
  Future<void> deleteData(CourseModel data) async {
    try {
      var course = await CourseServices.deleteCourse(data.deleteModel);
      courses = course.data;
    } on SocketException {
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  @override
  Future<void> updateData(CourseModel data) async {
    try {
      var course = await CourseServices.updateCourse(data.updateModel);
      courses = course.data;
    } on SocketException {
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }
}
```
