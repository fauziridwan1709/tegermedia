part of '_forum.dart';

class ForumState {
  AllForum _items;
  AllForum _items2;
  ForumDetailItem _chosenForum;

  AllForum get items => _items;
  AllForum get items2 => _items2;
  ForumDetailItem get chosenForum => _chosenForum;

  Future<void> fetchItems(int id, bool isClass) async {
    if (isClass) {
      var data =
          await ForumServices.getAllForum(schoolId: id, isClass: isClass);
      _items2 = data;
    } else {
      ///sekolah
      var data = await ForumServices.getAllForum(schoolId: id);
      _items = data;
      _items.data = data.data.where((element) => element.classId == 0).toList();
    }
  }

  Future<void> choseForum(ForumDetailItem value) async {
    _chosenForum = value;
  }

  Future<void> fetchChosenForum(int id) async {
    var data = await ForumServices.getForumById(id: id);
    _chosenForum = data.data;
  }

  //todo
  Future<void> addItem(ForumItem value) async {
    _items.data.add(value);
    // var resp = await ForumServices.
  }
}
