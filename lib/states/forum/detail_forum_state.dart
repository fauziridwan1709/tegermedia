part of '_forum.dart';

class DetailForumState {
  ForumDetailItem chosenForum;

  Future<void> fetchChosenForum(int id) async {
    try {
      var data = await ForumServices.getForumById(id: id);
      chosenForum = data.data;
    } on SocketException {
      print('no internet');
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('Timeout try again');
    } catch (e) {
      print(e);
      throw SiswamediaException('Terjadi kesalahan');
    }
  }
}
