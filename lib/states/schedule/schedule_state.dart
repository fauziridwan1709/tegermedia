part of '_schedule.dart';

class ScheduleState {
  List<ScheduleItem> _items;
  int _statusCode;

  Map<DateTime, List<ScheduleItem>> _dataMap = Map();
  DateTime day = DateTime.now();
  JadwalModel _solo = JadwalModel();
  List<String> kategori = ['Pelajaran', 'Ujian', 'Ekskul'];
  // 'Acara Sekolah'];
  String _fileName = '';
  List<dynamic> _dataList = <dynamic>[];

  Map<DateTime, List<ScheduleItem>> get datamap => _dataMap;
  List<ScheduleItem> get items => _items;
  int get statusCode => _statusCode;

  List<ScheduleItem> get selected => _dataMap[DateTime.now()];
  JadwalModel get solo => _solo;
  String get fileName => _fileName;
  List get dataList => _dataList;

  Future<void> resetItems() async {
    _dataMap = null;
    _items = null;
  }

  Future<void> fetchItems(int classId, bool isOrtu) async {
    var data = await ScheduleServices.getAllScheduleByClass(
        classId: classId, isOrtu: isOrtu);
    _dataMap = <DateTime, List<ScheduleItem>>{};
    _items = <ScheduleItem>[];
    _items = data.data;
    _statusCode = data.statusCode;
    print('fetching');
    for (var i = 0; i < _items.length; i++) {
      var raw = DateTime.parse(_items[i].startDate).toLocal().toString();
      print('data $raw');
      var time = DateTime.parse(raw.substring(0, 11) + '00:00:00.000');
      print(time);
      if (_dataMap[time] != null) {
        _dataMap[time].add(_items[i]);
      } else {
        _dataMap[time] = <ScheduleItem>[];
        _dataMap[time].add(_items[i]);
      }
    }
    print(_dataMap);
  }

  Future<GeneralResultAPI> createEvents(
      CreateScheduleModel item, String id, BuildContext context,
      {bool overlap = false}) async {
    var data = await ScheduleServices.createSchedule(
        reqBody: item, id: id, context: context, overlap: overlap);
    print('status ${data.statusCode}');
    return data;
  }

  Future<void> deleteSchedule(BuildContext context, int id) async {
    var data = await ScheduleServices.deleteScheduleById(id: id);
    if (data.statusCode.isSuccessOrCreated) {
      print(datamap);
      datamap.forEach((key, value) {
        var filter = value.removeWhere((element) => element.iD == id);
      });
      pop2(context);
      pop(context);
      CustomFlushBar.successFlushBar('Berhasil menghapus jadwal', context);
    } else {
      pop2(context);
      CustomFlushBar.errorFlushBar(data.message, context);
    }
  }

  void checkIn(int id, int scheduleId) {}

  void addList(dynamic value) {
    _dataList.add(value);
  }

  void setList() {
    _dataList = <dynamic>[];
  }

  Future<void> setName(String value) async {
    _fileName = value;
  }

  List<double> _offs = [];

  List<double> get offs => _offs;

  Future<void> addData(double value) async {
    _offs.add(value);
  }

  Future<void> reset() async {
    _offs = [];
  }

  void setJadwalModel(
      String judul, DateTime start, DateTime end, String kategori) {
    _solo.judul = judul;
    _solo.start = start;
    _solo.end = end;
    _solo.kategori = kategori;
  }
}
