import 'package:flutter/material.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/extension/_extension.dart';
import 'package:tegarmedia/core/navigator/navigator.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/ui/pages/Conference/_conference.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';

part 'schedule_state.dart';
