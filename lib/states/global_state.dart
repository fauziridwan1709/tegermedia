import 'package:tegarmedia/features/payment/presentation/states/_states.dart';
import 'package:tegarmedia/features/ppdb/presentation/states/_states.dart';
import 'package:tegarmedia/features/quiz/presentation/states/_states.dart';
import 'package:tegarmedia/features/studyJournal/presentation/states/_states.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/states/home/_home.dart';
import 'package:tegarmedia/states/journey/_journey.dart';

class GlobalState {
  static List<Injectable> injectDataMocks = <Injectable>[
    Inject(() => ClassState()),
    Inject(() => NotificationState()),
    Inject(() => ConferenceState()),
    Inject(() => NotificationState()),
    Inject(() => ProgressMessageState()),
    Inject(() => EditAbsensiState()),
    Inject(() => ClassroomState()),
    Inject(() => ProfileState()),
    Inject(() => FilterAssignmentState()),
    Inject(() => DocumentState()),
    Inject(() => AssignmentState()),
    Inject(() => PaymentState()),
    Inject(() => AuthState(currentState: UserCurrentState(classId: 7, classRole: 'GURU'))),
    Inject(() => ProgressUploadState()),
    Inject(() => ScheduleState()),
    Inject(() => AbsensiState()),
    Inject(() => MateriState()),
    Inject(() => ForumState()),
    Inject(() => MultiSelectClassState()),
    Inject(() => SchoolState()),
    Inject(() => CourseState(
            courseMap: HashMap.of({
          0: CourseByClassData(
              id: 0, name: 'test', listGuru: [ListGuru(userId: 8, nama: 'Muhamad Fauzi Ridwan')])
        }))),
    Inject(() => DetailClassState()),
    Inject(() => InteractionState()),
    Inject(() => HomeState()),
    Inject(() => CreateStudyJournalState()),
    Inject(() => TeacherState()),
    Inject(() => JourneyState()),
    Inject(() => DetailJournalState()),
    Inject(() => SppState()),
    Inject(() => PaymentSchoolState()),
  ];

  static var injectData = <Injectable>[
    Inject(() => ClassState()),
    Inject(() => NotificationState()),
    Inject(() => ConferenceState()),
    Inject(() => NotificationState()),
    Inject(() => ProgressMessageState()),
    Inject(() => EditAbsensiState()),
    Inject(() => ClassroomState()),
    Inject(() => ProfileState()),
    Inject(() => FilterAssignmentState()),
    Inject(() => DocumentState()),
    Inject(() => AssignmentState()),
    Inject(() => PaymentState()),
    Inject(() => AuthState()),
    Inject(() => ProgressUploadState()),
    Inject(() => ScheduleState()),
    Inject(() => AbsensiState()),
    Inject(() => MateriState()),
    Inject(() => ForumState()),
    Inject(() => MultiSelectClassState()),
    Inject(() => SchoolState()),
    Inject(() => CourseState()),
    Inject(() => DetailClassState()),
    Inject(() => InteractionState()),
    Inject(() => HomeState()),
    Inject(() => CreateStudyJournalState()),
    Inject(() => TeacherState()),
    Inject(() => JourneyState()),
    Inject(() => DetailJournalState()),
    Inject(() => SppState()),
    Inject(() => PulsaState()),
    Inject(() => PrePaidPulsesState()),
    Inject(() => SchoolNewsState()),
    Inject(() => PaymentSchoolState()),
    Inject(() => AdmissionState()),
    Inject(() => TransactionSppPendingState()),
    Inject(() => TransactionSppSuccessState()),
    Inject(() => TransactionPulsaPendingState()),
    Inject(() => TransactionPulsaSuccessState()),
    Inject(() => TransactionPostpaidPendingState()),
    Inject(() => TransactionPostpaidSuccessState()),
    Inject(() => TransactionEdukasiPendingState()),
    Inject(() => TransactionEdukasiSuccessState()),
    Inject(() => SppPpobState()),
    Inject(() => SppMarketState()),
    Inject(() => QuizMediaState()),
  ];

  static ReactiveModel<CreateStudyJournalState> dummy() {
    return Injector.getAsReactive<CreateStudyJournalState>();
  }

  static ReactiveModel<SchoolState> school() {
    return Injector.getAsReactive<SchoolState>();
  }

  static ReactiveModel<ProfileState> profile() {
    return Injector.getAsReactive<ProfileState>();
  }

  static ReactiveModel<ClassState> classes() {
    return Injector.getAsReactive<ClassState>();
  }

  static ReactiveModel<DetailClassState> detailClass() {
    return Injector.getAsReactive<DetailClassState>();
  }

  static ReactiveModel<ScheduleState> schedule() {
    return Injector.getAsReactive<ScheduleState>();
  }

  static ReactiveModel<ForumState> forum() {
    return Injector.getAsReactive<ForumState>();
  }

  static ReactiveModel<CourseState> course() {
    return Injector.getAsReactive<CourseState>();
  }

  static ReactiveModel<AbsensiState> absensi() {
    return Injector.getAsReactive<AbsensiState>();
  }

  static ReactiveModel<EditAbsenSelectedState> editAbsensiSelected() {
    return Injector.getAsReactive<EditAbsenSelectedState>();
  }

  static ReactiveModel<AssignmentState> assignment() {
    return Injector.getAsReactive<AssignmentState>();
  }

  static ReactiveModel<MultiSelectClassState> multiSelectClass() {
    return Injector.getAsReactive<MultiSelectClassState>();
  }

  static ReactiveModel<AuthState> auth() {
    return Injector.getAsReactive<AuthState>();
  }

  static ReactiveModel<ConferenceState> conference() {
    return Injector.getAsReactive<ConferenceState>();
  }

  static ReactiveModel<DocumentState> document() {
    return Injector.getAsReactive<DocumentState>();
  }

  static ReactiveModel<MateriState> materi() {
    return Injector.getAsReactive<MateriState>();
  }

  static ReactiveModel<QuizState> quiz() {
    return Injector.getAsReactive<QuizState>();
  }

  static ReactiveModel<NotificationState> notification() {
    return Injector.getAsReactive<NotificationState>();
  }

  static ReactiveModel<InteractionState> interaction() {
    return Injector.getAsReactive<InteractionState>();
  }

  static ReactiveModel<TeacherState> teachers() {
    return Injector.getAsReactive<TeacherState>();
  }

  static ReactiveModel<StudentState> students() {
    return Injector.getAsReactive<StudentState>();
  }

  static ReactiveModel<HomeState> home() {
    return Injector.getAsReactive<HomeState>();
  }

  static ReactiveModel<JourneyState> journey() {
    return Injector.getAsReactive<JourneyState>();
  }

  static ReactiveModel<SppMarketState> market() {
    return Injector.getAsReactive<SppMarketState>();
  }

  static ReactiveModel<PulsaState> pulsa() {
    return Injector.getAsReactive<PulsaState>();
  }

  static ReactiveModel<PrePaidPulsesState> prePaidPulsesState() {
    return Injector.getAsReactive<PrePaidPulsesState>();
  }

  static ReactiveModel<PaymentSchoolState> listSchool() {
    return Injector.getAsReactive<PaymentSchoolState>();
  }

  static ReactiveModel<AdmissionState> admission() {
    return Injector.getAsReactive<AdmissionState>();
  }

  static ReactiveModel<TransactionSppPendingState> sppPending() {
    return Injector.getAsReactive<TransactionSppPendingState>();
  }

  static ReactiveModel<TransactionSppSuccessState> sppSuccess() {
    return Injector.getAsReactive<TransactionSppSuccessState>();
  }

  static ReactiveModel<TransactionPulsaPendingState> pulsaPending() {
    return Injector.getAsReactive<TransactionPulsaPendingState>();
  }

  static ReactiveModel<TransactionPulsaSuccessState> pulsaSuccess() {
    return Injector.getAsReactive<TransactionPulsaSuccessState>();
  }

  static ReactiveModel<TransactionEdukasiPendingState> edukasiPending() {
    return Injector.getAsReactive<TransactionEdukasiPendingState>();
  }

  static ReactiveModel<TransactionEdukasiSuccessState> edukasiSuccess() {
    return Injector.getAsReactive<TransactionEdukasiSuccessState>();
  }
}
