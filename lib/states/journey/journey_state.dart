part of '_journey.dart';

class JourneyState implements FutureState<JourneyState, int> {
  @override
  String cacheKey = 'LIST_STUDY_JOURNAL';

  final df = DateFormat('yyyy-MM-dd');
  List<CourseJourneyData> courseJourney;
  List<CourseJourneyData> incomingCourseJourney;
  CourseJourneyData detailJourney = CourseJourneyData(
      videoUrl: '',
      startTime: '',
      journalId: 0,
      guide: '',
      name: '',
      attachment: <AttachmentResponse>[],
      comment: <Comment>[],
      teacherUserId: 0);
  List<DataAssignmentReference> listAssessment = [];
  List<ListQuiz> listQuiz = [];
  List<Comment> listComment = [];
  bool hasReachedMax = false;
  bool incomingHasReachedMax = false;
  int courseIdSelected;
  int classId = 0;
  int courseId;
  int page = 1;
  int pageIncoming = 1;

  @override
  bool getCondition() {
    return courseJourney != null && courseJourney.isNotEmpty;
  }

  bool getIncomingCondition() {
    return incomingCourseJourney != null && incomingCourseJourney.isNotEmpty;
  }

  void changeCourseId(int courseId) {
    Utils.log('change course id', info: 'changed');
    courseIdSelected = courseId;
  }

  @override
  Future<void> retrieveData(int k) async {
    classId = globalState.GlobalState.auth().state.currentState.classId;
    if (courseIdSelected != null) {
      try {
        var after =
            '${df.format(DateTime.now().subtract(Duration(days: 1)))}T17:00:00.000Z';
        var before = '${df.format(DateTime.now())}T17:00:00.000Z';
        // var query = '';
        // if (today) {
        // var query = 'after=$after&before=$before&limit=10&page=$page';
        var query = 'before=$before&limit=10&page=$page';
        // } else {
        //   query = 'after=$tomorrow&limit=10&page=$page';
        // }
        var resp = await GetJourneyServices.getJourney(
            courseIdSelected.toString(), query);

        courseJourney = resp.data;
        if (resp.data.length < 10) {
          hasReachedMax = true;
        } else {
          hasReachedMax = false;
        }
      } catch (ex) {
        print('Error ' + ex.toString());
      }
    }
  }

  Future<void> retrieveDataIncoming() async {
    classId = globalState.GlobalState.auth().state.currentState.classId;
    if (courseIdSelected != null) {
      try {
        var after =
            '${df.format(DateTime.now().subtract(Duration(days: 0)))}T17:00:00.000Z';
        var before = '${df.format(DateTime.now())}17:00:00.000Z';
        var query = 'after_equal=$after&limit=10&page=$pageIncoming';
        var resp = await GetJourneyServices.getJourney(
            courseIdSelected.toString(), query);

        incomingCourseJourney = resp.data;
        if (resp.data.length < 10) {
          print('---data');
          incomingHasReachedMax = true;
        } else {
          incomingHasReachedMax = false;
        }
      } catch (ex) {
        print('Error ' + ex.toString());
      }
    }
  }

  Future<void> loadMore() async {
    page++;
    var before = '${df.format(DateTime.now())}T17:00:00.000Z';
    var resp = await GetJourneyServices.getJourney(
        courseIdSelected.toString(), 'before=$before&limit=10&page=$page');
    resp.statusCode.translate<void>(
        ifSuccess: () {
          courseJourney.addAll(resp.data);
          if (resp.data.length < 10) {
            hasReachedMax = true;
          } else {
            hasReachedMax = false;
          }
        },
        ifElse: () => resp.statusCode.toFailure('Failure'));
  }

  Future<void> loadMoreIncoming() async {
    pageIncoming++;
    var after =
        '${df.format(DateTime.now().subtract(Duration(days: 0)))}T17:00:00.000Z';

    var resp = await GetJourneyServices.getJourney(courseIdSelected.toString(),
        'after_equal=$after&limit=10&page=$pageIncoming');
    resp.statusCode.translate<void>(
        ifSuccess: () {
          incomingCourseJourney.addAll(resp.data);
          if (resp.data.length < 10) {
            incomingHasReachedMax = true;
          } else {
            incomingHasReachedMax = false;
          }
        },
        ifElse: () => resp.statusCode.toFailure('Failure'));
  }

  void getLinkAssessment(String journalId) async {
    var auth = GlobalState.auth();
    try {
      var query =
          'class_id=${auth.state.currentState.classId}&journal_id=$journalId';
      var response = await GetJourneyServices.getAssessment(query);
      listAssessment = response.data;
      Utils.log(response.toString(), info: 'get link assessment');
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }

  void getQuiz(String journalId) async {
    print('get data course');
    var auth = GlobalState.auth();
    try {
      var query = 'class_id=$classId&journal_id=$journalId&limit=50&page=1';
      var response = await GetJourneyServices.getQuiz(query);

      listQuiz = response;
      print('get link');
      Utils.log(response.toString(), info: 'get link quiz');
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }

  Future<void> sendComment(String message, String journalId) async {
    try {
      var params = <String, dynamic>{
        'comment': '$message',
        'sent_time': DateTime.now().toIso8601String() + 'Z'
      };
      var query = '$journalId';
      print('send course $params');
      var response = await GetJourneyServices.addComment(query, params);

      listComment = response;
      Utils.log(response.toString(), info: 'get link assessment');
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }

  Future<void> getDetailJourney(String journalId) async {
    try {
      var query = '$journalId';
      var response = await GetJourneyServices.getDetailJournal(query);

      detailJourney = response;
      listComment = detailJourney.comment;
      Utils.log(response.toString(), info: 'get link assessment');
    } catch (ex) {
      print('Error ' + ex.toString());
    }
  }
}
