import 'package:date_time_picker/date_time_picker.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/features/studyJournal/data/models/quiz.dart';
import 'package:tegarmedia/models/journey/courser_journey.dart';
import 'package:tegarmedia/services/journey/_journey.dart';
import 'package:tegarmedia/states/global_state.dart';

import '../../models/assignment/_assignment.dart';
import '../global_state.dart' as globalState;

part 'journey_state.dart';
