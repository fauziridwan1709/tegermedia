part of '_materi.dart';

class MateriState {
  final MateriDetail materi = MateriDetail();
  final List<MateriDetail> fav = <MateriDetail>[];
  String search = '01=';
  List<MateriDocItemModel> book;
  List<MateriPresItem> presentation;
  List<MateriVideoItemModel> video;
  double offset = 0.0;

  Future<void> setOffset(double value) async {
    offset = value;
  }

  Future<void> setResources(
      {List<MateriVideoItemModel> videoList,
      List<MateriPresItem> presList,
      List<MateriDocItemModel> bookList}) async {
    if (videoList != null) {
      video = videoList;
    }
    if (presList != null) {
      presentation = presList;
    }
    if (bookList != null) {
      book = bookList;
    }
  }

  Future<void> setSearch(String value) async {
    search = value.toLowerCase();
  }

  void setMateri(MateriDetail value) {
    try {
      materi.jenjang = value.jenjang;
      materi.kelas = value.kelas;
      materi.kurikulum = value.kurikulum;
      materi.pelajaran = value.pelajaran;
      materi.pengguna = value.pengguna;
      materi.name = value.kelas;
    } catch (e) {
      print(e);
    }
  }

  void setPelajaran(String value) {
    materi.pelajaran = value;
  }

  void setJenjang(String value) {
    materi.jenjang = value;
  }

  void setKelas(String value) {
    materi.kelas = value;
  }

  void setKurikulum(String value) {
    materi.kurikulum = value;
  }

  void setJudul(String value) {
    materi.judul = value;
  }

  void addToFav(MateriDetail value) {
    fav.add(value);
  }

  void like(MateriDetail value) {
    fav.add(value);
  }
}
