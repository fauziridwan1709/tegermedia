part of '_school.dart';

class SchoolState implements FutureState<SchoolState, int> {
  @override
  String cacheKey;

  List<DataSchoolModel> listSchool;

  @override
  Future<void> retrieveData({int data}) async {
    try {
      var response = await UsersServices.getMySchools();
      listSchool = response.data;
    } on SocketException {
      throw SocketException('No Internet Connection, try again');
    } on TimeoutException {
      throw TimeoutException('timeout, request again');
    } catch (e) {
      print(e);
      throw SiswamediaException('Terjadi Kesalahan ${e.toString()}');
    }
  }

  @override
  Future<void> createData(BuildContext context, int data) {
    // TODO: implement createData
    throw UnimplementedError();
  }

  @override
  Future<void> deleteData(BuildContext context, int id) {
    // TODO: implement deleteData
    throw UnimplementedError();
  }

  @override
  bool getCondition() {
    // TODO: implement getCondition
    throw UnimplementedError();
  }

  @override
  Future<void> updateData(BuildContext context, int data) {
    // TODO: implement updateData
    throw UnimplementedError();
  }
}
