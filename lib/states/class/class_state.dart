part of '_class.dart';

class ClassState implements FutureState<ClassState, ClassBySchoolModel> {
  ClassBySchoolModel classModel;

  //small cache
  HashMap<int, ClassBySchoolData> classMap = HashMap<int, ClassBySchoolData>();

  @override
  String cacheKey = TIME_LIST_CLASS;

  @override
  bool getCondition() {
    return classMap.isNotEmpty;
  }

  Future<void> createData(BuildContext context, ClassBySchoolModel data) async {
    // TODO: implement createData
    classMap[data.cudModel.detail.id] = data.cudModel.detail;
  }

  Future<void> deleteData(BuildContext context, int id) async {
    Navigator.pop(context);
    SiswamediaTheme.infoLoading(context: context, info: 'Menghapus kelas..');
    try {
      await ClassServices.deleteClass(id: id).timeout(GLOBAL_TIMEOUT).then((value) {
        if (value.statusCode == StatusCodeConst.success) {
          Navigator.pop(context);
          CustomFlushBar.successFlushBar(
            value.message,
            context,
          );
          classMap.remove(id);
        } else {
          Navigator.pop(context);
          CustomFlushBar.errorFlushBar(
            value.message,
            context,
          );
        }
      });
    } catch (e) {
      Navigator.pop(context);
      CustomFlushBar.errorFlushBar(
        e.toString(),
        context,
      );
    }
  }

  @override
  Future<void> retrieveData({ClassBySchoolModel data}) async {
    try {
      var response =
          await ClassServices.getClassBySchoolID(id: data.param.schoolId, param: data.param.me);
      if (response.dataModel.isNotEmpty) {
        classMap = HashMap<int, ClassBySchoolData>();
        response.dataModel.forEach((element) {
          classMap[element.id] = element;
        });
      }
    } on SocketException {
      throw await SiswamediaException('There is no Internet Connection');
    } on EmptyException {
      throw SiswamediaException('Terjadi Kesalahan');
    } catch (e) {
      throw SiswamediaException('Maaf, terjadi kesalahan');
    }
  }

  @override
  Future<void> updateData(BuildContext context, ClassBySchoolModel data) async {
    Navigator.pop(context);
    SiswamediaTheme.infoLoading(context: context, info: 'Mengubah info kelas');
    var detail = data.cudModel.detail;
    await ClassServices.updateClass(detailClass: detail).then((value) {
      if (value.statusCode == StatusCodeConst.success) {
        Navigator.pop(context);
        CustomFlushBar.successFlushBar(
          value.message,
          context,
        );
        classMap[detail.id] = detail;
      } else {
        Navigator.pop(context);
        CustomFlushBar.errorFlushBar(
          value.message,
          context,
        );
      }
    });
  }

  void addData(ClassBySchoolData data) {}
}
