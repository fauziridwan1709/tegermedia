part of '_class.dart';

class ClassroomState {
  ClassroomCourse classroomCourse;
  ClassroomStudentsData classroomStudents;
  ClassroomTeachersData classroomTeachers;
  Map<String, Courses> mapData = <String, Courses>{};
  Map<String, GoogleClassroomClass> mapCourses =
      <String, GoogleClassroomClass>{};
  bool isSelectAll = false;

  Future<ClassroomCreateResultModel> import(String email) async {
    mapCourses.forEach((key, value) async {
      var data = value;
      var respTeachers = await ClassroomServices.getAllTeachers(courseId: key);
      var respStudents = await ClassroomServices.getAllStudents(courseId: key);
      var teachers = <String>[];
      var students = <String>[];
      if (respStudents.students != null) {
        respStudents.students.forEach((element) {
          students.add(element.profile.emailAddress);
        });
      }

      if (respTeachers.teachers != null) {
        respTeachers.teachers.forEach((element) {
          if (email != element.profile.emailAddress) {
            teachers.add(element.profile.emailAddress);
          }
        });
      }
      data.studentList = students;
      data.teacherList = teachers;

      return await ClassroomCreateServices.createClassFromClassroom(data);
    });
  }

  Future<void> getClassroom(Email email) async {
    try {
      var response = await ClassroomServices.getCoursesByTeacher(email: email)
          .timeout(GLOBAL_TIMEOUT,
              onTimeout: () => throw TimeoutException('Failed to get course'));
      classroomCourse = response;
      print(response);
      print('berhasil');
    } on SocketException {
      throw SocketException('No Internet Connection');
    } on TimeoutException catch (e) {
      throw SiswamediaException(e.message);
    } catch (e) {
      print(e);
      throw SiswamediaException('Terjadi Kesalahan');
    }
  }

  Future<void> getTeachers(String courseId) async {
    try {
      var response = await ClassroomServices.getAllTeachers(courseId: courseId)
          .timeout(GLOBAL_TIMEOUT,
              onTimeout: () =>
                  throw TimeoutException('Failed to get teachers'));
      classroomTeachers = response;
    } on SocketException {
      throw SocketException('No Internet Connection');
    } on TimeoutException catch (e) {
      throw SiswamediaException(e.message);
    } catch (e) {
      print(e);
      throw SiswamediaException('Terjadi Kesalahan');
    }
  }

  Future<void> getStudents(String courseId) async {
    try {
      var response = await ClassroomServices.getAllStudents(courseId: courseId)
          .timeout(GLOBAL_TIMEOUT,
              onTimeout: () =>
                  throw TimeoutException('Failed to get students'));
      classroomStudents = response;
    } on SocketException {
      throw SocketException('No Internet Connection');
    } on TimeoutException catch (e) {
      throw SiswamediaException(e.message);
    } catch (e) {
      throw SiswamediaException('Terjadi Kesalahan');
    }
  }

  void changeTahunAjaran(String courseId, String newValue) {
    mapCourses[courseId].tahunAjaran = newValue;
  }

  void changeSemester(String courseId, String newValue) {
    mapCourses[courseId].semester = newValue;
  }

  void changeTingkat(String courseId, String newValue) {
    mapCourses[courseId].tingkat = newValue;
  }

  void changeJurusan(String courseId, String newValue) {
    mapCourses[courseId].jurusan = newValue;
  }

  void changePelajaran(String courseId, String newValue) {
    mapCourses[courseId].courseName = newValue;
  }

  void switchSelectAll(int schoolId) {
    if (mapCourses.length == classroomCourse.courses.length) {
      classroomCourse.courses.forEach((element) {
        mapCourses.remove(element.id);
      });
    } else {
      classroomCourse.courses.forEach((element) {
        mapCourses[element.id] = GoogleClassroomClass(
            name: element.name,
            gcrId: element.id,
            schoolId: schoolId,
            courseName: null,
            tingkat: null,
            tahunAjaran: null,
            jurusan: null,
            semester: null,
            studentList: null,
            teacherList: null);
      });
    }
  }

  void switchSelect(Courses course, int schoolId) {
    if (mapCourses.containsKey(course.id)) {
      mapCourses.remove(course.id);
    } else {
      mapCourses[course.id] = GoogleClassroomClass(
          name: course.name,
          gcrId: course.id,
          schoolId: schoolId,
          courseName: null,
          tingkat: null,
          tahunAjaran: null,
          jurusan: null,
          semester: null,
          studentList: null,
          teacherList: null);
    }
  }

  void select(Courses course, int schoolId) {
    if (!mapCourses.containsKey(course.id)) {
      mapCourses[course.id] = GoogleClassroomClass(
          name: course.name,
          gcrId: course.id,
          schoolId: schoolId,
          courseName: null,
          tingkat: null,
          tahunAjaran: null,
          jurusan: null,
          semester: null,
          studentList: null,
          teacherList: null);
    }
  }
}
