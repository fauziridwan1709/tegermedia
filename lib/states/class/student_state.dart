part of '_class.dart';

class StudentState {
  StudentsClassroomModel studentModel;

  StudentState({this.studentModel});

  Future<void> retrieveData(int classId) async {
    try {
      studentModel = await ClassServices.getStudentClassRoom(id: classId);
    } on SocketException {
      throw SiswamediaException('There is no Internet Connection');
    }
  }

  Future<void> kickMember(int classId, int userId) async {
    try {
      await ClassServices.kickMemberClass(classId: classId, userId: userId).then((value) async {
        if (value.statusCode == StatusCodeConst.success || value.statusCode == 201) {
          await retrieveData(classId);
        }
      });
    } on SocketException {
      throw SiswamediaException('There is no Internet Connection');
    } catch (e) {
      throw SiswamediaException('Error');
    }
  }
}
