part of '_class.dart';

class DetailClassState extends FutureState<DetailClassState, int> {
  @override
  String cacheKey = TIME_DETAIL_CLASS;

  int id;

  DataDetailClassId detailClass;

  HashMap<int, DataDetailClassId> detailClassMap =
      HashMap<int, DataDetailClassId>();

  Future<void> getDetailClass(int classId) async {
    try {
      var response = await ClassServices.getClassByID(id: classId);
      print('hoy');
      detailClassMap[classId] = response.data;
      // if (data.data.isEmpty) throw EmptyException();
      detailClass = response.data;
    } on SocketException {
      throw SocketException('There is no \nInternet Connection');
    } on TimeoutException {
      throw TimeoutException('Timeout, try again');
    } catch (e) {
      print(e);
      throw SiswamediaException('Maaf, terjadi kesalahan');
    }
  }

  @override
  Future<void> retrieveData({int data}) async {
    try {
      var response = await ClassServices.getClassByID(id: data);
      print('hoy');
      detailClassMap[data] = response.data;
      // if (data.data.isEmpty) throw EmptyException();
      detailClass = response.data;
    } on SocketException {
      throw SocketException('There is no \nInternet Connection');
    } on TimeoutException {
      throw TimeoutException('Timeout, try again');
    } catch (e) {
      print(e);
      throw SiswamediaException('Maaf, terjadi kesalahan');
    }
  }

  @override
  bool getCondition() {
    // TODO: implement getCondition
    return detailClassMap.containsKey(id);
  }

  void setId(int ko) {
    id = ko;
    cacheKey = '$TIME_DETAIL_CLASS$ko';
  }

  void updateCountCourse(int id, bool isAdded) {
    if (isAdded) {
      detailClassMap[id].totalCourse++;
    } else {
      detailClassMap[id].totalCourse--;
    }
  }

  void updateCountTeacher(int id) {
    detailClassMap[id].totalGuru--;
  }
}
