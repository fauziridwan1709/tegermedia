part of '_class.dart';

class TeacherState {
  TeachersClassroomModel teacherModel;

  TeacherState({this.teacherModel});

  Future<void> retrieveData(int classId) async {
    try {
      print('memanggil');
      var response = await ClassServices.getTeacherClassroom(id: classId);
      // print(response.data.first.email);
      if (response.data.isEmpty) throw EmptyException();
      teacherModel = response;
    } on SocketException {
      print('aduh');
      teacherModel = null;

      throw await SiswamediaException('There is no Internet Connection');
    } on EmptyException {
      teacherModel = null;
      print('here');
    }
  }

  void clearData() {
    teacherModel = null;
  }
}
