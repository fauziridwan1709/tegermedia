part of '_class.dart';

class ClassroomCredentialState {
  bool status;
  String url;

  Future<void> checkCredential() async {
    try {
      print('checking credential');
      var response = await Oauth2.getOauthUrl();
      url = response.data.url;
      status = false;
    } on AlreadyExistException {
      status = true;
    } on SocketException {
      throw SocketException('No Internet');
    } on TimeoutException {
      throw SiswamediaException('Timeout, poor connection');
    } catch (e) {
      print(e);
      throw SiswamediaException('Terjadi Kesalahan');
    }
  }

  void resetCredential() {
    status = null;
    url = null;
  }
}
