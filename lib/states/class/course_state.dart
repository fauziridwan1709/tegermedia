part of '_class.dart';

class CourseState implements FutureState<CourseState, CourseByClassModel> {
  CourseByClassModel courseModel;

  HashMap<int, CourseByClassData> courseMap = HashMap<int, CourseByClassData>();
  CourseState({this.courseMap});

  @override
  String cacheKey = TIME_LIST_COURSE;

  @override
  Future<void> createData(BuildContext context, CourseByClassModel data) {
    // TODO: implement createData
    throw UnimplementedError();
  }

  @override
  Future<void> deleteData(BuildContext context, int id) {
    // TODO: implement deleteData
    throw UnimplementedError();
  }

  @override
  bool getCondition() {
    return false;
  }

  @override
  Future<void> updateData(BuildContext context, CourseByClassModel data) {
    // TODO: implement updateData
    throw UnimplementedError();
  }

  void clearData() {
    courseModel = null;
  }

  @override
  Future<void> retrieveData({CourseByClassModel data}) async {
    try {
      vUtils.setLog('gila');
      print(data.param.classId);
      var response =
          await ClassServices.getCourseByClass(classId: data.param.classId);
      // if (response.data.isEmpty) throw EmptyException();
      courseModel = response;
      courseModel.data.sort((a, b) => a.id.compareTo(b.id));
      // courseModel.
      courseMap = HashMap<int, CourseByClassData>();
      courseModel.data.forEach((element) {
        courseMap[element.id] = element;
      });
    } on SocketException {
      throw SiswamediaException('There is no Internet Connection');
    } on EmptyException {
      throw SiswamediaException('Belum ada course');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }
}
