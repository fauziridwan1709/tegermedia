import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:tegarmedia/core/_core.dart';
import 'package:tegarmedia/core/constants/_constants.dart';
import 'package:tegarmedia/data/_static_data.dart';
import 'package:tegarmedia/models/_models.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/states/future.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:tegarmedia/utils/v_utils.dart';

part 'class_state.dart';
part 'classroom_credential_state.dart';
part 'classroom_state.dart';
part 'course_state.dart';
part 'detail_class_state.dart';
part 'student_state.dart';
part 'teacher_state.dart';
