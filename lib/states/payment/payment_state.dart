part of '_payment.dart';

class PaymentState {
  Map<String, PaymentModel> payments = <String, PaymentModel>{};

  void addPayment(PaymentModel value) {
    payments[value.name] = value;
  }

  void removePayment(PaymentModel value) {
    payments.remove(value.name);
  }
}

class PaymentModel {
  String name;
  int price;

  PaymentModel({this.name, this.price});
}
