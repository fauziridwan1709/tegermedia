import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:tegarmedia/aw/widgets/custom/_custom.dart';
import 'package:tegarmedia/core/environment/_environment.dart';
import 'package:tegarmedia/core/notification_plugin.dart';
import 'package:tegarmedia/core/state/_states.dart';
import 'package:tegarmedia/core/utils/_utils.dart';
import 'package:tegarmedia/core/versioning/_versioning.dart';
import 'package:tegarmedia/services/_services.dart';
import 'package:tegarmedia/services/firebase/fcm.dart';
import 'package:tegarmedia/states/_states.dart';
import 'package:tegarmedia/ui/pages/_pages.dart';
import 'package:tegarmedia/ui/widgets/_widgets.dart';
import 'package:url_launcher/url_launcher.dart';

import 'core/_core.dart';
import 'features/payment/presentation/states/_states.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Injector(
      inject: GlobalState.injectData,
      builder: (context) {
        return MaterialApp(
          key: Key('MaterialApp'),
          title: 'Siswamedia',
          debugShowCheckedModeBanner: false,
          navigatorKey: locator<NavigationService>().navigatorKey,
          builder: (context, child) => MediaQuery(
              data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child),
          home: Wrapper(),
          // home: PulsaPage(),
          // home: PaymentSppPage(),
          theme: ThemeData(
              primarySwatch: SiswamediaTheme.primary,
              accentColor: SiswamediaTheme.green,
              visualDensity: VisualDensity.compact,
              dividerColor: Colors.transparent,
              // inputDecorationTheme: InputDecorationTheme(
              //   filled: true,
              // ),
              appBarTheme: AppBarTheme(
                iconTheme: IconThemeData(color: SiswamediaTheme.green),
                brightness: Brightness.light,
                color: SiswamediaTheme.white,
              ),
              fontFamily: 'OpenSans'),
          initialRoute: '/',
        );
      },
    );
  }
}

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      initApp(context);
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  Future<void> initApp(BuildContext context) async {
    if (await canUpdate(context)) {
      return showDialog<void>(
          context: context,
          builder: (_) => WillPopScope(
                onWillPop: () async => SystemNavigator.pop(animated: true).then((_) => true),
                child: Dialog(
                  shape: RoundedRectangleBorder(borderRadius: radius(12)),
                  child: Container(
                    width: S.w * .7,
                    padding: EdgeInsets.all(16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset('assets/images/versioning/update.png',
                            height: S.w * .4, width: S.w * .4),
                        SizedBox(height: 20),
                        CustomText('Update!', Kind.heading2),
                        SizedBox(height: 5),
                        CustomText(
                            'Silahkan untuk mengupdate aplikasi ke versi terbaru untuk melanjutkan',
                            Kind.descBlack,
                            align: TextAlign.center),
                        SizedBox(height: 20),
                        CustomContainer(
                          color: SiswamediaTheme.nearBlack,
                          padding: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                          radius: radius(8),
                          onTap: () => launch(
                              'https://play.google.com/store/apps/details?id=com.siswamedia.main'),
                          child: Center(
                            child: Text('Update', style: semiWhite.copyWith(fontSize: S.w / 30)),
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
              )).then((value) => SystemNavigator.pop(animated: true));
    } else {
      await startTime();
    }
  }

  Future<void> startTime() async {
    final authState = GS.auth();
    Timer(
        Duration(milliseconds: 1000),
        () async =>
            await authState.setState((auth) => auth.initialize(), onData: (_, data) => route()));
  }

  void route() {
    Navigator.of(context).pushReplacement<void, void>(_route());
  }

  Route _route() {
    return MaterialPageRoute<void>(builder: (_) => Layout());
  }

  // Route _createRoute() {
  //   return PageRouteBuilder<void>(
  //     pageBuilder: (context, animation, secondary) => Layout(),
  //     transitionDuration: Duration(milliseconds: 1000),
  //     transitionsBuilder: (context, animation, secondaryAnimation, child) {
  //       var begin = 0.0;
  //       var end = 1.0;
  //       var curve = Curves.ease;
  //
  //       var tween =
  //           Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
  //
  //       return FadeTransition(
  //         opacity: animation.drive(tween),
  //         child: child,
  //       );
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    S().init(context);
    var size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: [
        Container(
          color: SiswamediaTheme.white,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40.0),
              child: Image.asset(
                'assets/logo tegarmedia-03.png',
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),

        if (SiswamediaConfig.isStaging)
          Positioned(
              top: 0,
              child: Container(
                height: 0,
                width: 0,
              )),
        // Positioned(
        //   bottom: 80,
        //   child: SimpleText('Versi terbaru, 29 May 2021 05:55 AM'),
        // ),
        Positioned(
          width: size.width,
          bottom: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(
                child: SpinKitFadingCube(
                  key: Key('SpinKitSplashScreen'),
                  duration: Duration(seconds: 1),
                  size: 40,
                  color: Colors.white,
                ),
              )
            ],
          ),
        )
      ],
    ));
  }
}

class Layout extends StatefulWidget {
  @override
  _LayoutState createState() => _LayoutState();
}

class _LayoutState extends State<Layout> {
  final notification = Injector.getAsReactive<NotificationState>();
  final auth = Injector.getAsReactive<AuthState>();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  List<Widget> _children;
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    _children = <Widget>[
      HomePage(),
      ClassPage(),
      AcademicHomePage(),
      Materi(),
      // PaymentPage(),
    ];

    initFirebaseMessaging();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      PrecacheImage.cache(context);
      await fetchData();
    });
  }

  void initFirebaseMessaging() async {
    print('initiate firebase messaging');
    await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage event) async {
      print('opened app');
      await notificationPlugin.showNotification(
        1,
        event.notification.body,
        event.notification.title,
        event.data['type'],
        event.data['payment_id'],
      );
      // if (auth.isLogin) {
      //   getDataCount();
      // }
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage event) async {
      await notificationPlugin.showNotification(
        1,
        event.notification.body,
        event.notification.title,
        event.data['type'],
        event.data['payment_id'],
      );
      // if (auth.isLogin) {
      //   getDataCount();
      // }
      Logger().d(event.data);
      if (event.data['type'] == 'MARKET_NOTIFICATION') {
        Logger().d(event.data);
        var spp = Injector.getAsReactive<SppState>();
        if (spp.state.pageContext != null) {
          Logger().d(ModalRoute.of(spp.state.pageContext).settings.name);
          Navigator.popUntil(spp.state.pageContext, (route) => route.isFirst);
        }
        // await showDialog<bool>(
        //     context: context,
        //     builder: (_) {
        //       return QuizDialog(
        //         image: 'assets/images/png/payment_info.png',
        //         title: 'Hati-hati lupa?',
        //         description: '',
        //         actions: ['Ya', 'Tidak'],
        //         type: DialogType.textImage,
        //         onTapFirstAction: () async {
        //           Navigator.pop(context, true);
        //         },
        //         onTapSecondAction: () {
        //           Navigator.pop(context, false);
        //         },
        //       );
        //     });
      }
      if (event.data['type'] == 'PPOB_NOTIFICATION') {
        Logger().d(event.data);
        var spp = Injector.getAsReactive<SppState>();
        if (spp.state.pageContext != null) {
          Logger().d(ModalRoute.of(spp.state.pageContext).settings.name);
          Navigator.popUntil(spp.state.pageContext, (route) => route.isFirst);
        }
        // await showDialog<bool>(
        //     context: context,
        //     builder: (_) {
        //       return QuizDialog(
        //         image: 'assets/images/png/payment_info.png',
        //         title: 'Hati-hati lupa?',
        //         description: '',
        //         actions: ['Ya', 'Tidak'],
        //         type: DialogType.textImage,
        //         onTapFirstAction: () async {
        //           Navigator.pop(context, true);
        //         },
        //         onTapSecondAction: () {
        //           Navigator.pop(context, false);
        //         },
        //       );
        //     });
      }
    });
    await _firebaseMessaging.getToken().then((token) => setState(() {
          print('token: $token');
          NotificationServices.registerToken(token: token);
        }));
    _firebaseMessaging.onTokenRefresh.listen((token) {
      NotificationServices.registerToken(token: token);
    });
    FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);
  }

  static Future<dynamic> myBackgroundMessageHandler(RemoteMessage event) async {
    if (Platform.isAndroid) {
      // Intent launchIntent =
      //     getPackageManager().getLaunchIntentForPackage("com.package.address");
      // if (launchIntent != null) {
      //   startActivity(
      //       launchIntent); //null pointer check in case package name was not found
      // }
      // AndroidIntent intent = AndroidIntent(
      //     // action: 'android.intent.action.MAIN',
      //     package: 'com.siswamedia.main',
      //     componentName: 'com.siswamedia.main.MainActivity'
      //     // data: 'package:com.siswamedia.main',
      //     // arguments: {'authAccount': currentUserEmail},
      //     );
    }
    Logger().d(event.data);
    print(event.data);
    print(event.notification.body);
    print(event.notification.title);
    await notificationPlugin.showNotification(
      1,
      event.notification.body,
      event.notification.title,
      event.data['type'],
      event.data['payment_id'],
    );
    if (event.data['type'] == 'MARKET_NOTIFICATION') {}
  }

  void getDataCount() async {
    if (auth.state.isLogin) {
      await notification.setState((s) => s.getCount());
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // PrecacheImage.cache(context);
  }

  Future<void> fetchData() async {
    var profile = Injector.getAsReactive<ProfileState>();
    var authState = auth.state;
    await auth.state.initialize();
    if (authState.isLogin) {
      await auth.setState(
        (s) => s.initializeState(),
        onData: (_, data) async =>
            await profile.setState((s) => s.retrieveData(), onError: (context, dynamic error) {
          CustomFlushBar.errorFlushBar('Sesimu telah berakhir, Login lagi', context);
          auth.setState((s) => s.removeToken());
          auth.refresh();
          profile.refresh();
        }, onData: (_, data) {
          String topic = '/topics/${profile.state.profile.id}';
          FCM.subscribeToTopic(topic);
          profile.notify();
          auth.notify();
        }),
      );
    }

    if (authState.currentState != null) {
      await authState.choseSchoolById(authState.currentState.schoolId);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: false,
        // body: IndexedStack(index: _selectedIndex, children: _children),
        body: _children[_selectedIndex],
        bottomNavigationBar: _convexNavigation());
  }

  Widget _convexNavigation() {
    return ConvexAppBar(
      items: [
        TabItem<IconData>(icon: CustomIcons.home, title: 'Home'),
        TabItem<IconData>(icon: CustomIcons.kelas, title: 'Kelas'),
        TabItem<IconData>(icon: CustomIcons.academy, title: 'Akademis'),
        TabItem<IconData>(icon: CustomIcons.materi, title: 'Materi'),
        // TabItem<IconData>(icon: CustomIcons.store, title: 'Belanja'),
      ],
      backgroundColor: SiswamediaTheme.white,
      activeColor: SiswamediaTheme.materialGreen[400],
      color: SiswamediaTheme.green,
      top: -8,
      height: 60,
      curve: Curves.easeInOut,
      initialActiveIndex: _selectedIndex,
      onTap: (int index) {
        setState(() => _selectedIndex = index);
      },
    );
  }
}

GetIt locator = GetIt.instance;

Future setupLocator() async {
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
}

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(Widget newPage) {
    return navigatorKey.currentState.push(MaterialPageRoute(builder: (_) => newPage));
  }
}
